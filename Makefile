src = $(wildcard Morpho/*.c)
obj = $(src:.c=.o)
PREFIX = /usr/local

LDFLAGS = -lm -lGL -lblas -llapack -lpthread -ldrawtext -lGLU -lglfw 
CFLAGS = -DPLATFORM -std=c99 -O3 -DSHOW_FONT="\"/usr/share/fonts/truetype/lao/Phetsarath_OT.ttf\""

morpho: $(obj)
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

.PHONY: clean
clean:
	rm -f $(obj) morpho

.PHONY: install
install: morpho
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $< $(DESTDIR)$(PREFIX)/bin/morpho

