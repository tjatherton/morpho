/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "manifold.h"

/*
 * Low level manifold manipulation
 */

expression *manifold_new() {
    manifold_object *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(manifold_object *) class_instantiate(class_lookup(MANIFOLD_LABEL));
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/* Initializes a manifold object */
void manifold_init(manifold_object *manifold) {
    if (manifold) {
        manifold->vertices=NULL;
        manifold->flags=0;
        
        manifold->grade=0;
        manifold->dimension=0;
        
        manifold->gradelower=NULL;
        manifold->graderaise=NULL;
    }
}

/* Frees a manifold object */
void manifold_freegraderaiseentrymapfunction(uid id, void *el, void *ref) {
    manifold_graderaiseentry *entry=(manifold_graderaiseentry *) el;
    
    linkedlist_map(entry->list, EVAL_FREE);
    linkedlist_free(entry->list);
    
    EVAL_FREE(entry);
}

void manifold_freevertexmapfunction(uid id, void *el, void *ref) {
    manifold_vertex *v=el;
    
    manifold_freevertex(v);
}

void manifold_free(manifold_object *manifold, interpretcontext *context) {
    if (manifold) {
        if (manifold->vertices) {
            /* Free attached vertices */
            idtable_map(manifold->vertices, manifold_freevertexmapfunction, NULL);
            idtable_free(manifold->vertices);
        }
        
        if (manifold->gradelower) {
            for (unsigned int i=0; i<manifold->grade; i++) {
                if (manifold->gradelower[i]) {
                    idtable_map(manifold->gradelower[i], idtable_genericfreemapfunction, NULL);
                    idtable_free(manifold->gradelower[i]);
                }
            }
            EVAL_FREE(manifold->gradelower); // Free the gradelower table itself
        }
        
        if (manifold->graderaise) {
            for (unsigned int i=0; i<manifold->grade; i++) {
                if (manifold->graderaise[i]) {
                    idtable_map(manifold->graderaise[i], manifold_freegraderaiseentrymapfunction, NULL);
                    idtable_free(manifold->graderaise[i]);
                }
            }
            EVAL_FREE(manifold->graderaise); // Free the graderaise table itself
        }
    }
    /* Note we don't free the manifold because the class takes care of that. */
}

/* Creates a new vertex element */
manifold_vertex *manifold_newvertex(MFLD_DBL *x, unsigned int dim, unsigned int flags) {
    manifold_vertex *m = EVAL_MALLOC(sizeof(manifold_vertex));
    
    if (m) {
        m->flags=flags;
        m->x=EVAL_MALLOC(sizeof(MFLD_DBL)*dim);
        if (m->x) {
            memcpy(m->x, x, sizeof(MFLD_DBL)*dim);
        }
    }
    
    return m;
}

/* Frees a vertex element */
void manifold_freevertex(manifold_vertex *v) {
    if (v) {
        if (v->x) EVAL_FREE(v->x);
        EVAL_FREE(v);
    }
}

/* Adds a vertex to an manifold */
uid manifold_addvertex(manifold_object *manifold, interpretcontext *context, unsigned int dim, MFLD_DBL *x, unsigned int flags) {
    uid id=0;
    manifold_vertex *m=NULL;
    
    /* Does the manifold have an idtable? */
    if (!manifold->vertices) {
        manifold->vertices=idtable_create(MANIFOLD_INITIALSIZE);
        manifold->dimension=dim;
    }
    
    if (manifold->vertices) {
        if (manifold->dimension==dim) {
           
            m=manifold_newvertex(x, manifold->dimension, flags);
            if (m) {
                id=idtable_insert(manifold->vertices, m);
                
                if (manifold->graderaise && manifold->graderaise[0]) {
                    /* If higher grade elements are already present, we need to create a grade raising element for this vertex */
                    manifold_graderaiseentry *vraise= manifold_newgraderaiseentry(context);
                    
                    idtable_insertwithid(manifold->graderaise[0], id, vraise);
                }
            } else MANIFOLD_ALLOCATIONFAILED;
        } else {
            error_raise(context, ERROR_MANIFOLD_INCSTDIMENSION, ERROR_MANIFOLD_INCSTDIMENSION_MSG, ERROR_FATAL);
        }
    }
    
    return id;
}

/* Deletes a vertex from a manifold */
int manifold_deletevertex(manifold_object *manifold, interpretcontext *context, uid id) {
    void *data=NULL;
    manifold_graderaiseentry *gr;
    
    if (manifold->vertices) {
        data = idtable_get(manifold->vertices, id);
        if (data) manifold_freevertex((manifold_vertex *) data);
        
        idtable_remove(manifold->vertices, id);
    }
    
    if (manifold->graderaise && manifold->graderaise[0]) {
        if (idtable_ispresent(manifold->graderaise[0], id)) {
            gr=idtable_get(manifold->graderaise[0], id);
            if (gr) manifold_freegraderaiseentry(gr);
            idtable_remove(manifold->graderaise[0], id);
        }
    }
    
    return TRUE;
}

/* Retrieves a vertex's position */
int manifold_getvertexposition(manifold_object *manifold, interpretcontext *context, uid id, MFLD_DBL *x) {
    int ret = FALSE;
    manifold_vertex *mx=NULL;
    
    if (manifold->vertices) {
        mx = (manifold_vertex *) idtable_get(manifold->vertices, id);
        
        if (mx) {
            memcpy(x, mx->x, sizeof(MFLD_DBL)*manifold->dimension);
            ret=TRUE; 
        }
    }
    
    return ret;
}

/* Sets a vertex's position */
int manifold_setvertexposition(manifold_object *manifold, interpretcontext *context, uid id, MFLD_DBL *x) {
    int ret = FALSE;
    manifold_vertex *mx=NULL;
    
    if (manifold->vertices) {
        mx = (manifold_vertex *) idtable_get(manifold->vertices, id);
        
        if (mx) {
            memcpy(mx->x, x, sizeof(MFLD_DBL)*manifold->dimension);
            ret=TRUE;
        }
    }
    
    return ret;
}

/*
 * Clone a manifold
 */

void manifold_clonevertexmapfunction(uid id, void *e, void *r ) {
    manifold_object *mobj = r;
    manifold_vertex *v = e;
    manifold_vertex *nv = NULL;
    
    nv=manifold_newvertex(v->x, mobj->dimension, v->flags);
    if (nv) idtable_insertwithid(mobj->vertices, id, nv);
}

void manifold_clonegradelowerentrymapfunction(uid id, void *e, void *r ) {
    idtable *target = r;
    manifold_gradelowerentry *el = e, *new=NULL;
    
    new = EVAL_MALLOC(sizeof(manifold_gradelowerentry));
    if (new) {
        new->el[0]=el->el[0]; new->el[1]=el->el[1];
        idtable_insertwithid(target, id, new);
    }
}

void manifold_clonegraderaiseentrymapfunction(uid id, void *e, void *r ) {
    idtable *target = r;
    manifold_graderaiseentry *el = e, *new=NULL;
    
    new = EVAL_MALLOC(sizeof(manifold_graderaiseentry));
    if (new) {
        new->list=NULL;
        if (el->list) {
            new->list=linkedlist_new();
            if (new->list) {
                for (linkedlistentry *f=el->list->first; f!=NULL; f=f->next) {
                    uid *fid=f->data;
                    if (fid) {
                        uid *nentry = EVAL_MALLOC(sizeof(uid));
                        *nentry = *fid;
                        linkedlist_addentry(new->list, nentry);
                    }
                }
            }
        }
        idtable_insertwithid(target, id, new);
    }
}

expression *manifold_clone(manifold_object *mobj, interpretcontext *context) {
    if (!mobj) return NULL;
    
    expression *new=manifold_new();
    
    if (eval_isobjectref(new)) {
        manifold_object *mnew = (manifold_object *) eval_objectfromref(new);
        
        mnew->dimension=mobj->dimension;
        mnew->flags=mobj->flags;
        
        /* Copy across vertices */
        if (mobj->vertices) {
            mnew->vertices=idtable_create(mobj->vertices->size);
            if (mnew->vertices) idtable_map(mobj->vertices, manifold_clonevertexmapfunction, mnew);
        }
        
        manifold_resizegradetable(mnew, context, mobj->grade);
        
        if (mobj->gradelower) {
            for (unsigned int i=0; i<mobj->grade; i++) {
                idtable_map(mobj->gradelower[i], manifold_clonegradelowerentrymapfunction, mnew->gradelower[i]);
            }
        }
        
        if (mobj->graderaise) {
            for (unsigned int i=0; i<mobj->grade; i++) {
                idtable_map(mobj->graderaise[i], manifold_clonegraderaiseentrymapfunction, mnew->graderaise[i]);
            }
        }
    }
    
    return new;
}

/* 
 * Fixed vertices
 */

int manifold_isvertexentryfixed(manifold_vertex *v) {
    if (v->flags & MANIFOLD_VERTEX_FIXED) return TRUE;
    return FALSE;
}

int manifold_isvertexfixed(manifold_object *manifold, interpretcontext *context, uid id) {
    manifold_vertex *mx=NULL;
    int ret=FALSE;
    
    if (manifold->vertices) {
        mx = (manifold_vertex *) idtable_get(manifold->vertices, id);
        
        if ((mx)&&(mx->flags & MANIFOLD_VERTEX_FIXED)) {
            ret=TRUE;
        }
    }
    
    return ret;
}

void manifold_setvertexfixed(manifold_object *manifold, interpretcontext *context, uid id, int fix) {
    manifold_vertex *mx=NULL;
    
    if (manifold->vertices) {
        mx = (manifold_vertex *) idtable_get(manifold->vertices, id);
        
        if ((mx)) {
            if (fix) { mx->flags |= MANIFOLD_VERTEX_FIXED;
            } else { mx->flags &= ~MANIFOLD_VERTEX_FIXED; }
        }
    }
}

/* Set whether vertices are fixed or not based on a selection */
typedef struct {
    selection_object *sel;
    manifold_object *mobj;
    int fix;
} manifold_fixwithselectionmaptype;

void manifold_fixwithselectionxmapfunction(uid id, void *e, void *r ) {
    manifold_fixwithselectionmaptype *ref = (manifold_fixwithselectionmaptype *) r;
    manifold_vertex *v = (manifold_vertex *) e;
    
    if (selection_isselected(ref->sel, ref->mobj, MANIFOLD_GRADE_POINT, id)) {
        if (ref->fix) { v->flags |= MANIFOLD_VERTEX_FIXED;
        } else { v->flags &= ~MANIFOLD_VERTEX_FIXED;
        }
    }
}

void manifold_fixwithselection(manifold_object *mobj,  interpretcontext *context, selection_object *sel, int fix) {
    manifold_fixwithselectionmaptype ref;
    
    ref.sel=sel;
    ref.mobj=mobj;
    ref.fix=fix;
    
    if ((mobj)&&(mobj->vertices)) idtable_map(mobj->vertices, manifold_fixwithselectionxmapfunction, &ref);
}

/* 
 * Grade lowering
 */

/* Create a new grade lowering entry */
manifold_gradelowerentry *manifold_newgradelowerentry(manifold_gradelowerentry *element, interpretcontext *context) {
    manifold_gradelowerentry *new=NULL;
    
    if (element) {
        new = EVAL_MALLOC(sizeof(manifold_gradelowerentry));
        if (new) {
            new->el[0]=element->el[0]; new->el[1]=element->el[1];
        } else {
            error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
        }
    }
    
    return new;
}

/* Free a grade lowering entry */
void manifold_freegradelowerentry(manifold_gradelowerentry *e) {
    EVAL_FREE(e);
}

/* Create a new, empty, grade raising entry */
manifold_graderaiseentry *manifold_newgraderaiseentry(interpretcontext *context) {
    manifold_graderaiseentry *new=NULL;
    
    new = EVAL_MALLOC(sizeof(manifold_graderaiseentry));
    if (new) {
        new->list=linkedlist_new();
        if (!new->list) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
    } else {
        error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
    }
    
    return new;
}

/* Free a grade raising entry */
void manifold_freegraderaiseentry(manifold_graderaiseentry *e) {
    if (e) {
        if (e->list) {
            linkedlist_map(e->list, EVAL_FREE);
            linkedlist_free(e->list);
        }
        EVAL_FREE(e);
    }
}

/* Adds an id to a grade raising entry */
void manifold_addidtograderaiseentry(interpretcontext *context, manifold_graderaiseentry *e, uid id) {
    uid *new;
    if (e) {
        new = EVAL_MALLOC(sizeof(uid));
        
        if (new) {
            *new = id;
            linkedlist_addentry(e->list, new);
        } else {
            error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
        }
    }
}

/* Adds an id to a grade raising entry */
void manifold_removeidfromgraderaiseentry(interpretcontext *context, manifold_graderaiseentry *raise, uid id) {
    if (raise && raise->list) {
        linkedlistentry *e=NULL;
        
        for (e=raise->list->first; e!=NULL; e=e->next) {
            if (e->data) {
                if (id == (*(uid *) e->data)) break;
            }
        }
    
        if (e) { /* Remove the entry */
            void *del=e->data;
            linkedlist_removeentry(raise->list, e->data);
            EVAL_FREE(del);
        }
    }
}

/* Looks up a grade raise entry for element id of grade grade and adds id target */
void manifold_lookupandaddidtograderaiseentry(manifold_object *manifold, interpretcontext *context, uid id, unsigned int grade, uid target ) {
    manifold_graderaiseentry *f = NULL;
    
    if (manifold->graderaise && manifold->graderaise[grade]) f=idtable_get(manifold->graderaise[grade], id);
    
    if (!f) {
        /* The element doesn't have a grade raising entry, so we will try to add one */
        f=manifold_newgraderaiseentry(context);
        
        if (f) idtable_insertwithid(manifold->graderaise[grade], id, f);
    }
    
    if (f) manifold_addidtograderaiseentry(context, f, target);
}

/* Looks up a grade raise entry for element id of grade grade and removes id target */
void manifold_lookupandremoveidfromgraderaiseentry(manifold_object *manifold, interpretcontext *context, uid id, unsigned int grade, uid target ) {
    manifold_graderaiseentry *f = NULL;

    if (manifold->graderaise && manifold->graderaise[grade]) f=idtable_get(manifold->graderaise[grade], id);
    if (f) manifold_removeidfromgraderaiseentry(context, f, target);
}

/* Add element */
uid manifold_addelement(manifold_object *manifold, interpretcontext *context, unsigned int grade, manifold_gradelowerentry *element) {
    uid id=0;
    manifold_gradelowerentry *ngl=NULL;
    int imax=(grade<=MANIFOLD_GRADE_AREA ? 2 : 1); // Record both if we are a line or area element, just the first.
    
    if (manifold) {
        /* Does the manifold already contain elements of sufficient grade to add a higher one? */
        if (grade > manifold->grade+1) {
            error_raise(context, ERROR_MANIFOLD_INSUFFICIENTGRADE, ERROR_MANIFOLD_INSUFFICIENTGRADE_MSG, ERROR_FATAL);
        }
        
        /* The manifold does not yet contain elements of sufficient grade */
        if (grade > manifold->grade) {
            manifold_resizegradetable(manifold, context, grade);
        }
        
        /* Now add the element into the grade lowering table */
        ngl = manifold_newgradelowerentry(element, context);
        if (ngl) id=idtable_insert(manifold->gradelower[grade-1], ngl);
        
        /* Add this element to the grade raising elements of the two elements of lower grade */
        for (unsigned int i=0; i<imax; i++) {
            manifold_graderaiseentry *low=(manifold_graderaiseentry *) idtable_get(manifold->graderaise[grade-1], element->el[i]);
            
            if (!low) {
                /* The element doesn't have a grade raising entry, so we will try to add one */
                low=manifold_newgraderaiseentry(context);
                
                if (low) idtable_insertwithid(manifold->graderaise[grade-1], element->el[i], low);
            }
            
            if (low) {
                manifold_addidtograderaiseentry(context, low, id);
            } else {
                error_raise(context, ERROR_MANIFOLD_INVALIDELEMENTID, ERROR_MANIFOLD_INVALIDELEMENTID_MSG, ERROR_FATAL);
            }
        }
    }
    
    return id;
}

/* Remove element. NOTE: It is important when deleting elements to start with the highest grade elements and proceed down in grade. */
void manifold_deleteelement(manifold_object *manifold, interpretcontext *context, unsigned int grade, uid id) {
    manifold_gradelowerentry *e=NULL;
    manifold_graderaiseentry *f=NULL;
    int imax=(grade<MANIFOLD_GRADE_AREA ? 2 : 1); // Record both if we are a line element, just the first.
    
    if (grade<=manifold->grade && grade>0) {
        if (manifold->gradelower && manifold->gradelower[grade-1]) {
            e=idtable_get(manifold->gradelower[grade-1], id);
            
            if (e) {
                /* Delink element id from the grade below graderaise entries */
                for (unsigned int i=0; i<imax; i++) {
                    f=NULL;
                    if (manifold->graderaise[grade-1]) f=idtable_get(manifold->graderaise[grade-1], e->el[i]);
                    if (f) manifold_removeidfromgraderaiseentry(context, f, id);
                }
                
                /* Delete the grade lower entry */
                idtable_remove(manifold->gradelower[grade-1], id);
                manifold_freegradelowerentry(e);
            }
        }
        
        /* Delete the grade raise entry */
        if (grade<manifold->grade && manifold->graderaise && manifold->graderaise[grade]) {
            f=idtable_get(manifold->graderaise[grade], id);
            
            if (f) {
                if (f->list) if (linkedlist_length(f->list)) {
                    /* If the element is still referenced by a higher level element, throw a warning. */
                    sprintf(error_buffer(), ERROR_MANIFOLD_REMOVE_MSG, id, grade);
                    error_raise(context, ERROR_MANIFOLD_REMOVE, error_buffer(), ERROR_WARNING);
                }
                
                idtable_remove(manifold->graderaise[grade], id);
                manifold_freegraderaiseentry(f);
            }
        }
    }
    
}

/* Converts a vertex to a multivector */
expression *manifold_vertextomultivector(manifold_object *manifold, interpretcontext *context, uid id) {
    manifold_vertex *v=NULL;
    expression *ret=NULL;
    
    if ((manifold)&&(manifold->vertices)) {
        v=idtable_get(manifold->vertices, id);
        
        ret=multivector_doublelisttomultivector(context, manifold->dimension, MANIFOLD_GRADE_LINE, v->x, manifold->dimension);
    }
    
    return ret;
}

/* Computes an element of specified grade as a multivector */
expression *manifold_elementtomultivector(manifold_object *manifold, interpretcontext *context, int grade, uid id) {
    expression *ret=NULL;
    manifold_gradelowerentry *e=NULL;
    expression *m1=NULL, *m2=NULL;
    
    switch (grade) {
        case 0:
            ret = manifold_vertextomultivector(manifold, context, id);
            break;
        case 1:
            if ((manifold->gradelower)&&(manifold->gradelower[grade-1])) {
                
                e = idtable_get(manifold->gradelower[grade-1], id);
                
                if (e) {
                    m1=manifold_vertextomultivector(manifold, context, e->el[0]);
                    m2=manifold_vertextomultivector(manifold, context, e->el[1]);
                }
                
                if (m1&&m2) ret = opsub(context, m2, m1);
            }
            break;
        default:
            if ((manifold->gradelower)&&(manifold->gradelower[grade-1])) {
                expression_objectreference *tmp=NULL;
                e = idtable_get(manifold->gradelower[grade-1], id);
                
                if (e) {
                    m1=manifold_elementtomultivector(manifold, context, grade-1, e->el[0]);
                    m2=manifold_elementtomultivector(manifold, context, MANIFOLD_GRADE_LINE, e->el[1]);
                }
                
                /* Should return only the grade matching the grade of the object */
                if (m1&&m2) tmp = (expression_objectreference *) opmul(context, m1, m2);
                if (eval_isobjectref(tmp)) ret=multivector_projectontograde((multivectorobject *) tmp->obj, grade);
                if (tmp) freeexpression((expression *) tmp);
            }

            break;
    }
    
    if (m1) freeexpression(m1);
    if (m2) freeexpression(m2);
    
    return ret;
}

/* 
 * Grade lowering and raising
 */

/* Adds an id to a list of id's, but only if it isn't in the list already */
void manifold_idlistadd(uid list[], unsigned int *length, uid id) {
    for (unsigned int i=0; i<*length; i++) if (list[i]==id) return;
    list[*length]=id;
    *length+=1;
}

/* This function lowers the grade of an element, i.e. successively finds all elements of lower grade that belong to that element.
 Input:   manifold_object *object      - the manifold to work on
 unsigned int     grade       - the initial grade
 uid              id          - element id of the root element
 unsigned int     targetgrade - the desired grade
 Output:  uid              out[]       - A list of ids of grade targetgrade that belong to the root element. This should be at least (grade-targetgrade)*2 big.
 unsigned int     nelements   - number of elements in the list
 Return:  TRUE on success [catastrophic if not!]
 */
/* MAJOR WARNING: Do not rely on the ordering of ANYTHING but the next lowest grade. e.g. the vertices v1 v2 v3 are NOT NECESSARILY in the correct sense.
   Always build geometric elemnts through the wedge product on the grade lower definition instead. */
int manifold_gradelower(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, uid *out, unsigned int *nelements) {
    manifold_gradelowerentry *root=NULL;
    uid temp[2*(grade-targetgrade)+2];
    uid tline[2*(grade-targetgrade)+2];
    unsigned int ntemp, nline=0;
    unsigned int cg=grade-1;
    
    /* Some initial checking of arguments and verifying that the data structure has necessary elements */
    if ((manifold)&&(targetgrade<grade)&&(manifold->gradelower)&&(manifold->gradelower[grade-1])) {
        /* Find the grade lowering entry of the root object */
        root=idtable_get(manifold->gradelower[grade-1], id);
        
        if (!root) return FALSE;
        
        *nelements=0;
        /* Add the ids of the grade below the root object to the out[] array */
        manifold_idlistadd(out, nelements, root->el[0]);
        /* If we're an area or a line, add both lowering elements */
        if (grade<=MANIFOLD_GRADE_AREA) {
            manifold_idlistadd(out, nelements, root->el[1]);
        } else {
            /* Store the line element for later */
            manifold_idlistadd(tline, &nline, root->el[1]);
        }
        
        /* Loop over grades */
        if (cg>0 && manifold->gradelower[cg-1]) while (cg>targetgrade) {
            ntemp=0; /* Initially empty list of elements */
            
            for (unsigned int i=0; i<*nelements; i++) {
                /* Loop over the elements in out[], finding their attached elements */
                manifold_gradelowerentry *lower=idtable_get(manifold->gradelower[cg-1], out[i]);
                
                if (lower) {
                    manifold_idlistadd(temp, &ntemp, lower->el[0]);
                    if (cg<=MANIFOLD_GRADE_AREA) {
                        manifold_idlistadd(temp, &ntemp, lower->el[1]);
                    } else {
                        /* Store the second line element for later */
                        manifold_idlistadd(tline, &nline, lower->el[1]);
                    }
                } else {
                    return FALSE;
                }
            }
            
            /* Copy the temporary list to the output list */
            memcpy(out, temp, sizeof(uid)*ntemp);
            *nelements=ntemp;
            
            cg--; /* Go down to the next grade */
            
            if (cg==MANIFOLD_GRADE_LINE) {
                // Now add in the line elements
                for (unsigned int i=0; i<nline; i++) {
                    manifold_idlistadd(out, nelements, tline[i]);
                }
            }
        }
        
    }
    
    return TRUE;
}

/* This function raises the grade of an element, i.e. successively finds all elements of higher grade that belong to that element.
 Input:   manifold_object *object      - the manifold to work on
 unsigned int     grade       - the initial grade
 uid              id          - element id of the root element
 unsigned int     targetgrade - the desired grade
 unsigned int     maxentries  - the maximum number of entries in the output array
 Output:  uid     out[]       - A list of ids of grade targetgrade that belong to the root element.
 
 unsigned int     nelements   - number of elements in the list
 Return:  TRUE on success [catastrophic if not!]
 */
int manifold_graderaise(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, unsigned int maxentries, uid *out, unsigned int *nelements) {
    manifold_graderaiseentry *root=NULL;
    uid temp[maxentries];
    unsigned int ntemp=0;
    unsigned int cg=grade+1;
    
    /* Some initial checking of arguments and verifying that the data structure has necessary elements */
    if ((manifold)&&(targetgrade>grade)&&(manifold->graderaise)&&(manifold->graderaise[grade])) {
        /* Find the grade raising entry of the root object */
        root=idtable_get(manifold->graderaise[grade], id);
        
        *nelements=0;
        /* Add the ids of the grade above the root object to the out[] array */
        for (linkedlistentry *e=root->list->first; (e!=NULL)&&(*nelements < maxentries); e=e->next) {
            manifold_idlistadd(out, nelements, *((uid *) e->data));
        }
        
        if (*nelements==maxentries) {
            error_raise(NULL, ERROR_MANIFOLD_BUFFER, ERROR_MANIFOLD_BUFFER_MSG, ERROR_WARNING);
        }
        
        /* Loop over grades */
        while (cg<targetgrade) {
            ntemp=0; /* Initially empty list of elements */
            
            if (manifold->graderaise[cg]) for (unsigned int i=0; i<*nelements; i++) {
                /* Loop over the elements in out[], finding their attached elements */
                manifold_graderaiseentry *raise=idtable_get(manifold->graderaise[cg], out[i]);
                
                if (raise) {
                    for (linkedlistentry *e=raise->list->first; (e!=NULL)&&(ntemp < maxentries); e=e->next) {
                        manifold_idlistadd(temp, &ntemp, *((uid *) e->data));
                    }
                }
                /* Elements may not be included in the grade raising table, so it's not an error if raise is NULL */
                /*else {
                    return FALSE;
                } */
            }
            
            /* Copy the temporary list to the output list */
            if (out) memcpy(out, temp, sizeof(uid)*ntemp);
            *nelements=ntemp;
            
            cg++; /* Go up to the next grade */
        }

    }
    
    if (*nelements==maxentries) {
        error_raise(NULL, ERROR_MANIFOLD_BUFFER, ERROR_MANIFOLD_BUFFER_MSG, ERROR_WARNING);
    }
    
    return TRUE;
}

/* Checks whether all the uids in evert are found in fvert */
int manifold_gradefindtest(uid *fvert, unsigned int nfvert, uid *evert, unsigned int nevert) {
    for (unsigned int i=0; i<nevert; i++) {
        if (!manifold_idlistposition(fvert, nfvert, evert[i], NULL)) return FALSE;
    }
    return TRUE; 
}

/* Finds all elements of a particular grade that "comprise" element id, i.e. having only common vertices.
    includeconnected includes any connected elements (ie that have id as a component)  */
int manifold_gradefind(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, uid *out, unsigned int *nelements, int includeconnected) {
    if (targetgrade==MANIFOLD_GRADE_POINT) {
        return manifold_gradelower(manifold, grade, id, targetgrade, out, nelements);
    }
    
    uid vert[2*grade+2], evert[2*targetgrade+2], tid[MANIFOLD_GRADEFIND_MAXENTRIES];
    unsigned int nel, ntel, nevel;
    unsigned int cnt=0;
    
    /* First find all the vertices */
    manifold_gradelower(manifold, grade, id, MANIFOLD_GRADE_POINT, vert, &nel);

    for (unsigned int i=0; i<nel; i++) {
        /* Now raise up to the target grade */
        manifold_graderaise(manifold, MANIFOLD_GRADE_POINT, vert[i], targetgrade, MANIFOLD_GRADEFIND_MAXENTRIES, tid, &ntel);
        
        /* For every element found... */
        for (unsigned int j=0; j<ntel; j++) {
            /* ... find the vertices ... */
            manifold_gradelower(manifold, targetgrade, tid[j], MANIFOLD_GRADE_POINT, evert, &nevel);
            
            /* And check if they're all found in the original element */
            if (includeconnected ||
                ( targetgrade<=grade ?
                    manifold_gradefindtest(vert, nel, evert, nevel) /* If tg<g, all verts in evert must be in vert */
                 : manifold_gradefindtest(evert, nevel, vert, nel) /* If tg>g, all verts in vert must be in evert*/ ) ) {
                /* Make sure it's not already in the list */
                if (! manifold_idlistposition(out, cnt, tid[j], NULL)) {
                    out[cnt]=tid[j];
                    cnt++;
                }
            }
        }
    }
    
    if (nelements) *nelements=cnt;
    
    return TRUE;
}

/* Finds a grade 1 element that links v1 to v2 */
uid manifold_findlinearelement(manifold_object *manifold, uid v1, uid v2) {
    manifold_graderaiseentry *g1;
    manifold_gradelowerentry *h1;
    
    if ((manifold->graderaise)&&(manifold->graderaise[0])&&(manifold->gradelower[0])) {
        /* Get the grade raising table associated with vertex v1 */
        g1=(manifold_graderaiseentry *) idtable_get(manifold->graderaise[0], v1);
        
        if ((g1)&&(g1->list)) {
            for (linkedlistentry *e = g1->list->first; e!=NULL; e=e->next) {
                uid *r1 = e->data; /* This is the id of a higher grade element that contains v1 */
                
                /* Now look at the grade lowering table of that element */
                if (r1) h1=(manifold_gradelowerentry *) idtable_get(manifold->gradelower[0], *r1);
                
                if ( ((h1->el[0]==v1)&&(h1->el[1]==v2)) ||
                    ((h1->el[1]==v1)&&(h1->el[0]==v2)) ) {
                    return *r1; 
                }
            }
        }
    }
    
    return 0;
}

int manifold_idsortfunction(const void *v1, const void *v2) {
    uid id1 = *(uid *) v1;
    uid id2 = *(uid *) v2;
    return id1-id2;
}

#define MANIFOLD_FINDELEMENT_MAXCONNECTIONS 200

/* Finds an element given a specified number of vertices */
uid manifold_findelementfromvertices(manifold_object *mobj, int nv, uid *v) {
    unsigned int targetgrade = nv-1, nel=0, nel2=0;
    uid id[MANIFOLD_FINDELEMENT_MAXCONNECTIONS];
    uid vs[nv], vout[nv];
    
    /* Copy and sort the list of provided vertices */
    memcpy(vs, v, sizeof(uid)*nv);
    qsort(vs, nv, sizeof(uid), manifold_idsortfunction);
    
    if (manifold_graderaise(mobj, MANIFOLD_GRADE_POINT, v[0], targetgrade, MANIFOLD_FINDELEMENT_MAXCONNECTIONS, id, &nel)) {
        for (unsigned int i=0; i<nel; i++) {
            if (manifold_gradelower(mobj, targetgrade, id[i], MANIFOLD_GRADE_POINT, vout, &nel2)) {
                if (nel2==nv) {
                    qsort(vout, nv, sizeof(uid), manifold_idsortfunction);
                    if (memcmp(vs, vout, sizeof(uid)*nv)==0) {
                        return id[i];
                    }
                }
            }
        }
        
    }
    
    return 0;
}

/*
 * Utility functions
 */

/* Tests if a list contains only values */
int manifold_isvalididlist(manifold_object *obj, expression_list *list) {
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        expression *el=(expression *) e->data;
        
        if (!eval_isinteger(el)) return FALSE; // [TODO SHould validate against the manifold]
    }
    
    return TRUE;
}

/* Resize the grade tables */
typedef struct {
    idtable *t;
    interpretcontext *context;
} manifold_makevertexgraderaisefunction_ref;

/* This function is mapped over an existing grade's idtable to create a blank grade raising table */
void manifold_makevertexgraderaisefunction (uid id, void *data, void* ref) {
    manifold_makevertexgraderaisefunction_ref *r = (manifold_makevertexgraderaisefunction_ref *) ref;
    manifold_graderaiseentry *new=NULL;
    
    if (r) {
        new=manifold_newgraderaiseentry(r->context);
        
        if (new) {
            idtable_insertwithid(r->t, id, new);
        }
    }
}

/* Resizes the grade table */
void manifold_resizegradetable(manifold_object *manifold, interpretcontext *context, unsigned int maxgrade) {
    /* TODO: Implement ERROR_FREE correctly to avoid memory leaks in partial allocation successes. */
    unsigned int nv=0;
    
    if ((manifold) && (maxgrade>manifold->grade)) {
        if (manifold->vertices) nv=idtable_count(manifold->vertices);
        
        /* Deal with the grade lowering table first */
        if (manifold->gradelower) {
            manifold->gradelower=EVAL_REALLOC(manifold->gradelower, sizeof(idtable*)*maxgrade); /* We already have a grade table so we need to realloc it */
        } else {
            manifold->gradelower=EVAL_MALLOC(sizeof(idtable*)*maxgrade); /* We need to malloc a new grade table */
        }
        
        if (manifold->gradelower) {
            /* Now we need to allocate new idtables */
            for (unsigned int i=manifold->grade; i<maxgrade; i++) {
                manifold->gradelower[i]=idtable_create(nv+1); /* We suppose that there are a similar number of elements at each grade */
                
                if (!manifold->gradelower[i]) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
            }
        } else error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
        
        /* Now for the grade raising table */
        if (manifold->graderaise) {
            manifold->graderaise=EVAL_REALLOC(manifold->graderaise, sizeof(idtable*)*(maxgrade)); /* We already have a grade table so we need to realloc it */
        } else {
            manifold->graderaise=EVAL_MALLOC(sizeof(idtable*)*maxgrade); /* We need to malloc a new grade table */
        }
        
        if (manifold->graderaise) {
            /* Now we need to allocate new idtables */
            for (unsigned int i=manifold->grade; (i<maxgrade)&&(i<manifold->dimension); i++) {
                manifold->graderaise[i]=idtable_create(nv+1); /* We suppose that there are a similar number of elements at each grade */
                
                if (!manifold->graderaise[i]) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
            }
            
            /* We now have to create raising information for highest grade already present. */
            manifold_makevertexgraderaisefunction_ref ref; /* Put parameters in the reference structure */
            ref.context=context;
            ref.t=manifold->graderaise[manifold->grade];
            
            if (manifold->grade==0) {
                /* i.e. Vertices are present. */
                idtable_map(manifold->vertices, manifold_makevertexgraderaisefunction, (void *) &ref);
            } else {
                /* Any higher grade element */
                idtable_map(manifold->gradelower[manifold->grade-1], manifold_makevertexgraderaisefunction, (void *) &ref);
            }
        } else error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
        
        manifold->grade=maxgrade;
    }
}

/* Turn a list of integers into an array of uids */
int manifold_listtoidarray(expression_list *l, uid *id, unsigned int maxlength) {
    unsigned int i=0;
    if (l) {
        for (linkedlistentry *e=l->list->first; (e!=NULL) && (i<maxlength); e=e->next) {
            expression *val = (expression *) e->data;
            
            if (eval_isinteger(val)) {
                id[i] = eval_integervalue(val);
            } else {
                return FALSE;
            }
            
            i++;
        }
    }
    
    return TRUE;
}

/*
 * Determine whether an element is on a boundary
 */

int manifold_isboundary(manifold_object *manifold, unsigned int grade, uid id) {
    int result=FALSE;
    unsigned int nf=0;
    uid out[MANIFOLD_GRADEFIND_MAXENTRIES];
    
    if (manifold_gradefind(manifold, grade, id, grade+1, out, &nf, FALSE)) {
        if (nf<2) result=TRUE;
    }
    
    return result;
}

/*
 * Manifold refinement
 */

typedef struct {
    manifold_object *old;
    manifold_object *new;
    selection_object *sel;
    interpretcontext *context;
    idtable **refine;
} manifold_refinemapfunctionref;


void manifold_refineaddidmap(idtable *t, uid id, uid *array, unsigned int length) {
    unsigned int *old = idtable_get(t, id);
    uid *narray = EVAL_MALLOC(sizeof(uid)*length);
    
    if (narray) {
        memcpy(narray, array, sizeof(uid)*length);
        if (old) EVAL_FREE(old);
        idtable_insertwithid(t, id, (void *) narray);
    }
}

/* Vertices -- all we do is copy them */
void manifold_refinevertexmapfunction(uid id, void *data, void *r) {
    manifold_vertex *v = (manifold_vertex *) data;
    manifold_refinemapfunctionref *ref = (manifold_refinemapfunctionref *)r;
    uid nv;
    
    nv=manifold_addvertex(ref->new, ref->context, ref->old->dimension, v->x, v->flags);
    
    manifold_refineaddidmap(ref->refine[0], id, &nv, 1);
}

/* Edges -- Add an extra vertex */
void manifold_refineedgemapfunction(uid id, void *data, void *r) {
    manifold_gradelowerentry *gl = (manifold_gradelowerentry *) data;
    manifold_refinemapfunctionref *ref = (manifold_refinemapfunctionref *)r;
    manifold_gradelowerentry ngl;
    unsigned int flags = MANIFOLD_VERTEX_NONE;
    uid nv, *ov, nid[2], nedge[2];
    
    if (ref) {
        MFLD_DBL x[ref->old->dimension];
        
        /* Find the new vertex ids */
        for (unsigned int i=0; i<2; i++) {
            ov = idtable_get(ref->refine[0], gl->el[i]);
            if (ov) nid[i]=*ov;
            else return;
        }

        /* If we have a selection and this edge isn't in it, just insert a copy of the old element */
        if ((ref->sel)&&(!selection_isselected(ref->sel, ref->old, MANIFOLD_GRADE_LINE, id))) {
            ngl.el[0]=nid[0]; ngl.el[1]=nid[1];
            nedge[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);
            nedge[1]=0;
            manifold_refineaddidmap(ref->refine[1], id, nedge, 2);
            
        } else {
            /* Refine by splitting the vertex */
            manifold_vertexmidpoint(ref->old, gl->el, 2, x, &flags);
            nv = manifold_addvertex(ref->new, ref->context, ref->old->dimension, x, flags);
            
            ngl.el[0]=nid[0]; ngl.el[1]=nv;
            nedge[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);
            
            ngl.el[0]=nv; ngl.el[1]=nid[1];
            nedge[1]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);
            
            /* Now add a map from the old edge to the new ones */
            manifold_refineaddidmap(ref->refine[1], id, nedge, 2);
        }
    }
}

/* Finds the position that an id occupies in a list and returns it in *posn. Returns true of false depending on whether the element was present  */
int manifold_idlistposition(uid list[], unsigned int length, uid id, unsigned int *posn) {
    for (unsigned int i=0; i<length; i++) {
        if (list[i]==id) {
            if (posn) *posn=i;
            return TRUE;
        }
    }
    return FALSE;
}

void manifold_identifyedge(manifold_object *mobj, uid edge, uid *v, uid *s, int *sgn) {
    uid vs[2], vv[4]={v[0], v[1], v[2], v[0]};
    unsigned int nl;
    
    manifold_gradelower(mobj, MANIFOLD_GRADE_LINE, edge, MANIFOLD_GRADE_POINT, vs, &nl);
    for (unsigned int i=0; i<3; i++) {
        if ((vs[0]==vv[i])&&(vs[1]==vv[i+1])) {
            s[i]=edge; sgn[i]=1; break;
        } else if ((vs[0]==vv[i+1])&&(vs[1]==vv[i])) {
            s[i]=edge; sgn[i]=-1; break;
        }
    }
}

void manifold_checkfaceorientation(manifold_object *mobj, interpretcontext *context, expression *oldamv, uid i)  {
    expression_objectreference *fmvmul=NULL, *fmvnew=NULL;
    manifold_gradelowerentry *f=NULL;
    
    double sc;
    uid swp;
    
    f=(manifold_gradelowerentry *) idtable_get(mobj->gradelower[MANIFOLD_GRADE_AREA-1], i);
    if (f) {
        fmvnew=(expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, i);
        if (fmvnew) fmvmul = (expression_objectreference *) opmul(context, oldamv, (expression *) fmvnew);
        multivector_gradetodoublelist((multivectorobject *) fmvmul->obj, MANIFOLD_GRADE_POINT, &sc);
        if (sc>0.0) { // sc<0 means pointing in same direction...
            /* Reverse the order of the definition */
            swp=f->el[0];
            f->el[0]=f->el[1];
            f->el[1]=swp;
        }
        if (fmvnew) freeexpression((expression *) fmvnew);
        if (fmvmul) freeexpression((expression *) fmvmul);
    }
}

/* Determines the edges that bound an area element. */
void manifold_areaboundaryedges(manifold_object *mobj, manifold_gradelowerentry *gl, uid *v, uid *s, int *sgn) {
    unsigned int p=0;
    
    for (unsigned int i=0; i<3; i++) { s[i]=UID_NONE; sgn[i]=1; }
    
    manifold_identifyedge(mobj, gl->el[0], v, s, sgn);
    manifold_identifyedge(mobj, gl->el[1], v, s, sgn);
    
    if (manifold_idlistposition(s, 3, UID_NONE, &p)) {
        uid edge = manifold_findlinearelement(mobj, v[p], (p==2? v[0]: v[p+1]));
        if (edge!=UID_NONE) manifold_identifyedge(mobj, edge, v, s, sgn);
    }
}

/* Areas */
void manifold_refineareamapfunction(uid id, void *data, void *r) {
    manifold_gradelowerentry *gl = (manifold_gradelowerentry *) data;
    manifold_refinemapfunctionref *ref = (manifold_refinemapfunctionref *)r;
    uid v[(MANIFOLD_GRADE_AREA-MANIFOLD_GRADE_POINT)*2]; /* Vertex ids of the original vertices in the old manifold */
    uid s[3]; /* The boundary edges in the old manifold */
    uid rv[3]; /* Vertex ids of the original vertices in the new manifold */
    uid xv[3]; /* Vertex ids of the refined vertices in the new manifold */
    uid nedge[3]; /* Edge ids of the new edges created by refinement */
    uid erefine[6]; /* Edge ids of all the edges in the new manifold, ordered so they have the cyclic property */
    uid narea[4]; /* New area elements */
    unsigned int nv=0, nrefine=0;
    int sgn[3]; /* The sign of the old edges */
    int refine[3]; /* Whether to refine or not. */
    unsigned int rp=0, crp=0;
    int areasign=1;
    expression *mv=NULL;
    
    for (unsigned int i=0; i<3; i++) { xv[i]=UID_NONE; nedge[i]=UID_NONE; refine[i]=FALSE; }
    manifold_gradelower(ref->old, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, v, &nv);
    for (unsigned int i=0; i<nv; i++) rv[i]=* (int *) idtable_get(ref->refine[0], v[i]);
    
    if (nv==3) {
        /* Find the edges that bound the triangle in the old manifold */
        manifold_areaboundaryedges(ref->old, gl, v, s, sgn);
        
        /* Get the element in multivector form */
        mv=manifold_elementtomultivector(ref->old, ref->context, MANIFOLD_GRADE_AREA, id);
        
        /* Identify which edges have been refined */
        for (unsigned int i=0; i<3; i++) {
            uid *edge=idtable_get(ref->refine[1], s[i]);
            
            if (edge) {
                erefine[2*i]=edge[0]; erefine[2*i+1]=edge[1];
                
                if (edge[1]!=UID_NONE) {
                    refine[i]=TRUE; nrefine++;
                    manifold_gradelowerentry *sgl=idtable_get(ref->new->gradelower[0], edge[0]);
                    xv[i]=sgl->el[1]; /* The new vertex */
                    
                    if (sgn[i]<0) { erefine[2*i]=edge[1]; erefine[2*i+1]=edge[0]; }
                }
            }
        }
        
        /* Determine the overall sign of the old area alement */
        for (unsigned int i=0; i<3; i++) {
            if (s[i]==gl->el[0]) areasign*=sgn[i];
            if (s[i]==gl->el[1]) areasign*=sgn[i];
        }
        
        /* Now build the replacement linear elements and areas */
        manifold_gradelowerentry ngl;
        for (unsigned int i=0; i<4; i++) narea[i]=UID_NONE;
       
        switch (nrefine) {
            case 0:
                /* Copy the existing element, changing the line references using the idmap */
                if (ref->refine[1]) {
                    /* Loop up the old edges in the refinement map */
                    uid *edge1 = idtable_get(ref->refine[1], gl->el[0]);
                    uid *edge2 = idtable_get(ref->refine[1], gl->el[1]);
                    
                    if (edge1 && edge2) {
                        ngl.el[0]=edge1[0]; ngl.el[1]=edge2[0]; /* These edges haven't been refined, so the map entry is found in the first element. */
                        narea[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                    }
                }
                break;
            case 1:
                /* which edge to refine? */
                for (rp=0; rp<3; rp++) if (refine[rp]) break;
                
                crp=(rp+2) % 3;
                
                /* Add in the new line */
                ngl.el[0]=xv[rp]; /* The new vertex */
                ngl.el[1]=rv[crp]; /* The complementary vertex */
                nedge[rp]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);
                
                /* Add in the areas */
                unsigned int sl=(rp-1) % 3, sr=(rp+1) % 3; /* The edges to the left and right */
                ngl.el[0]=erefine[2*sl+0]; ngl.el[1]=nedge[rp];
                narea[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[0]);
                
                ngl.el[0]=nedge[rp]; ngl.el[1]=erefine[2*sr+0];
                //    ngl.el[0]=erefine[2*sr+0]; ngl.el[1]=nedge[rp];
                narea[1]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[1]);
                
                break;
            case 2:
                /* Locate the edges to refine. rp should point to the first edge, such that (rp+1) % 3 is also to be refined */
                for (rp=0; rp<3; rp++) if (refine[rp] && refine[(rp+1) % 3]) break;
                
                crp=(rp+2) % 3;
                
                /* Add in the new lines */
                ngl.el[0]=xv[rp]; /* The first new vertex */
                ngl.el[1]=xv[(rp+1) % 3]; /* The second new vertex */
                nedge[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);

                ngl.el[0]=xv[rp]; /* The new vertex */
                ngl.el[1]=rv[crp]; /* The complementary vertex of the first edge */
                nedge[1]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);

                /* Add in the areas */
                ngl.el[0]=erefine[2*rp+1]; /* i.e. the second new edge of the first edge */
                ngl.el[1]=erefine[(2*(rp+1)+0) % 6]; /* the first edge of the second. Remembering the periodic indexing. */
                narea[0]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[0]);
                
                ngl.el[0]=nedge[0]; /* i.e. the first new edge */
                ngl.el[1]=erefine[(2*(rp+1)+1) % 6]; /* the second edge of the second. Remembering the periodic indexing. */
                narea[1]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[1]);
                
                ngl.el[0]=nedge[1]; /* i.e. the second new edge */
                ngl.el[1]=erefine[(2*(rp+2)+0) % 6]; /* the unrefined edge two steps ahead. Remembering the periodic indexing. */
                narea[2]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[2]);
                
                break;
            case 3:
                /* Add in the lines */
                for (unsigned int i=0; i<3; i++) {
                    ngl.el[0]=xv[i]; ngl.el[1]=xv[(i == 2 ? 0: i+1)];
                    nedge[i]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_LINE, &ngl);
                }
                
                /* Add in the areas */
                for (unsigned int i=0; i<3; i++) {
                    ngl.el[0]=erefine[2*i+1]; ngl.el[1]=erefine[(i == 2 ? 0: 2*i+2)];
                    narea[i]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                    manifold_checkfaceorientation(ref->new, ref->context, mv, narea[i]);
                }
                
                ngl.el[0]=nedge[0]; ngl.el[1]=nedge[1];
                narea[3]=manifold_addelement(ref->new, ref->context, MANIFOLD_GRADE_AREA, &ngl);
                manifold_checkfaceorientation(ref->new, ref->context, mv, narea[3]);
                
                break;
        }
        
        /* Now add a map from the old edge to the new ones */
        manifold_refineaddidmap(ref->refine[2], id, narea, 4);
    }
    
    if (mv) freeexpression(mv);
}

/* Construct */
typedef struct {
    unsigned int grade;
    field_object *target;
} manifold_refinedmapfunctionref;

void manifold_refinedmapfunction(uid id, void *data, void *r) {
    manifold_refinedmapfunctionref *ref = (manifold_refinedmapfunctionref *) r;
    uid *narray = (uid *) data;
    unsigned int nel = 1 << ref->grade;
    
    expression_list *list = newexpressionlist(NULL) ;
    
    if (list && list->list) {
        for (unsigned int i=0; i<nel; i++) {
            if (narray[i]!=UID_NONE) linkedlist_addentry(list->list, newexpressioninteger(narray[i]));
        }
        field_insertwithid(ref->target, id, (expression *) list);
    }
}

/* Refines a manifold */
expression *manifold_refine(manifold_object *m, interpretcontext *context, selection_object *sel) {
    if (!m || !m->vertices) return NULL;
    
    expression_objectreference *mnew = (expression_objectreference *) manifold_new();
    manifold_object *new=NULL;
    idtable *refine[m->grade+1];
    expression_list *refinelist;
    for (unsigned int i=0; i<=m->grade; i++) refine[i]=idtable_create(m->vertices->nentries);
    
    if (mnew) new = (manifold_object *) mnew->obj;
    
    if (new) {
        /* Pass this structure to all map functions */
        manifold_refinemapfunctionref ref;
        ref.new=new; ref.context=context; ref.sel=sel; ref.old=m; ref.refine=refine;
        
        /* Copy across the vertices */
        idtable_map(m->vertices, manifold_refinevertexmapfunction, &ref);
        
        if (m->gradelower) {
            if (m->gradelower[0]) idtable_map(m->gradelower[0], manifold_refineedgemapfunction, &ref);
            
            if (m->gradelower[1]) idtable_map(m->gradelower[1], manifold_refineareamapfunction, &ref);
        }
        
    }
    
    /* Make an list of fields pointing to the updated ids */
    refinelist=newexpressionlist(NULL);
    if (refinelist) {
        expression *fref=NULL;
        
        for (unsigned int i=0; i<=m->grade; i++) {
            manifold_refinedmapfunctionref rref;
            rref.grade=i;
            fref=field_new(m, i);
            if (eval_isobjectref(fref)) {
                rref.target=(field_object *) eval_objectfromref(fref);
                linkedlist_addentry(refinelist->list, fref);
            }
            if (refine[i] && rref.target) {
                idtable_map(refine[i], manifold_refinedmapfunction, &rref);
            }
        }
    }
    
    for (unsigned int i=0; i<=m->grade; i++) {
        if (refine[i]) {
            idtable_map(refine[i], idtable_genericfreemapfunction, NULL);
            idtable_free(refine[i]);
        }
    }
    
    /* Free old versions and across the new data structures */
    manifold_free(m, context);
    m->vertices = new->vertices; new->vertices=NULL;
    m->gradelower = new->gradelower; new->gradelower=NULL;
    m->graderaise = new->graderaise; new->graderaise=NULL;
    freeexpression((expression *) mnew);
    
    return (expression *) refinelist;
}

/*
 * Edge flip
 */

void manifold_edgetofaces(manifold_object *mobj, uid edge, uid *faces, uid *cvert, unsigned int *nfaces) {
    uid v[2];
    uid vf[MANIFOLD_EDGETOFACE_MAXFACES];
    uid vv[4];
    unsigned int nv, nf, nvv;
    
    if (nfaces) *nfaces=0;
    
    /* Find the vertices attached to the edge */
    manifold_gradelower(mobj, MANIFOLD_GRADE_LINE, edge, MANIFOLD_GRADE_POINT, v, &nv);
    
    /* Loop over all the faces attached to each vertex */
    for (unsigned int i=0; i<2; i++) {
        /* Get all the attached faces */
        if (manifold_graderaise(mobj, MANIFOLD_GRADE_POINT, v[i], MANIFOLD_GRADE_AREA, MANIFOLD_EDGETOFACE_MAXFACES, vf, &nf)) {
        
            /* Get all the attached faces */
            for (unsigned int j=0; j<nf; j++) {
                manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, vf[j], MANIFOLD_GRADE_POINT, vv, &nvv);
                
                /* If both vertices are present, it's one of the faces we're interested in */
                if (manifold_idlistposition(vv, nvv, v[0], NULL) && manifold_idlistposition(vv, nvv, v[1], NULL)) {
                    /* Check we didn't already find this face */
                    if ((nfaces)&&(*nfaces==0 || faces[0]!=vf[j])) {
                        faces[*nfaces]=vf[j];
                        
                        /* Identify the associated complementary vertex */
                        if (cvert) {
                            for (unsigned int k=0; k<nvv; k++) {
                                if (vv[k]!=v[0] && vv[k]!=v[1]) cvert[*nfaces]=vv[k];
                            }
                        }
                        
                        (*nfaces)++;
                        if (*nfaces>1) { return; }
                    }
                }
            }
        }
    }
}

int manifold_equiangulatetest(manifold_object *mobj, manifold_gradelowerentry *edge, uid *cvert) {
    /* This test for whether to flip an edge is described in the Surface Evolver manual Section 16.12 Equiangulation
       (the variables are named to coincide with this) */
    MFLD_DBL a,b,c,d,e;
    
    /* Length of the common edge */
    a = manifold_vertexseparation(mobj, edge->el[0], edge->el[1]);
    
    /* Other edges of the one face */
    b = manifold_vertexseparation(mobj, edge->el[0], cvert[0]);
    c = manifold_vertexseparation(mobj, edge->el[1], cvert[0]);
    
    /* ... and the other face */
    d = manifold_vertexseparation(mobj, edge->el[0], cvert[1]);
    e = manifold_vertexseparation(mobj, edge->el[1], cvert[1]);
    
    return (((b*b + c*c - a*a)*d*e + (d*d + e*e - a*a)*b*c)< 0 ? TRUE : FALSE);
}

void manifold_edgeflip(manifold_object *mobj, interpretcontext *context, uid edge, manifold_gradelowerentry *e, uid *faces, uid *cvert, unsigned int nfaces) {
    manifold_gradelowerentry *f[2]={NULL, NULL};
    expression_objectreference *fmvold[2]={NULL, NULL}, *fmvnew[2]={NULL, NULL}, *fmvmul[2]={NULL,NULL};
    uid cedge[2];
    
    /* old face i has id faces[i] and vertices e[0], e[1] and cvert[i] */
    /* new face i has id faces[i] and vertices cvert[0], cvert[1] and e[i] */
    
    /* Loop over the faces and compute a multivector for each */
    for (unsigned int i=0; i<2; i++) {
        /* Find the gradelowerentry for the face */
        f[i] = idtable_get(mobj->gradelower[MANIFOLD_GRADE_AREA-1], faces[i]);
        
        /* Find the old face as a multivector */
        fmvold[i]=(expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, faces[i]);
        //multivector_print((multivectorobject *) fmvold[i]->obj);
        
        /* The new face will be defined by (cvert[0] -> cvert[1]) wedge (cvert[0] -> e->el[i])
         The edge (cvert[0] -> e->el[i]) is called the complementary edge-find it's id first */
        cedge[i]=manifold_findlinearelement(mobj, cvert[0], e->el[i]);
    }
    
    /* Correct vertex grade raise entries */
    for (unsigned int i=0; i<2; i++) {
        /* Delink the old vertices from the old edge */
        manifold_lookupandremoveidfromgraderaiseentry(mobj, context, e->el[i], MANIFOLD_GRADE_POINT, edge);
        
        /* Add the edge to the new vertices */
        manifold_lookupandaddidtograderaiseentry(mobj, context, cvert[i], MANIFOLD_GRADE_POINT, edge);
    }
    
    /* The complementary vertices now become the edge */
    for (unsigned int i=0; i<2; i++) {
        e->el[i]=cvert[i];
    }
    
    /* Loop over the faces, fix the faces and compute the new multivector for each */
    for (unsigned int i=0; i<2; i++) {
        double sc;
        /* Delink the old edges from the old face */
        manifold_lookupandremoveidfromgraderaiseentry(mobj, context, f[i]->el[0], MANIFOLD_GRADE_LINE, faces[i]);
        manifold_lookupandremoveidfromgraderaiseentry(mobj, context, f[i]->el[1], MANIFOLD_GRADE_LINE, faces[i]);
        
        /* Redefine the face */
        f[i]->el[0]=edge;
        f[i]->el[1]=cedge[i];
        
        /* Link the new edges to the new face */
        manifold_lookupandaddidtograderaiseentry(mobj, context, f[i]->el[0], MANIFOLD_GRADE_LINE, faces[i]);
        manifold_lookupandaddidtograderaiseentry(mobj, context, f[i]->el[1], MANIFOLD_GRADE_LINE, faces[i]);
        
        /* Find the new face as a multivector */
        fmvnew[i]=(expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, faces[i]);
        
        /* Ensure they have the same orientation */
        fmvmul[i] = (expression_objectreference *) opmul(context, (expression *) fmvold[i], (expression *) fmvnew[i]);
        multivector_gradetodoublelist((multivectorobject *) fmvmul[i]->obj, MANIFOLD_GRADE_POINT, &sc);
        if (sc>0.0) { // sc<0 means they lie in the same direction
            /* Reverse the order of the definition if in opposite directions */
            f[i]->el[0]=cedge[i];
            f[i]->el[1]=edge;
        }
    }
    
    /* Free temporary multivectors */
    for (unsigned int i=0; i<2; i++) {
        if (fmvold[i]) freeexpression((expression *) fmvold[i]);
        if (fmvnew[i]) freeexpression((expression *) fmvnew[i]);
        if (fmvmul[i]) freeexpression((expression *) fmvmul[i]);
    }
}

int manifold_tryedgeflip(manifold_object *mobj, interpretcontext *context, uid edge) {
    manifold_gradelowerentry *e=NULL;
    uid faces[2], cvert[2];
    unsigned int nf;
    int ret=FALSE;
    
    /* Find the edge */
    e = idtable_get(mobj->gradelower[MANIFOLD_GRADE_LINE-1], edge);
    
    if (e) {
        /* Now find the adjacent faces and complementary vertices */
        manifold_edgetofaces(mobj, edge, faces, cvert, &nf);
        
        if (nf==2) {
            /* Should we flip this edge? */
            int q = manifold_equiangulatetest(mobj, e, cvert);
            
            if (q) {
                /* Go ahead and flip the edge */
                manifold_edgeflip(mobj, context, edge, e, faces, cvert, nf);
                
                ret=TRUE;
                
               // class_callselector(context, (object *) mobj, MANIFOLD_CHECK, 0);
            }
        }
        
    }
    
    return ret;
}

typedef struct {
    manifold_object *mobj;
    interpretcontext *context;
    int nflipped;
} manifold_equiangulatereference;

void manifold_equiangulatemapfunction(uid id, void *e, void *r ) {
    manifold_equiangulatereference *ref = (manifold_equiangulatereference *) r;
    
    ref->nflipped+=manifold_tryedgeflip(ref->mobj, ref->context, id);
}

expression *manifold_equiangulate(manifold_object *mobj, interpretcontext *context) {
    manifold_equiangulatereference ref;
    
    ref.mobj=mobj;
    ref.context=context;
    ref.nflipped=0;
    
    if ((mobj->grade>0)&&(mobj->gradelower)) {
        if (mobj->gradelower[0]) {
            idtable_map(mobj->gradelower[0], manifold_equiangulatemapfunction, &ref);
        }
    }
    
    return newexpressioninteger(ref.nflipped);
}

/*
 * Derefinement
 */

/* Adds all the ids in src to target */
void manifold_appendgraderaiseentry(interpretcontext *context, manifold_graderaiseentry *target, manifold_graderaiseentry *src) {
    if (src && target) {
        for (linkedlistentry *e=src->list->first; e!=NULL; e=e->next) {
            manifold_addidtograderaiseentry(context, target, * (uid *) e->data);
        }
    }
}

/* Looks up all the entries in the graderaiseentry src, finds the corresponding gradelowerentries in the next grade up and replaces occurences of uid old with new.
   Then frees all entries in src.
    grade should be the grade of the source, not the target. */
void manifold_graderaiseentrysubstitute(manifold_object *mobj, interpretcontext *context, manifold_graderaiseentry *src, unsigned int grade, uid old, uid new) {
    manifold_gradelowerentry *gl;
    
    if (mobj->gradelower && mobj->gradelower[grade] && src && src->list) {
        for (linkedlistentry *e=src->list->first; e!=NULL; e=e->next) {
            uid id = * (uid *) e->data;
            /* Find the corresponding grade lower entry one grade up */
            gl=idtable_get(mobj->gradelower[grade], id); /* Note strange indexing! this effectively looks ONE UP from the current grade */
            
            if (gl) {
                /* Make the substitution */
                for (unsigned int i=0; i<2; i++) if (gl->el[i]==old) gl->el[i]=new;
            }
        }
        
        /* Now free all the elements (because the higher elements no longer refer to them) */
        linkedlist_map(src->list, EVAL_FREE);
        linkedlist_free(src->list);
        src->list=NULL;
    }
}

/* Merges the vertices joined by edge into one vertex, removing redundant elements */
/* Here the edge is AB, adjacent to faces P and Q
 *                 A
 *             T / | \ R
 *              /  |  \
 *   cvert[0] C  P | Q D cvert[1]
 *              \  |  /
 *             U \ | / S
 *                 B
 */

void manifold_pruneedge(manifold_object *mobj, interpretcontext *context, uid edge) {
    manifold_gradelowerentry *e=NULL;
    manifold_graderaiseentry *vre[2];
    uid faces[2], cvert[2];
    uid vert[2], medge[2];
    unsigned int nf;
    MFLD_DBL xx[mobj->dimension];
    
    /* Find the edge */
    e = idtable_get(mobj->gradelower[MANIFOLD_GRADE_LINE-1], edge);
    
    if (e) {
        for (unsigned int i=0; i<2; i++) vert[i]=e->el[i];
        
        /* Now find the adjacent faces and complementary vertices */
        manifold_edgetofaces(mobj, edge, faces, cvert, &nf);
        
        /* Delete the adjacent faces */
        for (unsigned int i=0; i<nf; i++) {
            manifold_deleteelement(mobj, context, MANIFOLD_GRADE_AREA, faces[i]);
        }
        
        /* Delete the edge */
        manifold_deleteelement(mobj, context, MANIFOLD_GRADE_LINE, edge);
        
        /* Merge edges R,S and T,U */
        for (unsigned int i=0; i<nf; i++) {
            /* Identify edges R and S from the vertex ids and find their associated grade raise entries */
            for (unsigned int j=0; j<2; j++) {
                medge[j]=manifold_findlinearelement(mobj, vert[j], cvert[i]);
                vre[j]=idtable_get(mobj->graderaise[MANIFOLD_GRADE_LINE], medge[j]);
            }
            
            if (vre[0] && vre[1]) {
                /* Copy the grade raise entry elements from S into R */
                manifold_appendgraderaiseentry(context,vre[0], vre[1]);
                /* Make higher order elements that refer to S instead point to R */
                manifold_graderaiseentrysubstitute(mobj, context, vre[1], MANIFOLD_GRADE_LINE, medge[1], medge[0]);
            }
            
            /* Delete edge S */
            manifold_deleteelement(mobj, context, MANIFOLD_GRADE_LINE, medge[1]);
        }
        
        /* Now have to reconnect all edges attached to vertex B to vertex A. */
        if (mobj->graderaise && mobj->graderaise[MANIFOLD_GRADE_POINT]) {
            for (unsigned int i=0; i<2; i++) vre[i]=idtable_get(mobj->graderaise[MANIFOLD_GRADE_POINT], vert[i]);
            
            if (vre[0] && vre[1]) {
                /* Copy the grade raise entry elements from B into A */
                manifold_appendgraderaiseentry(context, vre[0], vre[1]);
                manifold_graderaiseentrysubstitute(mobj, context, vre[1], MANIFOLD_GRADE_POINT, vert[1], vert[0]);
            }
        }
        
        /* Move the remaining vertex to the middle of the edge */
        manifold_vertexmidpoint(mobj, vert, 2, xx, NULL);
        manifold_setvertexposition(mobj, context, vert[0], xx);
        
        /* And now delete the second vertex in the edge. */
        manifold_deletevertex(mobj, context, vert[1]);
    }
}

/*
 * Manifold Orientation
 */

void manifold_vectoradd3d(double v1[3],double v2[3],double *v3) {
    v3[0] = v1[0] + v2[0];
    v3[1] = v1[1] + v2[1];
    v3[2] = v1[2] + v2[2];
    return;
}

void manifold_vectorsubtract3d(double v1[3],double v2[3],double *v3) {
    v3[0] = v1[0] - v2[0];
    v3[1] = v1[1] - v2[1];
    v3[2] = v1[2] - v2[2];
    return;
}

void manifold_crossproduct3d(double v1[3],double v2[3],double *v3) {
    v3[0] = v1[1]*v2[2] - v1[2]*v2[1];
    v3[1] = v1[2]*v2[0] - v1[0]*v2[2];
    v3[2] = v1[0]*v2[1] - v1[1]*v2[0];
    return;
}

double manifold_dotproduct3d(double v1[3],double v2[3]) {
    return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

double manifold_norm3d(double v1[3]) {
    return sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
}

void manifold_vectorscale3d(double v1[3],double scale,double v2[3]) {
    v2[0] = v1[0]*scale;
    v2[1] = v1[1]*scale;
    v2[2] = v1[2]*scale;
    return;
}

void manifold_normalizevector3d(double *src, double *dest) {
    double norm=0.0;
    for (unsigned int i=0;i<3;i++) { dest[i]=src[i]; norm+=dest[i]*dest[i]; }
    norm=sqrt(norm);
    if (fabs(norm)>MACHINE_EPSILON) for (unsigned int i=0;i<3;i++) dest[i]/=norm;
}

#define MANIFOLD_ORIENTMAXINTERSECTIONS 20

typedef struct {
    MFLD_DBL alpha;
    uid id;
} manifold_orientfacesintersection;

typedef struct {
    manifold_object *mobj;
    interpretcontext *context;
    
    uid testid;
    MFLD_DBL *V0;
    MFLD_DBL *nn;
    
    unsigned int nintersections;
    manifold_orientfacesintersection *intersectionlist;
} manifold_orientfacesintersectiontestref;

// Line-triangle test from http://geomalgorithms.com/a06-_intersect-2.html
void manifold_orientfaceintersectiontest (uid id, void *data, void *r) {
    manifold_orientfacesintersectiontestref *iref = (manifold_orientfacesintersectiontestref *) r;
    manifold_object *mobj=NULL;
    interpretcontext *context=NULL;
    
    uid vert[3];    // Vertex ids of this triangle
    MFLD_DBL v0[3], // Position of the first vertex
             u[3],  // Vector from v0->v1 on this triangle
             v[3],  // Vector from v0->v2 on this triangle
             p[3], // Vector from v0 in this triangle to the midpoint of the other triangle
             pi[3], // The intersection point, if it exists, of the ray with the plane of this triangle
             m[3], // Normal of this triangle
             w[3]; // pi - v0;
    MFLD_DBL a,b,alpha,s,t;
    MFLD_DBL uu,uv,vv,uw,vw,denom;
    unsigned int nel;
    
    if (!iref) return;
    mobj=iref->mobj; context=iref->context;
    
    /* Don't look at the triangles self-intersection */
    if (id!=iref->testid) {
        // Get the vertices
        manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vert, &nel);
        
        manifold_getvertexposition(mobj, iref->context, vert[0], v0); // The position of the first vertex
        
        manifold_vertexdisplacement(mobj, vert[0], vert[1], u); // Edge 1
        manifold_vertexdisplacement(mobj, vert[0], vert[2], v); // Edge 2
        manifold_crossproduct3d(u, v, m); // Normal
    
        manifold_vectorsubtract3d(v0, iref->V0, p); // p = V0 - v0
        a=manifold_dotproduct3d(m, p); // m . (v0 - V0)
        
        b=manifold_dotproduct3d(m, iref->nn); // m . n
        
        // If the denominator is zero, the triangles are perpendicular.
        if (fabs(b)<MACHINE_EPSILON) return;
        
        // If the numerator is zero, the triangles are coplanar.
        if (fabs(a)<MACHINE_EPSILON) return;
        
        // Calculate the intersection point
        alpha=a/b;
        manifold_vectorscale3d(iref->nn, alpha, pi);
        manifold_vectoradd3d(pi, iref->V0, pi);
        
        // w = pi - v0
        manifold_vectorsubtract3d(pi, v0, w);
        
        // Calculate the dot products
        uu=manifold_dotproduct3d(u, u);
        vv=manifold_dotproduct3d(v, v);
        uv=manifold_dotproduct3d(u, v);
        uw=manifold_dotproduct3d(u, w);
        vw=manifold_dotproduct3d(v, w);
        
        // Find the coordinates (s,t) in the basis of this triangle
        denom=1.0/(uv*uv - uu*vv);
        s = (uv*vw - vv*uw)*denom;
        t = (uv*uw - uu*vw)*denom;
        
        // Check if we're in the triangle
        if (s<0.0 || t<0.0 || s+t>1.0 ) return;
        
        // If we are, record the intersection point
        if (iref->nintersections>MANIFOLD_ORIENTMAXINTERSECTIONS) {
            printf("Too many intersections. Recompile with MANIFOLD_ORIENTMAXINTERSECTIONS set to a larger value.\n");
            return;
        }
        iref->intersectionlist[iref->nintersections].alpha=alpha;
        iref->intersectionlist[iref->nintersections].id=id;
        iref->nintersections++;
    }
}

typedef struct {
    manifold_object *mobj;
    interpretcontext *context;
    unsigned nflipped;
} manifold_orientfacesmapfunctionref;

int manifold_orientfacessortcomparison(const void* p1, const void* p2) {
    manifold_orientfacesintersection *e1 = (manifold_orientfacesintersection *) p1;
    manifold_orientfacesintersection *e2 = (manifold_orientfacesintersection *) p2;
    int ret=0;
    
    if (e1->alpha<e2->alpha) ret=-1;
    if (e1->alpha>e2->alpha) ret=1;
    
    return ret;
}

#define MANIFOLD_ORIENT_EPSILON 1e-12
void manifold_orientfacesmapfunction (uid id, void *data, void *r) {
    manifold_gradelowerentry *e = (manifold_gradelowerentry *) data;
    manifold_orientfacesmapfunctionref *ref=(manifold_orientfacesmapfunctionref *) r;
    manifold_orientfacesintersectiontestref iref;
    manifold_object *mobj=NULL;
    interpretcontext *context=NULL;
    manifold_orientfacesintersection intersections[MANIFOLD_ORIENTMAXINTERSECTIONS];
    uid vert[3];
    MFLD_DBL xm[3], nn[3];
    unsigned int nel;
    int sgn;
    expression *mva=NULL, *mvadual=NULL;
    
    if (!ref) return;
    mobj=ref->mobj; context=ref->context;
    
    // Get the vertices
    manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vert, &nel);
    if (nel) manifold_vertexmidpoint(mobj, vert, nel, xm, NULL); // Find the midpoint
    
    //manifold_vertexdisplacement(mobj, vert[0], vert[1], uu); // Edge 1
    //manifold_vertexdisplacement(mobj, vert[0], vert[2], vv); // Edge 2
    //manifold_crossproduct3d(uu, vv, nn); // Normal
    
    mva=manifold_elementtomultivector( mobj, ref->context, MANIFOLD_GRADE_AREA, id);
    if (eval_isobjectref(mva)) {
        mvadual=multivector_dual((multivectorobject *) eval_objectfromref(mva), ref->context);
        if (eval_isobjectref(mvadual)) {
            multivector_scale((multivectorobject *) eval_objectfromref(mvadual), ref->context, -1.0);
            multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(mvadual), MANIFOLD_GRADE_LINE, nn);
        }
    }
    if (mva) freeexpression(mva);
    if (mvadual) freeexpression(mvadual);
    
    iref.mobj=mobj; iref.context=context; iref.testid=id;
    iref.V0=xm; iref.nn=nn; // Pass the midpoint and normal
    intersections[0].alpha=0.0; intersections[0].id=id; // Include this one
    iref.nintersections=1; iref.intersectionlist=intersections;
    idtable_map(mobj->gradelower[MANIFOLD_GRADE_AREA-1], manifold_orientfaceintersectiontest, &iref);
    
    if (iref.nintersections>1) {
        /* Sort the list of intersections */
        qsort(intersections, iref.nintersections, sizeof(manifold_orientfacesintersection), manifold_orientfacessortcomparison);
        
        /* Remove any intersections that separated by less than MANIFOLD_ORIENT_EPSILON */
        for (unsigned int i=0; i<iref.nintersections-1; i++) {
            if (fabs((intersections[i].alpha-intersections[i+1].alpha)/(intersections[i].alpha+intersections[i+1].alpha))<MANIFOLD_ORIENT_EPSILON) {
                for (unsigned int j=i; j<iref.nintersections-1; j++) {
                    if (intersections[j].id!=id) intersections[j]=intersections[j+1];
                }
                iref.nintersections--;
            }
        }
        
        sgn=-1;
        for (unsigned int i=0; i<iref.nintersections; i++) {
            
            if (intersections[i].id==id && sgn<0) {
                uid swp=e->el[0];
                e->el[0]=e->el[1];
                e->el[1]=swp;
                
                ref->nflipped++;
            }
            sgn=-sgn;
        }
    } else if (iref.nintersections==1) {
        /* If there's only one intersection, orient the face with the positive direction of the largest coordinate */
        unsigned int il=0;
        for (unsigned int i=1; i<3; i++) { if (fabs(nn[i])>fabs(nn[il])) il=i; }
        
        
        if (nn[il]<0) {
            uid swp=e->el[0];
            e->el[0]=e->el[1];
            e->el[1]=swp;
            
            ref->nflipped++;
        }
    }
    
}

/* Attempts to orient a manifold */
unsigned int manifold_orient(manifold_object *mobj, interpretcontext *context) {
    manifold_orientfacesmapfunctionref ref;
    
    if (mobj->grade!=2) { printf("orient only works on surfaces for now.\n"); return 0; }
    if (mobj->dimension!=3) { printf("orient only works in 3d space for now.\n"); return 0; }
    
    ref.context=context;
    ref.mobj=mobj;
    ref.nflipped=0;
    
    if (mobj->gradelower[MANIFOLD_GRADE_AREA-1]) {
        idtable_map(mobj->gradelower[MANIFOLD_GRADE_AREA-1], manifold_orientfacesmapfunction, (void *) &ref);
    }
    
    return ref.nflipped;
}

/*
 * Shifts vertex positions
 */

typedef struct {
    field_object *fobj;
    MFLD_DBL scale;
    unsigned int dim;
} manifold_shiftverticesref;

void manifold_shiftverticesmapfunction (uid id, void *data, void *r) {
    manifold_vertex *v = (manifold_vertex *) data;
    manifold_shiftverticesref *ref = (manifold_shiftverticesref *) r;
    expression_objectreference *ret;
    MFLD_DBL x[ref->dim];
    
    ret=(expression_objectreference *) field_get(ref->fobj, id); /* TODO: generalize this to support arbitrary fields */
    
    if (eval_isobjectref(ret)) {
        if (multivector_gradetodoublelist((multivectorobject *) ret->obj, MANIFOLD_GRADE_LINE, x)) {
            for (unsigned int i=0; i<ref->dim; i++) {
                v->x[i]+=ref->scale * x[i];
            }
        }
    }
    
}

/* Displace the vertices according to a field */
void manifold_shiftvertices(manifold_object *mobj, expression_objectreference *fref, MFLD_DBL scale) {
    manifold_shiftverticesref ref;
    
    if (fref) {
        ref.fobj=(field_object *) fref->obj;
        ref.scale=scale;
        ref.dim=mobj->dimension;
        
        if (mobj && mobj->vertices) {
            idtable_map(mobj->vertices, manifold_shiftverticesmapfunction, &ref);
        }
    }
}

/* 
 * Some simple geometric operations
 */

/* Calculates the separation between two vertices */
MFLD_DBL manifold_vertexseparation(manifold_object *mobj, uid v1, uid v2) {
    MFLD_DBL length=0.0;
    manifold_vertex *v[2];
    
    if ((mobj)&&(mobj->vertices)) {
        v[0]=idtable_get(mobj->vertices, v1);
        v[1]=idtable_get(mobj->vertices, v2);
        
        /* length = sqrt(x^2 + y^2 + z^2 + ... ) */
        for (unsigned int i=0; i<mobj->dimension; i++) length+=(v[0]->x[i]-v[1]->x[i])*(v[0]->x[i]-v[1]->x[i]);
        length = sqrt(length);
    }
    
    return length;
}

/* Calculates the displacement vector x2 - x1, placing the result into xx */
void manifold_vertexdisplacement(manifold_object *mobj, uid v1, uid v2, MFLD_DBL *xx) {
    manifold_vertex *v[2];
    
    if ((mobj)&&(mobj->vertices)) {
        v[0]=idtable_get(mobj->vertices, v1);
        v[1]=idtable_get(mobj->vertices, v2);
        
        for (unsigned int i=0; i<mobj->dimension; i++) xx[i]=(v[1]->x[i]-v[0]->x[i]);
    }
    
}

/* Estimates a bivector 'normal' to a vertex */
expression *manifold_vertexnormal(manifold_object *mobj, interpretcontext *context, uid id) {
    uid faces[MANIFOLD_VERTEXNORMAL_MAXFACES];
    unsigned int nfaces=0;
    expression *mv=NULL, *res=NULL, *total=NULL;
    
    manifold_graderaise(mobj, MANIFOLD_GRADE_POINT, id, MANIFOLD_GRADE_AREA, MANIFOLD_VERTEXNORMAL_MAXFACES, faces, &nfaces);
    
    if (nfaces>0) {
        for (unsigned int i=0; i<nfaces; i++) {
            mv=manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, faces[i]);
            
            if (eval_isobjectref(mv)) {
                if (total) {
                    res=opadd(context, total, mv);
                    freeexpression(total);
                    total=res;
                } else {
                    total=cloneexpression(mv);
                }
            }
            
            if (mv) freeexpression(mv);
        }
    }
    
    /* Normalize */
    if (eval_isobjectref(total)) {
        multivectorobject *mvobj = (multivectorobject *) eval_objectfromref(total);
        expression *mvnorm = multivector_gradenorm(mvobj, MANIFOLD_GRADE_AREA, 1.0);
        
        if (eval_isreal(mvnorm)) multivector_scale(mvobj, context, 1.0/eval_floatvalue(mvnorm));
        freeexpression(mvnorm);
    }
    
    return total;
}

/* Calculates the midpoint of a list of vertices , placing the result into xx */
int manifold_vertexmidpoint(manifold_object *mobj, uid *v, unsigned int nv, MFLD_DBL *xx, unsigned int *flags) {
    manifold_vertex *vert[nv];
    unsigned int i,j;
    
    if (flags) *flags = MANIFOLD_VERTEX_NONE;
    
    if ((mobj)&&(mobj->vertices)) {
        for (i=0; i<nv; i++) {
            vert[i]=idtable_get(mobj->vertices, v[i]);
            if (!vert[i]) return FALSE;
        }
        /* Handle flags with care. Currently there's only one, fixed, and that should be set only if both attached vertices are fixed... */
        if (flags) *flags = vert[0]->flags & vert[1]->flags;
        
        for (i=0; i<mobj->dimension; i++) {
            xx[i]=0.0;
            for (j=0; j<nv; j++) xx[i]+=vert[j]->x[i];
            xx[i] /= (MFLD_DBL) nv;
        }
    }
    return TRUE;
}


/* 
 * Selectors
 */

/* -- Standard selectors -- */

/* new selector --- called upon instantiation. */
expression *manifold_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_init((manifold_object *) obj);
    
    return NULL;
}

/* free selector --- called upon destruction. */
expression *manifold_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_free((manifold_object *) obj, context);
    
    return NULL;
}

/* clone selector */
expression *manifold_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    return manifold_clone((manifold_object *) obj, context);
}

/* serialize selector */
expression *manifold_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    return NULL;
}

/* deserialize selector */
expression *manifold_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    return NULL;
}

/* -- Mesh manipulation -- */

/* Vertices */

/* Add vertex */
expression *manifold_addvertexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    uid id=0;
    
    if ((mobj)&&(nargs==1)) {
        if (eval_islist(args[0])) {
            expression_list *lst = (expression_list *) args[0];
            unsigned int length = eval_listlength(lst);
            
            MFLD_DBL pt[length];
            if (manifold_listtodoublearray(lst, pt, length)) {
                id=manifold_addvertex(mobj, context, length, pt, MANIFOLD_VERTEX_NONE);
            }
        }
    }
    
    return newexpressioninteger((int) id);
}

/* Delete vertex */
expression *manifold_deletevertexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((obj)&&(nargs==1)) {
        if (eval_isinteger(args[0])) {
           uid id = (uid) eval_integervalue(args[0]);
            
           manifold_deletevertex((manifold_object *) obj, context, id);
        }
    }
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_setvertexpositioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    
    if ((mobj)&&(nargs==2)) {
        if (eval_isinteger(args[0]) && eval_islist(args[1])) {
            uid id = (uid) eval_integervalue(args[0]);
            
            MFLD_DBL pt[mobj->dimension+1];
            
            if (eval_listtodoublearray((expression_list *) args[1], pt, mobj->dimension)) {
                manifold_setvertexposition(mobj, context, id, pt);
            } else {
                error_raise(context, ERROR_MANIFOLD_INVALIVERTEXLIST, ERROR_MANIFOLD_INVALIVERTEXLIST_MSG, ERROR_FATAL);
            }
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_getvertexpositioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    manifold_object *mobj = (manifold_object *) obj;
    if ((mobj)&&(nargs==1)) {
        if (eval_isinteger(args[0])) {
            uid id = (uid) eval_integervalue(args[0]);
            
            MFLD_DBL pt[mobj->dimension+1];
            
            if (manifold_getvertexposition(mobj, context, id, pt)) {
                ret = (expression *) manifold_listfromdoublearray(pt, mobj->dimension);
            } else {
                error_raise(context, ERROR_MANIFOLD_INVALIDVERTEXID, ERROR_MANIFOLD_INVALIDVERTEXID_MSG, ERROR_FATAL);
            }
        }
    }
    
    return ret;
}

expression *manifold_fixi(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sel=NULL;
    if (nargs==1) {
        if (eval_isobjectref(args[0])) sel=(selection_object *) ((expression_objectreference *) args[0])->obj;
        if ((sel) && (sel->clss=class_lookup(GEOMETRY_SELECTIONCLASS))) {
            manifold_fixwithselection((manifold_object *) obj, context, sel, TRUE);
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_unfixi(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sel=NULL;
    if (nargs==1) {
        if (eval_isobjectref(args[0])) sel=(selection_object *) ((expression_objectreference *) args[0])->obj;
        if ((sel) && (sel->clss=class_lookup(GEOMETRY_SELECTIONCLASS))) {
            manifold_fixwithselection((manifold_object *) obj, context, sel, FALSE);
        }
    }
    
    return (expression *) class_newobjectreference(obj);

}

expression *manifold_fixvertexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs==1 && eval_isinteger(args[0])) manifold_setvertexfixed((manifold_object *) obj, context, eval_integervalue(args[0]), TRUE);
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_unfixvertexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs==1 && eval_isinteger(args[0])) manifold_setvertexfixed((manifold_object *) obj, context, eval_integervalue(args[0]), FALSE);
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_isvertexfixedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs==1 && eval_isinteger(args[0]))
        return newexpressionbool(manifold_isvertexfixed((manifold_object *) obj, context, eval_integervalue(args[0])));
    else return NULL; 
}


/* Add elements */
expression *manifold_addelementi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    manifold_gradelowerentry e;
    /* Calls to add element contain the grade and the elements */
    
    manifold_object *mobj = (manifold_object *) obj;
    if ((mobj)&&(nargs==2)) {
        if (eval_isinteger(args[0]) &&
            eval_islist(args[1]) &&
            manifold_isvalididlist(mobj, (expression_list *) args[1])
            ) {
            
            if(manifold_listtoidarray((expression_list *) args[1], e.el, 2)) {
                uid new = manifold_addelement(mobj, context, (unsigned int) eval_integervalue(args[0]), &e);
                ret=newexpressioninteger((int) new);
            }
        }
    }

    return ret;
}

/* Counts the number of elements with a given grade */
expression *manifold_countelementsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    unsigned int grade=0, nelements=0;
    
    if ((nargs==1)&&(eval_isinteger(args[0]))) {
        grade=eval_integervalue(args[0]);
    }
    
    if (grade==0) {
        if (mobj->vertices) nelements=idtable_count(mobj->vertices);
    } else {
        if (grade<=mobj->grade) {
            if (mobj->gradelower[grade-1]) nelements=idtable_count(mobj->gradelower[grade-1]);
        }
    }
    
    return(newexpressioninteger(nelements));
}

/* Delete elements */
expression *manifold_deleteelementi(object *obj, interpretcontext *context, int nargs, expression **args);

/* -- Selection selectors */

expression *manifold_selectidi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    expression *sel = NULL;
    unsigned int grade;
    unsigned int nels=0;
    
    /* Validate parameters */
    if (!(nargs==2 && eval_isinteger(args[0]))) {
        error_raise(context, ERROR_MANIFOLD_SELECTIDARGS, ERROR_MANIFOLD_SELECTIDARGS_MSG, ERROR_FATAL);
    }
    
    grade = eval_integervalue(args[0]);
    
    if (eval_isinteger(args[1])) {
        nels = 1;
    } else if (eval_islist(args[1])) {
        nels = eval_listlength((expression_list *) args[1]);
    } else {
        error_raise(context, ERROR_MANIFOLD_SELECTIDARGS, ERROR_MANIFOLD_SELECTIDARGS_MSG, ERROR_FATAL);
    }
    
    uid ids[nels];
    
    if (eval_isinteger(args[1])) {
        ids[0]=(uid) eval_integervalue(args[1]);
    } else if (eval_islist(args[1])) {
        manifold_listtoidarray((expression_list *) args[1], ids, nels);
        
    }
    
    sel=selection_selectwithidlist(mobj, grade, nels, ids);
    
    return (expression *) sel;
}

expression *manifold_selectboundaryi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    return selection_selectboundary(mobj);
}

/* -- Mesh control selectors -- */

/* Refine selector */
expression *manifold_refinei(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *m = (manifold_object *) obj;
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    for (unsigned int i=0; i<nargs; i++) {
        expression_objectreference *oref = (expression_objectreference *) args[i];
        if (eval_isobjectref(oref) && oref->obj) {
            if (oref->obj->clss == class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) oref->obj;
        }
    }
    
    if (m) {
        ret = (expression *) manifold_refine(m, context, sel);
    }
    
    return ret;
}

expression *manifold_edgeflipi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    
    if (mobj && (nargs==1) && eval_isinteger(args[0])) {
        uid edge = (uid) eval_integervalue(args[0]);
        
        if ((mobj->gradelower)&&(mobj->gradelower[0])&&(idtable_get(mobj->gradelower[0], edge))) {
            manifold_tryedgeflip(mobj, context, edge);
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_equiangulatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    expression *ret=NULL;
    
    if (mobj) {
        ret=manifold_equiangulate(mobj, context);
    }
    
    return ret;
}

expression *manifold_prunei(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    
    if (mobj) {
        if (nargs==1) {
            if (eval_isinteger(args[0])) {
                unsigned int edge = eval_integervalue(args[0]);
                
                manifold_pruneedge(mobj, context, edge);
            }
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_orienti(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    unsigned int nflipped=0;
    
    nflipped=manifold_orient(mobj, context);
    
    return (expression *) newexpressioninteger(nflipped);
}

/* -- Fields -- */

expression *manifold_newfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    unsigned int grade=0;
    
    if (nargs>0) if (eval_isinteger(args[0])) grade = eval_integervalue(args[0]);
    
    return field_new(mobj, grade);
}

/* -- Export selectors -- */

typedef struct {
    FILE *f;
    manifold_object *mobj;
} manifold_exportmeshvertexmapfunctionref ;

void manifold_exportmeshvertexmapfunction(uid id, void *e, void *r ) {
    manifold_vertex *v = (manifold_vertex *) e;
    manifold_exportmeshvertexmapfunctionref *ref = (manifold_exportmeshvertexmapfunctionref *) r;
    
    fprintf(ref->f, "%u ", id);
    
    for (unsigned int i=0; i<ref->mobj->dimension; i++) {
        fprintf(ref->f, "%g ", v->x[i]);
    }
    
    fprintf(ref->f, "\n");
}

typedef struct {
    FILE *f;
    manifold_object *mobj;
    unsigned int grade;
} manifold_exportmeshgradelowerentrymapfunctionref ;


void manifold_exportmeshgradelowerentrymapfunction(uid id, void *e, void *r ) {
    manifold_exportmeshgradelowerentrymapfunctionref *ref = (manifold_exportmeshgradelowerentrymapfunctionref *) r;
    
    uid vertices[ref->mobj->grade+1]; /* Store */
    unsigned int nel=0;
    
    manifold_gradelower(ref->mobj, ref->grade, id, MANIFOLD_GRADE_POINT, vertices, &nel);
    
    fprintf(ref->f, "%u ", id);
    
    for (unsigned int i=0; i<nel; i++) {
        fprintf(ref->f, "%u ", vertices[i]);
    }
    
    fprintf(ref->f, "\n");
}

expression *manifold_exportmeshi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    manifold_exportmeshvertexmapfunctionref vref;
    manifold_exportmeshgradelowerentrymapfunctionref glref;
    
    if (nargs!=1) return NULL;
    if (args[0]->type!=EXPRESSION_STRING) return NULL;
    
    FILE *f;
    char *filename=((expression_string *)args[0])->value;
    
    f=fopen(filename, "w");
    if (!f) return NULL;
    
    if (mobj->vertices) {
        fprintf(f, "vertices\n\n");
        vref.f=f; vref.mobj=mobj;
        idtable_mapseq(mobj->vertices, manifold_exportmeshvertexmapfunction, &vref);
    }
    
    if (mobj->grade>0) {
        fprintf(f, "\nedges\n\n");
        
        glref.f=f; glref.mobj=mobj; glref.grade=MANIFOLD_GRADE_LINE;
        idtable_mapseq(mobj->gradelower[MANIFOLD_GRADE_LINE-1], manifold_exportmeshgradelowerentrymapfunction, &glref);
    }
    
    if (mobj->grade>1) {
        fprintf(f, "\nfaces\n\n");
        
        glref.f=f; glref.mobj=mobj; glref.grade=MANIFOLD_GRADE_AREA;
        idtable_mapseq(mobj->gradelower[MANIFOLD_GRADE_AREA-1], manifold_exportmeshgradelowerentrymapfunction, &glref);
    }
    
    if (mobj->grade>2) {
        fprintf(f, "\nvolumes\n\n");
        
        glref.f=f; glref.mobj=mobj; glref.grade=MANIFOLD_GRADE_VOLUME;
        idtable_mapseq(mobj->gradelower[MANIFOLD_GRADE_VOLUME-1], manifold_exportmeshgradelowerentrymapfunction, &glref);
    }
    
    fclose(f);
    
    return (expression *) class_newobjectreference(obj);
}

expression *manifold_exportvtki(object *obj, interpretcontext *context, int nargs, expression **args) {
    //manifold_object *mobj = (manifold_object *) obj;
    
    if (nargs!=1) return NULL;
    if (args[0]->type!=EXPRESSION_STRING) return NULL;
    
    FILE *f;
    char *filename=((expression_string *)args[0])->value;
    
    f=fopen(filename, "w");
    if (!f) return NULL;
    
    fclose(f);
    
    return (expression *) class_newobjectreference(obj);
}

typedef struct {
    manifold_object *mobj;
    unsigned int grade;
    int success;
} manifoldcheckref;

void manifold_checkgradelowermapfunction(uid id, void *e, void *r) {
    manifoldcheckref *ref=(manifoldcheckref *) r;
    manifold_gradelowerentry *gl = (manifold_gradelowerentry *) e;
    manifold_graderaiseentry *gr;
    
    if (ref->mobj->graderaise[ref->grade-1]) {
        //printf("Checking grade lower entry of element %u of grade %u.\n", id, ref->grade);
        for (unsigned int i=0; i<2; i++) {
            if (idtable_ispresent(ref->mobj->graderaise[ref->grade-1], gl->el[i])) {
                gr=idtable_get(ref->mobj->graderaise[ref->grade-1], gl->el[i]);
                
                if (gr && gr->list) {
                    linkedlistentry *f=NULL;
                    
                    for (f=gr->list->first; f!=NULL; f=f->next) {
                        if ((* (uid *) f->data)==id) break;
                    }
                    
                    if (!f) {
                        printf("While checking element %u of grade %u found element %u of %u lacks a reciprocal entry.\n", id, ref->grade, gl->el[i], ref->grade-1);
                        ref->success=FALSE;
                    }
                } else {
                   printf("While checking element %u of grade %u found element %u of %u has a blank grade raise entry.\n", id, ref->grade, gl->el[i], ref->grade-1);
                   ref->success=FALSE;
                }
            } else {
                printf("While checking element %u of grade %u found element %u of %u has no grade raise entry.\n", id, ref->grade, gl->el[i], ref->grade-1);
                ref->success=FALSE;
            }
        }
    }
    
}

void manifold_checkgraderaisemapfunction(uid id, void *e, void *r) {
    manifoldcheckref *ref=(manifoldcheckref *) r;
    manifold_gradelowerentry *gl;
    manifold_graderaiseentry *gr = (manifold_graderaiseentry *) e;
    
    if (ref->mobj->gradelower[ref->grade]) {
        //printf("Checking grade raise entry of element %u of grade %u.\n", id, ref->grade);
        
        linkedlistentry *f=NULL;
        
        if (gr->list && gr->list->first) {
            for (f=gr->list->first; f!=NULL; f=f->next) {
                uid oid = (* (uid *) f->data);
                
                gl=idtable_get(ref->mobj->gradelower[ref->grade], oid);
                
                if (gl) {
                    if ((gl->el[0]==id)||(gl->el[1]==id)) {
                        
                    } else {
                        printf("While checking element %u of grade %u found element %u of %u lacks a reciprocal entry.\n", id, ref->grade, oid, ref->grade+1);
                        ref->success=FALSE;
                    }
                } else {
                    printf("While checking element %u of grade %u found element %u of %u has no grade lower entry.\n", id, ref->grade, oid, ref->grade+1);
                    ref->success=FALSE;
                }
            }
        } //else  printf("While checking element %u of grade %u found an empty grade raise entry.\n", id, ref->grade);
    }
}

/* Checks a manifold data structure. Raises errors where it finds problems. */
expression *manifold_checki(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *mobj = (manifold_object *) obj;
    manifoldcheckref ref;
    
    ref.mobj=mobj; ref.success=TRUE;
    
    for (unsigned int i=0; i<mobj->grade; i++) {
        ref.grade=i+1;
        idtable_map(mobj->gradelower[i], manifold_checkgradelowermapfunction, &ref);
    }
    
    for (unsigned int i=0; i<mobj->grade; i++) {
        ref.grade=i;
        idtable_map(mobj->graderaise[i], manifold_checkgraderaisemapfunction, &ref);
    }
    
    return newexpressionbool(ref.success);
}

/* 
 * Initialization
 */

void manifoldinitialize(void) {
    classintrinsic *cls;

    cls=class_classintrinsic(MANIFOLD_LABEL, class_lookup(EVAL_OBJECT), sizeof(manifold_object), 25);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, manifold_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, manifold_freei);
    class_registerselector(cls, EVAL_CLONE, FALSE, manifold_clonei);
    
    class_registerselector(cls, MANIFOLD_ADDVERTEX, FALSE, manifold_addvertexi);
    class_registerselector(cls, MANIFOLD_DELETEVERTEX, FALSE, manifold_deletevertexi);
    class_registerselector(cls, MANIFOLD_SETVERTEXPOSITION, FALSE, manifold_setvertexpositioni);
    class_registerselector(cls, MANIFOLD_GETVERTEXPOSITION, FALSE, manifold_getvertexpositioni);
    class_registerselector(cls, MANIFOLD_FIX, FALSE, manifold_fixi);
    class_registerselector(cls, MANIFOLD_UNFIX, FALSE, manifold_unfixi);
    class_registerselector(cls, MANIFOLD_FIXVERTEX, FALSE, manifold_fixvertexi);
    class_registerselector(cls, MANIFOLD_UNFIXVERTEX, FALSE, manifold_unfixvertexi);
    class_registerselector(cls, MANIFOLD_ISVERTEXFIXED, FALSE, manifold_isvertexfixedi);
    class_registerselector(cls, MANIFOLD_EDGEFLIP, FALSE, manifold_edgeflipi);
    class_registerselector(cls, MANIFOLD_EQUIANGULATE, FALSE, manifold_equiangulatei);
    class_registerselector(cls, MANIFOLD_PRUNE, FALSE, manifold_prunei);
    class_registerselector(cls, MANIFOLD_CHECK, FALSE, manifold_checki);
    class_registerselector(cls, MANIFOLD_ORIENT, FALSE, manifold_orienti);
    
    class_registerselector(cls, MANIFOLD_NEWFIELD, FALSE, manifold_newfieldi);
    
    class_registerselector(cls, MANIFOLD_SELECTID, FALSE, manifold_selectidi);
    class_registerselector(cls, GEOMETRY_SELECTBOUNDARY_SELECTOR, FALSE, manifold_selectboundaryi);
    
    //class_registerselector(cls, MANIFOLD_EXPORTVTK, FALSE, manifold_exportvtki);
    class_registerselector(cls, MANIFOLD_EXPORTMESH, FALSE, manifold_exportmeshi);
    
    class_registerselector(cls, MANIFOLD_ADDELEMENT, FALSE, manifold_addelementi);
    class_registerselector(cls, MANIFOLD_COUNTELEMENTS, FALSE, manifold_countelementsi);
    
    class_registerselector(cls, MANIFOLD_REFINE, FALSE, manifold_refinei);
}
