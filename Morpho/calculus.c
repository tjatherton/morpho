/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * Calculus package
 */

#include "calculus.h"

/*
 * Functions to differentiate functions
 */

expression *eval_diffsin(interpretcontext *context, expression *exp, char *symbol) {
    expression_function *fexp = (expression_function *) exp;
    expression_function *dfexp;
    operator *mul=operator_find("*");
    expression_name name;
    name.type=EXPRESSION_NAME;
    name.name="cos";
    
    /* d sin(u)/dx = cos(u)*du/dx */
    
    dfexp=newexpressionfunction((expression *) &name, fexp->nargs, fexp->argslist, TRUE);
    return newexpressionoperator(mul, (expression *) dfexp, differentiate(context, fexp->argslist[0],symbol), FALSE);
}

expression *eval_diffcos(interpretcontext *context, expression *exp, char *symbol) {
    expression_function *fexp = (expression_function *) exp;
    expression_function *dfexp;
    operator *mul=operator_find("*");
    operator *sub=operator_find("-");
    expression_name name;
    name.type=EXPRESSION_NAME;
    name.name="sin";
    
    /* d cos(u)/dx = -sin(u)*du/dx */
    
    dfexp=newexpressionfunction((expression *) &name, fexp->nargs, fexp->argslist, TRUE);
    /* Note use of unary - here. */
    return newexpressionoperator(mul, newexpressionoperator(sub, NULL, (expression *) dfexp, FALSE), differentiate(context, fexp->argslist[0],symbol), FALSE);
}

expression *eval_diffexp(interpretcontext *context, expression *exp, char *symbol) {
    expression_function *fexp = (expression_function *) exp;
    expression_function *dfexp;
    operator *mul=operator_find("*");
    expression_name name;
    name.type=EXPRESSION_NAME;
    name.name="exp";
    
    dfexp=newexpressionfunction((expression *) &name, fexp->nargs, fexp->argslist, TRUE);
    return newexpressionoperator(mul, (expression *) dfexp, differentiate(context, fexp->argslist[0],symbol), FALSE);
}

/* 
 * Functions to differentiate operators 
 */

expression *eval_diffaddop(interpretcontext *context, expression *exp, char *symbol) {
    expression_operator *op=(expression_operator *) exp;
    return newexpressionoperator(op->op, differentiate(context, op->left,symbol), differentiate(context, op->right,symbol),FALSE);
}

expression *eval_diffsubop(interpretcontext *context, expression *exp, char *symbol) {
    expression_operator *op=(expression_operator *) exp;
    return newexpressionoperator(op->op, differentiate(context, op->left,symbol), differentiate(context, op->right,symbol),FALSE);
}

expression *eval_diffmulop(interpretcontext *context, expression *exp, char *symbol) {
    expression_operator *op=(expression_operator *) exp;
    expression *dleft, *dright;
    operator *add=operator_find("+");
    operator *mul=op->op;
    
    dleft=differentiate(context, op->left,symbol);
    dright=differentiate(context, op->right,symbol);

    return newexpressionoperator(add, newexpressionoperator(mul, dleft, cloneexpression(op->right),FALSE), newexpressionoperator(mul, cloneexpression(op->left), dright,FALSE),FALSE);
}

expression *eval_diffdivop(interpretcontext *context, expression *exp, char *symbol) {
    expression_operator *op=(expression_operator *) exp;
    expression *dleft, *dright;
    operator *sub=operator_find("-");
    operator *mul=operator_find("*");
    operator *div=op->op;
    
    dleft=differentiate(context, op->left,symbol);
    dright=differentiate(context, op->right,symbol);
    
    return newexpressionoperator(div,newexpressionoperator(sub, newexpressionoperator(mul, dleft, cloneexpression(op->right),FALSE), newexpressionoperator(mul, cloneexpression(op->left), dright, FALSE), FALSE),newexpressionoperator(mul, cloneexpression(op->right), cloneexpression(op->right), FALSE), FALSE);
}

expression *eval_diffpowop(interpretcontext *context, expression *exp, char *symbol) {
    expression_operator *op=(expression_operator *) exp;
    operator *sub=operator_find("-");
    operator *mul=operator_find("*");
    operator *pow=op->op;
    
    if (!eval_isnumerical(op->right)) printf("WARNING: differentiation of f(x)^g(x) not implemented.\n");
    
    return newexpressionoperator(mul, cloneexpression(op->right), newexpressionoperator(mul, newexpressionoperator(pow, cloneexpression(op->left),newexpressionoperator(sub, cloneexpression(op->right), newexpressioninteger(1),FALSE),FALSE), differentiate(context, op->left,symbol),FALSE),FALSE);
}

/*
 * Differentiation
 */

expression *difffunction(interpretcontext *context, expression_function *func, char *symbol) {
    intrinsicfunction *fn=NULL;
  //  if (func) if(op->op) if (op->op->diff!=NULL) return op->op->diff((expression *) op,symbol);

    if ((func)&&(eval_isname(func->name))) {
        /* Is this an intrinsic function? */
        fn = (intrinsicfunction *) hashget(intrinsics,((expression_name *)func->name)->name);
        
        /* If so, call the function that differentiates it, if defined. */
        if (fn) {
            if (fn->diff) return fn->diff(context,(expression *) func,symbol);
        }
        
        /* TODO: Differentiate user-defined functions. */
        
        sprintf(error_buffer(),ERROR_CALCCANTDIFFFUNC_MSG,((expression_name *)func->name)->name);
        error_raise(context, ERROR_CALCCANTDIFFFUNC, error_buffer(), ERROR_FATAL);
    }
    
    return NULL;
}

expression *diffoperator(interpretcontext *context, expression_operator *op, char *symbol) {
    if (op) {
        if(op->op) if (op->op->diff!=NULL) return op->op->diff(context,(expression *) op,symbol);
        sprintf(error_buffer(),ERROR_CALCCANTDIFFOP_MSG,op->op->symbol);
        error_raise(context, ERROR_CALCCANTDIFFOP, error_buffer(), ERROR_FATAL);
    }
    
    return NULL;
}

expression *differentiate(interpretcontext *context, expression *exp, char *symbol) {
    if (exp) switch (exp->type) {
        case EXPRESSION_FLOAT:
        case EXPRESSION_INTEGER:
        case EXPRESSION_COMPLEX:
            return newexpressioninteger(0);
        case EXPRESSION_NAME:
            if (!(strcmp(((expression_name *) exp)->name,symbol))) return newexpressioninteger(1);
            else return newexpressioninteger(0); /* Could create a diff object here... */
        case EXPRESSION_OPERATOR:
            return diffoperator(context, (expression_operator *) exp, symbol);
        case EXPRESSION_FUNCTION:
            return difffunction(context, (expression_function *) exp, symbol);
        default:
            error_raise(context, ERROR_CALCCANTDIFFEXP, ERROR_CALCCANTDIFFEXP_MSG, ERROR_FATAL);
            break;
    }
    
    return NULL;
}

expression *eval_differentiatei(interpretcontext *context, int nargs, expression **args) {
    expression *dexp;
    for (int i=1;i<nargs;i++) if (args[i]->type!=EXPRESSION_NAME) return NULL;
    
    dexp=differentiate(context, args[0],((expression_name *) args[1])->name);
    
    return interpretexpression(context,dexp);
}

void evalinitializecalculus(void) {
    intrinsic("d", TRUE, eval_differentiatei, NULL);
}
