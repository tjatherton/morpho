/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * hash.h - Implements a key-value store
 */

#ifndef Morpho_hash_h
#define Morpho_hash_h

typedef struct hashentry_s {
	char *key;
	void *value;
	struct hashentry_s *next;
} hashentry;

typedef struct hashtable_s {
	int size;
	hashentry **table;
} hashtable;

typedef void hashmapfunction (char *,void*,void*);

#include "eval.h"

hashtable *hashcreate( int size );
int hash( hashtable *ht, char *key );
hashentry *hashnewpair( char *key, void *value );
void hashinsert( hashtable *ht, char *key, void *value );
void hashremove( hashtable *ht, char *key);
void *hashget( hashtable *ht, char *key );
void hashmap(hashtable *ht, hashmapfunction *func,void *ref);
void hashfreevalue(char *name, void *el, void *ref);
void hashfree( hashtable *ht);


#endif
