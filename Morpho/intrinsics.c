/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "intrinsics.h"

hashtable *intrinsics;

/* intrinsic - Declare an intrinsic function. Public interface to the intrinsics system.
 * Input:   (char *) label                 - The name of the function, as would be encountered by the parser.
 *          (intrinsicfunctiontype *) func - A reference to a function that implements it.
 *          (diffexpfunctype *) diff       - A reference to a function that can differentiate it.
 * Output:
 * Returns:
 */
void intrinsic(char *label, int hold, intrinsicfunctiontype *func, diffexpfunctype *diff) {
    intrinsicfunction *new=EVAL_MALLOC(sizeof(intrinsicfunction));
    
    /*if (hold) {
        printf("Intrinsic '%s' uses hold.\n", label);
    }*/
    
    if (new) {
        new->hold=hold;
        new->func=func;
        new->diff=diff;
        hashinsert( intrinsics, label, (void *) new);
    }
}

/*
 * Flow control operations
 */

/* newexpressionreturn - Wraps a return expression around a value to force the evaluator to return.
 * Input:   (int) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_RETURN
 */
expression *newexpressionreturn(expression *value) {
    expression_return *ne=NULL;
    
    if(!(ne = EVAL_MALLOC(sizeof(expression_return)))) return NULL;
    
    ne->type=EXPRESSION_RETURN;
    ne->value=value;
    
    return (expression *) ne;
}

/* return */
expression *eval_return(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    if (nargs>0) if (args) ret=cloneexpression(args[0]);
    
    return newexpressionreturn(ret);
}

/* if */
expression *eval_if(interpretcontext *context, int nargs, expression **args) {
    expression_bool *test=NULL;
    expression *result=NULL;
    
    if ((nargs<2)||(nargs>3)) { printf("if requires 2 or 3 arguments.\n"); goto eval_if_cleanup; };
    
    test=(expression_bool *) interpretexpression(context, args[0]);
    if (test) {
        if (test->type==EXPRESSION_BOOL) {
            if (test->value) {
                result=interpretexpression(context, args[1]);
            } else {
                if (nargs>2) result=interpretexpression(context, args[2]);
                else result=newexpressionnone();
            }
        } else {
            printf("if requires a logical test as its first argument.");
            goto eval_if_cleanup;
        }
    }
    
eval_if_cleanup:
    if (test) freeexpression((expression *) test);
    
    return result;
}

/* for */
expression *eval_for(interpretcontext *context, int nargs, expression **args) {
    if ((nargs<3)||(nargs>4)) { printf("for() requires 3 or 4 arguments.\n"); return NULL; };
    
    /* Initialization */
    freeexpression(interpretexpression(context, args[0]));
         
    do {
        expression_bool *test = (expression_bool *) interpretexpression(context, args[1]);
        if (!test||test->type!=EXPRESSION_BOOL) {
            printf("for requires the test argument to return a logical value (true/false).");
            return NULL;
        }
        if (!test->value) {
            freeexpression((expression *) test);
            return newexpressionnone();
        }
        freeexpression((expression *) test);
        
        /* Loop body */
        if (nargs>3) freeexpression(interpretexpression(context, args[3]));
        
        /* Increment */
        freeexpression(interpretexpression(context, args[2]));
    } while (1);
    
    return NULL;
}

/* do */
expression *eval_do(interpretcontext *context, int nargs, expression **args) {
    genericcountertype *counter=NULL;
    if (nargs!=2) { printf("do() requires 2 arguments.\n"); return NULL; };
    
    counter=eval_listtocounter(context, (expression_list *) args[1], FALSE);
    if (!counter) goto eval_do_cleanup;
    
    if (counter) {
        eval_counterreset((genericcountertype *)counter);
        
        do {
            eval_counterstore(context, (genericcountertype *) counter);
            freeexpression(interpretexpression(context, args[0]));
            
        } while (!eval_counteradvance((genericcountertype *) counter));
    }
    
eval_do_cleanup:
    if (counter) EVAL_FREE(counter);
    
    return newexpressionnone();
}

/* while */
expression *eval_while(interpretcontext *context, int nargs, expression **args) {
    expression *test;
    if (nargs!=2) { printf("while() requires 2 arguments.\n"); return NULL; };
    
    test=interpretexpression(context, args[0]);
    if (!test) goto eval_while_cleanup;
    
    while (eval_isbool(test) && eval_boolvalue(test)) {
        freeexpression(interpretexpression(context, args[1]));
        
        freeexpression(test);
        test=interpretexpression(context, args[0]);
    }
    
eval_while_cleanup:
    if (test) freeexpression(test);
    
    return newexpressionnone();
}

/*
 * String operations
 */

expression *eval_tostring(interpretcontext *context, int nargs, expression **args) {
    size_t size=0, nbytes=0;
    expression_string *newstring=NULL;
    
    /* First establish the length required */
    for (unsigned int i=0; i<nargs; i++) size += sexpressionprint(args[i], 0, NULL);
    
    newstring=EVAL_MALLOC(sizeof(expression_string));
    
    if (newstring) {
        newstring->type=EXPRESSION_STRING;
        newstring->value=EVAL_MALLOC(size+1);
        
        if (newstring->value) {
            for (unsigned int i=0; i<nargs; i++) {
                nbytes += sexpressionprint(args[i], size-nbytes+1, newstring->value+nbytes);
            }
        } else {
            /* Delete newstring if the buffer could not be allocated */
            EVAL_FREE(newstring); newstring=NULL;
        }
    }
    
    return (expression *) newstring;
}

expression *eval_stringjoin(interpretcontext *context, int nargs, expression **args) {
    size_t stringlength=0;
    
    for (unsigned int i=0; i<nargs;i++) {
        if eval_isstring(args[i]) stringlength+=strlen(((expression_string *) args[i])->value);
    }
    
    char string[stringlength+1];
    size_t start=0;
    
    for (unsigned int i=0; i<nargs;i++) {
        if eval_isstring(args[i]) {
            strcpy(string+start, ((expression_string *) args[i])->value);
            start+=strlen(string+start);
        }
    }
    string[start]='\0';
    
    return newexpressionstring(string);
}

expression *eval_stringsplit(interpretcontext *context, int nargs, expression **args) {
    expression *lexp=NULL;
    char *string=NULL;
    char *split=NULL;
    char *token=NULL;
    
    if (nargs<1) return NULL;
    
    if (args[0]->type==EXPRESSION_STRING) string=((expression_string *) args[0])->value;
    if ((nargs>1)&&(args[1]->type==EXPRESSION_STRING)) split=((expression_string *) args[1])->value;

    if (split==NULL) split=" \t\n\r";
    
    linkedlist *list = linkedlist_new();
    
    if (list) {
        token=strtok(string, split);
        while (token!=NULL) {
            linkedlist_addentry(list, newexpressionstring(token));
            token=strtok(NULL, split);
        }
        
        lexp = (expression *) newexpressionlist(list);
        
        linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(list);
    }
    
    return lexp;
}

/*
 * List constructors
 */

expression *eval_listfromfloatarray(float *flt, unsigned int nentries) {
    expression_list *list = newexpressionlist(NULL);
    expression *exp;
    
    if (list) for (unsigned int i=0;i<nentries;i++) {
        exp=newexpressionfloat((double) flt[i]);
        if (exp) {
            linkedlist_addentry(list->list, exp);
        } else {
            freeexpression(((expression *) list));
            list=NULL;
        }
    }
    
    return (expression *) list;
}

int eval_listtofloatarray(expression_list *list, float *flt, unsigned int maxentries) {
    unsigned int i;
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<maxentries); e=e->next) {
        if (!eval_isnumerical(((expression *) e->data))) return FALSE;
        i++;
        
    }
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<maxentries); e=e->next) {
        flt[i]=(float) eval_floatvalue((expression *) e->data);
        i++;
        
    }
    
    return TRUE;
}

expression *eval_listfromdoublearray(double *flt, unsigned int nentries) {
    expression_list *list = newexpressionlist(NULL);
    expression *exp;
    
    if (list) for (unsigned int i=0;i<nentries;i++) {
        exp=newexpressionfloat(flt[i]);
        if (exp) {
            linkedlist_addentry(list->list, exp);
        } else {
            freeexpression(((expression *) list));
            list=NULL;
        }
    }
    
    return (expression *) list;
}

expression *eval_listfromunsignedintegerarray(unsigned int *data, unsigned int nentries) {
    expression_list *list = newexpressionlist(NULL);
    expression *exp;
    
    if (list) for (unsigned int i=0;i<nentries;i++) {
        exp=newexpressioninteger((int) data[i]);
        if (exp) {
            linkedlist_addentry(list->list, exp);
        } else {
            freeexpression(((expression *) list));
            list=NULL;
        }
    }
    
    return (expression *) list;
}

expression *eval_listfromexpressionarray(expression **data, unsigned int nentries) {
    expression_list *list = newexpressionlist(NULL);
    expression *exp;
    
    if (list) for (unsigned int i=0;i<nentries;i++) {
        exp=cloneexpression(data[i]);
        if (exp) {
            linkedlist_addentry(list->list, exp);
        } else {
            freeexpression(((expression *) list));
            list=NULL;
        }
    }
    
    return (expression *) list;
}


int eval_listtodoublearray(expression_list *list, double *flt, unsigned int maxentries) {
    unsigned int i;
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<maxentries); e=e->next) {
        if (!eval_isnumerical(((expression *) e->data))) return FALSE;
        i++;
        
    }
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<maxentries); e=e->next) {
        flt[i]=eval_floatvalue((expression *) e->data);
        i++;
        
    }
    
    return TRUE;
}

int eval_listtoexpressionarray(expression_list *list, expression **exp, unsigned int maxentries) {
    unsigned int i;
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<maxentries); e=e->next) {
        exp[i]=cloneexpression((expression *) e->data);
        i++;
    }
    
    return TRUE;
}


int eval_listtomatrixsize(expression_list *list, unsigned int *nrows, unsigned int *ncols) {
    expression_list *inner;
    unsigned i=0, j=0;
    unsigned int nr, nc, nl;
    
    nr=linkedlist_length(list->list);
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<nr); e=e->next) {
        inner=e->data;

        if (eval_islist(inner)) {
            nl=linkedlist_length(inner->list);
            if (i==0) nc=nl;
            if (nc!=nl) return FALSE;
            
            j=0;
            for (linkedlistentry *f=inner->list->first; (f!=NULL)&&(j<nl); f=f->next) {
                if (!eval_isnumerical(((expression *) f->data))) return FALSE;
                j++;
            }
            
            if (j<nl) return FALSE;
        } else {
            return FALSE;
        }
        i++;
    }
    if (i<nr) return FALSE;
    
    if (nrows) *nrows=nr;
    if (ncols) *ncols=nc;
    
    return TRUE;
}

int eval_listtodoublematrix(expression_list *list, double *flt, unsigned int nrows, unsigned int ncols, int colmajor) {
    expression_list *inner;
    unsigned i=0, j=0;
    
    if (!flt) return FALSE; 
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<nrows); e=e->next) {
        inner=e->data;
        if (eval_islist(inner)) {
            j=0;
            for (linkedlistentry *f=inner->list->first; (f!=NULL)&&(j<ncols); f=f->next) {
                if (!eval_isnumerical(((expression *) f->data))) return FALSE;
                j++;
            }
            
            if (j<ncols) return FALSE;
        } else {
            return FALSE;
        }
        i++;
    }
    if (i<nrows) return FALSE;
    
    i=0;
    for (linkedlistentry *e=list->list->first; (e!=NULL)&&(i<nrows); e=e->next) {
        inner=e->data;
        if (eval_islist(inner)) {
            j=0;
            for (linkedlistentry *f=inner->list->first; (f!=NULL)&&(j<ncols); f=f->next) {
                if (colmajor) {
                    /* Column major order */
                    flt[i+j*nrows]=eval_floatvalue((expression *) f->data);
                } else {
                    /* Row major order */
                    flt[j+ncols*i]=eval_floatvalue((expression *) f->data);
                }
                j++;
            }
        } else {
            return FALSE;
        }
        i++;
    }
    
    return TRUE;
}


/*
 * List operations
 */

/* member */

int eval_listmember(interpretcontext *context, expression_list *list, expression *exp) {
    expression_bool *res=NULL;
    int result=FALSE;
    
    if ((list)&&(exp)&&(list->list)) {
        
        for (linkedlistentry *entry = list->list->first; entry!=NULL; entry=entry->next) {
            res=(expression_bool *) opeq(context, exp, (expression *) entry->data);
            
            if (eval_isbool(res)) {
                if (res->value) { result=TRUE; freeexpression((expression *) res); break; }
            }
            
            freeexpression((expression *) res);
        }
    }
    
    
    return result;
}

expression *eval_listmemberi(interpretcontext *context, int nargs, expression **args) {
    if ((nargs==2)&&(eval_islist(args[0]))) {
        return newexpressionbool(eval_listmember(context, (expression_list *) args[0], args[1]));
    }
    
    return NULL;
}

/* length */
unsigned int eval_listlength(expression_list *list) {
    unsigned int n=0;
    if (!list) return 0;

    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) n++;
    
    return n;
}

expression *eval_length(interpretcontext *context, int nargs, expression **args) {
    if ((nargs!=1)||(args[0]->type!=EXPRESSION_LIST)) return NULL;
    
    return newexpressioninteger((int) eval_listlength((expression_list *) args[0]));
}

/* getelement */
expression *eval_getelement(expression_list *list, unsigned int element) {
    unsigned int n=1;
    if ((!list)||(list->type!=EXPRESSION_LIST)) return NULL;
    
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        if (n==element) return (expression *) e->data;
        n++;
    }
    
    printf("List has only %u elements.\n", n-1);
    return NULL;
}

/* setelement */
expression *eval_setelement(expression_list *list, unsigned int element, expression *exp) {
    unsigned int n=1;
    if (!list) return 0;
    
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        if (n==element) {
            /* Remove the old value */
            if (e->data) freeexpression((expression *) e->data);
            /* Save the new one */
            e->data=exp;
            return exp;
        };
        n++;
    }
    
    printf("List has only %u elements.\n", n-1);
    return NULL;
}

/* sort */
int eval_sortfn(const void *a, const void *b) {
    expression_bool *res;
    int val;
    
    /* Test for *equality* */
    res=(expression_bool *) opeq(defaultinterpretcontext, *(expression **) a, *(expression **) b);
    if (res) {
        val=res->value; freeexpression((expression *) res);
        
        if (val) return 0;
    }
    
    res=(expression_bool *) oplessthan(defaultinterpretcontext, *(expression **) a, *(expression **) b);
    if (res) {
        val=res->value; freeexpression((expression *) res);
        
        if (val) {
            return -1;
        } else {
            return 1;
        }
    }
    
    return 0;
}

expression *eval_sort(interpretcontext *context, int nargs, expression **args) {
    if ((nargs!=1)||(!eval_islist(args[0]))) return NULL;
    
    expression_list *list = (expression_list *) cloneexpression(args[0]);
    unsigned int length = eval_listlength(list);
    unsigned int i;
        
    if (length>0) {
        expression* listtab[length+1];
        
        /* Convert the list to an array compatible with qsort */
        i=0;
        for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next, i++) {
            listtab[i]=(expression *) e->data;
        }
        
        qsort(listtab, (size_t) length, sizeof(expression*), eval_sortfn);
        
        /* Copy the sorted array back into the linked list */
        i=0;
        for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next, i++)
            e->data=listtab[i];

    }
    
    return (expression *) list;
}

/* max and min */

typedef enum {EVAL_MIN, EVAL_MAX} evalminmaxselect;

expression *evalminmax(expression_list *list, evalminmaxselect select) {
    unsigned int length = eval_listlength(list);
    unsigned int i;
    expression *result=NULL;
    
    if (length>0) {
        expression* listtab[length+1];
        
        /* Convert the list to an array compatible with qsort */
        i=0;
        for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next, i++) {
            listtab[i]=(expression *) e->data;
        }
        
        qsort(listtab, (size_t) length, sizeof(expression*), eval_sortfn);
        
        switch (select) {
            case EVAL_MIN:
                result = cloneexpression(listtab[0]);
                break;
            case EVAL_MAX:
                result = cloneexpression(listtab[length-1]);
                break;
        }
    }
    
    return result;
}

expression *eval_min(interpretcontext *context, int nargs, expression **args) {
    if ((nargs!=1)||(!eval_islist(args[0]))) return NULL;
    
    return evalminmax( (expression_list *) args[0], EVAL_MIN);
}

expression *eval_max(interpretcontext *context, int nargs, expression **args) {
    if ((nargs!=1)||(!eval_islist(args[0]))) return NULL;
    
    return evalminmax( (expression_list *) args[0], EVAL_MAX);
}

/* join */
expression *eval_join(interpretcontext *context, int nargs, expression **args) {
    expression_list *new;
    if (nargs<1) return NULL;
    
    for (int i=0;i<nargs;i++) {
        if (args[i]->type!=EXPRESSION_LIST) {
            error_raise(context, ERROR_JOINLISTREQUIRED, ERROR_JOINLISTREQUIRED_MSG, ERROR_WARNING);
            return NULL;
        }
    }
    
    new=newexpressionlist(((expression_list *) args[0])->list);
    
    if (new) if (new->list) {
        for (int i=1;i<nargs;i++) {
            expression_list *lexp=(expression_list *) args[i];
            if (eval_islist(lexp)) for (linkedlistentry *e=lexp->list->first; e!=NULL; e=e->next)      linkedlist_addentry(new->list,cloneexpression((expression *) e->data));
        }
    }
    
    return (expression *) new;
}

/* function */
expression *eval_function(interpretcontext *context, int nargs, expression **args) {
    expression_function func;
    expression *ret=NULL;
    
    if (nargs!=2) return NULL;

    func.type=EXPRESSION_FUNCTION_DEFINITION;
    func.nargs=0;
    func.name=NULL;
    func.argslist=NULL;
    
    /* How many arguments will this function have? */
    
    if (eval_islist(args[0])) {
        func.nargs=eval_listlength((expression_list *) args[0]);
    } else if (eval_isname(args[0])) {
        func.nargs=1;
    }

    if (func.nargs>0) {
        expression *fargs[func.nargs];
    
        for (unsigned int i=0; i<func.nargs; i++) fargs[i]=NULL;
        
        func.argslist=fargs;
        
        if (eval_islist(args[0])) {
            expression_list *list = (expression_list *) args[0];
            
            if (list->list) {
                unsigned int k=0;
                for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
                    fargs[k]=(expression *) e->data;
                    k++;
                }
            }
        } else if (eval_isname(args[0])) {
            fargs[0]=args[0];
        }
        
        ret = (expression *) newexpressionfunctiondefinition(&func, args[1], TRUE);
    }
    
    return ret;
}

/* map */
expression *eval_map(interpretcontext *context, int nargs, expression **args) {
    /* Check arguments */
    if (nargs!=2) return NULL;
    
    /* Resolve the name */
    expression *name=interpretexpression(context, args[0]);
    if (!(eval_isname(name) || eval_isfunctiondefinition(name))) {
        freeexpression(name);
        return NULL;
    }
    
    expression_list *list=(expression_list *) args[1];
    if (list->type!=EXPRESSION_LIST) {
        list=(expression_list *) interpretexpression(context, (expression *) list);
        if (list->type!=EXPRESSION_LIST) return NULL;
    }
    
    expression_function func;
    expression_list *result=newexpressionlist(NULL);
    expression *exp=NULL;
    
    if (!result) { freeexpression(name); return NULL; }
    
    /* Fill out a function call structure */
    func.type=EXPRESSION_FUNCTION;
    func.nargs=1;
    func.argslist=&exp;
    func.name=name;
    
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        exp=(expression *) e->data;
        linkedlist_addentry(result->list,callfunction(context, &func));
    }
    
    /* If we interpreted the list, we need to free the expression. */
    if (((expression_list *) args[1])->type!=EXPRESSION_LIST) freeexpression((expression *) list);
    
    if (name) freeexpression(name);
    
    return (expression *) result;
}

/* table */
int eval_integervalue(expression *exp) {
    if (!exp) return NAN;
    if (exp->type==EXPRESSION_INTEGER) return ((expression_integer *)exp)->value;
    if (exp->type==EXPRESSION_FLOAT) return (int) ((expression_float *)exp)->value;
    return NAN;
}

double eval_floatvalue(expression *exp) {
    if (!exp) return NAN;
    if (exp->type==EXPRESSION_FLOAT) return ((expression_float *)exp)->value;
    if (exp->type==EXPRESSION_INTEGER) return (double) ((expression_integer *)exp)->value;
    return NAN;
}

char *eval_stringvalue(expression *exp) {
    if (eval_isstring(exp)) {
        return ((expression_string *) exp)->value;
    } else return NULL;
}

int eval_boolvalue(expression *exp) {
    if (!exp) return FALSE;
    if (exp->type==EXPRESSION_BOOL) return ((expression_bool *)exp)->value;
    return FALSE;
}

genericcountertype *eval_listtocounter(interpretcontext *context, expression_list *lst, int forcefloat) {
    if (!lst) return NULL;
    if (lst->type!=EXPRESSION_LIST) { printf("Table expects lists of counting variables.\n"); return 0;}
    expression_name *name=(expression_name *) lst->list->first->data;
    if (name->type!=EXPRESSION_NAME) { printf("First element of list must be a variable name.\n"); return 0;}

    unsigned int len=linkedlist_length(lst->list);
    expression *exp[len];
    genericcountertype *counter=NULL;
    countertype type=COUNTER_INT;
    unsigned int k;

    for (int i=0; i<len; i++) exp[i]=NULL;
    
    linkedlistentry *el=lst->list->first;
    for (int i=0; i<len; i++) {
        if (i==0) {
            exp[i]=(expression *) el->data;
        } else {
            exp[i]=interpretexpression(context, (expression *) el->data);
            if (exp[i]) {
                if ((exp[i]->type!=EXPRESSION_INTEGER)&&(exp[i]->type!=EXPRESSION_FLOAT)) goto eval_listtocounter_clearup;
                if (exp[i]->type!=EXPRESSION_INTEGER) type=COUNTER_FLOAT;
            } else goto eval_listtocounter_clearup;
        }
        
        el=el->next;
    }
    
    if (forcefloat) type=COUNTER_FLOAT;
    
    if (type==COUNTER_INT) {
        intcountertype *icounter =EVAL_MALLOC(sizeof(intcountertype));
        if (!icounter) goto eval_listtocounter_clearup;
        
        icounter->type=COUNTER_INT;
        
        if (len==2) {
            icounter->start=1;
            icounter->end=((expression_integer *) exp[1])->value;
            icounter->increment=1;
        } else {
            icounter->start=((expression_integer *) exp[1])->value;
            icounter->end=((expression_integer *) exp[2])->value;
            if (len==4) icounter->increment=((expression_integer *) exp[3])->value;
            else icounter->increment=1;
        }
        
        counter=(genericcountertype *) icounter;
    } else {
        floatcountertype *fcounter=EVAL_MALLOC(sizeof(floatcountertype));
        if (!fcounter) goto eval_listtocounter_clearup;
        
        fcounter->type=COUNTER_FLOAT;
        if (len==2) {
            printf("Must have a lower and upper bound for a counter with floating point");
            EVAL_FREE(fcounter);
            goto eval_listtocounter_clearup;
        } else {
            fcounter->start=eval_floatvalue(exp[1]);
            fcounter->end=eval_floatvalue(exp[2]);
            if (len==4) fcounter->increment=eval_floatvalue(exp[3]);
            else fcounter->increment=1.0;
        }
        
        counter=(genericcountertype *) fcounter;
    }
    
    counter->name=(expression_name *) exp[0];

    /* Ensure that nsteps always has the correct number of steps that this counter will make. */
    eval_counterreset(counter);
    for (k=0; !eval_counteradvance(counter); k++);
    counter->nsteps=k+1;
    
 eval_listtocounter_clearup:
    for (int i=1; i<len; i++) if(exp[i]) freeexpression(exp[i]);
        
    return counter;
}

void eval_counterreset(genericcountertype *counter) {
    switch (counter->type) {
        case COUNTER_INT:
            {
                intcountertype *cint=(intcountertype *) counter;
                cint->value=cint->start;
            }
            break;
        case COUNTER_FLOAT:
            {
                floatcountertype *fint=(floatcountertype *) counter;
                fint->value=fint->start;
            }
            break;
    }
}

int eval_counteradvance(genericcountertype *counter) {
    int reset=FALSE;
    
    switch (counter->type) {
        case COUNTER_INT:
            {
                intcountertype *cint=(intcountertype *) counter;
                cint->value+=cint->increment;
                if (cint->value>cint->end) {reset=TRUE; eval_counterreset(counter);}
            }
            break;
        case COUNTER_FLOAT:
            {
                floatcountertype *fint=(floatcountertype *) counter;
                fint->value+=fint->increment;
                if (fint->value>fint->end) {reset=TRUE; eval_counterreset(counter);}
            }
            break;
    }
    
    return reset;
}

void eval_counterstore(interpretcontext *context, genericcountertype *counter) {
    /* Use opassign to *safely* assign the values in the local context, freeing old versions. */
    switch (counter->type) {
        case COUNTER_INT:
            {
                intcountertype *cint=(intcountertype *) counter;
                expression_integer eint = {EXPRESSION_INTEGER, cint->value};
                freeexpression(opassign(context, (expression *) cint->name, (expression *) &eint));
            }
            break;
        case COUNTER_FLOAT:
            {
                floatcountertype *fint=(floatcountertype *) counter;
                expression_float eflt = {EXPRESSION_FLOAT, fint->value};
                freeexpression(opassign(context, (expression *) fint->name, (expression *) &eflt));
            }
            break;
    }
}

expression *eval_table(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    genericcountertype* counters[nargs];
    expression_list* lists[nargs];
    interpretcontext *table_context=NULL;
    expression *exp = args[0];
    int j;
    
    /* Check arguments */
    if (nargs<2) return NULL;
    for (int i=0; i<nargs-1; i++) {
        counters[i]=eval_listtocounter(context, (expression_list *) args[i+1], FALSE);
        lists[i]=newexpressionlist(NULL);
    }
    
    table_context = newinterpretcontext(context, nargs-1);
    if (!table_context) goto eval_table_clearup;
    
    for (int i=0; i<nargs-1; i++) {
        if (!counters[i] || !lists[i]) goto eval_table_clearup;
        eval_counterreset(counters[i]);
        eval_counterstore(table_context,counters[i]);
    }
    
    do {
        linkedlist_addentry(lists[nargs-2]->list, interpretexpression(table_context, exp));
        
        /* Advance the counter */
        for (j=nargs-2; j>=0; j--) {
            int adv=eval_counteradvance(counters[j]);
            eval_counterstore(table_context,counters[j]);
            
            if (!adv) {
                break;
            } else if (j==0) {
                ret = (expression *) lists[0];
                break;
            } else {
                linkedlist_addentry(lists[j-1]->list, lists[j]);
                lists[j]=newexpressionlist(NULL);
            }
        }
        
    } while (!ret);

eval_table_clearup:
    
    for (int i=0; i<nargs-1; i++) {
        if (counters[i]) EVAL_FREE(counters[i]);
        if (lists[i]) if (!ret||i>0) freeexpression((expression *) lists[i]);
    }
    if (table_context) freeinterpretcontext(table_context);
    
    return ret;
}

expression *eval_sum(interpretcontext *context, int nargs, expression **args) {
    expression *res=NULL, *sum=NULL, *ret=NULL;
    genericcountertype* counters[nargs];
    interpretcontext *sum_context=NULL;
    expression *exp = args[0];
    int j;
    
    /* Check arguments */
    if (nargs<2) return NULL;
    for (int i=0; i<nargs-1; i++) {
        counters[i]=eval_listtocounter(context, (expression_list *) args[i+1], FALSE);
    }
    
    sum_context = newinterpretcontext(context, nargs-1);
    if (!sum_context) goto eval_sum_clearup;
    
    for (int i=0; i<nargs-1; i++) {
        if (!counters[i]) goto eval_sum_clearup;
        eval_counterreset(counters[i]);
        eval_counterstore(sum_context,counters[i]);
    }
    
    do {
        res=interpretexpression(sum_context, exp);
        
        if (sum) {
            expression *new=opadd(sum_context, sum, res);
            freeexpression(sum);
            sum=new;
        } else {
            sum=cloneexpression(res);
        }
        
        freeexpression(res);
        
        /* Advance the counter */
        for (j=nargs-2; j>=0; j--) {
            int adv=eval_counteradvance(counters[j]);
            eval_counterstore(sum_context,counters[j]);
            
            if (!adv) {
                break;
            } else if (j==0) {
                ret = sum;
                break;
            }
        }
        
    } while (!ret);
    
eval_sum_clearup:
    
    for (int i=0; i<nargs-1; i++) {
        if (counters[i]) EVAL_FREE(counters[i]);
    }
    if (sum_context) freeinterpretcontext(sum_context);
    
    return ret;
}


/*
 * Complex numbers
 */

/* Real part */
expression *eval_re(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_COMPLEX) return newexpressionfloat(creal(((expression_complex *) arg)->value));

    if (arg->type==EXPRESSION_INTEGER) return newexpressioninteger(((expression_integer *) arg)->value);
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(((expression_float *) arg)->value);
    return NULL;
}

/* Imaginary part */
expression *eval_im(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_COMPLEX) return newexpressionfloat(cimag(((expression_complex *) arg)->value));
    
    if (arg->type==EXPRESSION_INTEGER) return newexpressioninteger(0);
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(0);
    return NULL;

}

/* Absolute value / Modulus */
expression *eval_abs(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_INTEGER) return newexpressioninteger(abs( ((expression_integer *) arg)->value));
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(fabs( ((expression_float *) arg)->value));
    
    if (arg->type==EXPRESSION_COMPLEX) return newexpressionfloat(cabs( ((expression_complex *) arg)->value));

    return NULL;
}

/* Argument */
expression *eval_arg(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_COMPLEX) return newexpressionfloat(carg(((expression_complex *) arg)->value));
    
    if (arg->type==EXPRESSION_INTEGER) return newexpressioninteger(((expression_integer *) arg)->value);
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(((expression_float *) arg)->value);
    return NULL;
    
    return NULL;
}

/* Complex conjugate */
expression *eval_conj(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(conj(((expression_complex *) arg)->value));
    
    if (arg->type==EXPRESSION_INTEGER) return newexpressioninteger(((expression_integer *) arg)->value);
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(((expression_float *) arg)->value);
    return NULL;
}

/*
 * Math operations
 */

/* exp */
expression *eval_exp(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(exp( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex( cexp( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( exp((double) ((expression_integer *) arg)->value));
    
    return NULL;
}

/* log */
expression *eval_log(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) {
        /* Detect negative numbers and resort to complex numbers if necessary */
        if (((expression_float *) arg)->value > 0) return newexpressionfloat(log( ((expression_float *) arg)->value));
        else return newexpressioncomplex(clog( (complex double) ((expression_float *) arg)->value));
    }
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex( cexp( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) {
        if (((expression_integer *) arg)->value > 0) return newexpressionfloat( log((double) ((expression_integer *) arg)->value));
        else return newexpressioncomplex( clog((complex double) ((expression_integer *) arg)->value));
    }
    return NULL;
}

/* sin */
expression *eval_sin(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(sin( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(csin( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( sin((double) ((expression_integer *) arg)->value));
    return NULL;
}

/* cos */
expression *eval_cos(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(cos( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(ccos( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( cos((double) ((expression_integer *) arg)->value));
    return NULL;
}

/* sinh */
expression *eval_sinh(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(sinh( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(csinh( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( sinh((double) ((expression_integer *) arg)->value));
    return NULL;
}

/* cosh */
expression *eval_cosh(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(cosh( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(ccosh( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( cosh((double) ((expression_integer *) arg)->value));
    return NULL;
}

/* tan */
expression *eval_tan(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) return newexpressionfloat(tan( ((expression_float *) arg)->value));
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(ctan( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) return newexpressionfloat( tan((double) ((expression_integer *) arg)->value));
    return NULL;
}

/* arctan */
expression *eval_arctan(interpretcontext *context, int nargs, expression **args) {
    if (nargs==1 && eval_isnumerical(args[0])) {
        if (eval_isreal(args[0])) {
            return newexpressionfloat(atan(eval_floatvalue(args[0])));
        } else if (eval_iscomplex(args[0])) {
            double x=eval_real(args[0]), y=eval_imag(args[0]);
            double u=0, v=0;
            
            u=0.5*(atan2(x,1-y)-atan2(-x,1+y));
            v=0.25*(-log(x*x+(y-1)*(y-1))+log(x*x+(y+1)*(y+1)));
            
            return newexpressioncomplex(u + I*v);
        }
    } else if (nargs==2 && eval_isnumerical(args[0]) && eval_isnumerical(args[1])) {
        // Use the definition arctan(x, y) = arctan(y/x) which is the opposite of the C standard.
        return newexpressionfloat(atan2(eval_floatvalue(args[1]), eval_floatvalue(args[0])));
    }
    return NULL;
}

/* mod */
expression *eval_mod(interpretcontext *context, int nargs, expression **args) {
    if (nargs!=2) return NULL;
    
    expression_integer *dividend=(expression_integer *) args[0];
    expression_integer *divisor=(expression_integer *) args[1];
    
    if (eval_isinteger(dividend)&&eval_isinteger(divisor)) return newexpressioninteger(dividend->value % divisor->value);

    return NULL;
}

/* sqrt */
expression *eval_sqrt(interpretcontext *context, int nargs, expression **args) {
    expression *arg=*args;
    if (nargs!=1||arg==NULL) return NULL;
    if (arg->type==EXPRESSION_FLOAT) {
        /* Detect negative numbers and resort to complex numbers if necessary */
        if (((expression_float *) arg)->value >= 0) return newexpressionfloat(sqrt( ((expression_float *) arg)->value));
        else return newexpressioncomplex(csqrt( (complex double) ((expression_float *) arg)->value));
    }
    if (arg->type==EXPRESSION_COMPLEX) return newexpressioncomplex(csqrt( ((expression_complex *) arg)->value));
    if (arg->type==EXPRESSION_INTEGER) {
        if (((expression_integer *) arg)->value > 0) return newexpressionfloat( sqrt((double) ((expression_integer *) arg)->value));
        else return newexpressioncomplex( csqrt((complex double) ((expression_integer *) arg)->value));
    }
    return NULL;
}

/*
 * Numerical methods
 */

int eval_evaluateexpwithvalue(interpretcontext *context, expression *exp, expression_name *var, double value, double *result) {
    expression_float val;
    val.type=EXPRESSION_FLOAT;
    val.value=value;
    expression *res;
    
    opassign(context, (expression *) var, (expression *) &val);
    res=interpretexpression(context, exp);
    
    if (eval_isreal(res)) {
        if (result) *result=eval_floatvalue(res);
    }
    
    if (res) freeexpression(res);
    else return FALSE;
    
    return TRUE;
}

#define ROOT_TOL 1e-8

/* Root finding by brent's algorithm */
expression *eval_findroot(interpretcontext *context, int nargs, expression **args) {
    if (nargs!=2) return NULL;
    double a,b,c,d=0,s,fa,fb,fc,fs;
    interpretcontext *localcontext=newinterpretcontext(context, 1);
    expression *exp=args[0];
    expression_name *var=NULL;
    floatcountertype *counter=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[1], TRUE);
    int mflag;
    expression *root=NULL;
    
    if ((counter)&&(localcontext)) {
        var=counter->name;
        
        a=counter->start;
        b=counter->end;
        
        if ((eval_evaluateexpwithvalue(localcontext, exp, var, a, &fa))&&
            (eval_evaluateexpwithvalue(localcontext, exp, var, b, &fb))) {
            
            /* Check the initial bracket contains a sign. */
            if (fa*fb>0) {
                printf("Initial bracket (%f, %f) does not contain a change in sign.\n", a, b);
                goto root_cleanup;
            }
            
            /* Swap if (fa<fb) */
            if (fabs(fa)<fabs(fb)) {
                double swp=fb;
                fb=fa; fa=swp;
                swp=b; b=a; a=swp;
            }
            
            c=a; fc=fa;
            
            mflag=TRUE;
            
            while (fabs(b-a)>ROOT_TOL*fabs(a)) {
                
                if ((fabs(fa-fc)>MACHINE_EPSILON) && (fabs(fa-fc)>MACHINE_EPSILON)) {
                    /* Inverse quadratic interpolation */
                    s= a*fb*fc/(fa-fb)/(fa-fc) + b*fa*fc/(fb-fa)/(fb-fc) + c*fa*fb/(fc-fa)/(fc-fb);
                } else {
                    /* Secant */
                    s=b-fb*(b-a)/(fb-fa);
                }
                
               /* printf("[%i %i %i %i %i]",(!(0.25*(3*a+b)<s<b)),(mflag && fabs(s-b)>=0.5*fabs(b-c)),(!mflag && fabs(s-b)>=0.5*fabs(c-d)),(mflag && fabs(b-c)<ROOT_TOL),(!mflag && fabs(c-d)<ROOT_TOL));*/
                
                /* Revert to bisection in a panic */
                if ( (!(0.25*(3*a+b)<s<b)) ||
                     (mflag && fabs(s-b)>=0.5*fabs(b-c)) ||
                     (!mflag && fabs(s-b)>=0.5*fabs(c-d)) ||
                     (mflag && fabs(b-c)<ROOT_TOL) ||
                     (!mflag && fabs(c-d)<ROOT_TOL) ) {
                    /* Bisection */
                    s=0.5*(a+b);
                    mflag=TRUE;
                } else mflag=FALSE;
                
                if (eval_evaluateexpwithvalue(localcontext, exp, var, s, &fs)) {
                    d=c;
                    c=b; fc=fb;
                    if (fa*fs<0) { b=s; fb=fs; } else { a=s; fa=fs; }
                    if (fabs(fa)<fabs(fb)) {
                        double swp=fb;
                        fb=fa; fa=swp;
                        swp=b; b=a; a=swp;
                    }
                } else goto root_cleanup;
            }
            
            root=newexpressionfloat(0.5*(a+b));
        }
    }
    
root_cleanup:
    
    if (localcontext) freeinterpretcontext(localcontext);
    if (counter) EVAL_FREE(counter);
    
    return root;
}

/*
 * Computer algebra functions
 */

/* hold */
expression *eval_hold(interpretcontext *context, int nargs, expression **args) {
    if (nargs==1) {
        return cloneexpression(args[0]);
    }
    
    return newexpressionnone();
}


/* eval */
expression *eval_eval(interpretcontext *context, int nargs, expression **args) {
    expression_string *string = NULL;
    
    if (nargs==1) {
        string=(expression_string *) args[0];
        
        if (string->type==EXPRESSION_STRING) {
            return execute(context,string->value);
        }
    }
    
    return newexpressionnone();
}

/* substitute */
expression *eval_subs(interpretcontext *context, int nargs, expression **args) {
    interpretcontext *ctxt;
    expression *ret=NULL;
    
    if (nargs==0) return NULL;
    if (nargs==1) return interpretexpression(context, args[0]);
    
    ctxt=newinterpretcontext(context, nargs-1);
    if (ctxt) {
        /* Define arguments in present context */
        for (unsigned int i=1; i<nargs;i++) freeexpression(interpretexpression(ctxt, args[i]));
        
        ret = interpretexpression(ctxt, args[0]);
        
        freeinterpretcontext(ctxt);
    }
    
    return ret;
}


/*
 * Random Numbers
 */

expression *eval_rand(interpretcontext *context, int nargs, expression **args) {
//    if (nargs!=0) return NULL;

    return newexpressionfloat(genrand_real1());
}

expression *eval_rand_integer(interpretcontext *context, int nargs, expression **args) {
    int max=0;
    
    if (!((nargs==1)&&(eval_isinteger(args[0])))) max=2;
    else max=((expression_integer *) args[0])->value;
    
    return newexpressioninteger((int) (genrand_real1()*max+1.0));
}

expression *eval_rand_normal(interpretcontext *context, int nargs, expression **args) {
    double x,y,r;

    do {
        x=2.0*genrand_real1()-1.0;
        y=2.0*genrand_real1()-1.0;
        
        r=x*x+y*y;
    } while (r>=1.0);

    return newexpressionfloat(x*sqrt((-2.0*log(r))/r));
}

/*
 * Basic operations
 */

/* remove - removes symbol */
expression *eval_remove(interpretcontext *context, int nargs, expression **args) {
    for (int i=0; i<nargs; i++) {
        expression *val=NULL;
        
        if (args[i]->type==EXPRESSION_NAME) {
            val=(expression *) hashget(context->symbols,((expression_name *) args[i])->name);
            if (val) {
                freeexpression(val);
                hashremove(context->symbols, ((expression_name *) args[i])->name);
                break;
            } else {
                printf("Symbol '%s' is not defined.\n",((expression_name *) args[i])->name);
            }
        } else {
            printf("Remove requires a valid symbol.\n");
        }
    }
    
    return newexpressionnone();
}

/*
 * Low level operations
 */

/* Prints a symbol */
void symbolprint(char *name, expression *exp, void *arg) {
    printf("'%s' -> ",name);
    expressionprint(exp);
    printf("\n");
}

/* Clears all definitions */
expression *eval_restart(interpretcontext *context, int nargs, expression **args) {
    if (nargs!=0) return NULL;
    if (context!=defaultinterpretcontext) {
        printf("restart() cannot be used inside functions or selectors.\n");
        return NULL;
    }
    
    if(defaultinterpretcontext) freeinterpretcontext(defaultinterpretcontext);
        
    defaultinterpretcontext = newinterpretcontext(NULL,EVAL_DEFAULTCONTEXTTABLESIZE);
    
    return newexpressionnone();
}

/* Prints a collection of expressions */
expression *eval_print(interpretcontext *context, int nargs, expression **args) {
    for (int i=0;i<nargs;i++) {
        if (args[i]) {
            if (args[i]->type==EXPRESSION_STRING) {
                printf("%s",((expression_string *) args[i])->value);
            } else {
                expressionprint(args[i]);
            }
        }
    }
    printf("\n");
    return newexpressionnone();
}

/* Checks if a symbol is defined */
expression *eval_isdefined(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    if (nargs!=1) return NULL;
    if (eval_isname(args[0])) {
        ret=lookupsymbol(context, ((expression_name *) args[0])->name);
        if(ret) return newexpressionbool(TRUE);
    }
    
    return newexpressionbool(FALSE);
}


/* Returns all symbols defined in the current context */
expression *eval_symbols(interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = linkedlist_new();
    expression *ret = NULL;
    expression *retsrt = NULL;
    
    if (list) {
        hashmap(context->symbols, eval_name_map, list);
        
        ret=(expression *) newexpressionlist(list);
        
        linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(list);
        
        if (ret) {
            retsrt=eval_sort(context, 1, &ret);
            freeexpression(ret);
        }
    }
    
    return retsrt;
}

/* Returns a list of all available intrinsic functions */
void eval_name_map(char *name, void *el, void *ref) {
    linkedlist *list = (linkedlist *) ref;
    
    linkedlist_addentry(list, newexpressionname(name));
}

expression *eval_intrinsicsi(interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = linkedlist_new();
    expression *ret = NULL;
    expression *retsrt = NULL;
    
    if (list) {
        hashmap(intrinsics, eval_name_map, list);
        
        ret=(expression *) newexpressionlist(list);
        
        linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(list);
        
        if (ret) {
            retsrt=eval_sort(context, 1, &ret);
            freeexpression(ret);
        }
    }
    
    return retsrt;
}

/* 
 * Helper functions
 */

/* eval_optionstart - identifies which arguments are options
 *
 * Input:   (interpretcontext *) context - the current context
 *          (int) narg      - number of arguments
 *          (expression *) args[] - the arguments
 *          (int) evaluate  - if TRUE, evaluates the non-option arguments
 * Output:
 * Returns: int - the argument at which the options begin, or if there are none returns a quantity > narg
 */

int eval_optionstart(interpretcontext *context, int narg, expression **args) {
    int i=0;
    expression_operator *op;
    unsigned int noptions = 0;
    
    for (i=narg-1; i>=0; i--) {
        if (eval_isoperator(args[i])) {
            op = (expression_operator *) args[i];
            /* If this is the assignment operator, it is an option */
            if (op->op->executeop == &opassign) noptions++;
            else break;
        } else break;
    }
    
    return narg+1-noptions;
}

/* eval_options - process option arguments for intrinsic functions and selectors
 * Input:   (interpretcontext *) context - the current context
 *          (int) start     - index of the first argument that is an option [counting from 1 as the first argument]
 *          (int) narg      - number of arguments
 *          (expression *) args[] - the arguments
 * Output:
 * Returns: (interpretcontext *) a context containing the arguments
 */
interpretcontext *eval_options(interpretcontext *context, int start, int narg, expression **args) {
    interpretcontext *ret=NULL;
    if (start<=narg) {
        ret = newinterpretcontext(context, narg-start+1);
        if (ret) {
            for(int i=start-1; i<narg; i++) freeexpression(interpretexpression(ret, args[i]));
            ret->parent=NULL; // Options should not link to any parent context.
        }
    }
    
    return ret;
}

expression *eval_argstolist(int nargs, expression **args) {
    expression_list *list=newexpressionlist(NULL);
    
    if (list) for (int i=0;i<nargs;i++) linkedlist_addentry(list->list, args[i]);
    
    return (expression *) list;
}

/* This function returns a list of all symbols present in the expression; 
   The list is lightweight, consisting of references to existing strings and so does
   not need to be explicitly freed. */
void eval_symbolsinexpression(expression *exp, linkedlist *list) {
    if (exp) switch (exp->type) {
        case EXPRESSION_NAME:
            linkedlist_addentry(list, ((expression_name *) exp)->name);
            break;
        case EXPRESSION_FUNCTION:
            {
                expression_function *fexp = (expression_function *) exp;
                for (unsigned int i=0; i<fexp->nargs; i++) eval_symbolsinexpression(fexp->argslist[i], list);
            }
            break;
        case EXPRESSION_FUNCTION_DEFINITION:
            {
                expression_functiondefinition *fexp = (expression_functiondefinition *) exp;
                for (unsigned int i=0; i<fexp->nargs; i++) eval_symbolsinexpression(fexp->argslist[i], list);
                eval_symbolsinexpression(fexp->eval, list);
            }
            break;
        case EXPRESSION_INDEX:
            {
                expression_index *iexp = (expression_index *) exp;
                for (unsigned int i=0; i<iexp->nargs; i++) eval_symbolsinexpression(iexp->argslist[i], list);
            }
            break;
        case EXPRESSION_LIST:
            {
                expression_list *lexp = (expression_list *) exp;
                for (linkedlistentry *e=lexp->list->first; e!=NULL; e=e->next) eval_symbolsinexpression((expression *) e->data, list);
            }
            break;
        case EXPRESSION_OPERATOR:
            {
                expression_operator *oexp = (expression_operator *) exp;
                if (oexp->left) eval_symbolsinexpression(oexp->left, list);
                if (oexp->left) eval_symbolsinexpression(oexp->right, list);
            }
            break;
        case EXPRESSION_RETURN:
            {
                expression_return *rexp = (expression_return *) exp;
                eval_symbolsinexpression(rexp->value, list);
            }
            break;
        default:
            break;
    }
}

/*
 * Type checking
 */

expression *eval_isstringi(interpretcontext *context, int nargs, expression **args) {
    if (nargs>0) {
        if (eval_isstring(args[0])) return newexpressionbool(TRUE);
    }
    return newexpressionbool(FALSE);
}

expression *eval_isnumberi(interpretcontext *context, int nargs, expression **args) {
    if (nargs>0) {
        if (eval_isnumerical(args[0])) return newexpressionbool(TRUE);
    }
    return newexpressionbool(FALSE);
}

expression *eval_islisti(interpretcontext *context, int nargs, expression **args) {
    if (nargs>0) {
        if (eval_islist(args[0])) return newexpressionbool(TRUE);
    }
    return newexpressionbool(FALSE);
}


/*
 * Timing
 */

expression *eval_timingi(interpretcontext *context, int nargs, expression **args) {
    expression *exp = NULL, *ret=NULL;
    clock_t start=0, end=0;
    if (nargs>0) {
        start = clock();
        
        for (unsigned int i=0; i<nargs; i++) {
            exp=interpretexpression(context, args[i]);
            if (i<nargs-1) freeexpression(exp);
        }
        
        end = clock();
        
        ret = (expression *) newexpressionlistfromargs(2, newexpressionfloat(((double) end-start)/((double) CLOCKS_PER_SEC)), exp);
    } else {
        ret = newexpressionfloat(((double) clock())/((double) CLOCKS_PER_SEC));
    }
    
    return ( ret!=NULL ? ret : newexpressionnone());
}

/*
 * Histogram
 */
int eval_histogram_nbins(interpretcontext *context, int nargs, expression **args, int *nbins, int *userbins) {
    expression_list *data=(expression_list *) args[0];
    int success=FALSE;
    /* The first argument must be a list */
    if (eval_isobjectref(data)) {
        
    }
    
    if (!eval_islist(data)) return FALSE;
    
    /* Deal with the bin specification */
    if (nargs==2) {
        expression_integer *inbins=(expression_integer *) args[1];
        if (eval_isinteger(inbins)) {
            *nbins=inbins->value;
            success=TRUE;
        } else if (eval_islist(inbins)) {
            *nbins=eval_listlength((expression_list *) inbins)-1; // -1 needed as nbins is the number of bins, not hte number of bounds.
            *userbins=TRUE;
            success=TRUE;
        }
    } else if (nargs==1) {
        *nbins=HISTOGRAM_DEFAULT_NBINS; success=TRUE;
    }
    
    return success;
}

void eval_histogram_bincounts(expression_list *data, expression_list *userbinlist, int userbins, int nbins, double *binbounds, unsigned int *bincounts) {
    double min=0.0, max=0.0;
    
    if (!userbins) {
        /* Loop over list determining minimum and maximum values */
        if (data->list) for (linkedlistentry *e=data->list->first; e!=NULL; e=e->next) {
            expression *exp = (expression *) e->data;
            double value;
            
            if (eval_isreal(exp)) {
                value=eval_floatvalue(exp);
                if (e==data->list->first) {
                    min=value; max=value;
                } else {
                    if (value<min) min=value;
                    if (value>max) max=value;
                }
            }
        }
        
        /* Now fill out the ticks separator record */
        double binwidth=(max-min)/nbins;
        for (unsigned int i=0;i<=nbins;i++) binbounds[i]=min+binwidth*i;
        
    } else {
        /* Read the bins from the list */
        eval_listtodoublearray(userbinlist, binbounds, nbins+1);
    }
    
    /* Now perform the counts */
    for (unsigned int i=0; i<=nbins; i++) bincounts[i]=0;
    
    /* Loop over data */
    if (data->list) for (linkedlistentry *e=data->list->first; e!=NULL; e=e->next) {
        expression *exp = (expression *) e->data;
        double value;
        
        if (eval_isreal(exp)) {
            value=eval_floatvalue(exp);
            /* Add one to the correct bin */
            for (unsigned int i=0; i<nbins; i++) {
                if ((value>=binbounds[i])&&(value<=binbounds[i+1])) {
                    bincounts[i]++;
                    break;
                }
            }
        }
    }
}

expression *eval_histogram(interpretcontext *context, int nargs, expression **args) {
    /* Validate arguments */
    if (nargs<1||nargs>2) return NULL;
    
    /* Local variables */
    interpretcontext *options=NULL;
    int nbins=HISTOGRAM_DEFAULT_NBINS;
    int userbins=FALSE;
    
    if (!eval_histogram_nbins(context, nargs, args, &nbins, &userbins)) return NULL;
    
    double binbounds[nbins+1];
    unsigned int bincounts[nbins+1];
    
    eval_histogram_bincounts((expression_list *) args[0], (nargs==2 ? (expression_list *) args[1] : NULL), userbins, nbins, binbounds, bincounts);
    
    for (unsigned int i=0; i<nbins; i++) {
        printf("%g - %g : %i\n",binbounds[i],binbounds[i+1],bincounts[i]);
    }
    
eval_histogram_cleanup:
    
    if (options) freeinterpretcontext(options);
    
    return (expression *) newexpressionnone();
}

/*
 * Initialization/Finalization
 */

/* evalinitializeintrinsics - Initialize intrinsic functions.
 * Input:
 * Output:
 * Returns:
 */
void evalinitializeintrinsics(void) {
    intrinsics = hashcreate(EVAL_INTRINSICTABLESIZE);
    if (intrinsics) {
        intrinsic("return",FALSE,eval_return,NULL);
        intrinsic("if",TRUE,eval_if,NULL);
        intrinsic("for",TRUE,eval_for,NULL);
        intrinsic("do",TRUE,eval_do,NULL);
        intrinsic("while",TRUE,eval_while,NULL);
        
        intrinsic("islist",FALSE,eval_islisti, NULL);
        intrinsic("isnumber",FALSE,eval_isnumberi, NULL);
        
        intrinsic("re",FALSE,eval_re,NULL);
        intrinsic("im",FALSE,eval_im,NULL);
        intrinsic("arg",FALSE,eval_arg,NULL);
        intrinsic("abs",FALSE,eval_abs,NULL);
        intrinsic("conj",FALSE,eval_conj,NULL);
        
        intrinsic("exp",FALSE,eval_exp,eval_diffexp);
        intrinsic("log",FALSE,eval_log,NULL);
        intrinsic("sin",FALSE,eval_sin,eval_diffsin);
        intrinsic("cos",FALSE,eval_cos,eval_diffcos);
        intrinsic("sinh",FALSE,eval_sinh,NULL);
        intrinsic("cosh",FALSE,eval_cosh,NULL);
        intrinsic("tan",FALSE,eval_tan,NULL);
        intrinsic("arctan",FALSE,eval_arctan,NULL);
        intrinsic("sqrt",FALSE,eval_sqrt,NULL);
        intrinsic("mod",FALSE,eval_mod,NULL);
        
        intrinsic("root",TRUE,eval_findroot,NULL);

        intrinsic("hold",TRUE,eval_hold,NULL);
        intrinsic("eval",FALSE,eval_eval,NULL);
        intrinsic("subs",TRUE,eval_subs,NULL);
        
        intrinsic("random",FALSE,eval_rand,NULL);
        intrinsic("randomnormal",FALSE,eval_rand_normal,NULL);
        intrinsic("randominteger",FALSE,eval_rand_integer,NULL);
        
        intrinsic("tostring",FALSE,eval_tostring,NULL);
        intrinsic("stringjoin",FALSE,eval_stringjoin,NULL);
        intrinsic("stringsplit",FALSE,eval_stringsplit,NULL);
        
        intrinsic("member",FALSE,eval_listmemberi,NULL);
        intrinsic("length",FALSE,eval_length,NULL);
        intrinsic("function",TRUE,eval_function,NULL);
        intrinsic("map",TRUE,eval_map,NULL);
        intrinsic("table",TRUE,eval_table,NULL);
        intrinsic("sum",TRUE,eval_sum,NULL);
        intrinsic("join",FALSE,eval_join,NULL);
        intrinsic("sort",FALSE,eval_sort,NULL);
        
        intrinsic("min",FALSE,eval_min,NULL);
        intrinsic("max",FALSE,eval_max,NULL);
        
        intrinsic("remove",TRUE,eval_remove,NULL);
        
        intrinsic("print",FALSE,eval_print,NULL);
        intrinsic("symbols",FALSE,eval_symbols,NULL);
        intrinsic("isdefined",TRUE,eval_isdefined,NULL);
        intrinsic("intrinsics",FALSE,eval_intrinsicsi,NULL);
        intrinsic("restart",FALSE,eval_restart,NULL);
        
        intrinsic("timing",TRUE,eval_timingi,NULL);
        
        intrinsic("histogram", FALSE, eval_histogram, NULL);
    }
}

void evalfinalizeintrinsics(void) {
    if(intrinsics) {
        hashmap(intrinsics, &hashfreevalue,NULL);
        hashfree(intrinsics);
    }
}
