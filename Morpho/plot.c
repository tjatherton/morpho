/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "plot.h"

#define PLOT_LABEL "plot"

/*
 * Interface functions
 */

expression *plot_newplot(void) {
    plot_object *obj=(plot_object *) class_instantiate(class_lookup(PLOT_LABEL));
    expression_objectreference *ref=NULL;
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/*
 * Internal functions
 */

typedef enum { LEFT, RIGHT } boundtype;

double bound(double x, double y, boundtype type) {
    double integerpart, fractionalpart;
    double est;
    
    fractionalpart=modf(x/y, &integerpart);
    
    est=integerpart*y;
    
    if (type==LEFT) {
        while (est<x) est+=y;
    } else {
        while (est>x) est-=y;
    }
    
    return est;
}

void plot_determineticks(plot_ticksrecord *record, float lower, float upper) {
    float width=0.0, l=lower, u=upper;
    double integerpart;
    int order=0;
    int j;
    
    width=upper-lower;
    /* Deal with zero width */
    if (fabsf(width)<MACHINE_EPSILON) {
        record->majortickstart=lower;
        record->majortickend=upper;
        record->majortickseparation=0.0;
        record->nticks=1;
        record->digits=(int) ceil(fabs(log10(fabs(lower))))+1;
        return;
    }
    
    order=(int) log10(fabs(width));
    record->majortickseparation=pow(10.0,order);
    
    /* Figure out the separation. We want to make sure there's at least 3 and at most 6 major ticks. */
    j=0;
    while ( width/record->majortickseparation < 3 ) {
        if (j%2==0) record->majortickseparation/=2.0;
        if (j%2==1) record->majortickseparation/=5.0;
    }
    j=0;
    while ( width/record->majortickseparation > 6 ) {
        if (j%2==0) record->majortickseparation*=2.0;
        if (j%2==1) record->majortickseparation*=5.0;
    }
    
    record->majortickstart=bound(l, record->majortickseparation, LEFT);
    record->majortickend=bound(u, record->majortickseparation, RIGHT);
    
    modf((record->majortickend-record->majortickstart)/record->majortickseparation, &integerpart);
    record->nticks=1 + (int) integerpart;
    
    record->digits=(int) ceil(fabs(log10(fabs(record->majortickseparation))))+1;
    if (lower<0) record->digits+=1;
}

void plot_setaxestitles(plot_object *obj, char *x, char *y) {
    if (x) {
        obj->axeslabels[0]=EVAL_STRDUP(x);
    } else {
        if(obj->axeslabels[0]) EVAL_FREE(obj->axeslabels[0]);
        obj->axeslabels[0]=NULL;
    };

    if (y) {
        obj->axeslabels[1]=EVAL_STRDUP(y);
    } else {
        if(obj->axeslabels[1]) EVAL_FREE(obj->axeslabels[0]);
        obj->axeslabels[1]=NULL;
    };
}

static double defaultcolors[] = { 0.0, 0.0, 1.0, 1.0, 0.0, 0.0 };
static unsigned int defaultcolorslength = 2;

void plot_setcolorfromoptions(plot_object *obj, interpretcontext *context,interpretcontext *options, char *label, double *col, int cnt) {
    unsigned int def = cnt % defaultcolorslength;
    expression_objectreference *cref=NULL;
    
    if (col) {
        for (unsigned int i=0; i<3; i++) col[i]=defaultcolors[3*def+i];
    }
    
    if ((col)&&(options)) {
        expression_objectreference *ref = (expression_objectreference *) lookupsymbol(options, label);
        
        /* Was color defined as an optional argument? */
        if (ref) {
            if (eval_isobjectref(ref)) {
                cref=ref;
            } else if (eval_islist(ref)) {
                /* If it's a list get it from the list */
                linkedlist *lst = ((expression_list *) ref)->list;
                linkedlistentry *e=NULL;
                if (lst) e=linkedlist_element(lst, cnt);
                if (e) cref=(expression_objectreference *) e->data;
            }
            
            if (eval_isobjectref(cref)) {
                double ncol[3];
            
                /* If so, call the selectors and if they produce something meaningful, go with it. */
                if (color_rgbfromobject(context, cref->obj, 0, NULL, ncol)) {
                    for (int i=0;i<3;i++) col[i]=ncol[i];
                }
            }
        }
    }
    graphics_setcolor((graphics_object *) obj,col[0],col[1],col[2]);
}

void plot_defaultparameters(plot_object *obj) {
    plot_setaxestitles(obj, NULL, NULL);
    
    /* Determine the bounding box */
    graphics_calculatebbox((graphics_object *) obj, &obj->plotrange);
    
    /* Work out the default ticks for this bounding box */
    plot_determineticks(&obj->ticks[0],obj->plotrange.p1[0],obj->plotrange.p2[0]);
    plot_determineticks(&obj->ticks[1],obj->plotrange.p1[1],obj->plotrange.p2[1]);
    
    /* Correct singular ranges */
    for (unsigned int i=0; i<2; i++) {
        if (fabsf(obj->plotrange.p2[i]-obj->plotrange.p1[i])<MACHINE_EPSILON) {
            float w = 0.1*obj->plotrange.p1[i];
            if (fabsf(w)<MACHINE_EPSILON) w = 1;
            obj->plotrange.p1[i]-=w;
            obj->plotrange.p2[i]+=w;
        }
    }
}

#define PLOT_LABELHEIGHT 10.0

void plot_computetransformation(plot_object *obj, plot_transformation *transform) {
    graphics_bbox gbbox = {{0.0,0.0},{245.0,0.0}}; /* 3.5 inch 3/4 aspect ratio */
    float axestitlesize[2]={0.0,0.0};
    
    /* Enforce the desired aspect ratio */
    gbbox.p2[1]=obj->aspectratio*gbbox.p2[0];
    
    transform->overall_bbox=transform->plotregion_bbox=gbbox;
    
    if (obj->axeslabels[0]) axestitlesize[0]=PLOT_LABELHEIGHT;
    if (obj->axeslabels[1]) axestitlesize[1]=PLOT_LABELHEIGHT;
    
    /* Only leave space on the left if the axis is on the left */
    if (!((obj->plotrange.p1[0]<0)&&(obj->plotrange.p2[0]>0))||(obj->framed==TRUE)) transform->plotregion_bbox.p1[0]+=10.0*obj->ticks[1].digits;
    
    /* Leave space on the bottom if the axis is on the bottom */
    if (!((obj->plotrange.p1[1]<0)&&(obj->plotrange.p2[1]>0))||(obj->framed==TRUE)) transform->plotregion_bbox.p1[1]+=10.0;
    
    /* Left axis title */
    transform->plotregion_bbox.p1[0]+=axestitlesize[1];
    
    /* Bottom axis title */
    transform->plotregion_bbox.p1[1]+=axestitlesize[0];
    
    /* Ensure there's enough room for the plot ink or frame */
    transform->plotregion_bbox.p1[0]+=1.0;
    transform->plotregion_bbox.p1[1]+=1.0;
    transform->plotregion_bbox.p2[0]-=1.0;
    transform->plotregion_bbox.p2[1]-=1.0;
    
    for (int i=0;i<2;i++) {
        if (fabsf(obj->plotrange.p2[i]-obj->plotrange.p1[i])<MACHINE_EPSILON) {
            printf("Warning: Uncorrected singular range.\n");
        }
        transform->m[i]=(transform->plotregion_bbox.p2[i]-transform->plotregion_bbox.p1[i])/(obj->plotrange.p2[i]-obj->plotrange.p1[i]);
        transform->c[i]=(obj->plotrange.p1[i]*transform->plotregion_bbox.p2[i]-obj->plotrange.p2[i]*transform->plotregion_bbox.p1[i])/(obj->plotrange.p1[i]-obj->plotrange.p2[i]);
    }
}

void plot_transformpoint(float *p, plot_transformation *transform) {
    p[0]=p[0]*transform->m[0]+transform->c[0];
    p[1]=p[1]*transform->m[1]+transform->c[1];
}

void plot_axes(plot_object *obj, graphics_object *graphics_obj, plot_transformation *transform) {
    char buffer[128];
    float p[2];
    
    graphics_linewidth(graphics_obj,0.5);
    graphics_setcolor(graphics_obj,0.0,0.0,0.0);
    
    /* x ticks */
    for (int i=0;i<obj->ticks[0].nticks;i++) {
        p[0]=obj->ticks[0].majortickstart+i*(obj->ticks[0].majortickseparation);
        
        /* If zero is included in the range, make it an axis. */
        if ((obj->plotrange.p1[1]<0)&&(obj->plotrange.p2[1]>0)&&(!obj->framed)) p[1]=0.0;
        else p[1]=obj->plotrange.p1[1];
        
        plot_transformpoint(p,transform);
        graphics_moveto(graphics_obj, p);
        p[1]+=4;
        graphics_lineto(graphics_obj, p);
        graphics_stroke(graphics_obj);
    
        p[1]-=4;
        sprintf(buffer,"%g",obj->ticks[0].majortickstart+i*(obj->ticks[0].majortickseparation));
        graphics_text(graphics_obj, buffer, p, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNTOP);
    }
    
    for (int i=0;i<obj->ticks[1].nticks;i++) {
        
        /* If zero is in the plot range, make it the axis */
        if ((obj->plotrange.p1[0]<0)&&(obj->plotrange.p2[0]>0)&&(!obj->framed)) p[0]=0.0;
        else p[0]=obj->plotrange.p1[0];
        
        p[1]=obj->ticks[1].majortickstart+i*(obj->ticks[1].majortickseparation);
        
        plot_transformpoint(p,transform);
        graphics_moveto(graphics_obj, p);
        p[0]+=4;
        graphics_lineto(graphics_obj, p);
        graphics_stroke(graphics_obj);
        
        p[0]-=6;
        sprintf(buffer,"%g",obj->ticks[1].majortickstart+i*(obj->ticks[1].majortickseparation));
        graphics_text(graphics_obj, buffer, p, GRAPHICS_ALIGNRIGHT, GRAPHICS_ALIGNMIDDLE);
    }
    
    /* Horizontal axis */
    p[0]=obj->plotrange.p1[0];
    if ((obj->plotrange.p1[1]<0)&&(obj->plotrange.p2[1]>0)) p[1]=0.0;
    else p[1]=obj->plotrange.p1[1];
    plot_transformpoint(p,transform);
    graphics_moveto(graphics_obj, p);
    p[0]=transform->plotregion_bbox.p2[0];
    graphics_lineto(graphics_obj, p);
    graphics_stroke(graphics_obj);
    
    /* Vertical axis */
    if ((obj->plotrange.p1[0]<0)&&(obj->plotrange.p2[0]>0)) p[0]=0.0;
    else p[0]=obj->plotrange.p1[0];
    p[1]=obj->plotrange.p1[1];
    plot_transformpoint(p,transform);
    graphics_moveto(graphics_obj, p);
    p[1]=transform->plotregion_bbox.p2[1];
    graphics_lineto(graphics_obj, p);
    graphics_stroke(graphics_obj);
    
    /* Frame */
    if (obj->framed) {
        p[0]=obj->plotrange.p1[0]; // Lower left
        p[1]=obj->plotrange.p1[1];
        plot_transformpoint(p,transform);
        graphics_moveto(graphics_obj, p);
        
        p[0]=obj->plotrange.p2[0];  // Lower right
        p[1]=obj->plotrange.p1[1];
        plot_transformpoint(p,transform);
        graphics_lineto(graphics_obj, p);
        
        p[0]=obj->plotrange.p2[0];  // Upper right
        p[1]=obj->plotrange.p2[1];
        plot_transformpoint(p,transform);
        graphics_lineto(graphics_obj, p);
        
        p[0]=obj->plotrange.p1[0];  // Upper left
        p[1]=obj->plotrange.p2[1];
        plot_transformpoint(p,transform);
        graphics_lineto(graphics_obj, p);
        
        graphics_closepath(graphics_obj);
        graphics_stroke(graphics_obj);
    }
    
    /* Horizontal axis label */
    if (obj->axeslabels[0]) {
        p[0]=(transform->plotregion_bbox.p1[0]+transform->plotregion_bbox.p2[0])*0.5;
        p[1]=1.0;
        graphics_text(graphics_obj, obj->axeslabels[0], p, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNBOTTOM);
    }
    
    /* Vertical axis label */
    if (obj->axeslabels[1]) {
        p[0]=6.0;
        p[1]=(transform->plotregion_bbox.p1[1]+transform->plotregion_bbox.p2[1])*0.5;
        graphics_savestate(graphics_obj);
        graphics_translate(graphics_obj, p);
        graphics_rotate(graphics_obj, 90.0);
        p[0]=0.0; p[1]=0.0;
        graphics_text(graphics_obj, obj->axeslabels[1], p, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNMIDDLE);
        graphics_restorestate(graphics_obj);
    }
}

void plot_transformgraphics(plot_object *src, graphics_object *dest, plot_transformation *transform) {
    graphics_element *el = NULL;
    graphics_element *new = NULL;
    
    for (linkedlistentry *e=src->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        
        if (el) {
            new=graphics_cloneentry(el);
            if (new) {
                switch (new->type) {
                    case GRAPHICS_MOVETO:
                    case GRAPHICS_LINETO:
                    case GRAPHICS_POINT:
                        {
                            graphics_pointelement *pel = (graphics_pointelement *) new;
                            plot_transformpoint(pel->point,transform);
                        }
                        break;
                        
                    case GRAPHICS_TEXT:
                        plot_transformpoint(((graphics_textelement *) new)->point,transform);
                        break;
                    
                    case GRAPHICS_SHADE_LATTICE:
                        {
                            graphics_shadelatticeelement *sel=(graphics_shadelatticeelement *) new;
                            unsigned int size=sel->size[0]*sel->size[1];
                            for (int i=0;i<size;i++) plot_transformpoint(&sel->data[i*5],transform);
                        }
                        break;
                        
                    default:
                        /* Leave the rest alone */
                        break;
                }
                
                linkedlist_addentry(dest->list, new);
            }
        }
    }
}

expression *plot_tographics(plot_object *obj) {
    expression_objectreference *graphics=NULL;
    plot_transformation transform;
    
    /* Compute the transformation matrix */
    plot_computetransformation(obj, &transform);
    
    graphics = (expression_objectreference *) graphics_newgraphics();
    if (graphics) {
        graphics_object *graphics_obj=(graphics_object *) graphics->obj;
        
        /* Enforce the bounding box */
        //graphics_forcebbox(graphics_obj, &transform.overall_bbox);
        
        /* Surround plot_transformgraphics with save and restore state */
        graphics_savestate(graphics_obj);
        plot_transformgraphics(obj, graphics_obj, &transform);
        graphics_restorestate(graphics_obj);
        
        plot_axes(obj, graphics_obj, &transform);
    }
    
    return (expression *) graphics;
}

/* 
 * Selectors
 */

expression *plot_tographicsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    return plot_tographics((plot_object *) obj);
}

expression *plot_exporti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_objectreference *ref=(expression_objectreference *) plot_tographics((plot_object *) obj);
    
    if (ref) {
        freeexpression(class_callselectorwithargslist(context, ref->obj, EVAL_EXPORTLABEL, nargs, args));
    }
    
    return (expression *) ref;
}

expression *plot_setaspectratioi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((obj!=NULL)&&(nargs==1)) {
        if (eval_isnumerical(args[0])) ((plot_object *) obj)->aspectratio=eval_floatvalue(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *plot_getaspectratioi(object *obj, interpretcontext *context, int nargs, expression **args) {
    return (expression *) newexpressionfloat((double) ((plot_object *) obj)->aspectratio);
}

expression *plot_setframedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((obj!=NULL)&&(nargs==1)) {
        if (eval_isbool(args[0])) ((plot_object *) obj)->framed=eval_boolvalue(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *plot_getframedi(object *obj, interpretcontext *context, int nargs, expression **args) {    
    return (expression *) newexpressionbool(((plot_object *) obj)->framed);
}

expression *plot_setplotrangei(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot_object *pobj=(plot_object *) obj;
    float xrange[2],yrange[2];
    expression_list *exp = (expression_list *) args[0];
    
    /* Make sure we're passed a list */
    if (nargs==1) if (exp) if (exp->type==EXPRESSION_LIST) {
        /* If the first element is numerical, assume this is the yrange */
        if (eval_isnumerical(((expression *) (exp->list->first->data)))) {
            /* Read the list into a float array */
            if (eval_listtofloatarray((expression_list *) args[0], yrange, 2)) {
                pobj->plotrange.p1[1]=yrange[0];
                pobj->plotrange.p2[1]=yrange[1];
            }
        } else if (eval_islist(((expression *) (exp->list->first->data)))) {
            /* Otherwise, set both xrange and yrange */
            expression_list *list1 = (expression_list *) eval_getelement(exp, 1);
            expression_list *list2 = (expression_list *) eval_getelement(exp, 2);
            
            if ((list1)&&(list2)&&(list1->type==EXPRESSION_LIST)&&(list2->type==EXPRESSION_LIST)) {
                /* Read the list into a float array */
                if (eval_listtofloatarray(list1, xrange, 2) &&
                    eval_listtofloatarray(list2, yrange, 2)) {
                    pobj->plotrange.p1[0]=xrange[0];
                    pobj->plotrange.p2[0]=xrange[1];
                    
                    pobj->plotrange.p1[1]=yrange[0];
                    pobj->plotrange.p2[1]=yrange[1];
                }
            }
        }
    }
    
    /* Update ticks record */
    plot_determineticks(&pobj->ticks[0], pobj->plotrange.p1[0], pobj->plotrange.p2[0]);
    plot_determineticks(&pobj->ticks[1], pobj->plotrange.p1[1], pobj->plotrange.p2[1]);
    
    return (expression *) class_newobjectreference(obj);
}

expression *plot_getplotrangei(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot_object *pobj=(plot_object *) obj;
    expression_list *xrange=NULL;
    expression_list *yrange=NULL;
    
    xrange=newexpressionlistfromargs(2,newexpressionfloat(pobj->plotrange.p1[0]),newexpressionfloat(pobj->plotrange.p2[0]));
    yrange=newexpressionlistfromargs(2,newexpressionfloat(pobj->plotrange.p1[1]),newexpressionfloat(pobj->plotrange.p2[1]));
    
    return (expression *) newexpressionlistfromargs(2,xrange,yrange);
}

/* Default values */
expression *plot_new(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot_object *pobj=(plot_object *) obj;
    pobj->list=linkedlist_new();
    pobj->axeslabels[0]=NULL;
    pobj->axeslabels[1]=NULL;
    pobj->aspectratio=0.75;
    pobj->framed=FALSE;
    pobj->plotrange.p1[0]=0.0;
    pobj->plotrange.p1[1]=0.0;
    pobj->plotrange.p2[0]=1.0;
    pobj->plotrange.p2[1]=1.0;
    
    plot_determineticks(&pobj->ticks[0], pobj->plotrange.p1[0], pobj->plotrange.p2[0]);
    plot_determineticks(&pobj->ticks[1], pobj->plotrange.p1[1], pobj->plotrange.p2[1]);
    return NULL;
}

expression *plot_free(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (context==NULL) {
        plot_object *pobj=(plot_object *) obj;
        if (pobj) {
            if (pobj->axeslabels[0]) EVAL_FREE(pobj->axeslabels[0]);
            if (pobj->axeslabels[1]) EVAL_FREE(pobj->axeslabels[1]);
            graphics_freei(obj,context,nargs,args);
        }
    }
    return NULL;
}

/* 
 * Intrinsic functions to generate various kinds of plot.
 */

expression *plot_plot(interpretcontext *context, int nargs, expression **args) {
    /* Initial validation*/
    if (nargs<2) return NULL;
    if (args[1]->type!=EXPRESSION_LIST) return NULL;
    
    interpretcontext *options=NULL;
    expression_objectreference *plot=(expression_objectreference *) plot_newplot();
    plot_object *plot_obj=NULL;
    interpretcontext *plotctxt=newinterpretcontext(context, 1);
    floatcountertype *counter=NULL;
    int somenumericals=FALSE;
    int needmoveto=TRUE;
    int success=FALSE;
    double col[3]={0,0,0};
    
    if ((!plotctxt)||(!plot)) goto plot_plot_cleanup;
    plot_obj=(plot_object *) plot->obj;
    
    options=class_options((object *) plot_obj,context,3,nargs,args);
    
    counter=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[1], TRUE);

    /* */
    graphics_linewidth((graphics_object *) plot_obj,1.0);
    
    /* Set color */
    plot_setcolorfromoptions(plot_obj, context, options, "color", col, 0);
    
    /* Evaluate the function at all points */
    eval_counterreset((genericcountertype *)counter);
    do {
        expression *ret=NULL;
        
        eval_counterstore(plotctxt, (genericcountertype *) counter);
        ret=interpretexpression(plotctxt, args[0]);

        if (eval_isnumerical(ret)) {
            somenumericals=TRUE;
            float p[2] = {counter->value, eval_floatvalue(ret)};

            if (needmoveto) { graphics_moveto((graphics_object *) plot->obj, p); needmoveto=FALSE; }
            else graphics_lineto((graphics_object *) plot->obj, p);
        } /* TODO: Deal with non-numerical values; poles, etc. */
        
        if (ret) freeexpression(ret);
    } while (!eval_counteradvance((genericcountertype *) counter));
    
    if (!somenumericals) {
        printf("Expression evaluated to nonnumerical values everywhere.\n");
        goto plot_plot_cleanup;
    } else {
        graphics_stroke((graphics_object *) plot_obj);
    }
    
    plot_defaultparameters(plot_obj);
    plot_setaxestitles(plot_obj, counter->name->name, NULL);
    /* Reprocess options */
    freeinterpretcontext(class_options((object *) plot_obj,context,3,nargs,args));
    success=TRUE;
    
plot_plot_cleanup:
    if (counter) EVAL_FREE(counter);
    if (plotctxt) freeinterpretcontext(plotctxt);
    if (!success) if (plot) { freeexpression((expression *) plot); plot=NULL; }
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}

expression *plot_parametricplot(interpretcontext *context, int nargs, expression **args) {
    /* Initial validation*/
    if (nargs<2) return NULL;
    if (args[1]->type!=EXPRESSION_LIST) return NULL;
    
    interpretcontext *options=NULL;
    expression_objectreference *plot=(expression_objectreference *) plot_newplot();
    plot_object *plot_obj=NULL;
    interpretcontext *plotctxt=newinterpretcontext(context, 1);
    floatcountertype *counter=NULL;
    int somenumericals=FALSE;
    int needmoveto=TRUE;
    int success=FALSE;
    double col[3]={0,0,0};
    
    if ((!plotctxt)||(!plot)) goto plot_plot_cleanup;
    plot_obj=(plot_object *) plot->obj;
    
    options=class_options((object *) plot_obj,context,3,nargs,args);

    counter=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[1], TRUE);
    
    /* */
    graphics_linewidth((graphics_object *) plot_obj,1.0);

    /* Set color */
    plot_setcolorfromoptions(plot_obj, context, options, "color", col, 0);
    
    /* Evaluate the function at all points */
    eval_counterreset((genericcountertype *)counter);
    do {
        expression *ret=NULL;
        
        eval_counterstore(plotctxt, (genericcountertype *) counter);
        ret=interpretexpression(plotctxt, args[0]);
        
        if (ret->type==EXPRESSION_LIST) {
            float p[2];
            
            if (eval_listtofloatarray((expression_list *) ret, p, 2)) {
                somenumericals=TRUE;
            
                if (needmoveto) { graphics_moveto((graphics_object *) plot->obj, p); needmoveto=FALSE; }
                else graphics_lineto((graphics_object *) plot->obj, p);
            }
        }
        
        if (ret) freeexpression(ret);
    } while (!eval_counteradvance((genericcountertype *) counter));
    
    if (!somenumericals) {
        printf("Expression evaluated to nonnumerical values everywhere.\n");
        goto plot_plot_cleanup;
    } else {
        graphics_stroke((graphics_object *) plot_obj);
    }
    
    plot_defaultparameters(plot_obj);
    /* Reprocess options */
    freeinterpretcontext(class_options((object *) plot_obj,context,3,nargs,args));
    success=TRUE;
    
plot_plot_cleanup:
    if (counter) EVAL_FREE(counter);
    if (plotctxt) freeinterpretcontext(plotctxt);
    if (!success) if (plot) freeexpression((expression *) plot);
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}


int plot_listplotlist(plot_object *plot_obj, interpretcontext *context, interpretcontext *options, expression_list *list, int *sn, int pltcnt) {
    float p[2],q[2],w[2];
    double col[3]={0,0,0};
    unsigned int count=0;
    unsigned int cnt=pltcnt;
    int somenumericals=FALSE;
    
    /* Set color */
    plot_setcolorfromoptions(plot_obj, context, options, "color", col, pltcnt);
    
    count=0;
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        expression *value=(expression *) e->data;
        
        if (value) {
            if (eval_isnumerical(value)) {
                p[0]=(float) count; p[1]=(float) eval_floatvalue(value);
                
                somenumericals=TRUE;
                graphics_point((graphics_object *) plot_obj, p);
                count++;
            } else if (eval_islist(value)) {
                if ((eval_listlength((expression_list *) value)<=4) && eval_listtofloatarray((expression_list *) value, p, 2)) {
                    somenumericals=TRUE;
                    
                    switch (eval_listlength((expression_list *) value)) {
                        case 3:
                        {
                            /* Error bar of form {x,y, dy} */
                            expression *errbar=eval_getelement((expression_list *) value, 3);
                            
                            if (errbar) {
                                w[0]=eval_floatvalue(errbar);
                                if (!isnan(w[0])) {
                                    q[0]=p[0];
                                    q[1]=p[1]-w[0];
                                    graphics_moveto((graphics_object *) plot_obj, q);
                                    q[1]=p[1]+w[0];
                                    graphics_lineto((graphics_object *) plot_obj, q);
                                    graphics_linewidth((graphics_object *) plot_obj, 0.5);
                                    graphics_stroke((graphics_object *) plot_obj);
                                }
                            }
                        }
                            break;
                        case 4:
                        {
                            /* Error bar of form {x,y, ylower, yupper} */
                            expression *errbar=eval_getelement((expression_list *) value, 3);
                            expression *errbar2=eval_getelement((expression_list *) value, 4);
                            
                            if (errbar&&errbar2) {
                                w[0]=eval_floatvalue(errbar);
                                w[1]=eval_floatvalue(errbar2);
                                if ((!isnan(w[0]))&&(!isnan(w[1]))) {
                                    q[0]=p[0];
                                    q[1]=w[0];
                                    graphics_moveto((graphics_object *) plot_obj, q);
                                    q[1]=w[1];
                                    graphics_lineto((graphics_object *) plot_obj, q);
                                    graphics_linewidth((graphics_object *) plot_obj, 0.5);
                                    graphics_stroke((graphics_object *) plot_obj);
                                }
                            }
                        }
                            break;
                        default:
                            break;
                    }
                    
                    graphics_point((graphics_object *) plot_obj, p);
                    count++;
                } else {
                    /* Otherwise, we're in a list of list */
                    count+=plot_listplotlist(plot_obj, context, options, (expression_list *) value, &somenumericals, cnt);
                    cnt++;
                }
            }
        }
    }
    
    if (sn) *sn |= somenumericals;
    
    return count;
}

expression *plot_listplot(interpretcontext *context, int nargs, expression **args) {
    /* Initial validation*/
    if (nargs<1) return NULL;
    if ((args[0]==NULL)||(args[0]->type!=EXPRESSION_LIST)) return NULL;
    
    interpretcontext *options=NULL;
    expression_objectreference *plot=(expression_objectreference *) plot_newplot();
    expression_list *list=(expression_list *) args[0];
    plot_object *plot_obj=NULL;
    int somenumericals=FALSE;
    int success=FALSE;
    unsigned int count=0;
    
    if (!plot) goto plot_listplot_cleanup;
    plot_obj=(plot_object *) plot->obj;
    
    options=class_options((object *) plot_obj,context,2,nargs,args);
    
    count = plot_listplotlist(plot_obj, context, options, list, &somenumericals, 0);
    
    if (!somenumericals) {
        printf("Expression evaluated to nonnumerical values everywhere.\n");
        goto plot_listplot_cleanup;
    }
    
    plot_defaultparameters(plot_obj);
    /* Reprocess options */
    freeinterpretcontext(class_options((object *) plot_obj,context,3,nargs,args));
    success=TRUE;
    
plot_listplot_cleanup:
    if (!success) if (plot) { freeexpression((expression *) plot); plot=NULL; }
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}

expression *plot_densityplot(interpretcontext *context, int nargs, expression **args) {
    /* Initial validation*/
    if (nargs<3) return NULL;
    if ((args[1]->type!=EXPRESSION_LIST)||(args[2]->type!=EXPRESSION_LIST)) return NULL;
    
    /* Local variables */
    interpretcontext *options=NULL;
    expression_objectreference *plot=(expression_objectreference *) plot_newplot();
    expression_objectreference *colormap=NULL;
    plot_object *plot_obj=NULL;
    interpretcontext *plotctxt=newinterpretcontext(context, 1);
    floatcountertype *counter[2]= {NULL,NULL};
    expression *ret=NULL;
    float *data;
    float min, max,range;
    unsigned int k;
    int success=FALSE;
    double col[3];
    
    if ((!plotctxt)||(!plot)) goto plot_densityplot_cleanup;
    plot_obj=(plot_object *) plot->obj;
    
    options=class_options((object *) plot_obj,context,4,nargs,args);
    
    /* Interpret the counter arguments */
    counter[0]=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[1], TRUE);
    counter[1]=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[2], TRUE);
    if (!counter[0]||!counter[1]) goto plot_densityplot_cleanup;
    
    /* Create the appropriate shade lattice */
    data = graphics_shadelattice((graphics_object *) plot->obj, counter[0]->nsteps, counter[1]->nsteps);
    if (!data) goto plot_densityplot_cleanup;
    
    k=0; min=0;max=0;
    eval_counterreset((genericcountertype *)counter[1]);
    /* Evaluate the function at all points */
    do {
        eval_counterstore(plotctxt, (genericcountertype *) counter[1]);

        eval_counterreset((genericcountertype *)counter[0]);
        do {
            eval_counterstore(plotctxt, (genericcountertype *) counter[0]);
            
            ret=interpretexpression(plotctxt, args[0]);
            if (eval_isnumerical(ret)) {
                float value=eval_floatvalue(ret);
                data[k*5]  =(float) counter[0]->value;
                data[k*5+1]=(float) counter[1]->value;
                data[k*5+2]=value;
                if (k==0) {
                    min=value; max=value;
                } else {
                    if (value<min) min=value;
                    if (value>max) max=value;
                }
            }
        
            if (ret) freeexpression(ret);
            k++;
        } while (!eval_counteradvance((genericcountertype *) counter[0]));
        
    } while (!eval_counteradvance((genericcountertype *) counter[1]));
    
    /* Look up colormap */
    colormap = (expression_objectreference *) lookupsymbol(options, "colormap");
    
    /* Rescale plot values and color */
    range=max-min;
    for (int i=0; i<k; i++) {
        float value=data[i*5+2];
        data[i*5+2]=(value-min)/range;
        data[i*5+3]=data[i*5+2];
        data[i*5+4]=data[i*5+2];
        
        if (colormap) {
            expression *val =newexpressionfloat((double) data[i*5+2]);
            if (val) {
                if (color_rgbfromobject(context, colormap->obj, 1, &val, col)) {
                    data[i*5+2]=(float) col[0];
                    data[i*5+3]=(float) col[1];
                    data[i*5+4]=(float) col[2];
                }
                freeexpression(val);
            }
        }
    }
    
    /* Must free our reference to the colormap */
    if (colormap) freeexpression((expression *) colormap);
    
    plot_defaultparameters(plot_obj);
    plot_setaxestitles(plot_obj, counter[0]->name->name, counter[1]->name->name);
    plot_obj->framed=TRUE;
    /* Reprocess options */
    freeinterpretcontext(class_options((object *) plot_obj,context,4,nargs,args));
    
    success=TRUE;
    
plot_densityplot_cleanup:
    if (counter[0]) EVAL_FREE(counter[0]);
    if (counter[1]) EVAL_FREE(counter[1]);
    if (plotctxt) freeinterpretcontext(plotctxt);
    if (!success) if (plot) freeexpression((expression *) plot);
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}

expression *plot_histogram(interpretcontext *context, int nargs, expression **args) {
    /* Validate arguments */
    if (nargs<1||nargs>2) return NULL;
    
    /* Local variables */
    interpretcontext *options=NULL;
    expression_objectreference *plot=NULL;
    plot_object *plotobj=NULL;
    double col[3]={0,0,0};
    int nbins=HISTOGRAM_DEFAULT_NBINS;
    int userbins=FALSE;
    
    if (!eval_histogram_nbins(context, nargs, args, &nbins, &userbins)) return NULL;
    
    double binbounds[nbins+1];
    unsigned int bincounts[nbins+1];
    
    eval_histogram_bincounts((expression_list *) args[0], (nargs==2 ? (expression_list *) args[1] : NULL), userbins, nbins, binbounds, bincounts);
    
    plot=(expression_objectreference *) plot_newplot();
    if (!plot) goto plot_histogram_cleanup;
    
    plotobj=(plot_object *) plot->obj;
    options=class_options((object *) plotobj,context,2,nargs,args);
    
    /* Set color */
    plot_setcolorfromoptions(plotobj, context, options, "color", col, 0);
    
    /* Draw the rectangles */
    if (plot) {
        float pt[2];
        
        for (unsigned int i=0; i<nbins; i++) {
            if (bincounts[i]>0) {
                /* Draw the rectangle */
                pt[0]=binbounds[i]; pt[1]=0.0;
                graphics_moveto((graphics_object *) plotobj, pt);
                pt[0]=binbounds[i+1];
                graphics_lineto((graphics_object *) plotobj, pt);
                pt[1]=(float) bincounts[i];
                graphics_lineto((graphics_object *) plotobj, pt);
                pt[0]=binbounds[i];
                graphics_lineto((graphics_object *) plotobj, pt);
                pt[1]=0.0;
                graphics_lineto((graphics_object *) plotobj, pt);
                
                graphics_setcolor((graphics_object *) plotobj, col[0], col[1], col[2]);
                graphics_fill((graphics_object *) plotobj);
            }
        }
        
        plot_defaultparameters(plotobj);
    }
    
plot_histogram_cleanup:
    
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}

hashtable *plot_ticksrecordtohashtableobject(plot_ticksrecord *record) {
    hashtable *ht = hashcreate(PLOT_TICKSRECORD_NENTRIES);

    
    if (ht) {
        hashinsert(ht, "digits", newexpressioninteger(record->digits));
        hashinsert(ht, "nticks", newexpressioninteger(record->nticks));
        hashinsert(ht, "majortickstart", newexpressionfloat(record->majortickstart));
        hashinsert(ht, "majortickend", newexpressionfloat(record->majortickend));
        hashinsert(ht, "majortickseparation", newexpressionfloat(record->majortickseparation));
    }
    
    return ht;
}

expression *plot_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot_object *gobj = (plot_object *) obj;
    expression_objectreference *newref = (expression_objectreference *) plot_newplot();
    
    if (newref) {
        plot_object *nobj = (plot_object *) newref->obj;
        
        if (nobj) {
            nobj->aspectratio=gobj->aspectratio;
            for (unsigned int i=0; i<2; i++) {
                nobj->axeslabels[i]=gobj->axeslabels[i];
                if (nobj->axeslabels[i]) nobj->axeslabels[i]=EVAL_STRDUP(nobj->axeslabels[i]);
                nobj->ticks[i]=gobj->ticks[i];
            }
            nobj->framed=gobj->framed;
            nobj->plotrange=gobj->plotrange;
        }
        
        if (gobj->list) {
            for (linkedlistentry *e=gobj->list->first; e!=NULL; e=e->next) {
                graphics_element *el = (graphics_element *) e->data;
                
                linkedlist_addentry(nobj->list, graphics_cloneentry(el));
            }
        }
    }
    
    return (expression *) newref;
}

expression *plot_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(6);
    plot_object *pobj=(plot_object *) obj;
    expression *ret=NULL;
    hashtable *tickht=NULL;
    
    if (ht) {
        hashinsert(ht, "aspectratio", newexpressionfloat((double) pobj->aspectratio));
        hashinsert(ht, "axeslabels", newexpressionlistfromargs(2, newexpressionstring(pobj->axeslabels[0]), newexpressionstring(pobj->axeslabels[1])));
        hashinsert(ht, "framed", newexpressionbool(pobj->framed));
        hashinsert(ht, "plotrange", graphics_bboxtolist(&pobj->plotrange));
        
        tickht=plot_ticksrecordtohashtableobject(&pobj->ticks[0]);
        hashinsert(ht, "xticks", newexpressionlistfromhashtable(tickht));
        hashmap(tickht, evalhashfreeexpression, NULL);
        hashfree(tickht);
        
        tickht=plot_ticksrecordtohashtableobject(&pobj->ticks[1]);
        hashinsert(ht, "yticks", newexpressionlistfromhashtable(tickht));
        hashmap(tickht, evalhashfreeexpression, NULL);
        hashfree(tickht);
        
        graphics_serializetohashtable((graphics_object *) obj, ht);
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
    }
    
    return ret;
}

void plotinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(PLOT_LABEL, class_lookup(GRAPHICS_LABEL), sizeof(plot_object), 9);
    class_registerselector(cls, "tographics", FALSE, plot_tographicsi);
    class_registerselector(cls, "export", FALSE, plot_exporti);
    
    class_registerselector(cls, "setaspectratio", FALSE, plot_setaspectratioi);
    class_registerselector(cls, "aspectratio", FALSE, plot_getaspectratioi);
    class_registerselector(cls, "setframed", FALSE, plot_setframedi);
    class_registerselector(cls, "framed", FALSE, plot_getframedi);
    class_registerselector(cls, "setplotrange", FALSE, plot_setplotrangei);
    class_registerselector(cls, "plotrange", FALSE, plot_getplotrangei);
    
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, plot_serializei);
    class_registerselector(cls, EVAL_CLONE, FALSE, plot_clonei);
    
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, plot_new);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, plot_free);
    
    intrinsic(PLOT_LABEL, TRUE, plot_plot,NULL);
    intrinsic("plotdensity", TRUE, plot_densityplot,NULL);
    intrinsic("plotparametric", TRUE, plot_parametricplot,NULL);
    intrinsic("plotlist", FALSE, plot_listplot,NULL);
    intrinsic("plothistogram", FALSE, plot_histogram,NULL);
    
}
