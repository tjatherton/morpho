/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * constants.h - Implements constants.
 */

#ifndef Morpho_constants_h
#define Morpho_constants_h

#include "eval.h"

#define PI 3.1415926535897932

extern hashtable *constants;

void constant_insert(char *label, expression *exp);

void evalinitializeconstants(void);
void evalfinalizeconstants(void);

#endif
