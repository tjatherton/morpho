/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * linkedlist.h - Implements a linked list data structure.
 */

#ifndef Morpho_linkedlist_h
#define Morpho_linkedlist_h

#include <stdlib.h>

typedef struct llistentry_s {
    struct llistentry_s *next;
    void *data;
} linkedlistentry;

typedef struct {
    linkedlistentry *first;
    linkedlistentry *last;
} linkedlist;

#include "eval.h"

typedef void linkedlistmapfunction (void*);

linkedlist *linkedlist_new(void);
linkedlistentry *linkedlist_addentry(linkedlist *list, void *data);
void linkedlist_removeentry(linkedlist *list, void *data);
unsigned int linkedlist_position(linkedlist *list, void *data);
unsigned int linkedlist_length(linkedlist *list);
linkedlistentry *linkedlist_element(linkedlist *list, unsigned int i);
int linkedlist_free(linkedlist *list);
void linkedlist_map(linkedlist *list, linkedlistmapfunction *func);


#endif
