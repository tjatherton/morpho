/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "eval.h"

char *eval_strdup(char *string) {
    size_t len;
    char *new=NULL;
    
    if (string) {
        len = strlen(string)+1;
        new=EVAL_MALLOC(len);
        if (new) {
            strncpy(new, string, len);
            new[len-1]='\0';
        }
    }
    
    return new;
}

/*
 * Initialization functions
 */

/* Global variables 
 * These are the global stores of information used by the whole evaluator.
 */
interpretcontext *defaultinterpretcontext;
expression parseeofexp = { EXPRESSION_NONE };

/* evalinitialize
 * This routine allocates global stores and calls other initialization routines to set up the grammar, etc.
 */
void evalinitialize(void) {
    defaultinterpretcontext = newinterpretcontext(NULL,EVAL_DEFAULTCONTEXTTABLESIZE);
    
    evalinitializeoperators();
    evalinitializeintrinsics();
    evalinitializeconstants();
    evalinitializeclasses();
    evalinitializecalculus();
    evalinitializeerror();
}

/* 
 * Finalization functions
 */

void evalhashfreeexpression(char *name, void *exp, void *ref) {
    freeexpression((expression *) exp);
}

/* evalfinalize
 * This routine deallocates global stores and calls other finalization routines to EVAL_FREE memory.
 */
void evalfinalize(void) {
    if(defaultinterpretcontext && defaultinterpretcontext->symbols) {
        hashmap(defaultinterpretcontext->symbols, &evalhashfreeexpression,NULL);
        hashfree(defaultinterpretcontext->symbols);
    }
    
    evalfinalizeoperators();
    evalfinalizeintrinsics();
    evalfinalizeconstants();
    evalfinalizeclasses();
}

/*
 * Lexer - breaks a given string into 'words' and identifies them for the parser.
 */

/* lexchar - categorizes individual characters for the lexer.
 * Input:   (char) c    - A character to be interpreted.
 * Output:
 * Returns: (chartype)  - The categorization for the character c.
 */
chartype lexchar(char c) {
    if (c=='\"') return LEX_STRING;
    if (isalpha(c)) return LEX_ALPHA;
    if (isdigit(c)) return LEX_NUMBER;
    
    if (ispunct(c)) return LEX_OPERATOR;
    
    return LEX_NONE;
}

/* lex - breaks a given string into tokens
 * Input:   (char *) string - A pointer to where to start lexing.
 *          (int)    lbp    - Left binding power
 *          (int)    force  - Whether to force continuation or not.
 * Output:  (token *) tok   - A token record filled out.
 * Returns: (char *)        - A pointer to the next token.
 */
char *lex(char *string, token *tok, int lbp, int force) {
    unsigned int i=0,j=0;
    chartype ctype;
    tokentype ttype=TOKEN_UNRECOGNIZED;
    char *cstring=string;
    int nestedcomment=0;
    
    /* Skip leading whitespace */
    for (;(isspace(cstring[i]))&&(cstring[i]!='\0');i++);
    
    /* If this is a comment, skip to the end of the line */
    if ((cstring[i]=='/')&&(cstring[i+1]=='/')) for (;cstring[i]!='\0';i++);

    /* Also support nested multiline comments */
    if ((cstring[i]=='/')&&(cstring[i+1]=='*')) {
        nestedcomment++; i+=2;
        for (;(cstring[i]!='\0')&&(nestedcomment>0);) {
            if ((cstring[i]=='/')&&(cstring[i+1]=='*')) { nestedcomment++;i+=2; } // Down one nesting level
            else if ((cstring[i]=='*')&&(cstring[i+1]=='/')) { nestedcomment--;i+=2; } // Up one nesting level
            else i++;
        }
    }
    
    /* Skip whitespace */
    for (;(isspace(cstring[i]))&&(cstring[i]!='\0');i++);
    
    tok->lbp=lbp;
    tok->nud=NULL;
    tok->led=NULL;
    tok->data=NULL;
    tok->continued=FALSE;
    
    while (((force)&&(cstring[i]=='\0'))||(nestedcomment>0)) {
        /* Continuation... */
        
        if (tok->cfunc) {
            char *ret = tok->cfunc(tok->cdata);
            /* The continuation function successfully brought in more data */
            if (ret) {
                cstring=ret;
                tok->base=ret;
                
                /* Strip leading whitespace and check for comments as before */
                i=0;
                if (nestedcomment>0) {
                    for (;(cstring[i]!='\0')&&(nestedcomment>0);) {
                        if ((cstring[i]=='/')&&(cstring[i+1]=='*')) { nestedcomment++;i+=2; } // Down one nesting level
                        else if ((cstring[i]=='*')&&(cstring[i+1]=='/')) { nestedcomment--;i+=2; } // Up one nesting level
                        else i++;
                    }
                }
                for (;(isspace(cstring[i]))&&(cstring[i]!='\0');i++);
                
                if ((cstring[i]=='/')&&(cstring[i+1]=='/')) for (;cstring[i]!='\0';i++);
                tok->continued=TRUE;
            } else return NULL;
        }
    }
    
    if (cstring[i]=='\0') {
        ttype=TOKEN_EOF;
    } else {
        ctype=lexchar(cstring[i]);
        switch (ctype) {
            case LEX_STRING:
                for (j=1;cstring[i+j]!='\"'&&cstring[i+j]!='\0';j++) if (cstring[i+j]=='\\'&&cstring[i+j+1]!='\0') j++;
                if (cstring[i+j]=='\"') j++; /* Skip over final quote */
                ttype=TOKEN_STRING;
                tok->nud=(nudtype *) parsestring; /* Forces the parser to use the parsestring parselet upon encountering a string */
                break;
            case LEX_ALPHA:
                for (j=1;(lexchar(cstring[i+j])==LEX_ALPHA)||(lexchar(cstring[i+j])==LEX_NUMBER)||(cstring[i+j]=='_');j++); /* TOTO: Make this less hacky */
                ttype = TOKEN_NAME;
                tok->nud=(nudtype *) parsename; /* Forces the parser to use the parsename parselet upon encountering a name */
                break;
            case LEX_NUMBER:
                ttype = TOKEN_INTEGER;
                tok->nud=(nudtype *) parseinteger; /* Forces the parser to use the parseinteger parselet upon encountering a number */
                for (j=1;lexchar(cstring[i+j])==LEX_NUMBER;j++);
                if (cstring[i+j]=='.') {
                    for (j+=1;lexchar(cstring[i+j])==LEX_NUMBER;j++);
                    ttype = TOKEN_FPOINT;
                }
                if (cstring[i+j]=='e'&&(cstring[i+j+1]=='+'||cstring[i+j+1]=='-')) {
                    for (j+=2;lexchar(cstring[i+j])==LEX_NUMBER;j++);
                    ttype = TOKEN_FPOINT;
                }
                if (cstring[i+j]=='e') {
                    for (j+=1;lexchar(cstring[i+j])==LEX_NUMBER;j++);
                    ttype = TOKEN_FPOINT;
                }
                
                /* Forces the parser to use the parsefloat parselet upon encountering a float */
                if (ttype==TOKEN_FPOINT) tok->nud=(nudtype *) parsefloat;
                break;
            
            case LEX_OPERATOR:
                /* Find the longest sequence of operator-like characters */
                for (j=1;lexchar(cstring[i+j])==LEX_OPERATOR;j++);

                {
                    char opstring[j+1];
                    operator *op;
                    strncpy(opstring,cstring+i,j);
                    
                    /* Attempt to identify the longest possible operator by working backwards. This
                       permits, e.g., '==' to be recognized before '='. */
                    for (; j>0; j--) {
                        opstring[j]='\0';
                        op = (operator *) hashget(operators,opstring); /* Is this in the operator table? */
                        if (op) {
                            /* tokens inherit many properties from the operator. */
                            tok->nud=op->nud;
                            tok->led=op->led;
                            tok->data=(void *) op;
                            tok->lbp=op->precedence;
                            ttype=TOKEN_OPERATOR;
                            break;
                        }
                    }
                    if (j==0) {
                       ttype=TOKEN_UNRECOGNIZED;
                       tok->token=cstring+i;
                       tok->tokenlength=1;
                       error_raisewithdata(NULL, 0, "Unrecognized operator", ERROR_PARSE,tok);
                    };
                }
                break;
                
            default:
                break;
        }
    }

    /* Correctly identify the token start, length and type */
    tok->token=cstring+i;
    tok->tokenlength=j;
    tok->toktype=ttype;
    
    /*{
        char tokens[j+1];
        strncpy(tokens,tok->token,j);
        tokens[j]='\0';
        printf("TOKEN: [%s]\n",tokens);
    }*/
    
    return cstring+i+j;
}

/*
 * Parser - Converts a string of words into a grammatical tree
 */

/* parseskip - Checks whether the next token matches some specified form, skipping it
 * Input:   (token *) tok  - The current token [i.e. the one before the expected form]
 *          (char *) value - An expected next token, e.g. ")"
 * Output:  (token *) tok  - is updated to point to the next token by lex.
 * Returns: (int)          - TRUE if the next token matched the expected form, FALSE if not.
 */
int parseskip(token *tok, char *value) {
    
    lex(tok->token+tok->tokenlength,tok,0,TRUE);
    
    {
        char tokenname[tok->tokenlength+1];
        strncpy(tokenname,tok->token,tok->tokenlength);
        tokenname[tok->tokenlength]='\0';
        
        return !strcmp(tokenname,value);
    }
    
    return 0;
}

/* parsecheck - Nondestructively checks whether the next token matches some specified form
 * Input:   (token *) tok  - The current token [i.e. the one before the proposed form]
 *          (char *) value - A proposed next token, e.g. ")"
 *          int force      - Set if more input is expected to force the lexer to continue if at the end of the
 *                           current line.
 * Output:
 * Returns: (int)          - TRUE if the next token matched the proposed form, FALSE if not.
 */
int parsecheck(token *tok, char *value, int force) {
    token temp;
    temp.base=tok->base;
    temp.cdata=tok->cdata;
    temp.cfunc=tok->cfunc;
    
    lex(tok->token+tok->tokenlength,&temp,0,force);
    
    if (temp.continued) {
        /* The continuation function was called */
        tok->base=temp.base;
        tok->token=temp.token;
        tok->tokenlength=0;
    }
    
    {
        char tokenname[temp.tokenlength+1];
        strncpy(tokenname,temp.token,temp.tokenlength);
        tokenname[temp.tokenlength]='\0';
        
        return !strcmp(tokenname,value);
    }
    
    return 0;

}

/* 
 * Constructors.
 *
 * -- N.B. These ALWAYS clone any expressions passed to them.
 */

/* newexpressioninteger - Creates an integer expression from a given value
 * Input:   (int) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_INTEGER
 */
expression *newexpressioninteger(int value) {
    expression_integer *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_integer))) == NULL) return NULL;
    
    ne->type=EXPRESSION_INTEGER;
    ne->value=value;
    
    return (expression *) ne;
}

/* newexpressionfloat - Creates a float expression from a given value
 * Input:   (double) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_FLOAT
 */
expression *newexpressionfloat(double value) {
    expression_float *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_float))) == NULL) return NULL;
    
    ne->type=EXPRESSION_FLOAT;
    ne->value=value;
    
    return (expression *) ne;
}

/* newexpressioncomplex - Creates a complex expression from a given value
 * Input:   (complex double) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_COMPLEX
 */
expression *newexpressioncomplex(complex double value) {
    expression_complex *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_complex))) == NULL) return NULL;
    
    ne->type=EXPRESSION_COMPLEX;
    ne->value=value;
    
    return (expression *) ne;
}


/* newexpressionbool - Creates a boolean expression from a given value
 * Input:   (int) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_BOOL
 */
expression *newexpressionbool(int value) {
    expression_bool *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_bool))) == NULL) return NULL;
    
    ne->type=EXPRESSION_BOOL;
    ne->value=value;
    
    return (expression *) ne;
}

/* newexpressionnone - Creates a null expression - this is typically returned on success by functions
 *                     that don't produce output.
 * Input:   (int) value    - a value
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_NONE
 */
expression *newexpressionnone(void) {
    expression *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression))) == NULL) return NULL;
    
    ne->type=EXPRESSION_NONE;
    
    return ne;
}

/* newexpressionname - Creates a string expression from a given value
 * Input:   (char *) value - a 0-terminated string.
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_STRING
 */
expression *newexpressionname(char *name) {
    expression_name *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_name))) == NULL) return NULL;
    
    ne->type=EXPRESSION_NAME;
    ne->name= EVAL_STRDUP(name);
    
    if (!ne->name) { EVAL_FREE(ne); return NULL; }
    
    return (expression *) ne;
}

/* newexpressionoperator - Creates a new operator expression
 * Input:   (char *) value - a 0-terminated string.
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_STRING
 */
expression *newexpressionoperator(operator *op, expression *left, expression *right, int clone) {
    expression_operator *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_operator))) == NULL) return NULL;
    
    ne->type=EXPRESSION_OPERATOR;
    
    ne->op=op;
    if (clone) {
        ne->left=cloneexpression(left);
        ne->right=cloneexpression(right);
    } else {
        ne->left=left;
        ne->right=right;
    }
    
    return (expression *) ne;
}


/* newexpressionstring - Creates a string expression from a given value
 * Input:   (char *) value - a 0-terminated string.
 * Output:
 * Returns: (expression *) - a newly allocated expression, of type EXPRESSION_STRING
 */
expression *newexpressionstring(char *value) {
    expression_string *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_string))) == NULL) return NULL;
    
    ne->type=EXPRESSION_STRING;
    ne->value= EVAL_STRDUP(value);
    
    if (!ne->value) { EVAL_FREE(ne); return NULL; }
    
    return (expression *) ne;
}

expression_function *newexpressionfunction(expression *name, int nargs, expression **args, int clone) {
    expression_function *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_function))) == NULL) return NULL;
    
    ne->type=EXPRESSION_FUNCTION;
    
    if (clone) {
        ne->name=cloneexpression(name);
    } else {
        ne->name=name;
    }
    
    if (!ne->name) goto newexpressionfunction_clearup;
    ne->nargs=nargs;
    if (nargs>0) {
        ne->argslist=EVAL_MALLOC(sizeof(expression*)*nargs);
        if (!ne->argslist) goto newexpressionfunction_clearup;
        
        for (int i=0;i<nargs;i++) {
            if (clone) {
                ne->argslist[i] = cloneexpression(args[i]);
            } else {
                ne->argslist[i] = args[i];
            }
        }
    }
    
    return ne;
    
newexpressionfunction_clearup:
    if (ne->name) EVAL_FREE(ne->name);
    EVAL_FREE(ne);
    
    return NULL;
}


/* newexpressionfunctiondefinition - Creates a function definition from a given function expression and definition
 * Input:   (expression_function *) func      - A parsed function expression that identifies the name of the function and its arguments.
 *          (expression *) def                - Syntax tree defining the function.
 * Output:
 * Returns: (expression_functiondefinition *) - a newly allocated expression, of type EXPRESSION_FUNCTION_DEFINITION
 */
expression_functiondefinition *newexpressionfunctiondefinition(expression_function *func, expression *def, int clone) {
    expression_functiondefinition *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_functiondefinition))) == NULL) return NULL;
    
    ne->type=EXPRESSION_FUNCTION_DEFINITION;
    ne->name=NULL;
    if (func->name) ne->name=cloneexpression(func->name);
    ne->nargs=func->nargs;
    if (func->nargs>0) {
        ne->argslist=EVAL_MALLOC(sizeof(expression*)*(func->nargs));
        for (int i=0;i<func->nargs;i++) {
            if (clone) {
                ne->argslist[i] = cloneexpression(func->argslist[i]);
            } else {
                ne->argslist[i] = func->argslist[i];
            }
        }
    }
    ne->eval=cloneexpression(def);
    
    return ne;
}

/* newexpressionlist - Creates a list expression from a given linkedlist, cloning the entries
 * Input:   (linkedlist) *list  - an existing list, or NULL to create a blank list
 * Output:
 * Returns: (expression_list *) - a newly allocated expression, of type EXPRESSION_LIST
 */
expression_list *newexpressionlist(linkedlist *list) {
    expression_list *ne;
    
    if((ne = EVAL_MALLOC(sizeof(expression_list))) == NULL) return NULL;
    ne->type = EXPRESSION_LIST;

    ne->list=linkedlist_new();
    if (!ne->list) { EVAL_FREE(ne); return NULL; };
    
    if (list) for (linkedlistentry *e=list->first; e!=NULL; e=e->next) {
        linkedlist_addentry(ne->list,cloneexpression((expression *) e->data));
    }
    
    return ne;
}

void eval_linkedlisttohashtablemapfunction(char *name, expression *exp, linkedlist *list) {
    operator *opassign = operator_find(OPERATOR_ASSIGN);
    
    if (opassign) {
        linkedlist_addentry(list, newexpressionoperator(opassign, newexpressionname(name), cloneexpression(exp), FALSE));
    }
}

/* newexpressionlistfromhashtable - Creates a list expression of the form 'name'='value'
 * Input:   (hashtable *) ht - A hashtable to use
 * Output: 
 * Returns: (expression_list *) - a newly allocated expression, of type EXPRESSION_LIST
 */
expression_list *newexpressionlistfromhashtable(hashtable *ht) {
    expression_list *ne=NULL;
    
    if((ne = EVAL_MALLOC(sizeof(expression_list))) == NULL) return NULL;
    ne->type = EXPRESSION_LIST;
    ne->list=linkedlist_new();
    
    if (!ne->list) { EVAL_FREE(ne); return NULL; };

    hashmap(ht, (hashmapfunction *) eval_linkedlisttohashtablemapfunction, ne->list);
    
    return ne;
}

/* newexpressionlistfromargs - Creates a list expression from a given linkedlist
 * Input:   (int) nargs         - the number of arguments.
 *          An optional number of arguments, of type expression *.
 * Output:
 * Returns: (expression_list *) - a newly allocated expression, of type EXPRESSION_LIST
 */
expression_list *newexpressionlistfromargs(int nargs, ...) {
    va_list a;
    expression_list *ne=NULL;
    
    if (nargs>0) {
        if((ne = EVAL_MALLOC(sizeof(expression_list))) == NULL) return NULL;
        ne->type = EXPRESSION_LIST;
        
        ne->list=linkedlist_new();
        if (!ne->list) { EVAL_FREE(ne); return NULL; };
        
        va_start(a, nargs);
        if (ne->list) for (unsigned int i=0; i<nargs; i++) {
            
            linkedlist_addentry(ne->list,cloneexpression(va_arg(a, expression*)));
        }
        va_end(a);
    }
    
    return ne;
}


/* Parselets
 * The parser is modularized, so that a number of functions or 'parselets' are responsible for parsing different kinds of
 * expression.
 */
expression *parsefunction(token *tok, expression *left, expressiontype type, char *leftdelim, char *rightdelim) {
    expression_function *ne=NULL;
    expression *args[EVAL_MAXFUNCTIONARGS];
    unsigned int nargs=0;
    
    /*if (!parseskip(tok,leftdelim)) {
        sprintf(error_buffer(),"Expected '%s'",leftdelim);
        error_raisewithdata(NULL, 0, error_buffer(), ERROR_PARSE,tok);
    }*/
    
    tok->forcecontinue=TRUE;
    for (nargs=0; !parsecheck(tok,rightdelim,TRUE); nargs++) {
        args[nargs]=parseexpression(tok,0,TRUE);
        if (!parsecheck(tok,rightdelim,TRUE)) if (!parseskip(tok,",")) {
            sprintf(error_buffer(),"Expected ',' or '%s'",rightdelim);
            error_raisewithdata(NULL, 0, error_buffer(), ERROR_PARSE,tok);
        }
    }
    
    parseskip(tok,rightdelim);
    tok->forcecontinue=FALSE;
    
    ne=newexpressionfunction(left, nargs, args, FALSE);
    if (ne) ne->type=type;
    
    return (expression *) ne;
}

expression *parsename(token *tok) {
    expression *ne=NULL;
    char tokenstring[tok->tokenlength+1];
    
    /* Zero terminate token */
    strncpy(tokenstring,tok->token,tok->tokenlength);
    tokenstring[tok->tokenlength]='\0';
    
    /* Detect if this is actually a constant value */
    expression *constant=hashget(constants,tokenstring);
    if (constant) return cloneexpression(constant);

    ne=newexpressionname(tokenstring);
    
    return ne;
}

expression *parseinteger(token *tok) {
    char num[tok->tokenlength+1];
    int value;
    
    strncpy(num,tok->token,tok->tokenlength);
    num[tok->tokenlength]='\0';
    sscanf(num,"%i",&value);
    
    return newexpressioninteger(value);
}

expression *parsefloat(token *tok) {
    char num[tok->tokenlength+1];
    double value;
    
    strncpy(num,tok->token,tok->tokenlength);
    num[tok->tokenlength]='\0';
    sscanf(num,"%lf",&value);
    
    return newexpressionfloat(value);
}

expression *parsefunctioncall(token *tok, expression *left) {
    return parsefunction(tok, left, EXPRESSION_FUNCTION, "(", ")");
}

expression *parseindex(token *tok, expression *left) {
    return parsefunction(tok, left, EXPRESSION_INDEX, "[", "]");
}

/* Parse infix operators */
expression *parseinfix(token *tok,expression *left) {
    operator *op = (operator *) tok->data;
    return newexpressionoperator(op,left,parseexpression(tok,tok->lbp,TRUE),FALSE);
}

/* Infix with optional right argument */
expression *parseinfixoptional(token *tok,expression *left) {
    operator *op = (operator *) tok->data;
    return newexpressionoperator(op,left,parseexpression(tok,tok->lbp,tok->forcecontinue),FALSE);
}

/* Right associative infix operators */
expression *parseinfixright(token *tok,expression *left) {
    operator *op = (operator *) tok->data;
    return newexpressionoperator(op,left,parseexpression(tok,tok->lbp-1,TRUE),FALSE);
}

/* Prefix operators */
expression *parseprefix(token *tok) {
    operator *op = (operator *) tok->data;
    tok->led=NULL;
    
    return newexpressionoperator(op,NULL,parseexpression(tok,PREFIX_PRECEDENCE,TRUE),FALSE);
}

/* Parentheses */
expression *parseparenthesis(token *tok) {
    expression *exp=parseexpression(tok,0,TRUE);
    
    /* Skip over closing parenthesis */
    if (!parseskip(tok,")")) { error_raisewithdata(NULL, 0, "Expected ')'", ERROR_PARSE,tok); }
    
    return exp;
}

/* Parse strings */
expression *parsestring(token *tok) {
    char string[tok->tokenlength];
    strncpy(string,tok->token+1,tok->tokenlength-2);
    string[tok->tokenlength-2]='\0';
    
    return newexpressionstring(string);
}

/* Parse lists */
expression *parselist(token *tok) {
    expression_list *ne=NULL;
    expression *exp;
    
    if((ne = EVAL_MALLOC(sizeof(expression_list))) == NULL) return NULL;
    ne->type=EXPRESSION_LIST;
    ne->list=linkedlist_new();
    if (!ne->list) { EVAL_FREE(ne); return(NULL); };
    
    tok->forcecontinue=TRUE; /* Force continuation over lines */
    for (int i=0; !parsecheck(tok,"}",TRUE); i++) {
        exp=parseexpression(tok,0,TRUE);
        linkedlist_addentry(ne->list,exp);
        
        /* Skip past separators */
        if (!parsecheck(tok,"}",TRUE)) {
            if (parsecheck(tok,",",TRUE)) {
                parseskip(tok,",");
            } /* else if (tok->base==tok->token) {
             
            } */ else {
                error_raisewithdata(NULL, 0, "Expected ',' or '}'", ERROR_PARSE,tok);
            }
        }
    }
    
    /* Skip over closing parenthesis */
    if (!parseskip(tok,"}")) { error_raisewithdata(NULL, 0, "Expected '}'", ERROR_PARSE,tok); }
    tok->forcecontinue=FALSE;
    
    return (expression *) ne;
}

/* parseexpression - Parse an expression using Pratt's algorithm
 * Input:   (token *) tok - the current token
 *          (int) rbp     - the right binding power of the current expression on the left. 
 *          (int) required - flag to indicate whether this expression is required or not (i.e. if the lexer
 *                           should force continuation.
 * Output:  (token *) tok - updated to the next token
 * Returns: (expression *) - an expression which is the head of the resulting syntax tree.
 */
expression *parseexpression(token *tok, int rbp, int required) {
    token next_token;
    expression *left=NULL;
    char *next=NULL;

    /* Copy across relevant data from tok to next_token */
    next_token.base=tok->base;
    next_token.cdata=tok->cdata;
    next_token.cfunc=tok->cfunc;
    next_token.forcecontinue=tok->forcecontinue;
    
    lex(tok->token+tok->tokenlength,tok,rbp,required);
 //   if (tok->toktype==TOKEN_EOF) return &parseeofexp;
    if (tok->nud) {
        left = tok->nud((void *) tok);
    } else {
        /* Detect missing operand */
        if ((tok->toktype==TOKEN_OPERATOR)&&(!((tok->led==NULL)&&(tok->nud==NULL)))) error_raisewithdata(NULL, 0, "Missing operand", ERROR_PARSE,tok);
        
        /* Detect symbol-like operators */
        if ((tok->toktype==TOKEN_OPERATOR)&&((tok->led==NULL)&&(tok->nud==NULL))) {
            /* These should be processed by the caller. */
            tok->tokenlength=0;
            return left;
        }
    }
    next=tok->token+tok->tokenlength;
    
    next=lex(next,&next_token,rbp,FALSE); /* A second token is never required */
    while(rbp < next_token.lbp && next_token.toktype!=TOKEN_EOF) {
        *tok = next_token;
        
        if (tok->led!=NULL) {
            left=tok->led((void *) tok, left); // How will the current token be correctly updated?
            next=tok->token+tok->tokenlength;
        }
        
        next=lex(next,&next_token,rbp,FALSE);
    }
    
   // if (tok->toktype==TOKEN_EOF) return &parseeofexp;
    
    return left;
}

/* parse - Parse a string. This is the public interface to the parser.
 * Input:   (char *) string - a string to be parsed.
 *          (eval_cfunctype *) cfunc - a continuation function that the lexer/parser can call for more input.
 *          (void *) cdata  - data to pass to the continuation function.
 * Output:
 * Returns: (expression *) - an expression which is the head of the resulting syntax tree.
 */
expression *parse(char *string, eval_cfunctype *cfunc, void *cdata) {
    expression *ret=NULL;
    token tok;
    jmp_buf buf;

    /* parsexpression needs us to set up the token we pass it with enough information for it to find the first token */
    tok.token=string;
    tok.base=string;
    tok.tokenlength=0;
    tok.cfunc=cfunc;
    tok.cdata=cdata;
    tok.forcecontinue=FALSE;
    
    if (setjmp(buf)) {
        /* TODO: Must do cleanup */
        
        
    } else {
        error_saveenv(NULL, &buf);
        ret=parseexpression(&tok,0,FALSE);
    }
    
    return ret;
}

size_t subtract_size_t(size_t a, size_t b) {
    if (b>a) return 0;
    else return a-b;
}

/* sexpressionprint - Prints an expression object to a string with length checking
 * Input:   (expression *) exp - an expression to be printed.
 *          (int) bufferlength - length of buffer
 *          (char *) buff    - buffer to use (if NULL, no bytes are written).
 * Output:  (char *) buff    - buffer containing the printed output
 * Returns: The number of characters written, or required if the buffer is too small (like snprintf).
 */
size_t sexpressionprint (expression *exp, size_t bufferlength, char *buffer) {
    size_t nbytes=0;
    
    if (exp) switch (exp->type) {
        case EXPRESSION_NAME:
            nbytes += snprintf(buffer, bufferlength, "%s", ((expression_name *) exp)->name);
            break;
        case EXPRESSION_INTEGER:
            nbytes += snprintf(buffer, bufferlength, "%i", ((expression_integer *) exp)->value);
            break;
        case EXPRESSION_FLOAT:
            nbytes += snprintf(buffer, bufferlength, "%g", ((expression_float *) exp)->value);
            break;
        case EXPRESSION_BOOL:
            if (((expression_bool *) exp)->value) {
                nbytes += snprintf(buffer, bufferlength, "true");
            } else {
                nbytes += snprintf(buffer, bufferlength, "false");
            }
            break;
        case EXPRESSION_COMPLEX:
        {
            expression_complex *cexp = (expression_complex *) exp;
            
            if (!eval_haszeroreal(cexp)) {
                /* Print the real part if it's nonzero */
                nbytes += snprintf(buffer, bufferlength, "%g" , creal(cexp->value));
                /* and a plus sign if there's an imaginary part */
                if (!eval_haszeroimag(exp)) {
                    nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), " + ");
                }
            } else if (eval_haszeroimag(cexp)) {
                /* If the number is zero, need to print 0 */
                nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "0");
            }
            
            if (!eval_haszeroimag(cexp)) {
                if(!(fabs(cimag(cexp->value)-1.0)<MACHINE_EPSILON)) {
                    if((fabs(cimag(cexp->value)+1.0)<MACHINE_EPSILON)) {
                        nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "-");
                    } else {
                        nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "%g", cimag(cexp->value));
                    }
                }
                
                nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "I");
            }
        }
            break;
        case EXPRESSION_STRING:
            if (((expression_string *) exp)->value) {
                nbytes += snprintf(buffer+nbytes, bufferlength, "\"%s\"",((expression_string *) exp)->value);
            }
            break;
        case EXPRESSION_OPERATOR:
        {
            expression_operator *expop=(expression_operator *) exp;
            
            if (expop->op->type==OPERATOR_INFIX) {
                if (expop->left) {
                    nbytes += sexpressionprint(expop->left, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
                }
                nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), " %s ", expop->op->symbol);
                if (expop->right) {
                    nbytes += sexpressionprint(expop->right, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
                }
            } /* TODO: Other kinds of operator are not handled! */
        }
            break;
        case EXPRESSION_FUNCTION:
        {
            expression_function *expfunc=(expression_function *) exp;
            
            nbytes += sexpressionprint(expfunc->name, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "(");
            
            for (int i=0;i<expfunc->nargs;i++) {
                nbytes += sexpressionprint(expfunc->argslist[i], subtract_size_t(bufferlength, nbytes), buffer+nbytes);
                if (i<expfunc->nargs-1) { nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ", ");  }
            }
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ")");
        }
            break;
        case EXPRESSION_LIST:
        {
            expression_list *explist=(expression_list *) exp;
            
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "{");
            for (linkedlistentry *e=explist->list->first; e!=NULL; e=e->next) {
                if(e->data) { nbytes += sexpressionprint((expression *) e->data, subtract_size_t(bufferlength, nbytes), buffer+nbytes); }
                if(e->next) { nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ", "); }
            }
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "}");
        }
            break;
        case EXPRESSION_INDEX:
        {
            expression_index *expind=(expression_index *) exp;
            
            nbytes += sexpressionprint(expind->name, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "[");
            for (int i=0;i<expind->nargs;i++) {
                nbytes += sexpressionprint(expind->argslist[i], subtract_size_t(bufferlength, nbytes), buffer+nbytes);
                
                if(i<expind->nargs-1) nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ", ");
            }
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "]");
        }
            break;
        case EXPRESSION_FUNCTION_DEFINITION:
        {
            expression_functiondefinition *expfunc=(expression_functiondefinition *) exp;
            
            nbytes += sexpressionprint(expfunc->name, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "(");
            for (int i=0;i<expfunc->nargs;i++) {
                nbytes += sexpressionprint(expfunc->argslist[i], subtract_size_t(bufferlength, nbytes), buffer+nbytes);
                
                if(i<expfunc->nargs-1) nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ", ");
            }
            nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), ")");
            if (expfunc->eval) {
                nbytes += snprintf(buffer+nbytes, subtract_size_t(bufferlength, nbytes), "=");
                nbytes += sexpressionprint(expfunc->eval, subtract_size_t(bufferlength, nbytes), buffer+nbytes);
            }
        }
            break;
        case EXPRESSION_OBJECT_REFERENCE:
        {
            expression_objectreference *ref=(expression_objectreference *) exp;
            if (ref->obj && ref->obj->clss) nbytes += snprintf(buffer, bufferlength, "<%s>", ref->obj->clss->name);
        }
            break;
        case EXPRESSION_NONE:
            break;
            
        case EXPRESSION_RETURN:
            break;
    }
    
    return nbytes;
}

/* expressionprinttobuffer - prints an expression to a newly allocated buffer.
 * Input:   (expression *) exp - an expression to be printed.
 * Output:
 * Returns: Pointer to a newly allocated string containing the output.
 */
char *expressionprinttobuffer (expression *exp) {
    size_t size = sexpressionprint(exp, 0, NULL);
    char *buffer;
    
    buffer = EVAL_MALLOC(size+1);
    if (buffer) {
        buffer[0]='\0';
        sexpressionprint(exp, size+1, buffer);
    }
    
    return buffer;
}

/* expressionprint - Prints an expression object to stdout
 * Input:   (expression *) exp - an expression to be printed.
 * Output:
 * Returns:
 */
void expressionprint (expression *exp) {
    char *string = expressionprinttobuffer(exp);
    
    if (string) {
        printf("%s", string);
        EVAL_FREE(string);
    }
}

void expressionprintnl (expression *exp) {
    expressionprint(exp);
    printf("\n");
}

/* cloneexpression - Clones an existing expression
 * Input:   (expression *) exp                - An expression to clone.
 * Output:
 * Returns: (expression *) - a newly allocated expression that clones exp.
 */
expression *cloneexpression(expression *exp) {
    expression *ne=NULL;
    
#ifdef EVAL_DETAILEDDEBUG
    printf("Cloning expression (%lx) ", (unsigned long int) exp);
    expressionprint(exp);
    printf("\n");
#endif

    if (exp==NULL) return NULL;
    
    switch (exp->type) {
        case EXPRESSION_INTEGER:
            ne=(expression *) newexpressioninteger(((expression_integer *) exp)->value);
            break;
        case EXPRESSION_FLOAT:
            ne=(expression *) newexpressionfloat(((expression_float *) exp)->value);
            break;
        case EXPRESSION_COMPLEX:
            ne=(expression *) newexpressioncomplex(((expression_complex *) exp)->value);
            break;
        case EXPRESSION_BOOL:
            ne=(expression *) newexpressionbool(((expression_bool *) exp)->value);
            break;
        case EXPRESSION_NAME:
            ne = (expression *) newexpressionname(((expression_name *) exp)->name);
            return ne;
            break;
        case EXPRESSION_OPERATOR:
            {
                expression_operator *oexp = (expression_operator *) exp;
                
                ne = (expression *) newexpressionoperator(oexp->op, oexp->left, oexp->right,TRUE);
            }
            break;
        case EXPRESSION_FUNCTION:
            {
                expression_function *fexp = (expression_function *) exp;
                
                ne = (expression *) newexpressionfunction(fexp->name, fexp->nargs, fexp->argslist,TRUE);
            }
            break;
        case EXPRESSION_INDEX:
            {
                expression_index *fexp = (expression_index *) exp;
                
                ne = (expression *) newexpressionfunction(fexp->name, fexp->nargs, fexp->argslist,TRUE);
                if (ne) ne->type=EXPRESSION_INDEX;
            }
            break;
        case EXPRESSION_FUNCTION_DEFINITION:
            {
                expression_functiondefinition *fexp = (expression_functiondefinition *) exp;
                expression_function func;
                func.type=EXPRESSION_FUNCTION;
                func.name=fexp->name;
                func.nargs=fexp->nargs;
                func.argslist=fexp->argslist;
                
                ne = (expression *) newexpressionfunctiondefinition(&func, fexp->eval, TRUE);
            }
            break;
        case EXPRESSION_STRING:
            ne = (expression *) newexpressionstring(((expression_string *) exp)->value);
            break;
        case EXPRESSION_LIST:
            ne = (expression *) newexpressionlist(((expression_list *) exp)->list);
            break;
        case EXPRESSION_OBJECT_REFERENCE:
            ne = (expression *) class_newobjectreference(((expression_objectreference *)exp)->obj);
            break;
        case EXPRESSION_NONE:
            ne = (expression *) newexpressionnone();
            break;
        case EXPRESSION_RETURN:
            printf("WARNING SHOULD NEVER REACH HERE!\n");
            return NULL;
            break;
    }
    
    return ne;
}

#ifdef DEBUG
expression *watchexp;

void setwatchfreeexpression(expression *exp) {
    watchexp = exp;
}
#endif

/* freeexpression - EVAL_FREEs an expression
 * Input:   (expression *) exp - expression to be EVAL_FREEd
 * Output:
 * Returns:
 */
void freeexpression(expression *exp) {
    
#ifdef EVAL_DETAILEDDEBUG
    if (exp) {
        printf("EVAL_FREEing expression (%lx) ", (unsigned long int) exp);
        expressionprint(exp);
        printf("\n");
    }
#endif
    
#ifdef DEBUG
    if (exp==watchexp) {
//        printf("WATCH!\n");
    }
#endif
    
    if (exp!=NULL) {
        switch (exp->type) {
            case EXPRESSION_OPERATOR:
                {
                    expression_operator *opexp=(expression_operator *) exp;
                    
                    if (opexp->left!=NULL) freeexpression(opexp->left);
                    if (opexp->right!=NULL) freeexpression(opexp->right);
                }
                break;
                
            case EXPRESSION_FUNCTION:
                {
                    expression_function *expfn=(expression_function *) exp;
                    
                    for (int i=0;i<expfn->nargs;i++) freeexpression(expfn->argslist[i]);
                    
                    if (expfn->name) freeexpression(expfn->name);
                    if (expfn->nargs>0) if (expfn->argslist) EVAL_FREE(expfn->argslist);
                    /* QUERY: The args themselves should be freed by the caller? */
                }
                break;
                
            case EXPRESSION_FUNCTION_DEFINITION:
                {
                    expression_functiondefinition *fexp=(expression_functiondefinition *) exp;
                    
                    for (int i=0;i<fexp->nargs;i++) freeexpression(fexp->argslist[i]);
                    if (fexp->eval!=NULL) freeexpression(fexp->eval);
                    
                    if (fexp->name) freeexpression(fexp->name);
                    if (fexp->nargs>0) if (fexp->argslist) EVAL_FREE(fexp->argslist);
                }
                break;
                
            case EXPRESSION_LIST:
                for (linkedlistentry *e=((expression_list *)exp)->list->first; e!=NULL; e=e->next) {
                    if(e->data) freeexpression(e->data);
                }
                linkedlist_free(((expression_list *)exp)->list);
                break;
                
            case EXPRESSION_STRING:
                EVAL_FREE(((expression_string *) exp)->value);
                break;
                
            case EXPRESSION_NAME:
                EVAL_FREE(((expression_name *) exp)->name);
                break;
                
            case EXPRESSION_OBJECT_REFERENCE:
                class_freeobjectreference((expression_objectreference *) exp);
                break;
                
            case EXPRESSION_INDEX:
                {
                    expression_index *iexp=(expression_index *) exp;
                    
                    for (int i=0;i<iexp->nargs;i++) freeexpression(iexp->argslist[i]);
                    
                    if (iexp->name) freeexpression(iexp->name);
                    if (iexp->nargs>0) if (iexp->argslist) EVAL_FREE(iexp->argslist);
                }

                break;
                
            case EXPRESSION_INTEGER:
            case EXPRESSION_FLOAT:
            case EXPRESSION_COMPLEX:
            case EXPRESSION_BOOL:
            case EXPRESSION_NONE:
            case EXPRESSION_RETURN:
                break;
                
        }
        EVAL_FREE(exp);
    }
}


/*
 * Interpreter - evaluates the contents of an expression tree.
 */

#define EVAL_MAXSAVECONTEXTS 5
interpretcontext *icsave[EVAL_MAXSAVECONTEXTS+1];
unsigned int icsaven=0;

/* newinterpretcontext - Creates a new context
 * Input:   (interpretcontext *) parent - a parent context.
 *          (int) size                  - the size of the symbol table for the new context.
 * Output:
 * Returns: (interpretcontext *)        - a newly allocated context.
 */
interpretcontext *newinterpretcontext(interpretcontext *parent, int size) {
    interpretcontext *newcontext;
    
    /* Re-use old contexts */
    if (icsaven>0) { newcontext=icsave[icsaven-1]; icsaven--; }
    else newcontext = EVAL_MALLOC( sizeof( interpretcontext ));
    
    if (newcontext == NULL) {
		return NULL;
	}
    
    newcontext->insideselector=FALSE;
    newcontext->symbols=NULL; /* hashcreate(size); *//* TODO: check for success! */
    newcontext->freerecords=NULL; /* linkedlist_new(); */
    newcontext->errorenv=NULL;
    newcontext->size=size;

    newcontext->parent=parent;
    
    if (parent) {
        if (parent->insideselector) {
            /* If we're inside a selector, place free records into the parent's parent 
             * (which is guaranteed to exist) */
            interpretcontextregisterfree(parent->parent, newcontext, (interpretcontextfreefunction *) errorfreeinterpretcontext);
        } else {
            interpretcontextregisterfree(parent, newcontext, (interpretcontextfreefunction *) errorfreeinterpretcontext);
        }
    }
    
    return newcontext;
}

void printfreerecords(interpretcontext *context) {
    
    if (context->freerecords) for (linkedlistentry *e=context->freerecords->first; e!=NULL; e=e->next) {
        interpretcontextfree *freerecord = e->data;
        
        printf("(%lx) ", (unsigned long int) freerecord->data);
    }
    printf("\n");
    
}

/* freeinterpretcontext - EVAL_FREEs a context, also freeing attached symbols and freerecords.
 * Input:   (interpretcontext *) context - a context to free.
 * Output:
 * Returns:
 */
void freeinterpretcontext(interpretcontext *context) {
    if (context) {
        /* Remove the context from the free list of the parent context */
        if (context->parent) {
            if (context->parent->insideselector) {
                /* If we're inside a selector, place free records into the parent's parent
                 * (which is guaranteed to exist) */
                interpretcontextderegisterfree(context->parent->parent, context);
            } else {
                interpretcontextderegisterfree(context->parent, context);
            }
        }
        
        if (context->symbols) {
            hashmap(context->symbols,&evalhashfreeexpression,NULL);
            hashfree(context->symbols);
        }
        
        if (context->freerecords) {
            for (linkedlistentry *e=context->freerecords->first; e!=NULL; e=e->next) EVAL_FREE(e->data);
            linkedlist_free(context->freerecords);
        }
        
        if (context->errorenv) EVAL_FREE(context->errorenv);
        
        if (icsaven<EVAL_MAXSAVECONTEXTS) {
            icsave[icsaven]=context;
            icsaven++;
        } else { EVAL_FREE(context); }
    }
}

/* freerecordsinterpretcontext - Calls the specified free function on any attached free records.
 * Input:   (interpretcontext *) context - a context to free.
 * Output:
 * Returns:
 */
void freerecordsinterpretcontext(interpretcontext *context) {
    if (context) {
        if (context->freerecords) {
            for (linkedlistentry *e=context->freerecords->first; e!=NULL; e=e->next) {
             interpretcontextfree *freerecord = (interpretcontextfree *) e->data;
             
             if (freerecord->freefunction) { (freerecord->freefunction) (freerecord->data); }
            }
        }
    }
}

/* errorfreeinterpretcontext - EVAL_FREEs a context, also freeing attached symbols and freerecords, calling
 *                             the specified free functions on the freerecords.
 * Input:   (interpretcontext *) context - a context to free.
 * Output:
 * Returns:
 */
void errorfreeinterpretcontext(interpretcontext *context) {
    if (context) {
        
        if (context->symbols) {
            hashmap(context->symbols,&evalhashfreeexpression,NULL);
            hashfree(context->symbols);
        }
        
        /* First call the free function on each record */
        freerecordsinterpretcontext(context);
        if (context->freerecords) {
            
            /* Now remove the records.. */
            for (linkedlistentry *e=context->freerecords->first; e!=NULL; e=e->next) EVAL_FREE(e->data);
            /* .. and free the list */
            linkedlist_free(context->freerecords);
        }
        
        EVAL_FREE(context);
    }
}

/* interpretcontextregisterfree - Registers an item to be freed upon an error.
 * Input:   (interpretcontext *) context - the context to use
 *          (void *) data                - a pointer to some data
 *          (interpretcontextfreefunction *) func - a function that will be called to free the data.
 * Output:
 * Returns:
 */
void interpretcontextregisterfree(interpretcontext *context, void *data, interpretcontextfreefunction *func) {
    if ((!context)||(!func)) return;
    interpretcontextfree *freerecord=EVAL_MALLOC(sizeof(interpretcontextfree));
    
    if (freerecord) {
        freerecord->data=data;
        freerecord->freefunction=func;
        if (!context->freerecords) context->freerecords=linkedlist_new();
        if (context->freerecords) linkedlist_addentry(context->freerecords, freerecord);
    }
}

/* interpretcontextregisterfree - Removes an item from the free list in a context, 
 *                                without freeing the attached data.
 * Input:   (interpretcontext *) context - the context to use
 *          (void *) data                - a pointer to the data record to remove.
 * Output:
 * Returns:
 */
void interpretcontextderegisterfree(interpretcontext *context, void *data) {
    interpretcontextfree *freerecord=NULL;
    interpretcontextfree *found=NULL;
    
    if (!context) return;
    
    if (context->freerecords) {
        for (linkedlistentry *e = context->freerecords->first; e!=NULL; e=e->next) {
            freerecord=(interpretcontextfree *) e->data;
            if (freerecord->data==data) {
                found=freerecord;
                break;
            }
        }
    }
    
    if (found) {
        if (context->freerecords) linkedlist_removeentry(context->freerecords, found);
        EVAL_FREE(found);
    }
}

/* interpretcontextclearfree - Clears all free records in a context without freeing them.
 * Input:   (interpretcontext *) context - the context to use
 * Output:
 * Returns:
 */
void interpretcontextclearfree(interpretcontext *context) {
    if (context) {
        if (context->freerecords) {
            for (linkedlistentry *e=context->freerecords->first; e!=NULL; e=e->next) {
                EVAL_FREE(e->data);
            }
            linkedlist_free(context->freerecords);
            context->freerecords=linkedlist_new();
        }
    }
}

/* findsymbol - Finds the current definition of a symbol in a given context.
 *              If the symbol isn't found, the parent contexts are successively searched.
 *              WARNING: the returned value must be copied before further usage.
 * Input:   (interpretcontext *) context - The context to begin searching.
 *          (char *) symbol              - The symbol to search for.
 * Output:
 * Returns: (expression *)               - an expression that matches the symbol, or NULL if no match was found.
 */
expression *findsymbol(interpretcontext *context, char *symbol) {
    expression *val=NULL;
    
    for (interpretcontext *ctxt=context; ctxt!=NULL&&val==NULL; ctxt=ctxt->parent) {
        if (ctxt->symbols) val=hashget(ctxt->symbols,symbol);
    }
    
    return val;
}


/* lookupsymbol - Looks up a symbol name from a given context. 
 *                If the symbol isn't found, the parent contexts are successively searched.
 *                Note that you MUST free what is returned! 
 * Input:   (interpretcontext *) context - The context to begin searching.
 *          (char *) symbol              - The symbol to search for.
 * Output:
 * Returns: (expression *)               - an expression that matches the symbol, or NULL if no match was found.
 */
expression *lookupsymbol(interpretcontext *context, char *symbol) {
    expression *val=NULL;

    for (interpretcontext *ctxt=context; ctxt!=NULL&&val==NULL; ctxt=ctxt->parent) {
        if (ctxt->symbols) val=hashget(ctxt->symbols,symbol);
        if (val) val=cloneexpression(val);
    }
    
    return val;
}

expression *eval_calluserfunction(interpretcontext *context, expression_function *expfunc,expression_functiondefinition *expdef) {
    char *name=NULL;
    
    if (eval_isname(expdef->name)) {
        name=((expression_name *) expdef->name)->name;
    }
    
    if (expdef->nargs!=expfunc->nargs) {
        sprintf(error_buffer(),ERROR_INCORRECTNUMARGS_MSG, name, expdef->nargs);
        error_raise(context, ERROR_INCORRECTNUMARGS, error_buffer(), ERROR_FATAL);
    }

    /* Create new context */
    interpretcontext *localcontext=newinterpretcontext(context, EVAL_LOCALCONTEXTTABLESIZE);
    expression *ret=NULL;

    if (!localcontext) return NULL;

    /* n.b. opassign interprets the argument expressions for us. */
    for (int i=0; i<expdef->nargs; i++) freeexpression(opassign(localcontext, expdef->argslist[i], expfunc->argslist[i]));

    /* Call */
    ret=interpretexpression(localcontext,expdef->eval);

    /* Inspect the contents --- this is so that a list always returns its last element */
    if(eval_islist(ret)) {
        expression_list *list = (expression_list *) ret;
        if (list->list->last) {
            expression *last = list->list->last->data;
            
            if (((expression_return *) last)&&(((expression_return *) last)->type==EXPRESSION_RETURN)) {
                ret = ((expression_return *) last)->value;
            } else {
                ret = cloneexpression(last);
            }
            
            freeexpression((expression *) list);
        } else {
            freeexpression(ret);
            ret=newexpressionnone();
        }
    }

    freeinterpretcontext(localcontext);
    
    return ret;
}

/* Performs a function call from a function specification. */
expression *callfunction(interpretcontext *context,expression_function *expfunc) {
    intrinsicfunction *func = NULL;
    expression *ret=NULL;
    expression_functiondefinition *anonymous=NULL;
    char *name=NULL;
    expression *nexp=NULL;
    
    /* Resolve name */
    if (eval_isfunctiondefinition(expfunc->name)) {
        anonymous=(expression_functiondefinition *) expfunc->name;
    } else {
        nexp=interpretexpression(context, expfunc->name);
        if (eval_isname(nexp)) {
            name=((expression_name *) nexp)->name;
        } else if (eval_isfunctiondefinition(nexp)) {
            anonymous=(expression_functiondefinition *) nexp;
        }
    }
    
    if (nexp) interpretcontextregisterfree(context, nexp, (interpretcontextfreefunction *) freeexpression);
    
    if (!name && !anonymous) {
        sprintf(error_buffer(),ERROR_CANTRESOLVENAME_MSG);
        error_raise(context, ERROR_CANTRESOLVENAME, error_buffer(), ERROR_FATAL);
    }
    
    /* If an intrinsic function, set up the call */
    if (!anonymous) func = (intrinsicfunction *) hashget(intrinsics,name);

    if (func) {
        expression *args[expfunc->nargs];
#ifdef EVAL_DETAILEDDEBUG
        printf("Calling intrinsic function ");
        expressionprint(expfunc);
        printf("\n");
#endif
        
        if (func->hold) { /* Simply copy the arguments, if hold is true */
            for (int i=0; i<expfunc->nargs; i++) {
                
                args[i]=cloneexpression(expfunc->argslist[i]);
            }
        } else { /* Evaluate arguments otherwise */
            for (int i=0; i<expfunc->nargs; i++) {
                args[i]=interpretexpression(context, expfunc->argslist[i]);
            }
        }
        
        ret=func->func(context,expfunc->nargs,args);
        
        if (ret==NULL) {
            /* The function could not be evaluated; hence we'll return a function specification
               with the arguments interpreted */
            ret=(expression *) newexpressionfunction(expfunc->name,expfunc->nargs,args,TRUE);
        }
        
        /* Delete arguments */
        for (int i=0; i<expfunc->nargs; i++) freeexpression(args[i]);
        
        if (nexp) freeexpression(nexp);
        
        return ret;
    }

    /* Is this an anonymous function? */
    if (anonymous) {
        ret = eval_calluserfunction(context,expfunc,anonymous);
    } else {
        /* Otherwise, look for a user-defined function and call if exists */
        expression_functiondefinition *expdef = (expression_functiondefinition *) lookupsymbol(context, name);
        
        if (expdef) {
            ret = eval_calluserfunction(context,expfunc,expdef);
        } else {
            sprintf(error_buffer(),ERROR_FUNCTIONNOTFOUND_MSG, name);
            error_raise(context, ERROR_FUNCTIONNOTFOUND, error_buffer(), ERROR_FATAL);
        }
    }
    
    if (nexp) freeexpression(nexp);
    
    return ret;
}

/* Performs indexing. Note that set is copied rather than used, so remember to free it! */
expression *interpretindex(interpretcontext *context, expression_index *index, expression *set) {
    expression *indx;
    expression *list=NULL;
    unsigned int i;
    char *name=NULL;
    expression *nexp=NULL;
    expression *ret=NULL;
    
    /* Resolve name */
    if (eval_isname(index->name)) {
        name=((expression_name *) index->name)->name;
    } else {
        nexp=interpretexpression(context, index->name);
        if (eval_isname(nexp)) {
            name=((expression_name *) nexp)->name;
        } else if (eval_islist(nexp) || eval_isobjectref(nexp)) {
            list=nexp;
        }
    }
    
    if (nexp) interpretcontextregisterfree(context, nexp, (interpretcontextfreefunction *) freeexpression);
    
    if ((!name)&&(!list)) {
        sprintf(error_buffer(),ERROR_CANTRESOLVEINDEXNAME_MSG);
        error_raise(context, ERROR_CANTRESOLVEINDEXNAME, error_buffer(), ERROR_FATAL);
    }
    
    /* Look up the name if the expression wasn't a list */
    if (!list) {
        list=findsymbol(context, name);
    }
    /* */
    if (!list) {
        if (hashget(intrinsics,name)) {
            sprintf(error_buffer(),ERROR_INCORRECTBRACKETS_MSG, name);
            error_raise(context, ERROR_INCORRECTBRACKETS, error_buffer(), ERROR_FATAL);
        } else {
            sprintf(error_buffer(),ERROR_SYMBOLNOTDEFINED_MSG, name);
            error_raise(context, ERROR_SYMBOLNOTDEFINED, error_buffer(), ERROR_FATAL);
        }
    } else if (!eval_islist(list)) {
        if (eval_isobjectref(list)) {
            
            if ((!set) && (!class_objectrespondstoselector(((expression_objectreference *) list)->obj, EVAL_INDEX))) {
                error_raise(context, ERROR_SYMBOLNOTINDEXABLE, ERROR_CLASSNOTINDEXABLE_MSG, ERROR_FATAL);
            } else if ((set) && (!class_objectrespondstoselector(((expression_objectreference *) list)->obj, EVAL_SETINDEX))) {
                error_raise(context, ERROR_SYMBOLNOTINDEXABLE, ERROR_CLASSNOTINDEXABLE_MSG, ERROR_FATAL);
            }
        } else if (list->type==EXPRESSION_FUNCTION_DEFINITION) {
            sprintf(error_buffer(),ERROR_INCORRECTBRACKETS_MSG,name);
            error_raise(context, ERROR_INCORRECTBRACKETS, error_buffer(), ERROR_FATAL);
        } else {
            sprintf(error_buffer(),ERROR_SYMBOLNOTINDEXABLE_MSG,name);
            error_raise(context, ERROR_SYMBOLNOTINDEXABLE, error_buffer(), ERROR_FATAL);
        }
    }
    
    /* Evaluate args and perform successive lookups */
    if (eval_islist(list)) {
        for (i=0;i<index->nargs;i++) {
            indx=interpretexpression(context, index->argslist[i]);
            if ((!indx)||(indx->type!=EXPRESSION_INTEGER)) {
                if (indx) freeexpression(indx);
                sprintf(error_buffer(), ERROR_INDEXNOTEVALUATABLE_MSG,i+1);
                error_raise(context, ERROR_INDEXNOTEVALUATABLE, error_buffer(), ERROR_FATAL);
                return NULL;
            } else {
                if ((set)&&(i==index->nargs-1)) {
                    expression *cset=cloneexpression(set);
                    if (cset) list=eval_setelement((expression_list *) list, ((expression_integer *) indx)->value, cset);
                    
                    freeexpression(indx);
                    break;
                } else {
                    list=eval_getelement((expression_list *) list, ((expression_integer *) indx)->value);
                    freeexpression(indx);
                }
            }
        }
        
        ret=cloneexpression(list);
    } else if (eval_isobjectref(list)) {
        /* If it's an object, call the indexing selectors */
        
        object *obj=eval_objectfromref(list);
        expression *indx[index->nargs+1];
        
        /* Interpret args */
        indx[0]=set;
        for (i=0;i<index->nargs;i++) {
            indx[i+1]=interpretexpression(context, index->argslist[i]);
            interpretcontextregisterfree(context, indx[i+1], (interpretcontextfreefunction *) freeexpression);
        }
        
        /* Now check that the indices are valid */
        /* REMOVED because some objects may not be indexed by numbers! */
        /*for (i=0;i<index->nargs; i++) if (!eval_isinteger(indx[i+1])) {
            sprintf(error_buffer(), ERROR_INDEXNOTEVALUATABLE_MSG,i+1);
            error_raise(context, ERROR_INDEXNOTEVALUATABLE, error_buffer(), ERROR_FATAL);
        }*/
        
        if (set) {
            ret=class_callselectorwithargslist(context, obj, EVAL_SETINDEX, index->nargs+1, indx);
        } else {
            ret=class_callselectorwithargslist(context, obj, EVAL_INDEX, index->nargs, indx+1);
        }
        
        for (i=0;i<index->nargs;i++) freeexpression(indx[i+1]);
    }
    
    if (nexp) freeexpression(nexp);
    
    return ret;
}

/* interpretexpression - Interprets an expression tree, perfoming lookups, operations and function calls as necessary.
 * Important: The expression returned by this function, if any, MUST be EVAL_FREEd by the caller
 *            when it is no longer needed.
 * Input:   (interpretcontext *) context - The context to use when looking for symbols.
 *          (expression *) exp           - The expression to interpret.
 * Output:
 * Returns: (expression *)               - the result of the interpretation, newly allocated.
 */
expression *interpretexpression(interpretcontext *context, expression *exp) {
    if (!exp) return NULL;
    
#ifdef EVAL_DETAILEDDEBUG
    printf("Interpreting expression ");
    expressionprint(exp);
    printf("\n");
#endif
    
    switch (exp->type) {
        case EXPRESSION_OPERATOR:
            {
                expression_operator *expop =(expression_operator *) exp;
                
                expression *left=expop->left;
                expression *right=expop->right;
                expression *ret=NULL;
                unsigned int freeargs=FALSE;
                
                /* Pre-evaluate arguments to operator unless an assignment is being made */
                if (expop->op->executeop!=opassign&&expop->op->executeop!=class_selectorop) {
                    left=interpretexpression(context, expop->left);
                    if (left) if (left->type==EXPRESSION_RETURN) return left;
                    right=interpretexpression(context, expop->right);
                    if (right) if (right->type==EXPRESSION_RETURN) {
                        if (left) freeexpression(left);
                        return right;
                    }
                    freeargs=TRUE;
                }
                
                
                /* Detect if either of the operands failed to evaluate, and if so return an unevaluated expression. */
                if (((left==NULL)&&(expop->left!=NULL))||((right==NULL)&&(expop->right!=NULL))) {
                    ret=newexpressionoperator(expop->op, expop->left, expop->right, TRUE);
                } else if (expop->op->executeop!=NULL) {
                    /* Otherwise, call the operator */
                    ret=(*expop->op->executeop)(context, left, right);
                } else {
                    sprintf(error_buffer(), ERROR_OPERATORNOTIMPLEMENTED_MSG, expop->op->symbol);
                    error_raise(context, ERROR_OPERATORNOTIMPLEMENTED, error_buffer(), ERROR_FATAL);
                }
                
                if (!ret) {
                    /* Last ditch - perhaps one of the arguments responds to a selector? */
                    if (expop->op->executeop==class_selectorop) {
                        /* If the op was the selectorop, it has failed and so we now need to evaluate the arguments */
                        left=interpretexpression(context, expop->left);
                        right=interpretexpression(context, expop->right);
                        freeargs=TRUE;
                    }
                    
                    if ((eval_isobjectref(left))) {
                        if (class_objectrespondstoselector(((expression_objectreference *) left)->obj, expop->op->selector)) {
                            ret=class_callselectorwithargslist(context, ((expression_objectreference *) left)->obj, expop->op->selector, 1, &right);
                        }
                    } else if ((left==NULL)&&(eval_isobjectref(right))) {
                        // A Prefix operator
                        if (class_objectrespondstoselector(((expression_objectreference *) right)->obj, expop->op->selector)) {
                            ret=class_callselectorwithargslist(context, ((expression_objectreference *) right)->obj, expop->op->selector, 0, NULL);
                        }
                    }
                    
                    /* Operator couldn't process arguments, so leave them unevaluated */
                    if (!ret) if ((left!=NULL)||(right!=NULL)) ret=newexpressionoperator(expop->op, left, right, TRUE);
                }
                    
                if (freeargs) {
                    if (left) freeexpression(left);
                    if (right) freeexpression(right);
                }
                
                return ret;
            }
            break;
        case EXPRESSION_INDEX:
            return interpretindex(context, (expression_index *) exp,NULL);
            break;
            
        case EXPRESSION_FUNCTION:
            return callfunction(context,(expression_function *) exp);
            break;
            
        case EXPRESSION_NAME:
            {
                expression *val=lookupsymbol(context,((expression_name *)exp)->name);
                if (val==NULL) {
                    /* printf("Name '%s' not recognized.\n", ((expression_name *)exp)->name); */
                    val=cloneexpression(exp);
                } /*else if (val->type==EXPRESSION_FUNCTION_DEFINITION) {
                    sprintf(error_buffer(),ERROR_XISAFUNCTION_MSG, ((expression_name *)exp)->name);
                    error_raise(context, ERROR_XISAFUNCTION, error_buffer(), ERROR_FATAL);
                }*/
                return val; /* No need to clone as lookupsymbol already returned a clone */
            }
            break;
        case EXPRESSION_LIST:
            {
                expression_list *newexp = newexpressionlist(NULL);

                /* Register this list for removal if an error is raised */
                interpretcontextregisterfree(context, newexp, (interpretcontextfreefunction *) freeexpression);
                
                for (linkedlistentry *e=((expression_list *) exp)->list->first; e!=NULL; e=e->next) {
                    expression *nexp=interpretexpression(context,(expression *) e->data);
                    linkedlist_addentry(newexp->list,nexp);
                    
                    if ((nexp)&&(nexp->type==EXPRESSION_RETURN)) {
                        return (expression *) newexp;
                    }
                }
                return (expression *) newexp;
            }
            break;
            
        case EXPRESSION_RETURN:
            break;
            
/* Expression types that don't need to be interpreted, just cloned. */
        case EXPRESSION_FLOAT:
        case EXPRESSION_INTEGER:
        case EXPRESSION_COMPLEX:
        case EXPRESSION_BOOL:
        case EXPRESSION_STRING:
        case EXPRESSION_OBJECT_REFERENCE:
        case EXPRESSION_NONE:
        case EXPRESSION_FUNCTION_DEFINITION:
            return cloneexpression(exp);
            break;
    }
    
    return NULL;
}

/* tryinterpretexpression - Interprets an expression tree, catching any errors that occur.
 * Important: This is NOT a public interface; it is expected that this would be called by public interfaces.
 * Input:   (interpretcontext *) context - The context to use when looking for symbols.
 *          (expression *) exp           - The expression to interpret.
 * Output:
 * Returns: (expression *)               - the result of the interpretation, newly allocated.
 */
expression *tryinterpretexpression(interpretcontext *context, expression *exp) {
    expression *ret=NULL;
    jmp_buf buf;
    
    if (setjmp(buf)) {
        /* Cleanup */
        //freerecordsinterpretcontext(context);
        
        ret=NULL;
    } else {
        error_saveenv(context, &buf);
        ret=interpretexpression(context, exp);
    }
    
    /* Clear the free records */
    interpretcontextclearfree(context);
    
    return ret;
}

/* interpret - Interprets an expression tree in the default context. One of the public interfaces to the interpreter.
 * Input:   (expression *) exp           - The expression to interpret.
 * Output:
 * Returns: (expression *)               - the result of the interpretation, newly allocated.
 */
expression *interpret(expression *exp) {
    return tryinterpretexpression(defaultinterpretcontext, exp);
}

/* execute - Parses and executes a string. One of the public interfaces to the interpreter.
 *           N.B. Use is discouraged for perfomance reasons. 
 *           Do not use for strings that are to be executed more than once.
 * Input:   (interpretcontext *) context - A context to use, or NULL for the default context.
 *          (char *) string              - The string to parse interpret.
 * Output:
 * Returns: (expression *)               - the result of the interpretation, newly allocated.
 */
expression *execute(interpretcontext *context, const char *string) {
    expression *exp=NULL;
    expression *ret=NULL;
    interpretcontext *ctxt=context;
    
    exp=parse((char *) string,NULL,NULL);
    if (exp) {
        if (ctxt==NULL) ctxt=defaultinterpretcontext;
        ret=tryinterpretexpression(ctxt, exp);
        freeexpression(exp);
    }
    
    return ret;
}

/* call - Calls a user-defined function defined in the default context with literal arguments
 *        One of the public interfaces to the interpreter.
 * Input:   (char *) name           - Name of the function
 *          int nargs               - Number of type/argument pairs.
 *          <variable number of arguments should then follow in pairs>
 *          Each pair should be:
 *              expressiontype type - Type of parameter
 *              void *value         - pointer to value
 * Output:
 * Returns: (expression *)          - the result of the interpretation, newly allocated.
 */
expression *call(char *name, unsigned int nargs, ...) {
    va_list a;
    expressiontype type;
    expression_functiondefinition *func=NULL;
    expression *ret=NULL;
    expression *arg;
    interpretcontext *localcontext=NULL;
    int argsok=TRUE;
    
    func = (expression_functiondefinition *) lookupsymbol(defaultinterpretcontext,name);
    if (func==NULL) {
        printf("User defined function '%s' not found.\n", name);
        return NULL;
    }
    if (func->type!=EXPRESSION_FUNCTION_DEFINITION) {
        printf("Symbol '%s' is not a function.\n", name);
        return NULL;
    }
    if (func->nargs!=nargs) {
        printf("User defined function '%s' expects %u arguments.\n",name,func->nargs);
        return NULL;
    }
    
    /* Create new context */
    localcontext=newinterpretcontext(defaultinterpretcontext, EVAL_LOCALCONTEXTTABLESIZE);
    if (!localcontext) return NULL;

    /* Add arguments to the local context */
    va_start(a, nargs);
    for (unsigned int i=0; i<nargs && argsok; i++) {
        type = va_arg(a, expressiontype);
        switch (type) {
            case EXPRESSION_FLOAT:
                arg=newexpressionfloat(va_arg(a, double));
                break;
            case EXPRESSION_INTEGER:
                arg=newexpressioninteger(va_arg(a, int));
                break;
            default:
                arg=NULL;
                argsok=FALSE;
                break;
        }
        if (!localcontext->symbols) localcontext->symbols=hashcreate(localcontext->size);
        if (argsok && arg && localcontext->symbols) hashinsert(localcontext->symbols, ((expression_name *) (func->argslist[i]))->name, arg);
    }
    va_end(a);
    
    /* Perform the call */
    ret=tryinterpretexpression(localcontext,func->eval);
    
    /* EVAL_FREE everything left in the local context, including the arguments */
    freeinterpretcontext(localcontext);
    
    return ret;
}
