/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* 
 * Morpho's default CLI interface.
 */

#ifndef Morpho_cli_h
#define Morpho_cli_h

#define USE_GLFW
#define USE_LIBDRAWTEXT
//#define USE_VR

// Warning: Linenoise doesn't work with XCode — so best to deactivate this while debugging.
#ifndef DEBUG
#define USE_LINENOISE
#endif

#ifdef USE_GLFW
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#endif

#ifdef USE_LIBDRAWTEXT
#include <drawtext.h>
#endif

#ifdef USE_LINENOISE
#include "linenoise.h"
#endif

#ifdef USE_VR
#include "vr.h"
#endif

#include "linkedlist.h"
#include "eval.h"

#define INPUT_BUFFER_SIZE 256

#define ERROR_CLI_MIX2D                  0x10100
#define ERROR_CLI_MIX2D_MSG              "Can't mix 2d and 3d objects."

typedef enum { CLI_FROMLIST, CLI_FROMCOMMANDLINE } cli_mode;

typedef struct clicontinues {
    cli_mode mode;
    linkedlist *commands;
    linkedlistentry *next;
    char *buffer;
    size_t bufferlength;
} cli_continuationinfo;

void cliinitialize(void);
void clifinalize(void);

void cli(char *fin, char *fout, int quiet);

#endif
