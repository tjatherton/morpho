/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "linalg.h"

#include CBLAS_INCLUDE

/* 
 * Type specific routines
 */

size_t linalg_sizefortype(matrixtype type) {
    size_t size=0;
    
    switch (type) {
        case MATRIX_FLOAT:
            size=sizeof(double);
            break;
        default:
            break;
    }
    
    return size;
}

void linalg_printelement(matrixobject *obj, unsigned int i, unsigned int j) {
    switch (obj->type) {
        case MATRIX_FLOAT:
            if (obj->data) printf("%g",((double *) obj->data)[i+j*obj->nrows]);
            break;
        default:
            break;
    }
}

void linalg_zeroelement(matrixobject *obj, unsigned int i, unsigned int j) {
    switch (obj->type) {
        case MATRIX_FLOAT:
            if (obj->data) ((double *) obj->data)[i+j*obj->nrows]=0;
            break;
        default:
            break;
    }
}

expression *linalg_getelement(matrixobject *obj, unsigned int i, unsigned int j) {
    expression *ret=NULL;
    
    /* TODO: Should check i and j are in bounds */
    
    switch (obj->type) {
        case MATRIX_FLOAT:
            ret = newexpressionfloat( ((double *) obj->data)[i+j*obj->nrows] );
            
            break;
        default:
            break;
    }
    
    return ret;
}

void linalg_setelement(interpretcontext *context, matrixobject *obj, unsigned int i, unsigned int j, expression *exp) {
    /* TODO: Should check i and j are in bounds */
    
    switch (obj->type) {
        case MATRIX_FLOAT:
            if (eval_isreal(exp)) {
                ((double *) obj->data)[i+j*obj->nrows] = eval_floatvalue(exp);
            } else if (eval_isnumerical(exp)) {
                /* TODO: Promote matrix to complex type */
            } else {
                error_raise(context, ERROR_LINALG_SETINDEX_ARG, ERROR_LINALG_SETINDEX_ARG_MSG, ERROR_FATAL);
            }
            break;
        default:
            break;
    }
}

/*
 * Matrix constructor routines
 */


expression *linalg_matrix_new(matrixtype type, unsigned int nrows, unsigned int ncols, void *data) {
    matrixobject *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(matrixobject *) class_instantiate(class_lookup(LINALG_MATRIXCLASS));
    
    if (obj) {
        obj->type=type;
        obj->ncols=ncols;
        obj->nrows=nrows;
        obj->size=nrows*ncols*linalg_sizefortype(type);
        
        if (obj->size>0) {
            obj->data=EVAL_MALLOC(obj->size);
            
            for (unsigned int i=0; i<obj->nrows; i++) {
                for (unsigned int j=0; j<obj->ncols; j++) {
                    linalg_zeroelement(obj, i, j);
                }
            }
        }
        
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

int linalg_matrix_resize(matrixobject *mobj, unsigned int nrows, unsigned int ncols, int copy) {
    /* TODO: Incomplete!! */
    if ((nrows!=mobj->nrows)&&(ncols!=mobj->ncols)) {
        if (copy) {
            
        } else {
            size_t size = ncols*nrows*linalg_sizefortype(mobj->type);
            
            if (mobj->data) {
                mobj->data=EVAL_REALLOC(mobj->data, size);
            } else {
                mobj->data=EVAL_MALLOC(size);
            }
            
            if (mobj->data) {
                mobj->ncols=ncols;
                mobj->nrows=nrows;
                mobj->size=size;
                
                return TRUE;
            }
        }
    }
    
    return FALSE;
}

expression *linalg_matrix_clone(matrixobject *mobj) {
    expression_objectreference *new;
    
    new=(expression_objectreference *) linalg_matrix_new(mobj->type, mobj->nrows, mobj->ncols, mobj->data);
    
    if (new) {
        matrixobject *nobj=(matrixobject *) new->obj;
        
        memcpy(nobj->data, mobj->data, mobj->size);
    }
    
    return (expression *) new;
}

/*
 * General utility functions
 */

void linalg_matrix_print(matrixobject *mobj) {
    for (unsigned int i=0; i<mobj->nrows; i++) {
        for (unsigned int j=0; j<mobj->ncols; j++) {
            linalg_printelement(mobj, i, j);
            printf("\t");
        }
        printf("\n");
    }
}

void linalg_vector_print(matrixobject *mobj) {
    for (unsigned int i=0; i<mobj->nrows; i++) {
        linalg_printelement(mobj, i, 0);
        printf("\t");
    }
    printf("\n");
}

void linalg_serializetohashtable(matrixobject *mobj, hashtable *ht) {
    expression_list *list=newexpressionlist(NULL);
    
    if (list) {
        linkedlist *lst=list->list;
        
        for (unsigned int i=0; i<mobj->nrows; i++) {
            expression_list *row=newexpressionlist(NULL);
            
            if (row) {
                linkedlist *rlst=row->list;
                
                for (unsigned int j=0; j<mobj->ncols; j++) {
                    linkedlist_addentry(rlst, linalg_getelement(mobj, i, j));
                }
                
                linkedlist_addentry(lst, row);
            }
        }
        
        hashinsert(ht, LINALG_MATRIXSERIALIZATIONLABEL, list);
    }
}

/*
 * Matrix and vector operations
 */

expression *linalg_matrix_add(interpretcontext *context, matrixobject *a, matrixobject *b, int subtract, matrixobject *out) {
    matrixobject *mat = out;
    expression *ret=NULL;
    
    if ((a)&&(b)&&(a->nrows==b->nrows)&&(a->ncols==b->ncols)) {
        if (out) {
            if (!((out->nrows==b->nrows)&&(out->ncols==b->ncols))) {
                error_raise(context, ERROR_LINALG_INCOMPATIBLE, ERROR_LINALG_INCOMPATIBLE_MSG, ERROR_FATAL);
            }
        } else {
            /* TODO: Type checking */
            ret=linalg_matrix_new(a->type, a->nrows, a->ncols, NULL);
            
            if (ret) {
                mat=(matrixobject *) ((expression_objectreference *) ret)->obj;
                
                mat->clss=a->clss; // Ensure that the new object has the same class as the old one.
            }
        }
        
        if (mat) {
            double scale=1.0;
            if (subtract) scale=-1.0;
            
            memcpy(mat->data, a->data, a->size);
            cblas_daxpy(a->nrows*a->ncols, scale, b->data, 1, mat->data, 1);
        }
        
    } else {
        error_raise(context, ERROR_LINALG_INCOMPATIBLE, ERROR_LINALG_INCOMPATIBLE_MSG, ERROR_FATAL);
    }
    
    return ret;
}

expression *linalg_matrix_multiply(interpretcontext *context, matrixobject *a, matrixobject *b, matrixobject *out) {
    matrixobject *mat = out;
    expression *ret=NULL;
    
    if ((a)&&(b)&&(a->ncols==b->nrows)) {
        if (out) {
            if (!((a->ncols==b->nrows)&&(out->ncols==b->ncols))) {
                error_raise(context, ERROR_LINALG_INCOMPATIBLE, ERROR_LINALG_INCOMPATIBLE_MSG, ERROR_FATAL);
            }
        } else {
            /* TODO: Type checking */
            ret=linalg_matrix_new(a->type, a->nrows, b->ncols, NULL);
            
            if (ret) {
                mat=(matrixobject *) ((expression_objectreference *) ret)->obj;
                mat->clss=a->clss; // Ensure that the new object has the same class as the old one.
            }
        }
        
        if (mat) {
            memcpy(mat->data, a->data, a->size);
            cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, a->nrows, b->ncols, a->ncols, 1.0, a->data, a->nrows, b->data, b->nrows, 0.0, mat->data, mat->nrows);
        }
        
    } else {
        error_raise(context, ERROR_LINALG_INCOMPATIBLE, ERROR_LINALG_INCOMPATIBLE_MSG, ERROR_FATAL);
    }
    
    return ret;
}

expression *linalg_matrix_scale(interpretcontext *context, matrixobject *a, expression *scale, matrixobject *out) {
    matrixobject *mat = out;
    expression *ret=NULL;
    
    if (a) {
        if (out) {
            if (!((a->ncols==out->ncols)&&(out->ncols==a->ncols))) {
                error_raise(context, ERROR_LINALG_INCOMPATIBLE, ERROR_LINALG_INCOMPATIBLE_MSG, ERROR_FATAL);
            }
        } else {
            /* TODO: Type checking */
            ret=linalg_matrix_new(a->type, a->nrows, a->ncols, NULL);
            
            if (ret) {
                mat=(matrixobject *) ((expression_objectreference *) ret)->obj;
                mat->clss=a->clss; // Ensure that the new object has the same class as the old one.
            }
        }
        
        if (mat) {
            double sf = eval_floatvalue(scale);
            
            memcpy(mat->data, a->data, a->size);
            cblas_dscal(a->ncols*a->nrows, sf, mat->data, 1);
        }
        
    }
    
    return ret;
}

expression *linalg_vector_dot(interpretcontext *context, matrixobject *a, matrixobject *b) {
    
    if ((a->nrows!=b->nrows)&&(a->ncols!=1)&&(b->ncols!=1)) {
        error_raise(context, ERROR_LINALGV_INCOMPATIBLE, ERROR_LINALGV_INCOMPATIBLE_MSG, ERROR_FATAL);
    }
    
    /* TODO: Type checking */
    return newexpressionfloat(cblas_ddot(a->nrows, a->data, 1, b->data, 1));
}

/*
 * Matrix selectors
 */

/* Initializes the matrix */
expression *linalg_matrix_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj = (matrixobject *) obj;
    
    mobj->data=NULL;
    mobj->nrows=0; mobj->ncols=0;
    mobj->type=MATRIX_NONE;
    
    return NULL;
}

/* Frees the matrix */
expression *linalg_matrix_freei(object *obj, interpretcontext *context, int nargs, expression **args) {    matrixobject *mobj = (matrixobject *) obj;
    
    if (mobj->data) EVAL_FREE(mobj->data);
    
    return NULL;
}

/* Gets the matrix element at a particular index */
expression *linalg_matrix_indexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj = (matrixobject *) obj;
    unsigned int i, j;
    expression *ret = NULL;

    if (nargs!=2) {
        error_raise(context, ERROR_LINALG_INDEX, ERROR_LINALG_INDEX_MSG, ERROR_FATAL);
    } else {
        if ((eval_isinteger(args[0]))&&(eval_isinteger(args[1]))) {
            i=eval_integervalue(args[0]);
            j=eval_integervalue(args[1]);
            ret=linalg_getelement(mobj, i-1, j-1);
        } else {
            error_raise(context, ERROR_LINALG_INDEX_NUM, ERROR_LINALG_INDEX_NUM_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

/* Sets the matrix element at a particular index */
expression *linalg_matrix_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj = (matrixobject *) obj;
    unsigned int i, j;
    
    if (nargs!=3) {
        error_raise(context, ERROR_LINALG_SETINDEX, ERROR_LINALG_SETINDEX_MSG, ERROR_FATAL);
    } else {
        if ((eval_isinteger(args[1]))&&(eval_isinteger(args[2]))) {
            i=eval_integervalue(args[1]);
            j=eval_integervalue(args[2]);
            linalg_setelement(context, mobj, i-1, j-1, args[0]);
        } else {
            error_raise(context, ERROR_LINALG_SETINDEX_NUM, ERROR_LINALG_SETINDEX_NUM_MSG, ERROR_FATAL);
        }
    }
    
    return (expression *) cloneexpression(args[0]);
}

/* Prints a matrix */
expression *linalg_matrix_printi(object *obj, interpretcontext *context, int nargs, expression **args) {
    linalg_matrix_print((matrixobject *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

/* Clones a matrix */
expression *linalg_matrix_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {

    return linalg_matrix_clone((matrixobject *) obj);
}

/* Adds two matrices */
expression *linalg_matrix_addi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *a = (matrixobject *) obj;
    matrixobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(matrixobject *) ref->obj;
                
                ret=linalg_matrix_add(context, a, b, FALSE, NULL);
            }
        }
    }
    
    return ret;
}

/* Subtracts a matrix from another */
expression *linalg_matrix_subtracti(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *a = (matrixobject *) obj;
    matrixobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(matrixobject *) ref->obj;
                
                ret=linalg_matrix_add(context, a, b, TRUE, NULL);
            }
        }
    }
    
    return ret;
}


/* Multiplies two matrices */
expression *linalg_matrix_multiplyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *a = (matrixobject *) obj;
    matrixobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(matrixobject *) ref->obj;
                
                ret=linalg_matrix_multiply(context, a, b, NULL);
            }
        } else if (eval_isnumerical(args[0])) {
            ret=linalg_matrix_scale(context, a, args[0], NULL);
        }
    }
    
    return ret;
}

/* Serializes a matrix */
expression *linalg_matrix_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(2);
    matrixobject *mobj=(matrixobject *) obj;
    expression *ret=NULL;
    
    if (ht) {
        linalg_serializetohashtable(mobj, ht);
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
        
    }
    
    return ret;
}

/* Deerializes a matrix */
expression *linalg_matrix_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj=(matrixobject *) obj;
    expression *exp;
    expression *ret=NULL;
    
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        /* The runtime environment passes an object that responds to the lookup protocol as the first argument. */
        expression_objectreference *hashobject = (expression_objectreference *) args[0];
        
        /* Verify that the object passed responds to 'lookup'. */
        if (class_objectrespondstoselector(hashobject->obj, EVAL_LOOKUPSELECTORLABEL)) {
            /* Call the selector if so */
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, LINALG_MATRIXSERIALIZATIONLABEL);
            if (eval_islist(exp)) {
                mobj->type=MATRIX_FLOAT;
                ret=linalg_listtomatrix((expression_list *) exp, mobj);
            }
            
            if (exp) freeexpression(exp);
            
        }
    }
    
    return ret;

}

/*
 * Vector constructor routines
 */

expression *linalg_vector_new(matrixtype type, unsigned int nels, void *data) {
    matrixobject *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(matrixobject *) class_instantiate(class_lookup(LINALG_VECTORCLASS));
    
    if (obj) {
        obj->type=type;
        obj->nrows=nels;
        obj->ncols=1;
        obj->size=nels*linalg_sizefortype(type);
        
        if (obj->size>0) {
            obj->data=EVAL_MALLOC(obj->size);
            
            for (unsigned int i=0; i<obj->nrows; i++) {
                linalg_zeroelement((matrixobject *) obj, i, 0);
            }
        }
        
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/*
 * Vector selectors
 */

/* Multiplies a vector by a quantity */
expression *linalg_vector_multiplyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *a = (matrixobject *) obj;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isnumerical(args[0])) {
            ret=linalg_matrix_scale(context, a, args[0], NULL);
        }
    }
    
    return ret;
}

/* Divides a vector by a quantity */
expression *linalg_vector_dividei(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *a = (matrixobject *) obj;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isnumerical(args[0])) {
            expression *exp=newexpressionfloat(1.0);
            expression *div=NULL;
            
            if (exp) div=opdiv(context, exp, args[0]);
            
            if (div) ret=linalg_matrix_scale(context, a, div, NULL);
        }
    }
    
    return ret;
}

/* Gets the vector element at a particular index */
expression *linalg_vector_indexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj = (matrixobject *) obj;
    unsigned int i;
    expression *ret = NULL;
    
    if (nargs!=1) {
        error_raise(context, ERROR_LINALGV_INDEX, ERROR_LINALGV_INDEX_MSG, ERROR_FATAL);
    } else {
        if (eval_isinteger(args[0])) {
            i=eval_integervalue(args[0]);
            ret=linalg_getelement(mobj, i-1, 0);
        } else {
            error_raise(context, ERROR_LINALG_INDEX_NUM, ERROR_LINALG_INDEX_NUM_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

/* Sets the vector element at a particular index */
expression *linalg_vector_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    matrixobject *mobj = (matrixobject *) obj;
    unsigned int i;
    
    if (nargs!=2) {
        error_raise(context, ERROR_LINALGV_SETINDEX, ERROR_LINALGV_SETINDEX_MSG, ERROR_FATAL);
    } else {
        if (eval_isinteger(args[1])) {
            i=eval_integervalue(args[1]);
            linalg_setelement(context, mobj, i-1, 0, args[0]);
        } else {
            error_raise(context, ERROR_LINALG_SETINDEX_NUM, ERROR_LINALG_SETINDEX_NUM_MSG, ERROR_FATAL);
        }
    }
    
    return (expression *) cloneexpression(args[0]);
}

/* Prints a vector */
expression *linalg_vector_printi(object *obj, interpretcontext *context, int nargs, expression **args) {
    linalg_vector_print((matrixobject *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

/* Dot product */
expression *linalg_vector_doti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if ((ref->obj)&&(ref->obj->clss==obj->clss)) {
                ret=linalg_vector_dot(context, (matrixobject *) obj, (matrixobject *) ref->obj);
            }
        }
    }
    
    return ret;
}

/*
 * Intrinsic functions
 */

expression *linalg_listtomatrix(expression_list *list, matrixobject *mobj) {
    unsigned int nrows, ncols;
    expression *ret=NULL;
    
    /* TO DO: Should be some type checking here. */
    
    if(eval_listtomatrixsize(list, &nrows, &ncols)) {
        if (!mobj) {
            ret = linalg_matrix_new(MATRIX_FLOAT, nrows, ncols, NULL);
        } else {
            if (linalg_matrix_resize(mobj, nrows, ncols, FALSE)) {
                ret = (expression *) class_newobjectreference((object *) mobj);
            }
        }
        
        if (ret) {
            matrixobject *mobj = (matrixobject *) ((expression_objectreference *) ret)->obj;
            
            eval_listtodoublematrix(list, (double *) mobj->data, nrows, ncols, TRUE);
        }
    }
    
    return (expression *) ret;
}

expression *linalg_listtovector(expression_list *list, matrixobject *vobj) {
    unsigned int nels=eval_listlength(list);
    expression *ret=NULL;
    
    /* TO DO: Should be some type checking here. */
    
    if(nels>0) {
        if (!vobj) {
            ret = linalg_vector_new(MATRIX_FLOAT, nels, NULL);
        } else {
            if (linalg_matrix_resize(vobj, nels, 1, FALSE)) {
                ret = (expression *) class_newobjectreference((object *) vobj);
            }
        }
        
        if (ret) {
            matrixobject *vobj = (matrixobject *) ((expression_objectreference *) ret)->obj;
            
            eval_listtodoublearray(list, (double *) vobj->data, nels);
        }
    }
    
    return (expression *) ret;
}

expression *linalg_matrixi(interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    
    switch (nargs) {
        case 1:
            if (eval_islist(args[0])) {
                ret = linalg_listtomatrix((expression_list *) args[0], NULL);
            }
            break;
        case 2:
            if ((eval_isinteger(args[0]))&&(eval_isinteger(args[1]))) {
                unsigned int nrows=eval_integervalue(args[0]);
                unsigned int ncols=eval_integervalue(args[1]);
                
                ret = linalg_matrix_new(MATRIX_FLOAT, nrows, ncols, NULL);
            }
            break;
    }
    
    return ret;
}

expression *linalg_vectori(interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    
    if (nargs==1) {
        if (eval_islist(args[0])) {
            ret = linalg_listtovector((expression_list *) args[0], NULL);
        } else if (eval_isinteger(args[0])) {
            ret = linalg_vector_new(MATRIX_FLOAT, eval_integervalue(args[0]), NULL);
        }
    }
    
    return ret;
}

/*
 * Initialization
 */

void linalginitialize(void) {
    classintrinsic *cls;
    
    /* Matrix class */
    cls=class_classintrinsic(LINALG_MATRIXCLASS, class_lookup(EVAL_OBJECT), sizeof(matrixobject), 10);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, linalg_matrix_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, linalg_matrix_freei);
    class_registerselector(cls, EVAL_CLONE, FALSE, linalg_matrix_clonei);
    class_registerselector(cls, LINALG_MATRIXPRINT, FALSE, linalg_matrix_printi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, linalg_matrix_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, linalg_matrix_deserializei);
    
    class_registerselector(cls, EVAL_INDEX, FALSE, linalg_matrix_indexi);
    class_registerselector(cls, EVAL_SETINDEX, FALSE, linalg_matrix_setindexi);

    class_registerselector(cls, OPERATOR_ADD_SELECTOR, FALSE, linalg_matrix_addi);
    class_registerselector(cls, OPERATOR_SUBTRACT_SELECTOR, FALSE, linalg_matrix_subtracti);
    class_registerselector(cls, OPERATOR_MULTIPLY_SELECTOR, FALSE, linalg_matrix_multiplyi);
    
    /* Vector class */
    cls=class_classintrinsic(LINALG_VECTORCLASS, class_lookup(EVAL_OBJECT), sizeof(matrixobject), 7);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, linalg_matrix_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, linalg_matrix_freei);
    class_registerselector(cls, EVAL_CLONE, FALSE, linalg_matrix_clonei);
    class_registerselector(cls, LINALG_MATRIXPRINT, FALSE, linalg_vector_printi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, linalg_matrix_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, linalg_matrix_deserializei);
    
    class_registerselector(cls, OPERATOR_ADD_SELECTOR, FALSE, linalg_matrix_addi);
    class_registerselector(cls, OPERATOR_SUBTRACT_SELECTOR, FALSE, linalg_matrix_subtracti);
    class_registerselector(cls, OPERATOR_DOT_SELECTOR, FALSE, linalg_vector_doti);
    class_registerselector(cls, OPERATOR_MULTIPLY_SELECTOR, FALSE, linalg_vector_multiplyi);
    class_registerselector(cls, OPERATOR_DIVIDE_SELECTOR, FALSE, linalg_vector_dividei);
    
    class_registerselector(cls, EVAL_INDEX, FALSE, linalg_vector_indexi);
    class_registerselector(cls, EVAL_SETINDEX, FALSE, linalg_vector_setindexi);
    
    /* Intrinsic functions */
    intrinsic(LINALG_MATRIXCLASS,FALSE,linalg_matrixi,NULL);
    intrinsic(LINALG_VECTORCLASS,FALSE,linalg_vectori,NULL);
}
