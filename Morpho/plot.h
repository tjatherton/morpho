/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_plot_h
#define Morpho_plot_h

#include "eval.h"
#include "graphics.h"

#define PLOT_TICKSRECORD_NENTRIES 5

typedef struct {
    int nticks;
    double majortickstart;
    double majortickseparation;
    double majortickend;
    int digits;
} plot_ticksrecord;

typedef struct {
    graphics_bbox overall_bbox;
    graphics_bbox plotregion_bbox;
    float m[2];
    float c[2];
} plot_transformation;

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    linkedlist *list;
    graphics_bbox plotrange;
    plot_ticksrecord ticks[2];
    char *axeslabels[2];
    float aspectratio;
    int framed;
} plot_object;

#define HISTOGRAM_DEFAULT_NBINS 5

void plot_determineticks(plot_ticksrecord *record, float lower, float upper);

void plotinitialize(void);

expression *plot_serializei(object *obj, interpretcontext *context, int nargs, expression **args);

#endif
