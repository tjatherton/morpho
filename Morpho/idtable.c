/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "idtable.h"

/* Creates an empty idtable */
idtable *idtable_create(int size) {
    idtable *table = NULL;
    int i;
    
    if(size < 1) return NULL;
    
    /* Allocate the table itself. */
    table = EVAL_MALLOC(sizeof(idtable));
    if(!table) return NULL;
    
    /* Allocate pointers to the head nodes. */
    table->table = EVAL_MALLOC(sizeof(identry*) * size);
    if(!table->table) {
        EVAL_FREE(table);
        return NULL;
    }
    
    for(i=0; i<size; i++) {
        table->table[i] = NULL;
    }
    
    table->size = size;
    table->lo=0;
    table->hi=0;
    table->nentries=0;
    
    return table;
}

/* Allocates a new identry */
identry *idtable_newentry(uid id, void *value) {
    identry *new = EVAL_MALLOC(sizeof(identry));
    
    if (new) {
        new->id=id;
        new->value = value;
        new->next = NULL;
    }
    
    return new;
}

/* Inserts a new entry with a specified id */
/* WARNING: Must always check EXTERNALLY that a pair already exists and free the value */
void idtable_insertwithid(idtable *t, uid id, void *value) {
    int bin = 0;
    identry *new = NULL;
    identry *next = NULL;
    identry *last = NULL;
    
    bin = (id % t->size); /* This mapping function can probably be optimized */
    
    /* Does this id already exist? */
    for (next = t->table[bin]; next!=NULL && next->id!=id; next=next->next) last=next;
    
    /* It does so replace it. */
    if(next != NULL && next->id==id) {
        next->value = value;
    } else {
    /* It doesn't so add the new pair */
        new = idtable_newentry(id,value);
        
        /* If this is the first element in the list, add it to the idtable. */
        if(next == t->table[bin]) {
            new->next = next;
            t->table[bin] = new;
            t->nentries++;
        } else {
            /* Otherwise just add it in */
            new->next=next;
            if (last) {
                last->next = new;
                t->nentries++;
            } else {
                /* Something went wrong! */
                EVAL_FREE(new);
            }
        }
    }
    
    if (id>t->hi) t->hi=id;
    else if (id<t->lo) t->lo=id;
}

/* Inserts a new entry and returns its id */
uid idtable_insert(idtable *t, void *value) {
    uid new=t->hi+1;
    
    idtable_insertwithid(t, new, value);
    return new;
}

/* Removes an entry with specified id. Warning: Does not free attached data. */
void idtable_remove(idtable *t, uid id) {
    int bin = 0;
    identry *e=NULL, *last=NULL;
    
    bin = (id % t->size); /* This mapping function can probably be optimized */
    
    if (t->table[bin]) {
        e=t->table[bin];
        
        /* Is it the first entry? */
        if (e->id==id) {
            t->table[bin]=e->next; /* The bin now links to the second element */
            EVAL_FREE(e); /* And it's safe to remove the first */
            t->nentries--;
        } else {
            /* If not, search for it. */
            last=e;
            for (e=e->next; e!=NULL; e=last->next) {
                if (e->id==id) {
                    last->next=e->next; /* Delink from list */
                    EVAL_FREE(e);
                    t->nentries--;
                    break;
                } else last=e;
            }
        }
    }
    
}

/* How many entries are in a table? */
unsigned int idtable_count(idtable *t) {
    unsigned int n=0;
    
    if (t)  n=t->nentries;
    
    return n;
}

/* Finds an entry with specified id */
void *idtable_get(idtable *t, uid id) {
    int bin = 0;
    identry *e=NULL;
    
    if (!t) return NULL; 
    
    bin = (id % t->size); /* This mapping function can probably be optimized */
    
    for (e = t->table[bin]; e!=NULL; e=e->next) if (e->id==id) return e->value;
    
    return NULL;
}

/* Is a specified id present? */
int idtable_ispresent(idtable *t, uid id) {
    int bin = 0;
    identry *e=NULL;
    
    bin = (id % t->size); /* This mapping function can probably be optimized */
    
    for (e = t->table[bin]; e!=NULL; e=e->next) if (e->id==id) return TRUE;
    
    return FALSE;
}


/* Maps a function over every entry in the idtable */
void idtable_map(idtable *t, idtablemapfunction *func, void *ref) {
    identry *e=NULL;
    
    if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
        for (e=t->table[i]; e!=NULL; e=e->next) func(e->id,e->value,ref);
    }
}

/* Maps a function over every pair of entries in the idtable, optionally checking both to make sure if there's a corresponding element in t2. */
void idtable_maptuples(idtable *t, idtable *t2, idtablemaptuplefunction *func, void *ref) {
    identry *e=NULL, *e2=NULL;
    
    if (t2) {
        /* Check against t2 */
        if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
            for (e=t->table[i]; e!=NULL; e=e->next) {
                if (idtable_ispresent(t, e->id)) for (unsigned int j=i; i<t->size; j++) { /* Loop over remaining bins */
                    for (e2=(j==i ? e->next : t->table[j]); e2!=NULL; e2=e2->next) {
                        if (idtable_ispresent(t2, e2->id)) func(e->id, e2->id, e->value, e2->value, ref);
                    }
                }
            }
        }
    } else {
        /* Don't check */
        if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
            for (e=t->table[i]; e!=NULL; e=e->next) {
                for (unsigned int j=i; j<t->size; j++) { /* Loop over remaining bins */
                    for (e2=(j==i ? e->next : t->table[j]); e2!=NULL; e2=e2->next) {
                        func(e->id, e2->id, e->value, e2->value, ref);
                    }
                }
            }
        }
    }
}

/* Maps a function over every entry in the idtable in sequential order of ids. 
   Less efficient than idtable_map so only use if necessary */
void idtable_mapseq(idtable *t, idtablemapfunction *func, void *ref) {
    for (uid i=t->lo; i<t->hi+1;i++) {
        if (idtable_ispresent(t, i)) {
            func(i,idtable_get(t, i),ref);
        }
    }
}

/* Maps a function over all entries in t that have a corresponding element in t2. */
void idtable_mapintersection(idtable *t, idtable *t2, idtablemapfunction *func, void *ref) {
    identry *e=NULL;
    
    if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
        for (e=t->table[i]; e!=NULL; e=e->next) {
            if (idtable_ispresent(t2, e->id)) func(e->id,e->value,ref);
        }
    }
}

/* Maps a function over all entries in t that don't have a corresponding element in t2. */
void idtable_mapcomplement(idtable *t, idtable *t2, idtablemapfunction *func, void *ref) {
    identry *e=NULL;
    
    if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
        for (e=t->table[i]; e!=NULL; e=e->next) {
            if (!idtable_ispresent(t2, e->id)) func(e->id,e->value,ref);
        }
    }
}

/* Maps a function over every entry in the idtable, but allow the function to abort the map.  */
void idtable_mapabort(idtable *t, idtablemapabortfunction *func, void *ref) {
    identry *e=NULL;
    int abort=FALSE;
    
    if (t) for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
        for (e=t->table[i]; e!=NULL; e=e->next) func(e->id,e->value,ref,&abort);
        if (abort) break;
    }
}

/* A generic map function that just calls EVAL_FREE on the argument el */
void idtable_genericfreemapfunction(uid id, void *el, void *ref) {
    EVAL_FREE(el);
}

/* A generic map function that just calls freeexpression on the argument el */
void idtable_genericfreeexpressionmapfunction(uid id, void *el, void *ref) {
    freeexpression((expression *) el);
}

/* Frees every element in an idtable. Warning: Does not free attached data. */
void idtable_free(idtable *t) {
    identry *e=NULL;
    identry *next=NULL;
    
    if ((t) && (t->table)) {
        for (unsigned int i=0; i<t->size; i++) { /* Loop over bins */
            for (e=t->table[i]; e!=NULL; e=next) {
                next=e->next; EVAL_FREE(e);
            }
        }
        
        EVAL_FREE(t->table);
    }

    EVAL_FREE(t);
}

/* -- idtable class -- */

/* Initializes the hashtable */
expression *idtable_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    
    hobj->table=idtable_create(IDTABLE_INITIALSIZE);
    
    return NULL;
}

/* Frees the hashtable */
expression *idtable_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    
    if (hobj->table) {
        idtable_map(hobj->table, idtable_genericfreeexpressionmapfunction, NULL);
        idtable_free(hobj->table);
    }
    
    return NULL;
}

/* Adds an entry */
expression *idtable_addi(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    expression_integer *key;
    expression *ret=FALSE;
    
    if ((hobj)&&(hobj->table)) {
        if (nargs==2) {
            key = (expression_integer *) args[0];
            if (eval_isinteger(key)) {
                idtable_insertwithid(hobj->table, key->value, cloneexpression(args[1]));
                ret=(expression *) class_newobjectreference(obj);
            }
        } else if (nargs==1) {
            uid id=idtable_insert(hobj->table, cloneexpression(args[0]));
            if (id) ret=newexpressioninteger(id);
        }
    }
    
    return (expression *) ret;
}

/* Adds an entry using setindex */
expression *idtable_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    expression_integer *key;
    
    if ((hobj)&&(hobj->table)&&(nargs==2)) {
        key = (expression_integer *) args[1];
        if (eval_isinteger(key)) {
            idtable_insertwithid(hobj->table, key->value, cloneexpression(args[0]));
        }
        
    }
    
    return (expression *) class_newobjectreference(obj);
}


/* Removes an entry */
expression *idtable_removei(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    expression_integer *key;
    
    if ((hobj)&&(hobj->table)&&(nargs==1)) {
        key = (expression_integer *) args[0];
        if (eval_isinteger(key)) {
            idtable_remove(hobj->table, key->value);
        }
        
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Looks up an entry */
expression *idtable_lookupi(object *obj, interpretcontext *context, int nargs, expression **args) {
    idtableobject *hobj = (idtableobject *) obj;
    expression_integer *key;
    expression *value=NULL;
    
    if ((hobj)&&(hobj->table)&&(nargs==1)) {
        key = (expression_integer *) args[0];
        if (eval_isinteger(key)) {
            value=idtable_get(hobj->table, key->value);
            if (value) return cloneexpression(value);
            else return newexpressionnone();
        }
        
    }
    
    return NULL;
}

#include <time.h>

void idtable_testforsize(unsigned int nels) {
    unsigned int size = 1000;
    unsigned int maxocc = 256;
    
    clock_t start=0, end=0;
    
    idtable *table=idtable_create(size);
    uid *ids = EVAL_MALLOC(sizeof(uid)*nels);
    int occupancy[maxocc];
    
    for (unsigned int i=0; i<maxocc; i++) occupancy[i]=0;
    
    printf("%u ",nels);
    if (table) {
        start = clock();
        for (unsigned int i=0; i<nels; i++) {
            ids[i] = idtable_insert(table, NULL);
        }
        end = clock();
        printf("%g ", ((double) end-start)/((double) CLOCKS_PER_SEC)/((double ) nels));
        
        start = clock();
        for (unsigned int i=0; i<nels; i++) {
            idtable_get(table, ids[i]);
        }
        end = clock();
        printf("%g\n", ((double) end-start)/((double) CLOCKS_PER_SEC)/((double ) nels));
        
       /* for (unsigned int i=0; i<table->size; i++) {
            unsigned int cnt = 0;
            
            for (identry *e = table->table[i]; e!=NULL; e=e->next) cnt++;
            
            if (cnt<maxocc) occupancy[cnt]+=1;
            //else printf("Occupancies full.\n");
            
            if (cnt>mocc) mocc = cnt;
        } */
        
        /*printf("Distribution of values.\n");
        for (unsigned int i=0; i<=mocc; i++) {
            printf("%u : %u\n", i, occupancy[i]);
        }*/
        
        //for (next = t->table[bin]; next!=NULL && next->id!=id; next=next->next) last=next;
        
        idtable_free(table);
    }
    
    if(ids) EVAL_FREE(ids);
}

/** @define Test the hashtable implementation */
void idtable_test(void) {

    for (unsigned int n=1; n<10000000; n=n*2) {
        idtable_testforsize(n);
    }
    exit(0);
}

void idtableinitialize(void) {
    //idtable_test();
    
    //classintrinsic *cls;
    
    /* idtable class */
    /*cls=class_classintrinsic(GEOMETRY_IDTABLECLASS, class_lookup(EVAL_OBJECT), sizeof(idtableobject), 7);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, idtable_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, idtable_freei);
    class_registerselector(cls, EVAL_ADDSELECTORLABEL, FALSE, idtable_addi);
    class_registerselector(cls, EVAL_REMOVESELECTORLABEL, FALSE, idtable_removei);
    class_registerselector(cls, EVAL_LOOKUPSELECTORLABEL, FALSE, idtable_lookupi);
    //class_registerselector(cls, EVAL_SERIALIZE, FALSE, class_hashtable_serializei);
    //class_registerselector(cls, EVAL_DESERIALIZE, FALSE, class_hashtable_deserializei);
    //class_registerselector(cls, EVAL_CLONE, FALSE, class_hashtable_clonei);
    
    class_registerselector(cls, EVAL_SETINDEX, FALSE, idtable_setindexi);
    class_registerselector(cls, EVAL_INDEX, FALSE, idtable_lookupi);*/

}

