/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_plot3d_h
#define Morpho_plot3d_h

#include "graphics3d.h"

#include "eval.h"
#include "graphics.h"
#include "plot.h"

typedef struct {
    int nticks;
    double majortickstart;
    double majortickseparation;
    double majortickend;
    int digits;
} plot3d_ticksrecord;

typedef struct {
    CLASS_GENERICOBJECTDATA
    linkedlist *list;
    float viewdirection[3];
    float viewvertical[3];
    graphics3d_bbox plotrange;
    int cliptrianglesbynormal;
    int showmesh;
    plot3d_ticksrecord ticks[3];
    char *axeslabels[3];
} plot3d_object;

void plot3dinitialize(void);

#endif
