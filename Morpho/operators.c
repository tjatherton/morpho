/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "operators.h"

hashtable *operators;

/* Register operators */
operator *newoperator(char *op, int precedence, operatortype type, nudtype *nud, ledtype *led, executeoptype *executeop, diffexpfunctype *diff, char *selector) {
    operator *newop = EVAL_MALLOC(sizeof( operator )+strlen(op)+1);
    
    if(!newop) {
		return NULL;
	}
    
    newop->symbol=(char *) newop+sizeof(operator);
    strcpy(newop->symbol,op);
    
    newop->precedence=precedence;
    newop->type=OPERATOR_INFIX;
    newop->nud=nud;
    newop->led=led;
    newop->executeop=executeop;
    newop->diff=diff;
    newop->selector=selector;
    
    hashinsert( operators, op, (void *) newop);
    
    return newop;
}

operator *operator_infix(char *op, int precedence, nudtype *nud, ledtype *led, executeoptype *executeop, diffexpfunctype *diff, char *selector) {
    return newoperator(op, precedence, OPERATOR_INFIX, nud, led, executeop, diff, selector);
}

operator *operator_prefix(char *op, nudtype *nud, executeoptype *executeop, diffexpfunctype *diff, char *selector) {
    return newoperator(op, 0, OPERATOR_PREFIX, nud, NULL, executeop, diff, selector);
}

operator *operator_symbol(char *op) {
    return newoperator(op,0,OPERATOR_SYMBOL,NULL,NULL,NULL,NULL,NULL);
}

/* Operator definitions */

/* Assignment */
expression *opassign(interpretcontext *context, expression *left, expression *right) {
    expression *result=NULL;
    if (left==NULL||right==NULL) return NULL;
    interpretcontext *ctxt=context;
    expression *shouldfree=NULL;
    
    /* Check for class definitions */
    if (left->type==EXPRESSION_OPERATOR) {
        expression_operator *op=(expression_operator *) left;
        if (!strcmp(op->op->symbol,"@")) {
            if (op->right) if (op->right->type==EXPRESSION_NAME)
            class_classuser(((expression_name *) op->right)->name, right);
            return newexpressionnone();
        }
    }
    
    /* Check for the special case that we're inside a selector */
    if (context->parent) if (context->parent->insideselector) {
        if (context->parent->symbols && hashget(context->parent->symbols, ((expression_name *)left)->name)) {
            /* Simply switch the destination context if so. */
            ctxt=context->parent;
        }
    }
    
    /* Check if we're assigning to an indexed object */
    if (left->type==EXPRESSION_INDEX) {
        result=interpretexpression(context, right);
        if (result) {
            expression *ret = interpretindex(context, (expression_index *) left, result);
            freeexpression(result);
            return ret;
        } else return NULL;
    }
    
    /* Otherwise, can only assign to names and functions! */
    if (!((left->type==EXPRESSION_NAME)||(left->type==EXPRESSION_FUNCTION))) return NULL;
    
    /* Check if the variable exists in the current context and flag it for later removal. 
       We wait because it may be needed in the evaluation of the rhs. */
    if (ctxt->symbols && (result=hashget(ctxt->symbols, ((expression_name *)left)->name))) {
        shouldfree=result;
    }

    /* Define a variable in the current context */
    if (left->type==EXPRESSION_NAME) {
        result=interpretexpression(context, right);
        if (!ctxt->symbols) ctxt->symbols=hashcreate(ctxt->size);
        if (result && ctxt->symbols) {
            hashinsert(ctxt->symbols, ((expression_name *)left)->name, result);
        }
    }
    
    /* Define a function in the current context */
    if (left->type==EXPRESSION_FUNCTION) {
        expression_function *func=(expression_function *) left;
        
        if (eval_isname(func->name)) {
            result = (expression *) newexpressionfunctiondefinition((expression_function *) left, right, TRUE);
            if (!ctxt->symbols) ctxt->symbols=hashcreate(ctxt->size);
           
            if (result && ctxt->symbols) {
                hashinsert(ctxt->symbols, ((expression_name *) func->name)->name, result);
            }
        }
    }
    
    if (shouldfree) freeexpression(shouldfree);
    
    return cloneexpression(result);
}

/* Semicolon (separates expressions) */
expression *opsemicolon(interpretcontext *context, expression *left, expression *right) {
    expression *ret=NULL;
    if (right) {
        ret=cloneexpression(right);
    } else {
        ret=newexpressionnone();
    }
    
    return ret;
}

/* 
 * Arithmatic operators
 */

typedef struct {
    expressiontype type;
    int i[2];
    double f[2];
    complex double c[2];
} opresolvedoperands;

/* evalresolvetype - Resolves the types that result from arithmatic operators
 * Input:   (expression *) exp - an expression to be printed.
 * Output:
 * Returns:
 */
int evalresolvetype(expression *left, expression *right, opresolvedoperands *resolvedoperands) {
    if ((!left)||(!right)||(!resolvedoperands)) return FALSE;
    
    /* The left operand is an object... */
    if (eval_isobjectref(left)) {
        resolvedoperands->type=EXPRESSION_OBJECT_REFERENCE;
        resolvedoperands->i[0]=((expression_integer *)left)->value;
        resolvedoperands->i[1]=((expression_integer *)right)->value;
        return TRUE;
        
    } else if (eval_isobjectref(right)) {
        /* The right operand is an object, so commute the order */
        resolvedoperands->type=EXPRESSION_OBJECT_REFERENCE;
        resolvedoperands->i[1]=((expression_integer *)left)->value;
        resolvedoperands->i[0]=((expression_integer *)right)->value;
        return TRUE;
        
    } else if (eval_islist(left) || eval_islist(right)) {
        /* If either of the operands are lists, resolve it as list. */
        resolvedoperands->type=EXPRESSION_LIST;
        return TRUE;
    } else if ((!eval_isnumerical(left))||(!eval_isnumerical(right))) {
        /* If either of the operands are nonnumerical, do not resolve the type. */
        resolvedoperands->type=EXPRESSION_NONE;
        return TRUE;
    }
    
    if (left->type==right->type) {
        resolvedoperands->type=left->type;
        switch (left->type) {
            case EXPRESSION_INTEGER:
                resolvedoperands->i[0]=((expression_integer *)left)->value;
                resolvedoperands->i[1]=((expression_integer *)right)->value;
                break;
            case EXPRESSION_FLOAT:
                resolvedoperands->f[0]=((expression_float *)left)->value;
                resolvedoperands->f[1]=((expression_float *)right)->value;
                break;
            case EXPRESSION_COMPLEX:
                resolvedoperands->c[0]=((expression_complex *)left)->value;
                resolvedoperands->c[1]=((expression_complex *)right)->value;
                break;
            default:
                break;
        }
    } else {
        /* Start off with integers, and promote the type if floats or complex numbers are present */
        resolvedoperands->type=EXPRESSION_INTEGER;
        if ((left->type==EXPRESSION_FLOAT)||(right->type==EXPRESSION_FLOAT)) resolvedoperands->type=EXPRESSION_FLOAT;
        if ((left->type==EXPRESSION_COMPLEX)||(right->type==EXPRESSION_COMPLEX)) resolvedoperands->type=EXPRESSION_COMPLEX;
        
        /* Now insert the left operand */
        switch (left->type) {
            case EXPRESSION_INTEGER:
                switch (resolvedoperands->type) {
                    case EXPRESSION_INTEGER: resolvedoperands->i[0]=((expression_integer *)left)->value; break;
                    case EXPRESSION_FLOAT: resolvedoperands->f[0]=(double) ((expression_integer *)left)->value; break;
                    case EXPRESSION_COMPLEX: resolvedoperands->c[0]=(complex double) ((expression_integer *)left)->value; break;
                    default: break;
                }
                break;
            case EXPRESSION_FLOAT:
                /* Only need to check for float and complex numbers */
                if (resolvedoperands->type==EXPRESSION_FLOAT) {
                    resolvedoperands->f[0]=((expression_float *)left)->value;
                } else if (resolvedoperands->type==EXPRESSION_COMPLEX) {
                    resolvedoperands->c[0]=(complex double) ((expression_float *)left)->value;
                }
                break;
            case EXPRESSION_COMPLEX:
                /* Complex is the highest type */
                resolvedoperands->c[0]=((expression_complex *)left)->value;
                break;
            default:
                break;
        }
        
        /* Now insert the right operand */
        switch (right->type) {
            case EXPRESSION_INTEGER:
                switch (resolvedoperands->type) {
                    case EXPRESSION_INTEGER: resolvedoperands->i[1]=((expression_integer *)right)->value; break;
                    case EXPRESSION_FLOAT: resolvedoperands->f[1]=(double) ((expression_integer *)right)->value; break;
                    case EXPRESSION_COMPLEX: resolvedoperands->c[1]=(complex double) ((expression_integer *)right)->value; break;
                    default: break;
                }
                break;
            case EXPRESSION_FLOAT:
                /* Only need to check for float and complex numbers */
                if (resolvedoperands->type==EXPRESSION_FLOAT) {
                    resolvedoperands->f[1]=((expression_float *)right)->value;
                } else if (resolvedoperands->type==EXPRESSION_COMPLEX) {
                    resolvedoperands->c[1]=(complex double) ((expression_float *)right)->value;
                }
                break;
            case EXPRESSION_COMPLEX:
                /* Complex is the highest type */
                resolvedoperands->c[1]=((expression_complex *)right)->value;
                break;
            default:
                break;
        }
    }
    
    return TRUE;
}

/* Calls a named selector on the left hand object, with the right as argument */
expression *opredirecttoselector(interpretcontext *context, char *selector, expression_objectreference *left, expression *right) {
    expression *ret=NULL;
    
    if (eval_isobjectref(left)&&(left->obj)) {
        if (class_objectrespondstoselector(left->obj, selector)) {
            ret=class_callselectorwithargslist(context, left->obj, selector, 1, &right);
        }
    } else if (eval_isobjectref(right)) {
        /* TODO and WARNING:
           Here we assume that an object's op with a non-object (presumably a number) always commutes.
           E.g. that 1 + OBJECT = OBJECT + 1
           What should happen is that objects should define a OPLEFT selector which should be directed to here... (maybe?)
           In any case this is a major
         */
        expression_objectreference *r = (expression_objectreference *) right;
        if (right->type) {
            ret=class_callselectorwithargslist(context, r->obj, selector, 1, (expression **) &left);
        }
    }
    
    return ret;
}

expression *oplistapply(interpretcontext *context, executeoptype *op, expression *left, expression *right) {
    unsigned int nl=0, nr=0, nn=0;
    linkedlistentry *el=NULL;
    linkedlistentry *er=NULL;
    expression_list *newlist=newexpressionlist(NULL); // Create a new list expression
    linkedlist *new = (newlist ? newlist->list : NULL); // Pointer to the new linkedlist embedded in it.
    
    /* Determine length of list */
    if (eval_islist(left)) {
        nl = eval_listlength((expression_list *) left);
        if (((expression_list *) left)->list) el=((expression_list *) left)->list->first;
    }
    if (eval_islist(right)) {
        nr = eval_listlength((expression_list *) right);
        if (((expression_list *) right)->list) er=((expression_list *) right)->list->first;
    }
    
    /* Identify the maximum extent of the list */
    nn=nl; if (nr>nn) nn=nr;
    
    for (unsigned int i=0; i<nn; i++) {
        expression *l=NULL;
        expression *r=NULL;
        expression *out=NULL;
        
        if (eval_islist(left)) {
            if (el) l=el->data;
        } else l=left;
        if (eval_islist(right)) {
            if (er) r=er->data;
        } else r=right;
        
        if (l && r) out = (*op) (context, l, r);
        else if (l) out = cloneexpression(l);
        else if (r) out = cloneexpression(r);
        
        linkedlist_addentry(new, out);
        
        /* Advance list counters */
        if (el) el=el->next;
        if (er) er=er->next;
    }
    
    return (expression *) newlist;
}

/* Addition */
expression *opadd(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL&&right!=NULL) return cloneexpression(right); // Unary +
    if (right==NULL) return NULL;
    
    opresolvedoperands operands;
    if (evalresolvetype(left, right, &operands)) {
        switch (operands.type) {
            case EXPRESSION_INTEGER:
                return newexpressioninteger(operands.i[0]+operands.i[1]);
            case EXPRESSION_FLOAT:
                return newexpressionfloat(operands.f[0]+operands.f[1]);
            case EXPRESSION_COMPLEX:
                return newexpressioncomplex(operands.c[0]+operands.c[1]);
            case EXPRESSION_LIST:
                return oplistapply(context, opadd, left, right);
            case EXPRESSION_OBJECT_REFERENCE:
                return opredirecttoselector(context, OPERATOR_ADD_SELECTOR, (expression_objectreference *) left, right);
            default:
                break;
        }
    }
    
    return NULL;
}

/* Subtraction */
expression *opsub(interpretcontext *context, expression *left, expression *right) {
    if (right==NULL) return NULL;
    
    if (left==NULL) { // Unary -
        if (right->type==EXPRESSION_INTEGER) return newexpressioninteger(- ((expression_integer *)right)->value);
        if (right->type==EXPRESSION_FLOAT) return newexpressionfloat(- ((expression_float *)right)->value);
        if (right->type==EXPRESSION_COMPLEX) return newexpressioncomplex(- ((expression_complex *)right)->value);
        return NULL;
    }
    
    opresolvedoperands operands;
    if (evalresolvetype(left, right, &operands)) {
        switch (operands.type) {
            case EXPRESSION_INTEGER:
                return newexpressioninteger(operands.i[0]-operands.i[1]);
            case EXPRESSION_FLOAT:
                return newexpressionfloat(operands.f[0]-operands.f[1]);
            case EXPRESSION_COMPLEX:
                return newexpressioncomplex(operands.c[0]-operands.c[1]);
            case EXPRESSION_LIST:
                return oplistapply(context, opsub, left, right);
            case EXPRESSION_OBJECT_REFERENCE:
                return opredirecttoselector(context, OPERATOR_SUBTRACT_SELECTOR, (expression_objectreference *) left, right);
            default:
                break;
        }
    }
    
    return NULL;
}

/* Multiplication */
expression *opmul(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL||right==NULL) return NULL;
    
    opresolvedoperands operands;
    if (evalresolvetype(left, right, &operands)) {
        switch (operands.type) {
            case EXPRESSION_INTEGER:
                return newexpressioninteger(operands.i[0]*operands.i[1]);
            case EXPRESSION_FLOAT:
                return newexpressionfloat(operands.f[0]*operands.f[1]);
            case EXPRESSION_COMPLEX:
                return newexpressioncomplex(operands.c[0]*operands.c[1]);
            case EXPRESSION_LIST:
                return oplistapply(context, opmul, left, right);
            case EXPRESSION_OBJECT_REFERENCE:
                return opredirecttoselector(context, OPERATOR_MULTIPLY_SELECTOR, (expression_objectreference *) left, right);
            default:
                break;
        }
    }
    
    return NULL;
}

/* Division */
expression *opdiv(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL||right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER) {
        
    };
    
    opresolvedoperands operands;
    if (evalresolvetype(left, right, &operands)) {
        switch (operands.type) {
            case EXPRESSION_INTEGER:
                /* Return an integer value if there's no remainder */
                if (operands.i[0] % operands.i[1] == 0) return newexpressioninteger(operands.i[0] / operands.i[1]);
                else return newexpressionfloat(((double) operands.i[0])/((double) operands.i[1]));
            case EXPRESSION_FLOAT:
                return newexpressionfloat(operands.f[0]/operands.f[1]);
            case EXPRESSION_COMPLEX:
                return newexpressioncomplex(operands.c[0]/operands.c[1]);
            case EXPRESSION_LIST:
                return oplistapply(context, opdiv, left, right);
            case EXPRESSION_OBJECT_REFERENCE:
                return opredirecttoselector(context, OPERATOR_DIVIDE_SELECTOR, (expression_objectreference *) left, right);
            default:
                break;
        }
    }

    
    return NULL;
}


/* Power */
expression *oppower(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL||right==NULL) return NULL;
    
    opresolvedoperands operands;
    if (evalresolvetype(left, right, &operands)) {
        switch (operands.type) {
            case EXPRESSION_INTEGER:
                return newexpressioninteger((int) pow((double) operands.i[0], (double) operands.i[1]));
            case EXPRESSION_FLOAT:
                return newexpressionfloat(pow((double) operands.f[0], (double) operands.f[1]));
            case EXPRESSION_COMPLEX:
                return newexpressioncomplex(cpow(operands.c[0], operands.c[1]));
            default:
                break;
        }
    }
    
    return NULL;
}

/*
 * Boolean operators
 */

/* AND */
expression *opand(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL||right==NULL) return NULL;

    int bleft=0;
    int bright=0;
    
    if ((left->type==EXPRESSION_INTEGER)||(left->type==EXPRESSION_BOOL)) bleft=((expression_bool *)left)->value;
    else if (left->type==EXPRESSION_FLOAT) bleft=(int) ((expression_float *)left)->value;

    if ((right->type==EXPRESSION_INTEGER)||(right->type==EXPRESSION_BOOL)) bright=((expression_bool *)right)->value;
    else if (right->type==EXPRESSION_FLOAT) bright=(int) ((expression_float *)right)->value;
    
    return newexpressionbool(bleft && bright);
}

/* OR */
expression *opor(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL||right==NULL) return NULL;
    
    int bleft=0;
    int bright=0;
    
    if ((left->type==EXPRESSION_INTEGER)||(left->type==EXPRESSION_BOOL)) bleft=((expression_bool *)left)->value;
    else if (left->type==EXPRESSION_FLOAT) bleft=(int) ((expression_float *)left)->value;
    
    if ((right->type==EXPRESSION_INTEGER)||(right->type==EXPRESSION_BOOL)) bright=((expression_bool *)right)->value;
    else if (right->type==EXPRESSION_FLOAT) bright=(int) ((expression_float *)right)->value;
    
    return newexpressionbool(bleft || bright);
}

/* NOT */
expression *opnot(interpretcontext *context, expression *left, expression *right) {
    if (left!=NULL||right==NULL) return NULL; // This is a prefix operator
    
    int bright=0;
    
    if ((right->type==EXPRESSION_INTEGER)||(right->type==EXPRESSION_BOOL)) bright=((expression_bool *)right)->value;
    else if (right->type==EXPRESSION_FLOAT) bright=(int) ((expression_float *)right)->value;
    
    return newexpressionbool(!bright);
}

/*
 * Comparison operators
 */

/* TODO: Should ensure that floating point comparison is handled rigorously. */

/* Equal to */
expression *opeq(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value == ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value == ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value == ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value == ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_OBJECT_REFERENCE&&right->type==EXPRESSION_OBJECT_REFERENCE)
        return newexpressionbool(((expression_objectreference *)left)->obj == ((expression_objectreference *)right)->obj);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING) {
        if (strcmp(((expression_string *)left)->value,((expression_string *)right)->value)==0) return newexpressionbool(TRUE); else return newexpressionbool(FALSE);
    }

    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME) {
        if (strcmp(((expression_name *)left)->name,((expression_name *)right)->name)==0) return newexpressionbool(TRUE); else return newexpressionbool(FALSE);
    }
    
    return NULL;
}

/* Equal to */
expression *opneq(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value != ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value != ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value != ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value != ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING)
        return newexpressionbool(strcmp(((expression_string *) left)->value, ((expression_string *) right)->value) != 0);
    
    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME)
        return newexpressionbool(strcmp(((expression_name *) left)->name, ((expression_name *) right)->name) == 0);
    
    return NULL;
}

/* Less than */
expression *oplessthan(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value < ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value < ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value < ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value < ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING)
        return newexpressionbool(strcmp(((expression_string *) left)->value, ((expression_string *) right)->value) < 0);
    
    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME)
        return newexpressionbool(strcmp(((expression_name *) left)->name, ((expression_name *) right)->name) < 0);
    
    return NULL;
}

/* Less than or equal to */
expression *oplessthaneq(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value <= ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value <= ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value <= ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value <= ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING)
        return newexpressionbool(strcmp(((expression_string *) left)->value, ((expression_string *) right)->value) <= 0);
    
    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME)
        return newexpressionbool(strcmp(((expression_name *) left)->name, ((expression_name *) right)->name) <= 0);
    
    return NULL;
}


/* Greater than */
expression *opgreaterthan(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value > ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value > ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value > ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value > ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING)
        return newexpressionbool(strcmp(((expression_string *) left)->value, ((expression_string *) right)->value) > 0);
    
    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME)
        return newexpressionbool(strcmp(((expression_name *) left)->name, ((expression_name *) right)->name) > 0);
    
    return NULL;
}

/* Greater than or equal to */
expression *opgreaterthaneq(interpretcontext *context, expression *left, expression *right) {
    if (left==NULL) return right; // Unary +
    if (right==NULL) return NULL;
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_integer *)left)->value >= ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_FLOAT )
        return newexpressionbool(((expression_float *)left)->value >= ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_INTEGER&&right->type==EXPRESSION_FLOAT)
        return newexpressionbool(((expression_integer *)left)->value >= ((expression_float *)right)->value);
    
    if (left->type==EXPRESSION_FLOAT&&right->type==EXPRESSION_INTEGER)
        return newexpressionbool(((expression_float *)left)->value >= ((expression_integer *)right)->value);
    
    if (left->type==EXPRESSION_STRING&&right->type==EXPRESSION_STRING)
        return newexpressionbool(strcmp(((expression_string *) left)->value, ((expression_string *) right)->value) >= 0);
    
    if (left->type==EXPRESSION_NAME&&right->type==EXPRESSION_NAME)
        return newexpressionbool(strcmp(((expression_name *) left)->name, ((expression_name *) right)->name) >= 0);
    
    return NULL;
}

/* Debugging operator */
/* Breakpoint */
expression *opbreakpoint(interpretcontext *context, expression *left, expression *right) {
    
    if (left) interpretexpression(context, left);
    if (right) return(interpretexpression(context, right));
    
    return NULL;
}

/* operator_find - Finds an operator from a label */
operator *operator_find(char *label) {
    return hashget(operators,label);
}

/* evalinitializeoperators - Initialize operators.
 * Input:
 * Output:
 * Returns:
 */
void evalinitializeoperators(void) {
    operators = hashcreate(EVAL_OPERATORTABLESIZE);
    if (operators) {
        /* Define operators */
        
        /* Comma */
        operator_infix(OPERATOR_SEMICOLON, 1, NULL, (ledtype *) parseinfixoptional, (executeoptype *) opsemicolon, NULL, NULL);
        
        /* Assignment */
        operator_infix(OPERATOR_ASSIGN, 10, NULL, (ledtype *) parseinfixright, (executeoptype *) opassign, NULL, OPERATOR_ASSIGN_SELECTOR);
        
        /* Logic */
        operator_infix(OPERATOR_AND, 30, NULL, (ledtype *) parseinfixright, opand,NULL,OPERATOR_AND_SELECTOR);
        operator_infix(OPERATOR_OR, 30, NULL, (ledtype *) parseinfixright, opor,NULL,OPERATOR_OR_SELECTOR);
        operator_prefix(OPERATOR_NOT, (nudtype *) parseprefix, opnot,NULL,OPERATOR_NOT_SELECTOR);
        
        /* Comparison */
        operator_infix(OPERATOR_ISEQUAL, 40, NULL, (ledtype *) parseinfix, opeq, NULL, OPERATOR_ISEQUAL_SELECTOR);
        operator_infix(OPERATOR_ISNOTEQUAL, 40, NULL, (ledtype *) parseinfix, opneq, NULL, OPERATOR_ISNOTEQUAL_SELECTOR);
        operator_infix(OPERATOR_ISLESSTHAN, 40, NULL, (ledtype *) parseinfix, oplessthan, NULL, OPERATOR_ISLESSTHAN_SELECTOR);
        operator_infix(OPERATOR_ISLESSTHANOREQUAL, 40, NULL, (ledtype *) parseinfix, oplessthaneq,NULL, OPERATOR_ISLESSTHANOREQUAL_SELECTOR);
        operator_infix(OPERATOR_ISGREATERTHAN, 40, NULL, (ledtype *) parseinfix, opgreaterthan,NULL, OPERATOR_ISGREATERTHAN_SELECTOR);
        operator_infix(OPERATOR_ISGREATERTHANOREQUAL, 40, NULL, (ledtype *) parseinfix, opgreaterthaneq, NULL, OPERATOR_ISGREATERTHANOREQUAL_SELECTOR);
        
        /* Arithmatic */
        operator_infix(OPERATOR_ADD, 50, (nudtype *) parseprefix, (ledtype *) parseinfix, (executeoptype *) opadd, eval_diffaddop, OPERATOR_ADD_SELECTOR);
        operator_infix(OPERATOR_SUBTRACT, 50, (nudtype *) parseprefix, (ledtype *) parseinfix, (executeoptype *) opsub,eval_diffsubop, OPERATOR_SUBTRACT_SELECTOR);
        operator_infix(OPERATOR_MULTIPLY, 60, NULL, (ledtype *) parseinfix, (executeoptype *) opmul, eval_diffmulop, OPERATOR_MULTIPLY_SELECTOR);
        operator_infix(OPERATOR_DIVIDE, 60, NULL, (ledtype *) parseinfix, (executeoptype *) opdiv,eval_diffdivop, OPERATOR_DIVIDE_SELECTOR);
        
        operator_prefix(OPERATOR_STARTLIST, (nudtype *) parselist, NULL, NULL, NULL);
        operator_symbol(OPERATOR_ENDLIST);
                
        operator_infix(OPERATOR_LEFTBRACKET, 110, (nudtype *) parseparenthesis, (ledtype *) parsefunctioncall, NULL, NULL, NULL);
        operator_symbol(OPERATOR_ENDBRACKET);
        
        operator_infix(OPERATOR_STARTINDEX, 110, NULL, (ledtype *) parseindex, NULL, NULL, NULL);
        operator_symbol(OPERATOR_ENDINDEX);
        
        /* Power - both C and FORTRAN style */
        operator_infix(OPERATOR_POWER, 80, NULL, (ledtype *) parseinfixright, (executeoptype *) oppower,eval_diffpowop, OPERATOR_POWER_SELECTOR);
        operator_infix(OPERATOR_POWER_FORTRAN, 80, NULL, (ledtype *) parseinfixright, (executeoptype *) oppower,eval_diffpowop, OPERATOR_POWER_SELECTOR);
        
        /* Class operator */
        operator_prefix(OPERATOR_CLASS, (nudtype *) parseprefix, NULL, NULL, NULL);
        operator_prefix(OPERATOR_BREAKPOINT, (nudtype *) parseprefix, opbreakpoint, NULL, NULL);
        
        /* Other symbols that must be recognized by the lexer, but are not parsed directly. */
        operator_symbol(OPERATOR_COMMA);
    }
}

/* evalfinalizeoperators - Finalize operators.
 * Input:
 * Output:
 * Returns:
 */
void evalfinalizeoperators(void) {
    if(operators!=NULL) {
        hashmap(operators, &hashfreevalue,NULL);
        hashfree(operators);
    }
}
