/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_linalg_h
#define Morpho_linalg_h

#include "eval.h"

#ifndef PLATFORM
#define CBLAS_INCLUDE <Accelerate/Accelerate.h>
#else
#define CBLAS_INCLUDE <cblas.h>
#endif

#define LINALG_MATRIXCLASS "matrix"
#define LINALG_VECTORCLASS "vector"
#define LINALG_MATRIXPRINT "print"

#define LINALG_MATRIXSERIALIZATIONLABEL "matrix"

#define ERROR_LINALG_INDEX                  0x4101
#define ERROR_LINALG_INDEX_MSG              "Selector 'index' needs two arguments for a matrix."

#define ERROR_LINALGV_INDEX                 0x4102
#define ERROR_LINALGV_INDEX_MSG             "Selector 'index' needs one argument for a vector."

#define ERROR_LINALG_INDEX_NUM              0x4103
#define ERROR_LINALG_INDEX_NUM_MSG          "Selector 'index' needs integer arguments."

#define ERROR_LINALG_SETINDEX_NUM           0x4104
#define ERROR_LINALG_SETINDEX_NUM_MSG       "Selector 'setindex' needs integer indices."

#define ERROR_LINALG_SETINDEX_ARG           0x4105
#define ERROR_LINALG_SETINDEX_ARG_MSG       "Selector 'setindex' requires a numerical argument."

#define ERROR_LINALG_SETINDEX               0x4106
#define ERROR_LINALG_SETINDEX_MSG           "Selector 'setindex' needs three arguments: a value and two indices."

#define ERROR_LINALGV_SETINDEX              0x4106
#define ERROR_LINALGV_SETINDEX_MSG          "Selector 'setindex' needs two arguments: a value and an index."

#define ERROR_LINALG_INCOMPATIBLE           0x4107
#define ERROR_LINALG_INCOMPATIBLE_MSG       "Incompatible matrices."

#define ERROR_LINALGV_INCOMPATIBLE          0x4108
#define ERROR_LINALGV_INCOMPATIBLE_MSG      "Incompatible vectors."

typedef enum {
    MATRIX_NONE,
    MATRIX_FLOAT
} matrixtype;

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    matrixtype type;
    unsigned int nrows;
    unsigned int ncols;
    size_t size; 
    void *data;
} matrixobject;

void linalginitialize(void);

expression *linalg_listtomatrix(expression_list *list, matrixobject *mobj);

#endif
