/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_error_h
#define Morpho_error_h

#include <setjmp.h>
#include "eval.h"

typedef enum {
    ERROR_INFORMATIONAL, /* An informational message */
    ERROR_WARNING, /* Recoverable error, but will trigger a halt after a specified number of warnings. */
    ERROR_ALLOCATION_FAILED, /* Memory allocation failed */
    ERROR_FATAL, /* Fatal error -- evaluation cannot continue */
    ERROR_PARSE /* Errors generated by the parser */
} errorclass;

typedef struct {
    int code;
    char *msg;
    errorclass clss;
    void *data;
} errorinfo;

extern char errorbuffer[];

void error_saveenv(interpretcontext *context, jmp_buf *env);

void error_raise(interpretcontext *context, int code, char *msg, errorclass class);

void error_raisefrominfo(interpretcontext *context, errorinfo *info);
void error_raisewithdata(interpretcontext *context, int code, char *msg, errorclass clss,void *data);

void evalinitializeerror(void);

#define error_buffer() errorbuffer
#define ERROR_BUFFERSIZE                255

#define ERROR_DEPRECATED                 0x0999
#define ERROR_DEPRECATED_MSG             "Using deprecated functionality---please migrate."

#define ERROR_INCORRECTBRACKETS          0x1000
#define ERROR_INCORRECTBRACKETS_MSG      "Brackets [] are used for indexing; use parentheses to call a function, i.e.  '%s(...)'."
#define ERROR_SYMBOLNOTDEFINED           0x1001
#define ERROR_SYMBOLNOTDEFINED_MSG       "Symbol '%s' not defined."
#define ERROR_SYMBOLNOTINDEXABLE         0x1002
#define ERROR_SYMBOLNOTINDEXABLE_MSG     "Symbol '%s' is not indexable."
#define ERROR_INDEXNOTEVALUATABLE        0x1003
#define ERROR_INDEXNOTEVALUATABLE_MSG    "Index %u cannot be evaluated."

#define ERROR_OPERATORNOTIMPLEMENTED     0x1004
#define ERROR_OPERATORNOTIMPLEMENTED_MSG "Operator %s not implemented."

#define ERROR_XISAFUNCTION               0x1005
#define ERROR_XISAFUNCTION_MSG           "Name '%s' is a function."

#define ERROR_INCORRECTNUMARGS           0x1006
#define ERROR_INCORRECTNUMARGS_MSG       "Function '%s' expects %i arguments."
#define ERROR_FUNCTIONNOTFOUND           0x1007
#define ERROR_FUNCTIONNOTFOUND_MSG       "Function '%s' not recognized."

#define ERROR_CLASSNOTDEFINED            0x1008
#define ERROR_CLASSNOTDEFINED_MSG        "Class '%s' not defined."
#define ERROR_CLASSNOTRESPONDTOSEL       0x1009
#define ERROR_CLASSNOTRESPONDTOSEL_MSG   "Object of class '%s' does not respond to selector '%s'."
#define ERROR_CLASSCANTSERIALIZE         0x1010
#define ERROR_CLASSCANTSERIALIZE_MSG     "Cannot serialize this object."
#define ERROR_CANTRESOLVENAME            0x1011
#define ERROR_CANTRESOLVENAME_MSG        "Could not resolve function name."
#define ERROR_CANTRESOLVEINDEXNAME       0x1012
#define ERROR_CANTRESOLVEINDEXNAME_MSG   "Could not resolve variable to index."
#define ERROR_CLASSCANTCLONE             0x1013
#define ERROR_CLASSCANTCLONE_MSG         "Cannot clone this object."

#define ERROR_CLASSNOTINDEXABLE          0x1014
#define ERROR_CLASSNOTINDEXABLE_MSG      "Object is not indexable."
#define ERROR_INDICESNOLIST              0x1015
#define ERROR_INDICESNOLIST_MSG          "Selector 'indices' should return a list."

#define ERROR_ALLOCATIONFAILED           0x1020
#define ERROR_ALLOCATIONFAILED_MSG       "Memory allocation failed."

#define ERROR_CALCCANTDIFFFUNC           0x1100
#define ERROR_CALCCANTDIFFFUNC_MSG       "Cannot differentiate function '%s'."
#define ERROR_CALCCANTDIFFEXP            0x1101
#define ERROR_CALCCANTDIFFEXP_MSG        "Cannot differentiate expression."
#define ERROR_CALCCANTDIFFOP             0x1102
#define ERROR_CALCCANTDIFFOP_MSG         "Cannot differentiate operator '%s'."



#endif
