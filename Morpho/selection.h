/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "geometry.h"

#ifndef selection_h
#define selection_h

/*
 * selection - a way to select parts of manifolds
 */

/* Selection type */
typedef struct selection_object_s {
    CLASS_GENERICOBJECTDATA
    
    linkedlist *selection;
} selection_object;

typedef struct {
    manifold_object *mref;
    unsigned int maxgrade; 
    
    idtable **selection;
} selection_manifoldref;

/* Classes */
#define GEOMETRY_SELECTIONCLASS "selection"

#define GEOMETRY_SELECTION_COORDS                  "coordinates"
#define GEOMETRY_SELECTION_GRADE                   "grade"
#define GEOMETRY_SELECTION_INCLUDEPARTIALELEMENTS  "includepartial"

/* Selector labels */
#define SELECTION_ISSELECTED_SELECTOR "isselected"
#define SELECTION_IDLISTFORGRADE_SELECTOR "idlistforgrade"
#define SELECTION_REFINE_SELECTOR "refine"
#define SELECTION_CHANGEGRADE_SELECTOR "changegrade"
//#define SELECTION_PRINTSELECTION_SELECTOR "printselection"

/* General */
/* Errors */
#define ERROR_SELECTIONCOORDS                           0x9700
#define ERROR_SELECTIONCOORDS_MSG                       "Option coordinates must be a list of coordinates."

#define ERROR_COORDSREQD                                0x9701
#define ERROR_COORDSREQD_MSG                            "A list of coordinates is required."

#define ERROR_COORDSLENGTH                              0x9702
#define ERROR_COORDSLENGTH_MSG                          "List of coordinates must have the correct dimensionality for the space."

expression *selection_selectwithfield(body_object *bobj, interpretcontext *context, field_object *field);
expression *selection_select(body_object *bobj, interpretcontext *context, expression *exp, interpretcontext *options);
expression *selection_selectwithidlist(manifold_object *mobj, unsigned int grade, unsigned int nels, uid *ids);
expression *selection_selectboundary(manifold_object *mobj);

int selection_isselected(selection_object *selection, manifold_object *mobj, unsigned int grade, uid id);
idtable *selection_idtableforgrade(selection_object *selection, manifold_object *mobj, unsigned int grade);

void selection_addelements(selection_manifoldref *mref, unsigned int grade, unsigned int nelements, uid *elements);

expression *selection_selectall(body_object *bobj, unsigned int ngrades, unsigned int *grades);

void selection_map(selection_object *sel, manifold_object *mobj, unsigned int grade, idtablemapfunction *func, void *ref);

/* Selectors */
expression *expression_isselectedi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *selection_loweri(object *obj, interpretcontext *context, int nargs, expression **args);
expression *selection_newi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *selection_freei(object *obj, interpretcontext *context, int nargs, expression **args);

/* Initialization */

void selectioninitialize(void);

#endif /* selection_h */
