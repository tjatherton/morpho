/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * eval.h - Evaluator
 *
 * Version 0.1
 *
 * T J Atherton
 *
 * This library implements a simple, compact expression evaluator with minimal external dependencies. It features:-
 *  1. A parser with easily modifiable/extensible grammar, supporting operators, names, numbers and function calls.
 *  2. Support for intrinsic functions, which may be easily defined outside the library.
 *  3. Support for user-defined functions.
 *  4. Variables with scoping.
 *
 * The evaluator consists of the following pieces:-
 *  1. A lexer, that identifies tokens from a provided string.
 *  2. A parser, that organizes the tokens into a syntax tree.
 *  3. An interpreter, that executes a syntax tree.
 *  4. [Planned] A compiler/bytecode interpreter for high performance execution.
 *  5. Operator definitions and associated functions to implement them.
 *  6. Intrinsic function definitions and associated functions to implement them.
 */

/*
 * To Do:
 * 1. Implement proper error handling during both parsing and execution.
 * 2. Efficient bytecode interpreter for perfomance.
 * 3. Not all operators and intrinsics are defined.
 */


#ifndef Morpho_eval_h
#define Morpho_eval_h

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <complex.h>
#include <setjmp.h>

#include "hash.h"
#include "linkedlist.h"


/* These control which functions are used by the evaluation library to allocate and free memory,
 * facilitating easy replacement of the standard functions if need be.
 */
#define EVAL_MALLOC malloc
#define EVAL_REALLOC realloc
#define EVAL_FREE   free
#define EVAL_STRDUP eval_strdup

char *eval_strdup(char *string);

#ifndef FALSE
#define TRUE (!0)
#define FALSE 0
#endif

#define MACHINE_EPSILON 1e-15

/* The size of the global hashtable used to contain operators. Increase if many more operators are defined */
#define EVAL_OPERATORTABLESIZE 32
/* The size of the global hashtable used to contain constants. Increase if many more constants are defined */
#define EVAL_CONSTANTTABLESIZE 32
/* The size of the global hashtable used to contain variables, user-defined functions, etc. It is expected this will need to be large */
#define EVAL_DEFAULTCONTEXTTABLESIZE 128
/* The size of a local hashtable used to contain variables, user-defined functions, etc. within a user-defined function call. This
 * has been kept small to minimize function overhead. */
#define EVAL_LOCALCONTEXTTABLESIZE 4

/* The precedence level of prefix operators such as unary + and -. See operator initialization for more. */
#define PREFIX_PRECEDENCE 70

/* Maximum number of arguments to function. */
#define EVAL_MAXFUNCTIONARGS 10

/* Types of expression */
typedef enum {
    EXPRESSION_NONE,
    EXPRESSION_FUNCTION,
    EXPRESSION_FUNCTION_DEFINITION,
    EXPRESSION_OPERATOR,
    EXPRESSION_NAME,
    EXPRESSION_FLOAT,
    EXPRESSION_INTEGER,
    EXPRESSION_COMPLEX,
    EXPRESSION_BOOL,
    EXPRESSION_LIST,
    EXPRESSION_STRING,
    EXPRESSION_OBJECT_REFERENCE,
    EXPRESSION_INDEX,
    EXPRESSION_RETURN
} expressiontype;

/* Generic expression type */
typedef struct {
    expressiontype type;
} expression;

/* The lexer identifies each character in the token stream as one of these types */
typedef enum { LEX_ALPHA, LEX_NUMBER, LEX_OPERATOR, LEX_STRING, LEX_NONE} chartype;

/* The lexer provides the parser with information about the type of each token it has identified. */
typedef enum { TOKEN_NAME, TOKEN_INTEGER, TOKEN_FPOINT, TOKEN_STRING, TOKEN_OPERATOR, TOKEN_EOF, TOKEN_UNRECOGNIZED } tokentype;

/* Different types of operators. */
typedef enum { OPERATOR_INFIX, OPERATOR_PREFIX, OPERATOR_SYMBOL } operatortype;

/* An interpreter context consists of a reference to a parent context and a symbol table */
typedef struct interpretcontextstruct {
    struct interpretcontextstruct *parent;
    hashtable *symbols;
    linkedlist *freerecords;
    jmp_buf *errorenv;
    int insideselector;
    unsigned int size; 
} interpretcontext;

/* A record of something that needs to be freed */
typedef void interpretcontextfreefunction (void*);

typedef struct {
    void *data;
    interpretcontextfreefunction *freefunction;
} interpretcontextfree;

/* A type of function that implements an operator.
 * Such a function receives an interpreter context, and a left and right expression (which may be null) and must return an expression.
 */
typedef expression* executeoptype (interpretcontext*,expression*,expression*);

/* Parser types
 * The nomenclature here is derived from Pratt's work on Top Down Operator Precedence parsing. Such functions may be defined for each
 * operator and are called as the parser encounters an operator during parsing.
 * nud [null denotation] -> called when the parser encounters an operator without a lhs, e.g. unary operators,
 *                          prefix operators [and symbols]. The argument is a pointer to the token containing the operator.
 * led [left denotation] -> called when the parser encounters an operator with a lhs. The arguments are a pointer to the token containing
 *                          the operator and a pointer to an expression containing the lhs.
 */
typedef expression* nudtype (void*);
typedef expression* ledtype (void*,expression*);

/* A type of function that differentiates an expression.
 * Such a function receives an expression to differentiate and a symbol.
 */
typedef expression* diffexpfunctype (interpretcontext*,expression*,char*);

/* Operators
 * An operator contains the following information:-
 * symbol       - a string, e.g. "=", as would be used in the input stream. The lexer uses this to identify tokens.
 * precedence   - a priority for the operator, described more fully in the parsing functions.
 * type         - category of operator
 * nud          - null denotation function as described in the type definition above.
 * led          - left denotation function as described in the type definition above.
 * executeop    - a function, called by the interpreter, that performs the operation.
 * diff         - a function that differentiates the operator.
 */
typedef struct {
    char *symbol;
    int precedence;
    operatortype type;
    nudtype *nud;
    ledtype *led;
    executeoptype *executeop;
    diffexpfunctype *diff;
    char *selector; 
} operator;

/* A type of function that the parser can call upon encountering a line ending if more input is required.
 This is used to implement multi-line parsing. */
typedef char* eval_cfunctype (void *);

/* Tokens
 * WARNINGS:
 * 1) tokens are intended for intermediate processing of strings passed to the parser.
 *    They merely point to information and are not a permanent store.
 * 2) char *token does NOT point to a null terminated string, rather its length is
 *    given by tokenlength. This is intended to minimize superfluous memory copying.
 */
typedef struct {
    char *token;
    char *base;
    unsigned int tokenlength;
    tokentype toktype;
    int lbp;
    nudtype *nud;
    ledtype *led;
    void *data;
    eval_cfunctype *cfunc;
    void *cdata;
    int continued;
    int forcecontinue;
} token;

/* Expression types
 * These collectively implement the various kinds of expression that may occur in the syntax tree. Each has a common field, 'type'
 * that allows the program to identify the expression and obtain the contained information using an appropriate cast.
 *
 * These are:-
 */

/*    1. Function definitions. Which contain the name of the function, number of arguments, a list of expressions identifying the argument
 names and an expression to be evaluated upon calling. */
typedef struct {
    expressiontype type;
    expression *name;
    unsigned int nargs;
    expression **argslist;
    expression *eval;
} expression_functiondefinition;

/*    2. Function call. This contains the name of the function, number of arguments and a list of expressions to be passed as arguments. */
typedef struct {
    expressiontype type;
    expression *name;
    unsigned int nargs;
    expression **argslist;
} expression_function;

/*    3. Names. */
typedef struct {
    expressiontype type;
    char *name;
} expression_name;

/*    4. Operators, with a reference to the corresponding operator object expression and left and right operands. */
typedef struct {
    expressiontype type;
    operator *op;
    expression *left;
    expression *right;
} expression_operator;

/*    5. Floating point numbers. */
typedef struct {
    expressiontype type;
    double value;
} expression_float;

/*    6. Integers. */
typedef struct {
    expressiontype type;
    int value;
} expression_integer;

/*    7. Complex numbers. */
typedef struct {
    expressiontype type;
    complex double value;
} expression_complex;

/*    8. Boolean. */
typedef struct {
    expressiontype type;
    int value;
} expression_bool;

/*    9. Lists. */
typedef struct {
    expressiontype type;
    linkedlist *list;
} expression_list;

/*    10. Strings. */
typedef struct {
    expressiontype type;
    char *value;
} expression_string;

/*    11. Object references. */
/* See classes.h */

/*    12. Indices. */
typedef struct {
    expressiontype type;
    expression *name;
    unsigned int nargs;
    expression **argslist;
} expression_index;

/* 12. return'd objects */
typedef struct {
    expressiontype type;
    expression *value;
} expression_return;

/* Include header files for subcomponents of the evaluator here as they may require the above types. */
#include "error.h"
#include "intrinsics.h"
#include "operators.h"
#include "constants.h"
#include "classes.h"
#include "calculus.h"

extern interpretcontext *defaultinterpretcontext;

void evalhashfreeexpression(char *name, void *exp, void *ref);

void evalinitialize(void);
void evalfinalize(void);

expression *parse(char *string, eval_cfunctype *cfunc, void *cdata);

void expressionprint (expression *exp);
expression *cloneexpression(expression *exp);
void freeexpression(expression *exp);
expression *callfunction(interpretcontext *context,expression_function *expfunc);

expression *interpret(expression *exp);
expression *execute(interpretcontext *context, const char *string);
expression *call(char *name, unsigned int nargs, ...);

int evaltest(void);

/* Macros */
#define eval_isnumerical(r) ((r)&&((r->type==EXPRESSION_FLOAT)||(r->type==EXPRESSION_INTEGER)||(r->type==EXPRESSION_COMPLEX)))
#define eval_isreal(r) ((r)&&((r->type==EXPRESSION_FLOAT)||(r->type==EXPRESSION_INTEGER)))
#define eval_isfloat(r) ((r)&&(r->type==EXPRESSION_FLOAT))
#define eval_isbool(r) ((r)&&(r->type==EXPRESSION_BOOL))
#define eval_isinteger(r) ((r)&&(r->type==EXPRESSION_INTEGER))
#define eval_isstring(r) ((r)&&(r->type==EXPRESSION_STRING))
#define eval_isname(r) ((r)&&(r->type==EXPRESSION_NAME))
#define eval_islist(r) ((r)&&(r->type==EXPRESSION_LIST))
#define eval_iscomplex(r) ((r)&&(r->type==EXPRESSION_COMPLEX))
#define eval_isoperator(r) ((r)&&((r->type==EXPRESSION_OPERATOR)))
#define eval_isfunction(r) ((r)&&((r->type==EXPRESSION_FUNCTION)))
#define eval_isfunctiondefinition(r) ((r)&&((r->type==EXPRESSION_FUNCTION_DEFINITION)))
#define eval_haszeroimag(r) ((r)&&(r->type==EXPRESSION_COMPLEX)&&(fabs(cimag(((expression_complex *) r)->value))<MACHINE_EPSILON))
#define eval_haszeroreal(r) ((r)&&(r->type==EXPRESSION_COMPLEX)&&(fabs(creal(((expression_complex *) r)->value))<MACHINE_EPSILON))
#define eval_isobjectref(r) ((r)&&(r->type==EXPRESSION_OBJECT_REFERENCE))

#define eval_real(r) creal(((expression_complex *) r)->value)
#define eval_imag(r) cimag(((expression_complex *) r)->value)

#define eval_objectfromref(r) (((expression_objectreference *) r)->obj)

#define eval_objectrefeq(r, s) (eval_isobjectref(r) && eval_isobjectref(s) && (((expression_objectreference *) r)->obj)==(((expression_objectreference *) s)->obj) )

/* Private functions */
expression *newexpressioninteger(int value);
expression *newexpressionfloat(double value);
expression *newexpressionstring(char *value);
expression *newexpressioncomplex(complex double value);
expression *newexpressionbool(int value);
expression *newexpressionnone(void);
expression_function *newexpressionfunction(expression *name, int nargs, expression **args, int clone);
expression_functiondefinition *newexpressionfunctiondefinition(expression_function *func, expression *def, int clone);
expression_list *newexpressionlist(linkedlist *list);
expression_list *newexpressionlistfromargs(int nargs, ...);
expression_list *newexpressionlistfromhashtable(hashtable *ht);
expression *newexpressionoperator(operator *op, expression *left, expression *right, int clone);
expression *newexpressionname(char *name);

expression *cloneexpression(expression *exp);

expression *parsefunction(token *tok, expression *left, expressiontype type, char *leftdelim, char *rightdelim);
expression *parsename(token *t);
expression *parseinteger(token *tok);
expression *parsefloat(token *tok);
expression *parseinfix(token *tok,expression *left);
expression *parseinfixoptional(token *tok,expression *left);
expression *parseinfixright(token *tok,expression *left);
expression *parseprefix(token *tok);
expression *parseparenthesis(token *tok);
expression *parsestring(token *tok);
expression *parselist(token *tok);
expression *parsefunctioncall(token *tok, expression *left);
expression *parseindex(token *tok, expression *left);

void parseerror(token *tok, char *message);
expression *parseexpression(token *tok, int rbp, int required);

void expressionprint (expression *exp);
size_t  sexpressionprint (expression *exp, size_t bufferlength, char *buffer);

interpretcontext *newinterpretcontext(interpretcontext *parent, int size);
void freeinterpretcontext(interpretcontext *context);
void errorfreeinterpretcontext(interpretcontext *context);
void interpretcontextregisterfree(interpretcontext *context, void *data, interpretcontextfreefunction *func);
void interpretcontextderegisterfree(interpretcontext *context, void *data);

expression *lookupsymbol(interpretcontext *context, char *symbol);
expression *findsymbol(interpretcontext *context, char *symbol);
expression *eval_calluserfunction(interpretcontext *context,expression_function *expfunc,expression_functiondefinition *expdef);
expression *callfunction(interpretcontext *context,expression_function *expfunc);
expression *interpretindex(interpretcontext *context, expression_index *index, expression *set);
expression *interpretexpression(interpretcontext *context, expression *exp);
#endif
