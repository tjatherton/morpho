/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* A field is an object that stores data with reference to a manifold. */

#include "geometry.h"

#ifndef field_h
#define field_h

/* Field type */
typedef struct field_object_s {
    CLASS_GENERICOBJECTDATA
    
    unsigned int grade; /* This is the grade of object the field is defined ON (e.g. vertex, edge) */
    
    expression_objectreference *manifold;
    idtable *entries;
    idtable *fix; 
} field_object;

/* */
#define GEOMETRY_FIELDCLASS "field"

#define GEOMETRY_FIELDTOTAL       "total"
#define GEOMETRY_FIELDSETGRADE    "setgrade"
#define GEOMETRY_FIELDGRADE       "grade"
#define GEOMETRY_FIELDSETMANIFOLD "setmanifold"
#define GEOMETRY_FIELDMANIFOLD    "manifold"
#define FIELD_MAP_SELECTOR        "map"
#define FIELD_MIN_SELECTOR        "min"
#define FIELD_MAX_SELECTOR        "max"
#define FIELD_BOUNDS_SELECTOR     "bounds"
#define FIELD_FIX_SELECTOR        "fix"
#define FIELD_UNFIX_SELECTOR      "unfix"

#define GEOMETRY_FIELD_DEFAULTSIZE 10

/* Errors */
#define ERROR_FIELD_INDEX                           0x9200
#define ERROR_FIELD_INDEX_MSG                       "Selector 'index' needs one argument for a field."

#define ERROR_FIELD_SETINDEX                        0x9201
#define ERROR_FIELD_SETINDEX_MSG                    "Selector 'setindex' needs one argument for a field."

#define ERROR_FIELD_INDEX_NUM                       0x9102
#define ERROR_FIELD_INDEX_NUM_MSG                   "Selector 'index' needs integer indices."

#define ERROR_FIELD_SETINDEX_NUM                    0x9103
#define ERROR_FIELD_SETINDEX_NUM_MSG                "Selector 'setindex' needs integer indices."

#define ERROR_FIELD_MANIFOLD                        0x9104
#define ERROR_FIELD_MANIFOLD_MSG                    "Fields defined on different manifolds."

#define ERROR_FIELD_GRADE                           0x9105
#define ERROR_FIELD_GRADE_MSG                       "Fields defined on different grades."

#define ERROR_FIELD_EMPTY                           0x9106
#define ERROR_FIELD_EMPTY_MSG                       "Field appears to be empty."

/*
 * Function prototypes
 */

expression *field_new(manifold_object *manifold, unsigned int grade);
void field_insertwithid(field_object *field, uid id, expression *value);
void field_accumulatewithid(field_object *field, interpretcontext *context, uid id, expression *value);
void field_accumulate(field_object *f, interpretcontext *context, field_object *df, MFLD_DBL scale);
expression *field_get(field_object *field, uid id);
void field_map(field_object *field, idtablemapfunction *func, void *ref);
idtable *field_computecolormap(field_object *fobj, interpretcontext *context, expression_objectreference *colormap, int scale, unsigned int grade);

double field_dot(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2);
expression *field_project(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2);
expression *field_localproject(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2, int onesided);

expression *field_newwithexpression(manifold_object *manifold, interpretcontext *context, expression *exp, expression *coords);

/* Selectors */
expression *field_newi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *field_freei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *field_indexi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *field_setindexi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *field_printi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *field_clonei(object *obj, interpretcontext *context, int nargs, expression **args);

void fieldinitialize(void);

#endif /* field_h */
