/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* GLFW based */

#include "eval.h"
#include "show.h"
#include "cli.h"

#ifdef USE_GLFW

#include "vr.h"
#include "graphics.h"
#include "graphics3d.h"
#include "geometry.h"
#include <pthread.h>

/*
 * Interthread communication
 */

/* This variable indicates whether the main thread has no windows open (and is therefore resting) or is actively maintaining windows. */
typedef enum {
    SHOW_MAINTHREADRESTING,
    SHOW_MAINTHREADAWAKE
} show_mainthreadstate;

pthread_mutex_t show_state_mutex;
show_mainthreadstate show_state;

pthread_mutex_t show_restmutex;
pthread_cond_t show_restcondition;

/* A message queue enabling the cli to issue commands to the main thread. */

typedef enum {
    SHOW_OPEN,
    SHOW_CLOSE,
    SHOW_CLOSED,
    SHOW_UPDATE,
    SHOW_QUIT
} show_messagetype;

typedef struct {
    show_messagetype type;
    void *ref;
    void *data;
} show_message;

pthread_mutex_t show_message_mutex;
linkedlist *show_messagequeue;

void show_initializecomm(void) {
    pthread_mutex_init(&show_state_mutex, NULL);
    show_state=SHOW_MAINTHREADRESTING;
    
    pthread_mutex_init(&show_restmutex, NULL);
    pthread_cond_init (&show_restcondition, NULL);
    
    pthread_mutex_init(&show_message_mutex, NULL);
    
    show_messagequeue=linkedlist_new();
}

void show_finalizecomm(void) {
    pthread_mutex_destroy(&show_state_mutex);
    pthread_mutex_destroy(&show_restmutex);
    pthread_cond_destroy(&show_restcondition);
    pthread_mutex_destroy(&show_message_mutex);
    
    if (show_messagequeue) linkedlist_free(show_messagequeue);
}

/* Adds a message to the queue */
void show_sendmessage(show_messagetype type, void *ref, void *data) {
    show_mainthreadstate state;
    show_message *msg=NULL;
    
    /* Add the message to the queue */
    pthread_mutex_lock(&show_message_mutex);
    msg=EVAL_MALLOC(sizeof(show_message));
    if (msg) {
        msg->type=type;
        msg->data=data;
        msg->ref=ref;
    }
    linkedlist_addentry(show_messagequeue, msg);
    pthread_mutex_unlock(&show_message_mutex);
    
    /* Find out the state of the main thread */
    pthread_mutex_lock(&show_state_mutex);
    state=show_state;
    pthread_mutex_unlock(&show_state_mutex);
    
    /* If the main thread is resting, tell it to wake up in order to receive the message. */
    if (state==SHOW_MAINTHREADRESTING) {
        pthread_mutex_lock(&show_restmutex);
        pthread_cond_signal(&show_restcondition);
        pthread_mutex_unlock(&show_restmutex);
    } else {
        /* If not, post an empty GLFW event to make sure it is received promptly. */
        glfwPostEmptyEvent();
    }
}

/* Pops the first message from the queue */
show_message *show_getmessage(void) {
    show_message *ret=NULL;
    
    pthread_mutex_lock(&show_message_mutex);
    if ((show_messagequeue)&&(show_messagequeue->first)) {
        ret = (show_message *) show_messagequeue->first->data;
        linkedlist_removeentry(show_messagequeue, ret);
    }
    pthread_mutex_unlock(&show_message_mutex);
    
    return ret;
}

/* Test whether messages are available */
int show_messageavailable(void) {
    int avail=FALSE;
    
    pthread_mutex_lock(&show_message_mutex);
    if ((show_messagequeue)&&(show_messagequeue->first)) {
        avail=TRUE;
    }
    pthread_mutex_unlock(&show_message_mutex);
    
    return avail;
}

/* Frees a message once it's done with */
void show_freemessage(show_message *msg) {
    EVAL_FREE(msg);
}


/*
 * Internal window data. A show_windowref holds all data to keep the status of the window.
 */

unsigned int nwindows;
int lastclosed;
linkedlist *windowrefs;

typedef struct {
    GLFWwindow *window;
    void *ref; /* The reference the CLI uses */
    void *data;
    
    GLfloat alpha;
    GLfloat beta;
    GLfloat xx0;
    GLfloat yy0;
    GLfloat zoom;
    int width;
    int height;
    
    enum { NORMAL, DRAGGING_LEFT, DRAGGING_RIGHT } state;
    
    int cursorX;
    int cursorY;
    
    int do_redraw;
} show_windowref;

void show_initializewindowrefs(void) {
    windowrefs=linkedlist_new();
}

/* Any windows still open should be destroyed */
void show_finalizewindowrefsmapfunction(void *ref) {
    show_windowref *r=(show_windowref *) ref;
    glfwDestroyWindow(r->window);
    EVAL_FREE(r);
}

void show_finalizewindowrefs(void) {
    if (windowrefs) {
        linkedlist_map(windowrefs, show_finalizewindowrefsmapfunction);
        linkedlist_free(windowrefs);
    }
}

show_windowref *show_newwindowref(GLFWwindow *window, void *ref, void *data) {
    show_windowref *nref=EVAL_MALLOC(sizeof(show_windowref));
    
    if (nref) {
        nref->alpha=0.f;
        nref->beta=0.f;
        nref->xx0=0.f;
        nref->yy0=0.f;
        nref->zoom=2.f;
        nref->width=0;
        nref->height=0;
        nref->state=NORMAL;
        nref->cursorX=0;
        nref->cursorY=0;
        nref->do_redraw = TRUE;
        
        nref->window=window;
        nref->ref=ref;
        nref->data=data;
        
        if (windowrefs) linkedlist_addentry(windowrefs, nref);
    }
    
    return nref;
}

void show_freewindowref(show_windowref *ref) {
    if (windowrefs) linkedlist_removeentry(windowrefs, ref);
    EVAL_FREE(ref);
}

show_windowref *show_lookupwindowref(GLFWwindow *window) {
    show_windowref *ret=NULL;
    if (windowrefs) {
        for (linkedlistentry *e=windowrefs->first; e!=NULL; e=e->next) {
            show_windowref *r=e->data;
            if (r->window==window) { ret=r; break; }
        }
    }
    return ret;
}

show_windowref *show_lookupwindowrefbyref(void *ref) {
    show_windowref *ret=NULL;
    if (windowrefs) {
        for (linkedlistentry *e=windowrefs->first; e!=NULL; e=e->next) {
            show_windowref *r=e->data;
            if (r->ref==ref) { ret=r; break; }
        }
    }
    return ret;
}

/*
 * Redraw handler
 */

/* Test that we have a valid graphics3d object reference */
int show_validategraphicsref(expression *gref) {
    expression_objectreference *href = (expression_objectreference *) gref;
    classgeneric *cls2d=class_lookup(GRAPHICS_LABEL);
    classgeneric *cls3d=class_lookup(GRAPHICS3D_LABEL);
    int ret=FALSE;
    
    if (eval_isobjectref(href)) {
        if (href->obj && (href->obj->clss==cls2d || href->obj->clss==cls3d)) ret=TRUE;
    }
    
    return ret;
}

void show_redraw3d(void *r, int setcontext, float *matrix, float *project) {
    expression_objectreference *gref;
    graphics3d_object *gobj=NULL;
    show_windowref *ref = (show_windowref *) r;
    
    /* Todo: Lots of optimization here! */
    
    float *cp;
    float col[3] = {0.f, 0.f, 0.f};
#ifdef USE_LIBDRAWTEXT
    struct dtx_font *font=NULL;
#endif
    int fontsize=12;
    
    if (!ref) return;
    
    if (setcontext) glfwMakeContextCurrent(ref->window);
    
    /* Clear screen */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glShadeModel (GL_SMOOTH);
    
    /* Enable anti-aliasing */
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_BLEND);
    glEnable(GL_NORMALIZE);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    /* Anything beyond a white background requires us to have something to show, which should have been passed in ref->data. */
    gref=ref->data;
    if (show_validategraphicsref((expression *) gref)) {
        gobj=(graphics3d_object *) gref->obj;
    } else return;
    
    graphics3d_bbox bbox;
    
    /* Switch on the z-buffer */
    glEnable(GL_DEPTH_TEST);
    
   /* GLfloat light_position1[] = { 2, 0, 2, 0.0 };
    GLfloat light_color1[] = { 0, 0.18, 0.5, 1.0 };
    glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_color1 );
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_color1 );
    
    GLfloat light_position2[] = { 2, 2, 3, 0.0 };
    GLfloat light_color2[] = { 0.18, 0.5, 0.18, 1.0 };
    glLightfv(GL_LIGHT2, GL_POSITION, light_position2);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_color2 );
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_color2 );
   
    GLfloat light_position3[] = { 0, 2, 2, 0.0 };
    GLfloat light_color3[] = { 0.5, 0.18, 0, 1.0 };
    glLightfv(GL_LIGHT3, GL_POSITION, light_position3);
    glLightfv(GL_LIGHT3, GL_DIFFUSE, light_color3 );
    glLightfv(GL_LIGHT3, GL_SPECULAR, light_color3 );

    GLfloat light_position4[] = { 0, 0, 2, 0.0 };
    GLfloat light_color4[] = { 0, 0, 0.18, 1.0 };
    glLightfv(GL_LIGHT4, GL_POSITION, light_position4);
    glLightfv(GL_LIGHT4, GL_DIFFUSE, light_color4 );
    glLightfv(GL_LIGHT4, GL_SPECULAR, light_color4 ); */
    
    glEnable(GL_COLOR_MATERIAL);
    
    if (project) {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glMultMatrixf(project);
    }
    
    /* Set up the camera */
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    /* Set up lighting (this is fixed relative to the eye. */
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);
    
    GLfloat light_color0[] = {0.35, 0.35, 0.35, 1.0};
    glLightModelfv( GL_LIGHT_MODEL_AMBIENT, light_color0 );
    
    GLfloat light_colorblack[] = { 0, 0, 0, 1.0};
    
    GLfloat light_position1[] = { 2, 0, 2, 0.0 };
    GLfloat light_color1[] = { 0.37, 0.37, 0.37, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color1 );
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_color1 );
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_colorblack );
    
    GLfloat light_position2[] = { 2, 2, 2, 0.0 };
    GLfloat light_color2[] = { 0.37, 0.37, 0.37, 1.0 };
    glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_color2 );
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_color2 );
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_colorblack );
    
    GLfloat light_position3[] = { 0, 2, 2, 0.0 };
    GLfloat light_color3[] = { 0.37, 0.37, 0.37, 1.0 };
    glLightfv(GL_LIGHT2, GL_POSITION, light_position3);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, light_color3 );
    glLightfv(GL_LIGHT2, GL_SPECULAR, light_color3 );
    glLightfv(GL_LIGHT2, GL_AMBIENT, light_colorblack );
    
    /* Set up the camera */
    /* Center the model at the origin */
    GLfloat center[3] = {0,0,0};
    float v0[3], v1[3], v2[3], m1[9], m2[9];
    
    if (!matrix) {
        graphics3d_calculatebbox(gobj, &bbox);
        /* */
        center[0]=0.5*(bbox.p1[0]+bbox.p2[0]);
        center[1]=0.5*(bbox.p1[1]+bbox.p2[1]);
        center[2]=0.5*(bbox.p1[2]+bbox.p2[2]);
        
        /* Build a triad of orthogonal vectors out of viewdirection and viewvertical*/
        graphics3d_normalizevector(gobj->viewdirection, v0);
        graphics3d_vectorcrossproduct(gobj->viewdirection, gobj->viewvertical, v1);
        graphics3d_normalizevector(v1, v1);
        graphics3d_vectorcrossproduct(v1, gobj->viewdirection, v2);
        graphics3d_normalizevector(v2, v2);

        graphics3d_rotation(v1, PI*ref->beta/180.0, m2);
        graphics3d_rotation(v2, -PI*ref->alpha/180.0, m1);

        graphics3d_transformvector(v0, m2, v1); // Destroys v1!
        graphics3d_transformvector(v1, m1, v0);

        glTranslatef(ref->xx0, ref->yy0, 0.0);

        gluLookAt(center[0]+ref->zoom*v0[0], center[1]+ref->zoom*v0[1], center[2]+ref->zoom*v0[2],
               center[0], center[1], center[2],
               v2[0], v2[1], v2[2]);
    } else {
        glMultMatrixf(matrix);
    }
    
    if (fontsize>100) fontsize=100;
#ifdef USE_LIBDRAWTEXT
    font = dtx_open_font(SHOW_FONT, fontsize);
#endif
    
    if (gobj->list) for (linkedlistentry *e=gobj->list->first; e!=NULL; e=e->next) {
        graphics3d_element *gel=(graphics3d_element *) e->data;
        
        switch (gel->type) {
            case GRAPHICS3D_TEXT:
#ifdef USE_LIBDRAWTEXT
            {
                graphics3d_textelement *text = (graphics3d_textelement *) gel;
                float x=0.0,y=0.0, z=0.0;/* u=0.0,v=0.0;*/
                
                if (font) {
                    glPushMatrix();
                    
                    dtx_use_font(font, fontsize);
                    
                    x=text->point[0]; y=text->point[1]; z=text->point[2];
                  /*  if (text->horizontal != GRAPHICS_ALIGNLEFT) u = dtx_string_width(text->string);
                    if (text->horizontal == GRAPHICS_ALIGNCENTER) u = 0.5*u;
                    if (text->vertical != GRAPHICS_ALIGNBOTTOM) v = dtx_string_height(text->string);
                    if (text->vertical == GRAPHICS_ALIGNMIDDLE) v = 0.5*v; */
                    
                    glTranslatef(x, y, z);
                    glScalef(1.0, 1.0, 1.0);
                    glColor3f(col[0], col[1], col[2]);
                    
                    dtx_string(text->string);
                    
                    glPopMatrix();
                    
                }
            }
#endif
                break;
            case GRAPHICS3D_POINT:
            {
                graphics3d_pointelement *pel = (graphics3d_pointelement *) gel;
                
                glPointSize(5.f);
                glBegin(GL_POINTS);
                glColor3f(col[0],col[1],col[2]);
                glVertex3f(pel->point[0], pel->point[1], pel->point[2]);
                glEnd();
            }
            case GRAPHICS3D_MOVETO:
            {
                graphics3d_pointelement *pel = (graphics3d_pointelement *) gel;
                cp=pel->point;
            }
                break;
            case GRAPHICS3D_LINETO:
            {
                graphics3d_pointelement *pel = (graphics3d_pointelement *) gel;
                if (cp) {
                    glLineWidth(1.0);
                    glBegin(GL_LINES);
                    glColor3f(col[0],col[1],col[2]);
                    glVertex3f(cp[0], cp[1], cp[2]);
                    glVertex3f(pel->point[0], pel->point[1], pel->point[2]);
                    glEnd();
                }
                
                cp=pel->point;
            }
                break;
            case GRAPHICS3D_STROKE:
                break;
            case GRAPHICS3D_SETCOLOR:
            {
                graphics3d_setcolorelement *sel = (graphics3d_setcolorelement *) gel;
                
                col[0]=sel->r; col[1]=sel->g; col[2]=sel->b;
            }
                break;
            case GRAPHICS3D_TRIANGLE:
            {
                graphics3d_triangleelement *gtel = (graphics3d_triangleelement *) gel;
                float s0[3], s1[3], w[3];
                graphics3d_vectorsubtract(gtel->p2, gtel->p1, s0);
                graphics3d_vectorsubtract(gtel->p3, gtel->p2, s1);
                graphics3d_vectorcrossproduct(s0, s1, w);
                
                glColor3f(col[0],col[1],col[2]);
                glBegin(GL_TRIANGLES);
                glNormal3f(w[0],w[1],w[2]);
                glVertex3f(gtel->p1[0], gtel->p1[1], gtel->p1[2]);
                glNormal3f(w[0],w[1],w[2]);
                glVertex3f(gtel->p2[0], gtel->p2[1], gtel->p2[2]);
                glNormal3f(w[0],w[1],w[2]);
                glVertex3f(gtel->p3[0], gtel->p3[1], gtel->p3[2]);
                glEnd();
            }
                break;
            case GRAPHICS3D_TRIANGLE_COMPLEX:
            {
                graphics3d_trianglecomplexelement *gtel=(graphics3d_trianglecomplexelement *) gel;
                
                for (unsigned int i=0; i<gtel->ntriangles; i++) {
                    glBegin(GL_TRIANGLES);
                    
                    for (unsigned int j=0; j<3; j++) {
                        unsigned int n=gtel->triangles[i*3+j];
                        glColor3f(gtel->pointcolors[3*n],gtel->pointcolors[3*n+1],gtel->pointcolors[3*n+2]);
                        glNormal3f(gtel->pointnormals[3*n],gtel->pointnormals[3*n+1],gtel->pointnormals[3*n+2]);
                        glVertex3f(gtel->points[3*n],gtel->points[3*n+1],gtel->points[3*n+2]);
                    }
                    
                    glEnd();
                }
                
                if (gobj->showmesh) {
                    glColor3f(0,0,0);
                    
                    for (unsigned int i=0; i<gtel->ntriangles; i++) {
                        glLineWidth(1.0);
                        glBegin(GL_LINE_LOOP);
                        for (unsigned int j=0; j<3; j++) {
                            unsigned int n=gtel->triangles[i*3+j];
                            glVertex3f(gtel->points[3*n],gtel->points[3*n+1],gtel->points[3*n+2]);
                        }
                        glEnd();
                    }
                }
            }
        }
    }
    
    if (font) dtx_close_font(font);
    
    glDisable(GL_DEPTH_TEST);
}

void show_redraw2d_pointsequence(linkedlistentry *first, linkedlistentry *end) {
    for (linkedlistentry *e=first; e!=end && e!=NULL; e=e->next) {
        graphics_element *el=(graphics_element *) e->data;
        switch (el->type) {
            case GRAPHICS_MOVETO:
            case GRAPHICS_LINETO:
            {
                graphics_pointelement *pel = (graphics_pointelement *) el;
                glVertex2f(pel->point[0], pel->point[1]);
            }
                break;
            default:
                break;
        }
    }
}

void show_redraw2d(show_windowref *ref) {
    graphics_object *g=(graphics_object *) eval_objectfromref(ref->data);
    graphics_bbox bbox;
    linkedlistentry *mv=NULL;
#ifdef USE_LIBDRAWTEXT
    struct dtx_font *font=NULL;
#endif
    float col[3] = {0.f, 0.f, 0.f};
    float width,height,ysf=1.0;
    int fontsize;
    if (!g) return;
    
    graphics_calculatebbox(g, &bbox);
    width=bbox.p2[0]-bbox.p1[0]; height=bbox.p2[1]-bbox.p1[1];
    //if (fabs(width)>MACHINE_EPSILON) xsf=ref->width/width;
    if (fabs(height)>MACHINE_EPSILON) ysf=ref->height/height;
    
    fontsize = (int) 12.0*ysf;
    
    glfwMakeContextCurrent(ref->window);
    
    // Reset identity
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
#define SHOW_2DMARGIN 0.05
    glOrtho((GLdouble) bbox.p1[0]-width*SHOW_2DMARGIN, (GLdouble) bbox.p2[0]+width*SHOW_2DMARGIN, (GLdouble) bbox.p1[1]-height*SHOW_2DMARGIN, (GLdouble) bbox.p2[1]+height*SHOW_2DMARGIN, -1.0, 1.0);
    
    /* Clear screen */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    if (fontsize>100) fontsize=100; 
#ifdef USE_LIBDRAWTEXT
    font = dtx_open_font(SHOW_FONT, fontsize);
#endif

    if (g->list) for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        graphics_element *el=(graphics_element *) e->data;
        
        switch (el->type) {
            case GRAPHICS_SETCOLOR:
            {
                graphics_setcolorelement *sel=(graphics_setcolorelement *) el;
                col[0]=sel->r; col[1]=sel->g; col[2]=sel->b;
            }
                break;
            case GRAPHICS_POINT:
            {
                graphics_pointelement *pel = (graphics_pointelement *) el;
                
                glPointSize(5.f);
                glBegin(GL_POINTS);
                glColor3f(col[0],col[1],col[2]);
                glVertex2f(pel->point[0], pel->point[1]);
                glEnd();
            }
                break;
            case GRAPHICS_MOVETO:
            {
                mv=e;
            }
                break;
            case GRAPHICS_TEXT:
#ifdef USE_LIBDRAWTEXT
            {
                graphics_textelement *text = (graphics_textelement *) el;
                float x=0.0,y=0.0,u=0.0,v=0.0;
                
                if (font) {
                    glPushMatrix();
                    
                    dtx_use_font(font, fontsize);
                    
                    x=text->point[0]; y=text->point[1];
                    if (text->horizontal != GRAPHICS_ALIGNLEFT) u = dtx_string_width(text->string);
                    if (text->horizontal == GRAPHICS_ALIGNCENTER) u = 0.5*u;
                    if (text->vertical != GRAPHICS_ALIGNBOTTOM) v = dtx_string_height(text->string);
                    if (text->vertical == GRAPHICS_ALIGNMIDDLE) v = 0.5*v;
                    
                    glTranslatef(x-u/ysf, y-v/ysf, 0.0);
                    glScalef(1.0/ysf, 1.0/ysf, 1.0);
                    glColor3f(0, 0, 0);
                
                    dtx_string(text->string);
                    
                    glPopMatrix();

                }
            }
#endif
                break;
            case GRAPHICS_LINETO:
                break;
            case GRAPHICS_STROKE:
            {
                glLineWidth(1.0);
                glBegin(GL_LINE_STRIP);
                glColor3f(col[0],col[1],col[2]);
                show_redraw2d_pointsequence(mv, e);
                glEnd();
            }
                break;
            case GRAPHICS_FILL:
            {
                glLineWidth(1.0);
                glBegin(GL_POLYGON);
                glColor3f(col[0],col[1],col[2]);
                show_redraw2d_pointsequence(mv, e);
                glEnd();
            }
                break;
            case GRAPHICS_SHADE_LATTICE:
            {
                graphics_shadelatticeelement *glel=(graphics_shadelatticeelement *) el;
                unsigned int nx=glel->size[0], ny=glel->size[1];
                
                glBegin(GL_TRIANGLES);
                for (unsigned int i=0; i<nx-1; i++) {
                    for (unsigned int j=0; j<ny-1; j++) {
                        // First triangle
                        unsigned int n=(j*nx+i);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                        n=(j*nx+i+1);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                        n=((j+1)*nx+i+1);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                        // Second triangle
                        n=(j*nx+i);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                        n=((j+1)*nx+i+1);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                        n=((j+1)*nx+i);
                        glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                        glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                    }
                }
                glEnd();
                
            }
                break;
            case GRAPHICS_SHADE_TRIANGLES:
            {
                graphics_shadetriangleselement *glel=(graphics_shadetriangleselement *) el;
                
                glBegin(GL_TRIANGLES);
                for (unsigned int i=0; i<glel->size; i++) {
                    unsigned int n=3*i;
                    glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                    glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                    n=3*i+1;
                    glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                    glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                    n=3*i+2;
                    glColor3f(glel->data[5*n+2],glel->data[5*n+3],glel->data[5*n+4]);
                    glVertex2f(glel->data[5*n], glel->data[5*n+1]);
                }
                glEnd();
            }
            default:
                break;
        }
    }
    
    if (font) dtx_close_font(font);
}

void show_redrawblank(show_windowref *ref) {
    
    glfwMakeContextCurrent(ref->window);
    
    // Reset identity
    //glMatrixMode(GL_PROJECTION);
    //glLoadIdentity();
    
    /* Clear screen */
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

}

void show_redraw(show_windowref *r) {
    expression_objectreference *ref=NULL;
    classgeneric *cls2d=class_lookup(GRAPHICS_LABEL);
    classgeneric *cls3d=class_lookup(GRAPHICS3D_LABEL);
    
    if (r) {
        ref=r->data;
        
        if (ref) {
            if (ref->obj->clss==cls2d) show_redraw2d(r);
            else if (ref->obj->clss==cls3d) show_redraw3d(r, TRUE, NULL, NULL);
        } else {
            show_redrawblank(r);
        }
    }
}

/*
 * Callbacks
 */

void show_glfwerrorcallback(int error, const char* description) {
    fputs(description, stderr);
}

void show_perspective( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar )
{
    GLdouble fW, fH;
    
    fH = tan( fovY / 360 * PI ) * zNear;
    fW = fH * aspect;
    
    glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

/* Frame buffer resize callback */
void show_framebuffersizecallback(GLFWwindow* window, int width, int height) {
    show_windowref *ref=show_lookupwindowref(window);
    
    float aspectratio = 1.f;
    
    if (height > 0) aspectratio = (float) width / (float) height;
    
    // Setup viewport
    glViewport(0, 0, width, height);
    
    ref->width=width;
    ref->height=height;
    
    // Change to the projection matrix and set our viewing volume
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    show_perspective(60.0, aspectratio, 1.0, 1024.0);
    
    if (ref) ref->do_redraw=TRUE;
}

/* Keypress callback */
void show_keycallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (action != GLFW_PRESS)
        return;
    
    show_windowref *ref=show_lookupwindowref(window);
    
    switch (key)
    {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_TAB: // Reset view
            ref->alpha=0;
            ref->beta=0;
            ref->xx0=0;
            ref->yy0=0;
            break;
        case GLFW_KEY_LEFT:
            if (ref) {
                if (mods & GLFW_MOD_SHIFT) ref->xx0 -= 0.1;
                else ref->alpha -= 5;
            }
            break;
        case GLFW_KEY_RIGHT:
            if (ref) {
                if (mods & GLFW_MOD_SHIFT) ref->xx0 += 0.1;
                else ref->alpha += 5;
            }
            break;
        case GLFW_KEY_UP:
            if (ref) {
                if (mods & GLFW_MOD_SHIFT) ref->yy0 += 0.1;
                else ref->beta -= 5;
            }
            break;
        case GLFW_KEY_DOWN:
            if (ref) {
                if (mods & GLFW_MOD_SHIFT) ref->yy0 -= 0.1;
                else ref->beta += 5;
            }
            break;
        case GLFW_KEY_PAGE_UP:
            if (ref) {
                ref->zoom -= 0.25f;
                if (ref->zoom < 0.f) ref->zoom = 0.f;
            }
            break;
        case GLFW_KEY_PAGE_DOWN:
            if (ref) ref->zoom += 0.25f;
            break;
        case GLFW_KEY_V:
#ifdef USE_VR
            if (!vr_isActive()) {
                vr_init();
                vr_setref(ref);
            } else {
                vr_finalize();
            }
#endif
        default:
            break;
    }
    
    if (ref) ref->do_redraw=TRUE;
}

/* Mouse button callback */
void show_mousebuttoncallback(GLFWwindow* window, int button, int action, int mods) {
    show_windowref *ref=show_lookupwindowref(window);
    
    switch (button) {
        case GLFW_MOUSE_BUTTON_LEFT:
            if (action == GLFW_PRESS) {
                if (ref) ref->state = DRAGGING_LEFT;
            } else {
                if (ref) ref->state = NORMAL;
            }
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            if (action == GLFW_PRESS) {
                if (ref) ref->state = DRAGGING_RIGHT;
            } else {
                if (ref) ref->state = NORMAL;
            }
            break;
        default:
            break;
    }
}

/* Cursor position callback (for dragging events) */
void show_cursorpositioncallback(GLFWwindow* window, double x, double y) {
    show_windowref *ref=show_lookupwindowref(window);
    
    if (ref) {
        switch (ref->state) {
            case DRAGGING_LEFT:
                ref->alpha += (GLfloat) (x - ref->cursorX) / 10.f;
                ref->beta += (GLfloat) (y - ref->cursorY) / 10.f;
                break;
            case DRAGGING_RIGHT:
                ref->xx0 += (GLfloat) (x - ref->cursorX) / 50.f;
                ref->yy0 -= (GLfloat) (y - ref->cursorY) / 50.f;
                break;
            default:
                break;
        }
        
        ref->cursorX = (int) x;
        ref->cursorY = (int) y;
        
        ref->do_redraw=TRUE;
    }
}

/* Scroll callback */
void show_scrollcallback(GLFWwindow* window, double x, double y) {
    show_windowref *ref=show_lookupwindowref(window);
    
    if (ref) {
        ref->zoom += (float) y / 4.f;
        if (ref->zoom < 0)
            ref->zoom = 0;
        
        ref->do_redraw=TRUE;
    }
}

void show_openwindow(void *ref, void *data) {
    show_windowref *wref=NULL;
    GLFWwindow* window=NULL;
    int width, height;
    
    window = glfwCreateWindow(640, 480, "Morpho", NULL, NULL);
    if (window) {
        wref=show_newwindowref(window, ref, data);
        
        // Add callbacks
        glfwMakeContextCurrent(window);
        glfwSetKeyCallback(window, show_keycallback);
        glfwSetFramebufferSizeCallback(window, show_framebuffersizecallback);
        glfwSetMouseButtonCallback(window, show_mousebuttoncallback);
        glfwSetCursorPosCallback(window, show_cursorpositioncallback);
        glfwSetScrollCallback(window, show_scrollcallback);
        
        // Enable vsync
        glfwMakeContextCurrent(window);
        glfwSwapInterval(1);
        
        glfwGetFramebufferSize(window, &width, &height);
        show_framebuffersizecallback(window, width, height);
        wref->width=width; wref->height=height;
        wref->do_redraw=TRUE;
        
        // Increment the counter of windows
        nwindows++;
    }
}

void show_closewindow(show_windowref *ref) {
    glfwDestroyWindow(ref->window);
    if (ref->data) freeexpression((expression *) ref->data);
    
    show_sendmessage(SHOW_CLOSED, ref->ref, NULL);
    
    show_freewindowref(ref);
    // Decrement the number of windows
    nwindows--;
    if (nwindows==0) lastclosed=4; // Last window has been closed
}

void show_updatewindow(show_windowref *ref, void *data) {
    if (ref) {
        if (ref->data) {
            freeexpression((expression *) ref->data);
        } else {
            
        }
        ref->data=data;
        ref->do_redraw=TRUE;
    }
}


/*
 * Monitor our windows
 */

/* Check if any should be redrawn */
void show_checkredraw(void) {
    if (windowrefs) {
        for (linkedlistentry *e=windowrefs->first; e!=NULL; e=e->next) {
            show_windowref *r=e->data;
            if (r->do_redraw && glfwGetWindowAttrib(r->window, GLFW_VISIBLE)) {
                show_redraw(r);
                
                glfwSwapBuffers(r->window);
                
                r->do_redraw=FALSE;
            }
        }
    }
}

/* Check if any should close */
void show_checkclose(void) {
    if (windowrefs) {
        for (linkedlistentry *e=windowrefs->first; e!=NULL; e=e->next) {
            show_windowref *r=e->data;
            if (glfwWindowShouldClose(r->window)) {
                show_closewindow(r); break;
            }
        }
    }
}

/*
 * Main thread.
 * This thread is responsible for issuing GLFW events etc.
 */

void show_mainthread(pthread_t *clithread) {
    show_message *msg;
    lastclosed=0;
    
    for (int quit=FALSE; !quit;) {
        if (nwindows==0 && lastclosed==0) {
            /* If no windows are open, the main thread remains in a SHOW_MAINTHREADRESTING state until awoken by sending a message. */
            pthread_mutex_lock(&show_state_mutex);
            show_state=SHOW_MAINTHREADRESTING;
            pthread_mutex_unlock(&show_state_mutex);
            
            pthread_mutex_lock(&show_restmutex);
            /* And await to be awoken with a message */
            pthread_cond_wait(&show_restcondition, &show_restmutex);
            pthread_mutex_unlock(&show_restmutex);
        } else {
            /* Otherwise, the main thread receives GLFW messages */
            pthread_mutex_lock(&show_state_mutex);
            show_state=SHOW_MAINTHREADAWAKE;
            pthread_mutex_unlock(&show_state_mutex);
            
            /* Main loop */
            while (!show_messageavailable() && ( nwindows>0 || lastclosed)) {
                show_checkredraw();
#ifdef USE_VR
                if (vr_isActive()) {
                    vr_loop();
                    glfwPollEvents();
                } else {
                    glfwWaitEvents();
                }
#else
                glfwWaitEvents();
#endif
                show_checkclose();
                if (lastclosed) lastclosed--;
            }
        }
        
        /* Process the messages */
        while (show_messageavailable()) {
            msg=show_getmessage();
            if (msg) {
                switch (msg->type) {
                    case SHOW_OPEN:
                        show_openwindow(msg->ref, msg->data);
                        break;
                    case SHOW_CLOSE:
                    {
                        show_windowref *ref=show_lookupwindowrefbyref(msg->ref);
                        if (ref) show_closewindow(ref);
                    }
                        break;
                    case SHOW_CLOSED:
                    {
                        object *showobj = msg->ref;
                        class_release(showobj);
                    }
                        break;
                    case SHOW_UPDATE:
                    {
                        show_windowref *ref=show_lookupwindowrefbyref(msg->ref);
                        if (ref) show_updatewindow(ref, msg->data);
                    }
                        break;
                    case SHOW_QUIT:
                        quit=TRUE;
                        break;
                }
                show_freemessage(msg);
            }
        }
    
    }
}

/*
 * CLI thread. GLFW reequires that calls are on the main thread. We therefore spin off the cli as a secondary thread.
 */

typedef struct {
    char *fin;
    char *fout;
    int quiet;
} show_clithreadtype;

/* This function is executed by the secondary CLI thread. */
void *show_clithread(void *ref) {
    show_clithreadtype *r = (show_clithreadtype *) ref;
    
    /* Jump into the cli. */
    cli(r->fin, r->fout, r->quiet);
    
    /* Once the cli quits, tell the main thread to quit too. */
    show_sendmessage(SHOW_QUIT, NULL, NULL);
    
    pthread_exit(NULL);
}

/* This function is called by main(). It sets up the CLI in a secondary thread and runs GLFW related matters on the main thread. */
void show_cli(char *fin, char *fout, int quiet) {
    show_clithreadtype ref;
    pthread_t clithread;
    int rc;
    
    ref.fin=fin; ref.fout=fout; ref.quiet=quiet;
    
    rc = pthread_create(&clithread, NULL, show_clithread, (void *) &ref);
    
    if (!rc) {
        show_mainthread(&clithread);
        
        /* Make sure we wait for the clithread to finish */
        pthread_join(clithread, NULL);
    }
}

/*
 * Show functions
 */

expression *show_new() {
    classgeneric *c=class_lookup(SHOW_LABEL);
    show_object *obj=(show_object *) class_instantiate(c);
    expression_objectreference *ref=NULL;
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/*
 * Show selectors
 */

expression *show_tographics(interpretcontext *context, expression *exp, object **update) {
    expression *data=NULL;
    
    /* Is the argument a graphics object? */
    if (show_validategraphicsref(exp)) {
        data=cloneexpression(exp);
    } else {
        /* Or does it respond to the draw() selector? */
        if (eval_isobjectref(exp)) {
            expression_objectreference *oref = (expression_objectreference *) exp;
            
            if (class_objectrespondstoselector(oref->obj, SHOW_DRAW)) {
                data=class_callselector(context, oref->obj, SHOW_DRAW, 0);
                
                /* If the result isn't a graphics object, get rid of it. */
                if (!show_validategraphicsref(data)) {
                    freeexpression((expression *) data);
                    data=NULL;
                }
            } else if (class_objectrespondstoselector(oref->obj, GRAPHICS_TOGRAPHICS)) {
                data=class_callselector(context, oref->obj, GRAPHICS_TOGRAPHICS, 0);
                
                /* If the result isn't a graphics object, get rid of it. */
                if (!show_validategraphicsref(data)) {
                    freeexpression((expression *) data);
                    data=NULL;
                }
            }
            
            /* If the object responds to changed keep a note of this. */
            if (update) if (class_objectrespondstoselector(oref->obj, GEOMETRY_CHANGED_SELECTOR)) {
                *update = oref->obj;
            }
        }
    }
    
    return data;
}

/* New */
expression *show_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    //show_object *sobj = (show_object *) obj;
    return NULL;
}

/* Free */
expression *show_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    //show_object *sobj = (show_object *) obj;
    return NULL;
}

/* Update */
expression *show_updatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *data=NULL;
    
    if (nargs>0) {
        data=show_tographics(context, args[0], NULL);
    }
    
    if (data) {
        show_sendmessage(SHOW_UPDATE, (show_object *) obj, data);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Close */
expression *show_closei(object *obj, interpretcontext *context, int nargs, expression **args) {
    show_object *sobj = (show_object *) obj;
    
    show_sendmessage(SHOW_CLOSE, (void *) sobj, NULL);
    
    return (expression *) class_newobjectreference(obj);
}

/*
 * Show constructor
 */

expression *show_show(interpretcontext *context, int nargs, expression **args) {
    expression_objectreference *sref=NULL;
    expression *data=NULL;
    object *update=NULL;
    
    sref=(expression_objectreference *) show_new();
    
    if (nargs>0) {
        data=show_tographics(context, args[0], &update);
    }
    
    if (eval_isobjectref(sref)) {
        show_sendmessage(SHOW_OPEN, (show_object *) sref->obj, data);
        
        /* Register listener */
        if (update) freeexpression(class_callselector(context, update, EVAL_ADDLISTENER, 3, EXPRESSION_NAME, GEOMETRY_CHANGED_SELECTOR, EXPRESSION_OBJECT_REFERENCE, sref, EXPRESSION_NAME, SHOW_UPDATESELECTOR));
        
        /* Retain the object by the gui, because the window may persist longer than the immediate response. */
        class_retain(sref->obj);
    }
    
    return (expression *) sref;
}

#endif

/*
 * Initialization
 */

/* Initializes the show class, and also the library. */
void showinitialize(void) {
#ifdef USE_GLFW
    classintrinsic *cls;
    
    nwindows=0;
    lastclosed=0;
    
    cls=class_classintrinsic(SHOW_LABEL, class_lookup(EVAL_OBJECT), sizeof(show_object), 10);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, show_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, show_newi);
    class_registerselector(cls, SHOW_CLOSESELECTOR, FALSE, show_closei);
    class_registerselector(cls, SHOW_UPDATESELECTOR, FALSE, show_updatei);
    
    /*class_registerselector(cls, SHOW_VIEWDIRECTION, FALSE, graphics3d_getviewdirectioni);
     class_registerselector(cls, SHOW_SETVIEWDIRECTION, FALSE, graphics3d_setviewdirectioni);
     class_registerselector(cls, SHOW_VERTICAL, FALSE, graphics3d_getviewverticali);
     class_registerselector(cls, SHOW_SETVERTICAL, FALSE, graphics3d_setviewverticali);*/
    
    show_initializecomm();
    show_initializewindowrefs();
    
    glfwSetErrorCallback(show_glfwerrorcallback);
    if (!glfwInit()) exit(EXIT_FAILURE);
    
    intrinsic(SHOW_LABEL, FALSE, show_show, NULL);
#endif
}

void showfinalize(void) {
#ifdef USE_GLFW
    show_finalizecomm();
    show_finalizewindowrefs();
    
    glfwTerminate();
#endif
}


