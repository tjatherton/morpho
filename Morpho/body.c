/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "body.h"

/*
 * Body low level functions
 */

expression *body_new(void) {
    body_object *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(body_object *) class_instantiate(class_lookup(GEOMETRY_BODYCLASS));
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/* Adds a manifold to the body */
void body_addmanifold(body_object *obj, manifold_object *mobj) {
    if (obj) {
        if (!obj->manifolds) obj->manifolds=linkedlist_new();
        
        if (obj->manifolds) linkedlist_addentry(obj->manifolds, (void *) class_newobjectreference((object *) mobj));
    }
}

/* TO DO */
void body_removemanifold(body_object *obj, manifold_object *mobj);


/* Adds a field to the body */
void body_addfield(body_object *obj, field_object *fobj) {
    if (obj) {
        if (!obj->fields) obj->fields=linkedlist_new();
        
        if (obj->fields) linkedlist_addentry(obj->fields, (void *) class_newobjectreference((object *) fobj));
    }
}

/* Creates a new functional ref */
functionalref *body_newfunctionalref(functionaltype type, functional_object *fobj, selection_object *sel) {
    functionalref *new = EVAL_MALLOC(sizeof(functionalref));
    
    if (new) {
        new->type=type;
        new->sel=NULL;
        if (fobj) new->ref=class_newobjectreference((object *)fobj);
        if (sel) new->sel=class_newobjectreference((object *)sel);
    }
    
    return new;
}

/* Frees a functional ref */
void body_freefunctionalref(functionalref *ref) {
    if (ref) {
        freeexpression((expression *) ref->ref);
        EVAL_FREE(ref);
    }
}

/* Adds a functional to the body */
void body_addfunctional(body_object *obj, functional_object *fobj, functionaltype type, selection_object *sel) {
    functionalref *ref = body_newfunctionalref(type, fobj, sel);
    
    if ((obj)&&(ref)) {
        if (!obj->functionalrefs) obj->functionalrefs=linkedlist_new();
        
        if (obj->functionalrefs) linkedlist_addentry(obj->functionalrefs, (void *) ref);
    } else {
        body_freefunctionalref(ref);
    }
}

/* Removes a functional from the body */
void body_removefunctional(body_object *bobj, functional_object *fobj) {
    if ((bobj->functionalrefs)&&(bobj->functionalrefs->first)) {
        /* Loop over functionals */
        for (linkedlistentry *e=bobj->functionalrefs->first; e!=NULL; e=e->next) {
            functionalref *f = (functionalref *) e->data;
            
            if (f && f->ref && f->ref->obj) {
                if ((functional_object *) f->ref->obj== fobj) {
                    linkedlist_removeentry(bobj->functionalrefs, f); /* Remove from list */
                    body_freefunctionalref(f); /* And free the reference */
                    break;
                }
            };
        }
    }
}

/*
 * Calculate energy and forces
 */

/* Wrapper to call a specified selector on the functionals. 
   This can be used to compute forces or energies due to a specific functional and manifold.
   Use selector -> GEOMETRY_FORCE_SELECTOR for forces.
   Use seelctor -> */
expression *body_functionalcompute(body_object *obj, interpretcontext *context, char *selector, expression_objectreference *functional, expression_objectreference *mobj, expression_objectreference *sel) {
    expression *exp = NULL;
    
    if ((functional)&&(functional->obj)) {
        if (class_objectrespondstoselector(functional->obj, selector)) {
            if (sel) {
                exp=class_callselector(context, functional->obj, selector, 2, EXPRESSION_OBJECT_REFERENCE, (expression *) mobj, EXPRESSION_OBJECT_REFERENCE, (expression *) sel);
            } else {
                exp=class_callselector(context, functional->obj, selector, 1, EXPRESSION_OBJECT_REFERENCE, (expression *) mobj);
            }
        }
    }
    
    return exp;
}

/* Simplifies accumulation. */
expression *body_functionalaccumulate(interpretcontext *context, expression *total, expression *exp) {
    expression *add;
    if (total) {
        if (exp) {
            add = opadd(context, total, exp);
            if (total) freeexpression(total);
            if (exp) freeexpression(exp);
        } else add=total;
    } else {
        add=exp;
    }
    return add;
}

/* Finds the total value of a functional on the body, looped over all manifolds. */
expression *body_functionaltotal(body_object *obj, interpretcontext *context, char *selector, expression_objectreference *functional, expression_objectreference *sel) {
    expression *exp=NULL;
    expression *total=NULL;
    
    /* Loop over manifolds */
    if (obj->manifolds) for (linkedlistentry *m = obj->manifolds->first; m!=NULL; m=m->next) {
        expression_objectreference *mref = (expression_objectreference *) m->data;
        
        exp = body_functionalcompute(obj, context, selector, functional, mref, sel);
        total = body_functionalaccumulate(context, total, exp);
    }
    
    return total;
}

/* Finds the total energy of a body. */
expression *body_totalenergy(body_object *obj, interpretcontext *context) {
    expression *exp=NULL;
    expression *total=NULL;

    if ((obj->functionalrefs)&&(obj->functionalrefs->first)) {
        /* Loop over energies */
        for (linkedlistentry *e=obj->functionalrefs->first; e!=NULL; e=e->next) {
            functionalref *f = (functionalref *) e->data;
            
            if ((f->type==GEOMETRY_ENERGY)&&(f->ref)) {
                exp = body_functionaltotal(obj, context, GEOMETRY_TOTAL_SELECTOR, f->ref, f->sel);
                if (exp) total = body_functionalaccumulate(context, total, exp);
            }
        }
    }
    
    return total;

}

/* Computes the total force for a manifold by summing over all the energies */
expression *body_totalforceformanifold(body_object *obj, interpretcontext *context, expression_objectreference *mref) {
    expression *force=NULL, *exp=NULL;
    
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        
        if (func->type==GEOMETRY_ENERGY) {
            exp=body_functionalcompute(obj, context, GEOMETRY_FORCE_SELECTOR, func->ref, mref, func->sel);
            force=body_functionalaccumulate(context, force, exp);
        }
    }

    return force;
}

/* Subtracts local constraints from the force */
expression *body_subtractlocalconstraints(body_object *obj, interpretcontext *context, expression_objectreference *mref, expression *frc, expression_objectreference *omit) {
    expression *force = frc, *exp=NULL;
    
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        expression *nforce = NULL;
        
        if (func->type==GEOMETRY_LOCALCONSTRAINT && !eval_objectrefeq(omit, func->ref)) {
            /* Compute the force associated with the constraint */
            exp=body_functionalcompute(obj, context, GEOMETRY_FORCE_SELECTOR, func->ref, mref, func->sel);
            
            int os = functional_isonesided(func->ref, context);
            
            /* Subtract the projection of force onto exp. */
            nforce=field_localproject(context, (expression_objectreference *) force, (expression_objectreference *) exp, os);
            if (exp) freeexpression(exp);
            
            if (nforce) {
                if (force) freeexpression(force);
                force = nforce;
            }
        }
    }
    
    return force;
}

void body_subtractprojectedcomponentwithtype_zeromapfunction(uid id, void *e, void *r ) {
    field_object *field = (field_object *) r;
    
    field_insertwithid(field, id, newexpressionfloat(0.0));
}

/* Subtracts the projected components of forces with a given type in the functional list from a given force. 
   (Primarily intended for constraint projection -> use type = GEOMETRY_CONSTRAINT).
   omit can be used to prevent subtracting a constraint from itself.
   Returns a new force vector. */
expression *body_subtractprojectedcomponentwithtype(body_object *obj, interpretcontext *context, expression_objectreference *mref, expression *frc, expression_objectreference *omit, functionaltype type, int zero) {
    expression *force = frc, *exp=NULL;
    
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        expression *nforce = NULL;
        
        if (func->type==type && !eval_objectrefeq(omit, func->ref)) {
            /* Compute the force associated with the constraint */
            exp = body_functionalcompute(obj, context, GEOMETRY_FORCE_SELECTOR, func->ref, mref, func->sel);
            /* Subtract local constraints from the force */
            exp = body_subtractlocalconstraints(obj, context, mref, exp, NULL);
            
            /* Zero fixed components */
            if (zero) if (eval_isobjectref(exp) && eval_isobjectref(force)) {
                field_object *expobj = (field_object *) eval_objectfromref(exp);
                field_object *forceobj = (field_object *) eval_objectfromref(force);
                
                idtable_mapcomplement(expobj->entries, forceobj->entries, body_subtractprojectedcomponentwithtype_zeromapfunction, expobj);
            }
            
            /* Subtract the projection of force onto exp. */
            nforce=field_project(context, (expression_objectreference *) force, (expression_objectreference *) exp);
            if (exp) freeexpression(exp);
            
            if (nforce) {
                if (force) freeexpression(force);
                force = nforce;
            }
        }
    }
    
    return force;
}



void manifold_fixwithselectionmapfunction(uid id, void *e, void *r ) {
    field_object *field = (field_object *) r;
    manifold_vertex *v=(manifold_vertex *) e;
    expression *exp=NULL;
    
    if (manifold_isvertexentryfixed(v)) {
        exp=idtable_get(field->entries, id);
        if (exp) {
            freeexpression(exp);
            idtable_remove(field->entries, id);
        }
    }
}

void body_forcefix(body_object *obj, interpretcontext *context, expression_objectreference *mref, expression_objectreference *force) {
    manifold_object *mobj=NULL;
    field_object *frc=NULL;
    
    if (mref) mobj= (manifold_object *) mref->obj;
    if (force) frc= (field_object *) force->obj;
    
    if ((mref)&&(frc)) {
        idtable_map(mobj->vertices, manifold_fixwithselectionmapfunction, (void *) frc);
    }
}

linkedlist *body_totalforce(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel) {
    linkedlist *mfrc=linkedlist_new();

    expression *force=NULL;
    
    if ((!obj)||(!obj->manifolds)||(!mfrc)) return NULL;
    
    /* Sequence */
    /* For each manifold... */
    for (linkedlistentry *e=obj->manifolds->first; e!=NULL; e=e->next) {
        expression_objectreference *mref = (expression_objectreference *) e->data;
        
        /* 1. Sum all conventional forces */
        if (func) {
            force = body_functionalcompute(obj, context, GEOMETRY_FORCE_SELECTOR, func, mref, sel);
        } else {
            force = body_totalforceformanifold(obj, context, mref);
        }
        
        /* 2. Set all forces on fixed vertices to zero. */
        body_forcefix(obj, context, mref, (expression_objectreference *) force);
        
        /* 3. Project forces along constraint directions. (see surface evolver manual 16.7.2) */
        force = body_subtractprojectedcomponentwithtype(obj, context, mref, force, func, GEOMETRY_CONSTRAINT, TRUE);
        
        /* 4. Subtract local constraints */
        force = body_subtractlocalconstraints(obj, context, mref, force, func);
        
        /* Retain the total force */
        linkedlist_addentry(mfrc, force);
    }
    
    return mfrc;
}

void body_movevertices(body_object *obj, interpretcontext *context, linkedlist *mfrc, MFLD_DBL scale) {
    linkedlistentry *f=mfrc->first;
    
    for (linkedlistentry *e=obj->manifolds->first; e!=NULL && f!=NULL; e=e->next) {
        expression_objectreference *mref = (expression_objectreference *) e->data;
        
        manifold_shiftvertices((manifold_object *) mref->obj, (expression_objectreference *) f->data, scale);
        
        f=f->next;
    }
}

/* Call this whenever the body changes, so that listeners can be informed. */
void body_changed(body_object *obj, interpretcontext *context) {
    expression *exp;
    
    exp=class_callselector(context, (object *) obj, GEOMETRY_CHANGED_SELECTOR, 0);
    
    freeexpression(exp);
}

/*
 * Back projection onto constraints
 */

/* Identifies constraint functionals on a body, returning a list and the number of constraints. Call with constraints=NULL to identify the number of constraints.  */
void body_identifyconstraints(body_object *obj, functionalref **constraints, functionalref **lconstraints, unsigned int *ncons, unsigned int *nlocalcons) {
    unsigned int nc=0, nlc=0;
    
    /* Loop over constraints */
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        
        if (func->type==GEOMETRY_CONSTRAINT) {
            if (constraints) constraints[nc]=func;
            nc++;
        }
        if (func->type==GEOMETRY_LOCALCONSTRAINT) {
            if (lconstraints) lconstraints[nlc]=func;
            nlc++;
        }
    }
    
    if (ncons) *ncons = nc;
    if (nlocalcons) *nlocalcons = nlc;
}

MFLD_DBL body_fieldlistdot(body_object *obj, interpretcontext *context, linkedlist *f1, linkedlist *f2) {
    MFLD_DBL result=0;
    unsigned int length=linkedlist_length(f1);
    
    if (linkedlist_length(f2)==length) {
        for (unsigned int i=0; i<length; i++) {
            linkedlistentry *e1=linkedlist_element(f1, i);
            linkedlistentry *e2=linkedlist_element(f2, i);
            
            if (e1 && e1->data && e2 && e2->data) {
                result += field_dot(context, (expression_objectreference *) e1->data, (expression_objectreference *) e2->data);
            }
        }
    }
    return result;
}

typedef struct {
    interpretcontext *context;
    expression_objectreference *value;
    field_object *dx; // To receive motions on vertices
    MFLD_DBL target;
    MFLD_DBL norm; // Set to zero before calling field_map -- the final value contains the final norm.
} body_localconstraintsmapfunctionref;

void body_projectontolocalconstraintsmapfunction (uid id, void *content, void *r) {
    body_localconstraintsmapfunctionref *ref = r;
    expression_objectreference *item = content;
    
    multivectorobject *mv=(multivectorobject *) item->obj;
    expression *norm=NULL, *val=NULL;
    MFLD_DBL fnorm=0.0, cv=0.0, dv=0.0, scale=0.0;
    
    field_object *value=(field_object *) ref->value->obj;
    
    if (mv) {
        /* Norm of the force vector */
        norm=multivector_gradenorm(mv, MANIFOLD_GRADE_LINE, 1.0);
        if (norm) fnorm=eval_floatvalue(norm);
        
        /* The current value, and update the norm */
        val=field_get(value, id);
        
        if (eval_isreal(val)) {
            cv=eval_floatvalue(val);
            dv=cv - ref->target;
            
            ref->norm+=(dv*dv); /* Update the norm */
            
            /* Calculate a restoring motion and output it to the field */
            if (fnorm*fnorm>MACHINE_EPSILON && ref->dx) {
                scale=dv/(fnorm*fnorm);
                
                expression_objectreference *newmvexp = (expression_objectreference *) multivector_clone(mv);
                if (eval_isobjectref(newmvexp)) multivector_scale((multivectorobject *) newmvexp->obj, ref->context, scale);
                
                field_accumulatewithid(ref->dx, ref->context, id, (expression *) newmvexp);
                if (newmvexp) freeexpression((expression *) newmvexp);
            }
        }
        
    }
    
    if (norm) freeexpression(norm);
}

expression_objectreference *body_manifoldfromindex(body_object *obj, interpretcontext *context, unsigned int i) {
    linkedlistentry *el=linkedlist_element(obj->manifolds, i);
    expression_objectreference *ret=NULL;
    
    if (el) ret = (expression_objectreference *) el->data;
    
    if (!eval_isobjectref(ret)) ret=NULL;
    
    return ret;
}

/* Gets the integrand for a given manifold */
expression_objectreference *body_integrandforfuncwithmanifold(body_object *obj, interpretcontext *context, expression_objectreference *manifold, expression_objectreference *func, expression_objectreference *sel) {
    expression_objectreference *ret = NULL;
    
    if (eval_isobjectref(func) && class_objectrespondstoselector(func->obj, GEOMETRY_INTEGRAND_SELECTOR)) {
        if (sel) {
            ret = (expression_objectreference *) class_callselector(context, func->obj, GEOMETRY_INTEGRAND_SELECTOR, 2, EXPRESSION_OBJECT_REFERENCE, manifold, EXPRESSION_OBJECT_REFERENCE,  sel);
        } else {
            ret = (expression_objectreference *) class_callselector(context, func->obj, GEOMETRY_INTEGRAND_SELECTOR, 1, EXPRESSION_OBJECT_REFERENCE, manifold);
        }
    }
    
    return ret;
}

void body_projectontolocalconstraints(body_object *obj, interpretcontext *context) {
    unsigned int nc=0;
    unsigned int n=0;
    classgeneric *fieldclass = NULL;
    body_localconstraintsmapfunctionref ref;
    unsigned int nmanifolds = 0;
    int success=FALSE;
    
    body_identifyconstraints(obj, NULL, NULL, NULL, &nc);
    
    if (nc==0) return; /* No need to reproject */
    
    /* Some initialization */
    ref.context = context;
    fieldclass=class_lookup(GEOMETRY_FIELDCLASS);
    nmanifolds=linkedlist_length(obj->manifolds);
    
    functionalref *constraints[nc];
    MFLD_DBL target[nc];
    
    body_identifyconstraints(obj, NULL, constraints, NULL, &nc);
    
    /* Get the target values */
    for (unsigned int i=0; i<nc; i++) {
        if (!functional_gettarget(constraints[i]->ref->obj, context, &target[i])) {
            target[i]=0;
        }
    }
    
    /* Now iterate */
    for (n=0; n<GEOMETRY_MAXREPROJECTIONITERATIONS && (!success); n++) {
        /* Create fields to hold the proposed move */
        expression_objectreference *dx[nmanifolds];
        linkedlist *dxlist = linkedlist_new();
        if (dxlist) for (unsigned int j=0; j<nmanifolds; j++) {
            expression_objectreference *m=body_manifoldfromindex(obj, context, j);
        
            dx[j]=(expression_objectreference *) field_new((manifold_object *) m->obj, MANIFOLD_GRADE_LINE);
            linkedlist_addentry(dxlist, dx[j]);
        }
        
        /* Loop over constraints... */
        for (unsigned int i=0; i<nc; i++) {
            ref.target=target[i];
            /* ... and manifolds */
            for (unsigned int j=0; j<nmanifolds; j++) {
                expression_objectreference *m=body_manifoldfromindex(obj, context, j);
                expression_objectreference *v = body_integrandforfuncwithmanifold(obj, context, m, constraints[i]->ref, constraints[i]->sel);
                
                expression_objectreference *f=(expression_objectreference *) body_functionalcompute(obj, context, GEOMETRY_FORCE_SELECTOR, constraints[i]->ref, m, constraints[i]->sel);
            
                ref.value = v;
                ref.norm = 0;
                if (eval_isobjectref(dx[j])) ref.dx = (field_object *) dx[j]->obj;
                
                if (eval_isobjectref(f) && f->obj->clss==fieldclass && eval_isobjectref(v) && v->obj->clss==fieldclass) {
                    field_map((field_object *) f->obj, body_projectontolocalconstraintsmapfunction, &ref);
                }
                
                freeexpression((expression *) v);
                freeexpression((expression *) f);
            }
        }
        
        //printf("Local constraint iteration %i residual %g.\n", n, sqrt(ref.norm));
        
        if (sqrt(ref.norm)<sqrt(MACHINE_EPSILON)) success=TRUE;
        else {
            body_movevertices(obj, context, dxlist, 1.0);
        }
        
        /* Free proposed move fields */
        linkedlist_free(dxlist);
        for (unsigned int j=0; j<nmanifolds; j++) {
            if (dx[j]) freeexpression((expression *) dx[j]);
        }
    }
    
    if (n>=GEOMETRY_MAXREPROJECTIONITERATIONS) {
        error_raise(context, GEOMETRY_REPROJECTMAX, GEOMETRY_REPROJECTMAX_MSG, ERROR_WARNING);
    }
}

void body_projectontoconstraints(body_object *obj, interpretcontext *context) {
    unsigned int nc=0;
    unsigned int n=0;
    
    body_projectontolocalconstraints(obj, context);
    
    body_identifyconstraints(obj, NULL, NULL, &nc, NULL);
    
    if (nc==0) return; // No need to reproject
    
    if (nc>1) printf("WARNING: Multiple constraints may not be reprojected correctly. Please consult developer.\n");
    
    functionalref *constraints[nc];
    MFLD_DBL target[nc];
    MFLD_DBL cv[nc];
    MFLD_DBL dv[nc];
    MFLD_DBL odv[nc];
    linkedlist *mfrc[nc];
    
    body_identifyconstraints(obj, constraints, NULL, &nc, NULL);
    
    /* Get the target values */
    for (unsigned int i=0; i<nc; i++) {
        if (!functional_gettarget(constraints[i]->ref->obj, context, &target[i])) {
            // If the functional has no target, remove it from consideration by copying the rest of the constraints over it.
            for (unsigned int j=i; j<nc-1; j++) constraints[j]=constraints[j+1];
            nc--;
        }
        mfrc[i]=NULL;
    }
    
    /* Now iterate */
    for (n=0; n<GEOMETRY_MAXREPROJECTIONITERATIONS; n++) {
        MFLD_DBL t=0.0;
        double norm=0.0;
        
        /* Get current values and determine discrepency */
        for (unsigned int i=0; i<nc; i++) {
            cv[i] = body_evaluateenergy(obj, context, constraints[i]->ref, constraints[i]->sel);
            dv[i] = cv[i]-target[i];
            norm+=(dv[i]*dv[i]);
            
            if (n>0 && fabs(dv[i])>fabs(odv[i])) {
                error_raise(context, GEOMETRY_CONSTRAINTDIVERGING, GEOMETRY_GEOMETRY_CONSTRAINTDIVERGING_MSG, ERROR_FATAL);
            }
        }

        //printf("Constraint iteration %i residual %g\n", n, sqrt(norm));
        
        /* Have we reprojected well enough? */
        if (sqrt(norm)<sqrt(MACHINE_EPSILON)) break;
        
        /* Get forces */
        for (unsigned int i=0; i<nc; i++) {
            mfrc[i] = body_totalforce(obj, context, constraints[i]->ref, constraints[i]->sel);
        }
        
        /* Solve for perturbation required to reproject.
           TODO: Incomplete; should solve the linear system for all the forces */
        t=body_fieldlistdot(obj, context, mfrc[0], mfrc[0]);
        if (fabs(t)>MACHINE_EPSILON) {
            t=dv[0]/t;
            
            body_movevertices(obj, context, mfrc[0], t);
        }
        
        for (unsigned int i=0; i<nc; i++) {
            odv[i]=dv[i];
        }
        
        /* Free any temporarily allocated forces */
        for (unsigned int i=0; i<nc; i++) {
            if (mfrc[i]) {
                linkedlist_map(mfrc[i], (linkedlistmapfunction *) &freeexpression);
                linkedlist_free(mfrc[i]);
                mfrc[i]=NULL;
            }
        }
    }
    
    if (n>=GEOMETRY_MAXREPROJECTIONITERATIONS) {
        error_raise(context, GEOMETRY_REPROJECTMAX, GEOMETRY_REPROJECTMAX_MSG, ERROR_WARNING);
    }
}


/* 
 * Relaxation functions
 */

typedef struct {
    expression_objectreference *manifold;
    expression_objectreference *functional;
    expression *force;
} forcetable;

/* relax -- simply take a step down the gradient. */
void body_relax(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale) {
    linkedlist *mfrc=NULL;
    
    /* Compute the total force on the body, with constraint directions */
    mfrc = body_totalforce(obj, context, func, sel);
    
    /* Move vertices. */
    body_movevertices(obj, context, mfrc, scale);
    
    /* Back projection of variables onto constraints. */
    body_projectontoconstraints(obj, context);
    
    /* Notify all listeners. */
    body_changed(obj, context);
    
    /* Free any temporarily allocated things */
    if (mfrc) {
        linkedlist_map(mfrc, (linkedlistmapfunction *) &freeexpression);
        linkedlist_free(mfrc);
    }
}

#define MFLD_GOLD 1.61803
//#define MFLD_GOLD 2

MFLD_DBL body_evaluateenergy(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel) {
    expression *exp=NULL;
    MFLD_DBL ret = 0.0;
    
    if (func) {
        exp = body_functionaltotal(obj, context, GEOMETRY_TOTAL_SELECTOR, func, sel);
    } else {
        exp = body_totalenergy(obj, context);
    }
    
    if (exp) {  ret = eval_floatvalue(exp); freeexpression(exp); }
    return ret;
}

/* linesearch -- perform a linesearch down the gradient direction of functional fref, or the total energy if
   no functional is supplied. */
void body_linesearch(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale, MFLD_DBL scale_limit) {
    linkedlist *mfrc=NULL;
    MFLD_DBL energy[3] = {0.0, 0.0, 0.0}; /* energies at different values of t */
    MFLD_DBL tvals[3] = {0.0, 0.0, 0.0}; /* Values of t */
    MFLD_DBL tlimit = scale_limit;
    MFLD_DBL t = scale, tn=0.0;
    
    if (tlimit<scale_limit*MFLD_GOLD) scale=0.99*tlimit/MFLD_GOLD;
    
    /* Compute the total force on the body, with constraint directions */
    mfrc = body_totalforce(obj, context, func, sel);
    
    if (mfrc) {
        /* Calculate the initial energy */
        energy[0]=body_evaluateenergy(obj, context, func, sel);
        
        /* Move vertices. */
        body_movevertices(obj, context, mfrc, t);
        energy[1]=body_evaluateenergy(obj, context, func, sel);
        tvals[1]=t;
        
        /* If e1<e0, successively increase the lengthscale until we get an increase */
        if (energy[1]<energy[0]) {
            for (tn=t*MFLD_GOLD; tn<tlimit; tn*=MFLD_GOLD) {
                body_movevertices(obj, context, mfrc, tn-t);
                energy[2]=body_evaluateenergy(obj, context, func, sel);
                tvals[2]=t=tn;
                
                if (energy[2]>energy[1]) {
                    break;
                } else {
                    /* If the function keeps going down, store this in the middle value */
                    energy[1]=energy[2];
                    tvals[1]=tvals[2];
                }
            }
            if (tn>tlimit) {
                body_movevertices(obj, context, mfrc, -t+tlimit);
                /* Linesearch exceeded limit */
                error_raise(context, GEOMETRY_LINESEARCHEXCEED, GEOMETRY_LINESEARCHEXCEED_MSG, ERROR_WARNING);
            }
        } else if (energy[1]>energy[0]) {
            /* Alternatively, successively decrease the lengthscale til we get a decrease*/
            energy[2]=energy[1]; tvals[2]=tvals[1]; /* Because now we're looking at the middle value */
            for (tn=t/MFLD_GOLD; tn>MACHINE_EPSILON; tn/=MFLD_GOLD) {
                body_movevertices(obj, context, mfrc, tn-t);
                energy[1]=body_evaluateenergy(obj, context, func, sel);
                tvals[1]=t=tn;
                
                if (energy[1]<energy[0]) {
                    break;
                } else {
                    /* If the function is still not small enough, store this in the end value */
                    energy[2]=energy[1];
                    tvals[2]=tvals[1];
                }
            }
            if (tn<=MACHINE_EPSILON) {
                body_movevertices(obj, context, mfrc, -t);
                /* Linesearch exceeded limit */
                error_raise(context, GEOMETRY_LINESEARCHSMALL, GEOMETRY_LINESEARCHSMALL_MSG, ERROR_WARNING);
            }
        } else {
            /* The energy is a constant. Abort. */
            body_movevertices(obj, context, mfrc, -t);
            error_raise(context, GEOMETRY_LINESEARCHCONST, GEOMETRY_LINESEARCHCONST_MSG, ERROR_WARNING);
        }
        
        /* Hopefully here we now have a bracketed minimum */
        if (energy[1]<energy[2] && energy[1]<energy[0]) {
            MFLD_DBL denom = (2*(energy[2]*(tvals[0] - tvals[1]) + energy[0]*(tvals[1] - tvals[2]) + energy[1]*(-tvals[0] + tvals[2])));
            
            if (fabs(denom)>MACHINE_EPSILON) {
                tn=(energy[2]*(tvals[0] - tvals[1])*(tvals[0] + tvals[1]) + energy[0]*(tvals[1] - tvals[2])*(tvals[1] + tvals[2]) +
                energy[1]*(-tvals[0]*tvals[0] + tvals[2]*tvals[2]))/denom;
                
                if (tn>tlimit) tn=tlimit;
                
                //printf("Using scale %g\n", tn);
                
                body_movevertices(obj, context, mfrc, tn-t);
            }
        }
    }
    
    /* Back projection of variables onto constraints. */
    body_projectontoconstraints(obj, context);
    
    /* Notify listeners */
    body_changed(obj, context);
    
    /* Free any temporarily allocated things */
    if (mfrc) {
        linkedlist_map(mfrc, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(mfrc);
    }
}

/* Relaxing fields */

/* Wrapper to call generalizedforce */
expression *body_computegeneralizedforce(body_object *obj, interpretcontext *context, expression_objectreference *functional, expression_objectreference *fobj, expression_objectreference *sel) {
    expression_objectreference *mobj=NULL;
    expression *exp = NULL;
    expression *args[3]={NULL, NULL, NULL};
    unsigned int nargs=0;
    field_object *fld=NULL;
    
    if (fobj) fld=(field_object *) fobj->obj;
    if (fld) mobj=fld->manifold;
    
    if (mobj) { args[nargs]=(expression *) mobj; nargs++; };
    if (fobj) { args[nargs]=(expression *) fobj; nargs++; };
    if (sel) { args[nargs]=(expression *) sel; nargs++; };
    
    if ((functional)&&(functional->obj)) {
        if (class_objectrespondstoselector(functional->obj, GEOMETRY_GENERALIZEDFORCE_SELECTOR)) {
            exp=class_callselectorwithargslist(context, functional->obj, GEOMETRY_GENERALIZEDFORCE_SELECTOR, nargs, args);
        }
    }
    
    return exp;
}

/* Computes the total force for a field by summing over all the energies */
expression *body_totalgeneralizedforceforfield(body_object *obj, interpretcontext *context, expression_objectreference *fref) {
    expression *force=NULL, *exp=NULL;
    
    if (!obj->functionalrefs) return NULL;
    
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        
        if (func->type==GEOMETRY_ENERGY) {
            exp=body_computegeneralizedforce(obj, context, func->ref, fref, func->sel);
            if (exp) force=body_functionalaccumulate(context, force, exp);
        }
    }
    
    return force;
}

expression *body_subtractlocalfieldconstraints(body_object *obj, interpretcontext *context, expression_objectreference *mref, expression *frc, expression_objectreference *omit);

linkedlist *body_totalgeneralizedforce(body_object *obj, interpretcontext *context, expression_objectreference *field, expression_objectreference *func, expression_objectreference *sel) {
    linkedlist *mfrc=linkedlist_new();
    
    expression *force=NULL;
    
    if ((!obj)||(!obj->fields)||(!mfrc)) return mfrc;
    
    /* Sequence */
    /* For each field... */
    if (obj->fields) for (linkedlistentry *e=obj->fields->first; e!=NULL; e=e->next) {
        expression_objectreference *fref = (expression_objectreference *) e->data;
        
        /* 1. Sum all conventional forces */
        if (func) {
            force = body_computegeneralizedforce(obj, context, func, fref, sel);
        } else {
            force = body_totalgeneralizedforceforfield(obj, context, fref);
        }
        
        /* 2. Set all forces on fixed field components to zero. */
        //body_forcefix(obj, context, mref, (expression_objectreference *) force);
        
        /* 3. Project forces along constraint directions. (see surface evolver manual 16.7.2) */
        /* TO DO: Selections could be used to narrow the scope a bit here.. */
        //force = body_subtractprojectedcomponentwithtype(obj, context, mref, force, GEOMETRY_CONSTRAINT);
        
        /* 4. Subtract local constraints */
        if (force) force = body_subtractlocalfieldconstraints(obj, context, fref, force, func);
        
        /* Retain the total force */
        linkedlist_addentry(mfrc, force);
    }
    
    return mfrc;
}

/* Subtracts local field constraints from the generalized force */
expression *body_subtractlocalfieldconstraints(body_object *obj, interpretcontext *context, expression_objectreference *fref, expression *frc, expression_objectreference *omit) {
    expression *force = frc, *exp=NULL;
    
    for (linkedlistentry *fe=obj->functionalrefs->first; fe!=NULL; fe=fe->next) {
        functionalref *func = (functionalref *) fe->data;
        expression *nforce = NULL;
        
        if (func->type==GEOMETRY_LOCALFIELDCONSTRAINT && !eval_objectrefeq(omit, func->ref)) {
            /* Compute the force associated with the constraint */
            exp=body_computegeneralizedforce(obj, context, func->ref, fref, func->sel);
            
            int os = functional_isonesided(func->ref, context);
            
            /* Subtract the projection of force onto exp. */
            nforce=field_localproject(context, (expression_objectreference *) force, (expression_objectreference *) exp, os);
            if (exp) freeexpression(exp);
            
            if (nforce) {
                if (force) freeexpression(force);
                force = nforce;
            }
        }
    }
    
    return force;
}

void body_updatefield(body_object *obj, interpretcontext *context, expression_objectreference *field, linkedlist *mfrc, MFLD_DBL scale) {
    if (!mfrc) return;
    linkedlistentry *f=mfrc->first;
    if (!f) return;
    
    expression_objectreference *dfref = (expression_objectreference *) f->data;
        
    if (field && dfref) field_accumulate((field_object *) eval_objectfromref(field), context, (field_object *) eval_objectfromref(dfref), scale);
}

/* relax -- simply take a step down the gradient. */
void body_relaxfield(body_object *obj, interpretcontext *context, expression_objectreference *field, expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale) {
    linkedlist *ffrc=NULL;
    
    /* Compute the total force on the body, with constraint directions */
    ffrc = body_totalgeneralizedforce(obj, context, field, func, sel);
    
    /* Update field values. */
    body_updatefield(obj, context, field, ffrc, scale);
    
    /* Back projection of field onto constraints. */
    
    /* Free any temporarily allocated things */
    if (ffrc) {
        linkedlist_map(ffrc, (linkedlistmapfunction *) &freeexpression);
        linkedlist_free(ffrc);
    }
}

/* linesearch -- perform a linesearch down the gradient direction of functional fref, or the total energy if
 no functional is supplied. */
void body_linesearchfield(body_object *obj, interpretcontext *context, expression_objectreference *field,  expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale, MFLD_DBL scalelimit) {
    linkedlist *mfrc=NULL;
    MFLD_DBL energy[3] = {0.0, 0.0, 0.0}; /* energies at different values of t */
    MFLD_DBL tvals[3] = {0.0, 0.0, 0.0}; /* Values of t */
    MFLD_DBL tlimit = scalelimit;
    MFLD_DBL t = scale, tn=0.0;
    
    if (tlimit<scalelimit*MFLD_GOLD) scale=0.99*tlimit/MFLD_GOLD;
    
    /* Compute the total generalized force on the body, with constraint directions */
    mfrc = body_totalgeneralizedforce(obj, context, field, func, sel);
    
    if (mfrc) {
        /* Calculate the initial energy */
        energy[0]=body_evaluateenergy(obj, context, func, sel);
        
        /* Move field. */
        body_updatefield(obj, context, field, mfrc, t);
        energy[1]=body_evaluateenergy(obj, context, func, sel);
        tvals[1]=t;
        
        /* If e1<e0, successively increase the lengthscale until we get an increase */
        if (energy[1]<energy[0]) {
            for (tn=t*MFLD_GOLD; tn<tlimit; tn*=MFLD_GOLD) {
                body_updatefield(obj, context, field, mfrc, tn-t);
                energy[2]=body_evaluateenergy(obj, context, func, sel);
                tvals[2]=t=tn;
                
                if (energy[2]>energy[1]) {
                    break;
                } else {
                    /* If the function keeps going down, store this in the middle value */
                    energy[1]=energy[2];
                    tvals[1]=tvals[2];
                }
            }
            if (tn>tlimit) {
                body_updatefield(obj, context, field, mfrc, -t+tlimit);
                /* Linesearch exceeded limit */
                error_raise(context, GEOMETRY_LINESEARCHEXCEED, GEOMETRY_LINESEARCHEXCEED_MSG, ERROR_WARNING);
            }
        } else if (energy[1]>energy[0]) {
            /* Alternatively, successively decrease the lengthscale til we get a decrease*/
            energy[2]=energy[1]; tvals[2]=tvals[1]; /* Because now we're looking at the middle value */
            for (tn=t/MFLD_GOLD; tn>MACHINE_EPSILON; tn/=MFLD_GOLD) {
                body_updatefield(obj, context, field, mfrc, tn-t);
                energy[1]=body_evaluateenergy(obj, context, func, sel);
                tvals[1]=t=tn;
                
                if (energy[1]<energy[0]) {
                    break;
                } else {
                    /* If the function is still not small enough, store this in the end value */
                    energy[2]=energy[1];
                    tvals[2]=tvals[1];
                }
            }
            if (tn<=MACHINE_EPSILON) {
                body_updatefield(obj, context, field, mfrc, -t);
                /* Linesearch exceeded limit */
                error_raise(context, GEOMETRY_LINESEARCHSMALL, GEOMETRY_LINESEARCHSMALL_MSG, ERROR_WARNING);
            }
        } else {
            /* The energy is a constant. Abort. */
            body_updatefield(obj, context, field, mfrc, -t);
            error_raise(context, GEOMETRY_LINESEARCHCONST, GEOMETRY_LINESEARCHCONST_MSG, ERROR_WARNING);
        }
        
        /* Hopefully here we now have a bracketed minimum */
        if (energy[1]<energy[2] && energy[1]<energy[0]) {
            MFLD_DBL denom = (2*(energy[2]*(tvals[0] - tvals[1]) + energy[0]*(tvals[1] - tvals[2]) + energy[1]*(-tvals[0] + tvals[2])));
            
            if (fabs(denom)>MACHINE_EPSILON) {
                tn=(energy[2]*(tvals[0] - tvals[1])*(tvals[0] + tvals[1]) + energy[0]*(tvals[1] - tvals[2])*(tvals[1] + tvals[2]) +
                    energy[1]*(-tvals[0]*tvals[0] + tvals[2]*tvals[2]))/denom;
                
                if (tn>tlimit) tn=tlimit;
            
                body_updatefield(obj, context, field, mfrc, tn-t);
            }
        }
    }
    
    /* Free any temporarily allocated things */
    if (mfrc) {
        linkedlist_map(mfrc, (linkedlistmapfunction *) &freeexpression);
        linkedlist_free(mfrc);
    }
}

/*
* New optimization algorithms
*/

/*
 * Approximate linesearch
 */
typedef double lnsrchfunc (void *);
typedef void lnsrchfuncd (double *, void *);

/* Approximate linesearch along a specified direction.
 * Input:
 *  func   - function to minimize. ref will be passed to func.
 *  fold   - old function value at x.
 *  nvars  - Number of variable lists
 *  nv     - List of number of variables per list
 *  x      - The initial point to go from, stored as a list of pointers to lists of doubles.
 *  g      - The gradient of the function at xold. (ordered same as x, but just in a single array).
 *  p      - A direction to move along                                - this is just a list of numbers
 *  s      - Scratch space same dimension as p.
 *  stpmax - Maximum step size to take
 *  ref    - A reference that will be passed to func
 * Output:
 *  x      - Updated
 *  fnew   - New value of function.
 * Returns:
 *  TRUE if successful, FALSE if x is too close to xold (usually signals convergence).
 */
int linesearchapprox(interpretcontext *context, lnsrchfunc *func, double fold, unsigned int n, unsigned int *nv, double **x, double *g, double *p, double *s, double stpmax, void *ref, double *fnew) {
    const double alf = 1e-4; // Ensures sufficient decrease in function value
    const double tolx = 1e-8; // Convergence criterion on delta x
    unsigned int ndof = 0;
    unsigned int i,j,k;
    int check=TRUE;
    double sum=0.0, slope=0.0, temp=0.0, test=0.0;
    double alamin=0.0, alam=0.0, alam2=0.0; // lambda and lambdamin
    double f=0.0, f2=0.0;
    double rhs1=0.0, rhs2=0.0, a=0.0, b=0.0, tmplam=0.0, disc=0.0;
    
    // Calculate number of degrees of freedom
    for (i=0; i<n; i++) {
        ndof += nv[i];
    }
    if (ndof==0) return FALSE;
    
    // Calculate |p|
    for (i=0; i<ndof; i++) sum+=p[i]*p[i];
    sum=sqrt(sum);
    
    // Scale p if too large.
    if (sum>stpmax) {
        for (i=0; i<ndof; i++) p[i]*= stpmax/sum;
    }
    
    // Calculate g . p and store x in s.
    for (i=0, k=0; i<n; i++) {
        for (j=0; j<nv[i]; j++,k++) {
            slope+=g[k]*p[k];
            s[k]=x[i][j];
        }
    }
    if (slope >= 0.0) {
        error_raise(context, 0, "Linesearch not provided with a descent direction", ERROR_WARNING);
    };
    
    // Calculate lambdamin
    // Identify the variable for which p/max(xold,1) is largest - i.e. the largest direction.
    test=0.0;
    for (i=0, k=0; i<n; i++) {
        for (j=0; j<nv[i]; j++,k++) {
            temp=fabs(p[k])/fmax(fabs(x[i][j]), 1.0);
            if (temp>test) test = temp;
        }
    }
    alamin = tolx/test;
    alam=1.0;
    
    // Start of iteration loop
    for (;;) {
        // Choose a position that is xold + lambda * p
        for (i=0, k=0; i<n; i++) {
            for (j=0; j<nv[i]; j++,k++) {
                x[i][j] = s[k] + alam*p[k];
            }
        }
        // Now evaluate the function
        f=(*func) (ref);
        
        // Is lambda < lambda_min? This usually means the function has converged...
        if (alam<alamin) {
            // Copy the solution back into x.
            for (i=0, k=0; i<n; i++) {
                for (j=0; j<nv[i]; j++,k++) {
                    x[i][j] = s[k];
                }
            }
            check=FALSE;
            break;
        } else if (f<=fold+alf*alam*slope) {
            // Function has decreased enough
            break;
        } else {
            // Backtrack
            if (alam == 1.0)
                // Called on first time
                tmplam = -slope/(2.0*(f-fold-slope));
            else {
                rhs1=f-fold-alam*slope;
                rhs2=f2-fold-alam2*slope;
            
                a=(rhs1/(alam*alam)-rhs2/(alam2*alam2))/(alam-alam2);
                b=(-alam2*rhs1/(alam*alam)+alam*rhs2/(alam2*alam2))/(alam-alam2);
                if (a==0.0) {
                    tmplam = -slope/(2.0*b);
                } else {
                    disc=b*b-3.0*a*slope;
                    if (disc < 0.0) tmplam=0.5*alam;
                    else if (b <= 0.0) tmplam=(-b+sqrt(disc))/(3.0*a);
                    else tmplam=-slope/(b+sqrt(disc));
                }
                if (tmplam>0.5*alam)
                tmplam=0.5*alam;
            }
            
            alam2=alam; // Save old lambda
            f2 = f;
            alam=fmax(tmplam,0.1*alam); // lambda > 0.1 lambda1
        }
    }
    if (fnew) *fnew = f;
    
    return check;
}

/*
 * Exact linesearch using Brent's method
 */

int linesearchexact(interpretcontext *context, lnsrchfunc *func, double fold, unsigned int n, unsigned int *nv, double **x, double **g, double *p, double *s, double stpmax, void *ref, double *fnew) {
    
    return TRUE;
}

double tstfunc(double *x) {
    //printf("  {%g, %g}\n", x[0],x[1]);
    return 0.1+(x[0]-2.0)*(x[0]-2.0)+0.04*(x[1]-3.0)*(x[1]-3.0);
}

void tstfuncd(double *g, double *x) {
    g[0] = 2.0*(-2.0 + x[0]);
    g[1] = 2.0*0.04*(-3.0 + x[1]);
}

void tstlnsrch(void) {
    double x[2] = {5.0, 1.0};
    double g[2] = {0, 0};
    double *xp[2] = {x, x+1};
    double p[2] = {1.0, 1.0}, q[2];
    unsigned int nv[2] = { 1, 1 };
    double f = tstfunc(x),fold=f;
    
    for (unsigned int k=0; k<100; k++) {
        tstfuncd(g, x);
        for (unsigned int i=0; i<2; i++) p[i]=-g[i];
        linesearchapprox(NULL, (lnsrchfunc *) tstfunc, f, 2, nv, xp, g, p, q, 10.0, &x, &f);
        printf("%u: %g {%g %g}\n", k, f, x[0], x[1]);
        if (fabs(fold-f)<3e-8*fabs(f)) break;
        fold=f;
    }
    
}

/*
 * Multidimensional minimization with the Fletcher-Reeves-Polak-Ribiere method
 */

void frpr(interpretcontext *context, lnsrchfunc *func, lnsrchfuncd *dfunc, unsigned int n, unsigned int *nv, double **x, void *ref) {
    int maxiter = 200; // Maximum number of iterations
    double eps = 1e-18; // Small number to alleviate case when minimum lies at 0
    double gtol = 1e-8; // Convergence criterion for zero gradient test
    double ftol = 1e-8; // Convergence criterion for change in function test
    double fp=0.0,fret=0.0, temp=0.0, test=0.0, den=0.0, gg, dgg, gam;
    unsigned int ndof=0, i, j, k;
    
    // Calculate number of degrees of freedom
    for (i=0; i<n; i++) {
        ndof += nv[i];
    }
    if (ndof==0) return;
    
    double xi[ndof], g[ndof], h[ndof], s[ndof]; // Should allocate
    
    // Compute function, store in fp
    fp = func(ref);
    // Compute derivative, store in xi
    dfunc(xi, ref);
    
    // Initialize g,h
    for (i=0; i<ndof; i++) {
        g[i] = -xi[i];
        h[i] = g[i];
    }
    
     printf("%u: %g {%g %g}, {%g %g}, {%g %g}\n", 0, fp, x[0][0], x[1][0], g[0], g[1], h[0], h[1]);
    
    /* Iterations */
    for (unsigned int iter=0; iter<maxiter; iter++) {
        // Linesearch along the direction h
        linesearchapprox(context, func, fp, n, nv, x, xi, h, s, 1.0, ref, &fret);
        
        // Converge if the change in function value is less than ftol times the average value.
        // eps is added so that if the extremum happens to be at zero we can still converge.
        if (2*fabs(fret-fp)<= ftol*(fabs(fret)+fabs(fp)+eps)) {
            return;
        }
        
        fp=fret;
        // Recalculate df
        dfunc(xi, ref);
        
        // Now find the largest component of the gradient
        den = 1.0/fmax(fp, 1.0);
        for (i=0, k=0; i<n; i++) {
            for (j=0; j<nv[i]; j++,k++) {
                temp = fabs(xi[k])*fmax(fabs(x[i][j]), 1.0)*den;
                if (temp>test) test = temp;
            }
        }
        if (test < gtol) return;
        
        // Compute g.g
        // dg = (df - g).df
        dgg=gg=0.0;
        for (k=0; k<ndof; k++) {
            gg+= g[k]*g[k];
            dgg+=(xi[k]+0.0*g[k])*(xi[k]); // Fletcher-Reeves
        }
        if (gg==0.0) return;
        gam=dgg/gg;
        
        // Update g, h
        for (k=0; k<ndof; k++) {
            g[k] = -xi[k];
            h[k] = g[k] + gam*h[k];
        }
        printf("%u: %g {%g %g}\n", iter, fp, x[0][0], x[1][0]);
    }
    
}

void tstfrpr(void) {
    /*
    double x[2] = {5.0, 1.0};
    double g[2] = {0, 0};
    double *xp[2] = {x, x+1};
    double *xpg[2] = {g, g+1};
    double p[2] = {1.0, 1.0}, q[2]={0.0,0.0}, s[2];
    unsigned int nv[2] = { 1, 1 };
    double f = tstfunc(x);
    
    frpr(NULL, (lnsrchfunc *) tstfunc, tstfuncd, 2, nv, xp, &x);
    */
}


/*
 * Selectors
 */

/* Initializes the body */
expression *body_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    bobj->manifolds=NULL;
    bobj->functionalrefs=NULL;
    bobj->fields=NULL;
    
    return NULL;
}

/* Frees the body and attached expressions */
expression *body_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    if (bobj) {
        if (bobj->manifolds) {
            linkedlist_map(bobj->manifolds, (linkedlistmapfunction *) freeexpression);
            linkedlist_free(bobj->manifolds);
        }
        if (bobj->functionalrefs) {
            linkedlist_map(bobj->functionalrefs, (linkedlistmapfunction *) body_freefunctionalref);
            linkedlist_free(bobj->functionalrefs);
        }
    }
    
    return NULL;
}

/* Draws the body */
expression *body_drawi(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* TODO: Currently only draws one manifold.
       Additionally manifold_draw has hold enabled and we need to account for this! */
    
    body_object *bobj = (body_object *) obj;
    expression *g=NULL;
    
    if (bobj->manifolds) {
        for (linkedlistentry *e=bobj->manifolds->first; e!=NULL; e=e->next) {
            expression_objectreference *mref = (expression_objectreference *) e->data;
            
            if (eval_isobjectref(mref) && class_objectrespondstoselector(mref->obj, MANIFOLD_DRAW)) {
                if (g) freeexpression(g);
                
                /* Call the draw class with anything we were passed */
                g=class_callselectorwithargslist(context, mref->obj, MANIFOLD_DRAW, nargs, args);
            }
        }
    }
    
    return g;
}

/* Refines the body */
expression *body_refinei(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    expression *g=NULL;
    
    if (bobj->manifolds) {
        for (linkedlistentry *e=bobj->manifolds->first; e!=NULL; e=e->next) {
            expression_objectreference *mref = (expression_objectreference *) e->data;
            
            if (class_objectrespondstoselector(mref->obj, MANIFOLD_REFINE)) {
                if (g) freeexpression(g);
                
                g=class_callselectorwithargslist(context, mref->obj, MANIFOLD_REFINE, nargs, args);
                
                if (eval_islist(g)) {
                    /* Notify listeners */
                    body_changed(bobj, context);
                    
                    freeexpression((expression *) g);
                    //expressionprint(g);
                }
            }
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

typedef struct {
    interpretcontext *context;
    manifold_object *mobj;
    graphics3d_object *gobj;
    float scale;
} body_visualizeforcesmapfunctionref;

void body_visualizeforcesmapfunction(uid id, void *data, void *r) {
    body_visualizeforcesmapfunctionref *ref = (body_visualizeforcesmapfunctionref *) r;
    expression_objectreference *fobj = (expression_objectreference *) data;
    if (!eval_isobjectref(fobj)) return;
    
    multivectorobject *mvobj =(multivectorobject *) fobj->obj;
    if (!mvobj) return;
    
    double xx[ref->mobj->dimension], ff[mvobj->dimension];
    float x1[3], x2[3];
    
    manifold_getvertexposition(ref->mobj, ref->context, id, xx);
    multivector_gradetodoublelist(mvobj, MANIFOLD_GRADE_LINE, ff);
    
    for (unsigned int i=0; i<3; i++) {
        x1[i]=xx[i];
        x2[i]=xx[i]+ref->scale * ff[i];
    }
    
    graphics3d_arrow(ref->gobj, x1, x2, 0.2, 10, NULL);
}

/* Visualizes forces acting on the body */
expression *body_visualizeforcesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    interpretcontext *options=NULL;
    body_object *bobj = (body_object *) obj;
    expression_objectreference *fref=NULL;
    expression_objectreference *sel=NULL;
    expression *g=NULL, *f=NULL, *sc;
    field_object *fld;
    graphics3d_object *gobj=NULL;
    body_visualizeforcesmapfunctionref ref;
    expression *exp=interpret(args[0]);
    int optionstart = 0;
    
    /* DEPRECATED */
    error_raise(context, ERROR_DEPRECATED, ERROR_DEPRECATED_MSG, ERROR_WARNING);
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    for (unsigned int i=0; (i<nargs) && (i<optionstart-1); i++) {
        expression_objectreference *exp = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(exp)) {
            if (exp->obj->clss != class_lookup(GEOMETRY_SELECTIONCLASS)) {
                fref = exp;
            } else {
                sel = exp;
            }
        } else {
            freeexpression((expression *) exp); exp=NULL;
        }
    }

    /* Process options */ 
    options=class_options(obj,context,optionstart,nargs,args);
    ref.scale=1.0;
    sc=lookupsymbol(options, GEOMETRY_SCALE_OPTION);
    if ((sc)&&(eval_isreal(sc))) ref.scale=eval_floatvalue(sc);
    if (sc) freeexpression(sc); 
    
    if (bobj && bobj->manifolds) {
        for (linkedlistentry *e=bobj->manifolds->first; e!=NULL; e=e->next) {
            expression_objectreference *mref = (expression_objectreference *) e->data;
            
            if (eval_isobjectref(mref) && class_objectrespondstoselector(mref->obj, MANIFOLD_DRAW)) {
                g=class_callselector(context, mref->obj, MANIFOLD_DRAW, 0);
                
                if ((g)&&(eval_isobjectref(g))) gobj=(graphics3d_object *) ((expression_objectreference *) g)->obj;

                ref.gobj=gobj;
                ref.mobj=(manifold_object *) mref->obj;
                ref.context=context;
                
                /* Compute the forcefield on this manifold */
                f=body_functionalcompute((body_object *) obj, context, GEOMETRY_FORCE_SELECTOR, fref, mref, sel);
                
                if (f) {
                    fld=(field_object *) (((expression_objectreference *) f)->obj);
                    if (fld) idtable_map(fld->entries, body_visualizeforcesmapfunction, &ref);
                }
                
                if (f) freeexpression(f);
            }
        }
    }
    
    if (options) freeinterpretcontext(options);
    if (exp) freeexpression(exp);
    if (fref) freeexpression((expression *) fref);
    if (sel) freeexpression((expression *) sel);
    
    return g;
}


/* Selects a subcomponent of the body */
expression *body_selecti(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    interpretcontext *options=NULL;
    int optionstart = 0;
    expression *exp=NULL;
    expression *tryexp=NULL;
    expression *selection=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart( context, nargs, args);
    
    /* If a non-options argument is present, it is something to be evaluated */
    if ((nargs>=1) && (optionstart>1)) exp=args[0];
    /* Try interpreting it and see if we get a field */
    if (exp) tryexp=interpretexpression(context, exp);
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Perform the selection */
    if (eval_isobjectref(tryexp) && class_objectisofclass(tryexp, GEOMETRY_FIELDCLASS)) {
        selection = selection_selectwithfield(bobj, context, (field_object *) eval_objectfromref(tryexp));
    } else {
        selection = selection_select(bobj, context, exp, options);
    }
    
    if (tryexp) freeexpression(tryexp);
    if (options) freeinterpretcontext(options);
    
    return selection;
}

/* Selects everything */
expression *body_selectalli(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    interpretcontext *options=NULL;
    int optionstart = 0;
    
    expression *selection=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart( context, nargs, args);
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Perform the selection */
    selection = selection_selectall(bobj, 0, NULL);

    if (options) freeinterpretcontext(options);
    
    return selection;
}

/* Adds an energy to the body */
expression *body_addenergyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        expression_objectreference *sref = NULL;
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(sref) && sref->obj && sref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) sref->obj;
        
        body_addfunctional((body_object *) obj, (functional_object *) oref->obj, GEOMETRY_ENERGY, sel);
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;
}

/* Adds a global constraint to the body */
expression *body_addconstrainti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        expression_objectreference *sref = NULL;
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(sref) && sref->obj && sref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) sref->obj;
        
        body_addfunctional((body_object *) obj, (functional_object *) oref->obj, GEOMETRY_CONSTRAINT, sel);
        
        /* If the functional enables setting a target value, evaluate the current value and set it as the target. */
        if (class_objectrespondstoselector(oref->obj, GEOMETRY_SETTARGET_SELECTOR)) {
            double val=(double) body_evaluateenergy((body_object *) obj, context, oref, (sel ? sref : NULL));
            expression *tmp=NULL;
            
            tmp=class_callselector(context, oref->obj, GEOMETRY_SETTARGET_SELECTOR, 1, EXPRESSION_FLOAT, val);
            if (tmp) freeexpression(tmp);
        }
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;

}

/* Adds a local constraint to the body */
expression *body_addlocalconstrainti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        expression_objectreference *sref = NULL;
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(sref) && sref->obj && sref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) sref->obj;
        
        body_addfunctional((body_object *) obj, (functional_object *) oref->obj, GEOMETRY_LOCALCONSTRAINT, sel);
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;
    
}

/* Adds a local field constraint to the body */
expression *body_addlocalfieldconstrainti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        expression_objectreference *sref = NULL;
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(sref) && sref->obj && sref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) sref->obj;
        
        body_addfunctional((body_object *) obj, (functional_object *) oref->obj, GEOMETRY_LOCALFIELDCONSTRAINT, sel);
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;
    
}


/* Adds a regularization functional to the body */
expression *body_addregularizationi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    selection_object *sel=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        expression_objectreference *sref = NULL;
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(sref) && sref->obj && sref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(selection_object *) sref->obj;
        
        body_addfunctional((body_object *) obj, (functional_object *) oref->obj, GEOMETRY_REGULARIZATION, sel);
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;
}

/* Gets a reference to the i'th manifold */
expression *body_manifoldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *body = (body_object *) obj;
    int which=0;
    expression *ret=NULL;
    
    if ((nargs==1)&&(eval_isinteger(args[0]))) {
        which = eval_integervalue(args[0]);
        
        if (body->manifolds) {
            if (which>0 && which<=linkedlist_length(body->manifolds)) {
                linkedlistentry *l = linkedlist_element(body->manifolds, which-1);
                if (l) ret = cloneexpression((expression *) l->data);
            };
        }
    }
        
    return ret;
}

/* Calculates the total energy */
expression *body_totalenergyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    return body_totalenergy((body_object *) obj, context);
}

/* Calculates the total force on the body */
expression *body_totalforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = body_totalforce((body_object *) obj, context, NULL, NULL);
    
    return (expression *) newexpressionlist(list);
}

/* Calculates the total generalized force on a field */
expression *body_totalgeneralizedforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = body_totalgeneralizedforce((body_object *) obj, context, NULL, NULL, NULL);
    
    return (expression *) newexpressionlist(list);
}


/* Relaxes the body */
expression *body_relaxi(object *obj, interpretcontext *context, int nargs, expression **args) {
    interpretcontext *options=NULL;
    int optionstart = 0;

    expression *exp=NULL;
    double scale=1.0;
    
    expression_objectreference *funcref=NULL;
    expression_objectreference *sel=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* If a non-options argument is present, see if it is a reference to a functional */
    for (unsigned int i=0; (i<nargs) && (i<optionstart-1); i++) {
        expression_objectreference *exp = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(exp)) {
            if (exp->obj->clss!=class_lookup(GEOMETRY_SELECTIONCLASS)) {
                funcref = exp;
            } else {
                sel = exp;
            }
        } else freeexpression((expression *) exp);
    }
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Look for a scale */
    if (options) exp=lookupsymbol(options, GEOMETRY_SCALE_OPTION);
    if (eval_isreal(exp)) scale=eval_floatvalue(exp);
    if (exp) freeexpression(exp);
    
    if (obj) body_relax((body_object *) obj, context, funcref, sel, scale);
    
    /* Cleanup */
    if (funcref) freeexpression((expression *) funcref);
    if (sel) freeexpression((expression *) sel);
    if (options) freeinterpretcontext(options);

    return (expression *) class_newobjectreference(obj);
}

/* Relaxes the body */
expression *body_relaxfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    interpretcontext *options=NULL;
    int optionstart = 0;
    
    expression *exp=NULL;
    double scale=1.0;
    
    expression_objectreference *funcref=NULL;
    expression_objectreference *sel=NULL;
    expression_objectreference *field=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* If a non-options argument is present, see if it is a reference to a functional */
    for (unsigned int i=0; (i<nargs) && (i<optionstart-1); i++) {
        expression_objectreference *exp = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(exp)) {
            if (exp->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) {
                sel = exp;
            } else if (exp->obj->clss==class_lookup(GEOMETRY_FIELDCLASS)) {
                field = exp;
            } else {
                funcref = exp;
            } 
        } else freeexpression((expression *) exp);
    }
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Look for a scale */
    if (options) exp=lookupsymbol(options, GEOMETRY_SCALE_OPTION);
    if (eval_isreal(exp)) scale=eval_floatvalue(exp);
    if (exp) freeexpression(exp);
    
    if (obj && field) body_relaxfield((body_object *) obj, context, field, funcref, sel, scale);
    
    if (!field) {
        error_raise(context, GEOMETRY_NOFIELD, GEOMETRY_NOFIELD_MSG, ERROR_FATAL);
    }
    
    /* Cleanup */
    if (funcref) freeexpression((expression *) funcref);
    if (sel) freeexpression((expression *) sel);
    if (field) freeexpression((expression *) field);
    if (options) freeinterpretcontext(options);
    
    return (expression *) class_newobjectreference(obj);
}

expression *body_linesearchi(object *obj, interpretcontext *context, int nargs, expression **args) {
    interpretcontext *options=NULL;
    int optionstart = 0;
    expression *exp=NULL;
    expression *tryexp=NULL;
    double scale=1.0;
    double scalelimit=10.0;

    expression_objectreference *func=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* If a non-options argument is present, it is something to be evaluated */
    if ((nargs>=1) && (optionstart>1)) exp=args[0];
    /* Try interpreting it and see if we get a field */
    if (exp) tryexp=interpretexpression(context, exp);
    if (eval_isobjectref(tryexp)) func=(expression_objectreference *) tryexp;
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Look for a scale */
    if (options) {
        exp=lookupsymbol(options, GEOMETRY_SCALE_OPTION);
        if (eval_isreal(exp)) scale=eval_floatvalue(exp);
        if (exp) freeexpression(exp);
        exp=lookupsymbol(options, GEOMETRY_SCALELIMIT_OPTION);
        if (eval_isreal(exp)) scalelimit=eval_floatvalue(exp);
        if (exp) freeexpression(exp);
    }
    
    if (obj) body_linesearch((body_object *) obj, context, func, NULL, scale, scalelimit);
    
    if (options) freeinterpretcontext(options);
    if (tryexp) freeexpression(tryexp);

    return (expression *) class_newobjectreference(obj);
}

expression *body_linesearchfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    interpretcontext *options=NULL;
    int optionstart = 0;
    MFLD_DBL scale=1.0, scalelimit=10.0;
    expression *exp=NULL;
    
    expression_objectreference *funcref=NULL;
    expression_objectreference *sel=NULL;
    expression_objectreference *field=NULL;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* If a non-options argument is present, see if it is a reference to a functional */
    for (unsigned int i=0; (i<nargs) && (i<optionstart-1); i++) {
        expression_objectreference *exp = (expression_objectreference *) interpretexpression(context, args[i]);
        int used=FALSE;
        
        if (eval_isobjectref(exp)) {
            if (exp->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) {
                sel = exp; used=TRUE;
            } else if (exp->obj->clss==class_lookup(GEOMETRY_FIELDCLASS)) {
                field = exp; used=TRUE;
            } else {
                funcref = exp; used=TRUE;
            }
        }
        
        if (!used) freeexpression((expression *) exp);
    }
    
    /* Options were found */
    if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
    
    /* Look for a scale */
    if (options) {
        exp=lookupsymbol(options, GEOMETRY_SCALE_OPTION);
        if (eval_isreal(exp)) scale=eval_floatvalue(exp);
        if (exp) freeexpression(exp);
        
        exp=lookupsymbol(options, GEOMETRY_SCALELIMIT_OPTION);
        if (eval_isreal(exp)) scalelimit=eval_floatvalue(exp);
        if (exp) freeexpression(exp);
    }
    
    if (!field) {
        error_raise(context, GEOMETRY_NOFIELD, GEOMETRY_NOFIELD_MSG, ERROR_FATAL);
    }
    
    if (obj && field) body_linesearchfield((body_object *) obj, context, field, funcref, sel, scale, scalelimit);
    
    if (options) freeinterpretcontext(options);
    
    if (funcref) freeexpression((expression *) funcref);
    if (sel) freeexpression((expression *) sel);
    if (field) freeexpression((expression *) field);
    
    return (expression *) class_newobjectreference(obj);
}

/* Evaluates a desired functional */
expression *body_evaluatefunctionali(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    expression_objectreference *sref = NULL;
    
    if (nargs>0) {
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(args[0])) {
            ret=body_functionaltotal((body_object *) obj, context, GEOMETRY_TOTAL_SELECTOR, (expression_objectreference *) args[0], sref);
        }
    }
    
    return ret;
}

/* Evaluates a desired functional */
expression *body_evaluateforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    expression_objectreference *sref = NULL;
    
    if (nargs>0) {
        if (nargs>1) sref = (expression_objectreference *) args[1];
        
        if (eval_isobjectref(args[0])) {
            ret=body_functionaltotal((body_object *) obj, context, GEOMETRY_FORCE_SELECTOR, (expression_objectreference *) args[0], sref);
        }
    }
    
    return ret;
}


/* Displays all currently active functionals */
expression *body_showfunctionalsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    if ((bobj->functionalrefs)&&(bobj->functionalrefs->first)) {
        /* Loop over functionals */
        for (linkedlistentry *e=bobj->functionalrefs->first; e!=NULL; e=e->next) {
            functionalref *f = (functionalref *) e->data;
            
            switch (f->type) {
                case GEOMETRY_ENERGY:
                    printf("%s ", GEOMETRY_ENERGY_LABEL);
                    break;
                case GEOMETRY_CONSTRAINT:
                    printf("%s ", GEOMETRY_CONSTRAINT_LABEL);
                    break;
                case GEOMETRY_FIELDCONSTRAINT:
                    printf("%s ", GEOMETRY_FIELDCONSTRAINT_LABEL);
                    break;
                case GEOMETRY_LOCALCONSTRAINT:
                    printf("%s ", GEOMETRY_LOCALCONSTRAINT_LABEL);
                    break;
                case GEOMETRY_LOCALFIELDCONSTRAINT:
                    printf("%s ", GEOMETRY_LOCALFIELDCONSTRAINT_LABEL);
                    break;
                case GEOMETRY_REGULARIZATION:
                    printf("%s ", GEOMETRY_REGULARIZATION_LABEL);
                    break;
            }
            
            if (f->ref && f->ref->obj && f->ref->obj && f->ref->obj->clss && f->ref->obj->clss->name) printf("<%s>", f->ref->obj->clss->name);
            
            if (f->sel) printf(" on selection ");
            
            printf("\n");
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *body_removefunctionali(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    
    if (nargs!=1) return NULL;
    if ((!args[0])||(!eval_isobjectref(args[0]))) return NULL;
    
    body_removefunctional(bobj, (functional_object *) ((expression_objectreference *) args[0])->obj);
    
    return (expression *) class_newobjectreference(obj);
}

expression *body_newfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    manifold_object *mobj=NULL;
    expression *ret=NULL, *exp=NULL, *coordinates=NULL;
    expression_objectreference *mref=NULL;

    interpretcontext *options=NULL;
    int optionstart = 0;
    
    /* TODO: Only uses the first manifold */
    if ((bobj->manifolds) && (bobj->manifolds->first)) {
        mref = (expression_objectreference *) bobj->manifolds->first->data;
        if (mref) mobj=(manifold_object *) mref->obj;
    }

    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    if (optionstart>1) {
        exp=args[0];
        
        /* Options were found */
        if (optionstart<=nargs) options = class_options(obj, context, optionstart, nargs, args);
        
        /* Get coordinate expression */
        coordinates = lookupsymbol(options, GEOMETRY_SELECTION_COORDS);
        if (coordinates) {
            if (!eval_islist(coordinates)) error_raise(context, ERROR_SELECTIONCOORDS, ERROR_SELECTIONCOORDS_MSG, ERROR_FATAL);
        } else {
            error_raise(context, ERROR_COORDSREQD, ERROR_COORDSREQD_MSG, ERROR_FATAL);
        }
        
        ret = field_newwithexpression(mobj, context, exp, coordinates);
        
        if (eval_isobjectref(ret)) {
            body_addfield(bobj, (field_object *) ((expression_objectreference *) ret)->obj );
        }
    }
    
    if (options) freeinterpretcontext(options);
    if (coordinates) freeexpression(coordinates);

    return ret;
}

/* Adds a field to the body */
expression *body_addfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        expression_objectreference *oref = (expression_objectreference *) args[0];
        
        if (eval_isobjectref(oref)) {
            body_addfield((body_object *) obj, (field_object *) eval_objectfromref(oref));
        }
        
        ret=(expression *) class_newobjectreference(obj);
    } else {
        error_raise(context, GEOMETRY_NEEDSOBJECT, GEOMETRY_NEEDSOBJECT_MSG, ERROR_FATAL);
    }
    
    return ret;
}

/* The body calls this selector whenever it has changed. */
expression *body_changedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    
    return (expression *) class_newobjectreference(obj);
}

/* Clones a body and all attached objects */
expression *body_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    body_object *bobj = (body_object *) obj;
    body_object *bnew = NULL;
    expression_objectreference *new=NULL;
    
    if (bobj) {
        new=(expression_objectreference *) body_new();
        
        if (eval_isobjectref(new)) {
            bnew=(body_object *) eval_objectfromref(new);

        }
    }
    
    return (expression *) class_newobjectreference((object *) new);
}

/*
 * Initialization
 */

void bodyinitialize(void) {
    classintrinsic *cls;
    
    //tstlnsrch();
    
    //tstfrpr();
    
    cls=class_classintrinsic(GEOMETRY_BODYCLASS, class_lookup(EVAL_OBJECT), sizeof(body_object), 20);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, body_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, body_freei);
    //class_registerselector(cls, EVAL_CLONE, FALSE, body_clonei);
    
    class_registerselector(cls, GEOMETRY_DRAW, TRUE, body_drawi);
    class_registerselector(cls, GEOMETRY_REFINE_SELECTOR, FALSE, body_refinei);
    class_registerselector(cls, GEOMETRY_VISUALIZEFORCES_SELECTOR, TRUE, body_visualizeforcesi); /* Note HOLD on arguments */
    class_registerselector(cls, GEOMETRY_SELECT_SELECTOR, TRUE, body_selecti); /* Note HOLD on arguments */
    class_registerselector(cls, GEOMETRY_SELECTALL_SELECTOR, TRUE, body_selectalli); /* Note HOLD on arguments */
    class_registerselector(cls, GEOMETRY_MANIFOLD_SELECTOR, FALSE, body_manifoldi);
    
    class_registerselector(cls, EVAL_INDEX, FALSE, body_manifoldi); /* indexing manifolds */
    
    class_registerselector(cls, GEOMETRY_NEWFIELD_SELECTOR, TRUE, body_newfieldi);
    class_registerselector(cls, GEOMETRY_ADDFIELD_SELECTOR, FALSE, body_addfieldi);
    
    class_registerselector(cls, GEOMETRY_ADDENERGY_SELECTOR, FALSE, body_addenergyi);
    class_registerselector(cls, GEOMETRY_ADDCONSTRAINT_SELECTOR, FALSE, body_addconstrainti);
    class_registerselector(cls, GEOMETRY_ADDLOCALCONSTRAINT_SELECTOR, FALSE, body_addlocalconstrainti);
    class_registerselector(cls, GEOMETRY_ADDLOCALFIELDCONSTRAINT_SELECTOR, FALSE, body_addlocalfieldconstrainti);
    class_registerselector(cls, GEOMETRY_ADDREGULARIZATION_SELECTOR, FALSE, body_addregularizationi);
    class_registerselector(cls, GEOMETRY_REMOVEFUNCTIONAL_SELECTOR, FALSE, body_removefunctionali);
    
    class_registerselector(cls, GEOMETRY_EVALUATEFUNCTIONAL_SELECTOR, FALSE, body_evaluatefunctionali);
    class_registerselector(cls, GEOMETRY_EVALUATEFORCE_SELECTOR, FALSE, body_evaluateforcei);
    
    class_registerselector(cls, GEOMETRY_TOTALENERGY_SELECTOR, FALSE, body_totalenergyi);
    class_registerselector(cls, GEOMETRY_TOTALFORCE_SELECTOR, FALSE, body_totalforcei);
    class_registerselector(cls, GEOMETRY_TOTALGENERALIZEDFORCE_SELECTOR, FALSE, body_totalgeneralizedforcei);
    class_registerselector(cls, GEOMETRY_SHOW_FUNCTIONALS, FALSE, body_showfunctionalsi);
    class_registerselector(cls, GEOMETRY_RELAX_SELECTOR, TRUE, body_relaxi);
    class_registerselector(cls, GEOMETRY_RELAXFIELD_SELECTOR, TRUE, body_relaxfieldi);
    class_registerselector(cls, GEOMETRY_LINESEARCH_SELECTOR, TRUE, body_linesearchi);
    class_registerselector(cls, GEOMETRY_LINESEARCHFIELD_SELECTOR, TRUE, body_linesearchfieldi);
    
    class_registerselector(cls, GEOMETRY_CHANGED_SELECTOR, FALSE, body_changedi);
}
