/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "eval.h"
#include "geometry.h"
#include "graphics3d.h"

#ifndef Morpho_manifold_h
#define Morpho_manifold_h

/* 
 * manifold - a replacement for mesh.
 */

/* Labels associated with the manifold class */
#define MANIFOLD_LABEL "manifold"

/*
 * The manifold class is designed to be precision independent; hence several macros are defined as 
 * veneers onto morpho functions, or replacements if need be.
 */
typedef double MFLD_DBL;

#define manifold_listtodoublearray(a, b, c) eval_listtodoublearray(a, b, c)
#define manifold_listfromdoublearray(a, b) eval_listfromdoublearray(a, b)

/* Vertex */

#define MANIFOLD_INITIALSIZE 100

typedef struct manifold_vertex_s {
    MFLD_DBL *x;
    
    unsigned int flags;
} manifold_vertex;

#define MANIFOLD_VERTEX_NONE  0
#define MANIFOLD_VERTEX_FIXED 1

typedef struct manifold_object_s {
    CLASS_GENERICOBJECTDATA
    
    /* Geometric properties of the manifold */
    unsigned int dimension;
    unsigned int grade; /* Highest grade present */
    
    idtable *vertices;
    
    idtable **gradelower;
    idtable **graderaise;
    
    unsigned int flags;
} manifold_object;

typedef struct {
    uid el[2]; // First element is grade - 1, second is of grade 1. 
} manifold_gradelowerentry;

typedef struct {
    linkedlist *list;
} manifold_graderaiseentry;

/*
 * Errors 
 */

#define ERROR_MANIFOLD_ALLOCATIONFAILED         0x10000
#define ERROR_MANIFOLD_ALLOCATIONFAILED_MSG     "Allocation failed."

#define ERROR_MANIFOLD_INCSTDIMENSION           0x10001
#define ERROR_MANIFOLD_INCSTDIMENSION_MSG       "Vertex dimension inconsistent with manifold."

#define ERROR_MANIFOLD_INVALIDVERTEXID          0x10002
#define ERROR_MANIFOLD_INVALIDVERTEXID_MSG      "Invalid vertex ID."

#define ERROR_MANIFOLD_INSUFFICIENTGRADE        0x10003
#define ERROR_MANIFOLD_INSUFFICIENTGRADE_MSG    "Manifold lacks sufficient grade."

#define ERROR_MANIFOLD_INVALIDELEMENTID         0x10004
#define ERROR_MANIFOLD_INVALIDELEMENTID_MSG     "Element id is invalid."

#define ERROR_MANIFOLD_INVALIVERTEXLIST         0x10005
#define ERROR_MANIFOLD_INVALIVERTEXLIST_MSG     "List is not a valid vertex position."

#define ERROR_MANIFOLD_SELECTIDARGS             0x10006
#define ERROR_MANIFOLD_SELECTIDARGS_MSG         "Selector 'selectids' requires two arguments: a grade and element ids."

#define ERROR_MANIFOLD_REMOVE                   0x10007
#define ERROR_MANIFOLD_REMOVE_MSG               "Warning: Element %u of grade %u has been deleted, but is still referred to by higher order elements."

#define ERROR_MANIFOLD_BUFFER                   0x10008
#define ERROR_MANIFOLD_BUFFER_MSG               "Warning: buffer full in manifold_graderaise()."

#define MANIFOLD_ALLOCATIONFAILED error_raise(context, ERROR_MANIFOLD_ALLOCATIONFAILED, ERROR_MANIFOLD_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);

/*
 * Property macros
 */

#define MFLD_SET(a, flg) (a |= flg)
#define MFLD_UNSET(a, flg) (a &= !flg)
#define MFLD_ISSET(a, flg) (a & flg)

#define MANIFOLD_GRADE_POINT 0
#define MANIFOLD_GRADE_LINE 1
#define MANIFOLD_GRADE_AREA 2
#define MANIFOLD_GRADE_VOLUME 3

//#define MANIFOLD_GRADE_LINE_LOWER 0
//#define MANIFOLD_GRADE_AREA_LOWER 1

#define MANIFOLD_EDGETOFACE_MAXFACES 20
#define MANIFOLD_VERTEXNORMAL_MAXFACES 20
#define MANIFOLD_GRADEFIND_MAXENTRIES 1024

/* 
 * Selectors
 */

#define MANIFOLD_ADDVERTEX "addvertex"
#define MANIFOLD_DELETEVERTEX "deletevertex"
#define MANIFOLD_GETVERTEXPOSITION "vertexposition"
#define MANIFOLD_SETVERTEXPOSITION "setvertexposition"
#define MANIFOLD_FIX "fix"
#define MANIFOLD_UNFIX "unfix"
#define MANIFOLD_FIXVERTEX "fixvertex"
#define MANIFOLD_UNFIXVERTEX "unfixvertex"
#define MANIFOLD_ISVERTEXFIXED "isvertexfixed"
#define MANIFOLD_EDGEFLIP "edgeflip"
#define MANIFOLD_EXPORTVTK "exportvtk" 
#define MANIFOLD_EXPORTMESH "exportmesh"
#define MANIFOLD_SELECTID "selectid"
#define MANIFOLD_ORIENT "orient"

#define MANIFOLD_NEWFIELD "newfield"

#define MANIFOLD_ADDELEMENT "addelement"
#define MANIFOLD_COUNTELEMENTS "countelements"
#define MANIFOLD_DRAW "draw"
#define MANIFOLD_REFINE "refine"
#define MANIFOLD_EQUIANGULATE "equiangulate"
#define MANIFOLD_SCALE "scale"
#define MANIFOLD_TRANSFORM "transform"
#define MANIFOLD_PRUNE "prune"
#define MANIFOLD_CHECK "check"

/*
 * Function prototypes
 */

expression *manifold_new(void);
void manifold_freevertex(manifold_vertex *);
manifold_graderaiseentry *manifold_newgraderaiseentry(interpretcontext *context);
void manifold_freegraderaiseentry(manifold_graderaiseentry *e);

int manifold_getvertexposition(manifold_object *manifold, interpretcontext *context, uid id, MFLD_DBL *x);
int manifold_setvertexposition(manifold_object *manifold, interpretcontext *context, uid id, MFLD_DBL *x);

void manifold_freegradelowerentry(manifold_gradelowerentry *e);
void manifold_resizegradetable(manifold_object *manifold, interpretcontext *context, unsigned int maxgrade);

void manifold_vertexinit(manifold_object *manifold, interpretcontext *context, uid id, unsigned int dim, MFLD_DBL *p);
void manifold_vertexfree(manifold_object *manifold, interpretcontext *context, uid id);
uid manifold_addvertex(manifold_object *manifold, interpretcontext *context, unsigned int dim, MFLD_DBL *x, unsigned int flags);
int manifold_deletevertex(manifold_object *manifold, interpretcontext *context, uid id);

int manifold_isboundary(manifold_object *manifold, unsigned int grade, uid id);

int manifold_isvertexentryfixed(manifold_vertex *v);

uid manifold_addelement(manifold_object *manifold, interpretcontext *context, unsigned int grade, manifold_gradelowerentry *element);

int manifold_gradelower(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, uid *out, unsigned int *nelements);
int manifold_graderaise(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, unsigned int maxentries, uid *out, unsigned int *nelements);
int manifold_gradefind(manifold_object *manifold, unsigned int grade, uid id, unsigned int targetgrade, uid *out, unsigned int *nelements, int includeconnected);

uid manifold_findlinearelement(manifold_object *manifold, uid v1, uid v2);
uid manifold_findelementfromvertices(manifold_object *manifold, int nv, uid *vids);

int manifold_idlistposition(uid list[], unsigned int length, uid id, unsigned int *posn);

void manifold_identifyedge(manifold_object *mobj, uid edge, uid *v, uid *s, int *sgn);
void manifold_areaboundaryedges(manifold_object *mobj, manifold_gradelowerentry *gl, uid *v, uid *s, int *sgn);
void manifold_edgetofaces(manifold_object *mobj, uid edge, uid *faces, uid *cvert, unsigned int *nfaces);

expression *manifold_vertextomultivector(manifold_object *manifold, interpretcontext *context, uid id);
expression *manifold_elementtomultivector(manifold_object *manifold, interpretcontext *context, int grade, uid id);

void manifold_shiftvertices(manifold_object *mobj, expression_objectreference *fref, MFLD_DBL scale);

MFLD_DBL manifold_vertexseparation(manifold_object *mobj, uid v1, uid v2);
void manifold_vertexdisplacement(manifold_object *mobj, uid v1, uid v2, MFLD_DBL *xx);
int manifold_vertexmidpoint(manifold_object *mobj, uid *v, unsigned int nv, MFLD_DBL *xx, unsigned int *flags);
expression *manifold_vertexnormal(manifold_object *mobj, interpretcontext *context, uid id);

void manifold_vectoradd3d(double v1[3],double v2[3],double *v3);
void manifold_vectorsubtract3d(double v1[3],double v2[3],double *v3);
void manifold_crossproduct3d(double v1[3],double v2[3],double *v3);
double manifold_dotproduct3d(double v1[3],double v2[3]);
double manifold_norm3d(double v1[3]);
void manifold_vectorscale3d(double v1[3],double scale,double v2[3]);
void manifold_normalizevector3d(double *src, double *dest);

double manifold_norm3d(double v1[3]);
unsigned int manifold_orient(manifold_object *mobj, interpretcontext *context);

void manifoldinitialize(void);

#endif
