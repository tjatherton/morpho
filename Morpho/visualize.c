/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "visualize.h"
#include "classes.h"

/* Automatically choose viewdirection */
void visualize_chooseviewdirection(graphics3d_object *gobj) {
#define min(w,a,b) ((w[a])<(w[b])?(a):(b))
#define max(w,a,b) ((w[a])>(w[b])?(a):(b))
    
    if (gobj) {
        graphics3d_bbox bbox;
        graphics3d_calculatebbox(gobj, &bbox);
        float w[3];
        unsigned int imin, imax;
        
        for (unsigned int i=0; i<3; i++) {
            w[i]=fabs(bbox.p2[i]-bbox.p1[i]);
        }
        
        imax = max(w, max(w, 0, 1), 2);
        imin = min(w, min(w, 0, 1), 2);
        
        if (w[imax]>5*w[imin]) {
            float xx[3] = {0.0, 0.0, 0.0};
            for (unsigned int i=0; i<3; i++) {
                gobj->viewdirection[i]=0.0;
                gobj->viewvertical[i]=0.0;
            }
            gobj->viewdirection[imin]=1.0;
            xx[imax]=1.0;
            graphics3d_vectorcrossproduct(xx, gobj->viewdirection, gobj->viewvertical);
        }
    }
}


/* Visualization functions for the manifold */

typedef struct {
    manifold_object *m; // The manifold to draw
    union {
        graphics_object *g2d;
        graphics3d_object *g3d;
    } g; // The graphics object to target
    visualize_selectionmode smode; 
    interpretcontext *context; // An interpretcontext
    expression_objectreference *sel; // A selection
    idtable *colormap[3]; // Colormaps for all grades
    float selcol[3]; // Selection color for each grade
    float deselcol[3];
    float *transform;
} manifold_drawreference;

void manifold_drawsetcolorinmapfunction(manifold_drawreference *ref, int selected) {
    switch (ref->m->dimension) {
        case 2:
            if (selected && ref->smode==MANIFOLD_SELECTIONMODE_HIGHLIGHT) graphics_setcolor(ref->g.g2d, ref->selcol[0], ref->selcol[1], ref->selcol[2]);
            else graphics_setcolor(ref->g.g2d, ref->deselcol[0], ref->deselcol[1], ref->deselcol[2]);
            break;
        case 3:
            if (selected && ref->smode==MANIFOLD_SELECTIONMODE_HIGHLIGHT) graphics3d_setcolor(ref->g.g3d, ref->selcol[0], ref->selcol[1], ref->selcol[2]);
            else graphics3d_setcolor(ref->g.g3d, ref->deselcol[0], ref->deselcol[1], ref->deselcol[2]);
            break;
        default: break;
    }
}

int manifold_drawisselected(manifold_drawreference *ref, int grade, uid id) {
    int ret=FALSE;
    if (ref->sel) {
        expression *out=NULL;
        
        out = class_callselector(ref->context, ref->sel->obj, SELECTION_ISSELECTED_SELECTOR, 3, EXPRESSION_OBJECT_REFERENCE, ref->sel, EXPRESSION_INTEGER, grade, EXPRESSION_INTEGER, (int) id);
        
        if (eval_isbool(out)) {
            ret=eval_boolvalue(out);
        }
        
        freeexpression(out);
    }
    return ret;
}

/* Functions to visualize meshes */
void manifold_drawvertexmapfunction(uid id, void *e, void *r ) {
    manifold_drawreference *ref=(manifold_drawreference *) r;
    manifold_vertex *dp=(manifold_vertex *) e;
    float p[3];
    int selected=manifold_drawisselected(ref, MANIFOLD_GRADE_POINT, id);
    
    if (ref->sel && !selected && ref->smode!=MANIFOLD_SELECTIONMODE_HIGHLIGHT) return;
    
    manifold_drawsetcolorinmapfunction(ref, selected);
    
    for (unsigned int i=0; i<ref->m->dimension; i++) p[i]=(float) dp->x[i];
    
    switch (ref->m->dimension) {
        case 2:
            if (ref->transform) graphics_transformpoint(p, ref->transform, p);
            graphics_point(ref->g.g2d, p);
            break;
        case 3: graphics3d_point(ref->g.g3d, p); break;
        default: break;
    }
}

void manifold_drawedgemapfunction(uid id, void *e, void *r ) {
    manifold_drawreference *ref=(manifold_drawreference *) r;
    manifold_gradelowerentry *gl = (manifold_gradelowerentry *) e;
    int selected=manifold_drawisselected(ref, MANIFOLD_GRADE_LINE, id);
    
    if (ref->sel && !selected && ref->smode!=MANIFOLD_SELECTIONMODE_HIGHLIGHT) return;
    
    if (ref && ref->g.g2d && ref->m) {
        MFLD_DBL p1[3] = {0,0,0}, p2[3] = {0,0,0};
        
        if (manifold_getvertexposition(ref->m, ref->context, gl->el[0], p1) &&
            manifold_getvertexposition(ref->m, ref->context, gl->el[1], p2)) {
            float q1[3] = {p1[0], p1[1], p1[2]}, q2[3] = {p2[0], p2[1], p2[2]};
            
            manifold_drawsetcolorinmapfunction(ref, selected);
            
            switch (ref->m->dimension) {
                case 2:
                    if (ref->transform) {
                        graphics_transformpoint(q1, ref->transform, q1);
                        graphics_transformpoint(q2, ref->transform, q2);
                    }
                    graphics_moveto(ref->g.g2d, q1);
                    graphics_lineto(ref->g.g2d, q2);
                    graphics_stroke(ref->g.g2d);
                    break;
                case 3:
                    graphics3d_moveto(ref->g.g3d, q1);
                    graphics3d_lineto(ref->g.g3d, q2);
                    graphics3d_stroke(ref->g.g3d);
                    break;
                default: break;
            }
        } else {
            /* TODO Should raise mesh inconsistency error */
        }
    }
}

void manifold_drawfacemapfunctionwithcm(uid id, manifold_drawreference *ref, uid *vid, manifold_vertex **v, graphics3d_trianglecomplexelement *el) {
    unsigned int i,j;
    float norm[3];
    
    for (i=0; i<3; i++) {
        for (j=0; j<3; j++) el->points[3*i+j]=(float) v[i]->x[j];
        
        if (ref->colormap[0]) {
            double *col = idtable_get(ref->colormap[0], vid[i]);
            if (col) for (j=0; j<3; j++) el->pointcolors[3*i+j] = (float) col[j];
        }
        
        /* Build up the triangle id */
        el->triangles[i]=i;
    }
    
    graphics3d_trianglenormal(el->points, el->triangles, norm);
    for (i=0; i<3; i++) for (j=0; j<3; j++) el->pointnormals[3*i+j]=norm[j];
}

/* Create an ordered list of vertices for a triangle */
int manifold_orderedverticesfortriangle(manifold_object *m, manifold_gradelowerentry *fc, uid v[3]) {
    manifold_gradelowerentry *s0=NULL, *s1=NULL;
    int succ=FALSE;
    
    s0=idtable_get(m->gradelower[0], fc->el[0]);
    s1=idtable_get(m->gradelower[0], fc->el[1]);
    
    if (s0 && s1) {
    
        v[0]=s0->el[0]; v[1]=s0->el[1];
        if ((s0->el[1] == s1->el[0]) || (s0->el[0] == s1->el[0])) { /* 1 2 , 2 3 or 1 2 , 1 3 */
            v[2]=s1->el[1];
        } else {
            v[2]=v[1];
            v[1]=s1->el[0];
        }
        succ=TRUE;
    }
    
    return succ;
}


void manifold_drawfacemapfunction(uid id, void *e, void *r ) {
    manifold_drawreference *ref=(manifold_drawreference *) r;
    manifold_object *manifold=NULL;
    uid vertexid[3];
    manifold_vertex *v[3] = {NULL, NULL, NULL};
    int selected=manifold_drawisselected(ref, MANIFOLD_GRADE_AREA, id);
    
    if (ref->sel && !selected && ref->smode!=MANIFOLD_SELECTIONMODE_HIGHLIGHT) return;
    
    if (ref && ref->g.g2d && ref->m) {
        manifold=ref->m; /* Use the manifold */
        
        if (manifold_orderedverticesfortriangle(manifold, (manifold_gradelowerentry *) e, vertexid)) {
            for (unsigned int i=0; i<3; i++) v[i]=idtable_get(manifold->vertices, vertexid[i]);
            
            if (ref->colormap[0]) {
                /* If we have a colormap on the vertices we can use a trianglecomplex with linear interpolation */
                switch (ref->m->dimension) {
                    case 2:
                    {
                        graphics_shadetriangles(ref->g.g2d, 1); /* One triangle, 3 points */
                    }
                        break;
                    case 3:
                    {
                        graphics3d_trianglecomplexelement *el = graphics3d_trianglecomplex(ref->g.g3d, 3, 1); /* One triangle, 3 points */
                        manifold_drawfacemapfunctionwithcm(id, ref, vertexid, v, el);
                    }
                        break;
                    default:
                        break;
                }
            } else {
                // If we have a colormap for the faces, use that to choose colors
                if (ref->colormap[2]) {
                    double *col = idtable_get(ref->colormap[2], id);
                    switch (ref->m->dimension) {
                        case 2:
                            graphics_setcolor(ref->g.g2d, col[0], col[1], col[2]);
                            break;
                        case 3:
                            graphics3d_setcolor(ref->g.g3d, col[0], col[1], col[2]);
                            break;
                        default: break;
                    }
                } else {
                    manifold_drawsetcolorinmapfunction(ref, selected);
                }
                
                switch (ref->m->dimension) {
                    case 2:
                    {
                        if (v[0] && v[1] && v[2]) {
                            float x[2];
                            x[0]=v[0]->x[0]; x[1]=v[0]->x[1];
                            graphics_moveto(ref->g.g2d, x);
                            x[0]=v[1]->x[0]; x[1]=v[1]->x[1];
                            graphics_lineto(ref->g.g2d, x);
                            x[0]=v[2]->x[0]; x[1]=v[2]->x[1];
                            graphics_lineto(ref->g.g2d, x);
                            graphics_fill(ref->g.g2d);
                        }
                    }
                        break;
                    case 3:
                        if (v[0] && v[1] && v[2]) graphics3d_trianglefromdouble(ref->g.g3d, v[0]->x, v[1]->x, v[2]->x);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}


expression_objectreference *manifold_draw(manifold_object *m, interpretcontext *context, idtable *colormap[3],  expression_objectreference *sel, visualize_selectionmode smode, unsigned int show, float *transform) {
    expression_objectreference *g=NULL;
    manifold_drawreference ref;
    
    ref.m=m;
    ref.context=context;
    ref.sel=NULL;
    ref.smode=smode;
    for (unsigned int i=0; i<3; i++) ref.colormap[i]=(colormap ? colormap[i] : NULL);
    ref.transform=transform;
    
    if ((sel)&&(class_objectrespondstoselector(sel->obj, SELECTION_ISSELECTED_SELECTOR))) ref.sel=sel;
    
    switch (m->dimension) {
        case 2:
            g=(expression_objectreference *) graphics_newgraphics();
            ref.g.g2d=(graphics_object *) g->obj;
            break;
        case 3:
            g=(expression_objectreference *) graphics3d_newgraphics3d();
            ref.g.g3d=(graphics3d_object *) g->obj;
            break;
        default: return NULL;
    }
    
    if (g) {
        ref.selcol[0]=1.0; ref.selcol[1]=0.0; ref.selcol[2]=0.0;
        ref.deselcol[0]=0.0; ref.deselcol[1]=0.0; ref.deselcol[2]=0.0;
        
        if (m->vertices) {
            if (show & MANIFOLD_DRAW_SHOWVERTICES) idtable_map(m->vertices, manifold_drawvertexmapfunction, &ref);
        }
        
        if ((m->grade>0)&&(m->gradelower)) {
            if (m->gradelower[0]) {
                if (show & MANIFOLD_DRAW_SHOWEDGES) idtable_map(m->gradelower[0], manifold_drawedgemapfunction, &ref);
            }
            
            ref.selcol[0]=1.0; ref.selcol[1]=0.9; ref.selcol[2]=0.9;
            ref.deselcol[0]=0.9; ref.deselcol[1]=0.9; ref.deselcol[2]=0.9;
            
            if ((m->grade>1)&&(m->gradelower[1])) {
                if (show & MANIFOLD_DRAW_SHOWFACES) idtable_map(m->gradelower[1], manifold_drawfacemapfunction, &ref);
            }
        }
    }

    /* Choose viewdirection */
    if (m->dimension==3) {
        visualize_chooseviewdirection(ref.g.g3d);
    }
    
    return g;
}

/*
 * Public interfaces for visualization
 */

/*
 * plotfield() visualizes a field on a manifold
 */
unsigned visualize_processshowmesh(expression *showmeshopt) {
    unsigned int show=MANIFOLD_DRAW_SHOWNONE;
    
    if (eval_isbool(showmeshopt)) {
        int sm = eval_boolvalue(showmeshopt);
        show = (sm ? MANIFOLD_DRAW_SHOWALL : 0);
    } else if (eval_isname(showmeshopt)) {
        expression_name *el = (expression_name *) showmeshopt;
        if (strcmp(el->name, VISUALIZE_SHOWMESH_VERTICES)==0) {
            show |= MANIFOLD_DRAW_SHOWVERTICES;
        } else if (strcmp(el->name, VISUALIZE_SHOWMESH_EDGES)==0) {
            show |= MANIFOLD_DRAW_SHOWEDGES;
        } else if (strcmp(el->name, VISUALIZE_SHOWMESH_FACES)==0) {
            show |= MANIFOLD_DRAW_SHOWFACES;
        }
    } else if (eval_islist(showmeshopt)) {
        linkedlist *lst = ((expression_list *) showmeshopt)->list;
        show = 0;
        
        /* Loop over elements in the list and see what's there */
        if (lst) for (linkedlistentry *e=lst->first; e!=NULL; e=e->next) {
            expression_name *el = (expression_name *) e->data;
            if (eval_isname(el)) {
                if (strcmp(el->name, VISUALIZE_SHOWMESH_VERTICES)==0) {
                    show |= MANIFOLD_DRAW_SHOWVERTICES;
                } else if (strcmp(el->name, VISUALIZE_SHOWMESH_EDGES)==0) {
                    show |= MANIFOLD_DRAW_SHOWEDGES;
                } else if (strcmp(el->name, VISUALIZE_SHOWMESH_FACES)==0) {
                    show |= MANIFOLD_DRAW_SHOWFACES;
                }
            }
        }
    }
    
    return show;
}

typedef struct {
    interpretcontext *context;
    manifold_object *mobj;
    graphics3d_object *gobj;
    float scale;
    unsigned int showgrade;
    enum {
        ARROW, CYLINDER
    } style;
} visualize_plotfieldmapfunctionref;

void visualize_plotfieldmapfunction(uid id, void *data, void *r) {
    visualize_plotfieldmapfunctionref *ref = (visualize_plotfieldmapfunctionref *) r;
    expression_objectreference *fobj = (expression_objectreference *) data;
    if (!eval_isobjectref(fobj)) return;
    
    multivectorobject *mvobj =(multivectorobject *) fobj->obj;
    if (!mvobj) return;
    
    double xx[ref->mobj->dimension], ff[mvobj->dimension];
    float x1[3], x2[3];
    
    manifold_getvertexposition(ref->mobj, ref->context, id, xx);
    multivector_gradetodoublelist(mvobj, ref->showgrade, ff);
    
    for (unsigned int i=0; i<3; i++) {
        x1[i]=xx[i]-0.5*ref->scale * ff[i];
        x2[i]=xx[i]+0.5*ref->scale * ff[i];
    }
    
    switch (ref->style) {
        case ARROW:
            graphics3d_arrow(ref->gobj, x1, x2, 0.2, 10, NULL);
            break;
        case CYLINDER:
            graphics3d_cylinder(ref->gobj, x1, x2, 0.2, 10, NULL);
            break;
    }
}

/* Plotfield visualizes a field on a manifold */
expression *visualize_plotfieldi(interpretcontext *context, int nargs, expression **args) {
    manifold_object *m = NULL;
    interpretcontext *options=NULL;
    int optionstart = 0;
    expression_objectreference *sel=NULL;
    expression_objectreference *fld=NULL;
    expression_objectreference *cm=NULL;
    unsigned int suppgrade=MANIFOLD_GRADE_POINT, showgrade=MANIFOLD_GRADE_POINT;
    unsigned int show=MANIFOLD_DRAW_SHOWALL;
    int usescale=TRUE, isscalar=FALSE;
    double scale=1.0;
    idtable *col[3]={ NULL, NULL, NULL };
    visualize_plotfieldmapfunctionref ref;
    
    expression *ret=NULL;
    
    ref.style=ARROW;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* Process options */
    options=eval_options(context, optionstart, nargs, args);
    
    /* Now try to identify different objects that may have been passed */
    for (unsigned int i=0; i<nargs && i<optionstart; i++) {
        expression_objectreference *oref = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(oref) && oref->obj) {
            if (oref->obj->clss == class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(expression_objectreference *) cloneexpression((expression *) oref);
            if (oref->obj->clss == class_lookup(GEOMETRY_FIELDCLASS)) fld=(expression_objectreference *) cloneexpression((expression *) oref);
        }
        if (oref) freeexpression((expression *) oref);
    }
    
    /* Now analyze options */
    if (options) {
        /* A color map object */
        cm=(expression_objectreference *) lookupsymbol(options, COLOR_MAPLABEL);
        if (!eval_isobjectref(cm)) { freeexpression((expression *) cm); cm=NULL; }
        
        /* Whether or not to scale the plot */
        expression *scaleopt=lookupsymbol(options, MANIFOLD_SCALE);
        if (eval_isreal(scaleopt)) {
            usescale=TRUE;
            scale=eval_floatvalue(scaleopt);
        }
        freeexpression(scaleopt);
        
        /* Which grade to show */
        expression *gradeopt = lookupsymbol(options, VISUALIZE_GRADE);
        if (eval_isinteger(gradeopt)) showgrade=eval_integervalue(gradeopt);
        if (gradeopt) freeexpression(gradeopt);
        
        /* Which grade to show */
        expression_name *style = (expression_name *) lookupsymbol(options, VISUALIZE_STYLE);
        if (eval_isname(style)) {
            if (strcmp(VISUALIZE_STYLE_CYLINDER, style->name)==0) {
                ref.style=CYLINDER;
            } else if (strcmp(VISUALIZE_STYLE_ARROW, style->name)==0) {
                ref.style=ARROW;
            }
        }
        if (style) freeexpression((expression *) style);
        
        /* What to show of the mesh. Here the user can provide either a simple true/false or a list of components */
        expression *showmeshopt = lookupsymbol(options, VISUALIZE_SHOWMESH);
        if (showmeshopt) {
            show = visualize_processshowmesh(showmeshopt);
            freeexpression(showmeshopt);
        }
    }
    
    /* Analyze the field to determine what we're dealing with */
    field_object *fobj = (field_object *) eval_objectfromref(fld);
    
    if (fobj) {
        /* Manifold */
        if (eval_isobjectref(fobj->manifold)) m=(manifold_object *) eval_objectfromref(fobj->manifold);
        
        /* What grade does the support live on? */
        suppgrade = fobj->grade;
        
        /* Is what's being shown a scalar or something else? */
        if (showgrade==0 || showgrade==m->dimension) isscalar=TRUE;
        
        /* Make sure that we show the mesh components that the field is supported on */
        if (isscalar) show |= MANIFOLD_DRAW_SHOWFACES;
        
        if (fobj && m && (m->dimension==2 || m->dimension==3) && suppgrade<MANIFOLD_GRADE_VOLUME) {
            if (isscalar) col[suppgrade] = field_computecolormap(fobj, context, cm, scale, showgrade);
            
            ret = (expression *) manifold_draw(m, context, col, (isscalar ? sel : NULL), MANIFOLD_SELECTIONMODE_DRAWSELECTED, show, NULL);
            
            /* If we're not drawing a scalar, map over the field and draw the entities */
            if (!isscalar)  {
                graphics3d_object *gobj=NULL;
                if ((ret)&&(eval_isobjectref(ret))) gobj=(graphics3d_object *) eval_objectfromref(ret);
                ref.gobj=gobj;
                ref.mobj=m;
                ref.context=context;
                ref.showgrade=showgrade;
                ref.scale=scale;
                
                if (fld) {
                    if (sel) {
                        /* Use the selection */
                        selection_object *sobj = (selection_object *) eval_objectfromref(sel);
                        idtable *sl = NULL;
                        
                        if (sobj) sl = selection_idtableforgrade(sobj, m, fobj->grade);
                        
                        if (sl) idtable_mapintersection(fobj->entries, sl, visualize_plotfieldmapfunction, &ref);
                    } else {
                        idtable_map(fobj->entries, visualize_plotfieldmapfunction, &ref);
                    }
                }
                
                visualize_chooseviewdirection(gobj);
            }
        }
    }
    
    
    
visualize_plotfieldi_cleanup:
    /* Free any colormaps */
    for (unsigned int i=0; i<3; i++) {
        if (col[i]) {
            idtable_map(col[i], idtable_genericfreemapfunction, NULL);
            idtable_free(col[i]);
        }
    }
    
    if (options) freeinterpretcontext(options);
    if (sel) freeexpression((expression *) sel);
    if (fld) freeexpression((expression *) fld);
    if (cm) freeexpression((expression *) cm);
    
    return ret;
}

expression *visualize_plotselectioni(interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    expression_objectreference *sel=NULL;
    expression *showmesh=NULL;
    unsigned int show=MANIFOLD_DRAW_SHOWALL;
    
    interpretcontext *options=NULL;
    int optionstart = 0;
    
    optionstart=eval_optionstart(context, nargs, args);
    if (optionstart<=nargs) options=eval_options(context,optionstart,nargs,args);
    
    /* Try to identify the selection */
    for (unsigned int i=0; i<optionstart; i++) {
        expression_objectreference *oref = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(oref) && oref->obj) {
            if (oref->obj->clss == class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(expression_objectreference *) cloneexpression((expression *) oref);
        }
        if (oref) freeexpression((expression *) oref);
    }
    
    if (options) {
        /* What to show */
        showmesh=lookupsymbol(options, VISUALIZE_SHOWMESH);
        if (showmesh) {
            show = visualize_processshowmesh(showmesh);
            freeexpression(showmesh);
        }
    }
    
    /* Do we have a selection object? */
    if (eval_isobjectref(sel)) {
        selection_object *s = (selection_object *) eval_objectfromref(sel);
        
        if (s && s->selection) {
            for (linkedlistentry *e = s->selection->first; e!=NULL; e=e->next) {
                selection_manifoldref *ref = (selection_manifoldref *) e->data;
                if (ref->mref && (ref->mref->dimension==2 || ref->mref->dimension==3)) {
                    ret = (expression *) manifold_draw(ref->mref, context, NULL, sel, MANIFOLD_SELECTIONMODE_HIGHLIGHT, show, NULL);
                }
            }
        }
    }
    
visualize_plotselectioni_cleanup:
    
    if (options) freeinterpretcontext(options);
    if (sel) freeexpression((expression *) sel);
    
    return ret;
}

/* Add visualization capabilities to the manifold object */
expression *manifold_drawi(object *obj, interpretcontext *context, int nargs, expression **args) {
    manifold_object *m = (manifold_object *) obj;
    expression *ret = NULL, *scaleopt=NULL;
    expression_objectreference *sel=NULL;
    expression_objectreference *fld=NULL;
    expression_objectreference *cm=NULL;
    int scale=TRUE;
    idtable *col[3]={ NULL, NULL, NULL };
    expression *showmesh=NULL, *trans=NULL;
    unsigned int show=MANIFOLD_DRAW_SHOWALL;
    float transform[6] = {1.0,0.0,0.0,0.0,1.0,0.0};
    int usetrans=FALSE;
    
    interpretcontext *options=NULL;
    int optionstart = 0;
    
    optionstart=eval_optionstart(context, nargs, args);
    if (optionstart<=nargs) options=eval_options(context,optionstart,nargs,args);
    
    /* Now try to identify different objects that may have been passed */
    for (unsigned int i=0; i<optionstart-1; i++) {
        expression_objectreference *oref = (expression_objectreference *) interpretexpression(context, args[i]);
        if (eval_isobjectref(oref) && oref->obj) {
            if (oref->obj->clss == class_lookup(GEOMETRY_SELECTIONCLASS)) sel=(expression_objectreference *) cloneexpression((expression *) oref);
            if (oref->obj->clss == class_lookup(GEOMETRY_FIELDCLASS)) fld=(expression_objectreference *) cloneexpression((expression *) oref);
        }
        if (oref) freeexpression((expression *) oref);
    }
    
    if (options) {
        /* A color map object */
        cm=(expression_objectreference *) lookupsymbol(options, COLOR_MAPLABEL);
        if (!eval_isobjectref(cm)) { freeexpression((expression *) cm); cm=NULL; }
        
        /* Whether or not to scale the plot */
        scaleopt=lookupsymbol(options, MANIFOLD_SCALE);
        if (eval_isbool(scaleopt)) scale=eval_boolvalue(scaleopt);
        freeexpression(scaleopt);
        
        /* What to show */
        showmesh=lookupsymbol(options, VISUALIZE_SHOWMESH);
        if (showmesh) {
            show = visualize_processshowmesh(showmesh);
            freeexpression(showmesh);
        }
        
        trans=lookupsymbol(options, MANIFOLD_TRANSFORM);
        if (eval_islist(trans)) usetrans=eval_listtofloatarray((expression_list *) trans, transform, 6);
        freeexpression(trans);
    }
    
    if (m && (m->dimension==2 || m->dimension==3)) {
        if (fld) {
            /* DEPRECATED */
            error_raise(context, ERROR_DEPRECATED, ERROR_DEPRECATED_MSG, ERROR_WARNING);
            
            col[0]=field_computecolormap((field_object *) fld->obj, context, cm, scale, MANIFOLD_GRADE_POINT);
        }
        
        ret = (expression *) manifold_draw(m, context, col, sel, MANIFOLD_SELECTIONMODE_DRAWSELECTED, show, (trans ? transform : NULL));
    }
    
    if (col[0]) {
        idtable_map(col[0], idtable_genericfreemapfunction, NULL);
        idtable_free(col[0]);
    }
    
    if (options) freeinterpretcontext(options);
    if (cm) freeexpression((expression *) cm);
    if (sel) freeexpression((expression *) sel);
    if (fld) freeexpression((expression *) fld);
    
    return ret;
}


void visualizeinitialize(void) {
    /* Add the draw selector to the manifold class */
    classintrinsic *cls=(classintrinsic *) class_lookup(MANIFOLD_LABEL);
    if (cls) class_registerselector(cls, MANIFOLD_DRAW, TRUE, manifold_drawi);
    
    /* Intrinsic functions */
    intrinsic(GEOMETRY_PLOTFIELD, TRUE, visualize_plotfieldi, NULL);
    intrinsic(GEOMETRY_PLOTSELECTION, TRUE, visualize_plotselectioni, NULL);
}


