/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* A color class */

#ifndef Morpho_color_h
#define Morpho_color_h

#include "eval.h"

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    double r;
    double g;
    double b;
} color_object;

#include "graphics.h"

#define COLOR_LABEL "color"
#define COLOR_MAPLABEL "colormap"
#define COLOR_REDSELECTOR "redcomponent"
#define COLOR_GREENSELECTOR "greencomponent"
#define COLOR_BLUESELECTOR "bluecomponent"

int color_iscolor(expression *exp);

/* Obtain the RGB values from a color */
int color_rgbfromobject(interpretcontext *context, object *obj, int nargs, expression **args, double *col);
/* A less flexible but more friendly veneer */
int color_rgbfromobjectref(interpretcontext *context, expression *exp, double *col);

void colorinitialize(void);

#endif
