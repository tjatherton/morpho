/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "graphics.h"

/*
 * Internal functions
 */

/* Constructors */

graphics_element *graphics_newelement(graphics_elementtype type) {
    graphics_element *el=EVAL_MALLOC(sizeof(graphics_element));
    if (el) el->type=type;
    return el;
}

graphics_pointelement *graphics_newpointelement(graphics_elementtype type, float *p) {
    graphics_pointelement *el=EVAL_MALLOC(sizeof(graphics_pointelement));
    if (el) {
        el->type=type;
        el->point[0]=p[0];
        el->point[1]=p[1];
    }

    return el;
}

graphics_rotateelement *graphics_newrotateelement(float angle) {
    graphics_rotateelement *el = EVAL_MALLOC(sizeof(graphics_rotateelement));
    if (el) {
        el->type=GRAPHICS_ROTATE;
        el->angle=angle;
    }
    return el;
}

graphics_linewidthelement *graphics_newlinewidthelement(float width) {
    graphics_linewidthelement *el = EVAL_MALLOC(sizeof(graphics_linewidthelement));
    if (el) {
        el->type=GRAPHICS_LINEWIDTH;
        el->width=width;
    }
    return el;
}

graphics_setcolorelement *graphics_newsetcolorelement(float r, float g, float b) {
    graphics_setcolorelement *el = EVAL_MALLOC(sizeof(graphics_setcolorelement));
    if (el) {
        el->type=GRAPHICS_SETCOLOR;
        el->r=r; el->g=g; el->b=b;
    }
    return el;
}

graphics_textelement *graphics_newtextelement(char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert) {
    graphics_textelement *el = EVAL_MALLOC(sizeof(graphics_textelement));
    
    if (el) {
        el->type=GRAPHICS_TEXT;
        el->point[0]=pt[0];
        el->point[1]=pt[1];
        el->horizontal=alignhoriz;
        el->vertical=alignvert;
        el->string=EVAL_STRDUP(string);
        if (!el->string) { EVAL_FREE(el); return NULL; }
    }
    return el;
}

graphics_shadelatticeelement *graphics_newshadelatticeelement(unsigned int nx, unsigned int ny) {
    graphics_shadelatticeelement *el = EVAL_MALLOC(sizeof(graphics_shadelatticeelement));
    float *data=NULL;
    
    if (el) {
        el->type=GRAPHICS_SHADE_LATTICE;
        el->size[0]=nx;
        el->size[1]=ny;
        data=EVAL_MALLOC(sizeof(float)*nx*ny*5);
        el->data=data;
        
        if (!data) { EVAL_FREE(el); return NULL; }
    }
    return el;
}

graphics_shadetriangleselement *graphics_newshadetriangleselement(unsigned int size) {
    graphics_shadetriangleselement *el = EVAL_MALLOC(sizeof(graphics_shadetriangleselement));
    float *data=NULL;
    
    if (el) {
        el->type=GRAPHICS_SHADE_TRIANGLES;
        el->size=size;
        data=EVAL_MALLOC(sizeof(float)*size*5*3);
        el->data=data;
        
        if (!data) { EVAL_FREE(el); return NULL; }
    }
    return el;
}

graphics_bboxelement *graphics_newbboxelement(graphics_bbox *bbox) {
    graphics_bboxelement *el = EVAL_MALLOC(sizeof(graphics_bboxelement));
    
    if (el) {
        el->type=GRAPHICS_FORCE_BBOX;
        el->bbox=*bbox;
    }
    return el;
}

graphics_element *graphics_cloneentry(graphics_element *el) {
    graphics_element *new=NULL;
    
    if (el) switch (el->type) {
        case GRAPHICS_CLOSE:
        case GRAPHICS_STROKE:
        case GRAPHICS_FILL:
        case GRAPHICS_SAVESTATE:
        case GRAPHICS_RESTORESTATE:
            new=graphics_newelement(el->type);
            break;
        case GRAPHICS_MOVETO:
        case GRAPHICS_LINETO:
        case GRAPHICS_POINT:
        case GRAPHICS_SCALE:
        case GRAPHICS_TRANSLATE:
            new=(graphics_element *) graphics_newpointelement(el->type, ((graphics_pointelement *) el)->point);
            break;
            
        case GRAPHICS_ROTATE:
            new=(graphics_element *) graphics_newrotateelement(((graphics_rotateelement *) el)->angle);
            break;
            
        case GRAPHICS_LINEWIDTH:
            new=(graphics_element *) graphics_newlinewidthelement(((graphics_linewidthelement *) el)->width);
            break;
            
        case GRAPHICS_FORCE_BBOX:
            new=(graphics_element *) graphics_newbboxelement(&((graphics_bboxelement *) el)->bbox);
            break;
            
        case GRAPHICS_SETCOLOR:
            {
                graphics_setcolorelement *lel= (graphics_setcolorelement *) el;
                new=(graphics_element *) graphics_newsetcolorelement(lel->r,lel->g,lel->b);
            }
            break;
            
        case GRAPHICS_TEXT:
            {
                graphics_textelement *tel= (graphics_textelement *) el;
                new=(graphics_element *) graphics_newtextelement(tel->string, tel->point, tel->horizontal, tel->vertical);
            }
            break;
        case GRAPHICS_SHADE_TRIANGLES:
            {
                graphics_shadetriangleselement *sel = (graphics_shadetriangleselement *) el;
                new=(graphics_element *) graphics_newshadetriangleselement(sel->size);
                if (new) memcpy(((graphics_shadetriangleselement *) new)->data, sel->data, sel->size*3*5);
            }
            break;
            
        case GRAPHICS_SHADE_LATTICE:
            {
                graphics_shadelatticeelement *sel = (graphics_shadelatticeelement *) el;
                new=(graphics_element *) graphics_newshadelatticeelement(sel->size[0], sel->size[1]);
                if (new) memcpy(((graphics_shadelatticeelement *) new)->data, sel->data, sel->size[0]*sel->size[1]*sizeof(float)*5);
            }
            break;
    }
    
    return new;
}

/* Other utility functions. */

void graphics_expandbbox(graphics_bbox *bbox, float *pt) {
    if (pt[0]<bbox->p1[0]) bbox->p1[0]=pt[0];
    if (pt[1]<bbox->p1[1]) bbox->p1[1]=pt[1];
    if (pt[0]>bbox->p2[0]) bbox->p2[0]=pt[0];
    if (pt[1]>bbox->p2[1]) bbox->p2[1]=pt[1];
}

void graphics_calculatebbox(graphics_object *g, graphics_bbox *bbox) {
    //    float lw=1.0;
    graphics_element *el=NULL;
    int first=TRUE;
    
    if (!g) return;
    
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        switch (el->type) {
            case GRAPHICS_FORCE_BBOX:
                /* Forced bounding box entries trump the calculation */
                *bbox=((graphics_bboxelement *) el)->bbox;
                return;
                break;
            case GRAPHICS_SAVESTATE:
            case GRAPHICS_RESTORESTATE:
                break;
            case GRAPHICS_SCALE:
                /* TODO: Implement */
                
                break;
            case GRAPHICS_ROTATE:
                /* TODO: Implement */
                
                break;
            case GRAPHICS_TRANSLATE:
                /* TODO: Implement */
                
                break;
            case GRAPHICS_POINT:
            case GRAPHICS_MOVETO:
            case GRAPHICS_LINETO:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                if (first) {
                    bbox->p1[0]=pel->point[0];
                    bbox->p1[1]=pel->point[1];
                    bbox->p2[0]=pel->point[0];
                    bbox->p2[1]=pel->point[1];
                    first=FALSE;
                } else {
                    graphics_expandbbox(bbox,pel->point);
                }
            }
                break;
            case GRAPHICS_TEXT:
            {
                graphics_bbox stringbox;
                graphics_textelement *tel=(graphics_textelement *) el;
                
                unsigned long length=strlen(tel->string);
                float width=((float) length)*(float) 10.0;
                float height=10.0;
                
                for (int i=0; i<2; i++) stringbox.p1[i]=stringbox.p2[i]=tel->point[i];
                switch (tel->horizontal) {
                    case GRAPHICS_ALIGNLEFT: stringbox.p2[0]+=width; break;
                    case GRAPHICS_ALIGNCENTER: stringbox.p1[0]-=width/2; stringbox.p2[0]+=width/2; break;
                    case GRAPHICS_ALIGNRIGHT: stringbox.p1[0]-=width; break;
                    default: break;
                }
                switch (tel->vertical) {
                    case GRAPHICS_ALIGNBOTTOM: stringbox.p2[1]+=height;  break;
                    case GRAPHICS_ALIGNMIDDLE: stringbox.p1[1]-=height/2; stringbox.p2[1]+=height/2; break;
                    case GRAPHICS_ALIGNTOP: stringbox.p1[1]-=height; break;
                    default: break;
                }
                
                if (first) {
                    *bbox=stringbox;
                    first=FALSE;
                }
                
                graphics_expandbbox(bbox,stringbox.p1);
                graphics_expandbbox(bbox,stringbox.p2);
            }
                break;
            case GRAPHICS_LINEWIDTH:
                //                lw=((graphics_linewidthelement *) el)->width;
                break;
            case GRAPHICS_STROKE:
            case GRAPHICS_CLOSE:
            case GRAPHICS_SETCOLOR:
            case GRAPHICS_FILL:
                break;
            case GRAPHICS_SHADE_TRIANGLES:
            {
                /* maybe should store the bbox ? */
                graphics_shadetriangleselement *sel=(graphics_shadetriangleselement *) el;
                
                graphics_bbox sbbox;
                sbbox.p1[0]=sel->data[0];
                sbbox.p2[0]=sel->data[0];
                sbbox.p1[1]=sel->data[1];
                sbbox.p2[1]=sel->data[1];
                
                unsigned int size = sel->size;
                for (unsigned int k=0; k<size*3; k++) {
                    graphics_expandbbox(&sbbox,&sel->data[k*5]);
                }
                
                if (first) {
                    *bbox=sbbox;
                    first=FALSE;
                } else {
                    graphics_expandbbox(bbox,sbbox.p1);
                    graphics_expandbbox(bbox,sbbox.p2);
                }
            }
                break;
            case GRAPHICS_SHADE_LATTICE:
            {
                /* maybe should store the bbox ? */
                graphics_shadelatticeelement *sel=(graphics_shadelatticeelement *) el;
                
                graphics_bbox sbbox;
                sbbox.p1[0]=sel->data[0];
                sbbox.p2[0]=sel->data[0];
                sbbox.p1[1]=sel->data[1];
                sbbox.p2[1]=sel->data[1];
                
                unsigned int size = sel->size[0]*sel->size[1];
                for (unsigned int k=0; k<size; k++) {
                    graphics_expandbbox(&sbbox,&sel->data[k*5]);
                }
                
                if (first) {
                    *bbox=sbbox;
                    first=FALSE;
                } else {
                    graphics_expandbbox(bbox,sbbox.p1);
                    graphics_expandbbox(bbox,sbbox.p2);
                }
            }
                break;
        }
    }
}

int graphics_validate_pointlist(expression_list *list, unsigned int dim) {
    /* Validate the input as a set of coordinate pairs */
    
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        expression_list *lst=(expression_list *) e->data;
        
        if(lst->type!=EXPRESSION_LIST) { printf("One or more lists as coordinate pairs are required.\n"); return FALSE;}
        if (linkedlist_length(lst->list)!=dim) {
            printf("Accepts %uD coordinates only.\n",dim); return FALSE;
        } else {
            for (linkedlistentry *c=lst->list->first; c!=NULL; c=c->next) {
                if ( (((expression *) c->data)->type!=EXPRESSION_FLOAT) &&
                    (((expression *) c->data)->type!=EXPRESSION_INTEGER) ) {
                    printf("Requires numerical values.\n"); return FALSE;
                }
            }
        }
    }
    
    return TRUE;
}

/*
 * Interface functions
 */

expression *graphics_newgraphics(void) {
    graphics_object *obj=(graphics_object *) class_instantiate(class_lookup(GRAPHICS_LABEL));
    expression_objectreference *ref=NULL;
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

void graphics_point(graphics_object *obj,float *pt) {
    graphics_pointelement *el = graphics_newpointelement(GRAPHICS_POINT, pt);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_moveto(graphics_object *obj,float *pt) {
    graphics_pointelement *el = graphics_newpointelement(GRAPHICS_MOVETO, pt);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_lineto(graphics_object *obj,float *pt) {
    graphics_pointelement *el = graphics_newpointelement(GRAPHICS_LINETO, pt);
    if (el) linkedlist_addentry(obj->list, el);}

void graphics_scale(graphics_object *obj, float *s) {
    graphics_pointelement *el = graphics_newpointelement(GRAPHICS_SCALE, s);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_rotate(graphics_object *obj, float angle) {
    graphics_rotateelement *el = graphics_newrotateelement(angle);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_translate(graphics_object *obj, float *t) {
    graphics_pointelement *el = graphics_newpointelement(GRAPHICS_TRANSLATE, t);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_savestate(graphics_object *obj) {
    graphics_element *sel = graphics_newelement(GRAPHICS_SAVESTATE);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics_restorestate(graphics_object *obj) {
    graphics_element *sel = graphics_newelement(GRAPHICS_RESTORESTATE);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics_linewidth(graphics_object *obj,float width) {
    graphics_linewidthelement *el = graphics_newlinewidthelement(width);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_setcolor(graphics_object *obj,float r, float g, float b) {
    graphics_setcolorelement *el = graphics_newsetcolorelement(r, g, b);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_closepath(graphics_object *obj) {
    graphics_element *sel = graphics_newelement(GRAPHICS_CLOSE);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics_fill(graphics_object *obj) {
    graphics_element *sel = graphics_newelement(GRAPHICS_FILL);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics_stroke(graphics_object *obj) {
    graphics_element *sel = graphics_newelement(GRAPHICS_STROKE);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics_forcebbox(graphics_object *obj, graphics_bbox *bbox) {
    graphics_bboxelement *bel = graphics_newbboxelement(bbox);
    if (bel) linkedlist_addentry(obj->list, bel);
}

void graphics_text(graphics_object *obj, char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert) {
    graphics_textelement *el = graphics_newtextelement(string, pt, alignhoriz, alignvert);
    
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics_line(graphics_object *obj, linkedlist *list, int close, int fill) {
    for (linkedlistentry *e=list->first; e!=NULL; e=e->next) {
        expression_list *lst=(expression_list *) e->data;
        graphics_pointelement *el = EVAL_MALLOC(sizeof(graphics_pointelement));
        
        if (el) {
            if (e==list->first) el->type=GRAPHICS_MOVETO;
            else el->type=GRAPHICS_LINETO;
            
            eval_listtofloatarray(lst, el->point, 2);
            
            linkedlist_addentry(obj->list, el);
        }
    }
    
    if (close) graphics_closepath(obj);
    if (fill) graphics_fill(obj);
    if (linkedlist_length(list)>1) graphics_stroke(obj);
}

void graphics_arrow(graphics_object *obj, float *x1, float *x2, float frac) {
    float x3[3], x4[3];
#define AR 0.33333333333;
    
    /* The base */
    graphics_moveto(obj, x1);
    graphics_lineto(obj, x2);
    graphics_stroke(obj);
    
    /* Other two points */
    x3[0]=x2[0] - x2[0]*frac + frac*x1[0] + (x1[1] - x2[1])*AR;
    x3[1]=x2[1] - x2[1]*frac + frac*x1[1] + (x2[0] - x1[0])*AR;
    
    x4[0]=x2[0] - x2[0]*frac + frac*x1[0] - (x1[1] - x2[1])*AR;
    x4[1]=x2[1] - x2[1]*frac + frac*x1[1] - (x2[0] - x1[0])*AR;
    
    graphics_moveto(obj, x3);
    graphics_lineto(obj, x2);
    graphics_lineto(obj, x4);
    graphics_fill(obj);
}


/* graphics_shadetriangles - Creates and adds a Shading gouraud triangles element to the graphics object.
 * Input:   (graphics_object *) obj - The graphics object to use
 *          (unsigned int) size     - number of triangles in the lattice
 * Returns: A pointer to the data section of the triangles element, which is an array of floats of size
 *          size*3*5 and must be filled out by the caller upon return.
 *              x -- point 1
 *              y
 *              r
 *              g
 *              b
 *              x -- point 2
 *              y
 *              r
 *              g
 *              b
 *              x -- point 3
 *              y
 *              r
 *              g
 *              b ...
 *          -or- NULL on failure.
 */
float *graphics_shadetriangles(graphics_object *obj, unsigned int size) {
    graphics_shadetriangleselement *el = (graphics_shadetriangleselement *) graphics_newshadetriangleselement(size);
    
    if (el) linkedlist_addentry(obj->list, el);
    else return NULL;
    
    return el->data;
}


/* graphics_shadelattice - Creates and adds a Shading gouraud lattice element to the graphics object.
 * Input:   (graphics_object *) obj - The graphics object to use
 *          (unsigned int) nx       - number of x points on the lattice
 *          (unsigned int) ny       - number of y points on the lattice
 * Returns: A pointer to the data section of the lattice element, which is an array of floats of size
 *          nx*ny*5 and must be filled out by the caller upon return.
 *              x
 *              y
 *              r
 *              g
 *              b ... etc.
 *          -or- NULL on failure.
 */
float *graphics_shadelattice(graphics_object *obj, unsigned int nx, unsigned int ny) {
    graphics_shadelatticeelement *el = (graphics_shadelatticeelement *) graphics_newshadelatticeelement(nx, ny);

    if (el) linkedlist_addentry(obj->list, el);
    else return NULL;
    
    return el->data;
}

/* 
 * Linear point transformations
 */

/* Transforms a vector
 * Input:	v1		- A point.
 *			m		- A matrix { R11 R12 T1
 *                               R21 R22 T2 }
 * Output:  v2      - R.v1 + T
 * Returns:			-
 */
void graphics_transformpoint(float v1[2], float m[6], float v2[2]) {
    float out[3];
    for (unsigned int i=0;i<2;i++) {
        out[i]=m[i*3+2];
        for (unsigned int j=0; j<2; j++) {
            out[i]+=m[3*i+j]*v1[j];
        }
    }
    
    for (unsigned int i=0;i<2;i++) v2[i]=out[i]; 
}

/*
 * ---- Postscript Export ----
 */

expression *graphics_exporteps(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs!=1) return NULL;
    if (args[0]->type!=EXPRESSION_STRING) return NULL;
    
    FILE *f;
    char *filename=((expression_string *)args[0])->value;
    graphics_object *g=(graphics_object *) obj;
    graphics_element *el=NULL;
    graphics_bbox bbox;
    
    graphics_calculatebbox(g,&bbox);
    
    f=fopen(filename, "w");
    if (!f) return NULL;
    
    fprintf(f, "%s","%!PS-Adobe-3.0 EPSF-3.0\n");
    fprintf(f, "%s %f %f %f %f\n","%%BoundingBox:",bbox.p1[0],bbox.p1[1],bbox.p2[0],bbox.p2[1]);
    
    fprintf(f,"%s\n%s\n%s\n","/Helvetica findfont","10 scalefont","setfont");
    
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        switch (el->type) {
            case GRAPHICS_POINT:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                fprintf(f, "%f %f %f 0 360 arc fill\n",pel->point[0],pel->point[1],(bbox.p2[0]-bbox.p1[0])*GRAPHICS_POINTSIZE);
            }
            case GRAPHICS_MOVETO:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                fprintf(f, "%f %f moveto\n",pel->point[0],pel->point[1]);
            }
                break;
            case GRAPHICS_LINETO:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                fprintf(f, "%f %f lineto\n",pel->point[0],pel->point[1]);
            }
                break;
            case GRAPHICS_LINEWIDTH:
                fprintf(f,"%g setlinewidth\n",((graphics_linewidthelement *) el)->width);
                break;
            case GRAPHICS_SETCOLOR:
            {
                graphics_setcolorelement *cel=(graphics_setcolorelement *) el;
                fprintf(f,"%g %g %g setrgbcolor\n",cel->r,cel->g,cel->b);
            }
                break;
            case GRAPHICS_STROKE:
                fprintf(f,"stroke\n");
                break;
            case GRAPHICS_CLOSE:
                fprintf(f,"closepath\n");
                break;
            case GRAPHICS_FILL:
                fprintf(f,"fill\n");
                break;
                
            case GRAPHICS_SAVESTATE:
                fprintf(f,"gsave\n");
                break;
                
            case GRAPHICS_RESTORESTATE:
                fprintf(f,"grestore\n");
                break;
                
            case GRAPHICS_SCALE:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                fprintf(f,"%f %f scale\n",pel->point[0],pel->point[1]);
            }
                break;
                
            case GRAPHICS_ROTATE:
                fprintf(f,"%f rotate\n",((graphics_rotateelement *) el)->angle);
                break;
                
            case GRAPHICS_TRANSLATE:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                fprintf(f,"%f %f translate\n",pel->point[0],pel->point[1]);
            }
                break;
            case GRAPHICS_TEXT:
            {
                graphics_textelement *tel=(graphics_textelement *) el;
                fprintf(f,"(%s)\n", tel->string);
                
                /* Push the horizontal coordinate onto the stack */
                if (tel->horizontal==GRAPHICS_ALIGNLEFT) {
                    fprintf(f, "%f\n",tel->point[0]);
                } else {
                    fprintf(f, "dup stringwidth pop\n");
                    if (tel->horizontal==GRAPHICS_ALIGNCENTER)
                        fprintf(f, "2 div\n");
                    fprintf(f,"%f sub neg\n",tel->point[0]);
                    
                }
                /* Push the vertical coordinate onto the stack and moveto */
                switch (tel->vertical) {
                    case GRAPHICS_ALIGNBOTTOM: fprintf(f, "%f moveto\n",tel->point[1]); break;
                    case GRAPHICS_ALIGNMIDDLE: fprintf(f, "%f moveto\n",tel->point[1]-5.0); break;
                    case GRAPHICS_ALIGNTOP: fprintf(f, "%f moveto\n",tel->point[1]-10.0); break;
                    default: break;
                }
                fprintf(f,"show\n");
            }
                break;
            case GRAPHICS_SHADE_TRIANGLES:
            {
                graphics_shadetriangleselement *sel=(graphics_shadetriangleselement *) el;
                fprintf(f, "%s","<<\n");
                fprintf(f, "%s","/ShadingType 4\n");
                fprintf(f, "%s","/ColorSpace /DeviceRGB\n");
                fprintf(f, "%s","/DataSource [\n");
                for (unsigned int k=0; k<3*sel->size; k++) {
                    fprintf(f, "0 "); /* Edge flag [see Adobe PS Language reference v3 chapter 4, p271] */
                    for (unsigned int l=0; l<5; l++) {
                        fprintf(f, "%f ", sel->data[k*5+l]);
                    }
                    fprintf(f, "\n");
                }
                fprintf(f, "%s","]\n");
                fprintf(f, "%s",">>\n");
                fprintf(f, "%s","shfill\n");
            }
                break;
            case GRAPHICS_SHADE_LATTICE:
            {
                graphics_shadelatticeelement *sel=(graphics_shadelatticeelement *) el;
                unsigned int size = sel->size[0]*sel->size[1];
                fprintf(f, "%s","<<\n");
                fprintf(f, "%s","/ShadingType 5\n");
                fprintf(f, "%s","/ColorSpace /DeviceRGB\n");
                fprintf(f, "/VerticesPerRow %u\n", sel->size[0]);
                fprintf(f, "%s","/DataSource [\n");
                for (unsigned int k=0; k<size; k++) {
                    for (unsigned int l=0; l<5; l++) {
                        fprintf(f, "%f ", sel->data[k*5+l]);
                    }
                    fprintf(f, "\n");
                }
                fprintf(f, "%s","]\n");
                fprintf(f, "%s",">>\n");
                fprintf(f, "%s","shfill\n");
            }
                break;
            case GRAPHICS_FORCE_BBOX:
                /* This is already detected when the bbox was calculated. */
                break;
        }
    }
    
    fclose(f);
    
    return (expression *) class_newobjectreference(obj);
}

/*
 * ---- PDF Export ----
 */

#define WRITESTRINGTOFILE(s,f) if (f) fprintf(f, "%s",s)

int graphics_writepdfcontent(FILE *f, graphics_object *g) {
    char s[255];
    graphics_element *el=NULL;
    int c=0;
    int resobj=0;
    
    c+=sprintf(s, "/DeviceRGB CS\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "/DeviceRGB cs\n"); WRITESTRINGTOFILE(s,f);
    
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        switch (el->type) {
            case GRAPHICS_POINT:
            {
                //         graphics_pointelement *pel=(graphics_pointelement *) el;
                //         fprintf(f, "%f %f %f 0 360 arc fill\n",pel->point[0],pel->point[1],(bbox.p2[0]-bbox.p1[0])*0.004);
            }
            case GRAPHICS_MOVETO:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                c+=sprintf(s, "%f %f m\n",pel->point[0],pel->point[1]);
                WRITESTRINGTOFILE(s,f);
            }
                break;
            case GRAPHICS_LINETO:
            {
                graphics_pointelement *pel=(graphics_pointelement *) el;
                c+=sprintf(s, "%f %f l\n",pel->point[0],pel->point[1]);
                WRITESTRINGTOFILE(s,f);
            }
                break;
                
            case GRAPHICS_LINEWIDTH:
                c+=sprintf(s,"%g w\n",((graphics_linewidthelement *) el)->width);
                WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_SETCOLOR:
            {
                graphics_setcolorelement *cel=(graphics_setcolorelement *) el;
                c+=sprintf(s,"%g %g %g SC\n",cel->r,cel->g,cel->b);
                WRITESTRINGTOFILE(s,f);
                c+=sprintf(s,"%g %g %g sc\n",cel->r,cel->g,cel->b);
                WRITESTRINGTOFILE(s,f);
            }
                break;
                
            case GRAPHICS_STROKE:
                c+=sprintf(s,"S\n");
                WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_CLOSE:
                c+=sprintf(s,"h\n");
                WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_FILL:
                c+=sprintf(s,"f\n");
                WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_SAVESTATE:
                //      c+=sprintf(s,"q\n");
                //      WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_RESTORESTATE:
                //     c+=sprintf(s,"Q\n");
                //     WRITESTRINGTOFILE(s,f);
                break;
                
            case GRAPHICS_SCALE:
            {
                //      graphics_pointelement *pel=(graphics_pointelement *) el;
                //      fprintf(f,"%f %f scale\n",pel->point[0],pel->point[1]);
            }
                break;
                
            case GRAPHICS_ROTATE:
                //     fprintf(f,"%f rotate\n",((graphics_rotateelement *) el)->angle);
                break;
                
            case GRAPHICS_TRANSLATE:
            {
                //    graphics_pointelement *pel=(graphics_pointelement *) el;
                //    fprintf(f,"%f %f translate\n",pel->point[0],pel->point[1]);
            }
                break;
            case GRAPHICS_TEXT:
            {
                graphics_textelement *tel=(graphics_textelement *) el;
                float x=0,y=0;
                
                c+=sprintf(s,"BT\n"); WRITESTRINGTOFILE(s,f);
                c+=sprintf(s,"/F1 10 Tf\n"); WRITESTRINGTOFILE(s,f); // Select font
                
                /* Push the horizontal coordinate onto the stack */
                /* if (tel->horizontal==GRAPHICS_ALIGNLEFT) {
                 //         fprintf(f, "%f\n",tel->point[0]);
                 } else {
                 //       fprintf(f, "dup stringwidth pop\n");
                 //     if (tel->horizontal==GRAPHICS_ALIGNCENTER)
                 //         fprintf(f, "2 div\n");
                 //     fprintf(f,"%f sub neg\n",tel->point[0]);
                 
                 }*/
                x=tel->point[0];
                
                switch (tel->vertical) {
                    case GRAPHICS_ALIGNBOTTOM: y=tel->point[1]; break;
                    case GRAPHICS_ALIGNMIDDLE: y=tel->point[1]-5.0; break;
                    case GRAPHICS_ALIGNTOP: y=tel->point[1]-10.0; break;
                    default: break;
                }
                
                c+=sprintf(s,"%f %f Td\n", x, y); WRITESTRINGTOFILE(s,f);
                
                c+=sprintf(s,"(%s) Tj\n", tel->string); WRITESTRINGTOFILE(s,f);
                
                c+=sprintf(s,"ET\n"); WRITESTRINGTOFILE(s,f);
            }
                break;
            case GRAPHICS_SHADE_TRIANGLES:
            case GRAPHICS_SHADE_LATTICE:
            {
                c+=sprintf(s, "/SH%i sh\n",resobj); WRITESTRINGTOFILE(s,f);
                resobj++;
            }
                break;
            case GRAPHICS_FORCE_BBOX:
                /* This is already detected when the bbox was calculated. */
                break;
        }
    }
    
    return c;
}

int graphics_writepdfshadetriangles(FILE *f, graphics_shadetriangleselement *sel) {
    char s[255];
    int c=0;
    
    c+=sprintf(s, "%s","<<\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/ShadingType 4\n");  WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/ColorSpace /DeviceRGB\n");  WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/DataSource [\n");  WRITESTRINGTOFILE(s,f);
    for (unsigned int k=0; k<3*sel->size; k++) {
        c+=sprintf(s, "0 ");  WRITESTRINGTOFILE(s,f);
        for (unsigned int l=0; l<5; l++) {
            c+=sprintf(s, "%f ", sel->data[k*5+l]);  WRITESTRINGTOFILE(s,f);
        }
        c+=sprintf(s, "\n");  WRITESTRINGTOFILE(s,f);
    }
    c+=sprintf(s, "%s","]\n");  WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s",">>\n");  WRITESTRINGTOFILE(s,f);
    return c;
}

int graphics_writepdfshadelattice(FILE *f, graphics_shadelatticeelement *sel) {
    char s[255];
    int c=0;
    unsigned int size = sel->size[0]*sel->size[1];
    
    c+=sprintf(s, "%s","<<\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/ShadingType 5\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/ColorSpace /DeviceRGB\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "/VerticesPerRow %u\n", sel->size[0]); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s","/DataSource [\n"); WRITESTRINGTOFILE(s,f);
    for (unsigned int k=0; k<size; k++) {
        for (unsigned int l=0; l<5; l++) {
            c+=sprintf(s, "%f ", sel->data[k*5+l]); WRITESTRINGTOFILE(s,f);
        }
        c+=sprintf(s, "\n"); WRITESTRINGTOFILE(s,f);
    }
    c+=sprintf(s, "%s","]\n"); WRITESTRINGTOFILE(s,f);
    c+=sprintf(s, "%s",">>\n"); WRITESTRINGTOFILE(s,f);
    return c;
}

int graphics_writepdfresourcedictionary(FILE *f, graphics_object *g, int startobj) {
    graphics_element *el=NULL;
    int i=0;
    int c=0;
    
    c+=fprintf(f, "/Resources\n<<\n/ProcSet 6 0 R\n/Font << /F1 7 0 R >>\n");
    c+=fprintf(f, "/Shading <<\n");
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        if ((el->type==GRAPHICS_SHADE_TRIANGLES)||(el->type==GRAPHICS_SHADE_LATTICE)) {
            c+=fprintf(f,"/SH%i %i 0 R\n",i,startobj+i);
            /*   c+=fprintf(f,"/SH%i ",i);
             if (el->type==GRAPHICS_SHADE_TRIANGLES) c+=graphics_writepdfshadetriangles(f, (graphics_shadetriangleselement *) el);
             else if (el->type==GRAPHICS_SHADE_LATTICE) c+=graphics_writepdfshadelattice(f, (graphics_shadelatticeelement *) el);*/
            i++;
        }
    }
    c+=fprintf(f, ">>\n");
    c+=fprintf(f, ">>\n");
    
    return c;
}

int graphics_writepdfresourceobjects(FILE *f, graphics_object *g, int startobj, int *objects, int startoffset, int *endobj) {
    graphics_element *el=NULL;
    int obj=startobj;
    int c=0;
    
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics_element *) e->data;
        switch (el->type) {
            case GRAPHICS_SHADE_TRIANGLES:
            {
                objects[obj]=c+startoffset; obj++;
                graphics_shadetriangleselement *sel=(graphics_shadetriangleselement *) el;
                //int length=graphics_writepdfshadetriangles(NULL, sel);
                
                c+=fprintf(f, "%i 0 obj\n",obj);
                //                c+=fprintf(f, "<< /Length %i >>\n",length);
                //             c+=fprintf(f, "stream\n");
                
                c+=graphics_writepdfshadetriangles(f, sel);
                
                //           c+=fprintf(f, "endstream\nendobj\n");
                c+=fprintf(f, "endobj\n");
            }
                break;
            case GRAPHICS_SHADE_LATTICE:
            {
                objects[obj]=c+startoffset; obj++;
                graphics_shadelatticeelement *sel=(graphics_shadelatticeelement *) el;
                //    int length=graphics_writepdfshadelattice(NULL, sel);
                
                c+=fprintf(f, "%i 0 obj\n",obj);
                //    c+=fprintf(f, "<< /Length %i >>\n",length);
                //    c+=fprintf(f, "stream\n");
                
                c+=graphics_writepdfshadelattice(f, sel);
                
                c+=fprintf(f, "endobj\n");
            }
                break;
            default:
                break;
        }
    }
    
    if (endobj) *endobj=obj;
    
    return c;
}

expression *graphics_exportpdf(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* See "PDF Reference second edition, Adobe Portable Document Format Version 1.3" by Adobe Systems Incorporated, Addison-Wesley (2000) */
    /* N.B. This is currently limited to 1 page of PDF - it is intended for generating single
     images rather than whole documents. */
    if (nargs!=1) return NULL;
    if (args[0]->type!=EXPRESSION_STRING) return NULL;
    
    FILE *f;
    char *filename=((expression_string *)args[0])->value;
    graphics_object *g=(graphics_object *) obj;
    graphics_bbox bbox;
    unsigned int c=0,xrefpos=0;
    int objects[GRAPHICS_PDFMAXOBJECTS];
    int nobjects=0;
    
    graphics_calculatebbox(g,&bbox);
    
    f=fopen(filename, "w");
    if (!f) return NULL;
    
    // File Header
    c+=fprintf(f, "%s","%PDF-1.3\n");
    
    /* File Body */
    // Object 1 - The catalog
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "1 0 obj\n<< /Type /Catalog\n /Outlines 2 0 R\n/Pages 3 0 R >>\nendobj\n");
    
    // Object 2 - Outlines
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "2 0 obj\n<< /Type Outlines\n/Count 0 >>\nendobj\n");
    
    // Object 3 - Pages
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "3 0 obj\n<< /Type /Pages\n/Kids [4 0 R]\n/Count 1 >>\nendobj\n");
    
    // Object 4 - Page descriptor
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "4 0 obj\n<< /Type /Page\n/Parent 3 0 R\n");
    c+=fprintf(f, "/MediaBox [%f %f %f %f]\n",bbox.p1[0],bbox.p1[1],bbox.p2[0],bbox.p2[1]);
    c+=fprintf(f, "/Contents 5 0 R\n");
    c+=graphics_writepdfresourcedictionary(f,g,8);
    c+=fprintf(f, ">>\nendobj\n");
    
    // Object 5 - Page contents
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "5 0 obj\n");
    c+=fprintf(f, "<< /Length %i >>\n",graphics_writepdfcontent(NULL,g));
    c+=fprintf(f, "stream\n");
    
    graphics_writepdfcontent(f,g);
    
    c+=fprintf(f, "endstream\nendobj\n");
    
    // Object 6 - Procedure set
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "6 0 obj\n[/PDF /Text]\nendobj\n");
    
    // Object 7 - Font record for default font (Helvetica)
    objects[nobjects]=c; nobjects++;
    c+=fprintf(f, "7 0 obj\n<< /Type /Font\n/Subtype /Type1\n");
    c+=fprintf(f, "/Name /F1\n/BaseFont /Helvetica\n/Encoding /MacRomanEncoding\n");
    c+=fprintf(f, ">> endobj\n");
    
    // Now create any additional objects necessary for the content.
    c+=graphics_writepdfresourceobjects(f, g, nobjects, objects, c, &nobjects);
    
    // Cross reference table
    xrefpos=c;
    c+=fprintf(f,"xref\n");
    c+=fprintf(f,"0 %i\n", nobjects);
    c+=fprintf(f,"0000000000 65535 f \n");
    for (int j=0; j<nobjects; j++) {
        fprintf(f, "%010i 00000 n \n",objects[j]);
    }
    
    // File trailer
    c+=fprintf(f,"\ntrailer\n");
    c+=fprintf(f, "<< /Size %i\n/Root 1 0 R >>\n",nobjects);
    c+=fprintf(f,"startxref\n");
    fprintf(f, "%u\n",xrefpos);
    c+=fprintf(f,"%%EOF\n");
    
    fclose(f);
    
    return (expression *) class_newobjectreference(obj);
}

void graphics_freeelement(graphics_element *element) {
    if (element) {
        switch (element->type) {
            case GRAPHICS_TEXT:
                if (((graphics_textelement *) element)->string) EVAL_FREE(((graphics_textelement *) element)->string);
                break;
            case GRAPHICS_SHADE_LATTICE:
                if (((graphics_shadelatticeelement *) element)->data) EVAL_FREE(((graphics_shadelatticeelement *) element)->data);
                break;
            case GRAPHICS_SHADE_TRIANGLES:
                if (((graphics_shadetriangleselement *) element)->data) EVAL_FREE(((graphics_shadetriangleselement *) element)->data);
            default:
                break;
        }
        EVAL_FREE(element);
    }
}

void graphics_serializetohashtable(graphics_object *gobj, hashtable *ht) {
    expression_list *list=newexpressionlist(NULL);
    expression *farg=NULL;
    
    if (list) {
        linkedlist *lst=list->list;
        
        for (linkedlistentry *e=gobj->list->first; e!=NULL; e=e->next) {
            graphics_element *el=(graphics_element *) e->data;
            
            switch (el->type) {
                case GRAPHICS_POINT:
                    farg=eval_listfromfloatarray(((graphics_pointelement *) el)->point, 2);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_POINT_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_MOVETO:
                    farg=eval_listfromfloatarray(((graphics_pointelement *) el)->point, 2);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_MOVETO_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_LINETO:
                    farg=eval_listfromfloatarray(((graphics_pointelement *) el)->point, 2);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_LINETO_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_SCALE:
                    farg=eval_listfromfloatarray(((graphics_pointelement *) el)->point, 2);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_SCALE_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_TRANSLATE:
                    farg=eval_listfromfloatarray(((graphics_pointelement *) el)->point, 2);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_TRANSLATE_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_ROTATE:
                    farg = newexpressionfloat(((graphics_rotateelement *) el)->angle);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_ROTATE_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_LINEWIDTH:
                    farg = newexpressionfloat(((graphics_linewidthelement *) el)->width);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_LINEWIDTH_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS_TEXT:
                {
                    graphics_textelement *tel = (graphics_textelement *) el;
                    expression* targs[4];
                    char *align, *valign;
                    char alignstring[32];
                    
                    targs[0] = (expression *) newexpressionstring(tel->string);
                    targs[1] = (expression *) eval_listfromfloatarray(tel->point, 2);
                    switch (tel->horizontal) {
                        case GRAPHICS_ALIGNLEFT: align = "left"; break;
                        case GRAPHICS_ALIGNRIGHT: align = "right"; break;
                        case GRAPHICS_ALIGNCENTER: align = "center"; break;
                        default: align=""; break;
                    }
                    sprintf(alignstring, "align=%s", align);
                    targs[2] = (expression *) newexpressionstring(alignstring);
                    switch (tel->vertical) {
                        case GRAPHICS_ALIGNTOP: valign = "top"; break;
                        case GRAPHICS_ALIGNBOTTOM: valign = "bottom"; break;
                        case GRAPHICS_ALIGNMIDDLE: valign = "middle"; break;
                        default: valign=""; break;
                    }
                    sprintf(alignstring, "valign=%s", valign);
                    targs[3] = (expression *) newexpressionstring(alignstring);
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_TEXT_SELECTOR), 4, targs, FALSE));
                }
                    break;
                case GRAPHICS_SETCOLOR:
                {
                    graphics_setcolorelement *scel = (graphics_setcolorelement *) el;
                    expression* scargs[3];
                    
                    scargs[0]=newexpressionfloat((double) scel->r);
                    scargs[1]=newexpressionfloat((double) scel->g);
                    scargs[2]=newexpressionfloat((double) scel->b);
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_SETCOLOR_SELECTOR), 3, scargs, FALSE));
                }
                    break;
                case GRAPHICS_STROKE:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_STROKE_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS_CLOSE:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_CLOSE_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS_FILL:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_FILL_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS_RESTORESTATE:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_RESTORESTATE_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS_SAVESTATE:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS_SAVESTATE_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS_FORCE_BBOX:
                    /* TODO */
                    break;
                case GRAPHICS_SHADE_LATTICE:
                    /* TODO */
                    break;
                case GRAPHICS_SHADE_TRIANGLES:
                    /* TODO */
                    break;
            }
        }
        
        hashinsert(ht, GRAPHICS_DISPLAYLIST, list);
    }
}

/*
 * Intrinsic function veneers
 */

expression *graphics_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_object *gobj=(graphics_object *) obj;
    
    gobj->list=linkedlist_new();
    
    return NULL;
}

expression *graphics_newgraphicsi(interpretcontext *context, int nargs, expression **args) {
    return (expression *) graphics_newgraphics();
}

expression *graphics_pointi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float p[2];
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }

    if (eval_listtofloatarray(list, p, 2)) {
        graphics_point((graphics_object *) obj, p);
        
    } else if (graphics_validate_pointlist(list,2)) {
        for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
            expression_list *lst=(expression_list *) e->data;
        
            if (eval_listtofloatarray(lst, p, 2)) graphics_point((graphics_object *) obj, p);
        }
    }
    
    if(nargs!=1) { linkedlist_free(list->list); EVAL_FREE(list); };
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_setcolori(object *obj, interpretcontext *context, int nargs, expression **args) {
    double col[3]={0.0,0.0,0.0};
    int success=TRUE;
    
    if (nargs==3) {
        for (unsigned int i=0; i<2; i++) {
            if (eval_isreal(args[i])) {
                col[i]= eval_floatvalue(args[i]);
            } else {
                success=FALSE;
            }
        }
    } else if (nargs==1) {
        expression_objectreference *ref=(expression_objectreference *) args[0];
        
        if (eval_isobjectref(ref)) {
            if (!color_rgbfromobject(context, ref->obj, 0, NULL, col)) {
                success=FALSE;
            }
        }
    }
    
    if (success) graphics_setcolor((graphics_object *) obj, (float) col[0], (float) col[1], (float) col[2]);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_linewidthi(object *obj, interpretcontext *context, int nargs, expression **args) {
    
    if ((nargs==1)&&(eval_isreal(args[0]))) {
        graphics_linewidth((graphics_object *) obj, (float) eval_floatvalue(args[0]));
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_movei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float coords[2];
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    }
    
    if (eval_listtofloatarray(list, coords, 2)) {
        graphics_moveto((graphics_object *) obj, coords);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_scalei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float coords[2];
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    }
    
    if (eval_listtofloatarray(list, coords, 2)) {
        graphics_scale((graphics_object *) obj, coords);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_translatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float coords[2];
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    }
    
    if (eval_listtofloatarray(list, coords, 2)) {
        graphics_translate((graphics_object *) obj, coords);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_rotatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    
    if (nargs==1) {
        if (eval_isreal(args[0])) {
            graphics_rotate((graphics_object *) obj, eval_floatvalue(args[0]));
        }
    }
    
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_linetoi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float coords[2];
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    }
    
    if (eval_listtofloatarray(list, coords, 2)) {
        graphics_lineto((graphics_object *) obj, coords);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_linei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }
    
    if (graphics_validate_pointlist(list,2)) {
        graphics_line((graphics_object *) obj, list->list, FALSE, FALSE);
    }
    
    if(nargs!=1) { linkedlist_free(list->list); EVAL_FREE(list); };
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_arrowi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }
    
    if (graphics_validate_pointlist(list,2)) {
//        graphics_arrow((graphics_object *) obj, list->list, FALSE, FALSE);
    }
    
    if(nargs!=1) { linkedlist_free(list->list); EVAL_FREE(list); };
    
    return (expression *) class_newobjectreference(obj);
}


expression *graphics_polygoni(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }
    
    if (graphics_validate_pointlist(list,2)) {
        graphics_line((graphics_object *) obj, list->list, TRUE, FALSE);
    }
    
    if(nargs!=1) { linkedlist_free(list->list); EVAL_FREE(list); };
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_filli(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    if (nargs==0) {
        graphics_fill((graphics_object *) obj);
    } else if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }
    
    if (list) if (graphics_validate_pointlist(list,2)) {
        graphics_line((graphics_object *) obj, list->list, FALSE, TRUE);
        
        if (nargs>1) { linkedlist_free(list->list); EVAL_FREE(list); };
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_texti(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs!=2) return NULL;
    if (args[0]->type!=EXPRESSION_LIST) { printf("text expects a pair of coordinates as the first argument.\n"); return NULL; }
    if (args[1]->type!=EXPRESSION_STRING) { printf("text expects a string as the first argument.\n");  return NULL; }
    float pt[2];
    
    eval_listtofloatarray((expression_list *) args[0],pt, 2);
    
    graphics_text((graphics_object *) obj, ((expression_string *)args[1])->value, pt, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNMIDDLE);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_closei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_closepath((graphics_object *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_strokei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_stroke((graphics_object *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_savestatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_savestate((graphics_object *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_restorestatei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_restorestate((graphics_object *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_shadelatticei(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* TODO */
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_shadetrianglesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* TODO */
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_setbboxi(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* TODO */
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics_bboxi(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_bbox bbox;
    expression *e[2];
    
    graphics_calculatebbox((graphics_object *) obj, &bbox);

    e[0]=eval_listfromfloatarray(bbox.p1, 2);
    e[1]=eval_listfromfloatarray(bbox.p2, 2);
    
    return (expression *) eval_listfromexpressionarray(e, 2);
}


expression *graphics_bboxtolist(graphics_bbox *bbox) {
    expression_list *p1, *p2;
    
    p1=newexpressionlistfromargs(2, newexpressionfloat((float) bbox->p1[0]), newexpressionfloat((float) bbox->p1[1]));
    p2=newexpressionlistfromargs(2, newexpressionfloat((float) bbox->p2[0]), newexpressionfloat((float) bbox->p2[1]));
    
    return (expression *) newexpressionlistfromargs(2, p1, p2);
}

expression *graphics_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(6);
    graphics_object *gobj=(graphics_object *) obj;
    expression *ret=NULL;
    
    if (ht) {
        graphics_serializetohashtable(gobj, ht);
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
        
    }
    
    return ret;
}

expression *graphics_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *exp;
    expression *ret=NULL;
    
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        /* The runtime environment passes an object that responds to the lookup protocol as the first argument. */
        expression_objectreference *hashobject = (expression_objectreference *) args[0];
        
        /* Verify that the object passed responds to 'lookup'. */
        if (class_objectrespondstoselector(hashobject->obj, EVAL_LOOKUPSELECTORLABEL)) {
            /* Call the selector if so */
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, GRAPHICS_DISPLAYLIST);
            if (eval_islist(exp)) {
                expression_list *list = (expression_list *) exp;
                
                if (list->list) {
                    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
                        expression_function *entry = (expression_function *) e->data;
                        if (eval_isfunction(entry)) {
                            if (class_objectrespondstoselector(obj, eval_stringvalue(entry->name))) {
                                freeexpression(class_callselectorwithargslist(context, obj, eval_stringvalue(entry->name), entry->nargs, entry->argslist));
                            }
                        }
                    }
                }
            }
            
            if (exp) freeexpression(exp);
            
        }
    }
    
    return ret;
}

expression *graphics_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics_object *gobj = (graphics_object *) obj;
    expression_objectreference *newref = (expression_objectreference *) graphics_newgraphics();
    
    if (newref) {
        graphics_object *nobj = (graphics_object *) newref->obj;
        
        if (gobj->list) {
            for (linkedlistentry *e=gobj->list->first; e!=NULL; e=e->next) {
                graphics_element *el = (graphics_element *) e->data;
                
                linkedlist_addentry(nobj->list, graphics_cloneentry(el));
            }
        }
    }
    
    return (expression *) newref;
}

expression *graphics_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (context==NULL) {
        graphics_object *gobj=(graphics_object *) obj;
        if (gobj) {
            if (gobj->list) {
                linkedlist_map(gobj->list, (linkedlistmapfunction *) graphics_freeelement);
                linkedlist_free(gobj->list);
            }
        }
        
    }
    return NULL;
}

/*
 * Initialization
 */

void graphicsinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(GRAPHICS_LABEL, class_lookup(EVAL_OBJECT), sizeof(graphics_object), 10);
    class_registerselector(cls, GRAPHICS_POINT_SELECTOR, FALSE, graphics_pointi);
    class_registerselector(cls, GRAPHICS_MOVETO_SELECTOR, FALSE, graphics_movei);
    class_registerselector(cls, "line", FALSE, graphics_linei);
    class_registerselector(cls, "polygon", FALSE, graphics_polygoni);
    class_registerselector(cls, GRAPHICS_SETCOLOR_SELECTOR, FALSE, graphics_setcolori);
    class_registerselector(cls, GRAPHICS_LINEWIDTH_SELECTOR, FALSE, graphics_linewidthi);
    class_registerselector(cls, GRAPHICS_TEXT_SELECTOR, FALSE, graphics_texti);
    class_registerselector(cls, "exporteps", FALSE, graphics_exporteps);
    class_registerselector(cls, "exportpdf", FALSE, graphics_exportpdf);
    
    class_registerselector(cls, GRAPHICS_SAVESTATE_SELECTOR, FALSE, graphics_savestatei);
    class_registerselector(cls, GRAPHICS_RESTORESTATE_SELECTOR, FALSE, graphics_restorestatei);
    class_registerselector(cls, GRAPHICS_SCALE_SELECTOR, FALSE, graphics_scalei);
    class_registerselector(cls, GRAPHICS_TRANSLATE_SELECTOR, FALSE, graphics_translatei);
    class_registerselector(cls, GRAPHICS_ROTATE_SELECTOR, FALSE, graphics_rotatei);
    
    class_registerselector(cls, GRAPHICS_LINETO_SELECTOR, FALSE, graphics_linetoi);
    
    class_registerselector(cls, GRAPHICS_CLOSE_SELECTOR, FALSE, graphics_closei);
    class_registerselector(cls, GRAPHICS_STROKE_SELECTOR, FALSE, graphics_strokei);
    class_registerselector(cls, GRAPHICS_FILL_SELECTOR, FALSE, graphics_filli);
    
    class_registerselector(cls, GRAPHICS_SHADE_LATTICE_SELECTOR, FALSE, graphics_shadelatticei);
    class_registerselector(cls, GRAPHICS_SHADE_TRIANGLES_SELECTOR, FALSE, graphics_shadetrianglesi);
    class_registerselector(cls, GRAPHICS_SET_BBOX_SELECTOR, FALSE, graphics_setbboxi);
    class_registerselector(cls, GRAPHICS_BBOX_SELECTOR, FALSE, graphics_bboxi);
    
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, graphics_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, graphics_deserializei);
    class_registerselector(cls, EVAL_CLONE, FALSE, graphics_clonei);
    
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, graphics_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, graphics_freei);
    
    intrinsic(GRAPHICS_LABEL, FALSE, graphics_newgraphicsi,NULL);
    
    /* Initialize other components of the graphics system. */
    graphics3dinitialize();
    colorinitialize();
    plotinitialize();
    plot3dinitialize();
}
