/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef functional_h
#define functional_h

#include "geometry.h"

/* Functional type */
typedef struct functional_object_s {
    CLASS_GENERICOBJECTDATA
    
    double prefactor;
    unsigned int grade;
    
    unsigned int flags;
    
    expression *target;
    
    idtablemapfunction *integrand;
    idtablemapfunction *force;
    idtablemapfunction *generalizedforce;
    idtablemapfunction *dependencies;
    
    expression_objectreference *field;
    expression *param;
} functional_object;

typedef struct {
    manifold_object *mobj;
    field_object *target;
    functional_object *fobj;
    selection_object *sobj; 
    field_object *field;
    field_object *dependencies; 
    interpretcontext *context;
} functionalmapref;

/* Classes */
#define GEOMETRY_FUNCTIONALCLASS "functional"

#define GEOMETRY_LEVELSETCLASS "levelset"
#define GEOMETRY_CENTRALPAIRWISECLASS "centralpairwise"
#define GEOMETRY_CONSTANTFORCECLASS "constantforce"
#define GEOMETRY_GRAVITYCLASS "gravity"
#define GEOMETRY_HARMONICFORCECLASS "harmonic"
#define GEOMETRY_LINETENSIONCLASS "linetension"
#define GEOMETRY_LINETENSIONCYLCLASS "linetensioncyl"
#define GEOMETRY_HOOKECLASS "hookeelasticity"
#define GEOMETRY_LINEARELASTICCLASS "linearelastic"
#define GEOMETRY_LINECURVSQCLASS "linecurvsq"
#define GEOMETRY_LINETORSIONSQCLASS "linetorsionsq"
#define GEOMETRY_ENCLOSEDAREACLASS "enclosedarea"
#define GEOMETRY_EQUILENGTHCLASS "equilength"
#define GEOMETRY_SURFACETENSIONCLASS "surfacetension"
#define GEOMETRY_ENCLOSEDVOLUMECLASS "enclosedvolume"
#define GEOMETRY_EQUIAREACLASS "equiarea"
#define GEOMETRY_EQUIAREAWEIGHTEDCLASS "equiareaweighted"
#define GEOMETRY_GAUSSCURVCLASS "gausscurv"
#define GEOMETRY_GAUSSSQCURVCLASS "gausssqcurv"
#define GEOMETRY_MEANCURVCLASS "meancurv"
#define GEOMETRY_MEANSQCURVCLASS "meansqcurv"
#define GEOMETRY_VERTEXNORMALCLASS "vertexnormal"
#define GEOMETRY_ELECTROSTATICCLASS "electrostatic"
#define GEOMETRY_FIELDHARMONICCLASS "fieldharmonic"
#define GEOMETRY_FIELDHARMONIC2CLASS "fieldharmonic2"
#define GEOMETRY_NEMATICCLASS "nematic"
#define GEOMETRY_NEMATICELECTRICCLASS "nematicelectric"
#define GEOMETRY_NEMATICCYLCLASS "nematiccyl"
#define GEOMETRY_NEMATICANCHORINGCLASS "nematicanchoring"
#define GEOMETRY_NEMATICANCHORINGNORMALCLASS "nematicanchoringnormal"
#define GEOMETRY_NEMATICANCHORINGCYLCLASS "nematicanchoringcyl"
#define GEOMETRY_FIELDNORMSQCLASS "fieldnormsq"
#define GEOMETRY_ANCHORINGCLASS "anchoring"
#define GEOMETRY_VOLUMECLASS "volume"
#define GEOMETRY_LINEARELASTIC3DCLASS "linearelastic3d"

/* Selector labels */
#define GEOMETRY_GRADE_SELECTOR "grade"
#define GEOMETRY_TOTAL_SELECTOR "total"
#define GEOMETRY_FORCE_SELECTOR "force"
#define GEOMETRY_INTEGRAND_SELECTOR "integrand"
#define GEOMETRY_SETINTEGRANDONLY_SELECTOR "setintegrandonly"
#define GEOMETRY_GENERALIZEDFORCE_SELECTOR "generalizedforce"

#define GEOMETRY_SETFORCE_SELECTOR "setforce"
#define GEOMETRY_GETFORCE_SELECTOR "getforce"
#define GEOMETRY_SETDIRECTION_SELECTOR "setdirection"
#define GEOMETRY_GETDIRECTION_SELECTOR "direction"

#define GEOMETRY_SETORIGIN_SELECTOR "setorigin"
#define GEOMETRY_GETORIGIN_SELECTOR "origin"
#define GEOMETRY_SETVALUE_SELECTOR "setvalue"
#define GEOMETRY_GETVALUE_SELECTOR "value"
/*#define GEOMETRY_SETPATH_SELECTOR "setpath"
#define GEOMETRY_GETPATH_SELECTOR "path"*/
#define GEOMETRY_SETPREFERREDVALUE_SELECTOR "setpreferredvalue"
#define GEOMETRY_GETPREFERREDVALUE_SELECTOR "preferredvalue"
#define GEOMETRY_SETREFERENCE_SELECTOR "setreference"
#define GEOMETRY_GETREFERENCE_SELECTOR "reference"
#define GEOMETRY_SETPOTENTIAL_SELECTOR "setfield"
#define GEOMETRY_GETPOTENTIAL_SELECTOR "field"
#define GEOMETRY_SETWEIGHT_SELECTOR "setweight"
#define GEOMETRY_GETWEIGHT_SELECTOR "weight"
#define GEOMETRY_SETFIELD_SELECTOR "setfield"
#define GEOMETRY_GETFIELD_SELECTOR "getfield"
#define GEOMETRY_SETCOORDINATES_SELECTOR "setcoordinates"
#define GEOMETRY_GETCOORDINATES_SELECTOR "coordinates"
#define GEOMETRY_SETGRADIENT_SELECTOR "setgradient"
#define GEOMETRY_GETGRADIENT_SELECTOR "gradient"
#define GEOMETRY_SETEXPRESSION_SELECTOR "setexpression"
#define GEOMETRY_GETEXPRESSION_SELECTOR "expression"
#define GEOMETRY_SETONESIDED_SELECTOR "setonesided"
#define GEOMETRY_GETONESIDED_SELECTOR "onesided"
#define GEOMETRY_SETCONSTANTS_SELECTOR "setconstants"
#define GEOMETRY_GETCONSTANTS_SELECTOR "constants"

#define GEOMETRY_TARGET_SELECTOR "target"
#define GEOMETRY_SETTARGET_SELECTOR "settarget"
#define GEOMETRY_PREFACTOR_SELECTOR "prefactor"
#define GEOMETRY_SETPREFACTOR_SELECTOR "setprefactor"

#define GEOMETRY_SETDIRECTOR_SELECTOR "setdirector"
#define GEOMETRY_GETDIRECTOR_SELECTOR "director"

#define NEMATIC_SPLAY "splay"
#define NEMATIC_TWIST "twist"
#define NEMATIC_BEND "bend"


/* General */
#define INTEGRANDONLY_FLAG                        (1<<2)
#define TUPLES_FLAG                               (1<<3)

/* Errors */
#define ERROR_FUNCTIONAL                           0x9500
#define ERROR_FUNCTIONAL_MSG                       "No integrand defined for this functional."

#define ERROR_ZEROELEMENT                          0x9501
#define ERROR_ZEROELEMENT_MSG                      "Element has zero modulus."

#define ERROR_NOFORCE                              0x9502
#define ERROR_NOFORCE_MSG                          "No force set."

#define ERROR_NOVALUE                              0x9502
#define ERROR_NOVALUE_MSG                          "No value set."

#define ERROR_NOFIELDVAL                           0x9503
#define ERROR_NOFIELDVAL_MSG                       "Selector 'setvalue' requires a field object as a parameter."

#define ERROR_NOFIELD                              0x9504
#define ERROR_NOFIELD_MSG                          "Selector 'setfield' requires a field object as a parameter."

#define ERROR_EMPTYSELECTION                       0x9505
#define ERROR_EMPTYSELECTION_MSG                   "Selection contains no elements of grade %u."

#define ERROR_COORDINATES                          0x9506
#define ERROR_COORDINATES_MSG                      "Coordinates should be a list of names."

#define ERROR_COORDINATESDIM                       0x9507
#define ERROR_COORDINATESDIM_MSG                   "Coordinates list doesn't match dimension of object."

#define ERROR_DIRECTION                            0x9508
#define ERROR_DIRECTION_MSG                        "No direction set."

#define ERROR_DIMENSION                            0x9509
#define ERROR_DIMENSION_MSG                        "This functional only works in %u dimensions."

#define ERROR_NOINDICES                            0x9510
#define ERROR_NOINDICES_MSG                        "Object must respond to indices selector."

#define ERROR_NONNUMERICAL                         0x9511
#define ERROR_NONNUMERICAL_MSG                     "Field entry evaluated to nonnumerical values."

#define ERROR_COORDINATESPW                        0x9512
#define ERROR_COORDINATESPW_MSG                    "Coordinate for centralpairwise should be a single name."

#define FUNCTIONAL_MAXENTRIES 20

/* Selectors */
expression *functional_newi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *functional_freei(object *obj, interpretcontext *context, int nargs, expression **args);

/* Intrinsic constructors */

/* Initialization */
void functionalinitialize(void);

/* Veneers */
int functional_isonesided(expression_objectreference *func, interpretcontext *context);
int functional_gettarget(object *obj, interpretcontext *context, MFLD_DBL *target);

#endif /* functional_h */

