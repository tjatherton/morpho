/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_calculus_h
#define Morpho_calculus_h

/*
 * Calculus package
 */

#include "eval.h"

/*
 * Functions that implement differentiation for functions
 */

expression *eval_diffsin(interpretcontext *context, expression *exp, char *symbol);
expression *eval_diffcos(interpretcontext *context, expression *exp, char *symbol);
expression *eval_diffexp(interpretcontext *context, expression *exp, char *symbol);

/*
 * Functions that implement differentiation for operators
 */

expression *eval_diffaddop(interpretcontext *context, expression *op, char *symbol);
expression *eval_diffsubop(interpretcontext *context, expression *exp, char *symbol);
expression *eval_diffmulop(interpretcontext *context, expression *exp, char *symbol);
expression *eval_diffdivop(interpretcontext *context, expression *exp, char *symbol);
expression *eval_diffpowop(interpretcontext *context, expression *exp, char *symbol);

/* 
 * Interface functions
 */

expression *differentiate(interpretcontext *context, expression *exp, char *symbol);
expression *eval_differentiatei(interpretcontext *context, int nargs, expression **args);

void evalinitializecalculus(void);

#endif
