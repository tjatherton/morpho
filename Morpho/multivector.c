/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "multivector.h"

#include CBLAS_INCLUDE

classintrinsic *multivectorclass=NULL;

/* 
 * Code to abstract away particular representations of multivectors
 */

/* Determine the size of storage required for a single element of a multivector of specified type */
size_t multivector_sizefortype(multivectortype type) {
    size_t size=0;
    
    switch (type) {
        case MULTIVECTOR_DOUBLES:
            size=sizeof(double);
            break;
        case MULTIVECTOR_EXPRESSIONS:
            size=sizeof(expression*);
        default:
            break;
    }
    
    return size;
}

/* Determine the total number of elements in a multivector of a given dimension */
#define multivector_lengthfordimension(dim) (1<<((unsigned int) dim))

/* Fills out two arrays (that must have size dim+1) with information about where elements of each grade are
   to be found in a multivector with dimension dim. */
void multivector_gradearray(unsigned int dim, unsigned int *startindices, unsigned int *lengths) {
    unsigned int clength=1;
    
    /* First fill out the lengths */
    for (unsigned int i=0; i<dim/2+1; i++) {
        lengths[i]=clength;
        lengths[dim-i]=clength;
        
        if (clength==1) clength=dim;
        else clength=((clength*(clength-1))/2);
    }
    
    /* Now fill out the positions */
    if (startindices) for (unsigned int i=0, clength=0; i<=dim; i++) {
        startindices[i]=clength;
        clength+=lengths[i];
    }
}

/* Zero an element of a multivector. Warning: No bounds checking performed here! */
void multivector_zeroelement(multivectorobject *obj, unsigned int i) {
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            if (obj->data) ((double *) obj->data)[i]=(double) 0;
            break;
        case MULTIVECTOR_EXPRESSIONS:
            if (obj->data) ((expression **) obj->data)[i]=NULL;
        default:
            break;
    }
}

/* Prints an element */
void multivector_printelement(multivectorobject *obj, unsigned int i) {
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            if (obj->data) printf("%g",((double *) obj->data)[i]);
            break;
        case MULTIVECTOR_EXPRESSIONS:
            if (obj->data) expressionprint(((expression **) obj->data)[i]);
        default:
            break;
    }
}

/* Projects a multivector onto a particular grade */
expression *multivector_projectontograde(multivectorobject *mobj, unsigned int i) {
    expression *exp=multivector_new(mobj->type, mobj->dimension);
    unsigned int startindex[mobj->dimension+1];
    unsigned int lengths[mobj->dimension+1];
    
    multivector_gradearray(mobj->dimension, startindex, lengths);
    
    if (exp) {
        multivectorobject *newobj = (multivectorobject *) ((expression_objectreference *)exp)->obj;
        double *src = mobj->data;
        double *dest = newobj->data;
        
        memcpy(dest+startindex[i], src+startindex[i], lengths[i]*sizeof(double));
    }
    
    return exp;
}

/* Returns a particular grade of a multivector */
expression *multivector_getgradeaslist(multivectorobject *obj, unsigned int i) {
    unsigned int lengths[obj->dimension+1];
    unsigned int startindex[obj->dimension+1];
    multivector_gradearray(obj->dimension, startindex, lengths);
    expression *ret = NULL;
    
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            if (lengths[i]==1) {
                ret = newexpressionfloat(((double *) obj->data)[startindex[i]]);
            } else {
                ret = eval_listfromdoublearray(((double *) obj->data)+startindex[i], lengths[i]);
            }
            break;
        case MULTIVECTOR_EXPRESSIONS:
            if (lengths[i]==1) {
                ret = cloneexpression(((expression **) obj->data)[startindex[i]]);
            } else {
                ret = eval_listfromexpressionarray(((expression **) obj->data)+startindex[i], lengths[i]);
            }
            break;
        default:
            break;
    }
    
    return ret;
}

/* Returns a component j of grade i of a multivector */
expression *multivector_getelement(multivectorobject *obj, unsigned int i, unsigned int j) {
    unsigned int lengths[obj->dimension+1];
    unsigned int startindex[obj->dimension+1];
    multivector_gradearray(obj->dimension, startindex, lengths);
    expression *ret = NULL;
    
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            ret = newexpressionfloat(((double *) obj->data)[startindex[i]+j]);
            break;
        case MULTIVECTOR_EXPRESSIONS:
            ret = cloneexpression(((expression **) obj->data)[startindex[i]+j]);
            break;
        default:
            break;
    }
    
    return ret;
}

/* Promotes a multivector */
int multivector_promote(multivectorobject *obj, interpretcontext *context) {
    unsigned int dim = obj->dimension;
    unsigned int length = multivector_lengthfordimension(dim);
    size_t size=length*sizeof(expression**);
    expression **new=EVAL_MALLOC(size);
    double *old = (double *) obj->data;
    int ret=FALSE;
    
    if (new) {
        for (unsigned int i=0; i<length; i++) {
            new[i]=newexpressionfloat(old[i]);
        }
        
        EVAL_FREE(obj->data); /* Destroy old data */
        obj->type=MULTIVECTOR_EXPRESSIONS;
        obj->size=size;
        obj->data=new; /* Patch in new data */
        
        ret=TRUE;
    } else {
        error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
    }
    
    return ret;
}

/* Sets a particular grade of a multivector */
void multivector_setgradetoexpression(multivectorobject *obj, interpretcontext *context, unsigned int i, expression *exp) {
    unsigned int lengths[obj->dimension+1];
    unsigned int startindex[obj->dimension+1];
    multivector_gradearray(obj->dimension, startindex, lengths);
    
    /* Check compatibility of shape */
    if ( ((lengths[i]==1)&&(eval_islist(exp))) ||
         ((lengths[i]>1) &&
            ( (!eval_islist(exp)) ||
              ((eval_islist(exp))&&(eval_listlength((expression_list *) exp)!=lengths[i]))
            )
         ) ) {
        error_raise(context, ERROR_MULTIVECTOR_INCOMPATIBLESHAPE, ERROR_MULTIVECTOR_INCOMPATIBLESHAPE_MSG, ERROR_FATAL);
    }
    
    /* Check if we need to promote the multivector */
    if (obj->type==MULTIVECTOR_DOUBLES) if ( !((lengths[i]==1)&&(eval_isreal(exp))) &&
        !((lengths[i]>1)&&(eval_islist(exp))&&(multivector_isnumericallist((expression_list *) exp))) ) {
        multivector_promote(obj,context);
    }
    
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            if (lengths[i]==1) {
                if (eval_isreal(exp)) {
                    ((double *) obj->data)[startindex[i]] = eval_floatvalue(exp);
                }
            } else {
                if (eval_islist(exp) && multivector_isnumericallist((expression_list *) exp)) {
                    eval_listtodoublearray((expression_list *) exp, ((double *) obj->data) + startindex[i], lengths[i]);
                }
            }
            break;
        case MULTIVECTOR_EXPRESSIONS:
        {
            expression **data = (expression **) obj->data;
            if (lengths[i]==1) {
                freeexpression(data[startindex[i]]);
                data[startindex[i]] = cloneexpression(exp);
                
            } else {
                for (unsigned int j=0; j<lengths[i]; j++) freeexpression(data[startindex[i]+j]);
                eval_listtoexpressionarray((expression_list *) exp, data+startindex[i], lengths[i]);
            }
        }
            break;
        default:
            break;
    }
}

void multivector_scale(multivectorobject *obj, interpretcontext *context, double scale) {
    double *data=NULL;
    unsigned int length = multivector_lengthfordimension(obj->dimension);
    
    if (obj) {
        switch (obj->type) {
            case MULTIVECTOR_DOUBLES:
                data = (double * ) obj->data;
                for (unsigned int i=0; i<length; i++) data[i] *= scale;
                break;
            default:
                /* TODO: EXPRESSIONS NOT IMPLEMENTED */
                break;
        }
    }
}


void multivector_setelementtoexpression(multivectorobject *obj, interpretcontext *context, unsigned int i, unsigned int j, expression *exp) {
    unsigned int lengths[obj->dimension+1];
    unsigned int startindex[obj->dimension+1];
    multivector_gradearray(obj->dimension, startindex, lengths);

    /* Bounds check */
    if (j>=lengths[i]) {
        error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
    }
    
    /* Check if we need to promote the multivector */
    if ((obj->type==MULTIVECTOR_DOUBLES)&&(!eval_isreal(exp))) {
        multivector_promote(obj,context);
    }
    
    switch (obj->type) {
        case MULTIVECTOR_DOUBLES:
            ((double *) obj->data)[startindex[i]+j] = eval_floatvalue(exp);
            break;
        case MULTIVECTOR_EXPRESSIONS:
            freeexpression(((expression **) obj->data)[startindex[i]+j]);
            ((expression **) obj->data)[startindex[i]+j] = cloneexpression(exp);
            break;
        default:
            break;
    }
}

/*
 * Multivector operations
 */

expression *multivector_new(multivectortype type, unsigned int dimension) {
    multivectorobject *obj=NULL;
    expression_objectreference *ref=NULL;
    multivectorlength length = multivector_lengthfordimension(dimension);
    
    /* Lightweight instantiation for performance reasons */
    obj=(multivectorobject *) class_lwinstantiate((classgeneric *) multivectorclass);
    multivector_newi((object *) obj, NULL, 0, NULL);
    
    if (obj) {
        obj->type=type;
        obj->dimension=dimension;
        obj->size=length*multivector_sizefortype(type);
        
        if (obj->size>0) {
            obj->data=EVAL_MALLOC(obj->size);
            
            if (type==MULTIVECTOR_DOUBLES) {
                memset(obj->data, 0, obj->size);
            } else {
                for (unsigned int i=0; i<length; i++) {
                    multivector_zeroelement(obj, i);
                }
            }
        }
        
        ref=class_newobjectreference((object *) obj);
    }
    
//    printf("Multivector %p created.\n", obj);
    
    return (expression *) ref;
}

expression *multivector_clone(multivectorobject *mobj) {
    expression_objectreference *new;
    
    new=(expression_objectreference *) multivector_new(mobj->type, mobj->dimension);
    
    if (new) {
        multivectorobject *nobj=(multivectorobject *) new->obj;
        
        switch (mobj->type) {
            case MULTIVECTOR_DOUBLES:
                memcpy(nobj->data, mobj->data, mobj->size);
                break;
            case MULTIVECTOR_EXPRESSIONS:
                {
                    expression **old = (expression **) mobj->data;
                    expression **new = (expression **) nobj->data;
                
                    for (unsigned int i=0; i<multivector_lengthfordimension(mobj->dimension); i++) {
                        new[i]=cloneexpression(old[i]);
                    }
                }
                break;
            case MULTIVECTOR_NONE:
                break;
        }
    }
    
    return (expression *) new;
}

void multivector_print(multivectorobject *mobj) {
    if (!mobj) return;
    
    unsigned int startindex[mobj->dimension+1];
    unsigned int lengths[mobj->dimension+1];
    
    multivector_gradearray(mobj->dimension, startindex, lengths);
    
    printf("{");
    for (unsigned int i=0; i<mobj->dimension+1; i++) {
        if (lengths[i]>1) printf("{");
        for (unsigned int j=0;j<lengths[i];j++) {
            multivector_printelement(mobj, startindex[i]+j);
            if (j<lengths[i]-1) printf(", ");
        }
        if (lengths[i]>1) printf("}");
        if (i<mobj->dimension) printf(", ");
    }
    printf("}\n");
}

/* Tests if a list and any sublists purely contain real values */
int multivector_isnumericallist(expression_list *list) {
    for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
        expression *el=(expression *) e->data;
        
        if (eval_islist(el)) {
            if(!multivector_isnumericallist((expression_list *) el)) return FALSE;
        } else if (!eval_isreal(el)) return FALSE;
    }
    
    return TRUE;
}

/* Tests if a list has compatible shape to represent a multivector of dimension dim */
int multivector_iscompatiblelist(expression_list *list, unsigned int dim) {
    unsigned int lengths[dim+1];
    unsigned int i;
    linkedlistentry *e=NULL;
    
    multivector_gradearray(dim, NULL, lengths);
    
    for (i=0, e=list->list->first; e!=NULL; e=e->next, i++) {
        expression *el=(expression *) e->data;
        
        if (lengths[i]==1) {
            /* If this grade contains a single element, it should not be a list */
            if (eval_islist(el)) return FALSE;
        } else {
            /* Otherwise, check it is a list... */
            if (!eval_islist(el)) return FALSE;
            /* ... and one of the right length */
            if (eval_listlength((expression_list *) el)!=lengths[i]) return FALSE;
        }
    }
    
    return TRUE;
}

/* Turns a list into a multivector. */
expression *multivector_listtomultivector(interpretcontext *context, expression_list *list, multivectorobject *obj) {
    unsigned int nels=eval_listlength(list);
    expression *ret=NULL;
    unsigned int dim=nels-1;
    unsigned int lengths[nels];
    unsigned int startindex[nels];
    unsigned int i; linkedlistentry *e=NULL;
    multivectorobject *mobj;
    
    multivector_gradearray(dim, startindex, lengths);
    
    if(nels>0) {
        
        if (multivector_iscompatiblelist(list, dim)) {
            /* Create the multivector */
            if (multivector_isnumericallist(list)) {
                ret=multivector_new(MULTIVECTOR_DOUBLES, dim);
            } else {
                ret=multivector_new(MULTIVECTOR_EXPRESSIONS, dim);
            }
            
            if (ret) {
                mobj = (multivectorobject *) ((expression_objectreference *)ret)->obj;
                
                for (i=0, e=list->list->first; e!=NULL; e=e->next, i++) {
                    expression *el=(expression *) e->data;
                    
                    if (lengths[i]==1) {
                        if(mobj->type==MULTIVECTOR_DOUBLES) {
                            ((double *) mobj->data)[startindex[i]]=eval_floatvalue(el);
                        } else if(mobj->type==MULTIVECTOR_EXPRESSIONS) {
                            ((expression **) mobj->data)[startindex[i]]=cloneexpression(el);
                        }
                    } else {
                        if(mobj->type==MULTIVECTOR_DOUBLES) {
                            eval_listtodoublearray((expression_list *) el, ((double *) mobj->data)+startindex[i], lengths[i]);
                        } else if (mobj->type==MULTIVECTOR_EXPRESSIONS) {
                            eval_listtoexpressionarray((expression_list *) el, ((expression **) mobj->data)+startindex[i], lengths[i]);
                        }
                    }
                }
            }

        } else {
            error_raise(context, ERROR_MULTIVECTOR_INCOMPATIBLESHAPE, ERROR_MULTIVECTOR_INCOMPATIBLESHAPE_MSG, ERROR_FATAL);
        }
    }
    
    /* Patch the data into an existing multivector */
    if (obj && mobj) {
        /* Free any existing data */
        if ((obj->type==MULTIVECTOR_EXPRESSIONS)&&(obj->data)) {
            expression **data = (expression **) obj->data;
            for(i=0; i<multivector_lengthfordimension(dim); i++) EVAL_FREE(data[i]);
        }
        if (obj->data) EVAL_FREE(obj->data);
        
        /* Now copy everything over */
        obj->type=mobj->type;
        obj->size=mobj->size;
        obj->data=mobj->data;
        obj->dimension=mobj->dimension;
        
        /* Now get rid of the temporary multivector */
        mobj->data=NULL;
        EVAL_FREE(ret);

        /* And return a reference to the existing object */
        ret=(expression *) class_newobjectreference((object *) obj);
    }
    
    return (expression *) ret;
}

/* Constructs a multivector containing a single grade from a list */
expression *multivector_doublelisttomultivector(interpretcontext *context, unsigned int dim, unsigned int grade, double *x, unsigned int length) {
    expression_objectreference *mref;
    multivectorobject *mobj=NULL;
    unsigned int startindices[dim+1];
    unsigned int lengths[dim+1];
    
    mref=(expression_objectreference *) multivector_new(MULTIVECTOR_DOUBLES, dim);
    if (mref) {
        mobj=(multivectorobject *) mref->obj;
        
        multivector_gradearray(dim, startindices, lengths);
        if (length==lengths[grade]) memcpy(((double *) mobj->data)+startindices[grade], x, sizeof(double)*lengths[grade]);
    }
    
    return (expression *) mref;
}

/* Constructs a unit multivector */
expression *multivector_unitmultivector(interpretcontext *context, unsigned int dim, unsigned int grade, unsigned int el) {
    expression_objectreference *mref;
    multivectorobject *mobj=NULL;
    unsigned int startindices[dim+1];
    unsigned int lengths[dim+1];
    
    mref=(expression_objectreference *) multivector_new(MULTIVECTOR_DOUBLES, dim);
    if (mref) {
        mobj=(multivectorobject *) mref->obj;
        double *x=(double *) mobj->data;
        
        multivector_gradearray(dim, startindices, lengths);
        if (el<lengths[grade]) x[startindices[grade]+el]=1.0;
    }
    
    return (expression *) mref;
}


/* Retrieves a particular grade from a multivector */
int multivector_gradetodoublelist(multivectorobject *mobj, unsigned int grade, double *x) {
    if (!mobj || mobj->type!=MULTIVECTOR_DOUBLES) return FALSE;
    
    unsigned int startindices[mobj->dimension+1];
    unsigned int lengths[mobj->dimension+1];
    multivector_gradearray(mobj->dimension, startindices, lengths);
    double *v = (double *) mobj->data;
    
    for (unsigned int i=0; i<lengths[grade]; i++) {
        x[i]=v[startindices[grade]+i];
    }
    
    return TRUE;
}

expression_list *multivector_tolist(multivectorobject *mobj) {
    expression_list *list=newexpressionlist(NULL);
    
    unsigned int lengths[mobj->dimension+1];
    unsigned int startindex[mobj->dimension+1];
    
    multivector_gradearray(mobj->dimension, startindex, lengths);
    
    if (list) {
        linkedlist *lst=list->list;
        
        switch (mobj->type) {
            case MULTIVECTOR_DOUBLES:
            {   double *data = (double *) mobj->data;
                for (unsigned int i=0; i<mobj->dimension+1; i++) {
                    if (lengths[i]==1) {
                        linkedlist_addentry(lst, newexpressionfloat(data[startindex[i]]));
                    } else {
                        linkedlist_addentry(lst, eval_listfromdoublearray(data+startindex[i], lengths[i]));
                    }
                }
            }
                break;
            case MULTIVECTOR_EXPRESSIONS:
            {   expression **data = (expression **) mobj->data;
                for (unsigned int i=0; i<mobj->dimension+1; i++) {
                    if (lengths[i]==1) {
                        linkedlist_addentry(lst, cloneexpression(data[startindex[i]]));
                    } else {
                        linkedlist_addentry(lst, eval_listfromexpressionarray(data+startindex[i], lengths[i]));
                    }
                }
            }
                
                break;
            default:
                break;
        }
    }
    
    return list;
}

void multivector_serializetohashtable(multivectorobject *mobj, hashtable *ht) {
    if (!mobj) return;
    expression_list *list=NULL;
    
    list=multivector_tolist(mobj);
    
    if (list) {
        hashinsert(ht, GEOMETRY_MULTIVECTORSERIALIZATIONLABEL, list);
    }
}

expression *multivector_adds(interpretcontext *context, multivectorobject *a, multivectorobject *b, int subtract, double scale, multivectorobject *out) {
    multivectorobject *mat = out;
    expression *ret=NULL;
    
    if ((a)&&(b)&&(a->dimension==b->dimension)) {
        if (out) {
            if (!(out->dimension==b->dimension)) {
                error_raise(context, ERROR_MULTIVECTOR_DIFFDIM, ERROR_MULTIVECTOR_DIFFDIM_MSG, ERROR_FATAL);
            }
        } else {
            /* TODO: Type checking */
            /* TODO: Only calculate necessary elements for efficiency! */
            ret=multivector_clone(a);
            
            if (ret) {
                mat=(multivectorobject *) ((expression_objectreference *) ret)->obj;
            }
        }
        
        if (mat) {
            double sc=scale;
            if (subtract) sc=-sc;
            
            memcpy(mat->data, a->data, a->size); /* TODO THIS WON'T WORK if the sizing is different! */
            cblas_daxpy(multivector_lengthfordimension(a->dimension), sc, b->data, 1, mat->data, 1);
        }
        
    } else {
        error_raise(context, ERROR_MULTIVECTOR_DIFFDIM, ERROR_MULTIVECTOR_DIFFDIM_MSG, ERROR_FATAL);
    }
    
    return ret;
}

/* Totals a list of multivectors */
expression *multivector_total(interpretcontext *context, expression_objectreference **mv, unsigned int n) {
    expression *ret=NULL;
    if (mv[0]) ret=multivector_clone((multivectorobject *) eval_objectfromref(mv[0]));
    
    if (ret) for (unsigned int i=1; i<n; i++) {
        if (mv[i]) multivector_add(context, (multivectorobject *) eval_objectfromref(ret), (multivectorobject *) eval_objectfromref(mv[i]), FALSE, (multivectorobject *) eval_objectfromref(ret));
    }
    
    return ret;
}

/* Totals a list of multivectors with weights */
expression *multivector_weightedtotal(interpretcontext *context, expression_objectreference **mv, double *weight, unsigned int n) {
    expression *ret=NULL;
    if (mv[0]) ret=multivector_clone((multivectorobject *) eval_objectfromref(mv[0]));
    
    multivector_scale((multivectorobject *) eval_objectfromref(ret), context, weight[0]);
    
    if (ret) for (unsigned int i=1; i<n; i++) {
        if (mv[i]) multivector_adds(context, (multivectorobject *) eval_objectfromref(ret), (multivectorobject *) eval_objectfromref(mv[i]), FALSE, weight[i], (multivectorobject *) eval_objectfromref(ret));
    }
    
    return ret;
}

/* Totals a list of multivectors with weights, storing the result in an existing multivector that is overwritten. Warning: no checking is performed for speed. */
void multivector_weightedtotalinplace(interpretcontext *context, expression_objectreference **mv, double *weight, unsigned int n, expression_objectreference *out) {
    multivectorobject *mvout = (multivectorobject *) eval_objectfromref(out);
    /* Copy data from the first multivector into the second */
    multivectorobject *mv1 = (multivectorobject *) eval_objectfromref(mv[0]);
    memcpy(mvout->data, mv1->data, mv1->size);
    multivector_scale(mvout, context, weight[0]);
    
    for (unsigned int i=1; i<n; i++) {
        if (mv[i]) multivector_adds(context, mvout, (multivectorobject *) eval_objectfromref(mv[i]), FALSE, weight[i], mvout);
    }
}

/* TODO Temporary multiplication tables */
static int mul2d[16] = {
    1,  2,  3,  4,
    2,  1,  4,  3,
    3, -4,  1, -2,
    4, -3,  2, -1
};

static int mul3d[64] = {
    1,    2,    3,    4,    5,    6,    7,    8,
    2,    1,    5,   -7,    3,    8,   -4,    6,
    3,   -5,    1,    6,   -2,    4,    8,    7,
    4,    7,   -6,    1,    8,   -3,    2,    5,
    5,   -3,    2,    8,   -1,   -7,    6,   -4,
    6,    8,   -4,    3,    7,   -1,   -5,   -2,
    7,    4,    8,   -2,   -6,    5,   -1,   -3,
    8,    6,    7,    5,   -4,   -2,   -3,   -1
};

expression *multivector_multiply(interpretcontext *context, multivectorobject *a, multivectorobject *b, int *signature, multivectorobject *out) {
    multivectorobject *mat = out;
    expression *ret=NULL;

    unsigned int i;
    
    /* TODO: Only calculate necessary elements for efficiency! */
    if ((a)&&(b)&&(a->dimension==b->dimension)) {
        if (out) {
            if (!(out->dimension==b->dimension)) {
                error_raise(context, ERROR_MULTIVECTOR_DIFFDIM, ERROR_MULTIVECTOR_DIFFDIM_MSG, ERROR_FATAL);
            }
        } else {
            /* TODO: Type checking */
            /* TODO: Only calculate necessary elements for efficiency! */
            ret=multivector_clone(a);
            
            if (ret) {
                mat=(multivectorobject *) ((expression_objectreference *) ret)->obj;
            }
        }
        
        if (mat) {
            unsigned int size=multivector_lengthfordimension(a->dimension);
            double temp[size*size];
            double *out = (double *) mat->data;
            
            /* Zero the temporary array */
            for (i=0; i<size*size; i++) temp[i]=0.0;
            
            /* Form the outer product */
            cblas_dger(CblasRowMajor, size, size, 1.0, a->data, 1, b->data, 1, temp, size);
            
            /* Print for diagnosis purposes */
            /*for (i=0; i<size; i++) {
                for (unsigned int j=0; j<size; j++) {
                    printf("%g ",temp[size*i+j]);
                }
                printf("\n");
            }*/
            
            /*  1     2     3     4     5     6     7     8
             
                X     1     2     3     12    23    31    123
                
            1   1     11    12    13    112   123   131   1123
                1     X     12    13    2     123   -3    23
             =  2     1     5     -7    3     8     -4    6
             
            2   2     21    22    23    212   223   231   2123
                2    -12    X     23    -1    3     123   -13
             =  3    -5     1     6     -2    4     8     7
             
            3   3     31    32    33    312   323   331   3123
                3    -13   -23    X     123   -2    1     12
             =  4     7    -6     1     8     -3    2     5
             
            12  12    121   122   123   1212  1223  1231  12123
                12    -2    1     123   -1122 13    23    -11223
                12    -2    1     123   -X    -31   23    -3
                5     -3    2     8     -1    -7    6     -4
            
            23  23    231   232   233   2312  2323  2331  23123
                23    123   -3    2     31    -2233 21    313
                23    123   -3    2     31    -X    -12   -1
                6     8     -4    3     7     -1    -5    -2
             
            31  31    311   312   313   3112  3123  3131  31123
                31    3     123   -1    32    12    -3311 323
                31    3     123   -1   -23    12    -X    -2
                7     4     8     -2    -6    5     -1    -3
             
           123  123   1231  1232  1233  12312 12323 12331 123123
                123   23    -13   12    232   -133  121   -X
                123   23    31    12    -3    -1    -2    -X
                8     6     7     5     -4    -2    -3    -1 */
             
            int *mul=NULL;
            
            if (a->dimension==2) mul=mul2d;
            else if (a->dimension==3) mul=mul3d;
            
            for (i=0; i<size; i++) out[i]=0.0;
            if (mul) for (i=0; i<size*size; i++) {
                if (mul[i]>0) {
                    out[mul[i]-1] += temp[i];
                } else {
                    out[-mul[i]-1] -= temp[i];
                }
            }
        }
            
    } else {
        error_raise(context, ERROR_MULTIVECTOR_DIFFDIM, ERROR_MULTIVECTOR_DIFFDIM_MSG, ERROR_FATAL);
    }
    
    return ret;
}

expression *multivector_dual(multivectorobject *mobj, interpretcontext *context) {
    expression *ret=NULL;
    unsigned int size=multivector_lengthfordimension(mobj->dimension);
    double *new=NULL;
    double *old=(double *) mobj->data;
    int *indx=NULL;
    unsigned int i;
    
    if (mobj->dimension==2) indx=mul2d+(size-1)*size;
    else if (mobj->dimension==3) indx=mul3d+(size-1)*size;
    
    if (indx) {
        ret=multivector_clone(mobj);
        if (eval_isobjectref(ret)) {
            multivectorobject *newobj = (multivectorobject *) eval_objectfromref(ret);
            new=newobj->data;
            for (i=0; i<size; i++) new[abs(indx[i])-1] = (indx[i]<0 ? -old[i] : old[i]);
        }
    }
    
    return ret;
}

/* Finds the norm of a specific grade  */
expression *multivector_gradenormsq(multivectorobject *a, unsigned int grade) {
    unsigned int startindices[a->dimension+1];
    unsigned int lengths[a->dimension+1];
    multivector_gradearray(a->dimension, startindices, lengths);
    expression *ret =NULL;
    
    switch (a->type) {
        case MULTIVECTOR_DOUBLES:
        {
            double total=0.0;
            double *x=(double *) a->data;
            
            for (unsigned int i=startindices[grade]; i<startindices[grade]+lengths[grade]; i++) {
                total+=x[i]*x[i];
            }
            
            ret=newexpressionfloat(total);
        }
        default:
            break;
    }
    
    return ret;
}

/* Finds the norm of a specific grade  */
expression *multivector_gradenorm(multivectorobject *a, unsigned int grade, double scale) {
    unsigned int startindices[a->dimension+1];
    unsigned int lengths[a->dimension+1];
    multivector_gradearray(a->dimension, startindices, lengths);
    expression *ret =NULL;
    
    switch (a->type) {
        case MULTIVECTOR_DOUBLES:
        {
            double total=0.0;
            double *x=(double *) a->data;
            
            for (unsigned int i=startindices[grade]; i<startindices[grade]+lengths[grade]; i++) {
                total+=x[i]*x[i];
            }
            
            ret=newexpressionfloat(scale*sqrt(total));
        }
        default:
            break;
    }
    
    return ret;
}

expression *multivector_subtractcomponent(interpretcontext *context, expression *m1, expression *m2) {
    expression *m12=NULL, *m22=NULL, *m2s=NULL, *out=NULL;
    double fg=0.0, gg=0.0;
    m12=opmul(context, m1, m2);
    m22=opmul(context, m2, m2);
 
    if (eval_isobjectref(m12) && eval_isobjectref(m22)) {
        multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(m12), 0, &fg);
        multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(m22), 0, &gg);
        
        m2s = multivector_clone((multivectorobject *) eval_objectfromref(m2));
        
        multivector_scale((multivectorobject *) eval_objectfromref(m2s), context, fg/gg);
        
        out = opsub(context, m1, m2s);
    } else if (eval_isobjectref(m12)) {
        out = multivector_clone((multivectorobject *) eval_objectfromref(m12));
    }
    
    if (m12) freeexpression(m12);
    if (m22) freeexpression(m22);
    if (m2s) freeexpression(m2s);
    
    return out;
}

/*
 * Selectors
 */

/* Initializes the multivector */
expression *multivector_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj = (multivectorobject *) obj;
    
    mobj->data=NULL;
    mobj->dimension=0;
    mobj->size=0;
    mobj->type=MULTIVECTOR_DOUBLES;
    
    return NULL;
}

/* Frees the multivector */
expression *multivector_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj = (multivectorobject *) obj;
    
//    printf("Multivector %p being freed.\n", obj);
    
    if (mobj->data) EVAL_FREE(mobj->data);
    
    return NULL;
}

/* Clones a multivector */
expression *multivector_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    
    return multivector_clone((multivectorobject *) obj);
}

/* Prints a multivector */
expression *multivector_printi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivector_print((multivectorobject *) obj);
    
    return (expression *) class_newobjectreference(obj);
}

/* Serializes a multivector */
expression *multivector_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(2);
    multivectorobject *mobj=(multivectorobject *) obj;
    expression *ret=NULL;
    
    if (ht) {
        multivector_serializetohashtable(mobj, ht);
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
    }
    
    return ret;
}

/* Deserializes a multivector */
expression *multivector_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj=(multivectorobject *) obj;
    expression *exp;
    expression *ret=NULL;
    
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        /* The runtime environment passes an object that responds to the lookup protocol as the first argument. */
        expression_objectreference *hashobject = (expression_objectreference *) args[0];
        
        /* Verify that the object passed responds to 'lookup'. */
        if (class_objectrespondstoselector(hashobject->obj, EVAL_LOOKUPSELECTORLABEL)) {
            /* Call the selector if so */
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, GEOMETRY_MULTIVECTORSERIALIZATIONLABEL);
            if (eval_islist(exp)) {
                multivector_listtomultivector(context, (expression_list *) exp, mobj);
            }
            
            if (exp) freeexpression(exp);
            
        }
    }
    
    return ret;
    
}

/* Turn the multivector into a list */
expression *multivector_tolisti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    list = multivector_tolist((multivectorobject *) obj);
    
    return (expression *) list; 
}

/* Gets the vector element at a particular index */
expression *multivector_indexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (!obj) return NULL;
    multivectorobject *mobj = (multivectorobject *) obj;
    unsigned int i,j;
    expression *ret = NULL;
    
    unsigned int lengths[mobj->dimension+1];
    unsigned int startindex[mobj->dimension+1];
    multivector_gradearray(mobj->dimension, startindex, lengths);
    
    if (nargs<1 || nargs>2) {
        error_raise(context, ERROR_MULTIVECTOR_INDEX, ERROR_MULTIVECTOR_INDEX_MSG, ERROR_FATAL);
    } else if (nargs==1) {
        if (eval_isinteger(args[0])) {
            i=eval_integervalue(args[0]);
            if (i<mobj->dimension+1) {
                ret=multivector_getgradeaslist(mobj, i);
            } else {
                error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
            }
        } else {
            error_raise(context, ERROR_MULTIVECTOR_INDEX_NUM, ERROR_MULTIVECTOR_INDEX_NUM_MSG, ERROR_FATAL);
        }
    } else if (nargs==2) {
        if (eval_isinteger(args[0])&&eval_isinteger(args[1])) {
            i=eval_integervalue(args[0]);
            j=eval_integervalue(args[1]);
            if ((i<mobj->dimension+1)&&(j>0)&&(j<=lengths[i])) {
                ret=multivector_getelement(mobj, i, j-1);
            } else {
                error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
            }
        } else {
            error_raise(context, ERROR_MULTIVECTOR_INDEX_NUM, ERROR_MULTIVECTOR_INDEX_NUM_MSG, ERROR_FATAL);
        }

    }
    
    return ret;
}

/* Sets the matrix element at a particular index */
expression *multivector_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj = (multivectorobject *) obj;
    int i=0, j=0;
    
    unsigned int lengths[mobj->dimension+1];
    unsigned int startindex[mobj->dimension+1];
    multivector_gradearray(mobj->dimension, startindex, lengths);
    
    switch (nargs) {
        case 2:
            if (eval_isinteger(args[1])) i = eval_integervalue(args[1]);
            if (i<0 || i>mobj->dimension+1) error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
            multivector_setgradetoexpression(mobj, context, i, args[0]);
            break;
            
        case 3:
            if (eval_isinteger(args[1])) i = eval_integervalue(args[1]);
            if (eval_isinteger(args[2])) j = eval_integervalue(args[2]);
            if (i<0 || i>mobj->dimension+1) error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
            if (j<0 || j>lengths[i]) error_raise(context, ERROR_MULTIVECTOR_INDEX_BOUND, ERROR_MULTIVECTOR_INDEX_BOUND_MSG, ERROR_FATAL);
            
            multivector_setelementtoexpression(mobj, context, i, j-1, args[0]);
            
            break;
            
        default:
            error_raise(context, ERROR_MULTIVECTOR_SETINDEX_NUM, ERROR_MULTIVECTOR_SETINDEX_NUM_MSG, ERROR_FATAL);
            break;
    }
    
    return (expression *) cloneexpression(args[0]);
}


expression *multivector_indicesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj = (multivectorobject *) obj;
    expression *ret=NULL;
    if (!mobj) return NULL;
    unsigned int ngrades=mobj->dimension+1;
    unsigned int nelements=0;
    
    unsigned int lengths[ngrades];
    multivector_gradearray(mobj->dimension, NULL, lengths);
    for (unsigned int i=0; i<ngrades; i++) nelements+=lengths[i];
    
    expression *ind[nelements];
    for (unsigned int i=0; i<nelements; i++) ind[i]=NULL;
    expression *el[2];
    
    unsigned int n=0;
    for (unsigned int i=0; i<ngrades; i++) {
        for (unsigned int j=0; j<lengths[i]; j++) {
            if (lengths[i]>1) {
                el[0]=newexpressioninteger(i);
                el[1]=newexpressioninteger(j+1);
                
                ind[n]=eval_listfromexpressionarray(el, 2);
                
                freeexpression(el[0]);
                freeexpression(el[1]);
                n++;
            } else {
                ind[n]=newexpressioninteger(i); n++;
            }
        }
    }
    
    /* IMPORTANT MUST RETURN THIS TO NORMAL [TEMPORARY SPEED UP] */
    //ret=eval_listfromexpressionarray(ind, nelements);
    ret=eval_listfromexpressionarray(ind+1, 3);
    
    for (unsigned int i=0; i<n; i++) {
        if (ind[i]) freeexpression(ind[i]);
    }
    
    return ret;
}

/* Adds two multivectors */
expression *multivector_addi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *a = (multivectorobject *) obj;
    multivectorobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(multivectorobject *) ref->obj;
                
                ret=multivector_add(context, a, b, FALSE, NULL);
            }
        } else if (eval_isnumerical(args[0])) {
            double x = eval_floatvalue(args[0]);
            expression_objectreference *ref = (expression_objectreference *) multivector_doublelisttomultivector(context, a->dimension, 0, &x, 1);
            
            if (eval_isobjectref(ref)) {
                b=(multivectorobject *) ref->obj;
                
                ret=multivector_add(context, a, b, FALSE, NULL);
            }
            
            if (ref) freeexpression((expression *) ref);
        }
    }
    
    return ret;
}

/* Subtracts two multivectors */
expression *multivector_subtracti(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *a = (multivectorobject *) obj;
    multivectorobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(multivectorobject *) ref->obj;
                
                ret=multivector_add(context, a, b, TRUE, NULL);
            }
        } else if (eval_isnumerical(args[0])) {
            double x = eval_floatvalue(args[0]);
            expression_objectreference *ref = (expression_objectreference *) multivector_doublelisttomultivector(context, a->dimension, 0, &x, 1);
            
            if (eval_isobjectref(ref)) {
                b=(multivectorobject *) ref->obj;
                
                ret=multivector_add(context, a, b, TRUE, NULL);
            }
            
            if (ref) freeexpression((expression *) ref);
        }
    }
    
    return ret;
}

expression *multivector_multiplyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *a = (multivectorobject *) obj;
    multivectorobject *b = NULL;
    expression *ret=NULL;
    
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            if (ref->obj->clss==obj->clss) {
                b=(multivectorobject *) ref->obj;
                
                ret=multivector_multiply(context, a, b, NULL, NULL);
            }
        } else if (eval_isnumerical(args[0])) {
            ret = multivector_clone((multivectorobject *) obj);
            
            if ((ret)&&(eval_isobjectref(ret))) {
                multivectorobject *new = (multivectorobject *) ((expression_objectreference *) ret)->obj;
                
                multivector_scale(new, context, eval_floatvalue(args[0]));
            }
            
        }
    }
    
    return ret;
}

expression *multivector_duali(object *obj, interpretcontext *context, int nargs, expression **args) {
    multivectorobject *mobj = (multivectorobject *) obj;
    
    return multivector_dual(mobj, context);
}

/*
 * Intrinsic functions
 */

expression *multivector_multivectori(interpretcontext *context, int nargs, expression **args) {
    expression *ret = NULL;
    
    switch (nargs) {
        case 0:
            ret=multivector_new(MULTIVECTOR_NONE, 0);
            break;
        case 1:
            if (eval_isinteger(args[0])) {
                int dim = eval_integervalue(args[0]);
                if (dim>=0) ret=multivector_new(MULTIVECTOR_DOUBLES, dim);
            } else if (eval_islist(args[0])) {
                ret=multivector_listtomultivector(context, (expression_list *) args[0], NULL);
            }
            break;
    }
    
    return ret;
}

/*
 * Initialization
 */

void multivectorinitialize(void) {
    multivectorclass=class_classintrinsic(GEOMETRY_MULTIVECTORCLASS, class_lookup(EVAL_OBJECT), sizeof(multivectorobject), 15);
    
    class_registerselector(multivectorclass, EVAL_NEWSELECTORLABEL, FALSE, multivector_newi);
    class_registerselector(multivectorclass, EVAL_FREESELECTORLABEL, FALSE, multivector_freei);
    class_registerselector(multivectorclass, EVAL_CLONE, FALSE, multivector_clonei);
    class_registerselector(multivectorclass, EVAL_PRINTSELECTORLABEL, FALSE, multivector_printi);
    class_registerselector(multivectorclass, EVAL_SERIALIZE, FALSE, multivector_serializei);
    class_registerselector(multivectorclass, EVAL_DESERIALIZE, FALSE, multivector_deserializei);
    
    class_registerselector(multivectorclass, EVAL_TOLIST_SELECTOR, FALSE, multivector_tolisti);
    
    class_registerselector(multivectorclass, EVAL_INDEX, FALSE, multivector_indexi);
    class_registerselector(multivectorclass, EVAL_SETINDEX, FALSE, multivector_setindexi);
    class_registerselector(multivectorclass, EVAL_INDICES, FALSE, multivector_indicesi);
    
    class_registerselector(multivectorclass, OPERATOR_ADD_SELECTOR, FALSE, multivector_addi);
    class_registerselector(multivectorclass, OPERATOR_SUBTRACT_SELECTOR, FALSE, multivector_subtracti);
    class_registerselector(multivectorclass, OPERATOR_MULTIPLY_SELECTOR, FALSE, multivector_multiplyi);
    
    class_registerselector(multivectorclass, MULTIVECTOR_DUAL_SELECTOR, FALSE, multivector_duali);
    
    /* Intrinsic functions */
    intrinsic(GEOMETRY_MULTIVECTORCLASS, FALSE, multivector_multivectori, NULL);
}

