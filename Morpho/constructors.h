/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "geometry.h"

#ifndef constructors_h
#define constructors_h

#define GEOMETRY_MANIFOLDLOAD "manifoldload"
#define GEOMETRY_MANIFOLDLINE "manifoldline"
#define GEOMETRY_MANIFOLDPOINTS "manifoldpoints"
#define GEOMETRY_MANIFOLDTRIANGULATE "manifoldtriangulate"
#define GEOMETRY_MANIFOLDIMPLICIT "manifoldimplicit"
#define GEOMETRY_MANIFOLDCUBOID "cuboid"
#define GEOMETRY_LINEMANIFOLD_CLOSEDOPTION "closed"

#define GEOMETRY_MMSTEPSIZE "stepsize"
#define GEOMETRY_MMGRADIENT "gradient"
#define GEOMETRY_MMSTART    "start"
#define GEOMETRY_MMMAXITER  "maxiterations"

#define GEOMETRY_MESHFILE_VERTICES "vertices"
#define GEOMETRY_MESHFILE_EDGES "edges"
#define GEOMETRY_MESHFILE_FACES "faces"
#define GEOMETRY_MESHFILE_VOLUMES "volumes"

#define ERROR_GEOMETRY_NONNUMERICAL                0x9300
#define ERROR_GEOMETRY_NONNUMERICAL_MSG            "Argument evaluated to nonnumerical values."

#define ERROR_GEOMETRY_POINTLIST                   0x9301
#define ERROR_GEOMETRY_POINTLIST_MSG               "Requires a list of points."

#define ERROR_GEOMETRY_POINTDIM                    0x9302
#define ERROR_GEOMETRY_POINTDIM_MSG                "Points must have the same dimensionality."

#define ERROR_GEOMETRY_POINTNUM                    0x9303
#define ERROR_GEOMETRY_POINTNUM_MSG                "Points must be numerical."

#define ERROR_GEOMETRY_COULDNTOPENFILE             0x9304
#define ERROR_GEOMETRY_COULDNTOPENFILE_MSG         "Couldn't open file."

#define ERROR_CONSTRUCTOR_ZEROGRADIENT             0x9305
#define ERROR_CONSTRUCTOR_ZEROGRADIENT_MSG         "Zero gradient detected."

#define ERROR_CONSTRUCTOR_MAXITER                  0x9306
#define ERROR_CONSTRUCTOR_MAXITER_MSG              "Too many iterations in surface projection: Solution may be of poor quality."

#define ERROR_CONSTRUCTOR_MMSYNTAX                 0x9307
#define ERROR_CONSTRUCTOR_MMSYNTAX_MSG             "The gradient of the function must be specified as a list."

#define ERROR_CONSTRUCTOR_MMIMPLICIT               0x9308
#define ERROR_CONSTRUCTOR_MMIMPLICIT_MSG           "manifoldimplicit requires a scalar function, its gradient and an initial starting point as arguments."

#define ERROR_CONSTRUCTOR_MMSTART                  0x9309
#define ERROR_CONSTRUCTOR_MMSTART_MSG              "Initial point must be real numbers."

#define ERROR_CONSTRUCTOR_MMEXPNONNUM              0x9310
#define ERROR_CONSTRUCTOR_MMEXPNONNUM_MSG          "Expression evaluated to nonnumerical value."

#define ERROR_CONSTRUCTOR_MMGRADNONNUM             0x9311
#define ERROR_CONSTRUCTOR_MMGRADNONNUM_MSG         "Gradient evaluated to nonnumerical value."

#define ERROR_CONSTRUCTOR_MMMERGEFAILED            0x9312
#define ERROR_CONSTRUCTOR_MMMERGEFAILED_MSG        "Front polygon merge failed."

#define ERROR_CONSTRUCTOR_MMMAXITER                0x9313
#define ERROR_CONSTRUCTOR_MMMAXITER_MSG            "Exceeded maximum number of iterations."

/* Public types for use with marching method for implicit surfaces */
/* Point primitive used by the marching method */
typedef struct  {
    double pt[3]; /* Point on the surface */
    double normal[3]; /* Local approximation to the normal */
    double t1[3]; /* } Tangent vectors */
    double t2[3]; /* } */
    double frontangle; /* Front angle, if the point is an actual front point */
    int angle_changed; /* if the actual front angle was changed and has to be recalculated */
    int border_point; /* if the point is on the border of the current triangulation */
    int exclude; /* Exclude from further distance checks */
} constructors_mmpoint;

/* Scalar function to be passed to the marching method.
 Input:  double *x     - a point in 3d
         void   *ref   - reference data needed to evaluate the function
 Output: double *gradf - should be updated with the current gradient
 Returns: the value of f at the point. */
typedef double constructors_mmfunc (double *, void *ref, double * );

/* Output returned by the marching method */
typedef struct {
    unsigned int npts;
    constructors_mmpoint *pts;
    unsigned int ntri;
    unsigned int *tri;
} constructors_mmoutput;

/*
 * Public function definitions
 */

expression *geometry_parametriclinei(interpretcontext *context, int nargs, expression **args);

void constructorsinitialize(void);

#endif
