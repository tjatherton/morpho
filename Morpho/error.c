/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "error.h"

#define ERR_CHARLIMIT 1024

char errorbuffer[ERR_CHARLIMIT];

void error_saveenv(interpretcontext *context, jmp_buf *env) {
    interpretcontext *ctxt=context;
    if (!ctxt) ctxt=defaultinterpretcontext;
    
    if (ctxt->errorenv==NULL) ctxt->errorenv=EVAL_MALLOC(sizeof(jmp_buf));
    
    if (ctxt->errorenv) memcpy(ctxt->errorenv, env, sizeof(jmp_buf));
}

void error_raisefrominfo(interpretcontext *context, errorinfo *info) {
    interpretcontext *ctxt=context;
    if (!ctxt) ctxt=defaultinterpretcontext;
    jmp_buf *error_env=NULL;
    
    /* Locate the most recent jmp_buf in the execution context stack */
    if (context) for (interpretcontext *c=ctxt; c!=NULL; c=c->parent) if (c->errorenv) { error_env=c->errorenv; break; }
    
    if (info->clss==ERROR_PARSE) {
        token *tok=(token *) info->data;
        unsigned int tokerr;
        unsigned int i;
        
        tokerr=(unsigned int) (tok->token-tok->base);
        if ((tokerr<ERR_CHARLIMIT)&&(tok->tokenlength<ERR_CHARLIMIT)) {
            for (i=0;i<=tokerr;i++) printf(" "); /* Extra space needed because of prompt */
            for (i=0;i<tok->tokenlength;i++) printf("^");
            printf("\n");
        }
    }
    
    if (info->msg) printf("%s\n",info->msg);
    
    if ((info->clss!=ERROR_INFORMATIONAL)&&(info->clss!=ERROR_WARNING)) {
        if (error_env) {
            longjmp(*error_env,info->code);
        } else {
            //exit(1); // Untrapped error.
        }
    }
}

void error_raise(interpretcontext *context, int code, char *msg, errorclass clss) {
    errorinfo info;
    
    info.code=code;
    info.clss=clss;
    info.msg=msg;
    info.data=NULL;
    
    error_raisefrominfo(context, &info);
}

void error_raisewithdata(interpretcontext *context, int code, char *msg, errorclass clss, void *data) {
    errorinfo info;
    
    info.code=code;
    info.clss=clss;
    info.msg=msg;
    info.data=data;
    
    error_raisefrominfo(context, &info);
}

void evalinitializeerror(void) {
    
}
