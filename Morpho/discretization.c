/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "classes.h"
#include "discretization.h"

/* ********************************************************************
 * Quadrature functions
 ******************************************************************** */

typedef enum {
    QUANTITY_REAL,
    QUANTITY_MULTIVECTOR
} discretization_quantitytype;

/* Recognize quantities and initialize an object to use for interpolation. This minimizes the need to allocate memory. */
void discretization_recognizequantities(unsigned int eldim, unsigned int nquantity, expression ***quantity, discretization_quantitytype *qtype, expression **q) {
    
    if (nquantity>0) {
        for (unsigned int i=0; i<nquantity; i++) { q[i]=NULL; }
        for (unsigned int i=0; i<nquantity; i++) {
            int isfloat=TRUE, isMV=TRUE;
            for (unsigned int j=0; j<eldim; j++) {
                if (!eval_isfloat(quantity[j][i])) isfloat=FALSE;
                if (!eval_isobjectref(quantity[j][i]) || !class_objectisofclass(quantity[j][i], GEOMETRY_MULTIVECTORCLASS)) isMV=FALSE;
            }
            if (isfloat) {
                qtype[i]=QUANTITY_REAL;
                q[i]=newexpressionfloat(0.0);
            } else if (isMV) {
                qtype[i]=QUANTITY_MULTIVECTOR;
                q[i]=multivector_clone((multivectorobject *) eval_objectfromref((expression_objectreference *) quantity[0][i]));
            }
        }
    }
}

/* ******************************************************************************************
 * Integration over a line using Gauss-Kronrod 7-15 rule.
 ****************************************************************************************** */

double testintegrand(unsigned int dim, double *t, double *x, expression **quantity, void *data) {
    return pow(x[0]*(1.0-x[0]), 20.0);
}

static double gk[] = {
    /* Gauss 7 pt nodes [pt, Gauss wt, Kronrod wt] */
    -0.949107912342759,  0.129484966168870,  0.063092092629979,
    0.949107912342759,  0.129484966168870,  0.063092092629979,
    -0.741531185599394,  0.279705391489277,  0.140653259715525,
    0.741531185599394,  0.279705391489277,  0.140653259715525,
    -0.405845151377397,  0.381830050505119,  0.190350578064785,
    0.405845151377397,  0.381830050505119,  0.190350578064785,
    0.000000000000000,  0.417959183673469,  0.209482141084728,
    
    /* Kronrod extension [pt, Gauss wt, Kronrod wt] */
    -0.991455371120813,  0.0, 0.022935322010529,
    0.991455371120813,  0.0, 0.022935322010529,
    -0.864864423359769,  0.0, 0.104790010322250,
    0.864864423359769,  0.0, 0.104790010322250,
    -0.586087235467691,  0.0, 0.169004726639267,
    0.586087235467691,  0.0, 0.169004726639267,
    -0.207784955007898,  0.0, 0.204432940075298,
    0.207784955007898,  0.0, 0.204432940075298
};

unsigned int gknpts=15;
unsigned int gk1=7;
unsigned int gk2=15;

/* Linearly interpolate the position. t goes from [0,1] */
void discretization_interpolatepositionline(unsigned int dim, MFLD_DBL *x[3], double t, MFLD_DBL *xout) {
    double lambda[2] = {1-t,t};
    for (unsigned int j=0; j<dim; j++) {
        xout[j]=0;
        for (unsigned int k=0; k<2; k++) xout[j]+=lambda[k]*x[k][j];
    }
}

/* Linearly interpolate the quantities. If qout[i] is already assigned uses inplace methods. t goes from [0, 1] */
void discretization_interpolatequantitiesline(interpretcontext *context, unsigned int dim, double t, unsigned int nquantity, expression **quantity[2], discretization_quantitytype *qtype, expression **qout) {
    expression_objectreference *mv[2];
    double val, lambda[2] = {1-t,t};
    
    for (unsigned int i=0; i<nquantity; i++) {
        switch (qtype[i]) {
            case QUANTITY_MULTIVECTOR:
                mv[0] = (expression_objectreference *) quantity[0][i];
                mv[1] = (expression_objectreference *) quantity[1][i];
                if (qout[i]) {
                    multivector_weightedtotalinplace(context, mv, lambda, 2, (expression_objectreference *) qout[i]);
                } else qout[i] = multivector_weightedtotal(context, mv, lambda, 2);
                break;
            case QUANTITY_REAL:
                val=lambda[0]*((expression_float *) quantity[0][i])->value + lambda[1]*((expression_float *) quantity[1][i])->value;
                if (!qout[i]) qout[i]=newexpressionfloat(val);
                else ((expression_float *) qout[i])->value=val;
                break;
        }
    }
}

/* Integrate over a line element
 * Input :  interpretcontext *context                      - An interpretcontext
 *          discretizationintegrandfunction * function     - function to integrate
 *          unsigned int dim                               - Dimension of the vertices
 *          MFLD_DBL **x                                   - vertices of the line x[0] = {x,y,z} etc.
 *          unsigned int nquantity                         - number of quantities per vertex
 *          expression **quantity                          - List of quantities for each vertex.
 *          void *data                                     - a pointer to any data required by the function
 *          double ge                                      - Global estimate of the integral (used for recursion).
 * Returns: Estimate for the integral
 */
double discretization_lineint(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[2], unsigned int nquantity, expression **quantity[3], discretization_quantitytype *qtype, expression **q, void *data, unsigned int recursiondepth, double ge) {
    double r[gknpts], r1=0.0, r2=0.0, eps;
    double xx[dim], gest=ge;
    double af=pow(0.5, (double) recursiondepth); // Length of whole line from recursion depth
    unsigned int i;
    
    /* Try low order method for rapid results on low order functions */
    for (unsigned int i=0; i<gknpts; i++) {
        double tt=0.5*(1.0+gk[3*i]); // Convert [-1,1] to [0,1]
        discretization_interpolatepositionline(dim, x, tt, xx);
        if (nquantity)  discretization_interpolatequantitiesline(context, dim, tt, nquantity, quantity, qtype, q);
        r[i] = (*function) (dim, &tt, xx, q, data);
    }
    
    for (i=0; i<gk1; i++) {
        r1+=r[i]*gk[3*i+1];
        r2+=r[i]*gk[3*i+2];
    }
    for (; i<gk2; i++) {
        r2+=r[i]*gk[3*i+2];
    }
    r1*=0.5; r2*=0.5;
    
    if (recursiondepth==0) gest=fabs(r2); // If at top level construct a global estimate of the integral
    
    eps=r2-r1;
    eps*=af;
    if (gest>MACHINE_EPSILON) eps/=gest; // Globally relative estimate using area factor
    
    //printf("Recursion depth %u: %g %g - %g\n",recursiondepth, r1, r2, eps);
    
    if (fabs(eps)<DISCRETIZATION_ACCURACYGOAL)  {
        return r2;
    }
    
    if (recursiondepth>DISCRETIZATION_MAXRECURSION) {
        error_raise(context, ERROR_DISCRETIZATION_RECURSION, ERROR_DISCRETIZATION_RECURSION_MSG, ERROR_WARNING);
        return r2;
    }
    
    /* Bisect:
     */
    MFLD_DBL *xn[2]; /* Will hold the vertices. */
    MFLD_DBL xm[dim];
    expression *qm[nquantity], **qn[2];
    
    /* New vertices s*/
    for (unsigned int i=0; i<dim; i++) {
        xm[i] = 0.5*(x[0][i]+x[1][i]);
    }
    /* Quantities */
    if (nquantity) {
        for (unsigned int i=0; i<nquantity; i++) qm[i]=NULL;
        discretization_interpolatequantitiesline(context, dim, 0.5, nquantity, quantity, qtype, qm);
    }
    
    r2=0.0;
    xn[0]=x[0]; xn[1]=xm;
    if (nquantity) { qn[0] = quantity[0]; qn[1] = qm; }
    r2+=discretization_lineint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    
    xn[0]=xm; xn[1]=x[1];
    if (nquantity) { qn[0] = qm; qn[1] = quantity[1]; }
    r2+=discretization_lineint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    
    r2*=0.5;
    
    /* Free interpolated quantities */
    if (nquantity) {
        for (unsigned int i=0; i<nquantity; i++) {
            freeexpression(qm[i]);
        }
    }
    
    return r2;
}

/* Integrate over a line - public interface.
 * Input : interpretcontext *context                      - An interpretcontext
 *         discretizationintegrandfunction * function     - function to integrate
 *         unsigned int dim                               - Dimension of the vertices
 *         MFLD_DBL **x                                   - vertices of the line endpoints x[0] = {x,y,z} etc.
 *         unsigned int nquantity                         - number of quantities per vertex
 *         expression **quantity                          - List of quantities for each endpoint.
 *         void *data                                     - a pointer to any data required by the function
 */
double discretization_lineintegrate(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[2], unsigned int nquantity, expression **quantity[2], void *data) {
    double result=0.0;
    discretization_quantitytype qtype[nquantity];
    expression *q[nquantity];
    
    discretization_recognizequantities(2, nquantity, quantity, qtype, q);
    
    /* Do the integration */
    result=discretization_lineint(context, function, dim, x, nquantity, quantity, qtype, q, data, 0, 0.0);
    
    /* Free any quantities allocated */
    if (nquantity>0) for (unsigned int i=0; i<nquantity; i++) {
        freeexpression(q[i]);
    }
    
    return result;
}

/* ******************************************************************************************
 * Integration over a triangle
 * Adaptive rules based on Walkington, "Quadrature on Simplices of arbitrary dimension"
 ****************************************************************************************** */

/* Points to evaluate the function at in Barycentric coordinates */
static double pts[] = {
    0.3333333333333333, 0.3333333333333333, 0.3333333333333333,
    0.6000000000000000, 0.2000000000000000, 0.2000000000000000,
    0.2000000000000000, 0.6000000000000000, 0.2000000000000000,
    0.2000000000000000, 0.2000000000000000, 0.6000000000000000,
    0.7142857142857143, 0.1428571428571429, 0.1428571428571429,
    0.1428571428571429, 0.7142857142857143, 0.1428571428571429,
    0.1428571428571429, 0.1428571428571429, 0.7142857142857143,
    0.4285714285714286, 0.4285714285714286, 0.1428571428571429,
    0.4285714285714286, 0.1428571428571429, 0.4285714285714286,
    0.1428571428571429, 0.4285714285714286, 0.4285714285714286,
    0.7777777777777778, 0.1111111111111111, 0.1111111111111111,
    0.1111111111111111, 0.7777777777777778, 0.1111111111111111,
    0.1111111111111111, 0.1111111111111111, 0.7777777777777778,
    0.3333333333333333, 0.5555555555555556, 0.1111111111111111,
    0.3333333333333333, 0.1111111111111111, 0.5555555555555556,
    0.5555555555555556, 0.3333333333333333, 0.1111111111111111,
    0.5555555555555556, 0.1111111111111111, 0.3333333333333333,
    0.1111111111111111, 0.3333333333333333, 0.5555555555555556,
    0.1111111111111111, 0.5555555555555556, 0.3333333333333333,
    0.3333333333333333, 0.3333333333333333, 0.3333333333333333
};

static double wts1[] = {-0.2812500000000000, 0.2604166666666667};
static double wts2[] = {0.06328125000000000, -0.2712673611111111, 0.2084201388888889};
static double wts3[] = {-0.007910156250000000, 0.1211015004960317, -0.3191433376736111,
    0.2059465680803571};
static unsigned int npts1 = 10;
static unsigned int npts2 = 20;

/* Linearly interpolate the position depending on the triangle */
void discretization_interpolatepositiontri(unsigned int dim, MFLD_DBL *x[3], double *lambda, MFLD_DBL *xout) {
    for (unsigned int j=0; j<dim; j++) {
        xout[j]=0;
        for (unsigned int k=0; k<3; k++) xout[j]+=lambda[k]*x[k][j];
    }
}

/* Linearly interpolate the quantities. If qout[i] is already assigned uses inplace methods. */
void discretization_interpolatequantitiestri(interpretcontext *context, unsigned int dim, double *lambda, unsigned int nquantity, expression **quantity[3], discretization_quantitytype *qtype, expression **qout) {
    expression_objectreference *mv[3];
    double val;
    
    for (unsigned int i=0; i<nquantity; i++) {
        switch (qtype[i]) {
            case QUANTITY_MULTIVECTOR:
                mv[0] = (expression_objectreference *) quantity[0][i];
                mv[1] = (expression_objectreference *) quantity[1][i];
                mv[2] = (expression_objectreference *) quantity[2][i];
                if (qout[i]) {
                    multivector_weightedtotalinplace(context, mv, lambda, 3, (expression_objectreference *) qout[i]);
                } else qout[i] = multivector_weightedtotal(context, mv, lambda, 3);
                break;
            case QUANTITY_REAL:
                val=lambda[0]*((expression_float *) quantity[0][i])->value + lambda[1]*((expression_float *) quantity[1][i])->value + lambda[2]*((expression_float *) quantity[2][i])->value;
                if (!qout[i]) qout[i]=newexpressionfloat(val);
                else ((expression_float *) qout[i])->value=val;
                break;
        }
    }
}

/* Integrate over a triangle in barycentric coordinates.
 * Input :  interpretcontext *context                      - An interpretcontext
 *          discretizationintegrandfunction * function     - function to integrate
 *          unsigned int dim                               - Dimension of the vertices
 *          MFLD_DBL **x                                   - vertices of the triangle x[0] = {x,y,z} etc.
 *          unsigned int nquantity                         - number of quantities per vertex
 *          expression **quantity                          - List of quantities for each vertex.
 *          void *data                                     - a pointer to any data required by the function
 *          double ge                                      - Global estimate of the integral (used for recursion). 
 * Returns: Estimate for the integral
 */
double discretization_triangleint(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[3], unsigned int nquantity, expression **quantity[3], discretization_quantitytype *qtype, expression **q, void *data, unsigned int recursiondepth, double ge) {
    double r[npts2], r1, rr, r2, rr2, r3, rr3, eps;
    double xx[dim], gest=ge;
    double af=pow(0.25, (double) recursiondepth); // Area of total triangle covered from recursion depth
    
    /* Try low order method for rapid results on low order functions */
    for (unsigned int i=0; i<npts1; i++) {
        discretization_interpolatepositiontri(dim, x, pts + 3*i, xx);
        if (nquantity)  discretization_interpolatequantitiestri(context, dim, pts + 3*i, nquantity, quantity, qtype, q);
        r[i] = (*function) (dim, pts + 3*i, xx, q, data);
    }
    rr=(r[1]+r[2]+r[3]);
    rr2=(r[4]+r[5]+r[6]+r[7]+r[8]+r[9]);
    r1 = wts1[0]*r[0] + wts1[1]*rr;
    r2 = wts2[0]*r[0] + wts2[1]*rr + wts2[2]*rr2;
    
    if (recursiondepth==0) gest=fabs(r2); // If at top level construct a global estimate of the integral

    eps=r2-r1;
    eps*=af;
    if (gest>MACHINE_EPSILON) eps/=gest; // Globally relative estimate using area factor

    if (fabs(eps)<DISCRETIZATION_ACCURACYGOAL)  {
        //printf("Lower order worked.\n");
        return r2;
    }
    
    /* Extend order */
    for (unsigned int i=npts1; i<npts2; i++) {
        discretization_interpolatepositiontri(dim, x, pts + 3*i, xx);
        if (nquantity) discretization_interpolatequantitiestri(context, dim, pts + 3*i, nquantity, quantity, qtype, q);
        r[i] = (*function) (dim, pts + 3*i, xx, q, data);
    }
    rr3=(r[10]+r[11]+r[12]+r[13]+r[14]+r[15]+r[16]+r[17]+r[18]+r[19]);
    r3 = wts3[0]*r[0] + wts3[1]*rr + wts3[2]*rr2 + wts3[3]*rr3;
    
    if (recursiondepth==0) gest=fabs(r3); // Use an improved estimate of the integral
    
    eps=r2-r3;
    eps*=af;
    if (gest>MACHINE_EPSILON) eps/=gest; // Globally relative estimate
    //printf("Estimates %lg %lg %lg, err=%g af=%g\n", r1,r2,r3, eps, af);
    if (fabs(eps)<DISCRETIZATION_ACCURACYGOAL) return r3;
    
    if (recursiondepth>DISCRETIZATION_MAXRECURSION) {
        error_raise(context, ERROR_DISCRETIZATION_RECURSION, ERROR_DISCRETIZATION_RECURSION_MSG, ERROR_WARNING);
        return r3;
    }
    //printf("Quadrasection %u.\n", recursiondepth+1);
    
    /* Quadrasect:
     *       2
     *      / \
     *   x20 - x12
     *    / \  / \
     *   0 - x01 - 1
     */
    MFLD_DBL *xn[3]; /* Will hold the vertices. */
    MFLD_DBL x01[dim], x12[dim], x20[dim]; /* Vertices from midpoints */
    expression *q01[nquantity], *q12[nquantity], *q20[nquantity], **qn[3];
    
    r3=0.0;
    /* New vertices s*/
    for (unsigned int i=0; i<dim; i++) {
        x01[i] = 0.5*(x[0][i]+x[1][i]);
        x12[i] = 0.5*(x[1][i]+x[2][i]);
        x20[i] = 0.5*(x[2][i]+x[0][i]);
    }
    /* Quantities */
    if (nquantity) {
        double ll[3];
        for (unsigned int i=0; i<nquantity; i++) { q01[i]=NULL; q12[i]=NULL; q20[i]=NULL; }
        ll[0]=0.5; ll[1]=0.5; ll[2]=0.0;
        discretization_interpolatequantitiestri(context, dim, ll, nquantity, quantity, qtype, q01);
        ll[0]=0.0; ll[1]=0.5; ll[2]=0.5;
        discretization_interpolatequantitiestri(context, dim, ll, nquantity, quantity, qtype, q12);
        ll[0]=0.5; ll[1]=0.0; ll[2]=0.5;
        discretization_interpolatequantitiestri(context, dim, ll, nquantity, quantity, qtype, q20);
    }
    
    xn[0]=x[0]; xn[1]=x01; xn[2]=x20;
    if (nquantity) { qn[0] = quantity[0]; qn[1] = q01; qn[2] = q20; }
    r3+=discretization_triangleint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    
    xn[0]=x01; xn[1]=x[1]; xn[2]=x12;
    if (nquantity) { qn[0] = q01; qn[1] = quantity[1]; qn[2] = q12; }
    r3+=discretization_triangleint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    
    xn[0]=x20; xn[1]=x12; xn[2]=x[2];
    if (nquantity) { qn[0] = q20; qn[1] = q12; qn[2] = quantity[2]; }
    r3+=discretization_triangleint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    
    xn[0]=x01; xn[1]=x12; xn[2]=x20;
    if (nquantity) { qn[0] = q01; qn[1] = q12; qn[2] = q20; }
    r3+=discretization_triangleint(context, function, dim, xn, nquantity, qn, qtype, q, data, recursiondepth+1, gest);
    r3*=0.25;
    
    /* Free interpolated quantities */
    if (nquantity) {
        for (unsigned int i=0; i<nquantity; i++) {
            freeexpression(q01[i]); freeexpression(q12[i]); freeexpression(q20[i]);
        }
    }
    
    //printf("Quadrasection %u %g.\n", recursiondepth+1, r3);
    
    return r3;
}

/* Integrate over a triangle - public interface.
 * Input : interpretcontext *context                      - An interpretcontext
 *         discretizationintegrandfunction * function     - function to integrate
 *         unsigned int dim                               - Dimension of the vertices
 *         MFLD_DBL **x                                   - vertices of the triangle x[0] = {x,y,z} etc.
 *         unsigned int nquantity                         - number of quantities per vertex
 *         expression **quantity                          - List of quantities for each vertex.
 *         void *data                                     - a pointer to any data required by the function
 */
double discretization_triangleintegrate(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[3], unsigned int nquantity, expression **quantity[3], void *data) {
    double result=0.0;
    discretization_quantitytype qtype[nquantity];
    expression *q[nquantity];
    
    discretization_recognizequantities(3, nquantity, quantity, qtype, q);
    
    /* Do the integration */
    result=discretization_triangleint(context, function, dim, x, nquantity, quantity, qtype, q, data, 0, 0.0);
        
    /* Free any quantities allocated */
    if (nquantity>0) for (unsigned int i=0; i<nquantity; i++) {
        freeexpression(q[i]);
    }
    
    return result;
}


/*
 * Initialization
 */

void discretizationinitialize(void) {
    /*interpretcontext *c = newinterpretcontext(NULL, 1);
    double x1[3] = {0.0, 0.0, 0.0}, x2[3]={2.0, 1.0, 1.0};
    double *x[2] = {x1, x2};
    double res=0.0;

    res=discretization_lineintegrate(c, testintegrand, 3, x, 0, NULL, NULL);
    
    printf("%g\n", res);*/
}
