/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "geometry.h"

void geometryinitialize(void) {
    multivectorinitialize();
    manifoldinitialize();
    fieldinitialize();
    functionalinitialize();
    bodyinitialize();
    constructorsinitialize();
    selectioninitialize();
    discretizationinitialize();
    visualizeinitialize();
    idtableinitialize();
}
