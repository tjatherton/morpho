/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "color.h"

/* 
 * Internal functions
 */

expression *color_newcolor(double r, double g, double b) {
    color_object *obj=(color_object *) class_instantiate(class_lookup(COLOR_LABEL));
    expression_objectreference *ref=NULL;
    
    if (obj) {
        obj->r=r;
        obj->g=g;
        obj->b=b;
        
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

int color_rgbfromobject(interpretcontext *context, object *obj, int nargs, expression **args, double *col) {
    expression *colorcomponentexp;
    
    if (col) {
        colorcomponentexp=class_callselectorwithargslist(context, obj, COLOR_REDSELECTOR, nargs, args); // Red component
        if (eval_isnumerical(colorcomponentexp)) {
            col[0]=eval_floatvalue(colorcomponentexp);
            freeexpression(colorcomponentexp);
        } else {
            if (colorcomponentexp) freeexpression(colorcomponentexp);
            return FALSE;
        }

        colorcomponentexp=class_callselectorwithargslist(context, obj, COLOR_GREENSELECTOR, nargs,args); // Green component
        if (eval_isnumerical(colorcomponentexp)) {
            col[1]=eval_floatvalue(colorcomponentexp);
            freeexpression(colorcomponentexp);
        } else {
            if (colorcomponentexp) freeexpression(colorcomponentexp);
            return FALSE;
        }
        
        colorcomponentexp=class_callselectorwithargslist(context, obj, COLOR_BLUESELECTOR, nargs,args); // Blue component
        if (eval_isnumerical(colorcomponentexp)) {
            col[2]=eval_floatvalue(colorcomponentexp);
            freeexpression(colorcomponentexp);
        } else {
            if (colorcomponentexp) freeexpression(colorcomponentexp);
            return FALSE;
        }
        
    }
    
    return TRUE;
}

/* A slightly friendlier veneer onto color_rgbfromobject */

int color_rgbfromobjectref(interpretcontext *context, expression *exp, double *col) {
    int ret=FALSE;
    if (eval_isobjectref(exp)) {
        object *obj = eval_objectfromref(exp);
        
        ret=color_rgbfromobject(context, obj, 0, NULL, col);
    }
    return ret;
}

/*
 * Selectors
 */

expression *color_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    color_object *cobj=(color_object *) obj;
    
    if (cobj) {
        cobj->r=0;
        cobj->g=0;
        cobj->b=0;
    }
    
    return NULL;
}

expression *color_redi(object *obj, interpretcontext *context, int nargs, expression **args) {
    double r=0.0;
    
    if (obj) r=((color_object *) obj)->r;
    return newexpressionfloat(r);
}

expression *color_greeni(object *obj, interpretcontext *context, int nargs, expression **args) {
    double g=0.0;
    
    if (obj) g=((color_object *) obj)->g;
    return newexpressionfloat(g);
}

expression *color_bluei(object *obj, interpretcontext *context, int nargs, expression **args) {
    double b=0.0;
    
    if (obj) b=((color_object *) obj)->b;
    return newexpressionfloat(b);
}

expression *color_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(3);
    color_object *cobj=(color_object *) obj;
    expression *ret=NULL;
    
    if (ht) {
        /* Insert values into the temporary hashtable */
        hashinsert(ht, COLOR_REDSELECTOR, newexpressionfloat(cobj->r));
        hashinsert(ht, COLOR_GREENSELECTOR, newexpressionfloat(cobj->g));
        hashinsert(ht, COLOR_BLUESELECTOR, newexpressionfloat(cobj->b));
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and associated values */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
    }
    
    return ret;
}

expression *color_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    color_object *cobj=(color_object *) obj;
    expression *exp;
    
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        /* The runtime environment passes an object that responds to the lookup protocol as the first argument. */
        expression_objectreference *hashobject = (expression_objectreference *) args[0];
        
        /* Verify that the object passed responds to 'lookup'. */
        if (class_objectrespondstoselector(hashobject->obj, EVAL_LOOKUPSELECTORLABEL)) {
            /* Call the selector if so */
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, COLOR_REDSELECTOR);
            if (eval_isreal(exp)) cobj->r=eval_floatvalue(exp); /* Ensure it contains a double */
            if (exp) freeexpression(exp);
            
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, COLOR_GREENSELECTOR);
            if (eval_isreal(exp)) cobj->g=eval_floatvalue(exp); /* Ensure it contains a double */
            if (exp) freeexpression(exp);
            
            exp=class_callselector(context, hashobject->obj, EVAL_LOOKUPSELECTORLABEL, 1, EXPRESSION_STRING, COLOR_BLUESELECTOR);
            if (eval_isreal(exp)) cobj->b=eval_floatvalue(exp); /* Ensure it contains a double */
            if (exp) freeexpression(exp);
        
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Clone */
expression *color_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    color_object *cobj=(color_object *) obj;
    expression *ret=NULL;
    
    ret=color_newcolor(cobj->r, cobj->g, cobj->b);
    
    return ret;
}

/* 
 * Intrinsics
 */

expression *color_newcolori(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    if (nargs!=3) return NULL;
    double r[3];
    int i;
    
    for (i=0;i<3;i++) {
        if (eval_isnumerical(args[i])) r[i]=eval_floatvalue(args[i]);
        else break;
    }
    
    if (i==3) /* i.e. success */ ret=color_newcolor(r[0],r[1],r[2]);
    
    return ret;

}

/* Constructs a greyscale */
expression *color_newgrayscalei(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    if (nargs!=1) return NULL;
    double gs;
    
    if (eval_isnumerical(args[0])) {
        gs=eval_floatvalue(args[0]);
        
        ret=color_newcolor(gs, gs, gs);
    }
    
    return ret;
}

/* Tests if something is a color */

int color_iscolor(expression *exp) {
    int ret=FALSE;
    if (eval_isobjectref(exp)) {
        object *obj = eval_objectfromref(exp);
        if (class_objectisofclass(exp, COLOR_LABEL)) ret=TRUE;
        else if (class_objectrespondstoselector(obj, COLOR_REDSELECTOR) &&
                 class_objectrespondstoselector(obj, COLOR_GREENSELECTOR) &&
                 class_objectrespondstoselector(obj, COLOR_BLUESELECTOR)) {
            ret=TRUE;
        }
    }
    
    return ret; 
}

/* 
 * Initialization
 */

void colorinitialize(void) {
    classintrinsic *cls;
    expression *col;
    
    cls=class_classintrinsic(COLOR_LABEL, class_lookup(EVAL_OBJECT), sizeof(color_object), 5);
    class_registerselector(cls, COLOR_REDSELECTOR, FALSE, color_redi);
    class_registerselector(cls, COLOR_GREENSELECTOR, FALSE, color_greeni);
    class_registerselector(cls, COLOR_BLUESELECTOR, FALSE, color_bluei);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, color_newi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, color_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, color_deserializei);
    class_registerselector(cls, EVAL_CLONE, FALSE, color_clonei);
    
    intrinsic(COLOR_LABEL, FALSE, color_newcolori,NULL);
    intrinsic("grayscale", FALSE, color_newgrayscalei,NULL);
    
    /* Declare commonly used colors */
    col=color_newcolor(1.0, 0.0, 0.0);
    if (col) constant_insert("red", col);

    col=color_newcolor(0.0, 1.0, 0.0);
    if (col) constant_insert("green", col);

    col=color_newcolor(0.0, 0.0, 1.0);
    if (col) constant_insert("blue", col);

    col=color_newcolor(1.0, 1.0, 1.0);
    if (col) constant_insert("white", col);

    col=color_newcolor(0.0, 0.0, 0.0);
    if (col) constant_insert("black", col);
    
}
