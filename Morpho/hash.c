/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include <stdlib.h>
#include <limits.h>
#include <string.h>

#include "hash.h"

// [TODO] Error checking
 
/* Create a new hashtable. */
hashtable *hashcreate( int size ) {
    
	hashtable *ht = NULL;
	int i;
    
	if( size < 1 ) return NULL;
    
	/* Allocate the table itself. */
	if( ( ht = EVAL_MALLOC( sizeof( hashtable ) ) ) == NULL ) {
		return NULL;
	}
    
	/* Allocate pointers to the head nodes. */
	if( ( ht->table = EVAL_MALLOC( sizeof( hashentry * ) * size ) ) == NULL ) {
        EVAL_FREE(ht);
		return NULL;
	}
	for( i = 0; i < size; i++ ) {
		ht->table[i] = NULL;
	}
    
	ht->size = size;
    
	return ht;
}

/* Hash a string for a particular hash table. */
int hash( hashtable *ht, char *key ) {
    // [TODO] This may not be an optimal hashing function
	unsigned long int hashval = 0;
	int i = 0;
    
    if (!key) return 0;
    
	/* Convert our string to an integer */
	while( hashval < ULONG_MAX && i < strlen( key ) ) {
		hashval = hashval << 8;
		hashval += key[ i ];
		i++;
	}
    
	return hashval % ht->size;
}

/* Create a key-value pair. */
hashentry *hashnewpair( char *key, void *value ) {
	hashentry *newpair;
    
	if( ( newpair = EVAL_MALLOC( sizeof( hashentry ) ) ) == NULL ) {
		return NULL;
	}
    
	if( ( newpair->key = EVAL_STRDUP( key ) ) == NULL ) {
        EVAL_FREE(newpair);
		return NULL;
	}
    
	newpair->value = value;
	newpair->next = NULL;
    
	return newpair;
}

/* Insert a key-value pair into a hash table. */
void hashinsert( hashtable *ht, char *key, void *value ) {
	int bin = 0;
	hashentry *newpair = NULL;
	hashentry *next = NULL;
	hashentry *last = NULL;
    
	bin = hash( ht, key );
    
	next = ht->table[ bin ];
    
	while( next != NULL && next->key != NULL && strcmp( key, next->key ) > 0 ) {
		last = next;
		next = next->next;
	}
    
	/* There's already a pair.  Let's replace that string. */
	if( next != NULL && next->key != NULL && strcmp( key, next->key ) == 0 ) {
        
        /* WARNING: Must always check EXTERNALLY that a pair already exists and free the value */
		next->value = value;
        
        /* Nope, could't find it. */
	} else {
		newpair = hashnewpair( key, value );
        
		/* We're at the start of the linked list in this bin. */
		if( next == ht->table[ bin ] ) {
			newpair->next = next;
			ht->table[ bin ] = newpair;
            
            /* We're at the end of the linked list in this bin. */
		} else if ( next == NULL ) {
			if (last) {
                last->next = newpair;
            } else {
                EVAL_FREE(newpair);
            }
            
            /* We're in the middle of the list. */
		} else  {
			newpair->next = next;
			if (last) {
                last->next = newpair;
            } else {
                EVAL_FREE(newpair);
            }
		}
	}
}

/* Delete a key-value pair from a hash table. Important: Does NOT free the attached values. */
void hashremove( hashtable *ht, char *key) {
	int bin = 0;
    hashentry *pair = NULL;
	hashentry *last = NULL;
    
    bin = hash( ht, key );
    
    pair = ht->table[ bin ];
    
	while( pair != NULL && pair->key != NULL && strcmp( key, pair->key ) > 0 ) {
		last = pair;
		pair = pair->next;
	}
    
    if( pair != NULL && pair->key != NULL && strcmp( key, pair->key ) == 0 ) {
        if (last==NULL) { /* This was the first key-value pair in this list */
            ht->table[ bin ] = pair->next;
        } else {
            last->next=pair->next;
        }
        
        EVAL_FREE(pair->key);
        EVAL_FREE(pair);
	}
}

/* Retrieve a key-value pair from a hash table. */
void *hashget( hashtable *ht, char *key ) {
	int bin = 0;
	hashentry *pair;
    
	bin = hash( ht, key );
    
	/* Step through the bin, looking for our value. */
	pair = ht->table[ bin ];
	while( pair != NULL && pair->key != NULL && strcmp( key, pair->key ) > 0 ) {
		pair = pair->next;
	}
    
	/* Did we actually find anything? */
	if( pair == NULL || pair->key == NULL || strcmp( key, pair->key ) != 0 ) {
		return NULL;
        
	} else {
		return pair->value;
	}
	
}

/* Apply a function to every key-value pair in a hash table */
void hashmap(hashtable *ht, hashmapfunction *func, void *ref) {
    if (ht) for (int i=0; i<ht->size; i++) {
        for (hashentry *h = ht->table[i]; h!=NULL; h = h->next) (*func)(h->key,h->value,ref);
    }
}

/* A hashmapfunction that frees the value of the hashentry. Intended for use with hashmap to conveniently free
 * simple entries in the hashtable. */
void hashfreevalue(char *name, void *el,void *ref) {
    EVAL_FREE(el);
}

/* Frees a hashtable. Important: Does not free any attached values. */
void hashfree( hashtable *ht) {
    hashentry *h=NULL;
    hashentry *nh=NULL;
    
    for(int i = 0; i < ht->size; i++ ) {
		for (h = ht->table[i]; h!=NULL; h = nh) {
            nh=h->next;
            EVAL_FREE(h->key);
            EVAL_FREE(h);
        }
	}
    
    EVAL_FREE(ht->table);
    
    if (ht) EVAL_FREE(ht);
}

