/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "mt19937ar.h"

#include "hash.h"
#include "eval.h"
#include "cli.h"
#include "show.h"

#include "graphics.h"
#include "geometry.h"
#include "manifold.h"
#include "io.h"
#include "linalg.h"

#define VERSION "0.3.8"

#define MORPHORC ".morphorc"
#define USER_HOME_DIR "HOME"

#define MAXIMUM_FILENAME_LENGTH 256

void randinitialize(void) {
    FILE *urandom;
    unsigned long seed=(unsigned long) time(NULL);
    char bytes[sizeof(unsigned long)];
    
    /* Initialize the random number generator */
    urandom=fopen("/dev/urandom", "r");
    if (urandom) {
        for(int i=0;i<sizeof(unsigned long);i++) bytes[i]=(char) fgetc(urandom);
        fclose(urandom);
        
        seed = *((unsigned long *) bytes);
    } else printf("Warning: initializing random number generator using time-not recommended for production runs.\n");
    
    init_genrand(seed);
}

int main(int argc, const char * argv[])
{
    char fin[MAXIMUM_FILENAME_LENGTH]="";
    char fout[MAXIMUM_FILENAME_LENGTH]="";
    
    printf("Morpho %s (%s) - In nova fert animus mutatas dicere formas corpora\n", VERSION, __DATE__);
    
    randinitialize();
    evalinitialize();
    graphicsinitialize();
    cliinitialize();
    showinitialize();
    geometryinitialize();
    ioinitialize();
    linalginitialize();
    
	/* Process arguments */
    for(int i=1;i<argc;i++) {
		if(strncmp("-o",argv[i],2)==0) {
			/* Specify the filename of the output file */
			strncpy(fout,argv[i]+2*sizeof(char),MAXIMUM_FILENAME_LENGTH);
        } else if(strncmp("-d",argv[i],2)==0) {
            printf("~%s\n",argv[i]+2);
            freeexpression(execute(NULL, argv[i]+2));
        } else {
			/* If not recognized as an option, treat as the filename */
			strncpy(fin,argv[i],MAXIMUM_FILENAME_LENGTH);
		}
	};
    
    /* If a .morphorc file exists in the home directory, run it. */
    {
        char *c=getenv(USER_HOME_DIR);
        char foc[strlen(c)+strlen(MORPHORC)+2];

        sprintf(foc, "%s/%s", c, MORPHORC);
        
        FILE *f=fopen(foc,"r"); // Does .morphorc exist?
        if (f) {
            fclose(f);
            cli(foc, NULL, TRUE); // Run it 
        }
    }
    
#ifdef USE_GLFW
    show_cli(fin,fout,FALSE);
#else
    cli(fin,fout,FALSE);
#endif
    
    evalfinalize();
    iofinalize();
    clifinalize();
    showfinalize();
    
    exit(0);
}


