/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_classes_h
#define Morpho_classes_h

#include "eval.h"

/* The size of the global hashtable used to contain classes. Increase if many more intrinsic classes are defined */
#define EVAL_CLASSTABLESIZE 100

#define EVAL_OBJECTLABEL "object"
#define EVAL_SELFLABEL "self"

#define OPERATOR_SELECTOR "."

#define EVAL_NEWSELECTORLABEL "new"
#define EVAL_FREESELECTORLABEL "free"
#define EVAL_RESPONDSTOSELECTORLABEL "respondstoselector"
#define EVAL_PERFORMSELECTOR "performselector"
#define EVAL_EXPORTLABEL "export"
#define EVAL_OBJECT "object"
#define EVAL_SUPER "super"
#define EVAL_CLASS "class"
#define EVAL_SELECTORS "selectors"
#define EVAL_SERIALIZE "serialize"
#define EVAL_DESERIALIZE "deserialize"
#define EVAL_CLONE "clone"
#define EVAL_DEEPCLONE "deepclone"
#define EVAL_ADDLISTENER "addlistener"
#define EVAL_REMOVELISTENER "removelistener"
#define EVAL_PRINTSELECTORLABEL "print"
#define EVAL_TOLIST_SELECTOR "tolist"

#define EVAL_INDEX "index"
#define EVAL_SETINDEX "setindex"
#define EVAL_INDICES "indices"

typedef enum { CLASS_INTRINSIC, CLASS_USER } classtype;

/* A generic class type. */
typedef struct classgeneric_s {
    classtype type;
    struct classgeneric_s *parent;
    char *name;
    size_t objectsize;
} classgeneric;

/* A type that contains information about an intrinsic class. */
typedef struct {
    classtype type;
    classgeneric *parent;
    char *name;
    size_t objectsize;
    hashtable *selectors;
} classintrinsic;

/* A type that contains information about a user class. */
typedef struct {
    classtype type;
    classgeneric *parent;
    char *name;
    size_t objectsize;
    unsigned int definitionstablesize;
    interpretcontext *definitions;
} classuser;

/* A generic object structure. */
#define CLASS_GENERICOBJECTDATA classgeneric *clss; int refcnt; hashtable *listeners; linkedlist *listeningto; 

typedef struct {
    CLASS_GENERICOBJECTDATA
} object;

/* A generic object from a user-defined class */
typedef struct {
    CLASS_GENERICOBJECTDATA
    interpretcontext *data;
} objectuser;

/* A type of function that implements a selector for an intrinsic class.
 * Such a function receives an interpreter context, a number of args and a list of expressions as args. It must return an expression.
 */
typedef expression* selectortype (object*,interpretcontext*,int,expression**);

/* A record for intrinsic class selectors. Like intrinsic functions, these are able to control pre-evaluation
 * of their arguments. */
typedef struct {
    int hold;
    classtype type;
    selectortype *func;
    expression *exp;
} selector;

extern hashtable *classes;

typedef struct {
    expressiontype type;
    object *obj;
} expression_objectreference;

typedef struct {
    object *listener;
    expression_name *reportselector;
} class_listener;

/* A hashtable structure. */
#define EVAL_HASHTABLE "hashtable"

#define EVAL_ADDSELECTORLABEL "add"
#define EVAL_REMOVESELECTORLABEL "remove"
#define EVAL_LOOKUPSELECTORLABEL "lookup"

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    hashtable *table;
} hashtableobject;

#define EVAL_CLASSES "classes"

#include "intrinsics.h"

/* Errors */
#define CLASS_ADDLISTENERARGS                           0x1501
#define CLASS_ADDLISTENERARGS_MSG                       "Selector 'addlistener' expects an object, the name of the selector to listen to and the name of the selector to call."

#define CLASS_COULDNTRESOLVEOBJ                         0x1502
#define CLASS_COULDNTRESOLVEOBJ_MSG                     "Couldn't resolve object."


/* 
 * Function definitions 
 */
classgeneric *class_lookup(char *label);

classintrinsic *class_classintrinsic(char *label, classgeneric *parent, size_t objectsize, int nselectors);
classuser *class_classuser(char *label, expression *def);
void class_registerselector(classintrinsic *cls, char *label, int hold, selectortype *func);
void class_free(classgeneric *cls);
void class_hashfreeselectors(char *name, void *sel, void *ref);

object *class_instantiate(classgeneric *cls);
object *class_lwinstantiate(classgeneric *cls);
expression_objectreference *class_newobjectreference(object *obj);
void class_freeobjectreference(expression_objectreference *ref);

void class_retain(object *obj);
void class_release(object *obj);

int class_objectisofclass(expression *ref, char *name);

expression *class_serializefromhashtable(interpretcontext *context, hashtable *table, char *classname);

/* Free listeners */
int class_addlistener(object *obj, expression_name *selector, expression_objectreference *listener, expression_name *reportselector);
int class_removelistener(object *obj, expression_name *selector, expression_objectreference *listener);
void class_removelistenerforallselectors(object *obj, object *listener);
void class_destroylisteners(object *obj);

/* Generic object constructor */
expression *class_newi(interpretcontext *context, int nargs, expression **args);

/* Selectors */
int class_lookupselector(classgeneric *cls, char *label, selector *sel);
#define class_objectrespondstoselector(obj, label) class_lookupselector((obj ? obj->clss : NULL), label, NULL)
//int class_objectrespondstoselector(object *obj, char *label);
expression *class_performselector(interpretcontext *context, object *obj, expression_function *sfunc);
expression *class_selectorop(interpretcontext *context, expression *left, expression *right);
expression *class_callselector(interpretcontext *context, object *obj, char *name, unsigned int nargs, ...);
expression *class_callselectorwithargslist(interpretcontext *context, object *obj, char *name, unsigned int nargs, expression **args);

int class_addlistener(object *obj, expression_name *selector, expression_objectreference *listener, expression_name *reportselector);

interpretcontext *class_options(object *obj, interpretcontext *context, int start, int narg, expression **args);

expression_objectreference *class_hashtable_objectfromhashtable(hashtable *table);

void evalinitializeclasses(void);
void evalfinalizeclasses(void);

#endif
