/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "geometry.h"

#ifndef visualize_h
#define visualize_h

/*
 * visualize - visualization tools 
 */

#define GEOMETRY_PLOTFIELD "plotfield"
#define GEOMETRY_PLOTSELECTION "plotselection"

#define MANIFOLD_DRAW_SHOWVERTICES 1
#define MANIFOLD_DRAW_SHOWEDGES    2
#define MANIFOLD_DRAW_SHOWFACES    4

#define MANIFOLD_SHOWMESH "showmesh"

#define MANIFOLD_DRAW_SHOWALL (MANIFOLD_DRAW_SHOWFACES | MANIFOLD_DRAW_SHOWEDGES | MANIFOLD_DRAW_SHOWVERTICES)
#define MANIFOLD_DRAW_SHOWNONE     0

#define VISUALIZE_GRADE "grade"
#define VISUALIZE_STYLE "style"
#define VISUALIZE_STYLE_CYLINDER "cylinder"
#define VISUALIZE_STYLE_ARROW "arrow"
#define VISUALIZE_SHOWMESH "showmesh"
#define VISUALIZE_SHOWMESH_VERTICES "vertices"
#define VISUALIZE_SHOWMESH_EDGES "edges"
#define VISUALIZE_SHOWMESH_FACES "faces"

typedef enum {
    MANIFOLD_SELECTIONMODE_NONE,
    MANIFOLD_SELECTIONMODE_HIGHLIGHT,
    MANIFOLD_SELECTIONMODE_DRAWSELECTED} visualize_selectionmode;

expression *visualize_plotfieldi(interpretcontext *context, int nargs, expression **args);

expression *manifold_drawi(object *obj, interpretcontext *context, int nargs, expression **args);

/* Initialization */

void visualizeinitialize(void);

#endif 
