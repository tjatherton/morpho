/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "graphics3d.h"
#include "color.h"

/* 
 * Constructors
 */

/* Create a new element with type */
graphics3d_element *graphics3d_newelement(graphics3d_elementtype type) {
    graphics3d_element *el=EVAL_MALLOC(sizeof(graphics3d_element));
    if (el) el->type=type;
    return el;
}

/* Create a new point element with specified value p[] */
graphics3d_pointelement *graphics3d_newpointelement(graphics3d_elementtype type, float *p) {
    graphics3d_pointelement *el=EVAL_MALLOC(sizeof(graphics3d_pointelement));
    if (el) {
        el->type=type;
        el->point[0]=p[0];
        el->point[1]=p[1];
        el->point[2]=p[2];
    }
    
    return el;
}

/* Create a new text element
   Input: char *string                  - The string to displayfcylinder
          float *pt                     - The location of the text
          graphics_alignment alignhoriz - How to align the text relative to the point horizontally
          graphics_alignment alignvert  - How to align the text relative to the point vertically
   Returns: The created element */
graphics3d_textelement *graphics3d_newtextelement(char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert) {
    graphics3d_textelement *el = EVAL_MALLOC(sizeof(graphics3d_textelement));
    
    if (el) {
        el->type=GRAPHICS3D_TEXT;
        el->point[0]=pt[0];
        el->point[1]=pt[1];
        el->point[2]=pt[2];
        el->horizontal=alignhoriz;
        el->vertical=alignvert;
        el->string=EVAL_STRDUP(string);
        if (!el->string) { EVAL_FREE(el); return NULL; }
    }
    return el;
}

/* Create a new color element */
graphics3d_setcolorelement *graphics3d_newsetcolorelement(float r, float g, float b) {
    graphics3d_setcolorelement *el = EVAL_MALLOC(sizeof(graphics3d_setcolorelement));
    if (el) {
        el->type=GRAPHICS3D_SETCOLOR;
        el->r=r; el->g=g; el->b=b;
    }
    return el;
}

/* Create a new triangle element from three points */
graphics3d_triangleelement *graphics3d_newtrianglefromdouble(double *p1, double *p2, double *p3) {
    graphics3d_triangleelement *el=EVAL_MALLOC(sizeof(graphics3d_triangleelement));
    unsigned int i;
    
    if (el) {
        el->type=GRAPHICS3D_TRIANGLE;
        for (i=0; i<3; i++) el->p1[i]=(float) p1[i];
        for (i=0; i<3; i++) el->p2[i]=(float) p2[i];
        for (i=0; i<3; i++) el->p3[i]=(float) p3[i];
    }
    
    return el;
}

/* Create a new triangle element from three points */
graphics3d_triangleelement *graphics3d_newtriangle(float *p1, float *p2, float *p3) {
    graphics3d_triangleelement *el=EVAL_MALLOC(sizeof(graphics3d_triangleelement));
    unsigned int i;
    
    if (el) {
        el->type=GRAPHICS3D_TRIANGLE;
        for (i=0; i<3; i++) el->p1[i]=p1[i];
        for (i=0; i<3; i++) el->p2[i]=p2[i];
        for (i=0; i<3; i++) el->p3[i]=p3[i];
    }
    
    return el;
}

/* Create a new (empty) triangle complex */
graphics3d_trianglecomplexelement *graphics3d_newtrianglecomplex(unsigned int npoints, unsigned int ntriangles) {
    graphics3d_trianglecomplexelement *el=EVAL_MALLOC(sizeof(graphics3d_trianglecomplexelement));
    
    if (el) {
        el->type=GRAPHICS3D_TRIANGLE_COMPLEX;
        el->npoints=npoints;
        el->ntriangles=ntriangles;
        el->points=NULL;
        el->triangles=NULL;
        el->pointcolors=NULL;
        el->pointnormals=NULL;
        
        el->points=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->points) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->pointcolors=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->pointcolors) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->pointnormals=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->pointnormals) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->triangles=EVAL_MALLOC(sizeof(unsigned int)*ntriangles*3);
        if (!el->triangles) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
    }
    
    return el;
    
graphics3d_newtrianglecomplexfromevalfloat_cleanup:
    
    if (el) {
        if (el->points) EVAL_FREE(el->points);
        if (el->pointcolors) EVAL_FREE(el->pointcolors);
        if (el->triangles) EVAL_FREE(el->triangles);
        
        EVAL_FREE(el);
    }
    
    return NULL;
}

/* Create a new triangle complex.
 Input: unsigned int npoints    - The number of points
        double *points          - The vertices as triads of coordinates
        double *pointcolors     - The point colors
        double *pointnormals    - The point normals
        unsigned int ntriangles - The number of triangles
        unsigned int *triangles - Triangles as vertex ids.
 Returns: The created element */
graphics3d_trianglecomplexelement *graphics3d_newtrianglecomplexwithdata(unsigned int npoints, double *points, double *pointcolors, double *pointnormals, unsigned int ntriangles, unsigned int *triangles) {
    graphics3d_trianglecomplexelement *el=EVAL_MALLOC(sizeof(graphics3d_trianglecomplexelement));
    unsigned int i;
    
    if (el) {
        el->type=GRAPHICS3D_TRIANGLE_COMPLEX;
        el->npoints=npoints;
        el->ntriangles=ntriangles;
        el->points=NULL;
        el->triangles=NULL;
        el->pointcolors=NULL;
        el->pointnormals=NULL;

        el->points=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->points) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->pointcolors=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->pointcolors) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->pointnormals=EVAL_MALLOC(sizeof(float)*npoints*3);
        if (!el->pointnormals) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        el->triangles=EVAL_MALLOC(sizeof(unsigned int)*ntriangles*3);
        if (!el->triangles) goto graphics3d_newtrianglecomplexfromevalfloat_cleanup;
        
        for (i=0;i<npoints*3;i++) el->points[i]=(float) points[i];
        for (i=0;i<npoints*3;i++) el->pointcolors[i]=(float) pointcolors[i];
        for (i=0;i<npoints*3;i++) el->pointnormals[i]=(float) pointnormals[i];
        for (i=0;i<ntriangles;i++) el->triangles[i]=triangles[i];
    }
    
    return el;
    
graphics3d_newtrianglecomplexfromevalfloat_cleanup:
    
    
    if (el) {
        if (el->points) EVAL_FREE(el->points);
        if (el->pointcolors) EVAL_FREE(el->pointcolors);
        if (el->pointnormals) EVAL_FREE(el->pointnormals);
        if (el->triangles) EVAL_FREE(el->triangles);
        
        EVAL_FREE(el);
    }
    
    return NULL;
}

/* Clones an element */
graphics3d_element *graphics3d_cloneelement(graphics3d_element *el) {
    graphics3d_element *ret = NULL;
    
    if (el) switch (el->type) {
        case GRAPHICS3D_POINT:
        case GRAPHICS3D_LINETO:
        case GRAPHICS3D_MOVETO:
        {   graphics3d_pointelement *pel = (graphics3d_pointelement *) el;
            ret = (graphics3d_element *) graphics3d_newpointelement(pel->type, pel->point);
        }
            break;
        case GRAPHICS3D_TEXT:
        {   graphics3d_textelement *tel = (graphics3d_textelement *) el;
            ret = (graphics3d_element *) graphics3d_newtextelement(tel->string, tel->point, tel->horizontal, tel->vertical);
        }
            break;
        case GRAPHICS3D_STROKE:
            ret = graphics3d_newelement(GRAPHICS3D_STROKE);
            break;
        case GRAPHICS3D_SETCOLOR:
        {   graphics3d_setcolorelement *cel = (graphics3d_setcolorelement *) el;
            ret = (graphics3d_element *) graphics3d_newsetcolorelement(cel->r, cel->g, cel->b);
        }
            break;
        case GRAPHICS3D_TRIANGLE:
        {   graphics3d_triangleelement *tel = (graphics3d_triangleelement *) el;
            ret = (graphics3d_element *) graphics3d_newtriangle(tel->p1, tel->p2, tel->p3);
        }
            break;
        case GRAPHICS3D_TRIANGLE_COMPLEX:
        {
            graphics3d_trianglecomplexelement *qel = (graphics3d_trianglecomplexelement *) el;
            graphics3d_trianglecomplexelement *new = graphics3d_newtrianglecomplex(qel->npoints, qel->ntriangles);
            if ((new) && (new->pointcolors) && (new->pointnormals) && (new->points) && (new->triangles)) {
                memcpy(new->points, qel->points, sizeof(float)*qel->npoints*3);
                memcpy(new->pointcolors, qel->pointcolors, sizeof(float)*qel->npoints*3);
                memcpy(new->pointnormals, qel->pointnormals, sizeof(float)*qel->npoints*3);
                memcpy(new->triangles, qel->triangles, sizeof(float)*qel->ntriangles*3);
            }
            ret = (graphics3d_element *) new;
        }
            
    }
    
    return ret;
}

/*
 * Interface functions
 */

/* Create a new graphics3d object */
expression *graphics3d_newgraphics3d(void) {
    classgeneric *c=class_lookup(GRAPHICS3D_LABEL);
    graphics3d_object *obj=(graphics3d_object *) class_instantiate(c);
    expression_objectreference *ref=NULL;
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/* New selector */
expression *graphics3d_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics3d_object *gobj=(graphics3d_object *) obj;
    
    gobj->list=linkedlist_new();
    
    gobj->viewdirection[0]=1.1;
    gobj->viewdirection[1]=0.9;
    gobj->viewdirection[2]=0.5;
    
    gobj->viewvertical[0]=0.0;
    gobj->viewvertical[1]=1.0;
    gobj->viewvertical[2]=0.0;
    
    gobj->cliptrianglesbynormal=FALSE;
    gobj->showmesh=FALSE;
    
    return NULL;
}

/* Helper function that creates a triangle from doubles */
void graphics3d_trianglefromdouble(graphics3d_object *obj, double *p1, double *p2, double *p3) {
    graphics3d_triangleelement *el = graphics3d_newtrianglefromdouble(p1,p2,p3);
    if (el) linkedlist_addentry(obj->list, el);
}

graphics3d_trianglecomplexelement *graphics3d_trianglecomplex(graphics3d_object *obj, unsigned int npoints, unsigned int ntriangles) {
    graphics3d_trianglecomplexelement *el = graphics3d_newtrianglecomplex(npoints,ntriangles);
    if (el) linkedlist_addentry(obj->list, el);
    return el;
}

void graphics3d_point(graphics3d_object *obj,float *pt) {
    graphics3d_pointelement *el = graphics3d_newpointelement(GRAPHICS3D_POINT, pt);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics3d_moveto(graphics3d_object *obj,float *pt) {
    graphics3d_pointelement *el = graphics3d_newpointelement(GRAPHICS3D_MOVETO, pt);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics3d_lineto(graphics3d_object *obj,float *pt) {
    graphics3d_pointelement *el = graphics3d_newpointelement(GRAPHICS3D_LINETO, pt);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics3d_setcolor(graphics3d_object *obj,float r, float g, float b) {
    graphics3d_setcolorelement *el = graphics3d_newsetcolorelement(r, g, b);
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics3d_stroke(graphics3d_object *obj) {
    graphics3d_element *sel = graphics3d_newelement(GRAPHICS3D_STROKE);
    if (sel) linkedlist_addentry(obj->list, sel);
}

void graphics3d_text(graphics3d_object *obj, char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert) {
    graphics3d_textelement *el = graphics3d_newtextelement(string, pt, alignhoriz, alignvert);
    
    if (el) linkedlist_addentry(obj->list, el);
}

void graphics3d_line(graphics3d_object *obj, linkedlist *list) {
    float p[3];
    unsigned int np=0;
    
    for (linkedlistentry *e=list->first; e!=NULL; e=e->next) {
        expression_list *lst=(expression_list *) e->data;
        
        if(eval_listtofloatarray(lst, p, 3)) {
            if (e==list->first) graphics3d_moveto(obj, p);
            else graphics3d_lineto(obj, p);
            np++;
        }
    }
    
    if (np>1) graphics3d_stroke(obj);
}


/* Draws an arrow
 Input: graphics3d_object *obj - The graphics3d object to draw in.
        float *x1              - The first point
        float *x2              - The second point
        float ar               - Aspect ratio for the arrow
        unsigned int n         - How many faces to use (more is better, but more costly)
        float *col             - Color of the triangle, or NULL for default grey
 */
void graphics3d_arrow(graphics3d_object *obj, float *x1, float *x2, float ar, unsigned int n, float *col) {
    unsigned int np=3*n+2; /* 2 points at the end plus 3*n points. */
    unsigned int nt=6*n;
    graphics3d_trianglecomplexelement *el;
    float dx[3], ndx[3], nndx[3], u[3], px[3], qx[3], length;
    float th, matrix[9];
    unsigned int i;
    
    /* Create the triangle complex */
    el=graphics3d_newtrianglecomplex(np, nt);
    
    if (el) {
    /* The arrow is structured like so:
     
     
           [n+1...2n-1] C--[2n+1...3n-1]
                       \|\
    [1..n] A------------B \
           |               \
           0                1 3n
     
     */
    
        /* First find the vector that separates x2 and x1 */
        graphics3d_vectorsubtract(x2, x1, dx);
        length=graphics3d_vectornorm(dx);
        graphics3d_normalizevector(dx, ndx);
        
        /* Now construct a perpendicular vector of length |dx|*ar/2 */
        graphics3d_vectorperp(dx, px);
        graphics3d_normalizevector(px, px);
        graphics3d_vectorscale(px, px, length*ar*0.5);
        
        /* The base of the arrow is at position 0 */
        graphics3d_vectorcopy(x1, el->points);
        // and the normal
        graphics3d_vectorscale(ndx, nndx, -1.0);
        graphics3d_vectorcopy(nndx, el->pointnormals);
        
        /* The head of the arrow is at position 1 */
        graphics3d_vectorcopy(x2, el->points+3*(3*n+1));
        // and the normal
        graphics3d_vectorcopy(ndx, el->pointnormals+3*(3*n+1));
        
        graphics3d_vectorscale(dx, u, (1-ar));
        
        /* Calculate all the vertex positions */
        for (i=0; i<n; i++) {
            /* Find vertex ring A by rotating px about ndx, to obtain qx, and then adding x1 */
            th = i*(2*PI)/(n);
            graphics3d_rotation(ndx, th, matrix);
            graphics3d_transformvector(px, matrix, qx);
            graphics3d_vectoradd(x1, qx, el->points+3*(i+1));
            
            /* Vertex ring B is obtained by simply translating the first circle by dx*(1-ar) */
            graphics3d_vectoradd(el->points+3*(i+1), u, el->points+3*(n+i+1));
            
            /* Vertex ring C is obtained by simply adding qx to the point in vertex ring B */
            graphics3d_vectoradd(el->points+3*(n+i+1), qx, el->points+3*(2*n+i+1));
            
            // and the normals
            graphics3d_normalizevector(qx, nndx);
            graphics3d_vectorscale(nndx, nndx, 1.0);
            graphics3d_vectorcopy(nndx, el->pointnormals+3*(i+1));
            graphics3d_vectorcopy(nndx, el->pointnormals+3*(n+i+1));
            graphics3d_vectorcopy(nndx, el->pointnormals+3*(2*n+i+1));
        }
        
        /* Generate the triangles */
        for (i=0; i<n; i++) {
            /* Layer 0-A */
            el->triangles[3*i]=0;
            el->triangles[3*i+1]=i+1;
            if (i<n-1) el->triangles[3*i+2]=i+2; else el->triangles[3*i+2]=1;
            
            /* Layer C-1 */
            el->triangles[3*(n+i)]=2*n+i+1;
            if (i<n-1) el->triangles[3*(n+i)+1]=2*n+i+2; else el->triangles[3*(n+i)+1]=2*n+1;
            el->triangles[3*(n+i)+2]=3*n+1;
            
            /* Layer A-B */
            el->triangles[3*(2*n+i)]=i+1;
            if (i<n-1) el->triangles[3*(2*n+i)+1]=i+2; else el->triangles[3*(2*n+i)+1]=1;
            el->triangles[3*(2*n+i)+2]=n+i+1;
            
            if (i<n-1) el->triangles[3*(3*n+i)]=i+2; else el->triangles[3*(3*n+i)]=1;
            if (i<n-1) el->triangles[3*(3*n+i)+1]=n+i+2; else el->triangles[3*(3*n+i)+1]=n+1;
            el->triangles[3*(3*n+i)+2]=n+i+1;
            
            /* Layer B-C */
            el->triangles[3*(4*n+i)]=n+i+1;
            if (i<n-1) el->triangles[3*(4*n+i)+1]=n+i+2; else el->triangles[3*(4*n+i)+1]=n+1;
            el->triangles[3*(4*n+i)+2]=2*n+i+1;
            
            if (i<n-1) el->triangles[3*(5*n+i)]=n+i+2; else el->triangles[3*(5*n+i)]=n+1;
            if (i<n-1) el->triangles[3*(5*n+i)+1]=2*n+i+2; else el->triangles[3*(5*n+i)+1]=2*n+1;
            el->triangles[3*(5*n+i)+2]=2*n+i+1;
        }
        
        /* Set point colors */
        if (col) {
            for (unsigned i=0; i<np; i++) memcpy(el->pointcolors+3*i, col, 3*sizeof(float));
        } else {
            for (unsigned i=0; i<3*np; i++) el->pointcolors[i]=0.7;
        }
    
        linkedlist_addentry(obj->list, el);
    }
}

/* Draws a cylinder
 Input: graphics3d_object *obj - The graphics3d object to draw in.
 float *x1              - The first point
 float *x2              - The second point
 float ar               - Aspect ratio for the cylinder
 unsigned int n         - How many faces to use (more is better, but more costly)
 float *col             - Color of the cylinder, or NULL for default
 */
void graphics3d_cylinder(graphics3d_object *obj, float *x1, float *x2, float ar, unsigned int n, float *col) {
    unsigned int np=2*n+2; /* 2 points at the end plus 2*n points. */
    unsigned int nt=4*n;
    graphics3d_trianglecomplexelement *el;
    float dx[3], ndx[3], nndx[3], u[3], px[3], qx[3], length;
    float th, matrix[9];
    unsigned int i;
    
    /* Create the triangle complex */
    el=graphics3d_newtrianglecomplex(np, nt);
    
    if (el) {
        /* The cylinder is structured like so:
            [n+1...2n-1]
                        \
     [1..n] A------------B
            |            |
            0            1 [2*n]
         
         */
        
        /* First find the vector that separates x2 and x1 */
        graphics3d_vectorsubtract(x2, x1, dx);
        length=graphics3d_vectornorm(dx);
        graphics3d_normalizevector(dx, ndx);
        
        /* Now construct a perpendicular vector of length |dx|*ar/2 */
        graphics3d_vectorperp(dx, px);
        graphics3d_normalizevector(px, px);
        graphics3d_vectorscale(px, px, length*ar*0.5);
        
        /* The base of the arrow is at position 0 */
        graphics3d_vectorcopy(x1, el->points);
        // and the normal
        graphics3d_vectorscale(ndx, nndx, -1.0);
        graphics3d_vectorcopy(nndx, el->pointnormals);
        
        /* The head of the arrow is at position 1 */
        graphics3d_vectorcopy(x2, el->points+3*(2*n+1));
        graphics3d_vectorscale(dx, u, (1-ar));
        // and the normal
        graphics3d_vectorscale(ndx, nndx, 1.0);
        graphics3d_vectorcopy(nndx, el->pointnormals+3*(2*n+1));
        
        /* Calculate all the vertex positions */
        for (i=0; i<n; i++) {
            /* Find vertex ring A by rotating px about ndx, to obtain qx, and then adding x1 */
            th = i*(2*PI)/(n);
            graphics3d_rotation(ndx, th, matrix);
            graphics3d_transformvector(px, matrix, qx);
            graphics3d_vectoradd(x1, qx, el->points+3*(i+1));
            
            /* Vertex ring B is obtained by simply translating the first circle by dx*(1-ar) */
            graphics3d_vectoradd(el->points+3*(i+1), u, el->points+3*(n+i+1));
            
            // and the normals
            graphics3d_normalizevector(qx, nndx);
            graphics3d_vectorscale(nndx, nndx, 1.0);
            graphics3d_vectorcopy(nndx, el->pointnormals+3*(i+1));
            graphics3d_vectorcopy(nndx, el->pointnormals+3*(n+i+1));
        }
        
        /* Generate the triangles */
        for (i=0; i<n; i++) {
            /* Layer 0-A */
            el->triangles[3*i]=0;
            el->triangles[3*i+1]=i+1;
            if (i<n-1) el->triangles[3*i+2]=i+2; else el->triangles[3*i+2]=1;
            
            /* Layer B-1 */
            el->triangles[3*(n+i)]=n+i+1;
            if (i<n-1) el->triangles[3*(n+i)+1]=n+i+2; else el->triangles[3*(n+i)+1]=n+1;
            el->triangles[3*(n+i)+2]=n+1;
            
            /* Layer A-B */
            el->triangles[3*(2*n+i)]=i+1;
            if (i<n-1) el->triangles[3*(2*n+i)+1]=i+2; else el->triangles[3*(2*n+i)+1]=1;
            el->triangles[3*(2*n+i)+2]=n+i+1;
            
            if (i<n-1) el->triangles[3*(3*n+i)]=i+2; else el->triangles[3*(3*n+i)]=1;
            if (i<n-1) el->triangles[3*(3*n+i)+1]=n+i+2; else el->triangles[3*(3*n+i)+1]=n+1;
            el->triangles[3*(3*n+i)+2]=n+i+1;
        }
        
        /* Set point colors */
        if (col) {
            for (unsigned i=0; i<np; i++) memcpy(el->pointcolors+3*i, col, 3*sizeof(float));
        } else {
            for (unsigned i=0; i<3*np; i++) el->pointcolors[i]=0.7;
        }
        
        linkedlist_addentry(obj->list, el);
    }
}

/* Draws a tube from a sequence of points
 Input: graphics3d_object *obj - The graphics3d object to draw in.
 float *x               - List of points
 int   np               - Number of points
 float rad              - Radius for the tube
 int closed             - If true, connects last point to first point (forming a torus)
 unsigned int n         - How many faces to use (more is better, but more costly)
 float *col             - A color, or NULL for default
 */
void graphics3d_tube(graphics3d_object *obj, float *x, int np, float rad, int closed, unsigned int n, float *col) {
    unsigned int nv; /* Number of vertices */
    unsigned int ntri; /* Number of triangles */
    graphics3d_trianglecomplexelement *el;
    float dx[3], ndx[3*np], nndx[3], px[3], qx[3];
    float th, matrix[9];
    unsigned int i, nt=0;
    
    /* Determine number of vertices and triangles */
    nv = n*np + (closed ? 0 : 2); // n points per point + 2 extra if the system isn't closed
    ntri = n*(2*(np-1) + (closed ? 0 : 2)); // 2 n triangles per segment + 2 n extra if the system is closed
    
    /* Create the triangle complex */
    el=graphics3d_newtrianglecomplex(nv, ntri);
    
    /* Initialize quantities */
    memset(ndx, 0, sizeof(float)*np*3);
    memset(el->points, 0, sizeof(float)*nv*3);
    memset(el->pointnormals, 0, sizeof(float)*nv*3);
    memset(el->triangles, 0, sizeof(unsigned int)*nt*3);
    
    if (el) {
        /* Loop over line segments to calculate separation vectors */
        for (unsigned int k=0; k<np-1; k++) {
            /* First find the vector that separates points x[k+1] and x[k] */
            graphics3d_vectorsubtract(x+3*(k+1), x+3*k, dx);
            graphics3d_normalizevector(dx, dx);
            graphics3d_vectoradd(ndx+3*k, dx, ndx+3*k);
            graphics3d_vectoradd(ndx+3*(k+1), dx, ndx+3*(k+1));
            if (k==np-1 && closed) graphics3d_vectoradd(ndx, dx, ndx); // Loop back around
        }
        
        /* Now loop over points and generate vertices */
        for (unsigned int k=0; k<np; k++) {
            if (k==0) {
                graphics3d_vectorperp(ndx+3*k, px);
            } else {
                graphics3d_vectorsubtract(el->points+(3*(k-1)*n), x+3*(k-1), dx);
                th=graphics3d_vectordotproduct(dx, ndx+3*k); // dx . R
                graphics3d_normalizevector(ndx+3*k, px); // Normalize R
                graphics3d_vectorscale(px, px, th);
                graphics3d_vectorsubtract(dx, px, px); // dx - dx . R \hat{R}
            }
            graphics3d_normalizevector(px, px);
            graphics3d_vectorscale(px, px, rad);
            
            /* Now build n points */
            for (i=0; i<n; i++) {
                /* Find vertex ring at point k by rotating px about ndx, to obtain qx, and then adding x1 */
                th = i*(2*PI)/(n);
                graphics3d_rotation(ndx+3*k, th, matrix);
                graphics3d_transformvector(px, matrix, qx);
                graphics3d_vectoradd(x+(3*k), qx, el->points+(3*k*n)+3*i);
                
                // and the normals
                graphics3d_normalizevector(qx, nndx);
                graphics3d_vectoradd(el->pointnormals+(3*k*n)+3*i, nndx, el->pointnormals+(3*k*n)+3*i );
            }
        }
        /* Loop over segments again to create triangles */
        nt=0;
        
        for (unsigned int k=0; k<np-1; k++) {
            //el->triangles + 3*nt
            for (i=0; i<n; i++) {
                el->triangles[3*nt]=(k*n+i);
                el->triangles[3*nt+1]=(i==n-1 ? (k*n) : (k*n+i)+1);
                el->triangles[3*nt+2]=((k+1)*n+i);
                nt++;
                el->triangles[3*nt]=(i==n-1 ? (k*n) : (k*n+i)+1);
                el->triangles[3*nt+1]=(i==n-1 ? ((k+1)*n) : ((k+1)*n+i)+1);
                el->triangles[3*nt+2]=((k+1)*n+i);
                nt++;
            }
        }
        
        /* End caps */
        if (!closed) {
            /* First and last point */
            graphics3d_vectorcopy(x, el->points+3*(nv-2));
            graphics3d_vectorcopy(x+3*(np-1), el->points+3*(nv-1));
            /* And their normals */
            graphics3d_vectorscale(ndx, el->pointnormals+3*(nv-2), -1.0);
            graphics3d_normalizevector(el->pointnormals+3*(nv-2), el->pointnormals+3*(nv-2));
            graphics3d_vectorscale(ndx+3*(np-1), el->pointnormals+3*(nv-1), 1.0);
            graphics3d_normalizevector(el->pointnormals+3*(nv-1), el->pointnormals+3*(nv-1));
            /* Now for caps */
            for (i=0; i<n; i++) {
                el->triangles[3*nt]=(i);
                el->triangles[3*nt+1]=(i==n-1 ? 0 : (i)+1);
                el->triangles[3*nt+2]=nv-2;
                nt++;
                el->triangles[3*nt]=((np-1)*n+i);
                el->triangles[3*nt+1]=(i==n-1 ? (np-1)*n+0 : ((np-1)*n+i)+1);
                el->triangles[3*nt+2]=nv-1;
                nt++;
            }
        }
        
        el->ntriangles=nt;
        
        /* Set point colors */
        if (col) {
            for (unsigned i=0; i<nv; i++) memcpy(el->pointcolors+3*i, col, 3*sizeof(float));
        } else {
            for (unsigned i=0; i<3*nv; i++) el->pointcolors[i]=0.7;
        }
        
        linkedlist_addentry(obj->list, el);
    }
}

/* Finds a vertex in a list
Input: float *v               - List of vertices
       uint  nv               - number of vertices in the list
       float *x               - The point to find
Output: uint *indx            - If found, this is updated with the index of the vertex
Returns: TRUE on success or FALSE otherwise
*/
int graphics3d_findvertex(float *v, unsigned int nv, float *x, unsigned int *indx) {
    float dx[3],sep;
    if (indx) for (unsigned int i=0; i<nv; i++) {
        graphics3d_vectorsubtract(v+3*i, x, dx);
        sep=graphics3d_vectornormsq(dx);
        if (sep<MACHINE_EPSILON) {
            *indx=i;
            return TRUE;
        }
    }
    return FALSE;
}

/* Draws a sphere
Input: graphics3d_object *obj - The graphics3d object to draw in.
       float *x               - Center of the sphere
       float rad              - Radius
       float tol              - Largest edge length
       float *col             - Color of the sphere (or NULL to use a default grey)
*/
void graphics3d_sphere(graphics3d_object *obj, float x[3], float rad, float tol, float *col) {
    graphics3d_trianglecomplexelement *el=NULL;
    unsigned int nv0=12, ntri0=20, ne0=30; /* Vertices, edges, faces of the initial icosahedron */
    unsigned int nv=12, ne=30, ntri=20; /* Vertices, edges, faces of the final state */
    unsigned int nvv=0, ntt=0; /* Number of vertices and faces of intermediate states */
    unsigned int nbi=0;
    unsigned int indx;
    unsigned int mp[3]; /* Indices of the midpoint of each triangle */
    float mpx[3]; /* A midpoint */
    
    float d0 = 1.05146; /* Separation between connecting vertices on base icosahedron */
    
    /* The base element is an icosahedron */
    float v0[] = {0.,0.,-1.,0.,0.,1.,-0.894427190999916,0.,-0.44721359549995787,0.894427190999916,0.,
        0.44721359549995787,0.7236067977499789,-0.5257311121191336,-0.44721359549995787,
        0.7236067977499789,0.5257311121191336,-0.44721359549995787,-0.7236067977499789,
        -0.5257311121191336,0.44721359549995787,-0.7236067977499789,0.5257311121191336,
        0.44721359549995787,-0.276393202250021,-0.85065080835204,-0.44721359549995787,
        -0.276393202250021,0.85065080835204,-0.44721359549995787,0.276393202250021,
        -0.85065080835204,0.44721359549995787,0.276393202250021,0.85065080835204,
        0.44721359549995787};
    /* Triangles constituting the icosahedron */
    unsigned int t0[] = {1, 11, 7, 1, 7, 6, 1, 6, 10, 1, 10, 3, 1, 3, 11, 4, 8, 0, 5, 4, 0,
        9, 5, 0, 2, 9, 0, 8, 2, 0, 11, 9, 7, 7, 2, 6, 6, 8, 10, 10, 4, 3, 3,
        5, 11, 4, 10, 8, 5, 3, 4, 9, 11, 5, 2, 7, 9, 8, 6, 2};
    
    /* First, how many bisections will we need and how many elements.. */
    for (float sep=d0*0.5; sep>tol; sep*=0.5) {
        nv+=ne; /* Each edge contributes a new vertex */
        ne=2*ne+3*ntri; /* Each edge is split into two and we gain three new edges per tri*/
        ntri*=4; /* Each triangle becomes four triangles */
        nbi++;
    }
    
    /* Create the triangle complex */
    el=graphics3d_newtrianglecomplex(nv, ntri);
    
    if (el) {
        /* Copy in all the vertices from the initial set */
        for (unsigned int i=0; i<nv0; i++) {
            graphics3d_vectorcopy(v0+3*i, el->points+3*i);
        }
        nvv=nv0;
        
        /* Copy in all the triangles from the initial set */
        memcpy(el->triangles, t0, 3*ntri0*sizeof(unsigned int));
        ntt=ntri0;
        
        unsigned int v1, v2;
        
        /* Loop over bisections */
        for (unsigned int k=0; k<nbi; k++) {
            unsigned int oldntt=ntt;
            /* Loop over triangles */
            for (unsigned int i=0; i<oldntt; i++) {
                /* Loop over the three edges in each triangle */
                for (unsigned int j=0; j<3; j++) {
                    /* The relevant vertex numbers */
                    v1=el->triangles[3*i+j]; v2=el->triangles[3*i+(j<2 ? j+1 : 0)];
                    /* Calculate the midpoint */
                    graphics3d_vectoradd(el->points+3*v1, el->points+3*v2, mpx);
                    graphics3d_vectorscale(mpx, mpx, 0.5);
                    
                    /* Does this exist in the vertex list already? */
                    if (graphics3d_findvertex(el->points, nvv, mpx, &indx)) {
                        mp[j]=indx;
                    } else {
                        /* If not create a new vertex */
                        graphics3d_vectorcopy(mpx, el->points+3*nvv);
                        mp[j]=nvv;
                        nvv++;
                    }
                }
                
                /* Now we need to split the old triangle into new triangles as follows:
                            t[1]
                           / 2  \
                        mp[0] - mp[1]
                        / 1 \ 0 / 3 \
                    t[0] -- mp[2] -- t[2]
                 */
                
                /* 1 */
                el->triangles[3*ntt] = el->triangles[3*i];
                el->triangles[3*ntt+1] = mp[0];
                el->triangles[3*ntt+2] = mp[2];
                ntt++;
                
                /* 2 */
                el->triangles[3*ntt] = mp[0];
                el->triangles[3*ntt+1] = el->triangles[3*i+1];
                el->triangles[3*ntt+2] = mp[1];
                ntt++;
                
                /* 3 */
                el->triangles[3*ntt] = mp[2];
                el->triangles[3*ntt+1] = mp[1];
                el->triangles[3*ntt+2] = el->triangles[3*i+2];
                ntt++;
                
                /* Finally replace the old triangle with this one */
                el->triangles[3*i] = mp[0];
                el->triangles[3*i+1] = mp[1];
                el->triangles[3*i+2] = mp[2];
            }
        }
        
        if (nvv!=nv) {
            
        }
    
        /* Now scale and translate the vertices */
        for (unsigned int i=0; i<nv; i++) {
            graphics3d_normalizevector(el->points+3*i, el->points+3*i);
            /* (use this as the normal) */
            graphics3d_vectorcopy(el->points+3*i, el->pointnormals+3*i);
            /* Now scale */
            graphics3d_vectorscale(el->points+3*i, el->points+3*i, rad);
            /* and translate */
            graphics3d_vectoradd(el->points+3*i, x, el->points+3*i);
        }
        
        /* Set point colors */
        if (col) {
            for (unsigned i=0; i<nv; i++) memcpy(el->pointcolors+3*i, col, 3*sizeof(float));
        } else {
            for (unsigned i=0; i<3*nv; i++) el->pointcolors[i]=0.7;
        }
        
        linkedlist_addentry(obj->list, el);
    }
}

/*
 * Internal utility functions for vector arithmatic
 */

void graphics3d_normalizevector(float *src, float *dest) {
    float norm=0.0;
    for (unsigned int i=0;i<3;i++) { dest[i]=src[i]; norm+=dest[i]*dest[i]; }
    norm=sqrtf(norm);
    if (fabs(norm)>MACHINE_EPSILON) for (unsigned int i=0;i<3;i++) dest[i]/=norm;
}

void graphics3d_vectorcopy(float v1[3],float v2[3]) {
	v2[0] = v1[0];
	v2[1] = v1[1];
	v2[2] = v1[2];
	return;
}

float graphics3d_vectornorm(float v1[3]) {
    return sqrtf(v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2]);
}

float graphics3d_vectornormsq(float v1[3]) {
    return v1[0]*v1[0]+v1[1]*v1[1]+v1[2]*v1[2];
}

void graphics3d_vectorscale(float v1[3],float v2[3], float a) {
    v2[0] = a*v1[0];
    v2[1] = a*v1[1];
    v2[2] = a*v1[2];
    return;
}

void graphics3d_vectoradd(float v1[3],float v2[3],float *v3) {
	v3[0] = v1[0] + v2[0];
	v3[1] = v1[1] + v2[1];
	v3[2] = v1[2] + v2[2];
	return;
}

void graphics3d_vectorsubtract(float v1[3],float v2[3],float *v3) {
	v3[0] = v1[0] - v2[0];
	v3[1] = v1[1] - v2[1];
	v3[2] = v1[2] - v2[2];
	return;
}

void graphics3d_vectorcrossproduct(float v1[3],float v2[3],float *v3) {
	v3[0] = v1[1]*v2[2] - v1[2]*v2[1];
	v3[1] = v1[2]*v2[0] - v1[0]*v2[2];
	v3[2] = v1[0]*v2[1] - v1[1]*v2[0];
	return;
}

float graphics3d_vectordotproduct(float v1[3],float v2[3]) {
    float res=0.0;
    res = v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2];
    return res;
}


/* Builds a rotation matrix that rotates a vector about the UNIT VECTOR n by theta degrees */
void graphics3d_rotation(float *n, float theta, float *matrix) {
    float x=n[0], y=n[1], z=n[2], ctheta=cosf(theta), stheta=sinf(theta), cc;
    cc=1-ctheta;
    
    matrix[0]=x*x*cc+ctheta;
    matrix[1]=x*y*cc-z*stheta;
    matrix[2]=x*z*cc+y*stheta;
    matrix[3]=x*y*cc+z*stheta;
    matrix[4]=y*y*cc+ctheta;
    matrix[5]=y*z*cc-x*stheta;
    matrix[6]=z*x*cc-y*stheta;
    matrix[7]=y*z*cc+x*stheta;
    matrix[8]=z*z*cc+ctheta;
}

/* 3x3 matrix multiplication. Assumes matrix is given in row major order. */
void graphics3d_transformvector(float *src, float *matrix, float *dest) {
    unsigned int i,j;
    for (i=0;i<3;i++) {
        dest[i]=0.0;
        for (j=0; j<3; j++) dest[i]+=src[j]*matrix[3*i+j];
    }
}

#define g3dSWP(x,y,t) {t=y; y=x; x=t;}

/* Constructs a vector perpendicular to vector v1 (but with no guaranteed orientation) */
void graphics3d_vectorperp(float v1[3], float v2[3]) {
    unsigned int order[3]={0,1,2}, tmp;
    
    /* Find the ordering of the three components */
    if (fabs(v1[order[0]]) > fabs(v1[order[1]])) g3dSWP(order[0], order[1], tmp);
    if (fabs(v1[order[1]]) > fabs(v1[order[2]])) g3dSWP(order[1], order[2], tmp);
    if (fabs(v1[order[0]]) > fabs(v1[order[1]])) g3dSWP(order[0], order[1], tmp);
    
    v2[order[0]]=0;
    v2[order[1]]=v1[order[2]];
    v2[order[2]]=-v1[order[1]];
}

void graphics3d_trianglenormal(float *pts, unsigned int tri[3],float *norm) {
    float s1[3],s2[3];
    graphics3d_vectorsubtract(pts+3*tri[1],pts+3*tri[0],s1);
    graphics3d_vectorsubtract(pts+3*tri[2],pts+3*tri[1],s2);
    graphics3d_vectorcrossproduct(s1,s2,norm);
    graphics3d_normalizevector(norm,norm);
}

/* The 3d rotation matrix takes an initial frame, generated by viewdirection, viewdirection X viewvertical and  viewdirection X viewvertical X viewdirection
   to (z,x,y) respectively */
void graphics3d_rotationmatrix(graphics3d_object *obj, float matrix[9]) {
    graphics3d_normalizevector(obj->viewdirection,matrix+6);
    //for (unsigned int i=0;i<3;i++) matrix[6+i]=-matrix[6+i];
    
    /* viewvertical X viewdirection -> x */
    graphics3d_vectorcrossproduct(obj->viewvertical,matrix+6,matrix);
    graphics3d_normalizevector(matrix,matrix);
    
    /* viewdirection X x -> y */
    graphics3d_vectorcrossproduct(matrix+6,matrix,matrix+3);
    graphics3d_normalizevector(matrix+3,matrix+3);
    
    //for (unsigned int i=0;i<9;i++) printf("%f ",matrix[i]);
    //printf("\n");
}

void graphics3d_rotatepoint(float *src, float *matrix, float *dest) {
    unsigned int i,j;
    for (i=0;i<3;i++) {
        dest[i]=0.0;
        for (j=0; j<3; j++) dest[i]+=src[j]*matrix[3*i+j];
    }
}

void graphics3d_projectpoint(float *src, float *dest) {
    dest[0]=(float) 100.0*src[0];
    dest[1]=(float) 100.0*src[1];
    dest[2]=src[2];
}

float *graphics3D_trianglepts;
unsigned int *graphics3D_triangleverts;
int graphics3D_trianglezcomparison(const void* p1, const void* p2) {
    unsigned int i=*(unsigned int *) p1;
    unsigned int j=*(unsigned int *) p2;
    
    float z1=0.0;
    float z2=0.0;
    
    /* Compute the z position of the center of mass of each triangle */
    for (unsigned int k=0;k<2;k++) {
        z1+=graphics3D_trianglepts[3*graphics3D_triangleverts[3*i+k]+2];
        z2+=graphics3D_trianglepts[3*graphics3D_triangleverts[3*j+k]+2];
    }
    
    if (z1<z2) return -1;
    if (z1>z2) return 1;
    
    return 0;
};

int graphics3D_trianglenormalfacescamera(float *p, unsigned int *tri) {
    float s1[2];
    float s2[2];
    float znorm;
    unsigned int i;
    
    /* s0 = v1-v0 */
    for (i=0;i<2;i++) s1[i]=p[3*tri[1]+i]-p[3*tri[0]+i];
    /* s1 = v2-v1 */
    for (i=0;i<2;i++) s2[i]=p[3*tri[2]+i]-p[3*tri[1]+i];
    
    /* Calculate z-component of cross product and check if positive (i.e. facing away from us) */
    znorm = (s1[0]*s2[1] - s1[1]*s2[0]);
    
    if (znorm > 0) return TRUE;
    return FALSE;
}

/*
 * Calculate bounding box
 */

void graphics3d_expandbbox(graphics3d_bbox *bbox, float *pt) {
    if (pt[0]<bbox->p1[0]) bbox->p1[0]=pt[0];
    if (pt[1]<bbox->p1[1]) bbox->p1[1]=pt[1];
    if (pt[2]<bbox->p1[2]) bbox->p1[2]=pt[2];
    if (pt[0]>bbox->p2[0]) bbox->p2[0]=pt[0];
    if (pt[1]>bbox->p2[1]) bbox->p2[1]=pt[1];
    if (pt[2]>bbox->p2[2]) bbox->p2[2]=pt[2];
}

void graphics3d_calculatebbox(graphics3d_object *g, graphics3d_bbox *bbox) {
    //    float lw=1.0;
    graphics3d_element *el=NULL;
    int first=TRUE;
    
    if (!g) return;
    
    for (unsigned int i=0; i<3; i++) {
        bbox->p1[i]=0.0; bbox->p2[i]=0.0;
    }
    
    for (linkedlistentry *e=g->list->first; e!=NULL; e=e->next) {
        el=(graphics3d_element *) e->data;
        switch (el->type) {
            case GRAPHICS3D_POINT:
            case GRAPHICS3D_MOVETO:
            case GRAPHICS3D_LINETO:
                {
                    graphics3d_pointelement *pel=(graphics3d_pointelement *) el;
                    if (first) {
                        for (unsigned int i=0; i<3; i++) {
                            bbox->p1[i]=pel->point[i];
                            bbox->p2[i]=pel->point[i];
                        }
                        first=FALSE;
                    } else {
                        graphics3d_expandbbox(bbox,pel->point);
                    }
                }
                break;
            case GRAPHICS3D_TEXT:
            {
                /* To do */
            }
                break;
            case GRAPHICS3D_TRIANGLE:
            {
                graphics3d_triangleelement *gel = (graphics3d_triangleelement *) el;
                if (first) {
                    for (unsigned int i=0; i<3; i++) {
                        bbox->p1[i]=gel->p1[i];
                        bbox->p2[i]=gel->p2[i];
                    }
                    first=FALSE;
                } else {
                    graphics3d_expandbbox(bbox,gel->p1);
                }
                graphics3d_expandbbox(bbox,gel->p2);
                graphics3d_expandbbox(bbox,gel->p3);
            }
                break;
            case GRAPHICS3D_TRIANGLE_COMPLEX:
            {
                graphics3d_trianglecomplexelement *tel = (graphics3d_trianglecomplexelement *) el;
                
                if (tel->npoints>0 && tel->points) {
                    if (first) {
                        for (unsigned int i=0; i<3; i++) {
                            bbox->p1[i]=tel->points[i];
                            bbox->p2[i]=tel->points[i];
                        }
                        first=FALSE;
                    }
                    
                    for (unsigned int i=0; i<tel->npoints; i++) {
                        graphics3d_expandbbox(bbox, tel->points + 3*i);
                    }
                }
            }
                break;
            case GRAPHICS3D_STROKE:
            case GRAPHICS3D_SETCOLOR:
                break;
        }
    }
}


/*
 * Flattening to a graphics object.
 */

typedef struct {
    linkedlistentry *col;
    linkedlistentry *start;
    linkedlistentry *end;
    float z;
    unsigned int n;
} graphics3d_sortelement;

void graphics3d_writesortelements(graphics3d_object *obj, graphics3d_sortelement *srt, unsigned int length, float *rmatrix, unsigned int *nel) {
    linkedlistentry *col=NULL, *lst=NULL;
    float p[3];
    unsigned int n=0, m=0;
    
    /* Initialize the sort elements */
    for (unsigned int i=0; i<length; i++) { srt[i].col=NULL; srt[i].start=NULL; srt[i].end=NULL; srt[i].z=0; srt[i].n=1000+i; }
    
    /* Now loop over the list */
    for (linkedlistentry *e=obj->list->first; e!=NULL; e=e->next) {
        graphics3d_element *el = (graphics3d_element *) e->data;
        
        //printf("%u ", n);
        
        if (!srt[n].start) { srt[n].start=e; m=0; }
        if (!srt[n].col) srt[n].col=col;
        srt[n].n=n;
        
        if (el) switch (el->type) {
            case GRAPHICS3D_STROKE:
                //printf("stroke.\n");
                if (m>0) srt[n].z /= (float) m;
                srt[n].end=e; n++; /* Move to next entry */
                break;
            case GRAPHICS3D_SETCOLOR:
                //printf("setcolor\n");
                //if (srt[n].start==e) srt[n].start=NULL;
                col=e;
                break;
            case GRAPHICS3D_TEXT:
                //printf("text\n");
                graphics3d_rotatepoint(((graphics3d_textelement *) el)->point,rmatrix,p);
                srt[n].z=p[2];
                srt[n].end=e; n++; /* Move to next entry */
                break;
            case GRAPHICS3D_POINT:
                //printf("point\n");
                graphics3d_rotatepoint(((graphics3d_pointelement *) el)->point,rmatrix,p);
                srt[n].z=p[2];
                srt[n].end=e; n++; /* Move to next entry */
                break;
            case GRAPHICS3D_LINETO:
                //printf("lineto\n");
                graphics3d_rotatepoint(((graphics3d_pointelement *) el)->point,rmatrix,p);
                srt[n].z+=p[2]; m++;
                break;
            case GRAPHICS3D_MOVETO:
                //printf("moveto\n");
                graphics3d_rotatepoint(((graphics3d_pointelement *) el)->point,rmatrix,p);
                srt[n].z+=p[2]; m++;
                break;
            case GRAPHICS3D_TRIANGLE:
                //printf("triangle\n");
                graphics3d_rotatepoint(((graphics3d_triangleelement *) el)->p1,rmatrix,p);
                srt[n].z=p[2];
                graphics3d_rotatepoint(((graphics3d_triangleelement *) el)->p2,rmatrix,p);
                srt[n].z+=p[2];
                graphics3d_rotatepoint(((graphics3d_triangleelement *) el)->p3,rmatrix,p);
                srt[n].z+=p[2];
                srt[n].z /= 3.0; /* Center of mass */
                srt[n].end=e; n++; /* Move to next entry */
                break;
            case GRAPHICS3D_TRIANGLE_COMPLEX:
            {
                graphics3d_trianglecomplexelement *tel = (graphics3d_trianglecomplexelement *) e->data;
                //if (srt[n].start!=e) { srt[n].end=lst; n++; } /* If we aren't starting with a fresh entry we should be */
                //printf("trianglecomplex\n");
                
                /* Scan over triangles in the trianglecomplex */
                for (unsigned int i=0; i<tel->ntriangles; i++) {
                    float zz=0;
                    
                    for (unsigned int j=0; j<3; j++) {
                        graphics3d_rotatepoint(tel->points+3*tel->triangles[3*i+j],rmatrix,p);
                        zz+=p[2];
                    }

                    srt[n].z=zz/3.0;
                }
                if (tel->ntriangles>0) srt[n].z /= ((float) tel->ntriangles);
                
                srt[n].end=e; n++; /* Move to next entry */
            }
                break;
        }
        
        lst=e;
    }
    *nel=n;
}

int graphics3d_sortelementcomparison(const void* p1, const void* p2) {
    graphics3d_sortelement *e1 = (graphics3d_sortelement *) p1;
    graphics3d_sortelement *e2 = (graphics3d_sortelement *) p2;
    int ret=0;
    
    if (e1->z<e2->z) ret=-1;
    if (e1->z>e2->z) ret=1;
        
    return ret;
}

/* Writes the elements of a list of graphics3d_sortelements to a target graphics_object */
void graphics3d_sortelementstographics(graphics3d_object *obj, graphics_object *target, graphics3d_sortelement *srt, unsigned int nel, float *rmatrix) {
    float p[3],q[3];
    
    for (unsigned int i=0; i<nel; i++) {
        int needsstroke=FALSE;
        
       /* if (srt[i].col) {
            graphics3d_setcolorelement *scol = (graphics3d_setcolorelement *) srt[i].col->data;
            if (scol) graphics_setcolor(target, scol->r, scol->g, scol->b);
        }*/
        
        //printf("Group %u %f.\n", i, srt[i].z);
        
        for (linkedlistentry *e=srt[i].start; e!=NULL; e=e->next) {
            graphics3d_element *el = (graphics3d_element *) e->data;
            
            if (el) switch (el->type) {
                case GRAPHICS3D_SETCOLOR:
                {
                    graphics3d_setcolorelement *scol = (graphics3d_setcolorelement *) el;
                    graphics_setcolor(target, scol->r, scol->g, scol->b);
                    //printf("-Setcolor.\n");
                }
                    break;
                case GRAPHICS3D_STROKE:
                    //printf("-stroke\n");
                    //graphics_stroke(target);
                    break;
                case GRAPHICS3D_TEXT:
                {
                    graphics3d_textelement *tel = (graphics3d_textelement *) el;
                    //printf("-text\n");
                    graphics3d_rotatepoint(tel->point,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    graphics_text(target, tel->string, q, tel->horizontal, tel->vertical);
                }
                    break;
                case GRAPHICS3D_POINT:
                {
                    graphics3d_pointelement *pel = (graphics3d_pointelement *) e->data;
                    graphics3d_rotatepoint(pel->point,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    graphics_point(target,q);
                    //printf("-point %f %f %f (%f %f %f)  \n", q[0], q[1], q[2], pel->point[0], pel->point[1], pel->point[2]);
                    needsstroke=TRUE;
                }
                    break;
                case GRAPHICS3D_LINETO:
                {
                    graphics3d_pointelement *pel = (graphics3d_pointelement *) e->data;
                    graphics3d_rotatepoint(pel->point,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    graphics_lineto(target,q);
                    //printf("-lineto %f %f %f\n", q[0], q[1], q[2]);
                    needsstroke=TRUE;
                }
                    break;
                case GRAPHICS3D_MOVETO:
                {
                    graphics3d_pointelement *pel = (graphics3d_pointelement *) e->data;
                    graphics3d_rotatepoint(pel->point,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    graphics_moveto(target,q);
                    //printf("-moveto %f %f %f\n", q[0], q[1], q[2]);
                    needsstroke=TRUE;
                }
                    break;
                case GRAPHICS3D_TRIANGLE:
                {
                    graphics3d_triangleelement *el=(graphics3d_triangleelement *) e->data;
                    //printf("-triangle ");
                    graphics3d_rotatepoint(el->p1,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    //printf(" ( %f,%f,%f ) ", p[0], p[1], p[2]);
                    graphics_moveto(target, q);
                    graphics3d_rotatepoint(el->p2,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    //printf(" ( %f,%f,%f ) ", p[0], p[1], p[2]);
                    graphics_lineto(target, q);
                    graphics3d_rotatepoint(el->p3,rmatrix,p);
                    graphics3d_projectpoint(p,q);
                    //printf(" ( %f,%f,%f ) ", p[0], p[1], p[2]);
                    graphics_lineto(target, q);
                    //printf("\n");
                    //graphics_savestate(target);
                    //graphics_setcolor(target, 1.0, 0.0, 0.0);
                    graphics_fill(target);
                    //graphics_restorestate(target);
                    //graphics_setcolor(target, 0.0, 0.0, 0.0);
                    //graphics_stroke(target);
                }
                    break;
                case GRAPHICS3D_TRIANGLE_COMPLEX:
                {
                    //printf("-trianglecomplex\n");
                    graphics3d_trianglecomplexelement *tel = (graphics3d_trianglecomplexelement *) e->data;
                    float pt[tel->npoints*3];
                    
                    /* Rotate and project all the points in the triangle complex */
                    for (unsigned int i=0; i<tel->npoints; i++) {
                        graphics3d_rotatepoint(tel->points+3*i,rmatrix,pt+3*i);
                        graphics3d_projectpoint(pt+3*i,pt+3*i);
                    }
                    
                    float *shade = graphics_shadetriangles(target, tel->ntriangles);
                    
                    if (shade) {
                        unsigned int j=0;
                        
                        for (unsigned int i=0; i<tel->ntriangles; i++) {
                            // Loop over vertices in triangle
                            for (unsigned int k=0;k<3;k++) {
                                // (x,y) coordinates
                                shade[j*5]=pt[3*tel->triangles[3*i+k]];
                                shade[j*5+1]=pt[3*tel->triangles[3*i+k]+1];
                                
                                // Copy the colors across
                                shade[j*5+2]=tel->pointcolors[3*tel->triangles[3*i+k]];
                                shade[j*5+3]=tel->pointcolors[3*tel->triangles[3*i+k]+1];
                                shade[j*5+4]=tel->pointcolors[3*tel->triangles[3*i+k]+2];
                                j++;
                            }
                        }
                    }
                    
                    // TODO: Could qsort these. 
                }
                    break;
            }
            
            if (e==srt[i].end) break;
        }
        
        if (needsstroke) graphics_stroke(target);
    }
}

expression *graphics3d_tographics(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics3d_object *src = (graphics3d_object *) obj;
    expression_objectreference *g=NULL;
    float rmatrix[9];
    unsigned int listlength=0, nel=0;
    graphics3d_sortelement *srt=NULL;
    
    /* Set up transformation matrices */
    graphics3d_rotationmatrix((graphics3d_object *) obj,rmatrix);
    
    /* Count elements */
    if (src->list) listlength=linkedlist_length(src->list);

    if (listlength>0) srt=EVAL_MALLOC(listlength*sizeof(graphics3d_sortelement));
    
    if (srt) {
        graphics3d_writesortelements(src, srt, listlength, rmatrix, &nel);
        qsort(srt, nel, sizeof(graphics3d_sortelement), graphics3d_sortelementcomparison);
    }

    /* Create the destination graphics object */
    g=(expression_objectreference *) graphics_newgraphics();
    
    if ((g)&&(srt)) {
        graphics_object *dest=(graphics_object *) g->obj;
        
        graphics3d_sortelementstographics(src, dest, srt, nel, rmatrix);
    }
    
    if (srt) EVAL_FREE(srt);
    
    return (expression *) g;
}


/*
 * Selectors
 */

/* Frees a graphics3d element and any associated data */
void graphics3d_freeelement(graphics3d_element *element) {
    if (element) switch (element->type) {
        case GRAPHICS3D_TEXT:
            EVAL_FREE(((graphics3d_textelement *) element)->string);
            break;
        case GRAPHICS3D_TRIANGLE_COMPLEX:
            {
                graphics3d_trianglecomplexelement *cel = (graphics3d_trianglecomplexelement *) element;
                
                if (cel->points) EVAL_FREE(cel->points);
                if (cel->triangles) EVAL_FREE(cel->triangles);
                if (cel->pointcolors) EVAL_FREE(cel->pointcolors);
                if (cel->pointnormals) EVAL_FREE(cel->pointnormals);
            }
            break;
        default:
            break;
    }
    
    if (element) EVAL_FREE(element);
}

expression *graphics3d_free(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (context==NULL) {
        graphics3d_object *gobj=(graphics3d_object *) obj;
        if (gobj) if (gobj->list) {
            linkedlist_map(gobj->list, (linkedlistmapfunction *) graphics3d_freeelement);
            linkedlist_free(gobj->list);
        }
    }
    return NULL;
}

expression *graphics3d_getviewverticali(object *obj, interpretcontext *context, int nargs, expression **args) {
    return (expression *) eval_listfromfloatarray(((graphics3d_object *) obj)->viewvertical,3);
}


expression *graphics3d_setviewverticali(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((nargs==1)&&(args[0]->type==EXPRESSION_LIST)) eval_listtofloatarray((expression_list *) args[0],((graphics3d_object *) obj)->viewvertical,3);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_getviewdirectioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    return (expression *) eval_listfromfloatarray(((graphics3d_object *) obj)->viewdirection,3);
}

expression *graphics3d_setviewdirectioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((nargs==1)&&(args[0]->type==EXPRESSION_LIST)) eval_listtofloatarray((expression_list *) args[0],((graphics3d_object *) obj)->viewdirection,3);

    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_getnormalclippingi(object *obj, interpretcontext *context, int nargs, expression **args) {
    return (expression *) newexpressionbool(((graphics3d_object *) obj)->cliptrianglesbynormal);
}

expression *graphics3d_setnormalclippingi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((nargs==1)&&(args[0]->type==EXPRESSION_BOOL)) ((graphics3d_object *) obj)->cliptrianglesbynormal = eval_boolvalue(args[0]);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_getshowmeshi(object *obj, interpretcontext *context, int nargs, expression **args) {
    return (expression *) newexpressionbool(((graphics3d_object *) obj)->showmesh);
}

expression *graphics3d_setshowmeshi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if ((nargs==1)&&(args[0]->type==EXPRESSION_BOOL)) ((graphics3d_object *) obj)->showmesh = eval_boolvalue(args[0]);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_pointi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    float p[3];
    
    list=(expression_list *) eval_argstolist(nargs,args);
    
    if (graphics_validate_pointlist(list,3)) {
        for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
            expression_list *lst=(expression_list *) e->data;
            
            if (eval_listtofloatarray(lst, p, 3)) graphics3d_point((graphics3d_object *) obj, p);
        }
    }
    
    linkedlist_free(list->list); EVAL_FREE(list);
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_linei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_list *list=NULL;
    
    if (nargs==1) {
        list=(expression_list *) args[0];
    } else {
        list=(expression_list *) eval_argstolist(nargs,args);
    }
    
    if (graphics_validate_pointlist(list,3)) {
        graphics3d_line((graphics3d_object *) obj, list->list);
    }
    
    if(nargs!=1) { linkedlist_free(list->list); EVAL_FREE(list); };
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_arrowi(object *obj, interpretcontext *context, int nargs, expression **args) {
    float x1[3]={0,0,0}, x2[3]={0,0,0}, ar=0.1;
    float colf[3];
    double cold[3];
    int colset=FALSE;
    
    if (nargs>=2) {
        if (eval_islist(args[0])) eval_listtofloatarray((expression_list *) args[0], x1, 3);
               if (eval_islist(args[1])) eval_listtofloatarray((expression_list *) args[1], x2, 3);
               for (unsigned int i=2; i<nargs; i++) {
                   if (color_iscolor(args[i])) {
                       if (color_rgbfromobjectref(context, args[i], cold)) {
                           for (unsigned int j=0; j<3; j++) colf[j]=(float) cold[j];
                           colset=TRUE;
                       }
                   } else if (eval_isreal(args[i])) ar = (float) eval_floatvalue(args[i]);
               }
        
        graphics3d_arrow((graphics3d_object *) obj, x1, x2, ar, 20, (colset ? colf : NULL));
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_cylinderi(object *obj, interpretcontext *context, int nargs, expression **args) {
    float x1[3]={0,0,0}, x2[3]={0,0,0}, ar=0.1;
    float colf[3];
    double cold[3];
    int colset=FALSE;
    
    if (nargs>=2) {
        if (eval_islist(args[0])) eval_listtofloatarray((expression_list *) args[0], x1, 3);
        if (eval_islist(args[1])) eval_listtofloatarray((expression_list *) args[1], x2, 3);
        for (unsigned int i=2; i<nargs; i++) {
            if (color_iscolor(args[i])) {
                if (color_rgbfromobjectref(context, args[i], cold)) {
                    for (unsigned int j=0; j<3; j++) colf[j]=(float) cold[j];
                    colset=TRUE;
                }
            } else if (eval_isreal(args[i])) ar = (float) eval_floatvalue(args[i]);
        }
                
        graphics3d_cylinder((graphics3d_object *) obj, x1, x2, ar, 20, (colset ? colf : NULL));
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_tubei(object *obj, interpretcontext *context, int nargs, expression **args) {
    float x[nargs*3], rad=0.1;
    unsigned int nv = nargs;
    float colf[3];
    double cold[3];
    int colset=FALSE;
    
    if (nargs>=2) {
        for (unsigned int i=0; i<nargs; i++) {
            if (eval_islist(args[i])) eval_listtofloatarray((expression_list *) args[i], x+3*i, 3);
            if (nargs>2 && eval_isreal(args[i])) {
                rad = (float) eval_floatvalue(args[i]);
                nv--;
            }
            if (color_iscolor(args[i])) {
                if (color_rgbfromobjectref(context, args[i], cold)) {
                    for (unsigned int j=0; j<3; j++) colf[j]=(float) cold[j];
                    colset=TRUE;
                }
                nv--;
            }
        }
        
        graphics3d_tube((graphics3d_object *) obj, x, nv, rad, FALSE, 20, (colset ? colf : NULL));
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_spherei(object *obj, interpretcontext *context, int nargs, expression **args) {
    float x[3], rad=0.1;
    float colf[3];
    double cold[3];
    int colset=FALSE;
    
    if (nargs>0) {
        for (unsigned int i=0; i<nargs; i++) {
            if (eval_islist(args[i])) eval_listtofloatarray((expression_list *) args[i], x, 3);
            if (eval_isreal(args[i])) rad = (float) eval_floatvalue(args[i]);
            if (color_iscolor(args[i])) {
                if (color_rgbfromobjectref(context, args[i], cold)) {
                    for (unsigned int j=0; j<3; j++) colf[j]=(float) cold[j];
                    colset=TRUE;
                }
            }
        }
        
        graphics3d_sphere((graphics3d_object *) obj, x, rad, 0.1, (colset ? colf : NULL));
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *graphics3d_setcolori(object *obj, interpretcontext *context, int nargs, expression **args) {
    double col[3];
    int success=TRUE;
    
    if (nargs==3) {
        for (unsigned int i=0; i<2; i++) {
            if (eval_isreal(args[i])) {
                col[i]= eval_floatvalue(args[i]);
            } else {
                success=FALSE;
            }
        }
    } else if (nargs==1) {
        expression_objectreference *ref=(expression_objectreference *) args[0];
        
        if (eval_isobjectref(ref)) {
            if (!color_rgbfromobject(context, ref->obj, 0, NULL, col)) {
                success=FALSE;
            }
        }
    }
    
    if (success) graphics3d_setcolor((graphics3d_object *) obj, (float) col[0], (float) col[1], (float) col[2]);
    
    return (expression *) class_newobjectreference(obj);
}

/* Adds text to the graphics3d object */
expression *graphics3d_texti(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (nargs!=2) return NULL;
    if (!eval_islist(args[0])) {
        error_raise(context, GRAPHICS3D_TEXT_ARGS, GRAPHICS3D_TEXT_ARGS_MSG, ERROR_FATAL); return NULL;
    }
    if (!eval_isstring(args[1])) {
        error_raise(context, GRAPHICS3D_TEXTSTRING_ARGS, GRAPHICS3D_TEXTSTRING_ARGS_MSG, ERROR_FATAL);  return NULL;
    }
    float pt[3];
    if(eval_listtofloatarray((expression_list *) args[0],pt,3)) {
        graphics3d_text((graphics3d_object *) obj, ((expression_string *)args[1])->value, pt, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNMIDDLE);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Adds a triangle */
expression *graphics3d_trianglei(object *obj, interpretcontext *context, int nargs, expression **args) {
    double pts[9];
    unsigned int i;
    
    if (nargs==3) {
        for (i=0;i<3;i++) {
            if (!(eval_islist(args[i]))||(!eval_listtodoublearray((expression_list *) args[i], pts+3*i, 3))) {
                error_raise(context, GRAPHICS3D_TRIANGLE_ARGS, GRAPHICS3D_TRIANGLE_ARGS_MSG, ERROR_FATAL);
                return NULL;
            }
        }
        graphics3d_trianglefromdouble((graphics3d_object *) obj, pts, pts+3, pts+6);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Serialization */

/* Serializes a graphics3d object to a hashtable */
void graphics3d_serializetohashtable(graphics3d_object *gobj, hashtable *ht) {
    expression_list *list=newexpressionlist(NULL);
    expression *farg=NULL;
    
    if (list) {
        linkedlist *lst=list->list;
        
        for (linkedlistentry *e=gobj->list->first; e!=NULL; e=e->next) {
            graphics3d_element *el=(graphics3d_element *) e->data;
            
            switch (el->type) {
                case GRAPHICS3D_POINT:
                    farg=eval_listfromfloatarray(((graphics3d_pointelement *) el)->point, 3);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_POINT_SELECTOR), 1, &farg, FALSE));
                case GRAPHICS3D_MOVETO:
                    farg=eval_listfromfloatarray(((graphics3d_pointelement *) el)->point, 3);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_MOVETO_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS3D_LINETO:
                    farg=eval_listfromfloatarray(((graphics3d_pointelement *) el)->point, 3);
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_LINETO_SELECTOR), 1, &farg, FALSE));
                    break;
                case GRAPHICS3D_STROKE:
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_STROKE_SELECTOR), 0, NULL, FALSE));
                    break;
                case GRAPHICS3D_TEXT:
                {
                    graphics3d_textelement *tel = (graphics3d_textelement *) el;
                    expression* targs[4];
                    char *align, *valign;
                    char alignstring[32];
                    
                    targs[0] = (expression *) newexpressionstring(tel->string);
                    targs[1] = (expression *) eval_listfromfloatarray(tel->point, 3);
                    switch (tel->horizontal) {
                        case GRAPHICS_ALIGNLEFT: align = "left"; break;
                        case GRAPHICS_ALIGNRIGHT: align = "right"; break;
                        case GRAPHICS_ALIGNCENTER: align = "center"; break;
                        default: align=""; break;
                    }
                    sprintf(alignstring, "align=%s", align);
                    targs[2] = (expression *) newexpressionstring(alignstring);
                    switch (tel->vertical) {
                        case GRAPHICS_ALIGNTOP: valign = "top"; break;
                        case GRAPHICS_ALIGNBOTTOM: valign = "bottom"; break;
                        case GRAPHICS_ALIGNMIDDLE: valign = "middle"; break;
                        default: valign=""; break;
                    }
                    sprintf(alignstring, "valign=%s", valign);
                    targs[3] = (expression *) newexpressionstring(alignstring);
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_TEXT_SELECTOR), 4, targs, FALSE));
                }
                    break;
                case GRAPHICS3D_SETCOLOR:
                {
                    graphics3d_setcolorelement *scel = (graphics3d_setcolorelement *) el;
                    expression* scargs[3];
                    
                    scargs[0]=newexpressionfloat((double) scel->r);
                    scargs[1]=newexpressionfloat((double) scel->g);
                    scargs[2]=newexpressionfloat((double) scel->b);
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_SETCOLOR_SELECTOR), 3, scargs, FALSE));
                }
                    break;
                case GRAPHICS3D_TRIANGLE:
                {
                    graphics3d_triangleelement *tel = (graphics3d_triangleelement *) el;
                    expression* targs[3];
                    
                    targs[0] = (expression *) eval_listfromfloatarray(tel->p1, 3);
                    targs[1] = (expression *) eval_listfromfloatarray(tel->p2, 3);
                    targs[2] = (expression *) eval_listfromfloatarray(tel->p3, 3);
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_TRIANGLE_SELECTOR), 3, targs, FALSE));
                }
                    break;
                case GRAPHICS3D_TRIANGLE_COMPLEX:
                {
//                    graphics3d_trianglecomplexelement *tcel = (graphics3d_trianglecomplexelement *) el;
                    expression* targs[3];
                    
                    
//                    eval_listfromunsignedintegerarray(tcel->triangles[], 3);
                    
                    /*tcel->points;
                    tcel->pointcolors;
                    tcel->pointnormals;
                    tcel->triangles;*/
                    
                    linkedlist_addentry(lst, newexpressionfunction(newexpressionname(GRAPHICS3D_TRIANGLE_SELECTOR), 3, targs, FALSE));
                }
                    break;
            }
        }
        
        hashinsert(ht, GRAPHICS_DISPLAYLIST, list);
    }
}

/* Serializes a graphics3d object  */
expression *graphics3d_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(6);
    graphics3d_object *gobj=(graphics3d_object *) obj;
    expression *ret=NULL;
    
    if (ht) {
        graphics3d_serializetohashtable(gobj, ht);
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
        
    }
    
    return ret;
}

/*
 * Cloning
 */

/* Copies the contents of one graphics3d object to another */
int graphics3d_copy(graphics3d_object *src, graphics3d_object *target) {
    if (src->list) for (linkedlistentry *e = src->list->first; e!=NULL; e=e->next) {
        graphics3d_element *el = (graphics3d_element *) e->data;
        
        if (el) {
            graphics3d_element *new = graphics3d_cloneelement(el);
            if (new && target->list) linkedlist_addentry(target->list, new);
        }
    }
    
    return TRUE;
}

/* Clones a graphics3d object */
expression *graphics3d_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics3d_object *gobj=(graphics3d_object *) obj;
    expression *ret = graphics3d_newgraphics3d();
    
    if (eval_isobjectref(ret)) {
        graphics3d_object *new = (graphics3d_object *) eval_objectfromref(ret);
        
        if (!graphics3d_copy(gobj, new)) {
            freeexpression(ret); ret=NULL;
            error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
        }
    }
    
    return ret;
}

/*
 * Joins two graphics3d objects together
 */

expression *graphics3d_joini(object *obj, interpretcontext *context, int nargs, expression **args) {
    graphics3d_object *gobj=(graphics3d_object *) obj;
    graphics3d_object *add=NULL;
    
    if (nargs==1 && eval_isobjectref(args[0])) {
        if (class_objectisofclass(args[0], obj->clss->name)) {
            add = (graphics3d_object *) eval_objectfromref(args[0]);
        }
    }
    
    if (!add) {
        error_raise(context, GRAPHICS3D_JOIN_ARGS, GRAPHICS3D_JOIN_ARGS_MSG, ERROR_FATAL);
        return NULL;
    }
    
    expression *ret = graphics3d_newgraphics3d();
    
    if (eval_isobjectref(ret)) {
        graphics3d_object *new = (graphics3d_object *) eval_objectfromref(ret);
        
        for (unsigned int i=0; i<3; i++) {
            new->viewdirection[i]=gobj->viewdirection[i];
            new->viewvertical[i]=gobj->viewvertical[i];
        }
        
        if (!(graphics3d_copy(gobj, new) && graphics3d_copy(add, new))) {
            freeexpression(ret); ret=NULL;
            error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
        }
        
    }
    
    return ret;
}



/*
 * Initialization
 */

void graphics3dinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(GRAPHICS3D_LABEL, class_lookup(EVAL_OBJECT), sizeof(graphics3d_object), 24);
    class_registerselector(cls, GRAPHICS3D_POINT_SELECTOR, FALSE, graphics3d_pointi);
    class_registerselector(cls, GRAPHICS3D_LINE_SELECTOR, FALSE, graphics3d_linei);
    class_registerselector(cls, GRAPHICS3D_ARROW_SELECTOR, FALSE, graphics3d_arrowi);
    class_registerselector(cls, GRAPHICS3D_CYLINDER_SELECTOR, FALSE, graphics3d_cylinderi);
    class_registerselector(cls, GRAPHICS3D_SPHERE_SELECTOR, FALSE, graphics3d_spherei);
    class_registerselector(cls, GRAPHICS3D_TUBE_SELECTOR, FALSE, graphics3d_tubei);
    class_registerselector(cls, GRAPHICS3D_SETCOLOR_SELECTOR, FALSE, graphics3d_setcolori);
    class_registerselector(cls, GRAPHICS3D_TEXT_SELECTOR, FALSE, graphics3d_texti);
    class_registerselector(cls, GRAPHICS3D_TRIANGLE_SELECTOR, FALSE, graphics3d_trianglei);

    class_registerselector(cls, GRAPHICS3D_TOGRAPHICS_SELECTOR, FALSE, graphics3d_tographics);
    class_registerselector(cls, GRAPHICS3D_VIEWDIRECTION_SELECTOR, FALSE, graphics3d_getviewdirectioni);
    class_registerselector(cls, GRAPHICS3D_SETVIEWDIRECTION_SELECTOR, FALSE, graphics3d_setviewdirectioni);
    class_registerselector(cls, GRAPHICS3D_VIEWVERTICAL_SELECTOR, FALSE, graphics3d_getviewverticali);
    class_registerselector(cls, GRAPHICS3D_SETVIEWVERTICAL_SELECTOR, FALSE, graphics3d_setviewverticali);
    class_registerselector(cls, GRAPHICS3D_NORMALCLIPPING_SELECTOR, FALSE, graphics3d_getnormalclippingi);
    class_registerselector(cls, GRAPHICS3D_SETNORMALCLIPPING_SELECTOR, FALSE, graphics3d_setnormalclippingi);
    class_registerselector(cls, GRAPHICS3D_SHOWMESH_SELECTOR, FALSE, graphics3d_getshowmeshi);
    class_registerselector(cls, GRAPHICS3D_SETSHOWMESH_SELECTOR, FALSE, graphics3d_setshowmeshi);
    
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, graphics3d_newi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, graphics3d_serializei);
    class_registerselector(cls, EVAL_CLONE, FALSE, graphics3d_clonei);
    class_registerselector(cls, EVAL_ADDSELECTORLABEL, FALSE, graphics3d_joini);
    class_registerselector(cls, GRAPHICS3D_JOIN_SELECTOR, FALSE, graphics3d_joini);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, graphics3d_free);
}
