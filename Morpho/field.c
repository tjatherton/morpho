/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "field.h"

/*
 * Field low level operations
 */

expression *field_new(manifold_object *manifold, unsigned int grade) {
    field_object *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(field_object *) class_instantiate(class_lookup(GEOMETRY_FIELDCLASS));
    
    if (obj) {
        obj->manifold=class_newobjectreference((object *) manifold);
        obj->grade=grade;
        
        ref=class_newobjectreference((object *) obj);
        
        /* Add a listener to the manifold so we hear about when it's been updated. */
        {
            expression_name *refine = (expression_name *) newexpressionname(MANIFOLD_REFINE);
            expression_objectreference *sref=class_newobjectreference((object *) obj);
            
            class_addlistener((object *) manifold, refine, sref, refine);
            
            if (refine) freeexpression((expression *) refine);
            if (sref) freeexpression((expression *) sref);
        }
    }
    
    return (expression *) ref;
}

void field_insertwithid(field_object *field, uid id, expression *value) {
    if (field) {
        /* Has this been fixed? */
        if (field->fix) {
            if (idtable_ispresent(field->fix, id)) return;
        }
        if (!field->entries) field->entries = idtable_create(GEOMETRY_FIELD_DEFAULTSIZE);
        
        if (field->entries) {
            /* If the entry already exists we need to free it */
            expression *exp = idtable_get(field->entries, id);
            if (exp) freeexpression(exp);
            idtable_insertwithid(field->entries, id, value);
        }
    }
}

/* Adds a value to a field (on the right). 
   IMPORTANT: This function takes a copy of your expression so you must free it after */
void field_accumulatewithid(field_object *field, interpretcontext *context, uid id, expression *value) {
    expression *exist=NULL;
    expression *res=NULL;
    
    if (field) {
        if (field->fix) {
            if (idtable_ispresent(field->fix, id)) return;
        }
        
        if (!field->entries) field->entries = idtable_create(GEOMETRY_FIELD_DEFAULTSIZE);
        
        if (field->entries) {
            exist = idtable_get(field->entries, id);
            
            if (exist) {
                res = opadd(context, exist, value);
                if (res) idtable_insertwithid(field->entries, id, res);
                freeexpression(exist);
            } else {
                idtable_insertwithid(field->entries, id, cloneexpression(value));
            }
        }
    }
}

expression *field_get(field_object *field, uid id) {
    expression *ret=NULL;
    
    if ((field)&&(field->entries)) {
        ret = (expression * ) idtable_get(field->entries, id);
    }
    
    return ret;
}

/* Fix and unfix elements */

void field_fix(field_object *field, uid id) {
    if (field && field->entries) {
        if (!field->fix) field->fix = idtable_create(field->entries->nentries);
        
        if (field->fix) idtable_insertwithid(field->fix, id, NULL);
    }
}

void field_unfix(field_object *field, uid id) {
    if (field && field->fix) {
        if (idtable_ispresent(field->fix, id)) idtable_remove(field->fix, id);
    }
}

void field_fixwithselectionmapfunction (uid id, void *content, void *r) {
    field_object *field = (field_object *) r;
    if (field->fix) idtable_insertwithid(field->fix, id, NULL);
}

void field_fixwithselection(field_object *field, selection_object *sel) {
    if (field && sel && field->entries) {
        if (!field->fix) field->fix = idtable_create(field->entries->nentries);
        if (field->fix && sel) selection_map(sel, (manifold_object *) field->manifold->obj, field->grade, field_fixwithselectionmapfunction, field);
    }
}

void field_unfixwithselectionmapfunction (uid id, void *content, void *r) {
    field_object *field = (field_object *) r;
    if (idtable_ispresent(field->fix, id)) idtable_remove(field->fix, id);
}

void field_unfixwithselection(field_object *field, selection_object *sel) {
    if (field && sel && field->entries) {
        if (field->fix && sel) selection_map(sel, (manifold_object *) field->manifold->obj, field->grade, field_unfixwithselectionmapfunction, field);
    }
}

/* Rebuild after the manifold is refined */

typedef struct {
    field_object *map;
    idtable *target;
 } field_refinerenameelementsmapfunctionref;

typedef struct {
    interpretcontext *context;
    field_object *field;
    manifold_object *manifold;
} field_refineaddnewelementsmapfunctionref;

void field_refinerenameelementsmapfunction (uid id, void *content, void *r) {
    field_refinerenameelementsmapfunctionref *ref = (field_refinerenameelementsmapfunctionref *) r;
    expression_list *list = (expression_list *) field_get(ref->map, id);
    
    if (eval_islist(list) && list->list) for (linkedlistentry *e = list->list->first; e!=NULL; e=e->next) {
        expression *entry = e->data;
        
        if (eval_isinteger(entry)) {
            /* Insert the old data into the new table, or clone it if we're past the first entry */
            idtable_insertwithid(ref->target, (uid) eval_integervalue(entry), (e == list->list->first ? content : cloneexpression(content)));
        }
    }
}

void field_refineaddnewelementsmapfunction (uid id, void *content, void *r) {
    field_refineaddnewelementsmapfunctionref *ref = r;
    expression_list *list = content;
    unsigned int i=0;
    uid vid[4], rpt; unsigned int nel;
    
    if (eval_islist(list)) {
        for (linkedlistentry *e = list->list->first; e!=NULL; e=e->next) {
            expression *exp = e->data;
            if (eval_isinteger(exp)) {
                uid lid = eval_integervalue(exp);
                manifold_gradelower(ref->manifold, MANIFOLD_GRADE_LINE, lid, MANIFOLD_GRADE_POINT, vid+2*i, &nel);
                i++;
            }
            /* Was the element refined? */
            if (i==2) {
                /* Is the first vertex of the second element the repeated entry? */
                if (manifold_idlistposition(vid, 2, vid[2], NULL)) {
                    rpt=vid[2]; vid[2]=vid[3];
                } else rpt=vid[3];
                // vid[0..2] now contains three distinct elements, the last of which is definitely one of the unique ones
                // But are the first two correctly ordered?
                if (vid[0]==rpt) { vid[0]=vid[1]; vid[1]=rpt; }
                /* vid[0..2] contains the three vertices, with the new one in the middle */
                
                expression *u1=field_get(ref->field, vid[0]);
                expression *u2=field_get(ref->field, vid[2]);
                
                /* Find the average and insert */
                if (u1 && u2) {
                    expression *temp[3];
                    temp[0]=opadd(ref->context, u1, u2);
                    temp[1]=newexpressionfloat(0.5);
                    temp[2]=opmul(ref->context, temp[0], temp[1]);
                    field_insertwithid(ref->field, vid[1], temp[2]);
                    freeexpression(temp[0]); freeexpression(temp[1]);
                }
            }
        }
    }
}

void field_refine(field_object *field, interpretcontext *context, manifold_object *mobj, expression_list *refine) {
    field_refinerenameelementsmapfunctionref ref;
    field_refineaddnewelementsmapfunctionref nref;
    
    /* First renumber items from the same grade as the field */
    linkedlistentry *entry=linkedlist_element(refine->list, field->grade);
    
    expression_objectreference *fobj=(expression_objectreference *) entry->data;
    if (eval_isobjectref(fobj)) ref.map = (field_object *) eval_objectfromref(fobj);
    
    /* Create a new idtable to hold the corrected entries */
    ref.target=idtable_create((field->entries==NULL ? 1 : field->entries->nentries));
    
    if (ref.target && ref.map) {
        /* Call the refine map function */
        idtable_map(field->entries, field_refinerenameelementsmapfunction, &ref);
        
        idtable_free(field->entries); // Get rid of the old idtable
        field->entries=ref.target;
    }
    
    /* If this is a field defined on vertices, identify additional vertices by looking at line elements.
     TODO: Generalize this to other grades. */
    if (field->grade==MANIFOLD_GRADE_POINT) {
        linkedlistentry *linemapentry=linkedlist_element(refine->list, MANIFOLD_GRADE_LINE);
        if (linemapentry) {
            expression_objectreference *linemapref=(expression_objectreference *) linemapentry->data;
            field_object *linemap = (field_object *) eval_objectfromref(linemapref);
            
            nref.field=field; nref.manifold=mobj;
            nref.context=context;
            
            if (linemap) {
                field_map(linemap, field_refineaddnewelementsmapfunction, &nref);
            }
        }
    }
}

/* Map */

void field_map(field_object *field, idtablemapfunction *func, void *ref) {
    idtable_map(field->entries, func, ref);
}

typedef struct {
    interpretcontext *context;
    expression *total;
    field_object *rhs;
    field_object *target;
    executeoptype *op;
} fieldopmapref ;

void fieldoplmapfunction (uid id, void *content, void *ref) {
    fieldopmapref *fref = (fieldopmapref *) ref;
    expression *left = (expression *) content;
    expression *right = field_get(fref->rhs, id);
    expression *out=NULL;
    int shouldfree=FALSE;
    
    if (!right) { right=newexpressionfloat(0.0); shouldfree=TRUE; };
    
    if ((fref)) {
        out = (*(fref->op)) (fref->context, left, right);
        
        if (out) field_insertwithid(fref->target, id, out);
    }
    
    if (shouldfree) freeexpression(right);
}

void fieldoprmapfunction (uid id, void *content, void *ref) {
    fieldopmapref *fref = (fieldopmapref *) ref;
    expression *left = field_get(fref->rhs, id);
    expression *right = (expression *) content;
    expression *out=NULL;
    
    if (!left) {
        /* Only apply this function to any terms NOT in the left hand field */
        left=newexpressionfloat(0.0);
        out = (*(fref->op)) (fref->context, left, right);
        
        if (out) field_insertwithid(fref->target, id, out);
        
        freeexpression(left);
    };
}

expression *field_op(interpretcontext *context, field_object *lhs, field_object *rhs, executeoptype *op) {
    fieldopmapref ref;
    expression_objectreference *out=NULL;
    manifold_object *manifold=NULL;
    
    if ((lhs)&&(lhs->manifold)) manifold=(manifold_object *) lhs->manifold->obj;
    
    if ((lhs)&&(rhs)&&(lhs->manifold)&&(rhs->manifold)&&(lhs->manifold->obj != rhs->manifold->obj)) {
        error_raise(context, ERROR_FIELD_MANIFOLD, ERROR_FIELD_MANIFOLD_MSG, ERROR_FATAL);
    }
    
    if ((lhs)&&(rhs)&&(lhs->grade != rhs->grade)) {
        error_raise(context, ERROR_FIELD_GRADE, ERROR_FIELD_GRADE_MSG, ERROR_FATAL);
    }
    
    /* Create a new field*/
    out=(expression_objectreference *) field_new(manifold, lhs->grade);
    
    if (out) {
        /* Fill in the fieldopmapref */
        ref.context=context;
        ref.rhs=rhs;
        ref.target=(field_object *) out->obj;
        ref.op=op;
        
        /* And map the function */
        idtable_map(lhs->entries, fieldoplmapfunction, &ref);
        ref.rhs=lhs;
        idtable_map(rhs->entries, fieldoprmapfunction, &ref);
    }
    
    return (expression *) out;
}

typedef struct {
    interpretcontext *context;
    expression *total;
    expression *exp;
    int right;
    field_object *target;
    executeoptype *op;
} fieldopconstmapref;

void fieldopconstantmapfunction (uid id, void *content, void *ref) {
    fieldopconstmapref *fref = (fieldopconstmapref *) ref;
    expression *this = (expression *) content;
    expression *out=NULL;
    
    if ((fref)&&(this)) {
        if (fref->right) {
            out = (*(fref->op)) (fref->context, this, fref->exp);
        } else {
            out = (*(fref->op)) (fref->context, fref->exp, this);
        }
        
        if (out) field_insertwithid(fref->target, id, out);
    }
}

expression *field_opwithconstant(interpretcontext *context, field_object *fld, expression *exp, int right, executeoptype *op) {
    fieldopconstmapref ref;
    expression_objectreference *out=NULL;
    manifold_object *manifold=NULL;
    
    if ((fld)&&(fld->manifold)) manifold=(manifold_object *) fld->manifold->obj;
    
    /* Create a new field*/
    out=(expression_objectreference *) field_new(manifold, fld->grade);
    
    if (out) {
        /* Fill in the fieldopmapref */
        ref.context=context;
        ref.exp=exp;
        ref.right=right;
        ref.target=(field_object *) out->obj;
        ref.op=op;
        
        /* And map the function */
        idtable_map(fld->entries, fieldopconstantmapfunction, &ref);
    }
    
    return (expression *) out;
}

double field_dot(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2) {
    expression_objectreference *fg=NULL;
    expression_objectreference *fgtotal=NULL;
    expression *fgnorm=NULL;
    double fgn=0.0;
    
    /* TODO Use dot to speed this up */
    /* <f . g>_0 */
    fg = (expression_objectreference *) opmul(context, (expression *) f1, (expression *) f2);
    if (eval_isobjectref(fg)) {
        /* Sum over the field elements */
        if (class_objectrespondstoselector(fg->obj, GEOMETRY_FIELDTOTAL)) {
            fgtotal=(expression_objectreference *) class_callselector(context, fg->obj, GEOMETRY_FIELDTOTAL, 0);
        }
        
        /* Extract the zero grade element */
        if (eval_isobjectref(fgtotal)) {
            /* TODO should call getgrade on fgtotal */
            fgnorm=multivector_getgradeaslist((multivectorobject *) fgtotal->obj, 0);
            if (eval_isnumerical(fgnorm)) fgn=eval_floatvalue(fgnorm);
        }
    }
    
    if (fg) freeexpression((expression *) fg);
    if (fgtotal) freeexpression((expression *) fgtotal);
    if (fgnorm) freeexpression(fgnorm);
    
    return fgn;
}


/* Subtracts the component of the field f1 that lies in the direction of f2 */
expression *field_project(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2) {
    expression *f3=NULL;
    expression *out=NULL;
    expression *scale=NULL;
    double fg, gg;
    
    fg = field_dot(context, f1, f2);
    gg = field_dot(context, f2, f2);
    
    if (gg>MACHINE_EPSILON) {
        scale = newexpressionfloat(fg/gg);
        
        f3 = opmul(context, (expression *) f2, scale);
        out = opsub(context, (expression *) f1, f3);
    } else {
        /* The constraint vector was zero */
    }
    
    if (scale) freeexpression(scale);
    if (f3) freeexpression(f3);
    
    return out;
}

typedef struct {
    interpretcontext *context;
    field_object *f2;
    field_object *target;
    int os;
} field_localprojectmapfunctionref;

void field_localprojectmapfunction(uid id, void *content, void *r) {
    field_localprojectmapfunctionref *ref = (field_localprojectmapfunctionref *) r;
    expression *e1 = (expression *) content;
    expression *e2 = NULL;
    expression *out = NULL;
    int success=TRUE;
    
    e2 = field_get(ref->f2, id);
    if (e2) {
        /* If the local projection is onesided, need to check sign of e1 . e2 */
        if (ref->os) {
            expression_objectreference *sgn = (expression_objectreference *) opmul(ref->context, e1, e2);
            MFLD_DBL dot=0.0;
            
            if (eval_isobjectref(sgn)) multivector_gradetodoublelist((multivectorobject *) sgn->obj, MANIFOLD_GRADE_POINT, &dot);
            freeexpression((expression *) sgn);
            
            if (dot<0) success=FALSE;
        }
        
        if (success) {
            out =  multivector_subtractcomponent(ref->context, e1, e2);
            field_insertwithid(ref->target, id, out);
        }
    } else {
        success = FALSE;
    }
    
    if (!success) {
        if (eval_isobjectref(e1)) {
            out = multivector_clone((multivectorobject *) eval_objectfromref(e1));
            field_insertwithid(ref->target, id, out);
        } else if (eval_isreal(e1)) {
            out = cloneexpression(e1);
            field_insertwithid(ref->target, id, out);
        } else {
            printf("Unrecognized quantity in field.\n");
        }
    }
}

/* Subtracts the component of every component of field f1 that lies in the direction of the corresponding element in f2 */
expression *field_localproject(interpretcontext *context, expression_objectreference *f1, expression_objectreference *f2, int onesided) {
    field_localprojectmapfunctionref ref;
    field_object *ff1 = (field_object *) eval_objectfromref(f1);
    field_object *ff2 = (field_object *) eval_objectfromref(f2);
    expression *out = NULL;
    ref.os=onesided;
    
    if (ff1 && ff2 && (eval_objectfromref(ff1->manifold) == eval_objectfromref(ff2->manifold)) && (ff1->grade==ff2->grade)) {
        out = field_new((manifold_object *) eval_objectfromref(ff1->manifold), ff1->grade);
        
        ref.target=(field_object *) eval_objectfromref(out);
        ref.f2=ff2;
        ref.context=context;
        
        field_map(ff1, field_localprojectmapfunction, &ref);
    }
    
    return out;
}

typedef struct {
    field_object *target;
    interpretcontext *context;
    expression *scale;
} field_accumulatemapfunctionref;

void field_accumulatemapfunction(uid id, void *content, void *ref) {
    field_accumulatemapfunctionref *eref = (field_accumulatemapfunctionref *) ref;
    expression *value=(expression *) content;
    expression *new=NULL;
    
    if (eref->scale) new=opmul(eref->context, eref->scale, value);
    
    field_accumulatewithid(eref->target, eref->context, id, (new==NULL ? value : new) );
    
    if (new) freeexpression(new); 
}

void field_accumulate(field_object *f, interpretcontext *context, field_object *df, MFLD_DBL scale) {
    field_accumulatemapfunctionref ref;
    
    ref.target=f; ref.context=context; ref.scale=NULL;
    if (!(fabs(scale-1.0)<MACHINE_EPSILON)) ref.scale=newexpressionfloat(scale);
    field_map(df, field_accumulatemapfunction, &ref);
    
    if (ref.scale) freeexpression(ref.scale);
}

/* 
 * Constructors
 */

typedef struct {
    unsigned int dim;
    interpretcontext *context;
    field_object *field;
    expression *exp;
    expression_list *coords;
} field_vertexmapfunctionref;

/* Map function over vertex entires; evaluates an expression substituting in coordinate positions into the expression. */
void field_vertexmapfunction(uid id, void *e, void *r ) {
    field_vertexmapfunctionref *ref = (field_vertexmapfunctionref *) r;
    manifold_vertex *vertex = (manifold_vertex *) e;
    expression *exp=NULL;
    unsigned int i=0;
    
    for (linkedlistentry *vx = ref->coords->list->first; (vx!=NULL)&&(i<ref->dim); vx=vx->next) {
        expression_name *name = (expression_name *) vx->data;
        expression_float *val = (expression_float *) newexpressionfloat(vertex->x[i]);
        
        /* substitute the ith coordinate into the appropriate name */
        // FIND THIS
        freeexpression(opassign(ref->context, (expression *) name, (expression *) val));
        
        if (val) freeexpression((expression *) val);
        
        i++;
    }
    
    exp=interpretexpression(ref->context, ref->exp);
    
    if (exp) field_insertwithid(ref->field, id, exp);
}

/* Checks whether a list of coordinates is acceptable, i.e. it is a list of names consistent with the dimension of the manifold */
int field_checkcoordinatelist(manifold_object *obj, expression *exp) {
    expression_list *elist = (expression_list *) exp;
    if (!eval_islist(exp)) return FALSE;
    
    if (eval_listlength(elist)<obj->dimension) return FALSE;
    
    for (linkedlistentry *vx=elist->list->first; vx!=NULL; vx=vx->next) {
        if (!eval_isname(((expression *) vx->data))) return FALSE;
    }
    
    return TRUE;
}

/* Create a new field by applying expression exp to each vertex. */
expression *field_newwithexpression(manifold_object *manifold, interpretcontext *context, expression *exp, expression *coords) {
    expression *fref=NULL;
    field_vertexmapfunctionref mapref;
    
    if (field_checkcoordinatelist(manifold, coords)) {
        fref=field_new(manifold, MANIFOLD_GRADE_POINT);
        
        if (fref) {
            mapref.context=newinterpretcontext(context, manifold->dimension+1);
            if (mapref.context) {
                mapref.dim=manifold->dimension;
                mapref.exp=exp;
                mapref.field=(field_object *) ((expression_objectreference *)fref)->obj;
                mapref.coords=(expression_list *) coords;
                
                idtable_map(manifold->vertices, field_vertexmapfunction, &mapref);
                
                freeinterpretcontext(mapref.context);
            }
        }
    }
        
    return fref;
}

/* --- field_bounds ---
   Finds the minimum and maximum value of a field */

typedef struct {
    interpretcontext *context;
    expression **min;
    expression **max;
} fieldboundmapfunctionref;

void fieldboundmapfunction (uid id, void *content, void *ref) {
    expression *exp=(expression *) content;
    fieldboundmapfunctionref *eref = (fieldboundmapfunctionref *) ref;
    expression_bool *cmp=NULL;
    
    /* Handle the min */
    if (eref->min) {
        if (*eref->min) {
            cmp = (expression_bool *) oplessthan(eref->context, exp, *eref->min);
            if (eval_isbool(cmp) && cmp->value) {
                freeexpression(*eref->min);
                *eref->min = cloneexpression(exp);
            }
        } else if (exp) {
            /* If no min already, clone this one */
            *eref->min = cloneexpression(exp);
        }
    }

    if (cmp) { freeexpression((expression *) cmp); cmp=NULL; }
    
    /* Handle the max */
    if (eref->max) {
        if (*eref->max) {
            cmp = (expression_bool *) opgreaterthan(eref->context, exp, *eref->max);
            if (eval_isbool(cmp) && cmp->value) {
                freeexpression(*eref->max);
                *eref->max = cloneexpression(exp);
            }
        } else if (exp) {
            /* If no max already, clone this one */
            *eref->max = cloneexpression(exp);
        }
    }
    
    if (cmp) { freeexpression((expression *) cmp); cmp=NULL; }
    
}

void field_bounds(field_object *fobj, interpretcontext *context, expression **min, expression **max) {
    fieldboundmapfunctionref ref;
    
    if (min) *min=NULL;
    if (max) *max=NULL;
    
    ref.min=min; ref.max=max; ref.context=context;
    field_map(fobj, fieldboundmapfunction, &ref);
}

/* --- tolist --- */

typedef struct {
    field_object *field;
    expression_list *new;
} field_tolistmapfunctionref;

void field_tolistmapfunction (uid id, void *content, void *ref) {
    field_tolistmapfunctionref *eref = (field_tolistmapfunctionref *) ref;
    expression *exp = NULL;
    
    if (eref) exp = field_get(eref->field, id);
    
    if (exp && eref->new) linkedlist_addentry(eref->new->list, cloneexpression(exp));
}

expression *field_tolist(field_object *field) {
    field_tolistmapfunctionref ref;
    expression_list *new=newexpressionlist(NULL);
    
    if (new) {
        ref.new=new;
        ref.field=field;
        field_map(field, field_tolistmapfunction, &ref);
    }
    
    return (expression *) new;
}


/* --- field_computecolormap ---
   Computes a color map  */

typedef struct {
    expression *min;
    expression *max;
    idtable *target;
    int grade;
    expression_objectreference *colormap;
    interpretcontext *context;
} field_computecolormapref;

void field_computecolormapmapfunction (uid id, void *content, void *ref) {
    field_computecolormapref *eref = (field_computecolormapref *) ref;
    expression *entry = (expression *) content;
    expression *sv;
    double *col=EVAL_MALLOC(sizeof(double)*3);
    
    if (!col) return;
    if (!eval_isnumerical(entry)) return;
    if (!eref || !eref->target) return;

    /* If requested, scale the entry */
    if (eref->min && eref->max) {
        /* Scaled value = (val-min)/(max-min) */
        expression *e[3] = {NULL, NULL};
        e[0]=opsub(eref->context, entry, eref->min);
        e[1]=opsub(eref->context, eref->max, eref->min);
        if (e[0] && e[1]) sv=opdiv(eref->context, e[0], e[1]);
        for (unsigned int i=0; i<2; i++) if (e[i]) freeexpression(e[i]);
    } else {
        sv=cloneexpression(entry);
    }
    
    /* Now apply the color map if available */
    if (eref->colormap) {
        if (sv) color_rgbfromobject(eref->context, eref->colormap->obj, 1, &sv, col);
    } else {
        /* Just copy the value to all components */
        for (unsigned int i=0; i<3; i++) col[i]=eval_floatvalue(sv);
    }
    
    idtable_insertwithid(eref->target, id, col);

    if (sv) freeexpression(sv);
    
}

idtable *field_computecolormap(field_object *fobj, interpretcontext *context, expression_objectreference *colormap, int scale, unsigned int grade) {
    field_computecolormapref ref;
    expression *min=NULL, *max=NULL;
    idtable *ret=NULL;
    unsigned int size=1;
    
    ref.colormap=colormap;
    ref.context=context;
    ref.grade=grade;
    
    /* Find the minimum and maximum to scale the results */
    if (scale) field_bounds(fobj, context, &min, &max);
    ref.min=min; ref.max=max;
    
    /* Create the idtable */
    if (fobj && fobj->entries) size=fobj->entries->nentries;
    ref.target = idtable_create(size);
    
    if (ref.target) {
        field_map(fobj, field_computecolormapmapfunction, &ref);
        ret = ref.target;
    }
    
    if (min) freeexpression(min);
    if (max) freeexpression(max);
    
    return ret;
}

/*
 * Selectors
 */

/* Initializes the field */
expression *field_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    
    fobj->manifold=NULL;
    fobj->entries=NULL;
    fobj->fix=NULL;
    fobj->grade=0;
    
    return NULL;
}

/* Frees the field and attached expressions */
expression *field_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    
    if (fobj) {
        if (fobj->manifold) freeexpression((expression *) fobj->manifold);
        if (fobj->entries) {
            idtable_map(fobj->entries, idtable_genericfreeexpressionmapfunction, NULL);
            idtable_free(fobj->entries);
        }
        if (fobj->fix) {
            idtable_free(fobj->fix);
        }
    }
    
    return NULL;
}

void field_clonemapfunction(uid id, void *e, void *n) {
    expression *exp = (expression *) e;
    field_object *new = (field_object *) n;
    
    field_insertwithid(new, id, cloneexpression(exp)); // TODO Should be deep clone.
}

/* Clones a field */
expression *field_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression_objectreference *fnew=NULL;
    
    if (fobj) {
        if (fobj->manifold) {
            fnew=(expression_objectreference *) field_new((manifold_object *) fobj->manifold->obj, fobj->grade);
        } else {
            fnew=(expression_objectreference *) field_new(NULL, fobj->grade);
        }
        
        if (fnew) idtable_map(fobj->entries, field_clonemapfunction, fnew->obj);
    }
    
    return (expression *) class_newobjectreference((object *) fnew);
}

/* Looks up an expression given an index */
expression *field_indexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (!obj) return NULL;
    field_object *fobj = (field_object *) obj;
    uid i;
    expression *ret = NULL;
    
    if (nargs!=1) {
        error_raise(context, ERROR_FIELD_INDEX, ERROR_FIELD_INDEX_MSG, ERROR_FATAL);
    } else if (nargs==1) {
        if (eval_isinteger(args[0])) {
            i=(uid) eval_integervalue(args[0]);
            ret=field_get(fobj, i);
            if (ret) ret = cloneexpression(ret);
        } else {
            error_raise(context, ERROR_FIELD_INDEX_NUM, ERROR_FIELD_INDEX_NUM_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

/* Sets an expression given an index */
expression *field_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (!obj) return NULL;
    field_object *fobj = (field_object *) obj;
    uid i;
    
    if (nargs==2) {
        if (eval_isinteger(args[1])) {
            i=(uid) eval_integervalue(args[1]);
            
            field_insertwithid(fobj, i, cloneexpression(args[0]));
        } else if (eval_isobjectref(args[1])) {
            expression_objectreference *sref=(expression_objectreference *) args[1];
            if (class_objectrespondstoselector(eval_objectfromref(sref), SELECTION_IDLISTFORGRADE_SELECTOR)) {
                expression *ret=class_callselector(context, eval_objectfromref(sref), SELECTION_IDLISTFORGRADE_SELECTOR, 2, EXPRESSION_OBJECT_REFERENCE, fobj->manifold, EXPRESSION_INTEGER, fobj->grade);
                
                if (eval_islist(ret)) {
                    linkedlist *list = ((expression_list *) ret)->list;
                    
                    if (list) for (linkedlistentry *e = list->first; e!=NULL; e=e->next) {
                        expression_integer *idx = (expression_integer *) e->data;
                        if (eval_isinteger(idx)) {
                            field_insertwithid(fobj, (uid) idx->value,cloneexpression(args[0]));
                        }
                    }
                }
            }
            
        } else {
            error_raise(context, ERROR_FIELD_INDEX_NUM, ERROR_FIELD_INDEX_NUM_MSG, ERROR_FATAL);
        }
    } else {
        error_raise(context, ERROR_FIELD_INDEX, ERROR_FIELD_INDEX_MSG, ERROR_FATAL);
    }
    
    return (expression *) class_newobjectreference(obj);
}


void fieldindicesmapfunction (uid id, void *content, void *ref) {
    //expression *e = (expression *) content;
    linkedlist *lst = (linkedlist *) ref;
    
    if (lst) {
        linkedlist_addentry(lst, newexpressioninteger(id));
    }
}

/* Returns all available indices in a field */
expression *field_indicesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *field = (field_object *) obj;
    expression *ret = NULL, *retsrt = NULL;
    
    if (field) {
        ret=(expression *) newexpressionlist(NULL);
        idtable_map(field->entries, fieldindicesmapfunction, ((expression_list *) ret)->list);
        
        if (ret) {
            retsrt=eval_sort(context, 1, &ret);
            freeexpression((expression *) ret);
        }
    }
    
    return retsrt;
}

/* Fixes elements */
expression *field_fixi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    selection_object *sel=NULL;
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            sel=(selection_object *) ((expression_objectreference *) args[0])->obj;
            if ((sel) && (sel->clss=class_lookup(GEOMETRY_SELECTIONCLASS))) {
                field_fixwithselection(fobj, sel);
            }
        } else if (eval_isinteger(args[0])) {
            field_fix(fobj, eval_integervalue(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Unfixes elements */
expression *field_unfixi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    selection_object *sel=NULL;
    if (nargs==1) {
        if (eval_isobjectref(args[0])) {
            sel=(selection_object *) ((expression_objectreference *) args[0])->obj;
            if ((sel) && (sel->clss=class_lookup(GEOMETRY_SELECTIONCLASS))) {
                field_unfixwithselection(fobj, sel);
            }
        } else if (eval_isinteger(args[0])) {
            field_unfix(fobj, eval_integervalue(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Gets the grade of a field */
expression *field_gradei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    
    return newexpressioninteger(fobj->grade);
}

/* Sets the grade of a field */
expression *field_setgradei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;

    if ((nargs=1)&&(args[0])&&eval_isinteger(args[0])) {
        fobj->grade=((expression_integer *) args[0])->value;
    }
    
    return (expression *) class_newobjectreference(obj);
}

typedef struct {
    interpretcontext *context;
    expression *total;
} fieldtotalmapref ;

void fieldtotalmapfunction (uid id, void *content, void *ref) {
    expression *e = (expression *) content;
    fieldtotalmapref *f = (fieldtotalmapref *) ref;
    
    if (f) {
        if (f->total) {
            expression *new=opadd(f->context, f->total, e);
            freeexpression(f->total);
            f->total=new;
        } else {
            f->total=cloneexpression(e); /* TODO Should be a deep clone */
        }
    }
}

/* Finds the total of a field, with an optional weighting field */
expression *field_totali(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    fieldtotalmapref total;
    expression *res=NULL;
    
    total.context=context;
    total.total=NULL;
    
    if (fobj->entries) {
        idtable_map(fobj->entries, fieldtotalmapfunction, &total);
        res=total.total;
    } else {
        error_raise(context, ERROR_FIELD_EMPTY, ERROR_FIELD_EMPTY_MSG, ERROR_WARNING);
        res=newexpressionfloat(0.0);
    }
    
    return res;
}

/*
 * Map a function over a field
 */

typedef struct {
    expression *name;
    field_object *src;
    field_object *target;
    interpretcontext *context;
} fieldmapref;

void field_mapmapfunction (uid id, void *content, void *ref) {
    fieldmapref *eref = (fieldmapref *) ref;
    expression_function func;
    
    func.type=EXPRESSION_FUNCTION;
    func.name=eref->name;
    func.nargs=1;
    func.argslist=(expression **) &content;
    
    field_insertwithid(eref->target, id, callfunction(eref->context, &func));
}

expression *field_mapi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression *ret=NULL;
    fieldmapref ref;
    manifold_object *manifold=NULL;
    
    if ((nargs!=1)||(!(eval_isname(args[0]) || eval_isfunctiondefinition(args[0])))) return NULL;
    
    if (eval_isobjectref(fobj->manifold)) manifold=(manifold_object *) eval_objectfromref(fobj->manifold);
    
    ret=field_new(manifold, fobj->grade);
    
    if (eval_isobjectref(ret)) {
        ref.src=fobj;
        ref.target=(field_object *) eval_objectfromref(ret);
        ref.name=args[0];
        ref.context=context;
        
        field_map(fobj, field_mapmapfunction, &ref);
    }
    
    return ret;
}

/*
 * Listeners
 */

/* Listener called when a manifold is refined */
expression *field_refinei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    
    if (nargs==2 && eval_isobjectref(args[0]) && eval_islist(args[1])) {
        field_refine(fobj, context, (manifold_object *) eval_objectfromref(args[0]), (expression_list *) args[1]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* 
 * Field arithmatic
 */

expression *field_addi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression_objectreference *rhs=NULL;
    expression *out=NULL;
    
    if (nargs==1) {
        rhs=(expression_objectreference *) args[0];
        
        if ((rhs)&&(eval_isobjectref(rhs))) out=field_op(context, fobj, (field_object *) rhs->obj, &opadd);
    }
    
    return out;
}

expression *field_subtracti(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression_objectreference *rhs=NULL;
    expression *out=NULL;
    
    if (nargs==1) {
        rhs=(expression_objectreference *) args[0];
        
        if ((rhs)&&(eval_isobjectref(rhs))) out=field_op(context, fobj, (field_object *) rhs->obj, &opsub);
    }

    return out;
}

expression *field_multiplyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression_objectreference *rhs=NULL;
    expression *out=NULL;
    
    if (nargs==1) {
        rhs=(expression_objectreference *) args[0];
        
        if (rhs) {
            if (eval_isobjectref(rhs)) {
                out=field_op(context, fobj, (field_object *) rhs->obj, &opmul);
            } else {
                out=field_opwithconstant(context, fobj, (expression *) rhs, TRUE, &opmul);
            }
        }
    }
    
    return out;
}

expression *field_dividei(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    expression_objectreference *rhs=NULL;
    expression *out=NULL;
    
    if (nargs==1) {
        rhs=(expression_objectreference *) args[0];
        
        if ((rhs)&&(eval_isobjectref(rhs))) out=field_op(context, fobj, (field_object *) rhs->obj, &opdiv);
    }
    
    return out;
}

expression *field_boundsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *min=NULL, *max=NULL, *ret=NULL;
    
    field_bounds((field_object *) obj, context, &min, &max);
    
    if (min && max) {
        ret = (expression *) newexpressionlistfromargs(2, min, max);
    }
    
    return ret;
}

expression *field_mini(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *min=NULL;
    
    field_bounds((field_object *) obj, context, &min, NULL);
    return min;
}

expression *field_maxi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *max=NULL;

    field_bounds((field_object *) obj, context, NULL, &max);
    return max;
}

expression *field_tolisti(object *obj, interpretcontext *context, int nargs, expression **args) {
    
    return  field_tolist((field_object *) obj);
}

expression *field_manifoldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;

    return (expression *) cloneexpression((expression *) fobj->manifold);
}

expression *field_setmanifoldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    field_object *fobj = (field_object *) obj;
    
    if (eval_isobjectref(args[0])) {
        /* TODO Check it's a manifold! */
        fobj->manifold = (expression_objectreference *) cloneexpression(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}


/* 
 *
 */

void fieldinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(GEOMETRY_FIELDCLASS, class_lookup(EVAL_OBJECT), sizeof(field_object), 18);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, field_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, field_freei);
    class_registerselector(cls, EVAL_CLONE, FALSE, field_clonei);
    
    class_registerselector(cls, GEOMETRY_FIELDTOTAL, FALSE, field_totali);
    class_registerselector(cls, GEOMETRY_FIELDSETGRADE, FALSE, field_setgradei);
    class_registerselector(cls, GEOMETRY_FIELDGRADE, FALSE, field_gradei);
    
    class_registerselector(cls, GEOMETRY_FIELDMANIFOLD, FALSE, field_manifoldi);
    class_registerselector(cls, GEOMETRY_FIELDSETMANIFOLD, FALSE, field_setmanifoldi);
    
    class_registerselector(cls, MANIFOLD_REFINE, FALSE, field_refinei);
    
    class_registerselector(cls, FIELD_FIX_SELECTOR, FALSE, field_fixi);
    class_registerselector(cls, FIELD_UNFIX_SELECTOR, FALSE, field_unfixi);
    
    class_registerselector(cls, FIELD_MAP_SELECTOR, FALSE, field_mapi);
    class_registerselector(cls, FIELD_MAX_SELECTOR, FALSE, field_maxi);
    class_registerselector(cls, FIELD_MIN_SELECTOR, FALSE, field_mini);
    class_registerselector(cls, FIELD_BOUNDS_SELECTOR, FALSE, field_boundsi);
    class_registerselector(cls, EVAL_TOLIST_SELECTOR, FALSE, field_tolisti);
    
    class_registerselector(cls, OPERATOR_ADD_SELECTOR, FALSE, field_addi);
    class_registerselector(cls, OPERATOR_SUBTRACT_SELECTOR, FALSE, field_subtracti);
    class_registerselector(cls, OPERATOR_MULTIPLY_SELECTOR, FALSE, field_multiplyi);
    class_registerselector(cls, OPERATOR_DIVIDE_SELECTOR, FALSE, field_dividei);
    
    class_registerselector(cls, EVAL_INDEX, FALSE, field_indexi);
    class_registerselector(cls, EVAL_SETINDEX, FALSE, field_setindexi);
    class_registerselector(cls, EVAL_INDICES, FALSE, field_indicesi);
}
