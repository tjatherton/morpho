/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "io.h"

/* 
 * Internally facing IO functions - use these to access IO facilities in a class independent manner
 */

expression_objectreference *io_open(interpretcontext *context, expression *exp) {
    expression *fileref=NULL;
    
    if (eval_isstring(exp)) {
        fileref=io_file_openi(context, 1, &exp);
    }
    
    return (expression_objectreference *) fileref;
}

expression *io_read(interpretcontext *context, expression_objectreference *objref) {
    expression *ref=NULL;
    if (objref) {
        if (objref->obj) {
            if (class_objectrespondstoselector(objref->obj, IO_FILE_READ)) {
                ref=class_callselector(context, objref->obj, IO_FILE_READ, 0);
            }
        }
    }
    return ref;
}

int io_eof(interpretcontext *context, expression_objectreference *objref) {
    expression_bool *ref=NULL;
    int ret=FALSE;
    
    if (objref) {
        if (objref->obj) {
            if (class_objectrespondstoselector(objref->obj, IO_FILE_EOF)) {
                ref=(expression_bool *) class_callselector(context, objref->obj, IO_FILE_EOF, 0);
                
                if (eval_isbool(ref)) ret=ref->value;
                
                if (ref) freeexpression((expression *) ref);
            }
        }
    }
    return ret;
}

void io_close(interpretcontext *context, expression_objectreference *objref) {
    if (objref) {
        if (objref->obj) {
            if (class_objectrespondstoselector(objref->obj, IO_FILE_CLOSE)) {
                freeexpression(class_callselector(context, objref->obj, IO_FILE_CLOSE, 0));
            }
        }
    }
}


/* 
 * file class
 */

filemode io_fileinterpretmode(expression *exp) {
    filemode mode=READ;
    expression_name *name=(expression_name *) exp;
    
    if (exp) {
        if (exp->type==EXPRESSION_NAME) {
            if (strcmp(name->name,"read")==0) {
                mode=READ;
            } else if (strcmp(name->name,"write")==0) {
                mode=WRITE;
            } else if (strcmp(name->name,"append")==0) {
                mode=APPEND;
            }
        }
    }
    
    return mode;
}

char *io_filemodecform(filemode mode) {
    switch (mode) {
        case READ:
            return "r"; break;
        case WRITE:
            return "w"; break;
        case APPEND:
            return "w+"; break;
        default:
            return ""; break;
    }
}

/* io_file_newi - Intrinsic function to open a file
 * Input:   (interpretcontext *context) - current eval context
 *          (int) nargs                 - number of arguments passed
 *          (expression **) args        - arguments
 * Output:
 * Returns: (object *) - a pointer to a newly created object.
 */
expression *io_file_openi(interpretcontext *context, int nargs, expression **args) {
    expression_objectreference *ref=NULL;
    interpretcontext *options=NULL;
    file_object *obj=NULL;
    filemode mode = READ;
    char *fname=NULL;
    expression *exp=NULL;
    
    FILE *f=NULL;
    
    /* Validate arguments */
    if ((nargs<1)||(!args[0])) return NULL;
    
    if (eval_isstring(args[0])) {
        fname = ((expression_string *) args[0])->value;
    } else {
        exp=interpretexpression(context, args[0]);
        if (eval_isstring(exp)) {
            fname=((expression_string *) exp)->value;
        }
    }
    
    if (fname) {
        options=eval_options(context,2,nargs,args);
        expression *modeexp = lookupsymbol(options, "mode");
        
        mode = io_fileinterpretmode(modeexp);
        
        if (modeexp) freeexpression(modeexp);
        
        f=fopen(fname, io_filemodecform(mode));
        
        if (f) {
            obj=(file_object *) class_instantiate(class_lookup(IO_FILE_CLASS));
            
            if (obj) {
                obj->mode=mode;
                obj->file=f;
                
                ref=class_newobjectreference((object *) obj);
            }
        }
    }
        
    if (options) freeinterpretcontext(options);
    if (exp) freeexpression(exp);
    
    return (expression *) ref;
}

expression *io_file_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    file_object *fobj=(file_object *) obj;
    
    if (fobj) {
        fobj->file=NULL;
    }
    
    return NULL;
}

expression *io_file_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    file_object *fobj=(file_object *) obj;
    
    if (fobj->file) {
        fclose(fobj->file);
    }
    
    return NULL; 
}

expression *io_file_closei(object *obj, interpretcontext *context, int nargs, expression **args) {
    file_object *fobj=(file_object *) obj;
    
    if (fobj->file) {
        if (fobj->file) fclose(fobj->file);
        
        fobj->file=NULL;
        fobj->mode=NONE;
    }
    
    return newexpressionnone();
}

expression *io_file_eofi(object *obj, interpretcontext *context, int nargs, expression **args) {
    file_object *fobj=(file_object *) obj;
    
    if (fobj->file) {
        if (feof(fobj->file)) return newexpressionbool(TRUE);
    }
    
    return newexpressionbool(FALSE);
}

expression *io_file_readi(object *obj, interpretcontext *context, int nargs, expression **args) {
    char buffer[IO_BUFFER_SIZE];
    file_object *fobj=(file_object *) obj;
    
    if ((fobj->file)&&(fobj->mode==READ)) {
        if( fgets(buffer, IO_BUFFER_SIZE-1, fobj->file) ) {
            for (unsigned int j=0; (j<IO_BUFFER_SIZE) && (buffer[j]!='\0'); j++) {
                if (buffer[j]=='\n') buffer[j]='\0';
                if (buffer[j]=='\r') buffer[j]='\0';
            }
            buffer[IO_BUFFER_SIZE-1]='\0';
            
            return newexpressionstring(buffer);
        }
    }
    
    return newexpressionnone();
}

expression *io_file_writei(object *obj, interpretcontext *context, int nargs, expression **args) {
    file_object *fobj=(file_object *) obj;
    
    if ((fobj->file)&&(fobj->mode==WRITE)) {
        for (unsigned int j=0; j<nargs; j++) {
            if (args[j]->type==EXPRESSION_STRING) {
                fputs(((expression_string *) args[j])->value, fobj->file);
                fputc('\n', fobj->file);
            }
        }
    }
    
    return newexpressionnone();
}

/*
 * System calls
 */

expression *io_systemi(interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    if ((nargs==1)&&(eval_isstring(args[0]))) {
        ret=newexpressioninteger(system(((expression_string *)args[0])->value));
    };
    
    return ret;
}

/* ioinitialize - Initialize io
 * Input:
 * Output:
 * Returns:
 */
void ioinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(IO_FILE_CLASS, class_lookup(EVAL_OBJECT), sizeof(file_object), 5);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, io_file_freei);
    class_registerselector(cls, IO_FILE_CLOSE, FALSE, io_file_closei);
    class_registerselector(cls, IO_FILE_EOF, FALSE, io_file_eofi);
    class_registerselector(cls, IO_FILE_READ, FALSE, io_file_readi);
    class_registerselector(cls, IO_FILE_WRITE, FALSE, io_file_writei);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, io_file_newi);
    
    /* Generic intrinsic constructor function for classes */
    intrinsic(IO_FILE_OPEN, TRUE, io_file_openi, NULL);
    
    /* Make a system call */
    intrinsic(IO_SYSTEM, FALSE, io_systemi, NULL);
}

/* iofinalize - Finalize io
 * Input:
 * Output:
 * Returns:
 */
void iofinalize(void) {
}
