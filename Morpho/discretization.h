/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * Discretization - various numerical calculus facilities
 */

#include "geometry.h"

#ifndef discretization_h
#define discretization_h

#define DISCRETIZATION_ACCURACYGOAL 1e-6
#define DISCRETIZATION_MAXRECURSION 100

#define ERROR_DISCRETIZATION_RECURSION                0x9250
#define ERROR_DISCRETIZATION_RECURSION_MSG            "Maximum depth of recursion exceeded in integration."

/* Generic specification for an integrand.
 * Input: unsigned int dim      - The dimension of the space
 *        double *lambda        - Barycentric coordinates for the element
 *        double *x             - Coordinates of the point calculated from interpolation
 *        expression **quantity - List of quantities evaluated for the point, calculated froms interpolation
 *        void *ref             - A reference passed by the caller (typically things constant over the domain) 
 */
typedef double (discretizationintegrandfunction) (unsigned int dim, double *lambda, double *x, expression **quantity, void *data);

/* Integrals over standard elements with linear interpolation */
double discretization_lineintegrate(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[2], unsigned int nquantity, expression **quantity[2], void *data);
double discretization_triangleintegrate(interpretcontext *context, discretizationintegrandfunction *function, unsigned int dim, MFLD_DBL *x[3], unsigned int nquantity, expression **quantity[3], void *data);

void discretizationinitialize(void);

#endif
