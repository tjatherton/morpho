/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_graphics3d_h
#define Morpho_graphics3d_h

#define GRAPHICS3D_LABEL "graphics3d"

#include "eval.h"

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    linkedlist *list;
    float viewdirection[3];
    float viewvertical[3];
    int cliptrianglesbynormal;
    int showmesh;
} graphics3d_object;

typedef struct {
    float p1[3];
    float p2[3];
} graphics3d_bbox;

typedef enum {
    GRAPHICS3D_MOVETO,
    GRAPHICS3D_LINETO,
    GRAPHICS3D_POINT,
    GRAPHICS3D_TEXT,
    GRAPHICS3D_SETCOLOR,
    GRAPHICS3D_STROKE,
    
    GRAPHICS3D_TRIANGLE,
    GRAPHICS3D_TRIANGLE_COMPLEX
} graphics3d_elementtype;

typedef struct {
    graphics3d_elementtype type;
} graphics3d_element;

typedef graphics3d_element graphics3d_strokeelement;

typedef struct {
    graphics3d_elementtype type;
    float r;
    float g;
    float b;
} graphics3d_setcolorelement;

typedef struct {
    graphics3d_elementtype type;
    float p1[3];
    float p2[3];
    float p3[3];
} graphics3d_triangleelement;

typedef struct {
    graphics3d_elementtype type;
    float point[3];
} graphics3d_pointelement;

#include "graphics.h"

typedef struct {
    graphics3d_elementtype type;
    float point[3];
    graphics_alignment horizontal;
    graphics_alignment vertical;
    char *string;
} graphics3d_textelement;

typedef struct {
    graphics3d_elementtype type;
    unsigned int npoints;
    float *points;
    float *pointcolors;
    float *pointnormals;
    unsigned int ntriangles;
    unsigned int *triangles;
} graphics3d_trianglecomplexelement;

expression *graphics3d_newgraphics3d(void);
graphics3d_pointelement *graphics3d_newpointelement(graphics3d_elementtype type, float *p);
void graphics3d_trianglefromdouble(graphics3d_object *obj, double *p1, double *p2, double *p3);
void graphics3d_point(graphics3d_object *obj,float *pt);
void graphics3d_moveto(graphics3d_object *obj,float *pt);
void graphics3d_lineto(graphics3d_object *obj,float *pt);
void graphics3d_setcolor(graphics3d_object *obj,float r, float g, float b);
void graphics3d_stroke(graphics3d_object *obj);
void graphics3d_text(graphics3d_object *obj, char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert);

void graphics3d_arrow(graphics3d_object *obj, float *x1, float *x2, float ar, unsigned int n, float *col);
void graphics3d_cylinder(graphics3d_object *obj, float *x1, float *x2, float ar, unsigned int n, float *col);

void graphics3d_normalizevector(float *src, float *dest);
void graphics3d_vectorcopy(float v1[3],float v2[3]);
float graphics3d_vectornorm(float v1[3]);
float graphics3d_vectornormsq(float v1[3]);
void graphics3d_vectorscale(float v1[3],float v2[3], float a);
void graphics3d_vectoradd(float v1[3],float v2[3],float *v3);
void graphics3d_vectorsubtract(float v1[3],float v2[3],float *v3);
void graphics3d_vectorcrossproduct(float v1[3],float v2[3],float *v3);
float graphics3d_vectordotproduct(float v1[3],float v2[3]);
void graphics3d_vectorperp(float v1[3], float v2[3]);
void graphics3d_rotation(float *n, float theta, float *matrix);
void graphics3d_transformvector(float *src, float *matrix, float *dest);
void graphics3d_trianglenormal(float *pts, unsigned int tri[3],float *norm);

graphics3d_trianglecomplexelement *graphics3d_trianglecomplex(graphics3d_object *obj, unsigned int npoints, unsigned int ntriangles);

/* Selectors */
#define GRAPHICS3D_POINT_SELECTOR "point"
#define GRAPHICS3D_LINETO_SELECTOR "lineto"
#define GRAPHICS3D_LINE_SELECTOR "line"
#define GRAPHICS3D_MOVETO_SELECTOR "moveto"
#define GRAPHICS3D_SETCOLOR_SELECTOR "setcolor"
#define GRAPHICS3D_STROKE_SELECTOR "stroke"
#define GRAPHICS3D_TEXT_SELECTOR "text"
#define GRAPHICS3D_TRIANGLE_SELECTOR "triangle"
#define GRAPHICS3D_TRIANGLECOMPLEX_SELECTOR "trianglecomplex"
#define GRAPHICS3D_ARROW_SELECTOR "arrow"
#define GRAPHICS3D_CYLINDER_SELECTOR "cylinder"
#define GRAPHICS3D_TUBE_SELECTOR "tube"
#define GRAPHICS3D_SPHERE_SELECTOR "sphere"

#define GRAPHICS3D_JOIN_SELECTOR "join"

#define GRAPHICS3D_TOGRAPHICS_SELECTOR "tographics"
#define GRAPHICS3D_VIEWDIRECTION_SELECTOR "viewdirection"
#define GRAPHICS3D_SETVIEWDIRECTION_SELECTOR "setviewdirection"
#define GRAPHICS3D_VIEWVERTICAL_SELECTOR "vertical"
#define GRAPHICS3D_SETVIEWVERTICAL_SELECTOR "setvertical"
#define GRAPHICS3D_SHOWMESH_SELECTOR "showmesh"
#define GRAPHICS3D_SETSHOWMESH_SELECTOR "setshowmesh"
#define GRAPHICS3D_NORMALCLIPPING_SELECTOR "normalclipping"
#define GRAPHICS3D_SETNORMALCLIPPING_SELECTOR "setnormalclipping"

/* Errors */
#define GRAPHICS3D_TRIANGLE_ARGS                   0x2001
#define GRAPHICS3D_TRIANGLE_ARGS_MSG               "triangle expects three points as its arguments.\n"

#define GRAPHICS3D_TEXT_ARGS                       0x2002
#define GRAPHICS3D_TEXT_ARGS_MSG                   "text expects a set of coordinates as the first argument.\n"

#define GRAPHICS3D_TEXTSTRING_ARGS                 0x2003
#define GRAPHICS3D_TEXTSTRING_ARGS_MSG             "text expects a string as the second argument.\n"

#define GRAPHICS3D_JOIN_ARGS                       0x2004
#define GRAPHICS3D_JOIN_ARGS_MSG                   "join expects a second graphics3d object as its argument.\n"


void graphics3d_calculatebbox(graphics3d_object *g, graphics3d_bbox *bbox);
expression *graphics3d_tographics(object *obj, interpretcontext *context, int nargs, expression **args);
expression *graphics3d_free(object *obj, interpretcontext *context, int nargs, expression **args);

void graphics3dinitialize(void);

#endif
