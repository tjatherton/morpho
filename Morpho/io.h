/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */


#ifndef Morpho_io_h
#define Morpho_io_h

#include "eval.h"
#include <stdio.h>

#define IO_FILE_CLASS "file"
#define IO_FILE_OPEN "open"

#define IO_FILE_CLOSE "close"
#define IO_FILE_EOF "eof"
#define IO_FILE_READ "read"
#define IO_FILE_WRITE "write"
#define IO_FILE_APPEND "append"

#define IO_SYSTEM "system"

#define IO_BUFFER_SIZE 2048

typedef enum { NONE, READ, WRITE, APPEND } filemode;

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    FILE *file;
    
    filemode mode;
    
} file_object;

/* Other components of morpho should use these functions to access files, because this allows abstraction to other
   I/O types. */
expression_objectreference *io_open(interpretcontext *context, expression *exp);
expression *io_read(interpretcontext *context, expression_objectreference *objref);
int io_eof(interpretcontext *context, expression_objectreference *objref);
void io_close(interpretcontext *context, expression_objectreference *objref);

expression *io_file_openi(interpretcontext *context, int nargs, expression **args);

void ioinitialize(void);
void iofinalize(void);

#endif
