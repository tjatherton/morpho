/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "constants.h"

hashtable *constants;

/* constant_int - Declare a constant integer value. */
void constant_int(char *label, int value) {
    expression *exp = newexpressioninteger(value);
    if (exp) hashinsert( constants, label, (void *) exp);
}

/* constant_float - Declare a constant integer value. */
void constant_float(char *label, double value) {
    expression *exp = newexpressionfloat(value);
    if (exp) hashinsert( constants, label, (void *) exp);
}

/* constant_complex - Declare a constant complex value. */
void constant_complex(char *label, complex double value) {
    expression *exp = newexpressioncomplex(value);
    if (exp) hashinsert( constants, label, (void *) exp);
}

/* constant_bool - Declare a constant boolean value. */
void constant_bool(char *label, int value) {
    expression *exp = newexpressionbool(value);
    if (exp) hashinsert( constants, label, (void *) exp);
}

/* constant_insert - Insert an expression as a constant. */
void constant_insert(char *label, expression *exp) {
    if (exp) hashinsert( constants, label, (void *) exp);
}

/* Returns a list of all available constants */
expression *eval_constantsi(interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = linkedlist_new();
    expression *ret = NULL;
    
    if (list) {
        hashmap(constants, eval_name_map, list);
        
        ret=(expression *) newexpressionlist(list);
        
        linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(list);
    }
    
    return ret;
}

/* evalinitializeconstants - Initialize constants.
 * Input:
 * Output:
 * Returns:
 */
void evalinitializeconstants(void) {
    constants = hashcreate(EVAL_CONSTANTTABLESIZE);
    if (constants) {
        constant_float("pi",PI);
        constant_float("e",2.7182818284590452);
        constant_complex("I", I);
        constant_bool("true",TRUE);
        constant_bool("false",FALSE);
    }
    
    intrinsic("constants",FALSE,eval_constantsi,NULL);
}

/* evalinitializeconstants - Finalize constants.
 * Input:
 * Output:
 * Returns:
 */
void evalfinalizeconstants(void) {
    if(constants) {
        hashmap(constants, &evalhashfreeexpression,NULL);
        hashfree(constants);
    }
}
