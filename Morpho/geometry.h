/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* Overall header file for differential geometry elements */

/* These comprise:
    Body        — A high level class that may contain manifolds, fields, energies and discretizations
    Manifold    - A class that represents a mesh and functions to manipulate it
    Field       - Field quantities that are defined with respect to a manifold
    Multivector - An elementary data type for geometric quantities
    Functional  - Functionals defined on manifolds and fields
    Selection   - Selections of manifolds
 
    IDTable     - A dictionary data structure using ids as the key.
 */

#ifndef geometry_h
#define geometry_h

#include "eval.h"
#include "multivector.h"
#include "idtable.h"
#include "manifold.h"
#include "field.h"
#include "body.h"
#include "selection.h"
#include "visualize.h"
#include "discretization.h"
#include "functional.h"
#include "constructors.h"

void geometryinitialize(void);

#endif
