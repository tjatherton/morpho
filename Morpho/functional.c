/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "functional.h"

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#include <cblas.h>
#include <lapacke.h>
#endif

/* ----------------------------------------------------------------------------------------------------
 * Generic Functional Class
 * ----------------------------------------------------------------------------------------------------  */

/*
 * Selectors
 */

/* Initializes the field */
expression *functional_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    fobj->prefactor=1.0;
    fobj->grade=0;
    fobj->integrand=NULL;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    fobj->dependencies=NULL;
    fobj->param=NULL;
    fobj->field=NULL;
    fobj->target=NULL;
    fobj->flags=0;
    
    return NULL;
}

/* Frees the field and attached expressions */
expression *functional_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    if (fobj->param) freeexpression(fobj->param);
    if (fobj->target) freeexpression(fobj->target);
    //if (fobj->field) freeexpression(fobj->field);
    
    return NULL;
}

/* Gets the current prefactor */
expression *functional_prefactori(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    return newexpressionfloat(fobj->prefactor);
}

/* Sets the functional's prefactor */
expression *functional_setprefactori(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    if ((nargs==1)&&(eval_isreal(args[0]))) {
        fobj->prefactor=eval_floatvalue(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

// Veneer onto gettarget
int functional_gettarget(object *obj, interpretcontext *context, MFLD_DBL *target) {
    int success=FALSE;
    
    if (class_objectrespondstoselector(obj, GEOMETRY_TARGET_SELECTOR)) {
        expression *exp=class_callselector(context, obj, GEOMETRY_TARGET_SELECTOR, 0);
        
        if (eval_isreal(exp)) {
            if (target) *target = (MFLD_DBL) eval_floatvalue(exp);
            success=TRUE; 
        }
        
        if (exp) freeexpression(exp);
    }
                
    return success;
}

/* Gets the current target */
expression *functional_targeti(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    expression *ret=NULL;
    
    if (fobj->target) ret=cloneexpression(fobj->target);
    else ret=newexpressionnone();
    
    return ret;
}

/* Sets the functional's target */
expression *functional_settargeti(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    if ((nargs==1)&&(eval_isreal(args[0]))) {
        fobj->target=cloneexpression(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Generic function to get the parameter value */
expression *functional_genericgetparami(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression *ret=NULL;
    
    if (obj) {
        if(fobj->param) {
            ret=cloneexpression(fobj->param);
        } else {
            error_raise(context, ERROR_NOVALUE, ERROR_NOVALUE_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

/* Generic function to set the parameter value */
expression *functional_genericsetparami(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    if (nargs==1) {
        if (fobj->param) freeexpression(fobj->param);
        
        fobj->param=cloneexpression(args[0]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Get the grade on which the functional is defined */
expression *functional_gradei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj = (functional_object *) obj;
    
    return newexpressioninteger(fobj->grade);
}

/* Generic total energy functional */
expression *functional_generictotali(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *field=NULL;
    expression *ret=NULL;
    
    if (class_objectrespondstoselector(obj, GEOMETRY_INTEGRAND_SELECTOR)) {
        /* First obtain the integrand (this must be returned as an object that responds to 'total') */
        field=class_callselectorwithargslist(context, obj, GEOMETRY_INTEGRAND_SELECTOR, nargs, args);
        
        if ((field)&&(eval_isobjectref(field))) {
            expression_objectreference *efield = (expression_objectreference *) field;
            
            /* Call 'total' */
            if (class_objectrespondstoselector(efield->obj, GEOMETRY_FIELDTOTAL)) {
                ret = class_callselector(context,efield->obj, GEOMETRY_FIELDTOTAL, 0);
                
            }
        }
        
        /* Now free the field */
        if (field) freeexpression(field);
    } else {
        error_raise(context, ERROR_FUNCTIONAL, ERROR_FUNCTIONAL_MSG, ERROR_FATAL);
    }
    
    return ret;
}

typedef struct {
    MFLD_DBL scale;
    interpretcontext *context;
} functional_rescalemapfunctionref;

void functional_rescalemapfunction(uid id, void *content, void *ref) {
    functional_rescalemapfunctionref *eref = (functional_rescalemapfunctionref *) ref;
    expression *exp=(expression *) content;
    if (eval_isreal(exp)) {
        ((expression_float *) exp)->value *= eref->scale ;
    } else if (eval_isobjectref(exp)) {
        multivector_scale((multivectorobject *) ((expression_objectreference *) exp)->obj, eref->context, eref->scale);
    }
}

/* Generic integrand -- subclasses should set up links to fobj->integrand to define an appropriate map function
 (or implement their own) */
expression *functional_integrandi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    manifold_object *mobj = NULL;
    selection_object *sobj = NULL;
    idtable *sel=NULL;
    expression_objectreference *field = NULL;
    functionalmapref ref;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        mobj = (manifold_object *) ((expression_objectreference *) args[0])->obj;
    }
    
    if ((nargs>1)&&(eval_isobjectref(args[1]))) {
        sobj = (selection_object *) ((expression_objectreference *) args[1])->obj;
    }
    
    if ((fobj)&&(mobj)) {
        if (!fobj->integrand) {
            error_raise(context, ERROR_FUNCTIONAL, ERROR_FUNCTIONAL_MSG, ERROR_FATAL);
        }
        
        ref.mobj=mobj;
        ref.fobj=fobj;
        ref.field=NULL;
        ref.sobj=sobj;
        ref.context=context;
        
        if ((mobj->clss==class_lookup(MANIFOLD_LABEL))&&(fobj->grade==MANIFOLD_GRADE_POINT || mobj->gradelower[fobj->grade-1]) ) {
            /* Create a new field to hold the results */
            field = (expression_objectreference *) field_new(mobj, fobj->grade);
            
            if (field) {
                ref.target=(field_object *) field->obj;
                if (sobj) {
                    sel = selection_idtableforgrade(sobj, mobj, fobj->grade);
                    if (!sel) {
                        char msg[255];
                        sprintf(msg, ERROR_EMPTYSELECTION_MSG, fobj->grade);
                        error_raise(context, ERROR_EMPTYSELECTION, msg, ERROR_WARNING);
                    }
                }
                
                idtable *t1=mobj->vertices;
                if (fobj->grade!=MANIFOLD_GRADE_POINT) t1=mobj->gradelower[fobj->grade-1];
                
                if (fobj->flags & TUPLES_FLAG) {
                    idtable_maptuples(t1, sel, (idtablemaptuplefunction *) fobj->integrand, &ref);
                } else {
                    if (sel) {
                        idtable_mapintersection(t1, sel, fobj->integrand, &ref);
                    } else {
                        idtable_map(t1, fobj->integrand, &ref);
                    }
                }
                
                /* Rescale the energy if prefactor is not 1.0 */
                if (fabs(fobj->prefactor-1.0)>MACHINE_EPSILON) {
                    functional_rescalemapfunctionref ref;
                    ref.context=context; ref.scale=fobj->prefactor;
                    field_map((field_object *) field->obj, functional_rescalemapfunction, &ref);
                }
            }
            
        }
    }
    
    return (expression *) field;
}

#define NUMERICALFORCEEPS 1e-6

void functional_numericalforceforvertex(uid id, void *content, void *ref, uid v) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp=NULL;
    unsigned int dim=eref->mobj->dimension;
    idtable *targetentries=eref->target->entries;
    
    /* Patch in a temporary idtable into the target field to receive the output of calls to integrand. */
    idtable *temp=idtable_create(1);
    if (!temp) return;
    eref->target->entries=temp;
    
    manifold_vertex *vert=idtable_get(eref->mobj->vertices, v);
    MFLD_DBL frc[dim];
    
    /* Loop over the coordinates of x */
    if (vert) for (unsigned int i=0; i<dim; i++) {
        MFLD_DBL x0=vert->x[i];
        MFLD_DBL a0=0.0, a1=0.0;
        
        vert->x[i]=x0+NUMERICALFORCEEPS;
        eref->fobj->integrand(id, content, eref);
        exp = field_get(eref->target, id);
        if (eval_isnumerical(exp)) a0=eval_floatvalue(exp);
        field_insertwithid(eref->target, id, NULL); // Free the value for the integrand
        
        vert->x[i]=x0-NUMERICALFORCEEPS;
        eref->fobj->integrand(id, content, ref);
        exp = field_get(eref->target, id);
        if (eval_isnumerical(exp)) a1=eval_floatvalue(exp);
        field_insertwithid(eref->target, id, NULL); // Free the value for the integrand
        
        vert->x[i]=x0;
        
        frc[i]=0.5*(a1-a0)/NUMERICALFORCEEPS;
    }
    
    /* Get rid of the temporary idtable */
    idtable_free(temp);
    
    /* Patch the target entries back in */
    eref->target->entries=targetentries;
    
    /* Add the force multivector to the field */
    expression *mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, frc, dim);
    if (mv) field_accumulatewithid(eref->target, eref->context, v, mv);
    if (mv) freeexpression(mv);
}

/* Numerically evaluate the force */
void functional_numericalforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    unsigned int grade = eref->fobj->grade; // The grade on which the functional acts
    uid v[eref->fobj->grade+1];
    unsigned int nelements=1;
    
    /* Find all the vertices present in the element */
    if (grade>MANIFOLD_GRADE_POINT) {
        manifold_gradelower(eref->mobj, grade, id, MANIFOLD_GRADE_POINT, v, &nelements);
    } else {
        v[0]=id;
    }
    
    for (unsigned int i=0; i<nelements; i++) {
        functional_numericalforceforvertex(id, content, ref, v[i]);
    }
    
    /* If there are other dependencies, also include those */
    if (eref->dependencies) {
        expression_list *lst = (expression_list *) field_get(eref->dependencies, id);
        
        if (eval_islist(lst) && lst->list) {
            for (linkedlistentry *e = lst->list->first; e!=NULL; e=e->next) {
                expression *did = (expression *) e->data;
                if (eval_isinteger(did)) {
                    uid vid = eval_integervalue(did);
                    functional_numericalforceforvertex(id, content, ref, vid);
                }
            }
        }
    }
}

/* Generic force -- subclasses should set up links to fobj->force to define an appropriate map function */
expression *functional_forcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    manifold_object *mobj = NULL;
    selection_object *sobj = NULL;
    idtable *sel=NULL;
    expression_objectreference *field = NULL;
    idtablemapfunction *fmapfunc=NULL;
    functionalmapref ref;
    expression *dep=NULL;
    
    if ((nargs>0)&&(eval_isobjectref(args[0]))) {
        mobj = (manifold_object *) ((expression_objectreference *) args[0])->obj;
    }
    
    if ((nargs>1)&&(eval_isobjectref(args[1]))) {
        sobj = (selection_object *) ((expression_objectreference *) args[1])->obj;
    }
    
    if ((fobj)&&(mobj)) {
        fmapfunc=fobj->force;
        if (!fmapfunc) {
            if (fobj->integrand) {
                fmapfunc = functional_numericalforce_mapfunction;
            } else error_raise(context, ERROR_FUNCTIONAL, ERROR_FUNCTIONAL_MSG, ERROR_FATAL);
        }
        
        ref.mobj=mobj;
        ref.fobj=fobj;
        ref.sobj=sobj;
        ref.field=NULL;
        ref.dependencies=NULL;
        ref.context=context;
        
        idtable *t1=mobj->vertices;
        if (fobj->grade!=MANIFOLD_GRADE_POINT) t1=mobj->gradelower[fobj->grade-1];
        
        if (sobj) sel = selection_idtableforgrade(sobj, mobj, fobj->grade);
        
        /* Determine dependencies for functionals that have dependencies beyond the scope of individual elements. */
        if (fobj->dependencies) {
            /* Create a new field to hold the dependencies */
            dep=field_new(mobj, fobj->grade);
            
            if (eval_isobjectref(dep)) {
                ref.dependencies=(field_object *) eval_objectfromref(dep);
                
                if (sel) {
                    idtable_mapintersection(t1, sel, fobj->dependencies, &ref);
                } else {
                    idtable_map(t1, fobj->dependencies, &ref);
                }
            }
        }
        
        if ((mobj->clss==class_lookup(MANIFOLD_LABEL))&&(fobj->grade==MANIFOLD_GRADE_POINT || mobj->gradelower[fobj->grade-1])) {
            /* Create a new field to hold the results */
            field = (expression_objectreference *) field_new(mobj, MANIFOLD_GRADE_POINT); /* Note forces are defined on vertices */
            
            if (field) {
                ref.target=(field_object *) field->obj;
                
                if (fobj->flags & TUPLES_FLAG) {
                    if (fobj->force) {
                        idtable_maptuples(t1, sel, (idtablemaptuplefunction *) fobj->force, &ref);
                    } else {
                        printf("Warning: This functional is defined over tuples but doesn't supply a force function.\n");
                    }
                } else {
                    if (sel) {
                        idtable_mapintersection(t1, sel, fmapfunc, &ref);
                    } else {
                        idtable_map(t1, fmapfunc, &ref);
                    }
                }
                
                /* Rescale the forces if prefactor is not 1.0 */
                if (fabs(fobj->prefactor-1.0)>MACHINE_EPSILON) {
                    functional_rescalemapfunctionref ref;
                    ref.context=context; ref.scale=fobj->prefactor;
                    field_map((field_object *) field->obj, functional_rescalemapfunction, &ref);
                }
            }
        }
        
        if (dep) freeexpression(dep);
    }
    
    return (expression *) field;
}

expression *functional_indexobject(interpretcontext *context, object *obj, expression *index) {
    expression *ret=NULL;
    if (eval_isinteger(index)) {
        ret=class_callselectorwithargslist(context, obj, EVAL_INDEX, 1, &index);
    } else if (eval_islist(index)) {
        unsigned int n=eval_listlength((expression_list *) index);
        if (n>0) {
            expression *ind[n];
            if (eval_listtoexpressionarray((expression_list *) index, ind, n)) {
                ret=class_callselectorwithargslist(context, obj, EVAL_INDEX, n, ind);
                for (unsigned int i=0; i<n; i++) if (ind[i]) freeexpression(ind[i]);
            }
        }
    }
    return ret;
}

expression *functional_setindexobject(interpretcontext *context, object *obj, expression *index, expression *value) {
    expression *ret=NULL;
    if (eval_isinteger(index)) {
        expression *ind[2] = { value, index };
        ret=class_callselectorwithargslist(context, obj, EVAL_SETINDEX, 2, ind);
    } else if (eval_islist(index)) {
        unsigned int n=eval_listlength((expression_list *) index);
        if (n>0) {
            expression *ind[n+2];
            for (unsigned int i=0; i<=n+1; i++) ind[i]=NULL;
            ind[0]=value;
            if (eval_listtoexpressionarray((expression_list *) index, ind+1, n)) {
                ret=class_callselectorwithargslist(context, obj, EVAL_SETINDEX, n+1, ind);
                for (unsigned int i=1; i<=n+1; i++) if (ind[i]) freeexpression(ind[i]);
            }
        }
    }
    return ret;
}

void functional_numericalgeneralizedforceforvertex(uid id, void *content, void *ref, uid v) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *fld=eref->field;
    idtable *targetentries=eref->target->entries;
    expression *valueref=NULL;
    object *value=NULL;
    expression *forceref=NULL;
    object *force=NULL;
    expression *indices=NULL;
    
    /* If no field is requested, simply return */
    if (!fld) return;
    
    valueref=field_get(fld, v); // Don't free this!
    
    // Don't adjust fixed fields r
    if (fld->fix) {
        if (idtable_ispresent(fld->fix, v)) return;
    }
    
    /* Now attempt to get the relevant indices available from the object */
    if (eval_isobjectref(valueref)) {
        value=eval_objectfromref(valueref);
        if (class_objectrespondstoselector(value, EVAL_INDICES)) {
            indices=class_callselector(eref->context, value, EVAL_INDICES, 0);
            if (!eval_islist(indices)) {
                error_raise(eref->context, ERROR_INDICESNOLIST, ERROR_INDICESNOLIST_MSG, ERROR_FATAL);
            }
        } else {
            error_raise(eref->context, ERROR_NOINDICES, ERROR_NOINDICES_MSG, ERROR_FATAL);
        }
    }
    
    /* Clone the object to use as the force */
    if (class_objectrespondstoselector(value, EVAL_CLONE)) {
        forceref=class_callselector(eref->context, value, EVAL_CLONE, 0);
        if (eval_isobjectref(forceref)) force=eval_objectfromref(forceref);
    }
    
    /* Patch in a temporary idtable into the target field to receive the output of calls to integrand. */
    idtable *temp=idtable_create(1);
    if (!temp) return;
    eref->target->entries=temp;
    
    /* Loop over each index */
    for (linkedlistentry *e = ((expression_list *) indices)->list->first; e!=NULL; e=e->next) {
        expression *cv=NULL;
        expression *x0=NULL, *x1=NULL;
        expression *exp=NULL;
        double a0=0.0,a1=0.0;
        /* Get the current value */
        cv=functional_indexobject(eref->context, value, (expression *) e->data);
        
        if (eval_isreal(cv)) {
            /* eps */
            expression_float eps;
            eps.type=EXPRESSION_FLOAT;
            eps.value=NUMERICALFORCEEPS;
            
            /* Evaluate +ve direction */
            x1=opadd(eref->context,cv,(expression *) &eps);
            freeexpression(functional_setindexobject(eref->context, value, (expression *) e->data, x1));
            
            /* And evaluate the integrand */
            eref->fobj->integrand(id, content, eref);
            exp = field_get(eref->target, id);
            if (eval_isreal(exp)) a1=eval_floatvalue(exp);  else error_raise(eref->context, ERROR_NONNUMERICAL, ERROR_NONNUMERICAL_MSG, ERROR_FATAL);
            field_insertwithid(eref->target, id, NULL); // Causes the integrand to be freed
            
            /* Evaluate -ve direction */
            x0=opsub(eref->context,cv,(expression *) &eps);
            freeexpression(functional_setindexobject(eref->context, value, (expression *) e->data, x0));
            
            /* And evaluate the integrand */
            eref->fobj->integrand(id, content, eref);
            exp = field_get(eref->target, id);
            if (eval_isreal(exp)) a0=eval_floatvalue(exp); else error_raise(eref->context, ERROR_NONNUMERICAL, ERROR_NONNUMERICAL_MSG, ERROR_FATAL);
            field_insertwithid(eref->target, id, NULL); // Causes the integrand to be freed
            
            /* Restore value */
            freeexpression(functional_setindexobject(eref->context, value, (expression *) e->data, cv));
            
            /* Now calculate generalized force */
            eps.value=-0.5*(a1-a0)/NUMERICALFORCEEPS;
            freeexpression(functional_setindexobject(eref->context, force, (expression *) e->data, (expression *) &eps));
        } else {
            error_raise(eref->context, ERROR_NONNUMERICAL, ERROR_NONNUMERICAL_MSG, ERROR_FATAL);
        }
        
        if (x0) freeexpression(x0);
        if (x1) freeexpression(x1);
        if (cv) freeexpression(cv);
    }
    
    /* Get rid of the temporary idtable */
    idtable_free(temp);
    
    /* Patch the target entries back in */
    eref->target->entries=targetentries;
    
    if (indices) freeexpression(indices);
    if (forceref) field_accumulatewithid(eref->target, eref->context, v, forceref);
    if (forceref) freeexpression(forceref);
}

/* Numerically evaluate the generalized force */
void functional_numericalgeneralizedforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    unsigned int grade = eref->fobj->grade; // The grade on which the functional acts
    uid v[eref->fobj->grade+1];
    unsigned int nelements=1;
    
    /* Find all the vertices present in the element */
    if (grade>MANIFOLD_GRADE_POINT) {
        manifold_gradelower(eref->mobj, grade, id, MANIFOLD_GRADE_POINT, v, &nelements);
    } else {
        v[0]=id;
    }
    
    /* Now evaluate the derivative due to the field defined on these vertices */
    for (unsigned int i=0; i<nelements; i++) {
        functional_numericalgeneralizedforceforvertex(id, content, ref, v[i]);
    }
}

expression *functional_generalizedforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    manifold_object *mobj = NULL;
    selection_object *sobj = NULL;
    field_object *fldobj = NULL;
    idtable *sel = NULL;
    idtablemapfunction *fmapfunc=NULL;
    expression_objectreference *target = NULL;
    functionalmapref ref;
    
    for (unsigned int i=0; i<nargs; i++) {
        if (eval_isobjectref(args[i])) {
            expression_objectreference *oref = (expression_objectreference *) args[i];
            
            if (oref->obj) {
                if (oref->obj->clss==class_lookup(MANIFOLD_LABEL)) mobj=(manifold_object *) oref->obj;
                else if (oref->obj->clss==class_lookup(GEOMETRY_SELECTIONCLASS)) sobj=(selection_object *)  oref->obj;
                else if (oref->obj->clss==class_lookup(GEOMETRY_FIELDCLASS)) fldobj=(field_object *)  oref->obj;
            }
        }
    }
    
    if ((fobj)&&(mobj)) {
        fmapfunc = fobj->generalizedforce;
        if (!fmapfunc) {
            if (fobj->integrand) {
                fmapfunc = functional_numericalgeneralizedforce_mapfunction;
            } else error_raise(context, ERROR_FUNCTIONAL, ERROR_FUNCTIONAL_MSG, ERROR_FATAL);
        }
        
        ref.mobj=mobj;
        ref.fobj=fobj;
        ref.field=fldobj;
        ref.context=context;
        
        /* Verify that the manifold possesses sufficient grade */
        if ((fobj->grade==MANIFOLD_GRADE_POINT || mobj->gradelower[fobj->grade-1])) {
            /* Create a new field to hold the results */
            target = (expression_objectreference *) field_new(mobj, MANIFOLD_GRADE_POINT); /* Note generalized forces are defined on vertices */
            
            if (target) {
                ref.target=(field_object *) target->obj;
                if (sobj) sel = selection_idtableforgrade(sobj, mobj, fobj->grade);
                
                idtable *t1=mobj->vertices;
                if (fobj->grade!=MANIFOLD_GRADE_POINT) t1=mobj->gradelower[fobj->grade-1];
                
                if (sel) {
                    idtable_mapintersection(t1, sel, fmapfunc, &ref);
                } else {
                    idtable_map(t1, fmapfunc, &ref);
                }
                
                /* Rescale the forces if prefactor is not 1.0 */
                if (fabs(fobj->prefactor-1.0)>MACHINE_EPSILON) {
                    functional_rescalemapfunctionref ref;
                    ref.context=context; ref.scale=fobj->prefactor;
                    field_map((field_object *) target->obj, functional_rescalemapfunction, &ref);
                }
            }
        }
    }
    
    return (expression *) target;
}

/* The generic functional is unable to compute totals, forces or integrands */
expression *functional_genericfaili(object *obj, interpretcontext *context, int nargs, expression **args) {
    error_raise(context, ERROR_FUNCTIONAL, ERROR_FUNCTIONAL_MSG, ERROR_FATAL);
    
    return NULL;
}

/* ----------------------------------------------------------------------------------------------------
 * Utility functions
 * ----------------------------------------------------------------------------------------------------  */

int functional_isonesided(expression_objectreference *func, interpretcontext *context) {
    if (!func) return FALSE;
    expression *ret=NULL;
    int result=FALSE;
    
    if (class_objectrespondstoselector(func->obj, GEOMETRY_GETONESIDED_SELECTOR)) {
        ret=class_callselector(context, func->obj, GEOMETRY_GETONESIDED_SELECTOR, 0);
    }
    
    if (ret) {
        if (eval_isbool(ret)) result=eval_boolvalue(ret);
        freeexpression(ret);
    }
    
    return result;
}

/* ----------------------------------------------------------------------------------------------------
 * Built-in Functionals
 * ----------------------------------------------------------------------------------------------------  */

/* A functional implements the protocol:
 -total()        -
 -integrand()    -
 -force()        -
*/

/* --- Levelset constraint --- */

#define LEVELSET_ONESIDED "o"
#define LEVELSET_EXPRESSION "l"
#define LEVELSET_GRADIENT "g"
#define LEVELSET_COORDINATES "c"

/* Gets the hashtable associated with the level set */
hashtable *levelset_findhashtable(functional_object *fobj, int create) {
    hashtable *ht = NULL;
    
    if (fobj->param) {
        expression_objectreference *htref=(expression_objectreference *) fobj->param;
        hashtableobject *htobj=NULL;
        
        if (htref) htobj=(hashtableobject *) eval_objectfromref(htref);
        if (htobj) ht=htobj->table;
    } else {
        if (create) {
            ht=hashcreate(4);
            if (ht) fobj->param=(expression *) class_hashtable_objectfromhashtable(ht);
        }
    }
    
    return ht;
}

/* Evaluates an expression given coordinate values
 Input -
    interpretcontext *context    - a host context for error reporting. Evaluation occurs in a new sandboxed context
    expression       *exp        - an expression to evaluate
    unsigned int     dim         - dimensionality of the coordinates
    expression_list  *coords     - list of coordinates. If NULL uses standard 'x', 'y', 'z' etc.
    MFLD_DBL         *x          - Coordinate values
 Returns -
    Evaluated expression.
    */
expression *levelset_evaluateexpressionwithcoordinates(interpretcontext *context, expression *exp, unsigned int dim, expression_list *coords, MFLD_DBL *x) {
    interpretcontext *localcntxt=newinterpretcontext(context, dim); // Create a new context
    expression *result=NULL;
    
    /* Validation */
    if (!localcntxt) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
    if (!eval_islist(coords)) {
        error_raise(context, ERROR_COORDINATES, ERROR_COORDINATES_MSG, ERROR_FATAL);
    }
    if (eval_listlength(coords)<dim) {
        error_raise(context, ERROR_COORDINATESDIM, ERROR_COORDINATESDIM_MSG, ERROR_FATAL);
    }
    
    /* Substitute the ith coordinate into the appropriate name */
    for (unsigned int i=0; i<dim; i++) {
        expression_float *val = (expression_float *) newexpressionfloat(x[i]);
        
        freeexpression(opassign(localcntxt, eval_getelement(coords, i+1 /* Morpho indexing! */), (expression *) val));
        if (val) freeexpression((expression *) val);
        
    }
    
    result=interpretexpression(localcntxt, exp);
    
    /* Destroy the context */
    freeinterpretcontext(localcntxt);
    
    return result;
}

/* Evaluates a quantity stored in the levelset dictionary with specified coordinate values
 Input -
   functional_object *fobj   - The functional object
   interpretcontext *context - The current context for error reporting
   char *quantity            - id of the quantity to evaluate
   unsigned int dim          - The dimensionality
   MFLD_DBL *x               - Current coordinate values
 */
expression *levelset_evaluatequantity(functional_object *fobj, interpretcontext *context, char *quantity, unsigned int dim, MFLD_DBL *x) {
    expression_list *coordinates=NULL;
    expression *exp=NULL, *result=NULL;
    
    /* Obtain coordinates and expression from table */
    hashtable *ht=levelset_findhashtable(fobj, TRUE);
    if (ht) {
        coordinates=(expression_list *) hashget(ht, LEVELSET_COORDINATES);
        exp=(expression *) hashget(ht, quantity);
    }
    
    if (exp) {
        result=levelset_evaluateexpressionwithcoordinates(context, exp, dim, coordinates, x);
    }

    return result;
}

expression *levelset_setonesidedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    
    // Validate arguments
    if (nargs==1 && eval_isbool(args[0])) {
        ht=levelset_findhashtable(fobj, TRUE);
        if (ht) {
            hashinsert(ht, LEVELSET_ONESIDED, cloneexpression(args[0]));
        }
    }
        
    return (expression *) class_newobjectreference(obj);
}

expression *levelset_getonesidedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    int onesided=FALSE;
    
    ht=levelset_findhashtable(fobj, FALSE);
    if (ht) {
        expression_bool *os=(expression_bool *) hashget(ht, LEVELSET_ONESIDED);
        if (eval_isbool(os)) onesided=os->value;
    }
    
    return newexpressionbool(onesided);
}

/* Tests if a levelset object is onesided */
int levelset_isonesided(functional_object *fobj) {
    hashtable *ht=levelset_findhashtable(fobj, TRUE);
    expression_bool *os=(expression_bool *) hashget(ht, LEVELSET_ONESIDED);
    int ret=FALSE;
    
    if (eval_isbool(os)) ret=os->value;
    
    return ret;
}

/* Tests if a vertex with position x would be active, i.e. violates the constraint */
int levelset_isactive(functional_object *fobj, interpretcontext *context, int dim, MFLD_DBL *x) {
    expression *result;
    int ret=FALSE;

    result = levelset_evaluatequantity(fobj, context, LEVELSET_EXPRESSION, dim, x);
    
    if (eval_isreal(result)) {
        if (eval_floatvalue(result)<0) ret=TRUE;
    }
    
    if (result) freeexpression(result);
    
    return ret;
}

expression *levelset_setexpressioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    
    if (nargs==1) {
        ht=levelset_findhashtable(fobj, TRUE);
        if (ht) {
            hashinsert(ht, LEVELSET_EXPRESSION, cloneexpression(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *levelset_getexpressioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    expression *ret=NULL;
    
    ht=levelset_findhashtable(fobj, FALSE);
    if (ht) {
        ret=hashget(ht, LEVELSET_EXPRESSION);
    }
    
    return ret;
}

expression *levelset_setcoordinatesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    
    if (nargs==1 && eval_islist(args[0])) {
        ht=levelset_findhashtable(fobj, TRUE);
        
        if (ht) {
            hashinsert(ht, LEVELSET_COORDINATES, cloneexpression(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *levelset_getcoordinatesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    expression *ret=NULL;
    
    ht=levelset_findhashtable(fobj, FALSE);
    if (ht) {
        ret=hashget(ht, LEVELSET_COORDINATES);
    }
    
    return cloneexpression(ret);
}

expression *levelset_setgradienti(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    
    if (nargs==1) {
        ht=levelset_findhashtable(fobj, TRUE);
        if (ht) {
            hashinsert(ht, LEVELSET_GRADIENT, cloneexpression(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *levelset_getgradienti(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    expression *ret=NULL;
    
    ht=levelset_findhashtable(fobj, FALSE);
    if (ht) {
        ret=hashget(ht, LEVELSET_GRADIENT);
    }
    
    return cloneexpression(ret);
}


void levelset_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v = (manifold_vertex *) content;
    expression *result=NULL;
    
    result = levelset_evaluatequantity(eref->fobj, eref->context, LEVELSET_EXPRESSION, eref->mobj->dimension, v->x);
    
    if (eval_isreal(result)) field_insertwithid(eref->target, id, result);
    else freeexpression(result);
}

void levelset_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v = (manifold_vertex *) content;
    expression_list *result=NULL;
    expression *frc=NULL;
    
    if (levelset_isonesided(eref->fobj)) {
        if (!levelset_isactive(eref->fobj, eref->context, eref->mobj->dimension, v->x)) return; 
    }
    
    result = (expression_list *) levelset_evaluatequantity(eref->fobj, eref->context, LEVELSET_GRADIENT, eref->mobj->dimension, v->x);
    
    if (eval_islist(result)) {
        unsigned int length = eval_listlength(result);
        MFLD_DBL f[length];
        
        eval_listtodoublearray(result, f, length);
        for (unsigned int i=0; i<length; i++) f[i]=-f[i]; /* The force is the negative gradient */
        frc=multivector_doublelisttomultivector(eref->context, eref->mobj->dimension, MANIFOLD_GRADE_LINE, f, length);
        

        field_accumulatewithid(eref->target, eref->context, id, frc);
        freeexpression(frc);
    }
    
    if (result) freeexpression((expression *) result);
}

expression *levelset_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=levelset_integrand_mapfunction;
    fobj->force=levelset_force_mapfunction;
    
    return NULL;
}

/* --- Constant --- */

void constantforce_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v = (manifold_vertex *) content;
    expression *frc = NULL, *xx=NULL;
    expression_objectreference *mul=NULL;
    
    if ((eref) && (eref->fobj)) frc=(expression *) eref->fobj->param;
    
    if (frc) {
        xx=multivector_doublelisttomultivector(eref->context, eref->mobj->dimension, MANIFOLD_GRADE_LINE, v->x, eref->mobj->dimension);
        
        mul=(expression_objectreference *) opmul(eref->context, frc, xx);
        if (mul) {
            MFLD_DBL vv;
            multivector_gradetodoublelist((multivectorobject *) mul->obj, MANIFOLD_GRADE_POINT, &vv);
            
            field_insertwithid(eref->target, id, newexpressionfloat(-vv));
        }
    }
    
    if (xx) freeexpression(xx);
    if (mul) freeexpression((expression *) mul);
}

void constantforce_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression_objectreference *frc = NULL;
    
    if ((eref) && (eref->fobj)) frc=(expression_objectreference *) eref->fobj->param;
    
    if (frc) {
        field_accumulatewithid(eref->target, eref->context, id, (expression *) frc);
    }
}

expression *constantforce_getforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression *ret=NULL;
    
    if (obj) {
        if(fobj->param) {
            ret=cloneexpression(fobj->param);
        } else {
            error_raise(context, ERROR_NOFORCE, ERROR_NOFORCE_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

expression *constantforce_setforcei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression_list *list=NULL;
    expression_objectreference *mv;
    unsigned int length=0;
    
    if ((nargs==1)&&(eval_islist(args[0]))) {
        list = (expression_list *) args[0];
        length=eval_listlength(list);
        
        double *f = EVAL_MALLOC(length*sizeof(double));
        if (eval_listtodoublearray(list, f, length)) {
            mv=(expression_objectreference *) multivector_doublelisttomultivector(context, length, MANIFOLD_GRADE_LINE, f, length);
            
            if (mv) fobj->param=(expression *) mv;
        }
        if (f) EVAL_FREE(f);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *constantforce_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=constantforce_integrand_mapfunction;
    fobj->force=constantforce_force_mapfunction;
    
    return NULL;
}

/* --- Central forces between pairs of particles --- */

/* Evaluates an expression given separation
 Input -
 interpretcontext *context    - a host context for error reporting. Evaluation occurs in a new sandboxed context
 expression       *exp        - an expression to evaluate
 expression       *coord      - a coordinate name.
 MFLD_DBL         r           - Separation value
 Returns -
 Evaluated expression.
 */
expression *centralpairwise_evaluateexpressionwithcoordinates(interpretcontext *context, expression *exp, expression *coord, MFLD_DBL r) {
    interpretcontext *localcntxt=newinterpretcontext(context, 1); // Create a new context
    expression *result=NULL;
    
    /* Validation */
    if (!localcntxt) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATION_FAILED);
    if (!eval_isname(coord)) {
        error_raise(context, ERROR_COORDINATESPW, ERROR_COORDINATESPW_MSG, ERROR_FATAL);
    }
    
    /* Substitute the ith coordinate into the appropriate name */
    expression_float val;
    val.type=EXPRESSION_FLOAT;
    val.value=r;
    freeexpression(opassign(localcntxt, coord, (expression *) &val));
    
    result=interpretexpression(localcntxt, exp);
    
    /* Destroy the context */
    freeinterpretcontext(localcntxt);
    
    return result;
}

/* Evaluates a quantity stored in the levelset dictionary with specified coordinate values
 Input -
 functional_object *fobj   - The functional object
 interpretcontext *context - The current context for error reporting
 char *quantity            - id of the quantity to evaluate
 MFLD_DBL r                - Current separation
 */
expression *centralpairwise_evaluatequantity(functional_object *fobj, interpretcontext *context, char *quantity, MFLD_DBL r) {
    expression *coordinates=NULL;
    expression *exp=NULL, *result=NULL;
    
    /* Obtain coordinates and expression from table */
    hashtable *ht=levelset_findhashtable(fobj, TRUE);
    if (ht) {
        coordinates= hashget(ht, LEVELSET_COORDINATES);
        exp=(expression *) hashget(ht, quantity);
    }
    
    if (exp) {
        result=centralpairwise_evaluateexpressionwithcoordinates(context, exp, coordinates, r);
    }
    
    return result;
}

expression *centralpairwise_setcoordinatesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    hashtable *ht;
    
    if (nargs==1 && eval_isname(args[0])) {
        ht=levelset_findhashtable(fobj, TRUE);
        
        if (ht) {
            hashinsert(ht, LEVELSET_COORDINATES, cloneexpression(args[0]));
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

void centralpairwise_integrand_mapfunction(uid id1, uid id2, void *content1, void *content2, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v1 = (manifold_vertex *) content1;
    manifold_vertex *v2 = (manifold_vertex *) content2;
    expression *result=NULL;
    int dim = eref->mobj->dimension;
    MFLD_DBL r=0,u;
    
    for (unsigned i=0; i<dim; i++) {
        u=(v2->x[i]-v1->x[i]);
        r+=u*u;
    }
    r=sqrt(r);
    
    result = centralpairwise_evaluatequantity(eref->fobj, eref->context, LEVELSET_EXPRESSION, r);
    
    if (eval_isreal(result)) {
        expression_float flt;
        flt.type=EXPRESSION_FLOAT;
        flt.value=0.5*eval_floatvalue(result);
        
        field_accumulatewithid(eref->target, eref->context, id1, (expression *) &flt);
        field_accumulatewithid(eref->target, eref->context, id2, (expression *) &flt);
    }
    //printf("%u %u %g %g\n", id1, id2, r, eval_floatvalue(result));
    if (result) freeexpression(result);
}

void centralpairwise_force_mapfunction(uid id1, uid id2, void *content1, void *content2, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v1 = (manifold_vertex *) content1;
    manifold_vertex *v2 = (manifold_vertex *) content2;
    expression *result=NULL;
    int dim = eref->mobj->dimension;
    MFLD_DBL r=0;
    MFLD_DBL dx[dim];
    expression *mv1=NULL, *mv2=NULL;
    
    /* Compute displacement vector and separation */
    for (unsigned i=0; i<dim; i++) {
        dx[i]=(v2->x[i]-v1->x[i]);
        r+=dx[i]*dx[i];
    }
    r=sqrt(r);
    
    result = centralpairwise_evaluatequantity(eref->fobj, eref->context, LEVELSET_GRADIENT, r);
    
    if (eval_isreal(result)) {
        double val=eval_floatvalue(result);
        for (unsigned i=0; i<dim; i++) dx[i]*=val/r;
        mv1=multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, dx, dim);
        for (unsigned i=0; i<dim; i++) dx[i]=-dx[i];
        mv2=multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, dx, dim);
        
        if (mv1) field_accumulatewithid(eref->target, eref->context, id1, mv1);
        if (mv2) field_accumulatewithid(eref->target, eref->context, id2, mv2);
    }
    //printf("%u %u %g %g\n", id1, id2, r, eval_floatvalue(result));
    if (result) freeexpression(result);
    if (mv1) freeexpression(mv1);
    if (mv2) freeexpression(mv2);
}

expression *centralpairwise_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=(idtablemapfunction *) centralpairwise_integrand_mapfunction;
    fobj->force=(idtablemapfunction *) centralpairwise_force_mapfunction;
    
    fobj->flags=(TUPLES_FLAG | fobj->flags);
    
    return NULL;
}

/* --- Gravity --- [Only works in 3D ] */

void gravity_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    uid v[3];
    MFLD_DBL z[3], tp[eref->mobj->dimension], ks0s1=0.0;
    unsigned int nel=0;
    
    if (eref->mobj->dimension!=3) {
        char err[ERROR_BUFFERSIZE];
        sprintf(err, ERROR_DIMENSION_MSG, eref->mobj->dimension);
        error_raise(eref->context, ERROR_DIMENSION, err, ERROR_FATAL);
    }
    
    for (unsigned int i=0; i<eref->mobj->dimension; i++) tp[i]=0;
    
    /* The area bivector */
    if (eref->fobj->param) {
        expression *area=NULL, *vol=NULL;
        multivectorobject *vmv=NULL;
        
        area = manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_AREA, id);
        
       // multivector_print((multivectorobject *) ((expression_objectreference *) area)->obj);
       // multivector_print((multivectorobject *) ((expression_objectreference *) eref->fobj->param)->obj);
        if (eval_isobjectref(area)) vol = opmul(eref->context, eref->fobj->param, area);
        //multivector_print((multivectorobject *) ((expression_objectreference *) vol)->obj);
        
        if (eval_isobjectref(vol)) {
            vmv = (multivectorobject *) ((expression_objectreference *) vol)->obj;
            if (vmv) multivector_gradetodoublelist(vmv, MANIFOLD_GRADE_VOLUME, tp);
        }
        
        ks0s1 = -tp[0]; // Negative sign because of geometric product
        
        if (area) freeexpression(area);
        if (vol) freeexpression(vol);
    } else {
        error_raise(eref->context, ERROR_DIRECTION, ERROR_DIRECTION_MSG, ERROR_FATAL);
    }
    
    /* Get the z coordinate of each vertex */
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, v, &nel);
    
    if (nel==3) {
        for (unsigned int i=0; i<nel; i++) {
            manifold_vertex *vert=idtable_get(eref->mobj->vertices, v[i]);
            if (vert) z[i]=vert->x[2]; // z coordinate
        }
        
        ks0s1=ks0s1*(z[0]*z[0] + z[1]*z[1] + z[2]*z[2] + z[0]*z[1] + z[0]*z[2] + z[1]*z[2])/24.0;
        
        field_insertwithid(eref->target, id, newexpressionfloat(ks0s1));
    }
}

expression *gravity_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=gravity_integrand_mapfunction;
    fobj->force=NULL;
    
    return NULL;
}

/* --- Harmonic point constraints --- */

void harmonic_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v = (manifold_vertex *) content;
    expression *x0 = NULL, *xx=NULL, *res=NULL;
    expression_objectreference *sub=NULL, *mul=NULL;
    
    if ((eref) && (eref->fobj)) x0=(expression *) eref->fobj->param;
    
    if (x0) {
        xx=multivector_doublelisttomultivector(eref->context, eref->mobj->dimension, MANIFOLD_GRADE_LINE, v->x, eref->mobj->dimension);
        
        sub=(expression_objectreference *) opsub(eref->context, xx, x0);
        if (sub) {
            mul=(expression_objectreference *) opmul(eref->context, (expression *) sub, (expression *) sub);
            
            if (mul) {
                res=multivector_gradenorm((multivectorobject *) mul->obj, MANIFOLD_GRADE_POINT, 0.5);
                if (eval_isreal(res)) field_insertwithid(eref->target, id, res);
            }
        }
    }
    
    if (xx) freeexpression(xx);
    if (sub) freeexpression((expression *) sub);
    if (mul) freeexpression((expression *) mul);
}

void harmonic_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_vertex *v = (manifold_vertex *) content;
    expression *x0 = NULL, *xx=NULL;
    expression_objectreference *sub=NULL;
    
    if ((eref) && (eref->fobj)) x0=(expression *) eref->fobj->param;
    
    if (x0) {
        xx=multivector_doublelisttomultivector(eref->context, eref->mobj->dimension, MANIFOLD_GRADE_LINE, v->x, eref->mobj->dimension);
        
        sub=(expression_objectreference *) opsub(eref->context, xx, x0);
        if (sub) {
            field_accumulatewithid(eref->target, eref->context, id, (expression *) sub);
        }
    }
    
    if (xx) freeexpression(xx);
    if (sub) freeexpression((expression *) sub);
}

expression *harmonic_getorigini(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression *ret=NULL;
    
    if (obj) {
        if(fobj->param) {
            ret=cloneexpression(fobj->param);
        } else {
            error_raise(context, ERROR_NOFORCE, ERROR_NOFORCE_MSG, ERROR_FATAL);
        }
    }
    
    return ret;
}

expression *harmonic_setorigini(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression_list *list=NULL;
    expression_objectreference *mv;
    unsigned int length=0;
    
    if ((nargs==1)&&(eval_islist(args[0]))) {
        list = (expression_list *) args[0];
        length=eval_listlength(list);
        
        double *f = EVAL_MALLOC(length*sizeof(double));
        if (eval_listtodoublearray(list, f, length)) {
            mv=(expression_objectreference *) multivector_doublelisttomultivector(context, length, MANIFOLD_GRADE_LINE, f, length);
            
            if (mv) fobj->param=(expression *) mv;
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *harmonic_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=harmonic_integrand_mapfunction;
    fobj->force=harmonic_force_mapfunction;
    
    return NULL;
}

/* --- Line tension --- */

/* The integrand map function for line tension */
void linetension_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0;
    
    if ((eref)&&(eref->mobj)) {
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        field_insertwithid(eref->target, id, newexpressionfloat(length));
    }
}

/* The integrand map function for line tension */
void linetension_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0;
    
    if ((eref)&&(eref->mobj)) {
        unsigned int dim = eref->mobj->dimension;
        MFLD_DBL xx[dim];
        expression *mv=NULL;
        
        /* Compute the force, which for vertex 0 is - (x1 - x0)/|x1-x0| */
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        manifold_vertexdisplacement(eref->mobj, lower->el[0], lower->el[1], xx); /* x1 - x0 */
        for (unsigned int i=0; i<dim; i++) xx[i]=-xx[i]/length;
        
        /* Convert to multivector */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], mv); // Add to field
        freeexpression(mv);
        
        /* Reverse the sign for the second vertex */
        for (unsigned int i=0; i<dim; i++) xx[i]=-xx[i];
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], mv); // Add to field
        freeexpression(mv);
    }
}

expression *linetension_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->param=NULL;
    fobj->integrand=linetension_integrand_mapfunction;
    fobj->force=linetension_force_mapfunction;
    
    return NULL;
}

/* --- Line tension cylindrical coordinates \int 2 Pi r dr --- */

/* The integrand map function for line tension */
void linetensioncyl_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0;
    MFLD_DBL x0[eref->mobj->dimension], x1[eref->mobj->dimension];
    
    if ((eref)&&(eref->mobj)) {
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        
        manifold_getvertexposition(eref->mobj, eref->context, lower->el[0], x0);
        manifold_getvertexposition(eref->mobj, eref->context, lower->el[1], x1);
        
        field_insertwithid(eref->target, id, newexpressionfloat(PI*(x1[0]+x0[0])*length));
    }
}


expression *linetensioncyl_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->param=NULL;
    fobj->integrand=linetensioncyl_integrand_mapfunction;
    fobj->force=NULL;
    
    return NULL;
}


/* --- Hookean elasticity, 1/2 (l-l0)^2/l0 --- */

MFLD_DBL hooke_resolvepreferredvalue(functionalmapref *fref, uid id) {
    MFLD_DBL length0=0.0;
    if (fref && fref->fobj && fref->fobj->param) {
        expression *param = fref->fobj->param;
        
        if (eval_isnumerical(param)) {
            length0=eval_floatvalue(param);
        } else if (eval_isobjectref(param)) {
            object *field=(object *) eval_objectfromref(param);
            
            if (field) {
                if (class_objectrespondstoselector( field, EVAL_INDEX)) {
                    expression *iid=newexpressioninteger(id);
                    expression *result=NULL;
                    
                    result=class_callselectorwithargslist(fref->context, field, EVAL_INDEX, 1, &iid);
                    
                    if (eval_isnumerical(result)) length0=eval_floatvalue(result);
                    
                    if (result) freeexpression(result);
                    if (iid) freeexpression(iid);
                }
            }
            
        }
    }
    
    return length0;
}

/* The integrand map function for hooke energy */
void hooke_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, length0=0.0;
    
    if ((eref)&&(eref->mobj)) {
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        length0=hooke_resolvepreferredvalue(eref, id);
        field_insertwithid(eref->target, id, newexpressionfloat( 0.5*(length-length0)*(length-length0)/length0) );
    }
}

/* The force map function for hookean spring */
void hooke_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0,length0=0.0,factor=1.0;
    
    if ((eref)&&(eref->mobj)) {
        unsigned int dim = eref->mobj->dimension;
        MFLD_DBL xx[dim];
        expression *mv=NULL;
        
        /* Compute the force */
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        length0=hooke_resolvepreferredvalue(eref, id);
        if (length0>0.0) factor=(length-length0)/length0;
        
        manifold_vertexdisplacement(eref->mobj, lower->el[0], lower->el[1], xx); /* x1 - x0 */
        for (unsigned int i=0; i<dim; i++) xx[i]=factor*xx[i]/length;
        
        /* Convert to multivector */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], mv); // Add to field
        freeexpression(mv);
        
        /* Reverse the sign for the second vertex */
        for (unsigned int i=0; i<dim; i++) xx[i]=-xx[i];
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], mv); // Add to field
        freeexpression(mv);
    }
}

expression *hooke_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->param=NULL;
    fobj->integrand=hooke_integrand_mapfunction;
    fobj->force=hooke_force_mapfunction;
    
    return NULL;
}

/* --- Linear elasticity --- */

int linearelastic_calculategrammatrix(manifold_object *m, interpretcontext *context, uid id, MFLD_DBL sf[4]) {
    expression *mv[5]={NULL, NULL, NULL, NULL, NULL}, *gp=NULL;
    manifold_gradelowerentry *gl=idtable_get(m->gradelower[MANIFOLD_GRADE_AREA-1], id);
    int success=FALSE;
    
    if (gl) {
        /*
        for (unsigned int i=0; i<2; i++) {
            mv[i]=manifold_elementtomultivector(m, context, MANIFOLD_GRADE_LINE, gl->el[i]);
            multivector_print((multivectorobject *) eval_objectfromref(mv[i]));
        }*/
        
        uid vid[3];
        unsigned int nel;
        
        /* Get multivectors corresponding to s0 and s1 respectively */
        mv[0]=manifold_elementtomultivector(m, context, MANIFOLD_GRADE_LINE, gl->el[0]);
        
        /* Find s1 as the difference between vertices 1 and 2 */
        manifold_gradelower(m, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vid, &nel);
        mv[2]=manifold_elementtomultivector(m, context, MANIFOLD_GRADE_POINT, vid[1]);
        mv[3]=manifold_elementtomultivector(m, context, MANIFOLD_GRADE_POINT, vid[2]);
        mv[1]=opsub(context, (expression *) mv[3], (expression *) mv[2]);
        if (mv[2]) freeexpression(mv[2]);
        if (mv[3]) freeexpression(mv[3]);
        //multivector_print((multivectorobject *) eval_objectfromref(mv[0]));
        //multivector_print((multivectorobject *) eval_objectfromref(mv[1]));
        
        if (mv[0] && mv[1]) {
            gp = opmul(context, mv[0], mv[1]);
            
            //multivector_print((multivectorobject *) eval_objectfromref(gp));

            /* s0.s0 */
            mv[2]=multivector_gradenormsq((multivectorobject *) eval_objectfromref((expression_objectreference *) mv[0]), MANIFOLD_GRADE_LINE);
            if (eval_isreal(mv[2])) sf[0]=eval_floatvalue(mv[2]);
            /* s1.s1 */
            mv[3]=multivector_gradenormsq((multivectorobject *) eval_objectfromref((expression_objectreference *) mv[1]), MANIFOLD_GRADE_LINE);
            if (eval_isreal(mv[3])) sf[1]=eval_floatvalue(mv[3]);
            /* s0.s1 */
            multivector_gradetodoublelist((multivectorobject *) eval_objectfromref((expression_objectreference *) gp), MANIFOLD_GRADE_POINT, &sf[2]);
            /* |s0 s1|_2 (s0 x s1 in 3d) */
            mv[4]=multivector_gradenorm((multivectorobject *) eval_objectfromref((expression_objectreference *) gp), MANIFOLD_GRADE_AREA, 1.0);
            if (eval_isreal(mv[4])) sf[3]=eval_floatvalue(mv[4]);
            success=TRUE;
        }
            
        for (unsigned int i=0; i<5; i++) {
            if (mv[i]) freeexpression(mv[i]);
        }
        if (gp) freeexpression(gp);
    }
    
    return success;
}

void linearelastic_integrand_mapfunction(uid id, void *content, void *ref) {
    MFLD_DBL sf[4],sfref[4],trCC,trCsq;
    manifold_object *rm=NULL;
    functionalmapref *eref = (functionalmapref *) ref;
    MFLD_DBL nu=0.5, dim=2;
    
    if (eval_isobjectref(eref->fobj->param)) {
        rm = (manifold_object *) eval_objectfromref((expression_objectreference *) eref->fobj->param);
    }
    
    /* Calculate elements of the gram matrix in both source and reference space */
    if (linearelastic_calculategrammatrix(eref->mobj, eref->context, id, sf) &&
        (rm && linearelastic_calculategrammatrix(rm, eref->context, id, sfref))) {
        
        MFLD_DBL r0r0=sfref[0], r1r1=sfref[1], r0r1=sfref[2], r0cr1=sfref[3], area=0.5*sfref[3]; // Reference elements
        MFLD_DBL s0s0=sf[0], s1s1=sf[1], s0s1=sf[2]; // Our elements
        
        /* Tr[C]^2 */
        trCsq=(r1r1*s0s0+r0r0*s1s1-2*r0cr1*r0cr1-2*r0r1*s0s1)/(r0cr1*r0cr1);
        trCsq*=0.25*trCsq;
        
        /* Tr[C.C] */
        MFLD_DBL u,v,w;
        u=r0cr1*r0cr1-r1r1*s0s0+r0r1*s0s1;
        v=r0cr1*r0cr1+r0r1*s0s1-r0r0*s1s1;
        w=r0cr1*r0cr1;
        trCC=0.25*(u*u+v*v+2*(r0r1*s0s0-r0r0*s0s1)*(r0r1*s1s1-r1r1*s0s1))/(w*w);
        
        field_insertwithid(eref->target, id, newexpressionfloat( area*(trCC + nu*trCsq)/(2*(1+nu)*(1+nu-dim*nu))));
    }
}

void linearelastic_force_mapfunction(uid id, void *content, void *ref) {
    MFLD_DBL sf[4],sfref[4];
    manifold_object *rm=NULL;
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL nu=0.5, dim=2;
    
    //printf("Face %u\n",id);
    
    if (eval_isobjectref(eref->fobj->param)) {
        rm = (manifold_object *) eval_objectfromref((expression_objectreference *) eref->fobj->param);
    }
    
    /* Calculate elements of the gram matrix in both source and reference space */
    if (linearelastic_calculategrammatrix(eref->mobj, eref->context, id, sf) &&
        (rm && linearelastic_calculategrammatrix(rm, eref->context, id, sfref))) {
        
        //MFLD_DBL energydensity;
        MFLD_DBL r0r0=sfref[0], r1r1=sfref[1], r0r1=sfref[2], r0cr1=sfref[3], area=0.5*sfref[3]; // Reference elements
        MFLD_DBL s0s0=sf[0], s1s1=sf[1], s0s1=sf[2]; // Our elements
        MFLD_DBL dtrCsqDs0s0,dtrCsqDs0s1,dtrCsqDs1s1; // Derivatives of trCsq
        MFLD_DBL dtrCCDs0s0,dtrCCDs0s1,dtrCCDs1s1; // Derivatives of trCC
        MFLD_DBL dfds0s0,dfds0s1,dfds1s1; // Derivatives of trCC
        MFLD_DBL u,v,w; // Temporary variables
        
        
        //printf("Dot products: ref: {%g %g %g} target: {%g %g %g}\n", r0r0, r0r1, r1r1, s0s0, s0s1, s1s1);
        
        /* Tr[C]^2 */
        /*trCsq=(r1r1*s0s0+r0r0*s1s1-2*r0cr1*r0cr1-2*r0r1*s0s1)/(r0cr1*r0cr1);
        trCsq*=0.25*trCsq;*/
        
        /* Tr[C.C] */
        /*u=r0cr1*r0cr1-r1r1*s0s0+r0r1*s0s1;
        v=r0cr1*r0cr1+r0r1*s0s1-r0r0*s1s1;
        w=r0cr1*r0cr1;
         trCC=0.25*(u*u+v*v+2*(r0r1*s0s0-r0r0*s0s1)*(r0r1*s1s1-r1r1*s0s1))/(w*w);*/
        
        /* energy density */
        //energydensity=(trCC + nu*trCsq)/(2*(1+nu)*(1+nu-dim*nu));
        
        /* Compute derivatives of Tr[C]^2 */
        u=(r1r1*s0s0+r0r0*s1s1-2*r0cr1*r0cr1-2*r0r1*s0s1);
        v=(r0cr1*r0cr1);
        v=1/(v*v); /* Now 1/r0cr1^4 */
        dtrCsqDs0s0=0.5*r1r1*u*v;
        dtrCsqDs1s1=0.5*r0r0*u*v;
        dtrCsqDs0s1=-r0r1*u*v;
        
        /* Compute derivatives of Tr[C.C] */
        dtrCCDs0s0=0.5*(r1r1*(r1r1*s0s0-r0cr1*r0cr1-2*r0r1*s0s1)+r0r1*r0r1*s1s1)*v;
        dtrCCDs1s1=0.5*(r0r0*r0r0*s1s1 + r0r1*r0r1*s0s0 - r0cr1*r0cr1*r0r0 - 2*r0r0*r0r1*s0s1)*v;
        dtrCCDs0s1=(r0cr1*r0cr1*r0r1 + r0r1*r0r1*s0s1 + r0r0*r1r1*s0s1 - r0r1*(r1r1*s0s0+r0r0*s1s1))*v;
        w=1.0/(2*(1+nu)*(1+nu-dim*nu));
        
        dfds0s0=w*(dtrCCDs0s0+nu*dtrCsqDs0s0);
        dfds0s1=w*(dtrCCDs0s1+nu*dtrCsqDs0s1);
        dfds1s1=w*(dtrCCDs1s1+nu*dtrCsqDs1s1);
        
        uid vid[3];
        unsigned int nel;
        
        /* Get multivectors corresponding to s0 and s1 respectively */
        expression_objectreference *s0=NULL, *x1=NULL, *x2=NULL, *s1=NULL;
        s0=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, lower->el[0]);
        
        /* Find s1 as the difference between vertices 1 and 2 */
        manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vid, &nel);
        x1=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_POINT, vid[1]);
        x2=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_POINT, vid[2]);
        s1=(expression_objectreference *) opsub(eref->context, (expression *) x2, (expression *) x1);
        
        //multivector_print((multivectorobject *) eval_objectfromref(manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_POINT, vid[0])));
        //multivector_print((multivectorobject *) eval_objectfromref(x1));
        //multivector_print((multivectorobject *) eval_objectfromref(x2));
        
        if (x1) freeexpression((expression *) x1);
        if (x2) freeexpression((expression *) x2);
        
        double wts[2];
        expression_objectreference *mv[2];
        mv[0]=s0; mv[1]=s1;
        /* Calculate multivectors necessary for the area derivatives */
        //expression_objectreference *tmp=NULL, *s0xs1=NULL, *s0s1s0=NULL, *s0s1s1=NULL;
        /* s0 wedge s1 projected onto grade 2 */
        //tmp=(expression_objectreference *) opmul(eref->context, (expression *) s0, (expression *) s1);
        //s0xs1=(expression_objectreference *) multivector_projectontograde((multivectorobject *) eval_objectfromref(tmp), MANIFOLD_GRADE_AREA);
        /* s0^s1 s0 */
        //s0s1s0=(expression_objectreference *) opmul(eref->context, (expression *) s0xs1, (expression *) s0);
        /* s0^s1 s1 */
        //s0s1s1=(expression_objectreference *) opmul(eref->context, (expression *) s0xs1, (expression *) s1);
        
        expression_objectreference *out=NULL;
        /* Force on vertex 0 */
        //mv[2]=s0s1s1;
        wts[0]=2*dfds0s0*area; wts[1]=dfds0s1*area; //wts[2]=0.5*energydensity/s0cs1;
        out=(expression_objectreference *) multivector_weightedtotal(eref->context, mv, wts, 2);
        field_accumulatewithid(eref->target, eref->context, vid[0], (expression *) out);
        //multivector_print((multivectorobject *) eval_objectfromref(out));
        if (out) freeexpression((expression *) out);
        
        /* Force on vertex 1 */
        wts[0]=(-2*dfds0s0 + dfds0s1)*area; wts[1]=(2*dfds1s1 - dfds0s1)*area;
        //wts[2]=-wts[2];
        //wts[3]=wts[2];
        //mv[2]=s0s1s0; mv[3]=s0s1s1;
        out=(expression_objectreference *) multivector_weightedtotal(eref->context, mv, wts, 2);
        field_accumulatewithid(eref->target, eref->context, vid[1], (expression *) out);
        //multivector_print((multivectorobject *) eval_objectfromref(out));
        if (out) freeexpression((expression *) out);
        
        /* Force on vertex 2 */
        wts[0]=-dfds0s1*area; wts[1]=-2*dfds1s1*area;
        //wts[2]=0.5*energydensity/s0cs1;
        //mv[2]=s0s1s0;
        out=(expression_objectreference *) multivector_weightedtotal(eref->context, mv, wts, 2);
        field_accumulatewithid(eref->target, eref->context, vid[2], (expression *) out);
        //multivector_print((multivectorobject *) eval_objectfromref(out));
        if (out) freeexpression((expression *) out);
        
        if (s0) freeexpression((expression *) s0);
        if (s1) freeexpression((expression *) s1);
        /*if (tmp) freeexpression((expression *) tmp);
        if (s0xs1) freeexpression((expression *) s0xs1);
        if (s0s1s0) freeexpression((expression *) s0s1s0);
        if (s0s1s1) freeexpression((expression *) s0s1s1);*/
    }
    
}

expression *linearelastic_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=linearelastic_integrand_mapfunction;
    fobj->force=linearelastic_force_mapfunction;
    
    return NULL;
}

/* --- Line curvature squared --- */

int linecurvaturesq_identifyadjacent(functionalmapref *ref, uid id, uid *edge, uid *vert) {
    uid e[MANIFOLD_GRADEFIND_MAXENTRIES], v[2];
    unsigned int nel, j;
    
    /* Identify adjacent vertices */
    manifold_graderaise(ref->mobj, MANIFOLD_GRADE_POINT, id, MANIFOLD_GRADE_LINE, MANIFOLD_GRADEFIND_MAXENTRIES, e, &nel);
    
    /* If there are more than 2 connections, use the given selection to restrict them. */
    if (nel>2 && ref->sobj) {
        j=0;
        for (unsigned int i=0; i<nel; i++) {
            if (selection_isselected(ref->sobj, ref->mobj, MANIFOLD_GRADE_LINE, e[i])) {
                if (j<2) {
                    edge[j]=e[i]; j++;
                } else return FALSE;
            }
        }
    } else if (nel==2) {
        edge[0]=e[0]; edge[1]=e[1];
    } else return FALSE;
    
    for (unsigned int i=0; i<2; i++) {
        manifold_gradelower(ref->mobj, MANIFOLD_GRADE_LINE, edge[i], MANIFOLD_GRADE_POINT, v, &nel);
        if (v[0]!=id) vert[i]=v[0]; else vert[i]=v[1];
    }
    
    return TRUE;
}

/* The integrand map function for line curvature */
void linecurvaturesq_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    uid v[2], e[2];
    MFLD_DBL s0[eref->mobj->dimension], s1[eref->mobj->dimension], s0s0=0.0, s1s1=0.0, s0s1=0.0, u, len;
    
    if (linecurvaturesq_identifyadjacent(eref, id, e, v)) {
        manifold_vertexdisplacement(eref->mobj, v[0], id, s0);
        manifold_vertexdisplacement(eref->mobj, id, v[1], s1);
        for (unsigned int i=0; i<eref->mobj->dimension; i++) {
            s0s0+=s0[i]*s0[i];
            s0s1+=s0[i]*s1[i];
            s1s1+=s1[i]*s1[i];
        }
        s0s0=sqrt(s0s0); s1s1=sqrt(s1s1);
        
        if (s0s0<sqrt(MACHINE_EPSILON) || s1s1<sqrt(MACHINE_EPSILON)) return;
        
        u=s0s1/s0s0/s1s1;
        len=0.5*(s0s0+s1s1);
        
        if (u<1) {
            u=acos(u);
        } else {
            u=0;
        }
        field_insertwithid(eref->target, id, newexpressionfloat( u*u/len ));
    }
}

void linecurvaturesq_vecaddscale(MFLD_DBL *v1, MFLD_DBL *v2, MFLD_DBL s1, MFLD_DBL s2, unsigned int dim, MFLD_DBL *out) {
    for (unsigned int i=0; i<dim; i++) {
        out[i] = v1[i]*s1+v2[i]*s2;
    }
}

void linecurvaturesq_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    unsigned int dim = eref->mobj->dimension;
    uid v[2], e[2];
    MFLD_DBL s0[dim], s1[dim], gradu[dim], gradu2[dim], frc[dim];
    MFLD_DBL s0s0=0.0, s1s1=0.0, s0s1=0.0, u, uu, uusq, vv, len;
    MFLD_DBL sqrts0s0, sqrts1s1, invsqrtSS, invsqrt1muu;
    MFLD_DBL duufac;
    expression *mv=NULL;
    
    if (linecurvaturesq_identifyadjacent(eref, id, e, v)) {
        manifold_vertexdisplacement(eref->mobj, v[0], id, s0);
        manifold_vertexdisplacement(eref->mobj, id, v[1], s1);
        for (unsigned int i=0; i<dim; i++) {
            s0s0+=s0[i]*s0[i];
            s0s1+=s0[i]*s1[i];
            s1s1+=s1[i]*s1[i];
        }
        
        sqrts0s0=sqrt(s0s0); sqrts1s1=sqrt(s1s1);
        invsqrtSS=1.0/sqrts0s0/sqrts1s1;
        
        u=s0s1*invsqrtSS;
        
        /* Detect the situation when two adjacent edges are parallel */
        if (fabs(1-u*u)<MACHINE_EPSILON || s0s0<MACHINE_EPSILON || s1s1<MACHINE_EPSILON ) {
            return;
        }
        
        invsqrt1muu=1.0/sqrt(1-u*u);
        len=0.5*(sqrts0s0+sqrts1s1);
        
        if (u<1) uu=acos(u);
        else uu=0;
        
        uusq=uu*uu;
        vv=1.0/len;
        
        /* the Functional is uusq*len; obtain derivatives by the chain rule */
        duufac =(-2*uu*invsqrt1muu);

        /* Force on first vertex: uusq*dvv + vv*gradu */
        linecurvaturesq_vecaddscale(s0, s1, s0s1*invsqrtSS/(s0s0), -invsqrtSS, dim, gradu);
        linecurvaturesq_vecaddscale(s0, gradu, -uusq * 0.5*vv*vv/sqrts0s0, -vv * duufac, dim, frc);
        
        /* Store */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, frc, dim);
        field_accumulatewithid(eref->target, eref->context, v[0], mv); // Add to field
        freeexpression(mv);
        
        /* Force on last vertex */
        linecurvaturesq_vecaddscale(s0, s1, invsqrtSS, -s0s1*invsqrtSS/(s1s1), dim, gradu2);
        linecurvaturesq_vecaddscale(s1, gradu2, -uusq * -0.5*vv*vv/sqrts1s1, -vv * duufac, dim, frc);
        
        /* Store */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, frc, dim);
        field_accumulatewithid(eref->target, eref->context, v[1], mv); // Add to field
        freeexpression(mv);
        
        /* Force on middle vertex */
        linecurvaturesq_vecaddscale(gradu, gradu2, -1.0, -1.0, dim, gradu);
        linecurvaturesq_vecaddscale(s0, s1, -0.5*vv*vv/sqrts0s0, 0.5*vv*vv/sqrts1s1, dim, frc); /* dvv */
        linecurvaturesq_vecaddscale(frc, gradu, -uusq, -vv * duufac, dim, frc);
        
        /* Store */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, frc, dim);
        field_accumulatewithid(eref->target, eref->context, id, mv); // Add to field
        freeexpression(mv);
    }
}

/* Identify dependencies */
void linecurvaturesq_dependencies_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    uid v[2], e[2];
    if (linecurvaturesq_identifyadjacent(eref, id, e, v)) {
        field_insertwithid(eref->dependencies, id, eval_listfromunsignedintegerarray(v, 2));
    }
}

expression *linecurvaturesq_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=linecurvaturesq_integrand_mapfunction;
    fobj->force=linecurvaturesq_force_mapfunction;
    fobj->dependencies=linecurvaturesq_dependencies_mapfunction;
    
    return NULL;
}

/* --- Line torsion squared --- */

int linetorsionsq_identifyadjacent(functionalmapref *ref, uid id, manifold_gradelowerentry *lower, uid *edges, uid *vert) {
    uid e[MANIFOLD_GRADEFIND_MAXENTRIES], vv[2];
    unsigned int nel=0, j=0, nv=0;
    
    /* For each vertex in the line element, identify adjacent edges */
    for (unsigned int i=0; i<2; i++) {
        uid v = lower->el[i];
        
        manifold_graderaise(ref->mobj, MANIFOLD_GRADE_POINT, v, MANIFOLD_GRADE_LINE, MANIFOLD_GRADEFIND_MAXENTRIES, e, &nel);
        if (nel>2 && ref->sobj) {
            j=0;
            for (unsigned int k=0; k<nel; k++) {
                if (e[k]!=id && selection_isselected(ref->sobj, ref->mobj, MANIFOLD_GRADE_LINE, e[k])) {
                    if (j<1) {
                        edges[i]=e[k]; j++;
                    } else return FALSE;
                }
            }
        } else if (nel==2) {
            edges[i]=(e[0]!=id ? e[0] : e[1]);
        } else return FALSE;
        
        if (vert) {
            manifold_gradelower(ref->mobj, MANIFOLD_GRADE_LINE, edges[i], MANIFOLD_GRADE_POINT, vv, &nv);
            if (nv==2) vert[i]=((vv[0]!=lower->el[0] && vv[0]!=lower->el[1]) ? vv[0] : vv[1]);
        }
    }
    
    return TRUE;
}

void linetorsionsq_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower=(manifold_gradelowerentry *) content;
    uid v[2], e[2];
    MFLD_DBL a[3], b[3], c[3], ab[3], bc[3], len, abc, normab, normbc, u;
    
    if (eref->mobj->dimension!=3) {
        char s[strlen(ERROR_DIMENSION_MSG)+1];
        sprintf(s, ERROR_DIMENSION_MSG, 3);
        error_raise(eref->context, ERROR_DIMENSION, s, ERROR_FATAL);
    }
    
    if (linetorsionsq_identifyadjacent(eref, id, lower, e, v)) {
        manifold_vertexdisplacement(eref->mobj, v[0], lower->el[0], a);
        manifold_vertexdisplacement(eref->mobj, lower->el[0], lower->el[1], b);
        manifold_vertexdisplacement(eref->mobj, lower->el[1], v[1], c);
        
        manifold_crossproduct3d(a, b, ab);
        manifold_crossproduct3d(b, c, bc);
        
        len=manifold_norm3d(b);
        abc=manifold_dotproduct3d(a, bc);
        normab=manifold_norm3d(ab);
        normbc=manifold_norm3d(bc);
        if (normab>MACHINE_EPSILON && normbc>MACHINE_EPSILON && len>MACHINE_EPSILON) {
            u=abc*len/normab/normbc;
            if (u>1.0) u=1.0;
            u=asin(u);
            u=u*u/len;
            field_insertwithid(eref->target, id, newexpressionfloat( u ));
        } else {
            error_raise(eref->context, ERROR_ZEROELEMENT, ERROR_ZEROELEMENT_MSG, ERROR_FATAL);
        }
    }
}

/* Identify dependencies */
void linetorsionsq_dependencies_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower=(manifold_gradelowerentry *) content;
    uid v[2], e[2];
    if (linetorsionsq_identifyadjacent(eref, id, lower, e, v)) {
        field_insertwithid(eref->dependencies, id, eval_listfromunsignedintegerarray(v, 2));
    }
}

expression *linetorsionsq_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=linetorsionsq_integrand_mapfunction;
    fobj->force=NULL;
    fobj->dependencies=linetorsionsq_dependencies_mapfunction;
    
    return NULL;
}


/* --- Enclosed area --- */

/* The integrand map function for line area */
void enclosedarea_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    expression_objectreference *v[2]={NULL, NULL};
    expression *out=NULL, *da=NULL;
    multivectorobject *mobj=NULL;
    
    
    if ((eref)&&(eref->mobj)) {
        for (unsigned int i=0; i<2; i++) {
            v[i]=(expression_objectreference *) manifold_vertextomultivector(eref->mobj, eref->context, lower->el[i]);
            if (!v[i]) return;
        }
        
        /* v[0] * v[1] using geometric product */
        /* TODO Should us opwedge */
        out = opmul(eref->context, (expression *) v[0], (expression *) v[1]);
        
        if (eval_isobjectref(out)) {
            mobj=(multivectorobject *) ((expression_objectreference *) out)->obj;
            
            if (mobj) da=multivector_gradenorm(mobj, MANIFOLD_GRADE_AREA, 0.5);
        }
        
        if (da) field_insertwithid(eref->target, id, da);
        
        /* Now free everything */
        if (out) freeexpression(out);
        for (unsigned int i=0; i<2; i++) {
            if (v[i]) freeexpression((expression *) v[i]);
        }
    }
    
}

/* The force map function for line area */
void enclosedarea_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    expression_objectreference *v[2]={NULL, NULL};
    expression *x01=NULL, *da=NULL, *zero=NULL;
    multivectorobject *mobj=NULL;
    expression *x101=NULL, *x010=NULL;
    MFLD_DBL x01norm=0.0;
    
    if ((eref)&&(eref->mobj)) {
        for (unsigned int i=0; i<2; i++) {
            v[i]=(expression_objectreference *) manifold_vertextomultivector(eref->mobj, eref->context, lower->el[i]);
            if (!v[i]) return;
        }
        
        /* v[0] * v[1] using geometric product */
        /* TODO: Should use wedge */
        x01 = opmul(eref->context, (expression *) v[0], (expression *) v[1]);
        //
        
        if (eval_isobjectref(x01)) {
            zero=newexpressionfloat(0.0);
            
            mobj=(multivectorobject *) ((expression_objectreference *) x01)->obj;
            
            if (mobj) {
                /* Clear out the scalar component */
                multivector_setgradetoexpression(mobj, eref->context, MANIFOLD_GRADE_POINT, zero);
                
                /* Calculate the area */
                if (mobj) da=multivector_gradenorm(mobj, MANIFOLD_GRADE_AREA, 1.0);
                if (eval_isnumerical(da)) x01norm = eval_floatvalue(da);
                
                if (x01norm>MACHINE_EPSILON) {
                    /* f0 = x1 x0 x1 */
                    x101 = opmul(eref->context, (expression *) v[1], x01);
                    if (x101) {
                        multivectorobject *m=(multivectorobject *) ((expression_objectreference *) x101)->obj;
                        multivector_scale(m, eref->context, 0.5/x01norm);
                        field_accumulatewithid(eref->target, eref->context, lower->el[0], x101);
                        freeexpression(x101);
                    }
                    
                    /* f1 = x0 x1 x0 */
                    x010 = opmul(eref->context, x01, (expression *) v[0]);
                    if (x010) {
                        multivectorobject *m=(multivectorobject *) ((expression_objectreference *) x010)->obj;
                        multivector_scale(m, eref->context, 0.5/x01norm);
                        field_accumulatewithid(eref->target, eref->context, lower->el[1], x010);
                        freeexpression(x010);
                    }
                }
            }
        }
        
        /* Now free everything */
        if (x01) freeexpression(x01);
        if (da) freeexpression(da);
        if (zero) freeexpression(zero);
        for (unsigned int i=0; i<2; i++) {
            if (v[i]) freeexpression((expression *) v[i]);
        }
        
    }
}

expression *enclosedarea_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=enclosedarea_integrand_mapfunction;
    fobj->force=enclosedarea_force_mapfunction;
    
    return NULL;
}

/* --- Equilength --- */

void equilength_edgelength(manifold_object *mobj, uid vert, uid edge, MFLD_DBL *length, MFLD_DBL *uvect) {
    manifold_gradelowerentry *gl=NULL;
    
    if ((mobj)&&(mobj->gradelower)) {
        gl = (manifold_gradelowerentry *) idtable_get(mobj->gradelower[0], edge);
        *length = manifold_vertexseparation(mobj, gl->el[0], gl->el[1]);
        if (uvect) {
            manifold_vertexdisplacement(mobj, gl->el[0], gl->el[1], uvect);
            if (*length>MACHINE_EPSILON) for (unsigned int i=0; i<mobj->dimension; i++) uvect[i]/=*length;
            /* Reverse direction if the first vertex is not the vertex of interest */
            if (gl->el[0]!=vert) for (unsigned int i=0; i<mobj->dimension; i++) uvect[i]=-uvect[i];
        }
    }
}

void equilength_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_graderaiseentry *gr=NULL;
    unsigned int n=0;
    MFLD_DBL mean=0.0, total=0.0;
    
    if (eref->mobj->graderaise) {
        /* Find line elements connected to the current vertex */
        gr = (manifold_graderaiseentry *) idtable_get(eref->mobj->graderaise[MANIFOLD_GRADE_POINT], id);
        
        if ((gr)&&(gr->list)) {
            unsigned int nconn = linkedlist_length(gr->list);
            uid eid;
            MFLD_DBL length[nconn];
            
            /* Find the id and length of all connected edges */
            n=0;
            for (linkedlistentry *e=gr->list->first; e!=NULL; e=e->next) {
                eid = *((uid *) e->data);
                equilength_edgelength(eref->mobj, id, eid, &length[n], NULL);
                mean+=length[n];
                n++;
            }
            mean /= ((MFLD_DBL) n);
            
            /* Now evaluate the functional at this vertex */
            for (unsigned int i=0; i<n; i++) total+=(1.0-length[i]/mean)*(1.0-length[i]/mean);
            
            field_insertwithid(eref->target, id, newexpressionfloat(total));
        }
    }
    
}

void equilength_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_graderaiseentry *gr=NULL;
    unsigned int n=0, dim=0;
    MFLD_DBL mean=0.0;
    expression *mv=NULL;
    
    if (eref->mobj->graderaise) {
        dim=eref->mobj->dimension;
        
        /* Find line elements connected to the current vertex */
        gr = (manifold_graderaiseentry *) idtable_get(eref->mobj->graderaise[MANIFOLD_GRADE_POINT], id);
        
        if ((gr)&&(gr->list)) {
            unsigned int nconn = linkedlist_length(gr->list);
            uid eid;
            MFLD_DBL length[nconn];
            MFLD_DBL uvect[nconn][dim];
            MFLD_DBL temp[dim];
            MFLD_DBL sumuvect[dim];
            /* Clear the vector */
            for (unsigned int i=0; i<dim; i++) {
                sumuvect[i]=0.0;
                temp[i]=0.0;
            }
            
            /* Find the id and length of all connected edges, and keep a running total of the unit vectors */
            n=0;
            for (linkedlistentry *e=gr->list->first; e!=NULL; e=e->next) {
                eid = *((uid *) e->data);
                equilength_edgelength(eref->mobj, id, eid, & length[n], temp);
                for (unsigned int i=0; i<dim; i++) {
                    uvect[n][i]=temp[i];
                    sumuvect[i]+=temp[i];
                }
                mean+=length[n];
                n++;
            }
            mean /= ((MFLD_DBL) n);
            
            /* SN = sum( u_i, {i,n} )/N */
            for (unsigned int i=0; i<dim; i++) sumuvect[i] /= ((MFLD_DBL) n);
            
            for (unsigned int i=0; i<dim; i++) temp[i]=0.0;
            
            /* The force due to each edge is equal to: - 2 * (1 - length/mean) * (N * mean + length * SN) / mean^2. */
            for (unsigned int i=0; i<n; i++) {
                for (unsigned int j=0; j<dim; j++) {
                    temp[j] += - 2.0 * (1.0 - length[i]/mean) * (uvect[i][j] * mean + length[i] * sumuvect[j])/(mean*mean);
                }
            }
            
            mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, temp, dim);
            field_accumulatewithid(eref->target, eref->context, id, mv); // Add to field
            freeexpression(mv);
        }
    }
}

expression *equilength_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=equilength_integrand_mapfunction;
    fobj->force=equilength_force_mapfunction;
    
    return NULL;
}

/* --- Surface tension --- */

expression *surfacetension_area(manifold_object *mobj, interpretcontext *context, uid id) {
    expression_objectreference *exp;
    expression *ret=NULL;
    exp = (expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, id);
    if (eval_isobjectref(exp)) ret=multivector_gradenorm((multivectorobject *) exp->obj, MANIFOLD_GRADE_AREA, 0.5);
    if (exp) freeexpression((expression *) exp);
    
    return ret;
}

void surfacetension_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp;
    
    exp = surfacetension_area(eref->mobj, eref->context, id);
    if (exp) field_insertwithid(eref->target, id, exp);
}

void surfacetension_force_withscale(uid id, void *content, void *ref, MFLD_DBL scale) {
    manifold_gradelowerentry *lower=(manifold_gradelowerentry *) content;
    functionalmapref *eref = (functionalmapref *) ref;
    expression_objectreference *exp=NULL, *s010=NULL, *s011=NULL, *sm011=NULL, *sm010=NULL;
    expression *norm=NULL, *s0vec=NULL, *s1vec=NULL, *s01bivec=NULL;
    unsigned int nel;
    uid s0[2], s1[2];
    double anorm=0.0;
    
    /* Lower the grade to obtain the vertex ids */
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_LINE, lower->el[0], MANIFOLD_GRADE_POINT, s0, &nel);
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_LINE, lower->el[1], MANIFOLD_GRADE_POINT, s1, &nel);
    
    /* The edges as multivectors */
    s0vec = manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, lower->el[0]);
    s1vec = manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, lower->el[1]);
    
    /* The element as a multivector, i.e. (s0.s1)+(s0^s1) */
    exp = (expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_AREA, id);
    
    if ((s0vec)&&(s1vec)&&(exp)) {
        /* Obtain |s0^s1| */
        norm = multivector_gradenorm((multivectorobject *) exp->obj, MANIFOLD_GRADE_AREA, 1.0);
        if (eval_isreal(norm)) anorm=eval_floatvalue(norm);
        if (fabs(anorm)<MACHINE_EPSILON) error_raise(eref->context, ERROR_ZEROELEMENT, ERROR_ZEROELEMENT_MSG, ERROR_FATAL);
        
        /* ... and <s0^s1>_2 */
        s01bivec = multivector_projectontograde((multivectorobject *) exp->obj, MANIFOLD_GRADE_AREA);
        
        /* Now compute the product of the s01 with the edges */
        s010=(expression_objectreference *) opmul(eref->context, s01bivec, s0vec);
        s011=(expression_objectreference *) opmul(eref->context, s01bivec, s1vec);
        
        /* And scale these by the 0.5/|s0^s1| */
        multivector_scale((multivectorobject *) s010->obj, eref->context, scale*0.5/anorm);
        multivector_scale((multivectorobject *) s011->obj, eref->context, scale*0.5/anorm);
        
        /* The force equals, for s0[0] := - <s0^s1>_2*s0 / |s0^s1| / 2, and similar expressions for the other vertices */
        field_accumulatewithid(eref->target, eref->context, s0[0], (expression *) s011);
        field_accumulatewithid(eref->target, eref->context, s1[1], (expression *) s010);
        
        /* TODO: Fix this once deepclone has been invented and field_accumulatewithid has been fixed!!
         --- [9/2/17] It's not clear what this is referring to... */
        sm010=(expression_objectreference *) multivector_clone((multivectorobject *) s010->obj);
        sm011=(expression_objectreference *) multivector_clone((multivectorobject *) s011->obj);
        
        multivector_scale((multivectorobject *) sm010->obj, eref->context, -1.0);
        multivector_scale((multivectorobject *) sm011->obj, eref->context, -1.0);
        field_accumulatewithid(eref->target, eref->context, s0[1], (expression *) sm011);
        field_accumulatewithid(eref->target, eref->context, s1[0], (expression *) sm010);
    }
    
    if (exp) freeexpression((expression *) exp);
    if (s0vec) freeexpression(s0vec);
    if (s1vec) freeexpression(s1vec);
    if (s01bivec) freeexpression(s01bivec);
    if (s010) freeexpression((expression *) s010);
    if (s011) freeexpression((expression *) s011);
    if (sm010) freeexpression((expression *) sm010);
    if (sm011) freeexpression((expression *) sm011);
    if (norm) freeexpression(norm);
}

void surfacetension_force_mapfunction(uid id, void *content, void *ref) {
    surfacetension_force_withscale(id, content, ref, 1.0);
}

expression *surfacetension_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=surfacetension_integrand_mapfunction;
    fobj->force=surfacetension_force_mapfunction;
    
    return NULL;
}


/*
 * Curvatures
 */

int curvature_identifyadjacent(functionalmapref *ref, uid id, uid *vert, int maxentries, unsigned int *nel) {
    uid e[MANIFOLD_GRADEFIND_MAXENTRIES], v[2];
    unsigned int n;
    
    /* Identify adjacent edges */
    manifold_graderaise(ref->mobj, MANIFOLD_GRADE_POINT, id, MANIFOLD_GRADE_LINE, MANIFOLD_GRADEFIND_MAXENTRIES, e, &n);
    
    for (unsigned int i=0; i<n && i<maxentries; i++) {
        unsigned int nn;
        manifold_gradelower(ref->mobj, MANIFOLD_GRADE_LINE, e[i], MANIFOLD_GRADE_POINT, v, &nn);
        if (v[0]!=id) vert[i]=v[0]; else vert[i]=v[1];
    }
    
    *nel = n;
    
    return TRUE;
}

/* Identify dependencies. */
void curvature_dependencies_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    uid v[MANIFOLD_GRADEFIND_MAXENTRIES];
    unsigned int nel=0;

    if (eref->dependencies) if (curvature_identifyadjacent(eref, id, v, MANIFOLD_GRADEFIND_MAXENTRIES, &nel)) {
        if (nel>0) field_insertwithid(eref->dependencies, id, eval_listfromunsignedintegerarray(v, nel));
    }
}

/* --- Gaussian curvature --- */

/* TODO boundaries */
/* Evaluate the gaussian curvature for vertex vid, optionally calculating the area of the star around vid as well. */
MFLD_DBL gausscurv_gausscurv(manifold_object *mobj, interpretcontext *context, unsigned int vid, MFLD_DBL *area) {
    uid faces[FUNCTIONAL_MAXENTRIES];
    unsigned int nfaces=0,nv;
    MFLD_DBL angle=0.0;
    
    manifold_graderaise(mobj, MANIFOLD_GRADE_POINT, vid, MANIFOLD_GRADE_AREA, FUNCTIONAL_MAXENTRIES, faces, &nfaces);
    
    if (area) *area=0.0;
    
    /* Loop over adjacent faces */
    for (unsigned int i=0; i<nfaces; i++) {
        uid vert[3]; // The vertices of each triangle
        MFLD_DBL xx[mobj->dimension], dp;
        unsigned int k=0;
        expression *mv[4];
        
        for (unsigned int j=0; j<4; j++) mv[j]=NULL;
        
        manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, faces[i], MANIFOLD_GRADE_POINT, vert, &nv);
        
        /* Loop over the edges and get the vertex displacements as a multivector */
        for (unsigned int j=0; j<nv; j++) {
            if (vert[j]!=vid) {
                manifold_vertexdisplacement(mobj, vid, vert[j], xx);
                mv[k]=multivector_doublelisttomultivector(context, mobj->dimension, MANIFOLD_GRADE_LINE, xx, mobj->dimension);
                k++;
            }
        }
        
        if (k==2) {
            /* Perform the geometric product */
            mv[2]=opmul(context, mv[0], mv[1]);
            
            if (eval_isobjectref(mv[2])) {
                multivectorobject *m=(multivectorobject *) ((expression_objectreference *) mv[2])->obj;
                multivector_gradetodoublelist(m, MANIFOLD_GRADE_POINT, &dp); // The dot product (Note we need to keep the sign)
                mv[3]=multivector_gradenorm(m, MANIFOLD_GRADE_AREA, 1.0); // The wedge product of the two vectors
                
                if (eval_isreal(mv[3])) {
                    angle += atan2(eval_floatvalue(mv[3]), dp);
                }
                
                if (area) *area += 0.5 * eval_floatvalue(mv[3]);
            }
        }
        
        for (unsigned int j=0; j<4; j++) if (mv[j]) freeexpression(mv[j]);
    }
    
    if (area) *area /= 3.0;
    
    return 2*PI-angle;
}

void gausscurv_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp=NULL;
    MFLD_DBL gcurv, area;
    
    gcurv=gausscurv_gausscurv(eref->mobj, eref->context, id, &area);
    if (eref->fobj->flags & INTEGRANDONLY_FLAG) gcurv /= area;
    exp=newexpressionfloat(gcurv);
    
    if (exp) field_insertwithid(eref->target, id, exp);
}

expression *gausscurv_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=gausscurv_integrand_mapfunction;
    fobj->force=NULL;
    
    return NULL;
}

/* --- Gaussian squared curvature --- */
void gausssqcurv_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp=NULL;
    MFLD_DBL gcurv, area, result;
    
    gcurv=gausscurv_gausscurv(eref->mobj, eref->context, id, &area);
    result = gcurv*gcurv/area;
    if (eref->fobj->flags & INTEGRANDONLY_FLAG) result /=area;
    
    exp=newexpressionfloat( result );
    
    if (exp) field_insertwithid(eref->target, id, exp);
}

expression *gausssqcurv_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=gausssqcurv_integrand_mapfunction;
    fobj->force=NULL;
    
    return NULL;
}

/* --- Mean curvature --- */
expression *meancurv_meancurv(manifold_object *mobj, interpretcontext *context, unsigned int vid, MFLD_DBL *area) {
    uid faces[FUNCTIONAL_MAXENTRIES];
    unsigned int nfaces=0,nv;
    expression *vfrc=NULL;
    
    manifold_graderaise(mobj, MANIFOLD_GRADE_POINT, vid, MANIFOLD_GRADE_AREA, FUNCTIONAL_MAXENTRIES, faces, &nfaces);
    
    if (area) *area=0.0;
    
    /* Loop over adjacent faces */
    for (unsigned int i=0; i<nfaces; i++) {
        uid vert[3], overt[2]; // The vertices of each triangle, and an ordered version
        MFLD_DBL xx[mobj->dimension], farea=0.0;
        unsigned int k=0;
        expression *s0, *s1, *mv[6];
        
        for (unsigned int j=0; j<6; j++) mv[j]=NULL;
        
        manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, faces[i], MANIFOLD_GRADE_POINT, vert, &nv);
        
        /* Reorder the vertices so that vid is first */
        for (unsigned int j=0; j<nv; j++) {
            if (vert[j]!=vid) { overt[k]=vert[j]; k++; }
        }
        
        if (k==2) {
            manifold_vertexdisplacement(mobj, vid, overt[0], xx);
            s0=multivector_doublelisttomultivector(context, mobj->dimension, MANIFOLD_GRADE_LINE, xx, mobj->dimension);
            manifold_vertexdisplacement(mobj, overt[0], overt[1], xx);
            s1=multivector_doublelisttomultivector(context, mobj->dimension, MANIFOLD_GRADE_LINE, xx, mobj->dimension);
            
            /* s0 ^ s1 */
            mv[0]=opmul(context, s0, s1);
            
            if (eval_isobjectref(mv[0])) {
                // Project onto grade 2
                mv[1]=multivector_projectontograde((multivectorobject *) ((expression_objectreference *) mv[0])->obj, MANIFOLD_GRADE_AREA);
                // |s0 ^ s1|
                mv[2]=multivector_gradenorm((multivectorobject *) ((expression_objectreference *) mv[1])->obj, MANIFOLD_GRADE_AREA, 1.0);
                
                if (eval_isreal(mv[2])) farea=eval_floatvalue(mv[2])/2;
                if (area) *area += farea; // Keep track of the area surrounding the vertex
                
                if (fabs(farea)<MACHINE_EPSILON) error_raise(context, ERROR_ZEROELEMENT, ERROR_ZEROELEMENT_MSG, ERROR_FATAL);
                
                // s1 ^ (s0 ^ s1)
                mv[3]=opmul(context, s1, mv[1]);
                // Project onto grade 1
                mv[4]=multivector_projectontograde((multivectorobject *) ((expression_objectreference *) mv[3])->obj, MANIFOLD_GRADE_LINE);
                // Scale
                multivector_scale((multivectorobject *) ((expression_objectreference *) mv[4])->obj, context, 0.5/eval_floatvalue(mv[2]));
                
                // Add onto the vertex force
                if (!vfrc) {
                    vfrc=cloneexpression(mv[4]);
                } else {
                    mv[5]=opadd(context, vfrc, mv[4]);
                    freeexpression(vfrc);
                    vfrc=cloneexpression(mv[5]);
                }
                //multivector_print((multivectorobject *) ((expression_objectreference *) vfrc)->obj);
            }
        }
        
        for (unsigned int j=0; j<6; j++) if (mv[j]) freeexpression(mv[j]);
        if (s0) freeexpression(s0);
        if (s1) freeexpression(s1);
    }
    
    if (area) *area /= 3.0;
    
    return vfrc;
}

void meancurv_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *mcurv=NULL, *mnorm=NULL, *vdual=NULL, *prod=NULL;
    MFLD_DBL area=0.0, sgnmcurv=0.0;
    
    /* Calculate mean curvature */
    mcurv=meancurv_meancurv(eref->mobj, eref->context, id, &area);
    
    // Get the vertex normal as a vector
    if (eval_isobjectref(mcurv)) mnorm=manifold_vertexnormal(eref->mobj, eref->context, id);
    if (eval_isobjectref(mnorm)) vdual=multivector_dual((multivectorobject *) eval_objectfromref(mnorm), eref->context);
    if (eval_isobjectref(vdual)) multivector_scale((multivectorobject *) eval_objectfromref(vdual), eref->context, -1.0);
    
    // Dot it with the mean curvature
    if (eval_isobjectref(vdual) && eval_isobjectref(mcurv)) prod=opmul(eref->context, mcurv, vdual);
    
    if (eval_isobjectref(prod)) {
        multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(prod), MANIFOLD_GRADE_POINT, &sgnmcurv);
        sgnmcurv*=0.5;
        if (eref->fobj->flags & INTEGRANDONLY_FLAG) sgnmcurv/=area;
        field_insertwithid(eref->target, id, newexpressionfloat(sgnmcurv));
    }
    
    if (mcurv) freeexpression(mcurv);
    if (mnorm) freeexpression(mnorm);
    if (vdual) freeexpression(vdual);
    if (prod) freeexpression(prod);
}

expression *meancurv_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=meancurv_integrand_mapfunction;
    fobj->force=NULL;
    
    return NULL;
}

/* --- Mean square curvature --- */

void meansqcurv_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp=NULL, *mcurv=NULL, *avcurv=NULL;
    MFLD_DBL area=0.0, avmc, result;
    
    mcurv=meancurv_meancurv(eref->mobj, eref->context, id, &area);
    if (eval_isobjectref(mcurv)) {
        /* Average mean curvature 1/2 Fv / (area) */
        avcurv=multivector_gradenorm((multivectorobject *) eval_objectfromref(mcurv), MANIFOLD_GRADE_LINE, 0.5/area);
        
        if (eval_isreal(avcurv)) {
            avmc=eval_floatvalue(avcurv);
            result=avmc*avmc*area;
            if (eref->fobj->flags & INTEGRANDONLY_FLAG) result/=area;
            exp = newexpressionfloat(result);
        }
    }
    if (mcurv) freeexpression(mcurv);
    if (avcurv) freeexpression(avcurv);
    
    if (exp) field_insertwithid(eref->target, id, exp);
}

expression *meansqcurv_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=meansqcurv_integrand_mapfunction;
    fobj->force=NULL;
    fobj->dependencies=curvature_dependencies_mapfunction;
    
    return NULL;
}

expression *curv_setintegrandonlyi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    if (nargs==1 && eval_isbool(args[0])) {
        if (eval_boolvalue(args[0])) {
            fobj->flags |= INTEGRANDONLY_FLAG;
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* --- Vertex normals --- */

void vertexnormal_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *vnormal=NULL, *vdual;
    
    vnormal=manifold_vertexnormal(eref->mobj, eref->context, id);
    
    if (eval_isobjectref(vnormal)) {
        multivectorobject *mvobj=(multivectorobject *) eval_objectfromref(vnormal);
        vdual=multivector_dual(mvobj, eref->context);
        if (eval_isobjectref(vdual)) {
            multivector_scale((multivectorobject *) eval_objectfromref(vdual), eref->context, -1.0);
            field_insertwithid(eref->target, id, vdual);
        }
    }
    
    if (vnormal) freeexpression(vnormal);
}

expression *vertexnormal_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=NULL;
    fobj->force=vertexnormal_force_mapfunction;
    
    return NULL;
}

/* --- Enclosed volume --- */

/* TODO: This formulation only works for surfaces with no serious overhangs (i.e. volume elements are always positive) */

void enclosedvolume_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression_objectreference *exp=NULL, *exp2=NULL;
    unsigned int nel=5;
    uid xid[5];
    expression *xmv[3]={NULL, NULL, NULL};
    
    // Obtain the vertex ids
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, xid, &nel);
    
    // Now get the associated multivectors
    if (nel==3) {
        for (unsigned int i=0; i<3; i++) xmv[i]=manifold_vertextomultivector(eref->mobj, eref->context, xid[i]);
    }
    
    if ((eval_isobjectref(xmv[0]))&&(eval_isobjectref(xmv[1]))&&(eval_isobjectref(xmv[2]))) {
        // Volume contribution is (1/6) * <x0*x1*x2>_3
        exp=(expression_objectreference *) opmul(eref->context, xmv[0], xmv[1]);
        exp2=(expression_objectreference *) opmul(eref->context, (expression *) exp, xmv[2]);
        
        if (eval_isobjectref(exp2)) field_insertwithid(eref->target, id, multivector_gradenorm((multivectorobject *) exp2->obj, MANIFOLD_GRADE_VOLUME, 1.0/6.0));
    }
    
    for (unsigned int i=0; i<3; i++) if (xmv[i]) freeexpression(xmv[i]);
    if (exp) freeexpression((expression *) exp);
    if (exp2) freeexpression((expression *) exp2);
}

void enclosedvolume_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression_objectreference *vol=NULL, *vol2=NULL, *exp, *exp2;
    unsigned int nel;
    uid xid[5];
    expression *xmv[3]={NULL, NULL, NULL};
    expression *cp[3]={NULL, NULL, NULL};
    expression *ii=NULL;
    double volume=0.0;
    
    /* Obtain the vertex ids */
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, xid, &nel);
    
    /* Now get the associated multivectors */
    if (nel==3) {
        for (unsigned int i=0; i<3; i++) {
            xmv[i]=manifold_vertextomultivector(eref->mobj, eref->context, xid[i]);
        }
    }
    
    /* The unit volume */
    ii=multivector_unitmultivector(eref->context, eref->mobj->dimension, MANIFOLD_GRADE_VOLUME, 0);
    
    if ((eval_isobjectref(xmv[0]))&&(eval_isobjectref(xmv[1]))&&(eval_isobjectref(xmv[2]))) {
        /* evaluate geometric products */
        cp[0]=opmul(eref->context, xmv[1], xmv[2]);
        cp[1]=opmul(eref->context, xmv[2], xmv[0]);
        cp[2]=opmul(eref->context, xmv[0], xmv[1]);
        
        /* Volume contribution is (1/6) * <x0*x1*x2>_3  */
        vol=(expression_objectreference *) opmul(eref->context, cp[2], xmv[2]);
        if (vol) {
            vol2=(expression_objectreference *) multivector_projectontograde((multivectorobject *) vol->obj, MANIFOLD_GRADE_VOLUME);
            multivector_gradetodoublelist((multivectorobject *) vol2->obj, MANIFOLD_GRADE_VOLUME, &volume); /* TODO: Not dimensionally indep. */
            freeexpression((expression *) vol);
        }
        
        for (unsigned int i=0; i<3; i++) {
            exp=(expression_objectreference *) opmul(eref->context, ii, cp[i]);
            exp2=(expression_objectreference *) multivector_projectontograde((multivectorobject *) exp->obj, MANIFOLD_GRADE_LINE);
            freeexpression((expression *) exp);
            multivector_scale((multivectorobject *) exp2->obj, eref->context, volume/fabs(volume)/6.0);
            
            field_accumulatewithid(eref->target, eref->context, xid[i], (expression *) exp2);
            freeexpression((expression *) exp2);
        }
    }
    
    for (unsigned int i=0; i<3; i++) {
        if (xmv[i]) freeexpression(xmv[i]);
        if (cp[i]) freeexpression(cp[i]);
    }
    if (ii) freeexpression(ii);
    if (vol2) freeexpression((expression *) vol2);
}


expression *enclosedvolume_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=enclosedvolume_integrand_mapfunction;
    fobj->force=enclosedvolume_force_mapfunction;
    
    return NULL;
}

/* --- Equi area --- */

MFLD_DBL equiarea_computemeanarea(uid id, void *content, functionalmapref *eref, MFLD_DBL *aa, uid *ai, unsigned int *na) {
    uid aid[FUNCTIONAL_MAXENTRIES];
    unsigned int ngr=0;
    int ret=FALSE;
    MFLD_DBL mean=0.0;
    
    ret=manifold_graderaise(eref->mobj, MANIFOLD_GRADE_POINT, id, MANIFOLD_GRADE_AREA, FUNCTIONAL_MAXENTRIES, aid, &ngr);
    
    if (ret && ngr>0) {
        double areas[ngr];
        expression_objectreference *mv=NULL;
        expression *exp=NULL;
        
        /* Extract the area from the multivector version of each element */
        for (unsigned int i=0; i<ngr; i++) {
            areas[i]=0.0;
            
            /* Get the multivector */
            mv=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_AREA, aid[i]);
            /* Compute the norm */
            if (eval_isobjectref(mv)) exp=multivector_gradenorm((multivectorobject *) mv->obj, MANIFOLD_GRADE_AREA, 0.5);
            if (eval_isreal(exp)) areas[i]=eval_floatvalue(exp);
            
            if (mv) freeexpression((expression *) mv);
            if (exp) freeexpression(exp);
            mean+=areas[i];
        }
        
        mean = mean / (double) ngr;
        
        if (aa) for (unsigned int i=0; i<ngr; i++) aa[i]=areas[i];
        if (ai) for (unsigned int i=0; i<ngr; i++) ai[i]=aid[i];
        if (na) *na = ngr;
    }
    
    return mean;
}

void equiarea_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    double areas[FUNCTIONAL_MAXENTRIES], total=0.0, mean=0.0;
    unsigned nv=0;
    
    mean = equiarea_computemeanarea(id, content, eref, areas, NULL, &nv);
    
    for (unsigned int i=0; i<nv; i++) total+=(1.0-areas[i]/mean)*(1.0-areas[i]/mean);
    
    field_insertwithid(eref->target, id, newexpressionfloat(total));
    
}

expression *equiarea_gradarea(interpretcontext *context, manifold_object *mobj, uid id, uid aid) {
    expression *xx[3]={NULL, NULL, NULL};
    expression *ss[3]={NULL, NULL, NULL};
    expression *result = NULL;
    
    uid vid[4]; // The vertex ids in triangle aid.
    uid s12[2];
    unsigned int nvert;
    
    /* Lower the grade to get the vertex ids */
    
    if (manifold_gradelower(mobj, MANIFOLD_GRADE_AREA, aid, MANIFOLD_GRADE_POINT, vid, &nvert)) {
        /* Find the vertices that are NOT id */
        unsigned int k=0;
        for (unsigned int j=0; j<nvert; j++) {
            if (vid[j]!=id) { s12[k]=vid[j]; k++; }
        }
        
        /* There should be two. */
        if (k==2) {
            /* Get the vertex positions */
            xx[0]=manifold_vertextomultivector(mobj, context, id);
            xx[1]=manifold_vertextomultivector(mobj, context, s12[0]);
            xx[2]=manifold_vertextomultivector(mobj, context, s12[1]);
            
            /* Compute the edge vectors */
            if (xx[0] && xx[1] && xx[2]) {
                ss[0]=opsub(context, xx[1], xx[0]);
                ss[1]=opsub(context, xx[2], xx[0]);
                ss[2]=opsub(context, xx[2], xx[1]);
            }
            
            /* The gradient of the area due to vertex 0 is Cross[Cross[X1 - X0, X2 - X0], X2 - X1]/Area/4 */
            if (ss[0] && ss[1] && ss[2]) {
                expression_objectreference *exp=NULL, *exp2=NULL, *exp3=NULL, *exp4=NULL;
                expression *exp5=NULL;
                exp = (expression_objectreference *) opmul(context, ss[0], ss[1]);
                if (exp) exp2 = (expression_objectreference *) multivector_projectontograde((multivectorobject *) exp->obj, MANIFOLD_GRADE_AREA);
                
                if (exp2) exp3 = (expression_objectreference *) opmul(context, (expression *) exp2, ss[2]);
                if (exp3) exp4 = (expression_objectreference *) multivector_projectontograde((multivectorobject *) exp3->obj, MANIFOLD_GRADE_LINE);
                /* We now have -Cross[Cross[X1 - X0, X2 - X0], X2 - X1] in exp4 */
                
                /* Get the area |s0^s1|/2 */
                if (exp2) exp5=multivector_gradenorm((multivectorobject *) eval_objectfromref(exp2), MANIFOLD_GRADE_AREA, 0.5);
                
                /* Now scale by the area and 1/4 to get the gradient of the area wrt x0 */
                if (eval_isreal(exp5)) multivector_scale((multivectorobject *) exp4->obj, context, -0.25/eval_floatvalue(exp5));
                
                if (exp4) result=(expression *) exp4;
                
                /* Free the intermediates */
                if (exp) freeexpression((expression *) exp);
                if (exp2) freeexpression((expression *) exp2);
                if (exp3) freeexpression((expression *) exp3);
                //if (exp4) freeexpression((expression *) exp4);
                if (exp5) freeexpression((expression *) exp5);
            }
            
            // Free  intermediate expressions
            for (unsigned int j=0; j<3; j++) {
                if (xx[j]) freeexpression(xx[j]);
                if (ss[j]) freeexpression(ss[j]);
            }
        }
    }
    
    return result;
}

void equiarea_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    double areas[FUNCTIONAL_MAXENTRIES], mean=0.0;
    uid aid[FUNCTIONAL_MAXENTRIES];
    unsigned int ngr=0;
    
    /* Compute the mean area */
    mean = equiarea_computemeanarea(id, content, eref, areas, aid, &ngr);
    
    for (unsigned int i=0; i<ngr; i++) {
        expression_objectreference *gradA=(expression_objectreference *) equiarea_gradarea(eref->context, eref->mobj, id, aid[i]);
        /* The force on this vertex due to this triangle is 2 (a-m)/m^2 grad(A) */
        multivector_scale((multivectorobject *) gradA->obj, eref->context, -2.0*(areas[i]-mean)/(mean*mean));
        
        field_accumulatewithid(eref->target, eref->context, id, (expression *) gradA);
        //multivector_print((multivectorobject *) gradA->obj);
        
        if (gradA) freeexpression((expression *) gradA);
    }
}

expression *equiarea_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=equiarea_integrand_mapfunction;
    fobj->force=equiarea_force_mapfunction;
    
    return NULL;
}

/* --- Equi area weighted --- */

int equiarea_computeareaandweight(uid id, void *content, functionalmapref *eref, MFLD_DBL *aa, MFLD_DBL *wt, MFLD_DBL *ma, MFLD_DBL *mw, uid *ai, unsigned int *na) {
    uid aid[FUNCTIONAL_MAXENTRIES];
    unsigned int ngr=0;
    int ret=FALSE;
    object *fweight=NULL;
    MFLD_DBL marea=0.0, mweight=0.0;
    int success=TRUE;
    
    ret=manifold_graderaise(eref->mobj, MANIFOLD_GRADE_POINT, id, MANIFOLD_GRADE_AREA, FUNCTIONAL_MAXENTRIES, aid, &ngr);
    
    /* Is a weight set? */
    if (eval_isobjectref(eref->fobj->field)) {
        object *fw=(object *) eval_objectfromref(eref->fobj->field);
        
        if (class_objectrespondstoselector(fw, EVAL_INDEX)) {
            fweight=fw;
        }
    }
    
    if (ret && ngr>0) {
        double areas[ngr];
        double weights[ngr];
        expression_objectreference *mv=NULL;
        expression *exp=NULL;
        
        /* Extract the area from the multivector version of each element */
        for (unsigned int i=0; i<ngr; i++) {
            areas[i]=0.0;
            
            /* Get the multivector */
            mv=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_AREA, aid[i]);
            /* Compute the norm */
            if (eval_isobjectref(mv)) exp=multivector_gradenorm((multivectorobject *) mv->obj, MANIFOLD_GRADE_AREA, 0.5);
            if (eval_isreal(exp)) areas[i]=eval_floatvalue(exp);
            
            weights[i]=1.0;
            
            if (fweight) {
                expression *wexp=class_callselector(eref->context, fweight, EVAL_INDEX, 1, EXPRESSION_INTEGER, aid[i]);
                if (eval_isreal(wexp)) {
                    weights[i]=eval_floatvalue(wexp);
                } else success=FALSE;
                if (wexp) freeexpression(wexp);
            }
            
            mweight+=weights[i];
            
            if (mv) freeexpression((expression *) mv);
            if (exp) freeexpression(exp);
            marea+=areas[i];
        }
        
        marea = marea / (double) ngr;
        mweight = mweight / (double) ngr;
        
        if (aa) for (unsigned int i=0; i<ngr; i++) aa[i]=areas[i];
        if (wt) for (unsigned int i=0; i<ngr; i++) wt[i]=weights[i];
        if (ai) for (unsigned int i=0; i<ngr; i++) ai[i]=aid[i];
        if (na) *na = ngr;
        if (ma) *ma = marea;
        if (mw) *mw = mweight; 
    }
    
    return success;
}

void equiareaweighted_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    double areas[FUNCTIONAL_MAXENTRIES], weights[FUNCTIONAL_MAXENTRIES], total=0.0, marea, mweight;
    unsigned nv=0;
    
    if (equiarea_computeareaandweight(id, content, eref, areas, weights, &marea, &mweight, NULL, &nv)) {
        for (unsigned int i=0; i<nv; i++) {
            double x = (1.0-weights[i]*areas[i]/marea/mweight);
            total+=x*x;
        }
    
        field_insertwithid(eref->target, id, newexpressionfloat(total));
    }
}

void equiareaweighted_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    double areas[FUNCTIONAL_MAXENTRIES], weights[FUNCTIONAL_MAXENTRIES], marea, mweight;
    uid aid[FUNCTIONAL_MAXENTRIES];
    expression_objectreference *gradA[FUNCTIONAL_MAXENTRIES];
    expression_objectreference *gradM;
    unsigned int nv=0;
    
    /* Compute the mean area and mean weights */
    if (equiarea_computeareaandweight(id, content, eref, areas, weights, &marea, &mweight, aid, &nv)) {
        for (unsigned int i=0; i<nv; i++) {
            gradA[i]=(expression_objectreference *) equiarea_gradarea(eref->context, eref->mobj, id, aid[i]);
           // multivector_print((multivectorobject *) eval_objectfromref(gradA[i]));
        }
        
        gradM=(expression_objectreference *) multivector_total(eref->context, gradA, nv);
        
        //multivector_print((multivectorobject *) eval_objectfromref(gradM));
        
        if (gradM) for (unsigned int i=0; i<nv; i++) {
            double w=weights[i]/mweight;
            expression_objectreference *frc=(expression_objectreference *) multivector_clone((multivectorobject *) eval_objectfromref(gradM));
            
            /* frc is -1/ma^2 (2 w[[i]]) (w[[i]] area[[i]] - ma) (-grdA[[i]] +
             area[[i]] gradm/ma) */
            
            multivector_scale((multivectorobject *) eval_objectfromref(frc), eref->context, areas[i]/marea);
            multivector_add(eref->context, (multivectorobject *) eval_objectfromref(frc), (multivectorobject *) eval_objectfromref(gradA[i]), TRUE, (multivectorobject *) eval_objectfromref(frc));
            multivector_scale((multivectorobject *) eval_objectfromref(frc), eref->context, 2.0/(marea*marea)* w * (w*areas[i] - marea));
            
            //multivector_print((multivectorobject *) eval_objectfromref(frc));
            //printf("--\n");
            
            field_accumulatewithid(eref->target, eref->context, id, (expression *) frc);
            
            if (frc) freeexpression((expression *) frc);
        }
        
        for (unsigned int i=0; i<nv; i++) if (gradA[i]) freeexpression((expression *) gradA[i]);
        if (gradM) freeexpression((expression *) gradM);
    }
    
}

expression *equiareaweighted_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=equiareaweighted_integrand_mapfunction;
    fobj->force=equiareaweighted_force_mapfunction;
    
    return NULL;
}

/* --- Electrostatic energy --- */


expression *electrostatic_computeperpendicular(interpretcontext *context, expression *s1, expression *s2, int sgn) {
    expression *e[4]={NULL, NULL, NULL, NULL};
    expression *res=NULL;
    MFLD_DBL s=0.0, t=1.0;
    
    /* s1 - (s1.s2) s2 / (s2.2) */
    
    e[0]=opmul(context, s1, s2);
    e[1]=opmul(context, s2, s2);
    
    if (eval_isobjectref(e[0])) multivector_gradetodoublelist((multivectorobject *) ((expression_objectreference *) e[0])->obj, MANIFOLD_GRADE_POINT, &s);
    if (eval_isobjectref(e[1])) multivector_gradetodoublelist((multivectorobject *) ((expression_objectreference *) e[1])->obj, MANIFOLD_GRADE_POINT, &t);
    s/=t;
    
    if (s2) e[2]=multivector_clone((multivectorobject *) ((expression_objectreference *) s2)->obj);
    if (eval_isobjectref(e[2])) multivector_scale((multivectorobject *) ((expression_objectreference *) e[2])->obj, context, s);
    
    res=opsub(context, s1, e[2]);
    if (eval_isobjectref(res)) {
        
        e[3]=multivector_gradenorm((multivectorobject *) ((expression_objectreference *) res)->obj, MANIFOLD_GRADE_LINE, 1.0);
        if (eval_isnumerical(e[3])) t=eval_floatvalue(e[3]);
        
        multivector_scale((multivectorobject *) ((expression_objectreference *) res)->obj, context, ((double) sgn)/(t*t));
    }
    
    for (unsigned int i=0; i<4; i++) if (e[i]) freeexpression(e[i]);
    
    return res;
}

/* Calculates grad phi
 In:      eref    - the functional map reference
 id      - the id of the face
 gl      - the faces grade lower entry
 field   - the field that represents phi
 Out:   * area    - the area of the face
 * gradphi - grad phi
 vid     - the vertex ids
 sid     - edge ids
 * s       - the edge multivectors
 f       - field values
 * t       - the perpendicular distances
 In the above, items with a (*) need to be freed by the caller.
 */
void electrostatic_gradphi(functionalmapref *eref, uid id, manifold_gradelowerentry *gl, field_object *field, expression **area, expression **gradphi, uid vid[4], uid sid[3], int sgn[3], expression *s[3], expression *f[3], expression *t[3]) {
    unsigned int i, nel;
    expression *u=NULL, *t2[3];
    
    *area = surfacetension_area(eref->mobj, eref->context, id);
    
    for (i=0; i<3; i++) { f[i]=NULL; s[i]=NULL; t[i]=NULL; t2[i]=NULL; };
    
    /* Obtain the vertex ids */
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vid, &nel);
    /* And the edges */
    manifold_areaboundaryedges(eref->mobj, gl, vid, sid, sgn);
    
    /*for (i=0; i<3; i++) {
     manifold_vertex *tv=idtable_get(eref->mobj->vertices, vid[i]);
     for (unsigned int j=0; j<3; j++) printf("%g, ", tv->x[j]);
     printf("\n");
     }*/
    
    for (i=0; i<3; i++) {
        /* Now lookup the corresponding field values */
        f[i]=field_get(field, vid[i]);
        //expressionprint(f[i]); printf("\n");
        /* And edge as a multivector */
        s[i]=manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, sid[i]);
    }
    
    /* Now compute perpendiculars for each vertex */
    t[0]=electrostatic_computeperpendicular(eref->context, s[2], s[1], sgn[2]);
    t[1]=electrostatic_computeperpendicular(eref->context, s[0], s[2], sgn[0]);
    t[2]=electrostatic_computeperpendicular(eref->context, s[1], s[0], sgn[1]);
    
    /* scale the perpendiculars by the field value */
    for (i=0; i<3; i++) {
        double u=0;
        if (eval_isnumerical(f[i])) u=eval_floatvalue(f[i]);
        if (t[i]) t2[i]=multivector_clone((multivectorobject *) ((expression_objectreference *)t[i])->obj);
        if (t2[i]) multivector_scale((multivectorobject *) ((expression_objectreference *) t2[i])->obj, eref->context, u);
    }
    
    u=opadd(eref->context, t2[0], t2[1]);
    *gradphi=opadd(eref->context, u, t2[2]);
    
    //multivector_print((multivectorobject *) ((expression_objectreference *) (*gradphi))->obj);
    
    if (u) freeexpression(u);
    for (i=0; i<3; i++) if (t2[i]) freeexpression(t2[i]);
    
}

/* Frees data structures used in calculating grad phi */
void electrostatic_freegradphi(expression **area, expression **gradphi, expression *s[3], expression *t[3]) {
    for (unsigned int i=0; i<3; i++) {
        if (s[i]) freeexpression(s[i]);
        if (t[i]) freeexpression(t[i]);
    }
    if (*area) { freeexpression(*area); area=NULL; };
    if (*gradphi) { freeexpression(*gradphi); gradphi=NULL; };
}

void electrostatic_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *field=NULL;
    expression *v=NULL;
    
    /* Variables needed to calculate gradphi */
    uid vid[3], sid[3];
    int sgn[3];
    expression *f[3], *s[3], *t[3], *gradphi=NULL, *area=NULL;
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    /* Calculate gradphi */
    electrostatic_gradphi(eref, id, (manifold_gradelowerentry *) content, field, &area, &gradphi, vid, sid, sgn, s, f, t);
    
    /* Now calculate the norm of the gradphi multivector */
    if (eval_isobjectref(gradphi)) v=multivector_gradenorm((multivectorobject *) ((expression_objectreference *) gradphi)->obj, MANIFOLD_GRADE_LINE, 1.0);
    if (eval_isreal(v) && eval_isreal(area)) {
        MFLD_DBL val=eval_floatvalue(v);
        /* (gradphi . gradphi) area */
        val=val*val*eval_floatvalue(area);
        
        /* Insert into the field */
        field_insertwithid(eref->target, id, newexpressionfloat(val));
    }
    
    /* Free everything */
    if (v) freeexpression(v);
    electrostatic_freegradphi(&area, &gradphi, s, t);
}

void electrostatic_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *field=NULL;
    
    /* Variables needed to calculate gradphi */
    uid vid[3], sid[3];
    int sgn[3];
    expression *f[3], *s[3], *t[3], *gradphi=NULL, *area=NULL;
    
    expression_objectreference *xx[3];
    expression *e[2]={NULL, NULL}, *v=NULL;
    MFLD_DBL a=0.0, ff[3], df[3];
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
        if (!field) return;
    }
    
    /* Calculate gradphi */
    electrostatic_gradphi(eref, id, (manifold_gradelowerentry *) content, field, &area, &gradphi, vid, sid, sgn, s, f, t);
    
    if (eval_isreal(area)) a=eval_floatvalue(area);
    
    /* Calculate  force */
    for (unsigned int i=0; i<3; i++) {
        if (eval_isnumerical(f[i])) ff[i]=eval_floatvalue(f[i]);
        xx[i]=(expression_objectreference *) manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_POINT, vid[i]);
    }
    
    /*for (unsigned int i=0; i<3; i++) {
     printf("%g\n",ff[i]);
     multivector_print((multivectorobject *) xx[i]->obj);
     }*/
    
    df[0]=ff[2]-ff[1]; df[1]=ff[0]-ff[2]; df[2]=ff[1]-ff[0];
    for (unsigned int i=0; i<3; i++) {
        if (xx[i]) multivector_scale((multivectorobject *) xx[i]->obj, eref->context, df[i]);
    }
    e[0]=opadd(eref->context, (expression *) xx[0], (expression *) xx[1]);
    if (e[0]) e[1]=opadd(eref->context, e[0], (expression *) xx[2]);
    
    if (e[1]) for (unsigned int i=0; i<3; i++) {
        expression_objectreference *frc=(expression_objectreference *) multivector_clone((multivectorobject *) ((expression_objectreference *) e[1])->obj);
        if (frc) multivector_scale((multivectorobject *) frc->obj, eref->context, -0.5*df[i]/a);
        
        //multivector_print((multivectorobject *) frc->obj);
        
        field_accumulatewithid(eref->target, eref->context, vid[i], (expression *) frc);
        if (frc) freeexpression((expression *) frc);
    }
    
    /* Now add in the area component */
    if (eval_isobjectref(gradphi)) v=multivector_gradenorm((multivectorobject *) ((expression_objectreference *) gradphi)->obj, MANIFOLD_GRADE_LINE, 1.0);
    if (eval_isreal(v)) {
        MFLD_DBL vv=eval_floatvalue(v);
        surfacetension_force_withscale(id, content, ref, -vv*vv);
    }
    
    /*for (unsigned int i=0; i<3; i++) {
     expression_objectreference *xref=(expression_objectreference *) field_get(eref->target, vid[i]);
     
     if (xref) multivector_print((multivectorobject *) xref->obj);
     }*/
    
    /* Free everything */
    electrostatic_freegradphi(&area, &gradphi, s, t);
    if (v) freeexpression(v);
    for (unsigned int i=0; i<2; i++) if (e[i]) freeexpression(e[i]);
    for (unsigned int i=0; i<3; i++) if (xx[i]) freeexpression((expression *) xx[i]);
}

void electrostatic_generalizedforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *field=NULL;
    expression *v=NULL;
    
    /* Variables needed to calculate gradphi */
    uid vid[3], sid[3];
    int sgn[3];
    expression *f[3], *s[3], *t[3], *gradphi=NULL, *area=NULL;
    
    MFLD_DBL a=0.0;
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
        if (eref->field!=field) return;
    }
    
    /* Calculate gradphi */
    electrostatic_gradphi(eref, id, (manifold_gradelowerentry *) content, field, &area, &gradphi, vid, sid, sgn, s, f, t);
    
    if (eval_isreal(area)) a=eval_floatvalue(area);
    
    /* Calculate generalized force */
    for (unsigned int i=0; i<3; i++) {
        expression_objectreference *u=NULL;
        expression *res=NULL;
        MFLD_DBL df=0.0;
        /* D[F, psi[i]] = 2*(t[i] . gradphi)*area */
        u=(expression_objectreference *) opmul(eref->context, t[i], gradphi); /* TODO: opdot */
        if (eval_isobjectref(u)) multivector_gradetodoublelist((multivectorobject *) u->obj, MANIFOLD_GRADE_POINT, &df);
        
        /*multivector_print((multivectorobject *) ((expression_objectreference *) t[0])->obj);
         multivector_print((multivectorobject *) ((expression_objectreference *) gradphi)->obj);*/
        
        res=newexpressionfloat(-2.0*a*df);
        field_accumulatewithid(eref->target, eref->context, vid[i], res);
        if (u) freeexpression((expression *) u);
        if (res) freeexpression((expression *) res);
    }
    
    /* Free everything */
    if (v) freeexpression(v);
    electrostatic_freegradphi(&area, &gradphi, s, t);
}

expression *electrostatic_setfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    if (nargs==1 && eval_isobjectref(args[0])) {
        if (fobj->field) freeexpression((expression *) fobj->field);
        
        fobj->field=(expression_objectreference *) cloneexpression(args[0]);
    } else {
        error_raise(context, ERROR_NOFIELD, ERROR_NOFIELD_MSG, ERROR_FATAL);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *electrostatic_getfieldi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    expression *ret = NULL;
    
    if (fobj->field) ret=cloneexpression((expression *) fobj->field);
    else ret=newexpressionnone();
    
    return ret;
}

expression *electrostatic_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=electrostatic_integrand_mapfunction;
    fobj->force=electrostatic_force_mapfunction;
    fobj->generalizedforce=electrostatic_generalizedforce_mapfunction;
    
    return NULL;
}

/* --- Field harmonic (linear u, constant u0 over a line element) --- */

void fieldharmonic_calculatefld(functionalmapref *eref, manifold_gradelowerentry *lower, MFLD_DBL *length, MFLD_DBL fval[2], MFLD_DBL *fm) {
    *length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
    
    field_object *fld=NULL;
    if (eref->fobj->field) fld = (field_object *) eref->fobj->field->obj;
    
    if (eval_isnumerical(eref->fobj->param)) *fm = eval_floatvalue(eref->fobj->param);
    for (unsigned int i=0; i<2; i++) {
        expression *exp = field_get(fld, lower->el[i]);
        if (eval_isnumerical(exp)) fval[i]=eval_floatvalue(exp);
    }
}

void fieldharmonic_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, fm=0.0, val=0.0;
    
    if ((eref)&&(eref->fobj)&&(eref->fobj->field)&&(eref->mobj)) {
        fieldharmonic_calculatefld(eref, lower, &length, fval, &fm);
        
        val=(fval[0]*fval[0] + fval[0]*fval[1] + fval[1]*fval[1])/3.0 - (fval[0]+fval[1])*fm + fm*fm;
        
        field_insertwithid(eref->target, id, newexpressionfloat(val*length));
    }
}

void fieldharmonic_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, fm=0.0, val=0.0;
    
    if ((eref)&&(eref->mobj)) {
        unsigned int dim = eref->mobj->dimension;
        MFLD_DBL xx[dim];
        expression *mv=NULL;
        
        fieldharmonic_calculatefld(eref, lower, &length, fval, &fm);
        
        val=(fval[0]*fval[0] + fval[0]*fval[1] + fval[1]*fval[1])/3.0 - (fval[0]+fval[1])*fm + fm*fm;
        
        /* Compute the force, which for vertex 0 is - (x1 - x0)/|x1-x0| */
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        manifold_vertexdisplacement(eref->mobj, lower->el[0], lower->el[1], xx); /* x1 - x0 */
        for (unsigned int i=0; i<dim; i++) xx[i]=- val * xx[i]/length;
        
        /* Convert to multivector */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], mv); // Add to field
        freeexpression(mv);
        
        /* Reverse the sign for the second vertex */
        for (unsigned int i=0; i<dim; i++) xx[i]=-xx[i];
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], mv); // Add to field
        freeexpression(mv);
    }
}

void fieldharmonic_generalizedforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, fm=0.0;
    expression *u=NULL, *v=NULL;
    
    if ((eref)&&(eref->fobj)&&(eref->fobj->field)&&(eref->mobj)) {
        fieldharmonic_calculatefld(eref, lower, &length, fval, &fm);
        
        u=newexpressionfloat(-((2*fval[0]+fval[1])/3.0 - fm)*length);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], u);
        
        v=newexpressionfloat(-((2*fval[1]+fval[0])/3.0 - fm)*length);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], v);
    }
    
    if (u) freeexpression(u);
    if (v) freeexpression(v);
}

expression *fieldharmonic_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=fieldharmonic_integrand_mapfunction;
    fobj->force=fieldharmonic_force_mapfunction;
    fobj->generalizedforce=fieldharmonic_generalizedforce_mapfunction;
    
    return NULL;
}

/* --- Field harmonic 2 (linear u, linear preferred u0 over a line element) --- */

void fieldharmonic2_calculatefld(functionalmapref *eref, manifold_gradelowerentry *lower, MFLD_DBL *length, MFLD_DBL fval[2], MFLD_DBL f0val[2]) {
    expression *exp;
    
    *length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
    
    /* The field object */
    field_object *fld=NULL;
    if (eval_isobjectref(eref->fobj->field)) fld = (field_object *) eval_objectfromref(eref->fobj->field);
    
    /* The preferred value field */
    field_object *fld0=NULL;
    if (eval_isobjectref(eref->fobj->param)) fld0 = (field_object *) eval_objectfromref(eref->fobj->param);
    
    /* Read off the parameters */
    for (unsigned int i=0; i<2; i++) {
        fval[i]=0.0; f0val[i]=0.0;
        if (fld) exp = field_get(fld, lower->el[i]);
        if (eval_isnumerical(exp)) fval[i]=eval_floatvalue(exp);
        if (fld0) exp = field_get(fld0, lower->el[i]);
        if (eval_isnumerical(exp)) f0val[i]=eval_floatvalue(exp);
    }
}

void fieldharmonic2_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, f0val[2]={0.0,0.0}, val=0.0;
    
    if ((eref)&&(eref->fobj)&&(eref->fobj->field)&&(eref->mobj)) {
        fieldharmonic2_calculatefld(eref, lower, &length, fval, f0val);
        
        val=(fval[0]*fval[0] + fval[0]*fval[1] + fval[1]*fval[1] +
             f0val[0]*f0val[0] + f0val[0]*f0val[1] + f0val[1]*f0val[1] -
             2*fval[0]*f0val[0] - 2*fval[1]*f0val[1] - fval[0]*f0val[1] - fval[1]*f0val[0])/3.0;
        
        field_insertwithid(eref->target, id, newexpressionfloat(val*length));
    }
}

void fieldharmonic2_force_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, f0val[2]={0.0,0.0}, val=0.0;
    
    if ((eref)&&(eref->mobj)) {
        unsigned int dim = eref->mobj->dimension;
        MFLD_DBL xx[dim];
        expression *mv=NULL;
        
        fieldharmonic2_calculatefld(eref, lower, &length, fval, f0val);
        
        val=(fval[0]*fval[0] + fval[0]*fval[1] + fval[1]*fval[1] +
             f0val[0]*f0val[0] + f0val[0]*f0val[1] + f0val[1]*f0val[1] -
             2*fval[0]*f0val[0] - 2*fval[1]*f0val[1] - fval[0]*f0val[1] - fval[1]*f0val[0])/3.0;
        
        /* Compute the force, which for vertex 0 is - (x1 - x0)/|x1-x0| */
        length=manifold_vertexseparation(eref->mobj, lower->el[0], lower->el[1]);
        manifold_vertexdisplacement(eref->mobj, lower->el[0], lower->el[1], xx); /* x1 - x0 */
        for (unsigned int i=0; i<dim; i++) xx[i]=- val * xx[i]/length;
        
        /* Convert to multivector */
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], mv); // Add to field
        freeexpression(mv);
        
        /* Reverse the sign for the second vertex */
        for (unsigned int i=0; i<dim; i++) xx[i]=-xx[i];
        mv = multivector_doublelisttomultivector(eref->context, dim, MANIFOLD_GRADE_LINE, xx, dim);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], mv); // Add to field
        freeexpression(mv);
    }
}

void fieldharmonic2_generalizedforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    MFLD_DBL length=0.0, fval[2]={0.0,0.0}, f0val[2]={0.0,0.0};
    expression *u=NULL, *v=NULL;
    
    if ((eref)&&(eref->fobj)&&(eref->fobj->field)&&(eref->mobj)) {
        fieldharmonic2_calculatefld(eref, lower, &length, fval, f0val);
        
        u=newexpressionfloat(-length*(2*fval[0] + fval[1] - 2*f0val[0] - f0val[1])/3.0);
        field_accumulatewithid(eref->target, eref->context, lower->el[0], u);
        
        v=newexpressionfloat(-length*(fval[0] + 2*fval[1] - f0val[0] - 2*f0val[1])/3.0);
        field_accumulatewithid(eref->target, eref->context, lower->el[1], v);
    }
    
    if (u) freeexpression(u);
    if (v) freeexpression(v);
}

expression *fieldharmonic2_setvaluei(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        fobj->param=cloneexpression(args[0]);
    } else {
        error_raise(context, ERROR_NOFIELDVAL, ERROR_NOFIELDVAL_MSG, ERROR_FATAL);
    }
    
    return (expression *) class_newobjectreference(obj);
}

expression *fieldharmonic2_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=fieldharmonic2_integrand_mapfunction;
    fobj->force=fieldharmonic2_force_mapfunction;
    fobj->generalizedforce=fieldharmonic2_generalizedforce_mapfunction;
    
    return NULL;
}

/* Field norm squared. Calculates the norm of a quantity defined at each vertex. */

void fieldnormsq_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *fld=NULL;
    if (eval_isobjectref(eref->fobj->field)) fld = (field_object *) eval_objectfromref(eref->fobj->field);
    expression *exp=NULL, *dot=NULL, *out=NULL;
    
    if ((eref)&&(eref->fobj)&&(eref->fobj->field)&&(eref->mobj)) {
        if (fld) exp = field_get(fld, id);
        dot=opmul(eref->context, exp, exp);
        if (eval_isobjectref(dot)) {
            out=multivector_gradenorm((multivectorobject *) ((expression_objectreference *) dot)->obj, MANIFOLD_GRADE_POINT, 1.0);
        }
        if (dot) freeexpression(dot);
        
        field_insertwithid(eref->target, id, out);
    }
}

void fieldnormsq_force_mapfunction(uid id, void *content, void *ref) {
    /* The fieldnormsq does not cause forces on vertices, so this does nothing */
}

void fieldnormsq_generalizedforce_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *fld=NULL;
    expression *exp=NULL;
    
    if (eval_isobjectref(eref->fobj->field)) fld = (field_object *) eval_objectfromref(eref->fobj->field);
    
    if ((eref)&&(eref->fobj)&&(fld)&&(eref->mobj)) {
        if (fld) exp = field_get(fld, id);
        
        if (eval_isobjectref(exp)) {
            expression *new=multivector_clone((multivectorobject *) eval_objectfromref(exp));
            
            if (eval_isobjectref(new)) {
                multivector_scale((multivectorobject *) eval_objectfromref(new), eref->context, -2.0);
                field_accumulatewithid(eref->target, eref->context, id, new);
            }
            
            if (new) freeexpression(new);
        }
    }
}

expression *fieldnormsq_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_POINT;
    fobj->integrand=fieldnormsq_integrand_mapfunction;
    fobj->force=fieldnormsq_force_mapfunction;
    fobj->generalizedforce=fieldnormsq_generalizedforce_mapfunction;
    
    return NULL;
}

/* Nematic energy. */

/* Calculates the gradient of the director components
 In:      eref    - the functional map reference
 id      - the id of the face
 gl      - the faces grade lower entry
 field   - the field that represents phi
 Out:   * area    - the area of the face
 * gradnn - grad nn [nx ny nz]
 vid     - the vertex ids
 sid     - edge ids
 * s       - the edge multivectors
 * f       - field values
 * t       - the perpendicular distances
 In the above, items with a (*) need to be freed by the caller.
 */
void nematic_gradnn(functionalmapref *eref, uid id, manifold_gradelowerentry *gl, field_object *field, expression **area, double nn[3][3], expression *gradnn[3], uid vid[4], uid sid[3], int sgn[3], expression *s[3], expression *f[3], expression *t[3]) {
    unsigned int i, nel;
    expression *u=NULL, *t2[3];
    
    *area = surfacetension_area(eref->mobj, eref->context, id);
    
    for (i=0; i<3; i++) { f[i]=NULL; s[i]=NULL; t[i]=NULL; t2[i]=NULL; };
    
    /* Obtain the vertex ids */
    manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vid, &nel);
    /* And the edges */
    manifold_areaboundaryedges(eref->mobj, gl, vid, sid, sgn);
    
    for (i=0; i<3; i++) {
        /* Now lookup the corresponding field values */
        f[i]=field_get(field, vid[i]);
        /* Copy the components into the nn array */
        if (eval_isobjectref(f[i])) {
            multivectorobject *mv = (multivectorobject *) eval_objectfromref(f[i]);
            double line[3];
            
            if (mv) {
                if (multivector_gradetodoublelist(mv, MANIFOLD_GRADE_LINE, line)) {
                    for (unsigned int j=0; j<3; j++) nn[i][j]=line[j];
                }
            }
        }
        
        /* And edge as a multivector */
        s[i]=manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, sid[i]);
    }
    
    /* Now compute perpendiculars for each vertex */
    t[0]=electrostatic_computeperpendicular(eref->context, s[2], s[1], sgn[2]);
    t[1]=electrostatic_computeperpendicular(eref->context, s[0], s[2], sgn[0]);
    t[2]=electrostatic_computeperpendicular(eref->context, s[1], s[0], sgn[1]);
    
    /* Loop over components */
    for (unsigned int k=0; k<3; k++) {
        /* scale the perpendiculars by the kth component field value */
        for (i=0; i<3; i++) {
            if (t[i]) t2[i]=multivector_clone((multivectorobject *) ((expression_objectreference *)t[i])->obj);
            if (t2[i]) multivector_scale((multivectorobject *) ((expression_objectreference *) t2[i])->obj, eref->context, nn[i][k]);
        }
        
        u=opadd(eref->context, t2[0], t2[1]);
        gradnn[k]=opadd(eref->context, u, t2[2]);
        
        // Free intermediates
        if (u) freeexpression(u);
        for (i=0; i<3; i++) if (t2[i]) freeexpression(t2[i]);
        
        //multivector_print((multivectorobject *) eval_objectfromref(gradnn[k]));
    }
}

/* Frees data structures used in calculating grad nn */
void nematic_freegradnn(expression **area, expression *gradnn[3], expression *s[3], expression *t[3]) {
    for (unsigned int i=0; i<3; i++) {
        if (s[i]) freeexpression(s[i]);
        if (t[i]) freeexpression(t[i]);
    }
    if (*area) { freeexpression(*area); area=NULL; };
    if (*gradnn) {
        for (unsigned int i=0; i<3; i++) {
            freeexpression(gradnn[i]);
            gradnn[i]=NULL;
            
        }
    };
}

/* Obtain the divergence and the curl from the gradient tensor */
void nematic_divcurlnn(expression *gradnn[3], double dnn[3][3], double *divnn, double curlnn[3]) {
    multivectorobject *mv=NULL;
    
    /* Construct dnn tensor */
    for (unsigned i=0; i<3; i++) {
        if (eval_isobjectref(gradnn[i])) mv=(multivectorobject *) eval_objectfromref(gradnn[i]);
        multivector_gradetodoublelist(mv, MANIFOLD_GRADE_LINE, dnn[i]);
    }
    
    *divnn = dnn[0][0] + dnn[1][1] + dnn[2][2];
    curlnn[0] = dnn[2][1] - dnn[1][2];
    curlnn[1] = dnn[0][2] - dnn[2][0];
    curlnn[2] = dnn[1][0] - dnn[0][1];
}

/* Barycentric integration of dot products */
double nematic_bcint(double f[3], double g[3]) {
    return (f[0]*(2*g[0]+g[1]+g[2]) + f[1]*(g[0]+2*g[1]+g[2]) + f[2]*(g[0]+g[1]+2*g[2]))/12;
}

void nematic_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    //manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    field_object *field=NULL;
    
    /* Variables needed to calculate gradnn */
    uid vid[3], sid[3];
    int sgn[3];
    expression *f[3], *s[3], *t[3], *gradnn[3]={NULL, NULL, NULL}, *area=NULL;
    double nn[3][3] = {{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}}; // The field values
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    /* Lookup elastic constants */
    double ksplay=1.0, ktwist=1.0, kbend=1.0;
    
    if (eref->fobj->param) {
        hashtableobject *hto=NULL;
        hashtable *ht=NULL;
        
        /* Get the hashtable */
        if (eval_isobjectref(eref->fobj->param)) {
            hto = (hashtableobject *) eval_objectfromref(eref->fobj->param);
            if (hto) ht=hto->table;
        }
        
        /* Lookup and set constants */
        if (ht) {
            expression *exp=hashget(ht, NEMATIC_SPLAY);
            if (eval_isnumerical(exp)) ksplay=eval_floatvalue(exp);
            exp=hashget(ht, NEMATIC_TWIST);
            if (eval_isnumerical(exp)) ktwist=eval_floatvalue(exp);
            exp=hashget(ht, NEMATIC_BEND);
            if (eval_isnumerical(exp)) kbend=eval_floatvalue(exp);
        }
    }
    
    
    /* Calculate grad nn */
    nematic_gradnn(eref, id, (manifold_gradelowerentry *) content, field, &area, nn, gradnn, vid, sid, sgn, s, f, t);
    
    /* Obtain div and curl from grad nn */
    double dnn[3][3];
    double nnt[3][3];
    double divnn = 0.0;
    double curlnn[3] = {0.0,0.0,0.0};
    
    nematic_divcurlnn(gradnn, dnn, &divnn, curlnn);
    
    /* From components of the curl, construct the coefficients that go in front of integrals of
       nx^2, ny^2, nz^2, nx*ny, ny*nz, and nz*nx over the element. */
    
    double ctwst[6] = { curlnn[0]*curlnn[0], curlnn[1]*curlnn[1], curlnn[2]*curlnn[2],
                        2*curlnn[0]*curlnn[1], 2*curlnn[1]*curlnn[2], 2*curlnn[2]*curlnn[0]};
    
    double cbnd[6] = { ctwst[1] + ctwst[2], ctwst[0] + ctwst[2], ctwst[0] + ctwst[1],
                       -ctwst[3], -ctwst[4], -ctwst[5] };
    
    /* Now transpose the */
    for (unsigned int i=0; i<3; i++) for (unsigned int j=0; j<3; j++) nnt[i][j]=nn[j][i];
    
    /* Calculate integrals of nx^2, ny^2, nz^2, nx*ny, ny*nz, and nz*nx over the element */
    double integrals[6] = { nematic_bcint(nnt[0], nnt[0]), nematic_bcint(nnt[1], nnt[1]), nematic_bcint(nnt[2], nnt[2]),
                            nematic_bcint(nnt[0], nnt[1]), nematic_bcint(nnt[1], nnt[2]), nematic_bcint(nnt[2], nnt[0]) };
    
    /* Now we can calculate the components of splay, twist and bend */
    double splay=0.0, twist=0.0, bend=0.0, a=eval_floatvalue(area);
    
    splay = ksplay*0.5*a*divnn*divnn;
    for (unsigned int i=0; i<6; i++) {
        twist += ctwst[i]*integrals[i];
        bend += cbnd[i]*integrals[i];
    }
    twist *= ktwist*0.5*a;
    bend *= kbend*0.5*a;
    
    /* And insert the energy into the output field */
    field_insertwithid(eref->target, id, newexpressionfloat(splay+twist+bend));
    
    /* Free data structures associated with calculating gradnn */
    nematic_freegradnn(&area, gradnn, s, t);
}

expression *nematic_setconstantsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    interpretcontext *options=NULL;
    
    int optionstart = 0;
    
    /* Find the start of any options */
    optionstart = eval_optionstart(context, nargs, args);
    
    /* Process options */
    options=class_options(obj,context,optionstart,nargs,args);
    
    /* If no constants have been set, create a hashtable to hold them */
    if (!fobj->param) {
        hashtable *ht=hashcreate(2);
        if (ht) fobj->param=(expression *) class_hashtable_objectfromhashtable(ht);
    }
    
    if (fobj->param) {
        hashtableobject *hto=NULL;
        hashtable *ht=NULL;
        
        /* Get the hashtable */
        if (eval_isobjectref(fobj->param)) {
            hto = (hashtableobject *) eval_objectfromref(fobj->param);
            if (hto) ht=hto->table;
        }
        
        /* Lookup and set constants */
        if (ht) {
            char *keys[3] = {NEMATIC_SPLAY, NEMATIC_TWIST, NEMATIC_BEND};
            
            for (unsigned int i=0; i<3; i++) {
                expression *exp=lookupsymbol(options, keys[i]);
                if (exp) {
                    expression *old = hashget(ht, keys[i]); // Free the old one if it exists.
                    if (old) freeexpression(old);
                    hashinsert(ht, keys[i], exp);
                }
            }
        }
    }
    
    if (options) freeinterpretcontext(options);
    
    //return (expression *) class_newobjectreference((expression_objectreference *) fobj->param);
    return cloneexpression(fobj->param);
}

expression *nematic_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=nematic_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Nematic cylindrical coordinates */

typedef struct {
    double dn;
    double cn[3];
    double ksplay;
    double ktwist;
    double kbend;
} nematiccylintegranddata;

double nematiccylintegrand(unsigned int dim, double *lambda, double *x, expression **quantity, void *data) {
    nematiccylintegranddata *d=data;
    expression_objectreference *director = (expression_objectreference *) quantity[0];
    double nn[3], sp,tw,bx,by,bz;
    
    multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(director), MANIFOLD_GRADE_LINE, nn);
    /* Splay */
    sp=(nn[0]+d->dn*x[0])/x[0];
    /* Twist */
    tw=nn[0]*d->cn[0] + nn[1]*d->cn[1] + nn[2]*(d->cn[2]+nn[1]/x[0]);
    /* Bend */
    bx=(d->cn[1]*nn[0] - d->cn[0]*nn[1]);
    by=(d->cn[2]*nn[0] - d->cn[0]*nn[2] + nn[0]*nn[1]/x[0]);
    bz=(d->cn[1]*nn[2] - nn[1]*(nn[1]+d->cn[2]*x[0])/x[0]);
    
    //return (bx*bx + by*by + bz*bz);
    //return d->ksplay * sp*sp + d->ktwist * tw*tw + d->kbend * (bx*bx + by*by + bz*bz);
    return x[0]*(d->ksplay * sp*sp + d->ktwist * tw*tw + d->kbend * (bx*bx + by*by + bz*bz));
}

void nematiccyl_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *field=NULL;
    nematiccylintegranddata ncyldata;
    ncyldata.ksplay=1.0;
    ncyldata.ktwist=1.0;
    ncyldata.kbend=1.0;
    
    /* Variables needed to calculate gradnn */
    uid vid[3], sid[3];
    int sgn[3];
    expression *f[3], *s[3], *t[3], *gradnn[3]={NULL, NULL, NULL}, *area=NULL;
    double nn[3][3] = {{0.0,0.0,0.0},{0.0,0.0,0.0},{0.0,0.0,0.0}}; // The field values
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    /* Lookup elastic constants */
    if (eref->fobj->param) {
        hashtableobject *hto=NULL;
        hashtable *ht=NULL;
        
        /* Get the hashtable */
        if (eval_isobjectref(eref->fobj->param)) {
            hto = (hashtableobject *) eval_objectfromref(eref->fobj->param);
            if (hto) ht=hto->table;
        }
        
        /* Lookup and set constants */
        if (ht) {
            expression *exp=hashget(ht, NEMATIC_SPLAY);
            if (eval_isnumerical(exp)) ncyldata.ksplay=eval_floatvalue(exp);
            exp=hashget(ht, NEMATIC_TWIST);
            if (eval_isnumerical(exp)) ncyldata.ktwist=eval_floatvalue(exp);
            exp=hashget(ht, NEMATIC_BEND);
            if (eval_isnumerical(exp)) ncyldata.kbend=eval_floatvalue(exp);
        }
    }
    
    
    /* Calculate grad nn */
    nematic_gradnn(eref, id, (manifold_gradelowerentry *) content, field, &area, nn, gradnn, vid, sid, sgn, s, f, t);
    
    /* Obtain div and curl from grad nn */
    double dnn[3][3]; // Matrix of derivatives
    MFLD_DBL *x[3];
    expression **q[3];
    double result=0.0;

    nematic_divcurlnn(gradnn, dnn, &ncyldata.dn, ncyldata.cn);
    
    /* Set up data for integration */
    for (unsigned int i=0; i<3; i++) {
        manifold_vertex *v = (manifold_vertex *) idtable_get(eref->mobj->vertices, vid[i]);
        x[i]=v->x;
        q[i] = &f[i]; // The field values
    }
    
    result=discretization_triangleintegrate(eref->context, nematiccylintegrand, eref->mobj->dimension, x, 1, q, &ncyldata);
    
    /* Weight by twice the area */
    if (eval_isreal(area)) result*=eval_floatvalue(area); // Should be 2x but we have a factor of 1/2 from the Frank energy.
    
    /* And insert the energy into the output field */
    field_insertwithid(eref->target, id, newexpressionfloat(result));
    
    /* Free data structures associated with calculating gradnn */
    nematic_freegradnn(&area, gradnn, s, t);
}

expression *nematiccyl_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=nematiccyl_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Nematic electric field
   int (n . E)^2 dA */

double nematicelectricintegrand(unsigned int dim, double *lambda, double *x, expression **quantity, void *data) {
    expression_objectreference *director = (expression_objectreference *) quantity[0];
    double nn[3], ee[3], nndotee;
    
    multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(director), MANIFOLD_GRADE_LINE, nn);
    multivector_gradetodoublelist((multivectorobject *) data, MANIFOLD_GRADE_LINE, ee);
    
    nndotee=manifold_dotproduct3d(nn, ee);
    
    return (nndotee*nndotee);
}

void nematicelectric_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    field_object *director=NULL;
    multivectorobject *field=NULL;

    /* Obtain the director object */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        director=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    if (eref->fobj && eval_isobjectref(eref->fobj->param)) {
        field=(multivectorobject *) ((expression_objectreference *) eref->fobj->param)->obj;
    }
    
    if (director && field) {
        /* Obtain div and curl from grad nn */
        MFLD_DBL *x[3];
        expression **q[3];
        double result=0.0;
        uid vid[3];
        expression *f[3];
        unsigned int nel;
        
        /* Obtain the vertex ids */
        manifold_gradelower(eref->mobj, MANIFOLD_GRADE_AREA, id, MANIFOLD_GRADE_POINT, vid, &nel);
        
        /* Now lookup the corresponding director values */
        for (unsigned int i=0; i<3; i++) {
            f[i]=field_get(director, vid[i]);
        }
        
        /* Set up data for integration */
        for (unsigned int i=0; i<3; i++) {
            manifold_vertex *v = (manifold_vertex *) idtable_get(eref->mobj->vertices, vid[i]);
            x[i]=v->x;
            q[i] = &f[i]; // The field values
        }
        
        result=discretization_triangleintegrate(eref->context, nematicelectricintegrand, eref->mobj->dimension, x, 1, q, (void *) field);
        
        /* Weight by twice the area */
        expression *area = surfacetension_area(eref->mobj, eref->context, id);
        if (eval_isreal(area)) result*=2*eval_floatvalue(area);
        if (area) freeexpression(area);
        
        /* And insert the energy into the output field */
        field_insertwithid(eref->target, id, newexpressionfloat(result));
    }
}

expression *nematicelectric_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_AREA;
    fobj->integrand=nematicelectric_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Nematic anchoring */

/* 1/2 int W (n.s)^2 dl */

void nematicanchoring_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    field_object *field = NULL;
    expression *s0=NULL, *n0=NULL, *n1=NULL, *n0s0=NULL, *n1s0=NULL;
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    /* Get n0, n1 and s0 */
    if (field) {
        n0=field_get(field, lower->el[0]);
        n1=field_get(field, lower->el[1]);
        s0=manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, id);
    }
    
    /* Calculate the dot product */
    if (n0 && n1 && s0) {
        n0s0=opmul(eref->context, n0, s0);
        n1s0=opmul(eref->context, n1, s0);
        
        double n0s=0.0, n1s=0.0, dl=0.0, val=0.0;
        if (eval_isobjectref(n0s0)) multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(n0s0), MANIFOLD_GRADE_POINT, &n0s);
        if (eval_isobjectref(n1s0)) multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(n1s0), MANIFOLD_GRADE_POINT, &n1s);
        
        if (eval_isobjectref(s0)) {
            expression *exp=multivector_gradenorm((multivectorobject *) eval_objectfromref(s0), MANIFOLD_GRADE_LINE, 1.0);
            if (exp) {
                dl=eval_floatvalue(exp);
                freeexpression(exp);
            }
        }
        
        /* And insert the energy into the output field */
        if (fabs(n0s-n1s)>1e-6) {
            val=(-(n0s*n0s*n0s) + (n1s*n1s*n1s))/3/(n1s-n0s);
        } else {
            val=n0s*n0s;
        }
        
        field_insertwithid(eref->target, id, newexpressionfloat( 0.5*val/dl ));
    }
    
    if (s0) freeexpression(s0);
    if (n0s0) freeexpression(n0s0);
    if (n1s0) freeexpression(n1s0);
}

expression *nematicanchoring_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=nematicanchoring_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Normal anchoring */

/* 1/2 int W (n.N)^2 dl where N is the surface normal */

void nematicanchoringnormal_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    field_object *field = NULL;
    expression *s0=NULL, *n0=NULL, *n1=NULL, *n0nn=NULL, *n1nn=NULL, *nn=NULL;
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) ((expression_objectreference *) eref->fobj->field)->obj;
    }
    
    /* Get n0, n1 and s0 */
    if (field) {
        n0=field_get(field, lower->el[0]);
        n1=field_get(field, lower->el[1]);
        s0=manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, id);
        
        multivectorobject *mv = (multivectorobject *) eval_objectfromref(s0);
        double m[3];
        //multivector_print(mv);
        
        multivector_gradetodoublelist(mv, MANIFOLD_GRADE_LINE, m);
        m[2]=-m[1];
        m[1]=m[0];
        m[0]=m[2];
        m[2]=0.0;
        manifold_normalizevector3d(m, m);
        
        nn=multivector_doublelisttomultivector(eref->context, mv->dimension, MANIFOLD_GRADE_LINE, m, 3);
        
        //multivector_print((multivectorobject *) eval_objectfromref(nn));
        
    }
    
    /* Calculate the dot product */
    if (n0 && n1 && nn) {
        n0nn=opmul(eref->context, n0, nn);
        n1nn=opmul(eref->context, n1, nn);
        
        double n0s=0.0, n1s=0.0, dl=0.0, val=0.0;
        if (eval_isobjectref(n0nn)) multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(n0nn), MANIFOLD_GRADE_POINT, &n0s);
        if (eval_isobjectref(n1nn)) multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(n1nn), MANIFOLD_GRADE_POINT, &n1s);
        
        if (eval_isobjectref(s0)) {
            expression *exp=multivector_gradenorm((multivectorobject *) eval_objectfromref(s0), MANIFOLD_GRADE_LINE, 1.0);
            if (exp) {
                dl=eval_floatvalue(exp);
                freeexpression(exp);
            }
        }
        
        /* And insert the energy into the output field */
        if (fabs(n0s-n1s)>1e-6) {
            val=(-(n0s*n0s*n0s) + (n1s*n1s*n1s))/3/(n1s-n0s);
        } else {
            val=n0s*n0s;
        }
        
        field_insertwithid(eref->target, id, newexpressionfloat( 0.5*val*dl ));
    }
    
    if (s0) freeexpression(s0);
    if (nn) freeexpression(nn);
    if (n0nn) freeexpression(n0nn);
    if (n1nn) freeexpression(n1nn);
}

expression *nematicanchoringnormal_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=nematicanchoringnormal_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Nematic anchoring with cylindrical coordinates */

typedef struct {
    interpretcontext *context;
    expression *s0;
} nematicanchoringcyl_integrandref;

double nematicanchoringcyl_integrand(unsigned int dim, double *t, double *x, expression **quantity, void *data) {
    nematicanchoringcyl_integrandref *ref = (nematicanchoringcyl_integrandref *) data;
    expression *ns0;
    double ndots0=0.0;
    
    ns0=opmul(ref->context, quantity[0], ref->s0);
    if (eval_isobjectref(ns0)) multivector_gradetodoublelist((multivectorobject *) eval_objectfromref(ns0), MANIFOLD_GRADE_POINT, &ndots0);
    freeexpression(ns0);
    
    return ndots0*ndots0*x[0];
}

/* 1/2 int W (n.s)^2 r dl */
void nematicanchoringcyl_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    nematicanchoringcyl_integrandref iref;
    field_object *field = NULL;
    
    expression *s0=NULL, *nn[2]={NULL, NULL}, **q[2], *dl=NULL;
    MFLD_DBL *x[2];
    multivectorobject *s0mv;
    double len=0.0, res=0.0;
    
    iref.context=NULL; iref.s0=NULL;
    
    /* Obtain the field object from the map reference */
    if (eref->fobj && eval_isobjectref(eref->fobj->field)) {
        field=(field_object *) eval_objectfromref(eref->fobj->field);
    }

    /* Get n0, n1 and s0 */
    if (field) {
        iref.context=eref->context;
        
        nn[0]=field_get(field, lower->el[0]);
        nn[1]=field_get(field, lower->el[1]);
        iref.s0=manifold_elementtomultivector(eref->mobj, eref->context, MANIFOLD_GRADE_LINE, id);
        
        if (eval_isobjectref(iref.s0)) {
            /* Scale s0 to a unit vector */
            s0mv=(multivectorobject *) eval_objectfromref(iref.s0);
            dl=multivector_gradenorm(s0mv, MANIFOLD_GRADE_LINE, 1.0);
            if (eval_isreal(dl)) len=eval_floatvalue(dl);
            if (fabs(len)>0.0) multivector_scale(s0mv, eref->context, 1/len);
            
            /* Set up data for integration */
            for (unsigned int i=0; i<2; i++) {
                manifold_vertex *v = (manifold_vertex *) idtable_get(eref->mobj->vertices, lower->el[i]);
                x[i]=v->x;
                q[i] = &nn[i]; // The field values
            }
    
            res=discretization_lineintegrate(eref->context, nematicanchoringcyl_integrand, eref->mobj->dimension, x, 1, q, &iref);
            res*=len;
            
            field_insertwithid(eref->target, id, newexpressionfloat( 0.5*res ));
        }
    }
    
    if (s0) freeexpression(s0);
    if (dl) freeexpression(dl);
}

expression *nematicanchoringcyl_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_LINE;
    fobj->integrand=nematicanchoringcyl_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* ----------------------------------------------------------------------------------------------------
 * Volume functionals
 * ----------------------------------------------------------------------------------------------------  */

expression *volume_volume(manifold_object *mobj, interpretcontext *context, uid id) {
    expression_objectreference *exp;
    expression *ret=NULL;
    
    exp = (expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_VOLUME, id);
    if (eval_isobjectref(exp)) ret=multivector_gradenorm((multivectorobject *) exp->obj, MANIFOLD_GRADE_VOLUME, 1.0/6.0);
    if (exp) freeexpression((expression *) exp);
    return ret;
}

void volume_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    expression *exp = NULL;
    
    exp = volume_volume(eref->mobj, eref->context, id);
    if (exp) field_insertwithid(eref->target, id, exp);
}

expression *volume_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_VOLUME;
    fobj->integrand=volume_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/* Linear elasticity (3d version) */

MFLD_DBL linearelastic3d_calculategrammatrixdot(MFLD_DBL *v1, MFLD_DBL *v2, int n) {
    MFLD_DBL ret=0.0;
    for (unsigned int i=0; i<n; i++) ret+=v1[i]*v2[i];
    return ret;
}

/* Calculate Gram matrix in arbitrary dimensions */
int linearelastic3d_calculategrammatrix(manifold_object *m, interpretcontext *context, uid id, int grade, MFLD_DBL *sf) {
    manifold_gradelowerentry *gl=idtable_get(m->gradelower[grade-1], id); // The root grade lower entry
    unsigned int nv=grade+1;
    
    uid vid[nv]; // Vertex ids.
    MFLD_DBL s[grade][m->dimension]; // vertex displacements
    
    if (gl) {
        // Get the consituent vertices.
        manifold_gradelower(m, grade, id, MANIFOLD_GRADE_POINT, vid, &nv);
        if (nv!=grade+1) return FALSE;
        
        // Compute displacements
        for (unsigned int i=1; i<nv;i++) manifold_vertexdisplacement(m, vid[0], vid[i], s[i-1]);
        
        // Build Gram matrix
        for (unsigned int i=0; i<grade; i++) {
            for (unsigned int j=0; j<=i; j++) {
                sf[(i*m->dimension)+j]=linearelastic3d_calculategrammatrixdot(s[i],s[j],m->dimension);
                sf[(j*m->dimension)+i]=sf[(i*m->dimension)+j];
            }
        }
    }
    
    return TRUE;
}

void linearelastic3d_matrixmultiply(MFLD_DBL *a, MFLD_DBL *b, unsigned int n, MFLD_DBL *out, double scale) {
    cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, n, n, n, scale, a, n, b, n, 0.0, out, n);
}

int linearelastic3d_matrixinvert(MFLD_DBL *a, unsigned int n, MFLD_DBL *out) {
    int piv[n], nn=n;
    int info, lwork=n*n;
    double work[n*n];
    int success=FALSE;
    
    /* Copy a into out */
    memcpy(out, a, sizeof(double)*n*n);
    /* Compute LU decomposition, storing result in place */
    dgetrf_(&nn, &nn, out, &nn, piv, &info);
    
    if (!info) {
        /* Now compute inverse */
        dgetri_(&nn, out, &nn, piv, work, &lwork, &info);
        
        if (!info) success=TRUE;
    }
    
    return success;
}

void linearelastic3d_integrand_mapfunction(uid id, void *content, void *ref) {
    functionalmapref *eref = (functionalmapref *) ref;
    int grade=eref->fobj->grade;
    double sf[grade*grade], sfref[grade*grade], sfinv[grade*grade], c[grade*grade], csq[grade*grade];
    manifold_object *rm=NULL;
    MFLD_DBL nu=0.5, lambda, mu;
    MFLD_DBL trCsq=0.0, trCC=0.0, volume=0.0, result=0.0;
    
    lambda=3*nu/(1+nu);
    mu=3*(1-2*nu)/(2+2*nu);
    
    if (eval_isobjectref(eref->fobj->param)) {
        rm = (manifold_object *) eval_objectfromref((expression_objectreference *) eref->fobj->param);
    } else return;
    
    // Compute gramm matrices
    if (linearelastic3d_calculategrammatrix(eref->mobj, eref->context, id, grade, sf) &&
        linearelastic3d_calculategrammatrix(rm, eref->context, id, grade, sfref)) {
        
        //for (unsigned int i=0; i<grade*grade; i++) printf("%g, ",sf[i]);
        //printf("\n");
        
        // now compute Cauchy-Green tensor (sf^-1.sref - I)/2
        if (linearelastic3d_matrixinvert(sf, grade, sfinv)) {
            linearelastic3d_matrixmultiply(sfinv, sfref, grade, c, 0.5);
            for (unsigned int i=0; i<grade; i++) c[i*grade+i]-=0.5;
            
            // Compute Tr[C]
            for (unsigned int i=0; i<grade; i++) trCsq+=c[i*grade+i];
            // Tr[C]^2
            trCsq*=trCsq;
            
            // C.C
            linearelastic3d_matrixmultiply(c, c, grade, csq, 1.0);
            // Tr[C.C]
            for (unsigned int i=0; i<grade; i++) trCC+=csq[i*grade+i];
        }
    }
    
    expression *vol=volume_volume(rm, eref->context, id);
    if (eval_isreal(vol)) volume = eval_floatvalue(vol);
    if (vol) freeexpression(vol);
    
    result=volume*(0.5*lambda*trCC + mu*trCsq);
    
    field_insertwithid(eref->target, id, newexpressionfloat( result));
}

expression *linearelastic3d_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    functional_object *fobj=(functional_object *) obj;
    
    /* Call the superclass */
    functional_newi(obj, context, nargs, args);
    
    fobj->grade=MANIFOLD_GRADE_VOLUME;
    fobj->integrand=linearelastic3d_integrand_mapfunction;
    fobj->force=NULL;
    fobj->generalizedforce=NULL;
    
    return NULL;
}

/******************
 * Initialization *
 ******************/

void functionalinitialize(void) {
    classintrinsic *functionalcls, *functionalsubcls;
    
    functionalcls=class_classintrinsic(GEOMETRY_FUNCTIONALCLASS, class_lookup(EVAL_OBJECT), sizeof(functional_object), 9);
    class_registerselector(functionalcls, EVAL_NEWSELECTORLABEL, FALSE, functional_newi);
    class_registerselector(functionalcls, EVAL_FREESELECTORLABEL, FALSE, functional_freei);
    class_registerselector(functionalcls, GEOMETRY_TOTAL_SELECTOR, FALSE, functional_generictotali);
    class_registerselector(functionalcls, GEOMETRY_PREFACTOR_SELECTOR, FALSE, functional_prefactori);
    class_registerselector(functionalcls, GEOMETRY_SETPREFACTOR_SELECTOR, FALSE, functional_setprefactori);
    class_registerselector(functionalcls, GEOMETRY_TARGET_SELECTOR, FALSE, functional_targeti);
    class_registerselector(functionalcls, GEOMETRY_SETTARGET_SELECTOR, FALSE, functional_settargeti);
    class_registerselector(functionalcls, GEOMETRY_INTEGRAND_SELECTOR, FALSE, functional_integrandi);
    class_registerselector(functionalcls, GEOMETRY_FORCE_SELECTOR, FALSE, functional_forcei);
    /* Add functional_generalizedforcei to your subclass if it involves fields */
    //class_registerselector(functionalcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalcls, GEOMETRY_GRADE_SELECTOR, FALSE, functional_gradei);
    
    /* Level set constraints */
    functionalsubcls=class_classintrinsic(GEOMETRY_LEVELSETCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 7);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, levelset_newi);
    class_registerselector(functionalsubcls, GEOMETRY_SETEXPRESSION_SELECTOR, FALSE, levelset_setexpressioni);
    class_registerselector(functionalsubcls, GEOMETRY_GETEXPRESSION_SELECTOR, FALSE, levelset_getexpressioni);
    class_registerselector(functionalsubcls, GEOMETRY_SETONESIDED_SELECTOR, FALSE, levelset_setonesidedi);
    class_registerselector(functionalsubcls, GEOMETRY_GETONESIDED_SELECTOR, FALSE, levelset_getonesidedi);
    class_registerselector(functionalsubcls, GEOMETRY_SETCOORDINATES_SELECTOR, FALSE, levelset_setcoordinatesi);
    class_registerselector(functionalsubcls, GEOMETRY_GETCOORDINATES_SELECTOR, FALSE, levelset_getcoordinatesi);
    class_registerselector(functionalsubcls, GEOMETRY_SETGRADIENT_SELECTOR, FALSE, levelset_setgradienti);
    class_registerselector(functionalsubcls, GEOMETRY_GETGRADIENT_SELECTOR, FALSE, levelset_getgradienti);
    
    /* Central pairwise */
    functionalsubcls=class_classintrinsic(GEOMETRY_CENTRALPAIRWISECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 7);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, centralpairwise_newi);
    class_registerselector(functionalsubcls, GEOMETRY_SETEXPRESSION_SELECTOR, FALSE, levelset_setexpressioni);
    class_registerselector(functionalsubcls, GEOMETRY_GETEXPRESSION_SELECTOR, FALSE, levelset_getexpressioni);
    class_registerselector(functionalsubcls, GEOMETRY_SETCOORDINATES_SELECTOR, FALSE, centralpairwise_setcoordinatesi);
    class_registerselector(functionalsubcls, GEOMETRY_GETCOORDINATES_SELECTOR, FALSE, levelset_getcoordinatesi);
    class_registerselector(functionalsubcls, GEOMETRY_SETGRADIENT_SELECTOR, FALSE, levelset_setgradienti);
    class_registerselector(functionalsubcls, GEOMETRY_GETGRADIENT_SELECTOR, FALSE, levelset_getgradienti);
    
    /* Constant force */
    functionalsubcls=class_classintrinsic(GEOMETRY_CONSTANTFORCECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 3);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, constantforce_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETFORCE_SELECTOR, FALSE, constantforce_getforcei);
    class_registerselector(functionalsubcls, GEOMETRY_SETFORCE_SELECTOR, FALSE, constantforce_setforcei);
    
    /* Gravity */
    functionalsubcls=class_classintrinsic(GEOMETRY_GRAVITYCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTION_SELECTOR, FALSE, constantforce_getforcei);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTION_SELECTOR, FALSE, constantforce_setforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, gravity_newi);
    
    /* Harmonic */
    functionalsubcls=class_classintrinsic(GEOMETRY_HARMONICFORCECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 3);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, harmonic_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETORIGIN_SELECTOR, FALSE, harmonic_getorigini);
    class_registerselector(functionalsubcls, GEOMETRY_SETORIGIN_SELECTOR, FALSE, harmonic_setorigini);
    
    /* Line tension */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINETENSIONCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linetension_newi);
    
    /* Line tension cylindrical coordinates */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINETENSIONCYLCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linetensioncyl_newi);
    
    /* Hookean elasticity */
    functionalsubcls=class_classintrinsic(GEOMETRY_HOOKECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, hooke_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETPREFERREDVALUE_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETPREFERREDVALUE_SELECTOR, FALSE, functional_genericsetparami);
    
    /* Line curvature squared */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINECURVSQCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linecurvaturesq_newi);
/*    class_registerselector(functionalsubcls, GEOMETRY_GETPATH_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETPATH_SELECTOR, FALSE, functional_genericsetparami); */
    
    /* Line torsion squared */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINETORSIONSQCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linetorsionsq_newi);
    
    /* Enclosed area */
    functionalsubcls=class_classintrinsic(GEOMETRY_ENCLOSEDAREACLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, enclosedarea_newi);
    
    /* Equilength */
    functionalsubcls=class_classintrinsic(GEOMETRY_EQUILENGTHCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, equilength_newi);
    
    /* Surface tension */
    functionalsubcls=class_classintrinsic(GEOMETRY_SURFACETENSIONCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, surfacetension_newi);
    
    /* Linear elasticity */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINEARELASTICCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linearelastic_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETREFERENCE_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETREFERENCE_SELECTOR, FALSE, fieldharmonic2_setvaluei);
    
    /* Gaussian curvature */
    functionalsubcls=class_classintrinsic(GEOMETRY_GAUSSCURVCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_SETINTEGRANDONLY_SELECTOR, FALSE, curv_setintegrandonlyi);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, gausscurv_newi);
    
    /* Gaussian squared curvature */
    functionalsubcls=class_classintrinsic(GEOMETRY_GAUSSSQCURVCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_SETINTEGRANDONLY_SELECTOR, FALSE, curv_setintegrandonlyi);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, gausssqcurv_newi);
    
    /* Mean curvature */
    functionalsubcls=class_classintrinsic(GEOMETRY_MEANCURVCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_SETINTEGRANDONLY_SELECTOR, FALSE, curv_setintegrandonlyi);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, meancurv_newi);
    
    /* Mean squared curvature */
    functionalsubcls=class_classintrinsic(GEOMETRY_MEANSQCURVCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_SETINTEGRANDONLY_SELECTOR, FALSE, curv_setintegrandonlyi);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, meansqcurv_newi);
    
    /* Vertex normal */
    functionalsubcls=class_classintrinsic(GEOMETRY_VERTEXNORMALCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, vertexnormal_newi);
    
    /* Enclosed volume */
    functionalsubcls=class_classintrinsic(GEOMETRY_ENCLOSEDVOLUMECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, enclosedvolume_newi);
    
    /* Equi area */
    functionalsubcls=class_classintrinsic(GEOMETRY_EQUIAREACLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, equiarea_newi);
    
    /* Equi area weighted */
    functionalsubcls=class_classintrinsic(GEOMETRY_EQUIAREAWEIGHTEDCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, equiareaweighted_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETWEIGHT_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETWEIGHT_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Electrostatic energy */
    functionalsubcls=class_classintrinsic(GEOMETRY_ELECTROSTATICCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, electrostatic_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETPOTENTIAL_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETPOTENTIAL_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Field harmonic */
    functionalsubcls=class_classintrinsic(GEOMETRY_FIELDHARMONICCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, fieldharmonic_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETPOTENTIAL_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETPOTENTIAL_SELECTOR, FALSE, electrostatic_setfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_GETVALUE_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETVALUE_SELECTOR, FALSE, functional_genericsetparami);
    
    /* Field harmonic 2 */
    functionalsubcls=class_classintrinsic(GEOMETRY_FIELDHARMONIC2CLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, fieldharmonic2_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETPOTENTIAL_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETPOTENTIAL_SELECTOR, FALSE, electrostatic_setfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_GETVALUE_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETVALUE_SELECTOR, FALSE, fieldharmonic2_setvaluei);
    
    /* Field norm squared */
    functionalsubcls=class_classintrinsic(GEOMETRY_FIELDNORMSQCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, fieldnormsq_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETPOTENTIAL_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETPOTENTIAL_SELECTOR, FALSE, electrostatic_setfieldi);
 
    /* Nematic energy */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematic_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Nematic electric interaction */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICELECTRICCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematicelectric_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_GETFIELD_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETFIELD_SELECTOR, FALSE, functional_genericsetparami);
    
    /* Nematic energy cylindrical coordinates */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICCYLCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematiccyl_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    //class_registerselector(functionalsubcls, GEOMETRY_GETCONSTANTS_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETCONSTANTS_SELECTOR, TRUE, nematic_setconstantsi);
    
    /* Nematic anchoring */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICANCHORINGCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematicanchoring_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Nematic anchoring with normal */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICANCHORINGNORMALCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematicanchoringnormal_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Nematic anchoring cylindrical coordinates */
    functionalsubcls=class_classintrinsic(GEOMETRY_NEMATICANCHORINGCYLCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, GEOMETRY_GENERALIZEDFORCE_SELECTOR, FALSE, functional_generalizedforcei);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, nematicanchoringcyl_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETDIRECTOR_SELECTOR, FALSE, electrostatic_getfieldi);
    class_registerselector(functionalsubcls, GEOMETRY_SETDIRECTOR_SELECTOR, FALSE, electrostatic_setfieldi);
    
    /* Volume  */
    functionalsubcls=class_classintrinsic(GEOMETRY_VOLUMECLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, volume_newi);
    
    /* Linear elasticity in 3d */
    functionalsubcls=class_classintrinsic(GEOMETRY_LINEARELASTIC3DCLASS, (classgeneric *) functionalcls, sizeof(functional_object), 1);
    class_registerselector(functionalsubcls, EVAL_NEWSELECTORLABEL, FALSE, linearelastic3d_newi);
    class_registerselector(functionalsubcls, GEOMETRY_GETREFERENCE_SELECTOR, FALSE, functional_genericgetparami);
    class_registerselector(functionalsubcls, GEOMETRY_SETREFERENCE_SELECTOR, FALSE, fieldharmonic2_setvaluei);
}

