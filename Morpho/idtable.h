/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* An id table is a hashtable indexed by an unsigned integer id number (hence a fast hashing function).
 * This is one of the fundamental data structures used by geometry objects.
 */

#ifndef idtable_h
#define idtable_h

#include "classes.h"

#define UID_NONE 0

typedef unsigned int uid;

typedef struct identry_s {
    uid id;
    void *value;
    struct identry_s *next;
} identry;

typedef struct idtable_s {
    int size;
    uid lo;
    uid hi;
    unsigned int nentries;
    identry **table;
} idtable;

#define GEOMETRY_IDTABLECLASS "idtable"

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    idtable *table;
} idtableobject ;

#define IDTABLE_INITIALSIZE 10

typedef void idtablemapfunction (uid id, void* /* pointer to the content */, void* /* A reference you supply */);
typedef void idtablemaptuplefunction (uid id1, uid id2, void* /* pointer to the content */, void* /* pointer to the content */, void* /* A reference you supply */);

typedef void idtablemapabortfunction (uid id, void* /* pointer to the content */, void* /* A reference you supply */, int * /* A variable to abort */);

#include "eval.h"

idtable *idtable_create(int size);
identry *idtable_newentry(uid id, void *value);

void idtable_insertwithid(idtable *t, uid id, void *value);
uid idtable_insert(idtable *t, void *value);
void idtable_remove(idtable *t, uid id);
void *idtable_get(idtable *t, uid id);
int idtable_ispresent(idtable *t, uid id);
unsigned int idtable_count(idtable *t);

void idtable_map(idtable *t, idtablemapfunction ,void *ref);
void idtable_maptuples(idtable *t, idtable *t2, idtablemaptuplefunction *func, void *ref);
void idtable_mapseq(idtable *t, idtablemapfunction ,void *ref);
void idtable_mapintersection(idtable *t, idtable *t2, idtablemapfunction *func, void *ref);
void idtable_mapcomplement(idtable *t, idtable *t2, idtablemapfunction *func, void *ref);
void idtable_mapabort(idtable *t, idtablemapabortfunction *func, void *ref);

void idtable_genericfreemapfunction(uid id, void *el, void *ref);
void idtable_genericfreeexpressionmapfunction(uid id, void *el, void *ref);

void idtable_freevalue(uid id, void *el, void *ref);
void idtable_free(idtable *t);

void idtableinitialize(void);

#endif /* idtable_h */
