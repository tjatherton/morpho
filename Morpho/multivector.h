/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef multivector_h
#define multivector_h

#include "eval.h"
#include "error.h"
#include "linalg.h"

/* Multivector type */
typedef unsigned int multivectorlength;

typedef enum {
    MULTIVECTOR_NONE,
    MULTIVECTOR_DOUBLES,
    MULTIVECTOR_EXPRESSIONS
} multivectortype;

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    multivectortype type;
    unsigned int dimension;
    size_t size;
    void *data;
} multivectorobject;

/* */
#define GEOMETRY_MULTIVECTORCLASS "multivector"

#define GEOMETRY_MULTIVECTORSERIALIZATIONLABEL "multivector"

#define MULTIVECTOR_DUAL_SELECTOR "dual"

/* Errors */
#define ERROR_MULTIVECTOR_INCOMPATIBLESHAPE         0x9100
#define ERROR_MULTIVECTOR_INCOMPATIBLESHAPE_MSG     "List has incompatible shape."

#define ERROR_MULTIVECTOR_INDEX                     0x9101
#define ERROR_MULTIVECTOR_INDEX_MSG                 "Selector 'index' needs at least one argument for a multivector."

#define ERROR_MULTIVECTOR_INDEX_NUM                 0x9102
#define ERROR_MULTIVECTOR_INDEX_NUM_MSG             "Selector 'index' needs integer indices."

#define ERROR_MULTIVECTOR_INDEX_BOUND               0x9103
#define ERROR_MULTIVECTOR_INDEX_BOUND_MSG           "Index out of bounds."

#define ERROR_MULTIVECTOR_DIFFDIM                   0x9104
#define ERROR_MULTIVECTOR_DIFFDIM_MSG               "Multivectors have different dimensions."

#define ERROR_MULTIVECTOR_SETINDEX_NUM              0x9105
#define ERROR_MULTIVECTOR_SETINDEX_NUM_MSG          "Selector 'setindex' needs integer indices."

/* Initialization */
void multivectorinitialize(void);

/* Intrinsic functions */
expression *multivector_multivectori(interpretcontext *context, int nargs, expression **args);

/* Type functions */
size_t multivector_sizefortype(multivectortype type);
void multivector_zeroelement(multivectorobject *obj, unsigned int i);
int multivector_isnumericallist(expression_list *list);

/* Operations */
expression *multivector_new(multivectortype type, unsigned int dimension);
expression *multivector_clone(multivectorobject *mobj);

expression *multivector_total(interpretcontext *context, expression_objectreference **mv, unsigned int n);
expression *multivector_weightedtotal(interpretcontext *context, expression_objectreference **mv, double *weight, unsigned int n);
void multivector_weightedtotalinplace(interpretcontext *context, expression_objectreference **mv, double *weight, unsigned int n, expression_objectreference *out);
expression *multivector_adds(interpretcontext *context, multivectorobject *a, multivectorobject *b, int subtract, double scale, multivectorobject *out);
#define multivector_add(c, a, b, s, o) multivector_adds(c,a,b,s,1.0,o)

expression *multivector_projectontograde(multivectorobject *mobj, unsigned int i);
expression *multivector_getgradeaslist(multivectorobject *obj, unsigned int i);
void multivector_setgradetoexpression(multivectorobject *obj, interpretcontext *context, unsigned int i, expression *exp);

void multivector_print(multivectorobject *mobj);

void multivector_scale(multivectorobject *obj, interpretcontext *context, double scale);
expression *multivector_dual(multivectorobject *mobj, interpretcontext *context);

expression *multivector_doublelisttomultivector(interpretcontext *context, unsigned int dim, unsigned int grade, double *x, unsigned int length);
int multivector_gradetodoublelist(multivectorobject *mobj, unsigned int grade, double *x);
expression *multivector_unitmultivector(interpretcontext *context, unsigned int dim, unsigned int grade, unsigned int el);
expression *multivector_gradenormsq(multivectorobject *a, unsigned int grade);
expression *multivector_gradenorm(multivectorobject *a, unsigned int grade, double scale);
expression *multivector_subtractcomponent(interpretcontext *context, expression *m1, expression *m2);

/* Selectors */
expression *multivector_newi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_freei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_indexi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_setindexi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_printi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_clonei(object *obj, interpretcontext *context, int nargs, expression **args);

expression *multivector_addi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_subtracti(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_multiplyi(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_dividei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_doti(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_wedgei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_duali(object *obj, interpretcontext *context, int nargs, expression **args);

expression *multivector_serializei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_deserializei(object *obj, interpretcontext *context, int nargs, expression **args);
expression *multivector_tolisti(object *obj, interpretcontext *context, int nargs, expression **args);

#endif 
