/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * Experimental VR support
 */
#include "vr.h"

#ifdef USE_VR

/* Based on hellovr_opengl sample code */

#include <stdio.h>

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/glew.h>
#endif

#ifdef __APPLE__
#include <Accelerate/Accelerate.h>
#else
#include <cblas.h>
#include <clapack.h>
#endif

extern void show_redraw3d(void *ref, int setcontext, float *matrix, float *project);

/* Declare global interface entry points */
intptr_t VR_InitInternal( EVRInitError *peError, EVRApplicationType eType );
void VR_ShutdownInternal(void);
int VR_IsHmdPresent(void);
intptr_t VR_GetGenericInterface( const char *pchInterfaceVersion, EVRInitError *peError );
int VR_IsRuntimeInstalled(void);
const char * VR_GetVRInitErrorAsSymbol( EVRInitError error );
const char * VR_GetVRInitErrorAsEnglishDescription( EVRInitError error );

/* Hold VR system function pointers */
struct VR_IVRSystem_FnTable * VRsystem = NULL;
struct VR_IVRCompositor_FnTable * VRcompositor = NULL;
struct VR_IVRRenderModels_FnTable * VRrendermodels = NULL;

/* Global state variables */
int vr_initialized;
int vr_using;

uint32_t vr_renderwidth;
uint32_t vr_renderheight;

/* The current show window to render */
void *vr_windowref=NULL;

/* Frame buffer references for the lect and right eye*/
vr_framebuffer vr_leftEyeFramebuffer = {0};
vr_framebuffer vr_rightEyeFramebuffer = {0};

/* Poses for tracked devices */
TrackedDevicePose_t vr_trackeddevicepose[16];

/* Eye matrices */
vr_matrix vr_leftEye, vr_leftEyeProject;
vr_matrix vr_rightEye, vr_rightEyeProject;

/* The HMD pose */
vr_matrix vr_hmdpose;

/* Compute VR eye matrices */

void vr_eyematrix(EVREye e, vr_matrix *eye, vr_matrix *proj) {
    HmdMatrix44_t v;
    HmdMatrix34_t u;
    vr_matrix temp;
    /* Get the projection matrix for this eye */
    v = VRsystem->GetProjectionMatrix( e, 0.1, 10.0);
    vr_matrixfromopenvr44(v, proj);
    //vr_matrixprint(proj);
    
    /* Get the eye to head transform and invert it (giving the head to eye transform) */
    u=VRsystem->GetEyeToHeadTransform( e );
    vr_matrixfromopenvr34(u, &temp);
    vr_matrixinvert(&temp, eye);
    //vr_matrixprint(eye);
}


/* Initialize the VR system */
int vr_init(void) {
    EVRInitError eError;
    
    //if (vr_initialized) return TRUE;
   // printf("Initializing VR.\n");
    
    /* Check that the VR runtime is installed and that a headset is present */
    if (!VR_IsRuntimeInstalled()) {
        printf("Runtime is not installed.\n");
        return FALSE;
    }
    
    if (!VR_IsHmdPresent()) {
        printf("Headset is not present.\n");
        return FALSE;
    }
    
    /* If all is good, try to initialize VR. */
    VR_InitInternal(&eError, EVRApplicationType_VRApplication_Scene);
    
    /* Check whether an error occured */
    if (eError != EVRInitError_VRInitError_None) {
        printf("VR Error: %s\n",VR_GetVRInitErrorAsSymbol(eError));
        return FALSE;
    }
    
    /* We'll now initialize the VR function pointers */
    char fnTableName[128];

    /* System */
    sprintf(fnTableName, "FnTable:%s", IVRSystem_Version);
    VRsystem = (struct VR_IVRSystem_FnTable *)VR_GetGenericInterface(fnTableName, &eError);
    if (eError != EVRInitError_VRInitError_None) {
        printf("VR Error: Failed to initialize VR system functions. [VR_GetGenericInterface(\"%s\"): %s]", IVRSystem_Version, VR_GetVRInitErrorAsSymbol(eError));
        return FALSE;
    }
    
    /* Compositor */
    sprintf(fnTableName, "FnTable:%s", IVRCompositor_Version);
    VRcompositor = (struct VR_IVRCompositor_FnTable *)VR_GetGenericInterface(fnTableName, &eError);
    if (eError != EVRInitError_VRInitError_None) {
        printf("VR Error: Failed to initialize VR compositor functions. [VR_GetGenericInterface(\"%s\"): %s]", IVRSystem_Version, VR_GetVRInitErrorAsSymbol(eError));
        return FALSE;
    }
    
    /* Render models */
    sprintf(fnTableName, "FnTable:%s", IVRRenderModels_Version);
    VRrendermodels = (struct VR_IVRRenderModels_FnTable *)VR_GetGenericInterface(fnTableName, &eError);
    if (eError != EVRInitError_VRInitError_None) {
        printf("VR Error: Failed to initialize VR render models functions. [VR_GetGenericInterface(\"%s\"): %s]", IVRSystem_Version, VR_GetVRInitErrorAsSymbol(eError));
        return FALSE;
    }
    
    /* Render size */
    VRsystem->GetRecommendedRenderTargetSize( &vr_renderwidth, &vr_renderheight );
    //printf("Render size: (%u %u)\n", vr_renderwidth, vr_renderheight);
    
    vr_createFrameBuffer(vr_renderwidth, vr_renderheight, &vr_leftEyeFramebuffer);
    vr_createFrameBuffer(vr_renderwidth, vr_renderheight, &vr_rightEyeFramebuffer);
    
    /* Eye projection matrices */
    vr_eyematrix(EVREye_Eye_Left, &vr_leftEye, &vr_leftEyeProject);
    vr_eyematrix(EVREye_Eye_Right, &vr_rightEye, &vr_rightEyeProject);
    
    vr_initialized=TRUE;
    vr_using=TRUE;
    
    return TRUE;
}

/* Finalize the VR system */
void vr_finalize(void) {
    if (vr_initialized) {
        VR_ShutdownInternal();
        vr_using = FALSE;
    }
}

/* Framebuffer error codes */
void vr_framebuffererror(GLenum status) {
    switch (status) {
        case GL_FRAMEBUFFER_UNDEFINED:
            printf("Framebuffer undefined.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            printf("Incomplete attachment.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            printf("Incomplete missing attachment.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            printf("Incomplete draw buffer.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            printf("Incomplete read buffer.\n");
            break;
        case GL_FRAMEBUFFER_UNSUPPORTED:
            printf("Unsupported framebuffer.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
            printf("Incomplete multisample.\n");
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
            printf("Incomplete layer targets.\n");
            break;
    }
}

/* Create framebuffers. */
int vr_createFrameBuffer(int width, int height, vr_framebuffer *framebuffer) {
    GLenum status;
    
    /* Create and bind frame buffer object */
    glGenFramebuffers(1, &framebuffer->renderFramebufferId );
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->renderFramebufferId);
    
    /* Create a texture that will contain our rendered output */
    glGenTextures(1, &framebuffer->renderTextureId);
    /* Bind the newly created texture */
    glBindTexture(GL_TEXTURE_2D, framebuffer->renderTextureId);
    /* Create an empty image */
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    /* Filtering */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    
    /* Now create the depth buffer */
    glGenRenderbuffers(1, &framebuffer->depthBufferId);
    glBindRenderbuffer(GL_RENDERBUFFER, framebuffer->depthBufferId);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, framebuffer->depthBufferId);
    
    /* Set rendertexture as our color attachment */
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, framebuffer->renderTextureId, 0);
    
    /* Check status of the framebuffer */
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        vr_framebuffererror(status);
        return FALSE;
    }
    
    /* Now create the resolve framebuffer (this is what we will pass to the compositor) */
    glGenFramebuffers(1, &framebuffer->resolveFramebufferId );
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->resolveFramebufferId);
    
    /* Create the texture */
    glGenTextures(1, &framebuffer->resolveTextureId );
    glBindTexture(GL_TEXTURE_2D, framebuffer->resolveTextureId );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, framebuffer->resolveTextureId, 0);
    
    /* Check status of the framebuffer */
    status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        vr_framebuffererror(status);
        return FALSE;
    }
    
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    
    return TRUE;
}

/*vr_convertSteamVRMatrixToMatrix4(HmdMatrix34_t *m, mat4 *out) {
    
} */

void vr_loop(void) {
    EVRCompositorError cErr;
    
    /* Process events */
    struct VREvent_t vre;
    while( VRsystem->PollNextEvent(&vre, sizeof(vre)) ) {
        
    }
    
    /* Get positions of tracked objects */
    VRcompositor->WaitGetPoses(vr_trackeddevicepose, 16, NULL, 0);
    
    if (vr_trackeddevicepose[k_unTrackedDeviceIndex_Hmd].bPoseIsValid) {
        vr_matrix hmd;
        /* Convert the returned matrix into one of out 4x4 matrices */
        vr_matrixfromopenvr34(vr_trackeddevicepose[k_unTrackedDeviceIndex_Hmd].mDeviceToAbsoluteTracking, &hmd);
        /* And invert it */
        vr_matrixinvert(&hmd, &vr_hmdpose);
    }
    
    vr_matrix eye_l, eye_r;
    vr_matrixmultiply(&vr_leftEye, &vr_hmdpose, &eye_l);
    vr_matrixmultiply(&vr_rightEye, &vr_hmdpose, &eye_r);
    
    /*printf("left eye transform:\n");
    vr_matrixprint(&vr_leftEye);
    printf("pose:\n");
    vr_matrixprint(&vr_hmdpose);*/
    
    /* Now render */
    /* Left eye */
    glBindFramebuffer( GL_FRAMEBUFFER, vr_leftEyeFramebuffer.renderFramebufferId );
    glViewport(0, 0, vr_renderwidth, vr_renderheight );
    
    show_redraw3d(vr_windowref, FALSE, eye_l.f, vr_leftEyeProject.f);
    glFlush();
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    
    glBindFramebuffer(GL_READ_FRAMEBUFFER, vr_leftEyeFramebuffer.renderFramebufferId );
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, vr_leftEyeFramebuffer.resolveFramebufferId );

    glBlitFramebuffer( 0, 0, vr_renderwidth, vr_renderheight, 0, 0, vr_renderwidth, vr_renderheight,
        GL_COLOR_BUFFER_BIT,
        GL_LINEAR );

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0 );
    
    Texture_t leftEyeTexture = {(void*)(uintptr_t)vr_leftEyeFramebuffer.resolveFramebufferId, ETextureType_TextureType_OpenGL, EColorSpace_ColorSpace_Gamma};
    cErr = VRcompositor->Submit(EVREye_Eye_Left, &leftEyeTexture, NULL, EVRSubmitFlags_Submit_Default);
    
    /* Right eye */
    glBindFramebuffer( GL_FRAMEBUFFER, vr_rightEyeFramebuffer.renderFramebufferId );
    glViewport(0, 0, vr_renderwidth, vr_renderheight );
    
    show_redraw3d(vr_windowref, FALSE, eye_r.f, vr_rightEyeProject.f);
    glFlush();
    glBindFramebuffer( GL_FRAMEBUFFER, 0 );
    
    glBindFramebuffer(GL_READ_FRAMEBUFFER, vr_rightEyeFramebuffer.renderFramebufferId );
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, vr_rightEyeFramebuffer.resolveFramebufferId );

    glBlitFramebuffer( 0, 0, vr_renderwidth, vr_renderheight, 0, 0, vr_renderwidth, vr_renderheight,
        GL_COLOR_BUFFER_BIT,
        GL_LINEAR );

    glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0 );

    /* Submit to compositor */
    Texture_t rightEyeTexture = {(void*)(uintptr_t)vr_rightEyeFramebuffer.resolveFramebufferId, ETextureType_TextureType_OpenGL, EColorSpace_ColorSpace_Gamma};
    cErr = VRcompositor->Submit(EVREye_Eye_Right, &rightEyeTexture, NULL, EVRSubmitFlags_Submit_Default);
}

int vr_isActive(void) {
    return vr_using;
}

/* 4x4 matrix math */

void vr_matrixprint(vr_matrix *a) {
    for (unsigned int j=0; j<4; j++) { // row
        for (unsigned int i=0; i<4; i++) { // column
            printf("%g ",a->f[i*4+j]);
        }
        printf("\n");
    }
}

void vr_matrixfromopenvr44( HmdMatrix44_t in, vr_matrix *out) {
    for (unsigned int i=0; i<4; i++) {
        for (unsigned int j=0; j<4; j++) {
            out->f[j*4+i]=in.m[i][j];
        }
    }
}

void vr_matrixfromopenvr34( HmdMatrix34_t in, vr_matrix *out) {
    for (unsigned int i=0; i<3; i++) {
        for (unsigned int j=0; j<4; j++) {
            out->f[j*4+i]=in.m[i][j];
            
        }
    }
    out->f[3]=0.0; out->f[7]=0.0; out->f[11]=0.0; out->f[15]=1.0;
}

void vr_matrixmultiply(vr_matrix *a, vr_matrix *b, vr_matrix *out) {
    cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, 4, 4, 4, 1.0, a->f, 4, b->f, 4, 0.0, out->f, 4);
}

void vr_matrixinvert(vr_matrix *a, vr_matrix *out) {
    int m = 4, n = 4;
    int piv[4];
    int info, lwork=16;
    float work[16];
    /* Copy a into out */
    memcpy(out->f, a->f, sizeof(float)*16);
    /* Compute LU decomposition, storing result in place */
    sgetrf_(&m, &n, out->f, &m, piv, &info);
    
    if (!info) {
        /* Now compute inverse */
        sgetri_(&n, out->f, &n, piv, work, &lwork, &info);
    }
}

void vr_setref(void *ref) {
    vr_windowref = ref;
}

#endif
