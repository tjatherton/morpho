/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef vr_h
#define vr_h

#include "cli.h"

#ifdef USE_VR

#include <stdio.h>
#include "eval.h"
#include <openvr/openvr_capi.h>

int vr_init(void);
void vr_finalize(void);
void vr_loop(void);
int vr_isActive(void);
void vr_setref(void *ref);

typedef struct {
    GLuint depthBufferId;
    GLuint renderTextureId;
    GLuint renderFramebufferId;
    GLuint resolveTextureId;
    GLuint resolveFramebufferId;
} vr_framebuffer;

int vr_createFrameBuffer(int width, int height, vr_framebuffer * framebuffer);

typedef struct {
    float f[16];
} vr_matrix;

void vr_matrixprint(vr_matrix *a);
void vr_matrixfromopenvr44( HmdMatrix44_t in, vr_matrix *out);
void vr_matrixfromopenvr34( HmdMatrix34_t in, vr_matrix *out);
void vr_matrixmultiply(vr_matrix *a, vr_matrix *b, vr_matrix *out);
void vr_matrixinvert(vr_matrix *a, vr_matrix *out);

#endif

#endif
