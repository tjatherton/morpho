/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * operators.h - Implements intrinsic functions.
 */

#ifndef Morpho_operators_h
#define Morpho_operators_h

#include <stdio.h>

#include "eval.h"

/* Morpho operator symbols */
#define OPERATOR_SEMICOLON ";"
#define OPERATOR_ASSIGN "="

#define OPERATOR_AND "&&"
#define OPERATOR_OR "||"
#define OPERATOR_NOT "!"

#define OPERATOR_ISEQUAL "=="
#define OPERATOR_ISNOTEQUAL "!="
#define OPERATOR_ISLESSTHAN "<"
#define OPERATOR_ISLESSTHANOREQUAL "<="
#define OPERATOR_ISGREATERTHAN ">"
#define OPERATOR_ISGREATERTHANOREQUAL ">="

#define OPERATOR_ADD "+"
#define OPERATOR_SUBTRACT "-"
#define OPERATOR_MULTIPLY "*"
#define OPERATOR_DIVIDE "/"

#define OPERATOR_STARTLIST "{"
#define OPERATOR_ENDLIST "}"

#define OPERATOR_LEFTBRACKET "("
#define OPERATOR_ENDBRACKET ")"

#define OPERATOR_STARTINDEX "["
#define OPERATOR_ENDINDEX "]"

#define OPERATOR_POWER "^"
#define OPERATOR_POWER_FORTRAN "**"

#define OPERATOR_CLASS "@"
#define OPERATOR_BREAKPOINT "##"
#define OPERATOR_COMMA ","

/* Operator -> selector names */
#define OPERATOR_ASSIGN_SELECTOR "assign"

#define OPERATOR_AND_SELECTOR "and"
#define OPERATOR_OR_SELECTOR "or"
#define OPERATOR_NOT_SELECTOR "not"

#define OPERATOR_ISEQUAL_SELECTOR "iseq"
#define OPERATOR_ISNOTEQUAL_SELECTOR "isneq"
#define OPERATOR_ISLESSTHAN_SELECTOR "islt"
#define OPERATOR_ISLESSTHANOREQUAL_SELECTOR "islteq"
#define OPERATOR_ISGREATERTHAN_SELECTOR "isgt"
#define OPERATOR_ISGREATERTHANOREQUAL_SELECTOR "isgteq"

#define OPERATOR_ADD_SELECTOR "add"
#define OPERATOR_SUBTRACT_SELECTOR "subtract"
#define OPERATOR_MULTIPLY_SELECTOR "multiply"
#define OPERATOR_DIVIDE_SELECTOR "divide"

#define OPERATOR_DOT_SELECTOR "dot"

#define OPERATOR_POWER_SELECTOR "power"

extern hashtable *operators;

operator *operator_infix(char *op, int precedence, nudtype *nud, ledtype *led, executeoptype *executeop, diffexpfunctype *diff, char *selector);
operator *operator_prefix(char *op, nudtype *nud, executeoptype *executeop, diffexpfunctype *diff, char *selector);
operator *operator_symbol(char *op);

operator *operator_find(char *label);

expression *opassign(interpretcontext *context, expression *left, expression *right);
expression *opeq(interpretcontext *context, expression *left, expression *right);
expression *oplessthan(interpretcontext *context, expression *left, expression *right);
expression *opgreaterthan(interpretcontext *context, expression *left, expression *right);
expression *opadd(interpretcontext *context, expression *left, expression *right);
expression *opsub(interpretcontext *context, expression *left, expression *right);
expression *opmul(interpretcontext *context, expression *left, expression *right);
expression *opdiv(interpretcontext *context, expression *left, expression *right);

void evalinitializeoperators(void);
void evalfinalizeoperators(void);

#endif
