/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "constructors.h"
#include "io.h"

/*
 * Load a mesh file
 */

/* Load a mesh file */
/* TODO: Make this work with nonsequentially ordered elements */
expression *geometry_manifoldloadi(interpretcontext *context, int nargs, expression **args) {
    enum {MANIFOLD_LOAD_NONE, MANIFOLD_LOAD_VERTICES, MANIFOLD_LOAD_EDGES, MANIFOLD_LOAD_FACES, MANIFOLD_LOAD_VOLUMES} mode=MANIFOLD_LOAD_NONE;
    expression_objectreference *newbody = NULL;
    expression_objectreference *file=NULL;
    expression_objectreference *mref=NULL;
    manifold_object *m=NULL;
    expression *line=NULL;
    expression_list *linelist=NULL;
    interpretcontext *newcontext=newinterpretcontext(NULL, 1); /* An interpretcontext unconnected to ours for security reasons */
    unsigned int i;
    
    /* Check arguments */
    if (nargs!=1) return NULL;
    if (!eval_isstring(args[0])) return NULL;
    
    /* Open the stream */
    file=io_open(context, args[0]);
    
    if (file) {
        mref=(expression_objectreference *) manifold_new();
        if (eval_isobjectref(mref)) {
            m=(manifold_object *) mref->obj;
            
            while (!io_eof(context, file)) {
                line=io_read(context, file);
                
                if (eval_isstring(line)) {
                    /* Process commands */
                    if(strncmp(eval_stringvalue(line), GEOMETRY_MESHFILE_VERTICES, strlen(GEOMETRY_MESHFILE_VERTICES))==0) {
                            mode = MANIFOLD_LOAD_VERTICES;
                    } else if(strncmp(eval_stringvalue(line), GEOMETRY_MESHFILE_EDGES, strlen(GEOMETRY_MESHFILE_EDGES))==0) {
                            mode = MANIFOLD_LOAD_EDGES;
                    } else if(strncmp(eval_stringvalue(line), GEOMETRY_MESHFILE_FACES, strlen(GEOMETRY_MESHFILE_FACES))==0) {
                            mode = MANIFOLD_LOAD_FACES;
                    } else if(strncmp(eval_stringvalue(line), GEOMETRY_MESHFILE_VOLUMES, strlen(GEOMETRY_MESHFILE_VOLUMES))==0) {
                        mode = MANIFOLD_LOAD_VOLUMES;
                    } else if (mode!=MANIFOLD_LOAD_NONE) {
                        linelist=(expression_list *) eval_stringsplit(context, 1, &line);
                        
                        if (eval_islist(linelist)) {
                            unsigned int length=eval_listlength(linelist);
                            expression *lineexp[length];
                            for (i=0; i<length; i++) lineexp[i]=NULL; /* Make sure the expressions are properly zero'd */
                            
                            /* Use the interpreter to interpret the elements of the line */
                            i=0;
                            for (linkedlistentry *e=linelist->list->first; e!=NULL && i<length; e=e->next) {
                                expression_string *s=(expression_string *) e->data;
                                expression *u;
                                if (eval_isstring(s)) {
                                    u=parse(s->value, NULL, NULL);
                                    lineexp[i]=interpretexpression(newcontext, u);
                                    freeexpression(u);
                                }
                                i++;
                            }
                            
                            switch (mode) {
                                case MANIFOLD_LOAD_VERTICES:
                                    if (length>1) {
                                        MFLD_DBL p[length-1];
                                        if (eval_isinteger(lineexp[0])) {
                                            for (i=1; i<length; i++) if (eval_isnumerical(lineexp[i])) p[i-1]=eval_floatvalue(lineexp[i]);
                                            manifold_addvertex(m, context, length-1, p, MANIFOLD_VERTEX_NONE);
                                        }
                                        /* TODO: Make an idtable so that we can go from ids in the file to uids here */
                                    }
                                    break;
                                case MANIFOLD_LOAD_EDGES:
                                    if (length==3) {
                                        manifold_gradelowerentry el;
                                        
                                        if (eval_isinteger(lineexp[1]) && eval_isinteger(lineexp[2])) {
                                            el.el[0]=eval_integervalue(lineexp[1]);
                                            el.el[1]=eval_integervalue(lineexp[2]);
                                            
                                            manifold_addelement(m, context, 1, &el);
                                        }
                                    }
                                    break;
                                case MANIFOLD_LOAD_FACES:
                                    if (length==4) {
                                        uid p[3], s1,s2;
                                        manifold_gradelowerentry el;
                                        
                                        /* Find the three vertex ids */
                                        for (i=1; i<4; i++) {
                                            p[i-1]=(uid) eval_integervalue(lineexp[i]);
                                        }
                                        
                                        s1=manifold_findlinearelement(m, p[0], p[1]);
                                        s2=manifold_findlinearelement(m, p[1], p[2]);
                                        
                                        if (s1 && s2) {
                                            el.el[0]=s1;
                                            el.el[1]=s2;
                                            
                                            manifold_addelement(m, context, 2, &el);
                                        } else {
                                            printf("Error loading element %u of grade 2.\n",eval_integervalue(lineexp[0]));
                                        }
                                        
                                        /* To do: should check sign! */
                                    }

                                    break;
                                case MANIFOLD_LOAD_VOLUMES:
                                    if (length==5) {
                                        uid p[4], s1=0,s2=0;
                                        manifold_gradelowerentry el;
                                        
                                        /* Find the four vertex ids */
                                        for (i=1; i<5; i++) {
                                            p[i-1]=(uid) eval_integervalue(lineexp[i]);
                                        }
                                                                               
                                        s1 = manifold_findelementfromvertices(m, 3, p);
                                        s2 = manifold_findlinearelement(m, p[0], p[3]);
                                        
                                        if (s1 && s2) {
                                            el.el[0]=s1;
                                            el.el[1]=s2;
                                            manifold_addelement(m, context, 3, &el);
                                        } else {
                                            printf("Error loading element %u of grade 3.\n",eval_integervalue(lineexp[0]));
                                        }
                                        
                                        /* To do: should check sign! */
                                    }
                                    break;
                                case MANIFOLD_LOAD_NONE:
                                    break;
                            }

                            /* Free the interpreted expressions */
                            for (i=0; i<length; i++) freeexpression(lineexp[i]);
                        }
                        
                        freeexpression((expression *) linelist);
                    }
                }
                
                freeexpression(line);
            }
        
        }
        
        // manifold_orient(m, context); // Orient the manifold
        
        /* Enclose the manifold in a body */
        newbody=(expression_objectreference *) body_new();
        body_object *body = NULL;
        
        if (eval_isobjectref(newbody)) {
            body=(body_object *) newbody->obj;
            
            body_addmanifold(body, m);
        } else goto geometry_manifoldloadi_clearup;
        
        /* Lose our temporary reference to the manifold */
        if (mref) freeexpression((expression *) mref);
    } else {
        error_raise(context, ERROR_GEOMETRY_COULDNTOPENFILE, ERROR_GEOMETRY_COULDNTOPENFILE_MSG, ERROR_FATAL);
    }
    
geometry_manifoldloadi_clearup:
    
    if (file) {
        /* Close the stream */
        io_close(context, file);
        
        /* Remove the file reference */
        freeexpression((expression *) file);
    }

    if (newcontext) freeinterpretcontext(newcontext);
    
    return (expression *) newbody;
}

/*
 * Parametric line
 */

/* Construct a line */
expression *geometry_parametriclinei(interpretcontext *context, int nargs, expression **args) {
    genericcountertype *counter=NULL;
    interpretcontext *local_context=NULL;
    interpretcontext *options=NULL;
    expression_objectreference *new =NULL;
    expression_objectreference *newbody = NULL;
    manifold_gradelowerentry line_element;
    expression *res=NULL;
    expression *closed=NULL;
    unsigned int n=0;
    unsigned int done=FALSE;
    unsigned int dim=0;
    
    
    /* Check arguments */
    if (nargs<2) return NULL;
    if (!eval_islist(args[0])) return NULL;
    counter=eval_listtocounter(context, (expression_list *) args[1], FALSE);
    
    dim=eval_listlength((expression_list *) args[0]); /* Extract dimensionality of list */
    double x[dim*counter->nsteps]; /* A place to store all the vertices */
    
    options=eval_options(context,3,nargs,args);
    closed=lookupsymbol(options, GEOMETRY_LINEMANIFOLD_CLOSEDOPTION);
    
    local_context = newinterpretcontext(context, 1);
    if (!local_context) goto geometry_parametric_clearup;
    
    eval_counterreset(counter);
    eval_counterstore(local_context,counter);
    
    do {
        res=interpretexpression(local_context, args[0]);
        if (eval_islist(res)) {
            if (!eval_listtodoublearray((expression_list *) res, x+dim*n, dim)) {
                error_raise(context, ERROR_GEOMETRY_NONNUMERICAL, ERROR_GEOMETRY_NONNUMERICAL_MSG, ERROR_FATAL);
            }
        } else goto geometry_parametric_clearup;
        if (res) { freeexpression(res); res=NULL;};
        
        eval_counteradvance(counter);
        eval_counterstore(local_context, counter);
        n++;
    } while (n<counter->nsteps);
    
    /* Now convert these into a manifold */
    new=(expression_objectreference *) manifold_new();
    manifold_object *manifold = NULL;
    
    if (eval_isobjectref(new)) {
        manifold=(manifold_object *) new->obj;
    } else goto geometry_parametric_clearup;
    
    uid first, last, this;
    
    for (unsigned int i=0; i<n; i++) {
        this=manifold_addvertex(manifold, context, dim, x+dim*i, MANIFOLD_VERTEX_NONE);
        if (i==0) {
            first=this;
        } else {
            line_element.el[0]=last; line_element.el[1]=this;
            manifold_addelement(manifold, context, MANIFOLD_GRADE_LINE, &line_element);
        }
        last=this;
    }

    /* Check if we should close the manifold */
    if (closed) if (eval_isbool(closed)) if (eval_boolvalue(closed)) {
        line_element.el[0]=this; line_element.el[1]=first;
        manifold_addelement(manifold, context, MANIFOLD_GRADE_LINE, &line_element);
    }
    
    /* Enclose the manifold in a body */
    newbody=(expression_objectreference *) body_new();
    body_object *body = NULL;
    
    if (eval_isobjectref(newbody)) {
        body=(body_object *) newbody->obj;
        
        body_addmanifold(body, manifold); 
    } else goto geometry_parametric_clearup;
    
    done=TRUE;
    
geometry_parametric_clearup:
    
    if (res) freeexpression(res);
    if (new) freeexpression((expression *) new);
    if (counter) EVAL_FREE(counter);
    if (local_context) freeinterpretcontext(local_context);
    if (!done) EVAL_FREE(new);
    if (options) freeinterpretcontext(options);
    if (closed) freeexpression(closed); 
    
    return (expression *) newbody;
}


/*
 * List of points
 */

/* Construct a manifold from a list of points */
expression *geometry_manifoldpointsi(interpretcontext *context, int nargs, expression **args) {
    expression_objectreference *objref=NULL;
    manifold_object *mobj=NULL;
    expression_objectreference *newbody=NULL;
    expression_list *list;
    unsigned int i;
    
    /* Check arguments */
    if (nargs!=1) return NULL;
    if (!eval_islist(args[0])) return NULL;
    list = (expression_list *) args[0];
    
    objref=(expression_objectreference *) manifold_new();
    if (objref) {
        mobj=(manifold_object *) objref->obj;
        
        if (mobj) {
            
            i=0;
            for (linkedlistentry *e=list->list->first; e!=NULL; e=e->next) {
                expression_list *lexp=(expression_list *) e->data;
                
                if (eval_islist(lexp)) {
                    unsigned int length = eval_listlength(lexp);
                    double x[length];
                    
                    eval_listtodoublearray(lexp, x, length);
                    manifold_addvertex(mobj, context, length, x, MANIFOLD_VERTEX_NONE);
                    
                    i++;
                } else {
                    error_raise(context, ERROR_GEOMETRY_POINTLIST, ERROR_GEOMETRY_POINTLIST_MSG, ERROR_FATAL);
                }
            }
        }
        
        /* Enclose the manifold in a body */
        newbody=(expression_objectreference *) body_new();
        body_object *body = NULL;
        
        if (eval_isobjectref(newbody)) {
            body=(body_object *) newbody->obj;
            
            body_addmanifold(body, mobj);
        } else {
            freeexpression((expression *) objref);
        }
    }
    
    return (expression *) newbody;
}

/*
 * Delaunay triangulation
 */

/* Delaunay triangulation */
expression *geometry_manifoldtriangulatei(interpretcontext *context, int nargs, expression **args) {
    
    return NULL;
}

/*
 * Cuboids
 */

/* Creates the face of a cuboid. 
   Input:-
        manifold_object *manifold - The manifold object that is being constructed
        interpretcontext *context - The current interpretcontext
        uid *v                    - List of vertex ids
        unsigned int *fc          - Indices of the four vertices constituting the face 
 */
void geometry_manifoldcuboidface(manifold_object *manifold, interpretcontext *context, uid *v, unsigned int *vind) {
    manifold_gradelowerentry edge;
    manifold_gradelowerentry face;
    uid vcenter, ne[4];
    unsigned int fc[4];
    MFLD_DBL xx[manifold->dimension], xv[manifold->dimension];
    
    /* Ensure that the vertex ids form a cycle */
    fc[0]=vind[0];
    for (unsigned i=1; i<4; i++) {
        for (unsigned int j=1; j<4; j++) {
            /* Find an element that isn't already in the list... */
            if (! manifold_idlistposition(fc, i, vind[j], NULL)) {
                /* ... and is connected to the previous vertex. */
                if (manifold_findlinearelement(manifold, v[fc[i-1]], v[vind[j]])) {
                    fc[i]=vind[j];
                    break;
                }
            }
        }
    }
    
    /* Calculate the center of the face from the average of the four vertex positions */
    for (unsigned int i=0; i<manifold->dimension; i++) xx[i]=0.0;
    for (unsigned int j=0; j<4; j++) {
        manifold_getvertexposition(manifold, context, v[fc[j]], xv);
        for (unsigned int i=0; i<manifold->dimension; i++) xx[i]+=xv[i];
    }
    for (unsigned int i=0; i<manifold->dimension; i++) xx[i]*=0.25;
    
    /* Add the new vertex */
    vcenter=manifold_addvertex(manifold, context, manifold->dimension, xx, MANIFOLD_VERTEX_NONE);
    
    /* Add the edge connecting the vertex to the center */
    for (unsigned int j=0; j<4; j++) {
        edge.el[0]=vcenter; edge.el[1]=v[fc[j]];
        ne[j]=manifold_addelement(manifold, context, MANIFOLD_GRADE_LINE, &edge);
    }
    
    /* Add the faces */
    for (unsigned int j=0; j<4; j++) {
        face.el[0]=ne[j]; face.el[1]=ne[(j<3 ? j+1 : 0)];
        manifold_addelement(manifold, context, MANIFOLD_GRADE_AREA, &face);
    }
    
    /* TODO: Make sure the face orientations are correct */
}

/* Recursively create all the boundary edges for a cuboid. [Dimensionally independent] 
   Input:- 
     manifold_object *manifold - the manifold object
     interpretcontext *context - the current interpretcontext
     uid *v                    - the uids of the vertices to link
     unsigned int length       - number of vertices in the list to link
     unsigned int d1           - offset to index v by.
   Output:-
     uid *e                    - uid of edges added
     unsigned int *ne          - number of edges added
*/
void geometry_manifoldcuboidlinkvwithdim(manifold_object *manifold, interpretcontext *context, uid *v, unsigned int length, unsigned int d1, uid *e, unsigned int *ne) {
    manifold_gradelowerentry edge;
    
    if (length>2) {
        /* At each stage of recursion, we have an number of vertices equal to 2^N. */
        unsigned int hlength = length/2;
        
        /* We divide these into two pieces and link each of them internally. */
        geometry_manifoldcuboidlinkvwithdim(manifold, context, v, hlength, d1, e, ne);
        geometry_manifoldcuboidlinkvwithdim(manifold, context, v, hlength, d1+hlength, e, ne);
        
        /* We now make edges that link between the two halves. */
        for (unsigned int i=0; i<hlength; i++) {
            edge.el[0]=v[d1+i]; edge.el[1]=v[d1+i+hlength];
            e[*ne]=manifold_addelement(manifold, context, MANIFOLD_GRADE_LINE, &edge);
            (*ne)++;
        }
    } else {
        /* At some point only two vertices are left. So we'll just create a single new edge */
        edge.el[0]=v[d1]; edge.el[1]=v[d1+1];
        e[*ne]=manifold_addelement(manifold, context, MANIFOLD_GRADE_LINE, &edge);
        (*ne)++;
    }
}

/* Construct a generalized n-dimensional cuboid. */
expression *geometry_manifoldcuboidi(interpretcontext *context, int nargs, expression **args) {
    expression_objectreference *objref=NULL;
    manifold_object *mobj=NULL;
    expression_objectreference *newbody=NULL;
    unsigned int dim=0, np=0;

    if (nargs==2) {
        expression_list *x1 = (expression_list *) args[0];
        expression_list *x2 = (expression_list *) args[1];
        
        if (eval_islist(x1) && eval_islist(x2)) {
            dim=eval_listlength(x1);
            np=1<<dim;
            
            MFLD_DBL x1d[dim], x2d[dim], xx[dim];
            uid vid[np], eid[(np*dim)/2];
            
            if (eval_listlength(x2)!=dim) error_raise(context, ERROR_GEOMETRY_POINTDIM, ERROR_GEOMETRY_POINTDIM_MSG, ERROR_FATAL);
            
            /* Read the lists into an array */
            if (eval_listtodoublearray(x1, x1d, dim) && eval_listtodoublearray(x2, x2d, dim)) {
                objref=(expression_objectreference *) manifold_new();
                
                if (eval_isobjectref(objref)) {
                    mobj=(manifold_object *) objref->obj;
                    
                    /* Add the points. The vertices are added using the elements of x1 and x2. 
                       e.g. for 2D: (x1[0] x1[1]) , (x2[0] x1[1]), (x1[0] x2[1]), (x2[0] x2[1]).
                       Note that choice of 1 or 2 can be made from the binary digits of i in increasing order:
                       00 01 10 11 -> 11 21 12 22 */
                    for (unsigned int i=0; i<np; i++) {
                        for (unsigned int j=0; j<dim; j++) {
                            /* Binary shift used to extract relevant bit from i */
                            if (1<<j & i) xx[j]=x2d[j];
                            else xx[j]=x1d[j];
                        }
                        
                        /* Add vertex */
                        vid[i]=manifold_addvertex(mobj, context, dim, xx, MANIFOLD_VERTEX_NONE);
                    }
                    
                    /* Add the edges */
                    unsigned nedge=0;
                    geometry_manifoldcuboidlinkvwithdim(mobj, context, vid, np, 0, eid, &nedge);
                    
                    /* Add the faces */
                    uid face[4];
                    for (unsigned int i=0; i<np/4; i++) {
                        face[0]=4*i+0; face[1]=4*i+1; face[2]=4*i+2; face[3]=4*i+3;
                        geometry_manifoldcuboidface(mobj, context, vid, face);
                    }
                    for (unsigned int i=0; i<np/8; i++) {
                        for (unsigned int j=0; j<3; j+=2) {
                            face[0]=4*i+0+j; face[1]=4*i+1+j; face[2]=4*i+4+j; face[3]=4*i+5+j;
                            geometry_manifoldcuboidface(mobj, context, vid, face);
                        }
                        
                        for (unsigned int j=0; j<2; j++) {
                            face[0]=4*i+0+j; face[1]=4*i+2+j; face[2]=4*i+4+j; face[3]=4*i+6+j;
                            geometry_manifoldcuboidface(mobj, context, vid, face);
                        }
                    }
                }
            } else {
                error_raise(context, ERROR_GEOMETRY_POINTNUM, ERROR_GEOMETRY_POINTNUM_MSG, ERROR_FATAL);
            }
        }
        
        /* Orient the cuboid */
        manifold_orient(mobj, context);
        
        /* Enclose the manifold in a body */
        newbody=(expression_objectreference *) body_new();
        body_object *body = NULL;
        
        if (eval_isobjectref(newbody)) {
            body=(body_object *) newbody->obj;
            
            body_addmanifold(body, mobj);
        }
    }
    
    /* Get rid of the object reference */
    if (objref) freeexpression((expression *) objref);
    
    return (expression *) newbody;
}


/* ------------------------------------------------------------------------------------------------
 * Marching method for implicit surfaces
 * described in E. Hartmann, The Visual Computer (1998) 14:95-108
 * ---------------------------------------------------------------------------------------------- */

#define GEOMETRY_MMMAXITERATIONS 100

typedef struct {
    union {
        constructors_mmpoint *pt;
        unsigned int *uint;
        void *vd;
    } contents; /* The contents */
    size_t entrysize; /* Size of each buffer */
    unsigned int max; /* Size of the buffer */
    unsigned int n; /* Current number of entries */
} constructors_mmbuffer;

/* Polygon primitive used by the marching method */
typedef struct {
    int active; /* Is the polygon active or not? E.g. if all vertices are on a boundary, it ceases to be active */
    constructors_mmbuffer *buffer; /* Buffer containing the polygon */
} constructors_mmpolygon;

/* Attempts to find the closest point on a surface specified by a scalar function
 Input:   double *x0   - the initial point
          constructors_mmfunc *func - a function tha evaluates the scalar function and gradient
          void *ref    - data to pass to func
          double *tol   - convergence criterion for halting (separation of adjacent points) (optional)
 Output:  constructors_mmpoint *out - the point record is updated with point, normal and tangent vectors
 */
void constructors_mmsurfacepoint(interpretcontext *context, double *x0, constructors_mmfunc *func, void *ref, double *tol, constructors_mmpoint *out ) {
    double u[3]={x0[0], x0[1], x0[2]}; /* The current point */
    double gradf[3]={0,0,0};
    double f=0.0,df=0.0,sep=0.0;
    double eps = (tol ? *tol : MACHINE_EPSILON);
    unsigned int iter=0;
    
    do {
        /* Calculate the function and gradient */
        f = (*func) (u, ref, gradf);
        
        /* |grad f|^2 */
        df=0.0;
        for (unsigned int i=0; i<3; i++) df+=gradf[i]*gradf[i];
        
        /* Check for zero gradient */
        if (fabs(df)<eps) {
            error_raise(context, ERROR_CONSTRUCTOR_ZEROGRADIENT, ERROR_CONSTRUCTOR_ZEROGRADIENT_MSG, ERROR_FATAL);
        }
        
        sep=0.0; /* This will hold the separation of the two points */
        for (unsigned int i=0; i<3; i++) {
            double s = f/df * gradf[i];
            u[i] -= s;
            sep+=s*s;
        }
        
        iter++;
    } while (sep>eps && iter<GEOMETRY_MMMAXITERATIONS);
    
    if (iter>=GEOMETRY_MMMAXITERATIONS) {
        error_raise(context, ERROR_CONSTRUCTOR_MAXITER, ERROR_CONSTRUCTOR_MAXITER_MSG, ERROR_WARNING);
    }
    
    /* Update the point record with normal and tangent vectors */
    if (out) {
        for (unsigned int i=0; i<3; i++) {
            out->pt[i]=u[i];
            out->normal[i]=gradf[i];
        }
        manifold_normalizevector3d(out->normal, out->normal);
        /* Select t1 based on which components are large */
        if (fabs(out->normal[0])>0.5 || fabs(out->normal[1])>0.5) {
            out->t1[0] = out->normal[1];
            out->t1[1] = -out->normal[0];
            out->t1[2] = 0.0;
        } else {
            out->t1[0] = -out->normal[2];
            out->t1[1] = 0.0;
            out->t1[2] = -out->normal[0];
        }
        manifold_normalizevector3d(out->t1, out->t1);
        
        /* Construct t2 from cross product */
        manifold_crossproduct3d(out->normal, out->t1, out->t2);
        manifold_normalizevector3d(out->t2, out->t2);
    }
}

#define CONSTRUCTORS_DEFAULTBUFFERSIZE 16

/* Creates a buffer
 Input: size_t entrysize        - Size of each entry
        unsigned int *max       - [Optional] the size of each entry
 Returns: The created buffer */
constructors_mmbuffer *constructors_mmnewbuffer(size_t entrysize, unsigned int *max) {
    constructors_mmbuffer *buffer = EVAL_MALLOC(sizeof(constructors_mmbuffer));
    unsigned int size=( max && *max>CONSTRUCTORS_DEFAULTBUFFERSIZE ? *max : CONSTRUCTORS_DEFAULTBUFFERSIZE);
    
    if (buffer) {
        buffer->entrysize=entrysize;
        buffer->contents.vd=EVAL_MALLOC(entrysize*size);
        if (!buffer->contents.vd) {
            EVAL_FREE(buffer);
            return NULL;
        }
        buffer->max=size;
        buffer->n=0;
    }
    
    return buffer;
}

/* Frees a buffer */
void constructors_mmfreebuffer(constructors_mmbuffer *buffer) {
    if (buffer) {
        if (buffer->contents.vd) EVAL_FREE(buffer->contents.vd);
        EVAL_FREE(buffer);
    }
}

/* Resize a buffer if necessary.
Input: constructors_mmbuffer *buffer - Buffer to resize
       unsigned int n                - Required number of entries
Returns: TRUE on success, FALSE otherwise
*/
int constructors_mmresizebuffer(constructors_mmbuffer *buffer, unsigned int n) {
    unsigned int growto=buffer->max;
    while (growto<n) growto*=2; /* Make sure we make the buffer bigger than n */
    
    if (n>=buffer->max) {
        buffer->contents.vd=EVAL_REALLOC(buffer->contents.vd, buffer->entrysize*growto); /* Grow by factor of two each time */
        
        if (buffer->contents.vd) {
            buffer->max=growto;
        } else return FALSE;
    }
    
    return TRUE;
}

/* Create a new polygon, or return NULL if allocation failed */
constructors_mmpolygon *constructors_mmnewpolygon (unsigned int size) {
    constructors_mmpolygon *new = EVAL_MALLOC(sizeof(constructors_mmpolygon));
    if (new) {
        new->active=TRUE;
        new->buffer=constructors_mmnewbuffer(sizeof(unsigned int), &size);
    }
    return new;
}

/* Free a polygon */
void constructors_mmfreepolygon(constructors_mmpolygon *p) {
    if (p) {
        if (p->buffer) constructors_mmfreebuffer(p->buffer);
        EVAL_FREE(p);
    }
}

/* Add a vertex */
int constructors_mmaddvertex(constructors_mmbuffer *vertices, double *x, unsigned int *indx) {
    int success=FALSE;
    unsigned int n=vertices->n;
    if (constructors_mmresizebuffer(vertices, n+1)) {
        memcpy(vertices->contents.pt[n].pt, x, sizeof(double)*3);
        vertices->contents.pt[n].angle_changed=FALSE;
        vertices->contents.pt[n].border_point=FALSE;
        vertices->contents.pt[n].exclude=FALSE;
        vertices->contents.pt[n].frontangle=0.0;
        if (indx) *indx = n;
        vertices->n++;
    }
    
    return success;
}

/* Add a triangle */
int constructors_mmaddtriangle(constructors_mmbuffer *triangles, unsigned int tri[3]) {
    int success=FALSE;
    unsigned int n=triangles->n;
    
    if (constructors_mmresizebuffer(triangles, n+1)) {
        memcpy(triangles->contents.uint+3*n, tri, sizeof(unsigned int)*3);
        triangles->n++;
    }
    
    return success;
}

/* Delete and insert vertices from a front polygon
   Input: constructors_mmpolygon *poly - the polygon
          unsigned int i               - Index to modify
          unsigned int ndel            - Number of vertices to delete at i
          unsigned int nins            - Number of vertices to insert at i
          unsigned int *v              - Vertices to insert
   Returns: TRUE on success, otherwise FALSE
 */
int constructors_mmmodifypolygon(constructors_mmpolygon *poly, unsigned int i, unsigned int ndel, unsigned int nins, unsigned int *v) {
    constructors_mmbuffer *buffer= poly->buffer;
    int expandby = nins - ndel;
    
    /* Resize the buffer if necessary */
    if (expandby>0 && buffer->n+expandby>buffer->max) {
        if (!constructors_mmresizebuffer(buffer, buffer->n+expandby)) return FALSE;
    }
    
    /* Move the retained vertices */
    unsigned int nmove = buffer->n - i - ndel;
    unsigned int idest = i+nins;
    unsigned int isrc = i+ndel;
    memmove(buffer->contents.uint+idest, buffer->contents.uint+isrc, sizeof(unsigned int)*nmove);
    
    /* Now copy the inserted vertices into the buffer if any are supplied */
    if (v && nins>0) memcpy(buffer->contents.uint+i, v, sizeof(unsigned int)*nins);
    
    buffer->n+=expandby;
    
    return TRUE;
}

/* Split a polygon,
  Input: constructors_mmpolygon *poly - the polygon
         unsigned int i               - } indices i..j are put into a new polygon, i and j are retained
         unsigned int j               - } j must be bigger than i.
  Returns: (constructors_mmpolygon *) a new polygon
*/
constructors_mmpolygon *constructors_mmsplitpolygon(constructors_mmpolygon *poly, unsigned int i, unsigned int j) {
    constructors_mmpolygon *new=NULL;
    unsigned int size = j-i+1;
    
    /*for (unsigned int k=0; k<poly->buffer->n; k++) printf("%s %u ", (k==i || k==i+1 || k==j || k==j+1 ? "*": ""), poly->buffer->contents.uint[k]);
    printf("\n"); */
    
    new=constructors_mmnewpolygon(size);
    
    if (new) {
        /* Copy vertices into the new polygon */
        memcpy(new->buffer->contents.uint, poly->buffer->contents.uint+i, size*sizeof(unsigned int));
        new->buffer->n=size;
        /* Delete vertices from the old polygon */
        constructors_mmmodifypolygon(poly, i+1, size-2, 0, NULL);
    }
    
    /*for (unsigned int k=0; k<poly->buffer->n; k++) printf("%s %u ", (k==i || k==i+1? "*": ""), poly->buffer->contents.uint[k]);
    printf("\n");
    
    for (unsigned int k=0; k<new->buffer->n; k++) printf("%u ", new->buffer->contents.uint[k]);
    printf("\n");*/
    
    return new;
}

/* Merges two polygons,
  Input: constructors_mmpolygon *poly  - the front polygon
         unsigned int i                - index in front polygon
         constructors_mmpolygon *poly2 - the secondary polygon
         unsigned int i                - index into secondary polygon
  Returns: TRUE on success
*/
int constructors_mmmergepolygons(constructors_mmpolygon *poly, unsigned int i, constructors_mmpolygon *poly2, unsigned int j) {
    unsigned int size = poly2->buffer->n+2, n=poly2->buffer->n;

    /*printf("--merge--\n");
    
    for (unsigned int k=0; k<poly->buffer->n; k++) printf("%s %u ", (k==i || k==i+1? "*": ""), poly->buffer->contents.uint[k]);
    printf("\n");
    
    printf("--\n");
    for (unsigned int k=0; k<poly2->buffer->n; k++) printf("%s %u ", (k==j || k==j+1? "*": ""), poly2->buffer->contents.uint[k]);
    printf("\n");*/
    
    /* Expand the buffer */
    constructors_mmmodifypolygon(poly, i, 0, size, NULL);
    
    //memset(poly->buffer->contents.uint+i+1, 1, sizeof(unsigned int)*(size-1));
    
    memcpy(poly->buffer->contents.uint+i+1, poly2->buffer->contents.uint+j, sizeof(unsigned int)*(n - j));
    memcpy(poly->buffer->contents.uint+i+1+(n-j), poly2->buffer->contents.uint, sizeof(unsigned int)*(j+1));
    
   /* printf("--\n");
    
    for (unsigned int k=0; k<poly->buffer->n; k++) printf("%s %u ", (k==i ? "*": ""), poly->buffer->contents.uint[k]);
    printf("\n");
    
    printf("--endmerge--\n"); */
    
    return TRUE;
}

/* Converts buffers to an output object. The buffers should no longer be used but may be freed. */
constructors_mmoutput *contructors_mmconvertbufferstooutput(constructors_mmbuffer *vertices, constructors_mmbuffer *triangles) {
    constructors_mmoutput *out = EVAL_MALLOC(sizeof(constructors_mmoutput));
    
    if (out) {
        out->pts=NULL; out->tri=NULL;
        out->npts=vertices->n;
        out->pts=EVAL_REALLOC(vertices->contents.pt, sizeof(constructors_mmpoint)*out->npts);
        if (out->pts) vertices->contents.pt=NULL;
        out->ntri=triangles->n;
        out->tri=EVAL_REALLOC(triangles->contents.uint, sizeof(unsigned int)*3*out->ntri);
        if (out->tri) triangles->contents.pt=NULL;
    }
    
    return out;
}

/* */

void contructors_mmcheckfaceorientation(manifold_object *mobj, interpretcontext *context, uid i, double *normal)  {
    expression_objectreference  *fmvnew=NULL, *fmv=NULL, *res=NULL;
    manifold_gradelowerentry *f=NULL;
    
    double sc;
    uid swp;
    
    f=(manifold_gradelowerentry *) idtable_get(mobj->gradelower[MANIFOLD_GRADE_AREA-1], i);
    if (f) {
        fmvnew=(expression_objectreference *) manifold_elementtomultivector(mobj, context, MANIFOLD_GRADE_AREA, i);
        fmv=(expression_objectreference *) multivector_doublelisttomultivector(context, 3, MANIFOLD_GRADE_LINE, normal, 3);
        res=(expression_objectreference *) opmul(context, (expression *) fmvnew, (expression *) fmv);
        multivector_gradetodoublelist((multivectorobject *) res->obj, MANIFOLD_GRADE_VOLUME, &sc);
        
        if (sc<=0.0) { // sc<0 means pointing in same direction...
            /* Reverse the order of the definition */
            swp=f->el[0];
            f->el[0]=f->el[1];
            f->el[1]=swp;
        }
    }
    
    if (fmvnew) freeexpression((expression *) fmvnew);
    if (fmv) freeexpression((expression *) fmv);
    if (res) freeexpression((expression *) res);
}

/* Converts output object to a manifold object, optionally encapsulated within a body. */
expression *contructors_mmoutputtomanifold(interpretcontext *context, constructors_mmoutput *out, int encapsulate) {
    expression *ret=NULL;
    expression_objectreference *new=NULL, *newbody=NULL;
    manifold_object *mobj=NULL;
    uid s1,s2;
    manifold_gradelowerentry el;
    unsigned int tri[3];
    
    new=(expression_objectreference *) manifold_new();
    
    if (!new) return NULL;
    
    if (eval_isobjectref(new)) {
        mobj=(manifold_object *) new->obj;
        ret=(expression *) new;
        
        /* Add vertices */
        for (unsigned int i=0; i<out->npts; i++) {
            manifold_addvertex(mobj, context, 3, out->pts[i].pt, MANIFOLD_VERTEX_NONE);
        }
        
        /* Add triangles */
        for (unsigned int i=0; i<out->ntri; i++) {
            for (unsigned int j=0; j<3; j++) tri[j]=out->tri[3*i+j]+1; /* Morpho uids index from 1! */
            
            /* Do the edges already exist? */
            s1=manifold_findlinearelement(mobj, tri[0], tri[1]);
            s2=manifold_findlinearelement(mobj, tri[1], tri[2]);
            
            /* If not, let's add them */
            if (!s1) {
                el.el[0]=tri[0]; el.el[1]=tri[1];
                s1=manifold_addelement(mobj, context, MANIFOLD_GRADE_LINE, &el);
            }
            if (!s2) {
                el.el[0]=tri[1]; el.el[1]=tri[2];
                s2=manifold_addelement(mobj, context, MANIFOLD_GRADE_LINE, &el);
            }

            /* Now create the triangle */
            if (s1 && s2) {
                el.el[0]=s1;
                el.el[1]=s2;
                
                uid tid = manifold_addelement(mobj, context, MANIFOLD_GRADE_AREA, &el);
                
                contructors_mmcheckfaceorientation(mobj, context, tid, out->pts[out->tri[3*i]].normal);
            }
        }
    } else return NULL;
    
    if (encapsulate) {
        /* Enclose the manifold in a body */
        newbody=(expression_objectreference *) body_new();
        body_object *body = NULL;
        
        if (eval_isobjectref(newbody)) {
            body=(body_object *) newbody->obj;
            
            body_addmanifold(body, mobj);
            ret=(expression *) newbody;
        }
        
        freeexpression((expression *) new);
    }
    
contructors_mmoutputtomanifold_clearup:
    
    return ret;
}

/* Frees the output */
void constructors_mmfreeoutput(constructors_mmoutput *out) {
    if (out) {
        if (out->pts) EVAL_FREE(out->pts);
        if (out->tri) EVAL_FREE(out->tri);
        EVAL_FREE(out);
    }
}

/* -- Now for elements of the algorithm -- */

/* Build the initial hexagon of triangles [Step 0 in Hartmann]
   Input:  interpretcontext *context         - the interpretcontext
           constructors_mmfunc *func         - the surface function
           void *ref                         - reference to pass
           double *x                         - initial point
           double dt                         - the spacing
   Output: constructors_mmbuffer **vertices  - Vertex buffer is allocated and initialized
           constructors_mmbuffer **triangles - Triangle buffer is allocated and initialized
           linkedlist **polygons             - Polygon list is created
 */
void constructors_mminitialhexagon(interpretcontext *context, constructors_mmfunc *func, void *ref, double *x, double dt, constructors_mmbuffer **vertices, constructors_mmbuffer **triangles, linkedlist **polygons) {
    constructors_mmbuffer *v=NULL, *t=NULL;
    linkedlist *p=NULL;
    double x2[3]; /* Temporary storage */
    constructors_mmpolygon *cpoly;
    unsigned int vi=0;
    
    /* Data for the very first triangle */
    unsigned int tri0[] = {0,1,2, 0,2,3, 0,3,4, 0,4,5, 0,5,6, 0,6,1}, nt0=6;
    
    /* Initialize pt buffer */
    unsigned int pmax=7;
    v=constructors_mmnewbuffer(sizeof(constructors_mmpoint), &pmax);
    if (!v) {
        error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
    }
    
    constructors_mmpoint *pt=v->contents.pt; /* Shorthand for the point array */
    
    /* Obtain first point */
    constructors_mmaddvertex(v, x, &vi);
    constructors_mmsurfacepoint(context, x, func, ref, NULL, pt+vi);
    
    /* Now construct the first hexagon of points around the starting point */
    for (unsigned int i=0; i<6; i++) {
        /* q_i+􏰌2 =􏰍 p_1 􏰌+ dt*cos􏰊(i*pi/3) 􏰋t11+􏰌 dt*sin(i*pi/3) 􏰋t12 */
        manifold_vectorscale3d(pt[0].t1, dt*cos(i*PI/3), x2);
        manifold_vectoradd3d(pt[0].pt, x2, pt[i+1].pt);
        
        manifold_vectorscale3d(pt[0].t2, dt*sin(i*PI/3), x2);
        manifold_vectoradd3d(pt[i+1].pt, x2, x2);
        
        constructors_mmaddvertex(v, x2, &vi);
        constructors_mmsurfacepoint(context, x2, func, ref, NULL, pt+vi);
    }
    
    /* Initialize auxiliary variables for the first 7 points */
    for (unsigned int i=0; i<v->n; i++) {
        pt[i].frontangle=0.0;
        pt[i].border_point = FALSE;
        pt[i].exclude = FALSE;
        pt[i].angle_changed = ( i>0 ? TRUE : FALSE);
    }
    
    /* Create the triangle buffer and copy in the first six triangles */
    unsigned int tmax=nt0; /* Start with a buffer twice the size of the initial triangles */
    t=constructors_mmnewbuffer(sizeof(unsigned int)*3, &tmax);
    
    if (t) {
        memcpy(t->contents.uint, tri0, sizeof(unsigned int)*6*3);
        t->n=6;
    } else {
        error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
    }
    
    /* Set up the front polygon list and first polygon */
    p=linkedlist_new();
    if (!p) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
    cpoly=constructors_mmnewpolygon(6);
    if (!cpoly) error_raise(context, ERROR_ALLOCATIONFAILED, ERROR_ALLOCATIONFAILED_MSG, ERROR_ALLOCATIONFAILED);
    linkedlist_addentry(p, cpoly);
    
    /* The first front polygon consists of vertices 1-6 (and not 0) */
    for (unsigned int i=0; i<6; i++) cpoly->buffer->contents.uint[i]=i+1;
    cpoly->buffer->n=6;
    
    /* Return all the buffers allocated */
    if (vertices) *vertices=v;
    if (triangles) *triangles=t;
    if (polygons) *polygons=p;
}

/* Calculate front angle given indices iv and two other points iv1 and iv2 (to the left and right of i) */
double constructors_mmfrontangle(constructors_mmpoint *pt, unsigned int iv, unsigned int iv1, unsigned int iv2) {
    double *v1, *v2, omega1, omega2, zeta[2], xl[3];
    
    v1 = pt[iv1].pt;
    
    /* Shift this into local coordinates relative to pt[i] */
    manifold_vectorsubtract3d(v1, pt[iv].pt, xl);
    
    /* Project this position into the (t1, t2, n) frame at point i */
    zeta[0] = manifold_dotproduct3d(xl, pt[iv].t1);
    zeta[1] = manifold_dotproduct3d(xl, pt[iv].t2);
    
    /* Calculate polar angle from components of zeta */
    omega1=atan2(zeta[1], zeta[0]);
    
    /* v2 is the point to the right of the current pt with periodic bcs */
    v2 = pt[iv2].pt;
    
    /* Shift this into local coordinates relative to pt[i] */
    manifold_vectorsubtract3d(v2, pt[iv].pt, xl);
    
    /* Project this position into the (t1, t2, n) frame at point i */
    zeta[0] = manifold_dotproduct3d(xl, pt[iv].t1);
    zeta[1] = manifold_dotproduct3d(xl, pt[iv].t2);
    /* Calculate polar angle from components of zeta */
    omega2=atan2(zeta[1], zeta[0]);
    
    /* Return calculated front angle */
    return (omega2>=omega1 ? omega2-omega1 : omega2-omega1+2*PI);
}

/* Recalculate front angles in the current front polygon [Step 1 in Hartmann]
  Input:  constructors_mmbuffer *vertices - vertex buffer
          constructors_mmpolygon *polygon - polygon to loop over
*/
void constructors_mmrecalculatefrontangles(constructors_mmbuffer *vertices, constructors_mmpolygon *polygon, double *minfrontangle) {
    /* Unpack the structures into shorthand variables */
    constructors_mmpoint *pt = vertices->contents.pt; /* The vertex list */
    unsigned int *poly = polygon->buffer->contents.uint; /* The polygon list */
    unsigned int n = polygon->buffer->n; /* Number of points in the polygon */
    double mf=0;
    
    for (unsigned int i=0; i<n; i++) {
        if (pt[poly[i]].angle_changed && !pt[poly[i]].border_point) {
            pt[poly[i]].frontangle = constructors_mmfrontangle(pt, poly[i], (i==0 ? poly[n-1] : poly[i-1]), (i==n-1 ? poly[0] : poly[i+1]));
            
            pt[poly[i]].angle_changed=FALSE;
        }
        if (minfrontangle && pt[poly[i]].frontangle<mf) mf=pt[poly[i]].frontangle;
    }
    
    if (minfrontangle) *minfrontangle=mf;
}

int constructors_mmpointcheck(constructors_mmpoint *pt, unsigned int *poly1, unsigned int i, unsigned int *poly2, unsigned int j, unsigned int n, double dt2, int altcheck, int badcheck) {
    double x[3], dx2;
    /* Compute |x_i - x_j|^2 */
    manifold_vectorsubtract3d(pt[poly1[i]].pt, pt[poly2[j]].pt, x);
    dx2=manifold_dotproduct3d(x, x);
    
    /* Is this less than dt^2 ? and not a neighbor or a neigbor of a neighbor? */
    if (dx2>dt2) return FALSE;
    
    /* Are the two points excluded from distance checks? */
    if (pt[poly1[i]].exclude || pt[poly2[j]].exclude) return FALSE;
    
    /* [optional] Are the points neighbors of neighbors?  */
    if (altcheck) {
        if ( !( j-i>2 && (!(i==0 && (j==n-1 || j==n-2)) && !(i==1 && j==n-1)))) return FALSE;
    }
    
    /* [optional] Check for 'bad' near points by comparing angle to this point with the front angle. */
    if (badcheck) {
        if (constructors_mmfrontangle(pt, poly1[i], (i==0 ? poly1[n-1] : poly1[i-1]), poly2[j]) >
            pt[poly1[i]].frontangle) return FALSE;
    }
    
    return TRUE;
}

/* Protoype needed by step 2 */
void constructors_mmgeneratetriangles(interpretcontext *context, constructors_mmfunc *func, void *ref, constructors_mmbuffer *vertices, constructors_mmbuffer *triangles, constructors_mmpolygon *polygon, double dt, unsigned int *frci, int *halt);

/* Proximity check [Step 2 in Hartmann]
  Input:  interpretcontext *context        - the context
          constructors_mmfunc *func        - the implicit function
          void *ref                        - and its reference
          constructors_mmbuffer *vertices  - vertex buffer
          constructors_mmbuffer *triangles - triangle buffer
          linkedlist *polygons             - List of polygons
          constructors_mmpolygon *polygon  - curent polygon
          double dt                        - proximity test
*/
/* TODO: Use KD Trees to accelerate this */
void constructors_mmproximitycheck(interpretcontext *context,  constructors_mmfunc *func, void *ref, constructors_mmbuffer *vertices, constructors_mmbuffer *triangles, linkedlist *polygons, constructors_mmpolygon *polygon, double dt, int *halt) {
    constructors_mmpoint *pt = vertices->contents.pt; /* The vertex list */
    double dt2=dt*dt;
    constructors_mmpolygon *split=NULL, *del=NULL;
    
    /* Are there any triangles with frontangles less than PI/3? If so, abandon the distance check */
    for (unsigned int i=0; i<polygon->buffer->n; i++) {
        if (pt[polygon->buffer->contents.uint[i]].frontangle<0.3*PI) return;
    }
    
    /* Distance check within the current polygon */
    for (unsigned int i=0; i<polygon->buffer->n && !split; i++) {
        for (unsigned int j=i+1; j<polygon->buffer->n && !split; j++) {
            
            if (constructors_mmpointcheck(pt, polygon->buffer->contents.uint, i, polygon->buffer->contents.uint, j, polygon->buffer->n, dt2, TRUE, FALSE)) {
                /*for (unsigned int k=0; k<polygon->buffer->n; k++) printf("%s %u ", (k==i || k==j ? "*": ""), polygon->buffer->contents.uint[k]);
                printf("\n");*/
                
                /* Exclude these points from further distance checks */
                pt[polygon->buffer->contents.uint[i]].exclude=TRUE;
                pt[polygon->buffer->contents.uint[j]].exclude=TRUE;
                /* Recalculate front angles */
                pt[polygon->buffer->contents.uint[i]].angle_changed=TRUE;
                pt[polygon->buffer->contents.uint[j]].angle_changed=TRUE;
                
                /* Split off the polygon */
                split=constructors_mmsplitpolygon(polygon, i, j);
                
                /* Add the new polygon to the list */
                if (split) linkedlist_addentry(polygons, split);
            }
        }
    }
    
    /* Distance check with other polygons */
    for (linkedlistentry *e=polygons->first; e!=NULL && !del; e=e->next) {
        constructors_mmpolygon *poly2=(constructors_mmpolygon *) e->data;
        /* Don't compare with ourselves or with a polygon that got split off... */
        if (poly2!=polygon && poly2!=split) {
            /* Loop over points in both polygons */
            for (unsigned int i=0; i<polygon->buffer->n && !del; i++) {
                for (unsigned int j=0; j<poly2->buffer->n && !del; j++) {
                    
                    if (constructors_mmpointcheck(pt, polygon->buffer->contents.uint, i, poly2->buffer->contents.uint, j, polygon->buffer->n, dt2, FALSE, TRUE)) {
                        
                        /* Exclude these points from further distance checks */
                        pt[polygon->buffer->contents.uint[i]].exclude=TRUE;
                        pt[polygon->buffer->contents.uint[i]].angle_changed=TRUE;
                        pt[poly2->buffer->contents.uint[j]].exclude=TRUE;
                        pt[poly2->buffer->contents.uint[j]].angle_changed=TRUE;
                        
                        if (constructors_mmmergepolygons(polygon, i, poly2, j)) {
                            unsigned int *poly = polygon->buffer->contents.uint;
                            unsigned int n=polygon->buffer->n;
                            
                            /* Should flag this polygon for deletion */
                            del = poly2;
                            
                            /* Generate triangles at these two locations */
                            unsigned int v1=i, v2=i+1;
                            
                            pt[poly[v1]].frontangle = constructors_mmfrontangle(pt, poly[v1], (v1==0 ? poly[n-1] : poly[v1-1]), (v1==n-1 ? poly[0] : poly[v1+1]));
                            pt[poly[v2]].frontangle = constructors_mmfrontangle(pt, poly[v2], (v2==0 ? poly[n-1] : poly[v2-1]), (v2==n-1 ? poly[0] : poly[v2+1]));
                            
                            /* Make sure we go with the smallest front angle first */
                            if (pt[poly[i]].frontangle > pt[poly[i+1]].frontangle) {
                                v1=i+1;
                                v2=i;
                            }
                            /* First triangle */
                            constructors_mmgeneratetriangles(context, func, ref, vertices, triangles, polygon, dt, &poly[v1], halt);
                            poly = polygon->buffer->contents.uint; /* Update the shorthand */
                            
                            /*for (unsigned int k=0; k<polygon->buffer->n; k++) printf("%s %u ", (k==i ? "*": ""), polygon->buffer->contents.uint[k]);
                            printf("\n");*/
                            
                            /* Second triangle */
                            pt[poly[v2]].frontangle = constructors_mmfrontangle(pt, poly[v2], (v2==0 ? poly[n-1] : poly[v2-1]), (v2==n-1 ? poly[0] : poly[v2+1]));
                            constructors_mmgeneratetriangles(context, func, ref, vertices, triangles, polygon, dt, &poly[v2], halt);
                            
                            /*printf("--\n");
                            for (unsigned int k=0; k<polygon->buffer->n; k++) printf("%s %u ", (k==i ? "*": ""), polygon->buffer->contents.uint[k]);
                            printf("\n");*/
                        } else {
                            error_raise(context, ERROR_CONSTRUCTOR_MMMERGEFAILED, ERROR_CONSTRUCTOR_MMMERGEFAILED_MSG, ERROR_FATAL);
                        }
                    }
                }
            }
        }
    }
    
    /* And now recalculate front angles */
    if (split || del) constructors_mmrecalculatefrontangles(vertices, polygon, NULL);
    
    /* Now remove a merged polygon if any was detected */
    if (del) {
        linkedlist_removeentry(polygons, del);
        /* And free the deleted polygon */
        constructors_mmfreepolygon(del);
    }
    
}

/* Generate new triangles [Step 3 in Hartmann]
  Input:  interpretcontext *context        - the context
          constructors_mmfunc *func        - the surface function
          void *ref                        - reference to pass to the surface function
          constructors_mmbuffer *vertices  - vertex buffer
          constructors_mmbuffer *triangles - triangle buffer
          constructors_mmpolygon *polygon  - polygon to loop over
          double dt                        - approximate size of triangles
          unsigned int *frci               - If set, forces triangles to be generated at vertex frci
*/
void constructors_mmgeneratetriangles(interpretcontext *context, constructors_mmfunc *func, void *ref, constructors_mmbuffer *vertices, constructors_mmbuffer *triangles, constructors_mmpolygon *polygon, double dt, unsigned int *frci, int *halt) {
    /* Unpack the structures into shorthand variables */
    constructors_mmpoint *pt = vertices->contents.pt; /* The vertex list */
    unsigned int *poly = polygon->buffer->contents.uint; /* The polygon list */
    unsigned int n = polygon->buffer->n; /* Number of points in the polygon */
    
    unsigned int nnewt; /* Number of new triangles to generate */
    double x[3], xl[3], deltaomega, sep;
    unsigned int v1, v2, vk;
    
    /* Identify the index on the front polygon with minimal front angle
       OR that corresponds to the forced vertex */
    unsigned int k=0;
    double omega=pt[poly[0]].frontangle;
    
    for (unsigned int i=1; i<n; i++) {
        if ((frci && poly[i]==*frci)) {
            omega = pt[poly[i]].frontangle;
            k=i;
            break; /* Definitely stop here */
        } else if (pt[poly[i]].frontangle<omega) {
            omega = pt[poly[i]].frontangle;
            k=i;
        }
    }
    
    /* 1. Determine the neighbors of the selected vertex */
    vk = poly[k];
    v1 = (k==0 ? poly[n-1] : poly[k-1]);
    v2 = (k==n-1 ? poly[0] : poly[k+1]);
    
    /* 2. Determine number of triangles to be generated */
    nnewt= ((int) trunc(3*omega/PI))+1;
    
    deltaomega=omega/nnewt;
    
    /* Correct for extreme cases */
    if (deltaomega<0.8 && nnewt>1) {
        nnewt--; deltaomega=omega/nnewt;
    } else if (nnewt==1 && deltaomega>0.8) {
        /* |v2-v1| */
        manifold_vectorsubtract3d(pt[v1].pt, pt[v2].pt, xl);
        sep=manifold_norm3d(xl);
        if (sep>1.2*dt) {
            nnewt=2; deltaomega=deltaomega/2;
        }
    } else if (omega<3) {
        double sep1, sep2;
        /* |v1-vk| */
        manifold_vectorsubtract3d(pt[v1].pt, pt[vk].pt, xl);
        sep1=manifold_norm3d(xl);
        /* |v2-vk| */
        manifold_vectorsubtract3d(pt[v2].pt, pt[vk].pt, xl);
        sep2=manifold_norm3d(xl);
        
        if (sep1<=0.5*dt || sep2 <= 0.5*dt) {
            nnewt=1;
        }
    }
    
    /* 3. Generate the triangles */
    unsigned int tri[3];
    unsigned int vi[nnewt+1]; /* Store the ids of the new vertices */
    vi[0]=v1;
    vi[nnewt]=v2;
    
    if (nnewt==1) {
        tri[0] = (k==0 ? poly[n-1] : poly[k-1]);
        tri[1] = (k==n-1 ? poly[0] : poly[k+1]);
        tri[2] = poly[k];
        constructors_mmaddtriangle(triangles, tri);
    } else {
        /* Generate the new points */
        double q0[3]; /* Orthogonal projection of v1 onto tangent plane */
        double qi[3]; /* New point in tangent plane */
        double theta;
        
        /* Find (v1-vk) */
        manifold_vectorsubtract3d(pt[v1].pt, pt[vk].pt, xl);
        /* Project  onto tangent plane */
        q0[0]=manifold_dotproduct3d(xl, pt[vk].t1);
        q0[1]=manifold_dotproduct3d(xl, pt[vk].t2);
        q0[2]=0;
        /* Now normalize */
        manifold_normalizevector3d(q0, q0);
        
        for (unsigned int i=1; i<nnewt; i++) {
            theta=i*deltaomega;
            /* Rotate the vector in the tangent plane */
            qi[0]=q0[0]*cos(theta) - q0[1]*sin(theta);
            qi[1]=q0[0]*sin(theta) + q0[1]*cos(theta);
            
            /* Now build the vector pt + dt*(t1*qi[0] + t2*qi[1]) */
            manifold_vectorscale3d(pt[vk].t1, dt*qi[0], xl);
            manifold_vectoradd3d(pt[vk].pt, xl, x);
            manifold_vectorscale3d(pt[vk].t2, dt*qi[1], xl);
            manifold_vectoradd3d(x, xl, x);
            
            /* Add the vertex */
            constructors_mmaddvertex(vertices, x, vi+i);
            pt = vertices->contents.pt; /* Update the shorthand */
            
            /* Reproject onto the surface */
            constructors_mmsurfacepoint(context, x, func, ref, NULL, pt+vi[i]);
        }
        
        /* Generate the new triangles */
        tri[2]=vk;
        for (unsigned int i=0; i<nnewt; i++) {
            tri[0]=vi[i];
            tri[1]=vi[i+1];
            constructors_mmaddtriangle(triangles, tri);
            
        }
    }
    
    /* 4. Correct the front polygon */
    /* Set angle changed for new points and all neighbors to TRUE so they're recalculated */
    for (unsigned int i=0; i<nnewt+1; i++) {
        vertices->contents.pt[vi[i]].angle_changed=TRUE;
    }
    
    /* Insert new vertices into front polygon */
    constructors_mmmodifypolygon(polygon, k, 1, nnewt-1, vi+1);
    
    /*for (unsigned int i=0; i<polygon->buffer->n; i++) printf("%u ",polygon->buffer->contents.uint[i]);
    printf("\n");*/
}

/* Public interface to marching method for implicit surfaces
  Input:  interpretcontext *context   - interpretcontext for error reporting
          constructors_mmfunc *func   - level set function defining the surface
          void *ref                   - reference to be passed to func
          double x[3]                 - an initial point
          double dt                   - lengthscale for the mesh
          int maxiterations           - maximum number of iterations (or negative for default)
  Returns: (constructors_mmoutput *) an output object containing the vertex, triangle and normal data
*/
constructors_mmoutput *geometry_manifoldimplicit(interpretcontext *context, constructors_mmfunc *func, void *ref, double x[3], double dt, int maxiterations) {
    /* Buffers for vertices, triangles and the front polygon */
    constructors_mmbuffer *vertices=NULL, *triangles=NULL;
    linkedlist *polygons=NULL;
    constructors_mmoutput *out=NULL;
    unsigned int iter=0, maxiter=(maxiterations>0 ? maxiterations : 10000);
    int halt=FALSE;
    double minfrontangle=0.0;
    
    /* Step 0: Initialize buffers and hexagon */
    constructors_mminitialhexagon(context, func, ref, x, dt, &vertices, &triangles, &polygons);
    
    /* Loop, working on the front polygon until there are no polygons left. */
    do {
        linkedlistentry *e=polygons->first;
        constructors_mmpolygon *p=e->data;
        int finished=FALSE;
        
        do {
            
            /* Step 1: Calculate front angles */
            constructors_mmrecalculatefrontangles(vertices, p, &minfrontangle);
            
            /* Step 2: Proximity check between polygons */
             /* Only if there are no front angles < 60deg */
            constructors_mmproximitycheck(context, func, ref, vertices, triangles, polygons, p, dt, &halt);
            
            /* Step 3: Create new triangles */
            constructors_mmgeneratetriangles(context, func, ref, vertices, triangles, p, dt, NULL, &halt);
            
            /* Is the front polygon now just a triangle? */
            if (p->buffer->n==3) {
                constructors_mmaddtriangle(triangles, p->buffer->contents.uint);
                
                /* Remove the polygon */
                linkedlist_removeentry(polygons, p);
                constructors_mmfreepolygon(p);
                finished=TRUE;
            }
            
            iter++;
        } while (!finished && iter<maxiter && !halt);
        
    } while (polygons->first && iter<maxiter && !halt);
    
    if (polygons) {
        for (linkedlistentry *e=polygons->first; e!=NULL; e=e->next) {
            constructors_mmpolygon *p = (constructors_mmpolygon *) e->data;
            
            /*for (unsigned int k=0; k<p->buffer->n; k++) printf("%u ", p->buffer->contents.uint[k]);
            printf("\n");*/
            
            constructors_mmfreepolygon(p);
        }
        linkedlist_free(polygons);
    }
    
    /* Extract information from buffers for output */
    out=contructors_mmconvertbufferstooutput(vertices, triangles);
    
    constructors_mmfreebuffer(vertices);
    constructors_mmfreebuffer(triangles);
    
    return out;
}

typedef struct {
    interpretcontext *context;
    expression *exp;
    expression *gradient;
} geometry_manifoldimplicitref;

double constructors_mmexpressionwithcoords(double *x, void *ref, double *grad) {
    geometry_manifoldimplicitref *r = (geometry_manifoldimplicitref *) ref;
    char *coords[3] = {"x","y","z"};
    expression_name name; name.type=EXPRESSION_NAME;
    expression_float flt; flt.type=EXPRESSION_FLOAT;
    expression *result=NULL;
    double out=0.0;
    
    /* Substitute the ith coordinate into the appropriate name */
    for (unsigned int i=0; i<3; i++) {
        name.name=coords[i];
        flt.value=x[i];
        
        /* Assign */
        freeexpression(opassign(r->context, (expression *) &name, (expression *) &flt));
    }
    
    result=interpretexpression(r->context, r->exp);
    if (eval_isreal(result)) {
        out=eval_floatvalue(result);
    } else {
        error_raise(r->context, ERROR_CONSTRUCTOR_MMEXPNONNUM, ERROR_CONSTRUCTOR_MMEXPNONNUM_MSG, ERROR_FATAL);
    }
    if (result) freeexpression(result);
    
    if (r->gradient) {
        result=interpretexpression(r->context, r->gradient);
        if (!eval_listtodoublearray((expression_list *) result, grad, 3)) {
            error_raise(r->context, ERROR_CONSTRUCTOR_MMGRADNONNUM, ERROR_CONSTRUCTOR_MMGRADNONNUM_MSG, ERROR_FATAL);
        }
        if (result) freeexpression(result);
    }
    
    return out;
}

double constructors_mmexpression(double *x, void *ref, double *grad) {
    geometry_manifoldimplicitref *r = (geometry_manifoldimplicitref *) ref;
    double out=constructors_mmexpressionwithcoords(x, ref, grad);
    double eps=1e-6;
    
    /* If no gradient, compute it numerically */
    if (!r->gradient) {
        double x2[3];
        
        for (unsigned int i=0; i<3; i++) {
            for (unsigned int j=0; j<3; j++) x2[j]=x[j];
            x2[i]+=eps;
            grad[i] = (constructors_mmexpressionwithcoords(x2, ref, grad) - out)/eps;
        }
    }
    
    return out;
}

/* Triangulate an implicit manifold */
expression *geometry_manifoldimpliciti(interpretcontext *context, int nargs, expression **args) {
    geometry_manifoldimplicitref ref;
    constructors_mmoutput *ret=NULL;
    interpretcontext *options=NULL;
    double x[3]={1.0, 1.0, 1.0};
    double dt=0.3;
    expression *out=NULL, *grad=NULL;
    int optionsstart = eval_optionstart(context, nargs, args);
    int maxiterations = -1;
    
    /* Validate arguments */
    if (nargs<1) {
        error_raise(context, ERROR_CONSTRUCTOR_MMIMPLICIT, ERROR_CONSTRUCTOR_MMIMPLICIT_MSG, ERROR_FATAL);
    }
    
    /* Set function */
    ref.exp=args[0];
    ref.gradient=NULL;
    
    /* Process options */
    if (nargs>1) options=eval_options(context, optionsstart, nargs, args);
    
    if (options) {
        /* Stepsize */
        expression *e = (expression *) lookupsymbol(options, GEOMETRY_MMSTEPSIZE);
        if (eval_isreal(e)) dt = eval_floatvalue(e);
        if (e) freeexpression(e);
        
        /* Starting point */
        e = (expression *) lookupsymbol(options, GEOMETRY_MMSTART);
        if (e) {
            expression *start = interpretexpression(context, e);
            if (eval_islist(start)) {
                if (!eval_listtodoublearray((expression_list *) e, x, 3)) {
                    error_raise(context, ERROR_CONSTRUCTOR_MMSTART, ERROR_CONSTRUCTOR_MMSTART_MSG, ERROR_FATAL);
                }
            }
            if (start) freeexpression(start);
        }
        if (e) freeexpression(e);
        
        /* Gradient */
        grad = (expression *) lookupsymbol(options, GEOMETRY_MMGRADIENT);
        if (grad) {
            if (eval_islist(e)) {
                ref.gradient=e;
            } else {
                error_raise(context, ERROR_CONSTRUCTOR_MMSYNTAX, ERROR_CONSTRUCTOR_MMSYNTAX_MSG, ERROR_FATAL);
            }
        }
        
        /* Max iterations */
        e = (expression *) lookupsymbol(options, GEOMETRY_MMMAXITER);
        if (e) {
            if (eval_isinteger(e)) {
                maxiterations = eval_integervalue(e);
            }
        }
        if (e) freeexpression(e);
    }
    
    ref.context = newinterpretcontext(context, 3);
    
    /* Create the manifold */
    ret=geometry_manifoldimplicit(context, constructors_mmexpression, &ref, x, dt, maxiterations);
    
    /* Convert the output to a manifold and embed in a body */
    if (ret) {
        out=contructors_mmoutputtomanifold(context, ret, TRUE);
    }
    
    /* Cleanup */
    if (grad) freeexpression(grad);
    if (ref.context) freeinterpretcontext(ref.context);
    if (options) freeinterpretcontext(options);
    if (ret) constructors_mmfreeoutput(ret);
    
    return out;
}

/*
* Initialize the constructor library
*/

void constructorsinitialize(void) {
    intrinsic(GEOMETRY_MANIFOLDLOAD, FALSE, geometry_manifoldloadi, NULL);
    intrinsic(GEOMETRY_MANIFOLDLINE, TRUE, geometry_parametriclinei, NULL);
    intrinsic(GEOMETRY_MANIFOLDPOINTS, FALSE, geometry_manifoldpointsi, NULL);
    //intrinsic(GEOMETRY_MANIFOLDTRIANGULATE, FALSE, geometry_manifoldtriangulatei, NULL);
    intrinsic(GEOMETRY_MANIFOLDCUBOID, FALSE, geometry_manifoldcuboidi, NULL);
    intrinsic(GEOMETRY_MANIFOLDIMPLICIT, TRUE, geometry_manifoldimpliciti, NULL);
}
