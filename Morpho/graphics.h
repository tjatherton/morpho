/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef Morpho_graphics_h
#define Morpho_graphics_h

#include <stdio.h>
#include "eval.h"

#define GRAPHICS_LABEL "graphics"

typedef struct {
    float p1[2];
    float p2[2];
} graphics_bbox;

typedef struct {
    CLASS_GENERICOBJECTDATA
    
    linkedlist *list;
} graphics_object;

typedef enum {  GRAPHICS_SAVESTATE,
                GRAPHICS_RESTORESTATE,
                GRAPHICS_SCALE,
                GRAPHICS_TRANSLATE,
                GRAPHICS_ROTATE,
                GRAPHICS_POINT,
                GRAPHICS_LINETO,
                GRAPHICS_MOVETO,
                GRAPHICS_CLOSE,
                GRAPHICS_LINEWIDTH,
                GRAPHICS_SETCOLOR,
                GRAPHICS_STROKE,
                GRAPHICS_FILL,
                GRAPHICS_TEXT,
                GRAPHICS_SHADE_LATTICE,
                GRAPHICS_SHADE_TRIANGLES,
                GRAPHICS_FORCE_BBOX
            } graphics_elementtype;

#define GRAPHICS_SAVESTATE_SELECTOR "savestate"
#define GRAPHICS_RESTORESTATE_SELECTOR "restorestate"
#define GRAPHICS_SCALE_SELECTOR "scale"
#define GRAPHICS_TRANSLATE_SELECTOR "translate"
#define GRAPHICS_ROTATE_SELECTOR "rotate"
#define GRAPHICS_POINT_SELECTOR "point"
#define GRAPHICS_LINETO_SELECTOR "lineto"
#define GRAPHICS_MOVETO_SELECTOR "move"
#define GRAPHICS_CLOSE_SELECTOR "close"
#define GRAPHICS_LINEWIDTH_SELECTOR "linewidth"
#define GRAPHICS_SETCOLOR_SELECTOR "setcolor"
#define GRAPHICS_STROKE_SELECTOR "stroke"
#define GRAPHICS_FILL_SELECTOR "fill"
#define GRAPHICS_TEXT_SELECTOR "text"
#define GRAPHICS_SHADE_LATTICE_SELECTOR "shadelattice"
#define GRAPHICS_SHADE_TRIANGLES_SELECTOR "shadetriangles"
#define GRAPHICS_SET_BBOX_SELECTOR "setbbox"
#define GRAPHICS_BBOX_SELECTOR "bbox"

#define GRAPHICS_TOGRAPHICS "tographics"

#define GRAPHICS_DISPLAYLIST "displaylist"

#define GRAPHICS_POINTSIZE 0.005

typedef struct {
    graphics_elementtype type;
} graphics_element;

typedef graphics_element graphics_strokeelement;
typedef graphics_element graphics_fillelement;
typedef graphics_element graphics_closepathelement;

typedef struct {
    graphics_elementtype type;
    float point[2];
} graphics_pointelement;

typedef struct {
    graphics_elementtype type;
    float angle;
} graphics_rotateelement;

typedef struct {
    graphics_elementtype type;
    float width;
} graphics_linewidthelement;

typedef struct {
    graphics_elementtype type;
    float r;
    float g;
    float b;
} graphics_setcolorelement;

typedef struct {
    graphics_elementtype type;
    graphics_bbox bbox;
} graphics_bboxelement;

typedef graphics_pointelement graphics_movetoelement;

typedef graphics_pointelement graphics_linetoelement;

typedef enum {GRAPHICS_ALIGNLEFT, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNRIGHT, GRAPHICS_ALIGNBOTTOM, GRAPHICS_ALIGNMIDDLE, GRAPHICS_ALIGNTOP } graphics_alignment;

typedef struct {
    graphics_elementtype type;
    float point[2];
    graphics_alignment horizontal;
    graphics_alignment vertical;
    char *string;
} graphics_textelement;

typedef struct {
    graphics_elementtype type;
    int size;
    float *data;
} graphics_shadetriangleselement;

typedef struct {
    graphics_elementtype type;
    int size[2];
    float *data;
} graphics_shadelatticeelement;

/* Rest of the graphics subsystem */
#include "plot.h"
#include "graphics3d.h"
#include "plot3d.h"
#include "color.h"

#define GRAPHICS_PDFMAXOBJECTS 128

/* graphics functions */
graphics_element *graphics_cloneentry(graphics_element *el);

expression *graphics_newgraphics(void);

void graphics_point(graphics_object *obj,float *pt);
void graphics_moveto(graphics_object *obj,float *pt);
void graphics_lineto(graphics_object *obj,float *pt);
void graphics_linewidth(graphics_object *obj,float width);
void graphics_setcolor(graphics_object *obj,float r, float g, float b);
void graphics_closepath(graphics_object *obj);
void graphics_fill(graphics_object *obj);
void graphics_stroke(graphics_object *obj);
void graphics_scale(graphics_object *obj, float *s);
void graphics_rotate(graphics_object *obj, float angle);
void graphics_translate(graphics_object *obj, float *t);
void graphics_savestate(graphics_object *obj);
void graphics_restorestate(graphics_object *obj);
void graphics_text(graphics_object *obj, char *string, float *pt, graphics_alignment alignhoriz, graphics_alignment alignvert);
float *graphics_shadelattice(graphics_object *obj, unsigned int nx, unsigned int ny);
float *graphics_shadetriangles(graphics_object *obj, unsigned int n);

void graphics_transformpoint(float v1[2], float m[6], float v2[2]);

int graphics_validate_pointlist(expression_list *list, unsigned int dim);

void graphics_forcebbox(graphics_object *obj, graphics_bbox *bbox);

void graphics_line(graphics_object *obj, linkedlist *list, int close, int fill);
void graphics_linefrompoints(graphics_object *obj, float *p1, float *p2);

void graphics_calculatebbox(graphics_object *g, graphics_bbox *bbox);

/* Selectors */
//expression *graphics_export(object *obj, interpretcontext *context, int nargs, expression **args);
expression *graphics_freei(object *obj, interpretcontext *context, int nargs, expression **args);

void graphics_serializetohashtable(graphics_object *gobj, hashtable *ht);

expression *graphics_bboxtolist(graphics_bbox *bbox);
expression *graphics_serializei(object *obj, interpretcontext *context, int nargs, expression **args);

void graphicsinitialize(void);

#endif
