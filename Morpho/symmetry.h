/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * Symmetry - Provide symmetry information for manifoldss
 */

#include "geometry.h"

#ifndef symmetry_h
#define symmetry_h

/* Symmetry type */
typedef struct symmetry_object_s {
    CLASS_GENERICOBJECTDATA
    
    //linkedlist *selection;
} symmetry_object;

/* Classes */
#define GEOMETRY_PERIODICBCCLASS "periodicbc"

#endif
