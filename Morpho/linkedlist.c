/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include <stdio.h>
#include "linkedlist.h"

/* Create a new linked list. */
linkedlist *linkedlist_new(void) {
    linkedlist *list=EVAL_MALLOC(sizeof(linkedlist));
    
    if (list) {
        list->first=NULL;
        list->last=NULL;
    }
    
    return list;
}

/* Add an entry to a linked list. */
linkedlistentry *linkedlist_addentry(linkedlist *list, void *data) {
    linkedlistentry *entry=NULL;
    if (!list) return NULL;
    
    entry = EVAL_MALLOC(sizeof(linkedlistentry));
    
    if (entry) {
        entry->data=data;
        entry->next=NULL;
        
        if (list->last) list->last->next=entry;
        list->last=entry;
        if (!list->first) list->first=entry;
    }
    
    return entry;
}

/* Remove an entry from a linked list. Does NOT free attached data */
void linkedlist_removeentry(linkedlist *list, void *data) {
    linkedlistentry *prev=NULL;
    
    for (linkedlistentry *entry = list->first;entry!=NULL;entry=entry->next) {
        if (entry->data==data) {
            if (prev) { /* Middle of the list */
                prev->next=entry->next;
            } else { /* Start of the list */
                list->first=entry->next;
            }
            if (entry->next==NULL) list->last=prev;
            EVAL_FREE(entry);
            return;
        }
        prev=entry;
    }

    return;
}

/* Find the position of an entry in a linked list. */
unsigned int linkedlist_position(linkedlist *list, void *data) {
    int i=0;
    
    if (list->first) for (linkedlistentry *entry = list->first; entry!=NULL; entry=entry->next) {
        if (entry->data == data) return i+1;
        i++;
    }
    
    return 0;
}

/* Find the number of items in a linked list. */
unsigned int linkedlist_length(linkedlist *list) {
    int i=0;
    
    if (list) for (linkedlistentry *entry = list->first;entry!=NULL;entry=entry->next) i++;
    
    return i;
}

/* Find a particular entry from an index. */
linkedlistentry *linkedlist_element(linkedlist *list, unsigned int i) {
    linkedlistentry *entry=NULL;
    unsigned int j=0;
    
    for (entry=list->first;(entry!=NULL)&&(j<i);entry=entry->next) j++;
    
    return entry;
}

/* EVAL_FREEs a linked list. 
 * Important: Does NOT free attached data structures. */
int linkedlist_free(linkedlist *list) {
    linkedlistentry *next;
    if (!list) return 0;
    
    for (linkedlistentry *entry = list->first;entry!=NULL;entry=next) {
        next=entry->next;
        EVAL_FREE(entry);
    }
    
    EVAL_FREE(list);
    
    return 0;
}

/* Apply a function to every member of a linked list. */
void linkedlist_map(linkedlist *list, linkedlistmapfunction *func) {
    if ((func)&&(list)) for (linkedlistentry *entry = list->first;entry!=NULL;entry=entry->next) {
        func(entry->data);
    }
}
