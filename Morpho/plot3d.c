/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "plot3d.h"

#define PLOT3D_LABEL "plot3d"

expression *plot3d_newplot(void) {
    plot3d_object *obj=(plot3d_object *) class_instantiate(class_lookup(PLOT3D_LABEL));
    expression_objectreference *ref=NULL;
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

/*
 * Selectors
 */

expression *plot3d_drawaxesi(object *obj, interpretcontext *context, int nargs, expression **args) {
    char buffer[128];
    expression_objectreference *ref = (expression_objectreference *) graphics3d_newgraphics3d();
    graphics3d_object *axesobj=NULL;
    plot3d_object *pobj = (plot3d_object *) obj;
    float p[3];
    int axischoice[3];
    int axistickalignment[3]={1,0,0};
    float axisticklength[3];
    unsigned int i,j;
    
    /* Length of tick marks */
    for (i=0;i<3;i++) axisticklength[i]=(pobj->plotrange.p2[i]-pobj->plotrange.p1[i])*0.05;
    /* Choice of where to display axes */
    for (i=0;i<3;i++) if (pobj->viewdirection[i]>0) axischoice[i]=1; else axischoice[i]=0;
    
    if (ref) if (ref->type==EXPRESSION_OBJECT_REFERENCE) {
        axesobj=(graphics3d_object *) ref->obj;
        
        /* Draw the axes and ticks */
        for (i=0; i<3; i++) {
            /* Decide positioning of axis */
            for (j=0; j<3; j++) if (axischoice[j]==0) p[j]=pobj->plotrange.p1[j]; else p[j]=pobj->plotrange.p2[j];
            
            /* Draw the axis */
            p[i]=pobj->plotrange.p1[i];
            graphics3d_moveto(axesobj, p);
            p[i]=pobj->plotrange.p2[i];
            graphics3d_lineto(axesobj, p);
            graphics3d_stroke(axesobj);
            
            for (j=0;j<pobj->ticks[i].nticks;j++) {
                p[i]=pobj->ticks[i].majortickstart+j*(pobj->ticks[i].majortickseparation);
                
                graphics3d_moveto(axesobj, p);
                p[axistickalignment[i]]+=axisticklength[i];
                graphics3d_lineto(axesobj, p);
                graphics3d_stroke(axesobj);
                
                p[axistickalignment[i]]-=axisticklength[i];
                
                sprintf(buffer,"%g",pobj->ticks[i].majortickstart+j*(pobj->ticks[i].majortickseparation));
                graphics3d_text(axesobj, buffer, p, GRAPHICS_ALIGNCENTER, GRAPHICS_ALIGNTOP);
            }

        }
    }
    
    return (expression *) ref;
}

expression *plot3d_tographicsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    /* TODO: Add axes */
    return graphics3d_tographics(obj, context, nargs, args);
}

expression *plot3d_setplotrangei(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot3d_object *pobj=(plot3d_object *) obj;
    float xrange[2],yrange[2],zrange[2];
    expression_list *exp = (expression_list *) args[0];
    
    /* Make sure we're passed a list */
    if (nargs==1) if (exp) if (exp->type==EXPRESSION_LIST) {
        /* If the first element is numerical, assume this is the yrange */
        if (eval_isnumerical(((expression *) (exp->list->first->data)))) {
            /* Read the list into a float array */
            if (eval_listtofloatarray((expression_list *) args[0], zrange, 2)) {
                pobj->plotrange.p1[2]=zrange[0];
                pobj->plotrange.p2[2]=zrange[1];
            }
        } else if (eval_islist(((expression *) (exp->list->first->data)))) {
            /* Otherwise, set both xrange and yrange */
            expression_list *list1 = (expression_list *) eval_getelement(exp, 1);
            expression_list *list2 = (expression_list *) eval_getelement(exp, 2);
            expression_list *list3 = (expression_list *) eval_getelement(exp, 3);
            
            if ((list1)&&(list2)&&(list3)&&(list1->type==EXPRESSION_LIST)&&(list2->type==EXPRESSION_LIST)&&(list3->type==EXPRESSION_LIST)) {
                /* Read the list into a float array */
                if (eval_listtofloatarray(list1, xrange, 2) &&
                    eval_listtofloatarray(list2, yrange, 2) &&
                    eval_listtofloatarray(list3, zrange, 2)) {
                    pobj->plotrange.p1[0]=xrange[0];
                    pobj->plotrange.p2[0]=xrange[1];
                    
                    pobj->plotrange.p1[1]=yrange[0];
                    pobj->plotrange.p2[1]=yrange[1];
                    
                    pobj->plotrange.p1[2]=zrange[0];
                    pobj->plotrange.p2[2]=zrange[1];
                }
            }
        }
    }
    
    /* Update ticks record */
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[0], pobj->plotrange.p1[0], pobj->plotrange.p2[0]);
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[1], pobj->plotrange.p1[1], pobj->plotrange.p2[1]);
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[2], pobj->plotrange.p1[2], pobj->plotrange.p2[2]);
    
    return (expression *) class_newobjectreference(obj);
}

expression *plot3d_getplotrangei(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot3d_object *pobj=(plot3d_object *) obj;
    expression_list *xrange=NULL;
    expression_list *yrange=NULL;
    expression_list *zrange=NULL;
    
    xrange=newexpressionlistfromargs(2,newexpressionfloat(pobj->plotrange.p1[0]),newexpressionfloat(pobj->plotrange.p2[0]));
    yrange=newexpressionlistfromargs(2,newexpressionfloat(pobj->plotrange.p1[1]),newexpressionfloat(pobj->plotrange.p2[1]));
    zrange=newexpressionlistfromargs(2,newexpressionfloat(pobj->plotrange.p1[2]),newexpressionfloat(pobj->plotrange.p2[2]));
    
    return (expression *) newexpressionlistfromargs(3,xrange,yrange,zrange);
}

expression *plot3d_new(object *obj, interpretcontext *context, int nargs, expression **args) {
    plot3d_object *pobj = (plot3d_object *) obj;
    
    pobj->list=linkedlist_new();
    pobj->axeslabels[0]=NULL;
    pobj->axeslabels[1]=NULL;
    pobj->axeslabels[2]=NULL;

    pobj->viewdirection[0]=1.0;
    pobj->viewdirection[1]=1.0;
    pobj->viewdirection[2]=1.0;

    pobj->viewvertical[0]=0.0;
    pobj->viewvertical[1]=0.0;
    pobj->viewvertical[2]=1.0;

    pobj->plotrange.p1[0]=0.0;
    pobj->plotrange.p1[1]=0.0;
    pobj->plotrange.p1[2]=0.0;
    pobj->plotrange.p2[0]=1.0;
    pobj->plotrange.p2[1]=1.0;
    pobj->plotrange.p2[2]=1.0;

    pobj->cliptrianglesbynormal=FALSE;
    pobj->showmesh=FALSE;
    
    /* Update ticks record */
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[0], pobj->plotrange.p1[0], pobj->plotrange.p2[0]);
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[1], pobj->plotrange.p1[1], pobj->plotrange.p2[1]);
    plot_determineticks((plot_ticksrecord *) &pobj->ticks[2], pobj->plotrange.p1[2], pobj->plotrange.p2[2]);
    
    return NULL;
}
    
expression *plot3d_free(object *obj, interpretcontext *context, int nargs, expression **args) {
    if (context==NULL) {
        plot3d_object *pobj=(plot3d_object *) obj;
        if (pobj) {
            if (pobj->axeslabels[0]) EVAL_FREE(pobj->axeslabels[0]);
            if (pobj->axeslabels[1]) EVAL_FREE(pobj->axeslabels[1]);
            if (pobj->axeslabels[2]) EVAL_FREE(pobj->axeslabels[2]);
            graphics3d_free(obj,context,nargs,args);
        }
    }
    return NULL;
}

/*
 * Serialization
 */

expression *plot3d_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtable *ht=hashcreate(6);
//    plot3d_object *pobj=(plot3d_object *) obj;
    expression *ret=NULL;
//    hashtable *tickht=NULL;
    
//    pobj->
    
    if (ht) {
//        hashinsert(ht, "aspectratio", newexpressionfloat((double) pobj->));
        
/*        hashinsert(ht, "aspectratio", newexpressionfloat((double) pobj->aspectratio));
        hashinsert(ht, "axeslabels", newexpressionlistfromargs(2, newexpressionstring(pobj->axeslabels[0]), newexpressionstring(pobj->axeslabels[1])));
        hashinsert(ht, "framed", newexpressionbool(pobj->framed));
        hashinsert(ht, "plotrange", graphics_bboxtolist(&pobj->plotrange));
        
//        tickht=plot_ticksrecordtohashtableobject(&pobj->ticks[0]);
        hashinsert(ht, "xticks", newexpressionlistfromhashtable(tickht));
        hashmap(tickht, evalhashfreeexpression, NULL);
        hashfree(tickht);
        
//        tickht=plot_ticksrecordtohashtableobject(&pobj->ticks[1]);
        hashinsert(ht, "yticks", newexpressionlistfromhashtable(tickht));
        hashmap(tickht, evalhashfreeexpression, NULL);
        hashfree(tickht);
        
        graphics_serializetohashtable((graphics_object *) obj, ht);
        */
        
        /* Serialize the hashtable */
        ret=class_serializefromhashtable(context, ht, obj->clss->name);
        
        /* Free the hashtable and all associated expressions */
        hashmap(ht, evalhashfreeexpression, NULL);
        hashfree(ht);
    }
    
    return ret;
}

/*
 * Intrinsic functions
 */

expression *plot3d_surfaceplot(interpretcontext *context, int nargs, expression **args) {
    /* Initial validation*/
    if (nargs<3) return NULL;
    if ((args[1]->type!=EXPRESSION_LIST)||(args[2]->type!=EXPRESSION_LIST)) return NULL;
    
    /* Local variables */
    interpretcontext *options=NULL;
    expression_objectreference *plot=(expression_objectreference *) plot3d_newplot();
    expression_objectreference *colormap=NULL;
    plot3d_object *plot3d_obj=NULL;
    interpretcontext *plotctxt=newinterpretcontext(context, 1);
    floatcountertype *counter[2]= {NULL,NULL};
    expression *ret=NULL;
    graphics3d_trianglecomplexelement *trianglecomplex=NULL;
    float min=0, max=0; //, range=0;
    unsigned int k;
    int success=FALSE;
    double col[3];
    float norm[3];
    
    if ((!plotctxt)||(!plot)) goto plot3d_surfaceplot_cleanup;
    plot3d_obj=(plot3d_object *) plot->obj;
    
    options=class_options((object *) plot3d_obj,context,4,nargs,args);
    
    /* Interpret the counter arguments */
    counter[0]=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[1], TRUE);
    counter[1]=(floatcountertype *) eval_listtocounter(context, (expression_list *) args[2], TRUE);
    if (!counter[0]||!counter[1]) goto plot3d_surfaceplot_cleanup;
    
    /* Create the appropriate triangle complex */
    trianglecomplex=graphics3d_trianglecomplex((graphics3d_object *) plot3d_obj, (counter[0]->nsteps)*(counter[1]->nsteps), (counter[0]->nsteps-1)*(counter[1]->nsteps-1)*2);
    if (!trianglecomplex) goto plot3d_surfaceplot_cleanup;
    
    k=0; min=0;max=0;
    eval_counterreset((genericcountertype *)counter[1]);
    /* Evaluate the function at all points */
    do {
        eval_counterstore(plotctxt, (genericcountertype *) counter[1]);
        
        eval_counterreset((genericcountertype *)counter[0]);
        do {
            eval_counterstore(plotctxt, (genericcountertype *) counter[0]);
            
            ret=interpretexpression(plotctxt, args[0]);
            if (eval_isnumerical(ret)) {
                float value=eval_floatvalue(ret);
                trianglecomplex->points[3*k] = (float) counter[0]->value;
                trianglecomplex->points[3*k+1] = (float) counter[1]->value;
                trianglecomplex->points[3*k+2] = value;
                if (k==0) {
                    min=value; max=value;
                } else {
                    if (value<min) min=value;
                    if (value>max) max=value;
                }
            } else if (eval_islist(ret)) {
                float coords[3];
                
                if (eval_listtofloatarray((expression_list *) ret, coords, 3)) {
                    for (unsigned int j=0; j<3; j++) trianglecomplex->points[3*k+j] = (float) coords[j];
                    /*printf("{");
                    for (unsigned int j=0; j<3; j++) {
                        printf("%f",coords[j]);
                        if (j<2) printf(", ");
                    }
                    printf("},\n");*/
                }
            }
            
            if (ret) freeexpression(ret);
            k++;
        } while (!eval_counteradvance((genericcountertype *) counter[0]));
        
    } while (!eval_counteradvance((genericcountertype *) counter[1]));
    
    /* Look up colormap */
    colormap = (expression_objectreference *) lookupsymbol(options, "colormap");
    
    /* Rescale plot values and color */
 //   range=max-min;
    for (unsigned int i=0; i<k; i++) {
/*        float value=trianglecomplex->points[3*i+2];
        trianglecomplex->pointcolors[i*3]= (value-min)/range;
        trianglecomplex->pointcolors[i*3+1]=trianglecomplex->pointcolors[i*3];
        trianglecomplex->pointcolors[i*3+2]=trianglecomplex->pointcolors[i*3];*/
        trianglecomplex->pointcolors[i*3]=1.0;
        trianglecomplex->pointcolors[i*3+1]=1.0;
        trianglecomplex->pointcolors[i*3+2]=1.0;
        
        if (colormap) {
            expression *val =newexpressionfloat((double) trianglecomplex->points[3*i+2]);
            if (val) {
                if (color_rgbfromobject(context, colormap->obj, 1, &val, col)) {
                    trianglecomplex->pointcolors[i*3]=(float) col[0];
                    trianglecomplex->pointcolors[i*3+1]=(float) col[1];
                    trianglecomplex->pointcolors[i*3+2]=(float) col[2];
                }
                freeexpression(val);
            }
        }
    }
    
    /* Zero vertex normals */
    for (unsigned int i=0; i<3*trianglecomplex->npoints; i++) trianglecomplex->pointnormals[i]=0.0;
        
    /* 
     * Construct the triangle data from the plot
     *     |   |   |
     * i+1 * - * - * -
     *     |B/A| / |   (•) normal out of paper
     * i   * - * - * -
     *     j   j+1
     */
    k=0;
    for (unsigned int i=0;i<counter[0]->nsteps-1;i++) {
        for (unsigned int j=0;j<counter[1]->nsteps-1;j++) {
            /* Triangle A (see above diagram) */
            trianglecomplex->triangles[k*3]=(i*counter[0]->nsteps)+j;
            trianglecomplex->triangles[k*3+1]=(i*counter[0]->nsteps)+j+1;
            trianglecomplex->triangles[k*3+2]=((i+1)*counter[0]->nsteps)+j+1;
            graphics3d_trianglenormal(trianglecomplex->points, trianglecomplex->triangles+k*3, norm);
            
            /* Add the normal to all the vertices on this triangle */
            for (unsigned int l=0;l<3;l++) graphics3d_vectoradd(trianglecomplex->pointnormals+3*trianglecomplex->triangles[3*k+l], norm, trianglecomplex->pointnormals+3*trianglecomplex->triangles[3*k+l]);
            
            k++;
            /* Triangle B (see above diagram) */
            trianglecomplex->triangles[k*3]=(i*counter[0]->nsteps)+j;
            trianglecomplex->triangles[k*3+1]=((i+1)*counter[0]->nsteps)+j+1;
            trianglecomplex->triangles[k*3+2]=((i+1)*counter[0]->nsteps)+j;
            graphics3d_trianglenormal(trianglecomplex->points, trianglecomplex->triangles+k*3, norm);
            /* Add the normal to all the vertices on this triangle */
            for (unsigned int l=0;l<3;l++) graphics3d_vectoradd(trianglecomplex->pointnormals+3*trianglecomplex->triangles[3*k+l], norm, trianglecomplex->pointnormals+3*trianglecomplex->triangles[3*k+l]);
            k++;
        }
    }
    
    /* Normalize vertex normals */
    for (unsigned int i=0; i<trianglecomplex->npoints; i++) {
        graphics3d_normalizevector(trianglecomplex->pointnormals+3*i, trianglecomplex->pointnormals+3*i);
    }
    
  /*  for (unsigned int i=0; i<trianglecomplex->npoints; i++) {
        printf("(%f %f %f) (%f %f %f) [%f %f %f]\n",trianglecomplex->points[3*i],trianglecomplex->points[3*i+1],trianglecomplex->points[3*i+2],trianglecomplex->pointnormals[3*i],trianglecomplex->pointnormals[3*i+1],trianglecomplex->pointnormals[3*i+2],trianglecomplex->pointcolors[3*i],trianglecomplex->pointcolors[3*i+1],trianglecomplex->pointcolors[3*i+2]);
    }*/
    
    /* for (unsigned int i=0; i<trianglecomplex->ntriangles; i++) {
        printf("(%u %u %u)\n",trianglecomplex->triangles[3*i],trianglecomplex->triangles[3*i+1],trianglecomplex->triangles[3*i+2]);
    }*/
    
    /* Must free our reference to the colormap */
    if (colormap) freeexpression((expression *) colormap);
    
 //   plot_defaultparameters(plot_obj);
 //   plot_setaxestitles(plot_obj, counter[0]->name->name, counter[1]->name->name);
 //   plot_obj->framed=TRUE;
    
    success=TRUE;
    
plot3d_surfaceplot_cleanup:
    if (counter[0]) EVAL_FREE(counter[0]);
    if (counter[1]) EVAL_FREE(counter[1]);
    if (plotctxt) freeinterpretcontext(plotctxt);
    if (!success) if (plot) freeexpression((expression *) plot);
    if (options) freeinterpretcontext(options);
    
    return (expression *) plot;
}

/* 
 * Initialization
 */

void plot3dinitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(PLOT3D_LABEL, class_lookup(GRAPHICS3D_LABEL), sizeof(plot3d_object), 6);
    class_registerselector(cls, "tographics", FALSE, plot3d_tographicsi);
    class_registerselector(cls, "drawaxes", FALSE, plot3d_drawaxesi);
    class_registerselector(cls, "plotrange", FALSE, plot3d_getplotrangei);
    class_registerselector(cls, "setplotrange", FALSE, plot3d_setplotrangei);
    
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, plot3d_new);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, plot3d_free);
    
    //intrinsic("plotsurface", TRUE, plot3d_surfaceplot,NULL);
    
}
