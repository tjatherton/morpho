/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "classes.h"

hashtable *classes;

/* class_lookup - Looks up a class from a label.
 * Input:   (char *) label
 * Output:
 * Returns:
 */
classgeneric *class_lookup(char *label) {
    if (classes) return hashget(classes, label);
    return NULL;
}

/* class_classintrinsic - Declare an intrinsic class.
 * Input:   (char *) label      - The name of the class, as may be encountered by the new() function.
 *          (int) nselectors    - An estimate of the number of selectors implemented by this class.
 * Output:
 * Returns: A reference to the newly created class structure, for use with class_registerselector
 */
classintrinsic *class_classintrinsic(char *label, classgeneric *parent, size_t objectsize, int nselectors) {
    classintrinsic *nc = EVAL_MALLOC(sizeof(classintrinsic));
    if (nc) {
        nc->type=CLASS_INTRINSIC;
        nc->name=label;
        nc->parent=(classgeneric *) parent;
        nc->objectsize=objectsize;
        nc->selectors=hashcreate(nselectors);
        if (!nc->selectors) { EVAL_FREE(nc); return NULL; }
        hashinsert(classes, label, nc);
    }
    
    return nc;
}

/* class_classuser - Define a user-defined class.
 * Input:   (char *) label      - The name of the class, as may be encountered by the new() function.
 *          (expression *) def  - The definition
 * Output:
 * Returns: A reference to the newly created class structure, for use with class_registerselector
 */
classuser *class_classuser(char *label, expression *def) {
    expression_list *explist=(expression_list *) def;
    expression_name *parent=NULL;
    if ((!label)||(!def)||(def->type!=EXPRESSION_LIST)) return NULL;
    
    classuser *nc = EVAL_MALLOC(sizeof(classuser));
    if (nc) {
        nc->type=CLASS_USER;
        nc->parent=NULL;
        nc->name=NULL;
        nc->definitions=NULL;
        
        nc->name=EVAL_STRDUP(label);
        if (!nc->name) goto class_classuser_cleanup;
        
        nc->definitionstablesize=eval_listlength(explist);
        
        nc->definitions=newinterpretcontext(NULL, nc->definitionstablesize);
        if (!nc->definitions) goto class_classuser_cleanup;
        
        /* All user-defined objects have the same size */
        nc->objectsize=sizeof(objectuser);
        
        /* Evaluate definitions into the definition context */
        for (linkedlistentry *e=explist->list->first; e!=NULL; e=e->next) {
            freeexpression(interpretexpression(nc->definitions, (expression *) e->data));
        }
        
        /* Establish if a parent class is specified */
        parent=(expression_name *) lookupsymbol(nc->definitions, EVAL_SUPER);
        if (parent) {
            if (parent->type==EXPRESSION_NAME) {
                nc->parent=class_lookup(parent->name);
            }
        }
        
        if (nc->parent==NULL) nc->parent=class_lookup(EVAL_OBJECT); /* Otherwise all classes inherit from the object class */
        
        
        /* Insert into class table */
        hashinsert(classes, label, nc);
    }
    
    return nc;

class_classuser_cleanup:
    if (nc) {
        if (nc->name) EVAL_FREE(nc->name);
        if (nc->definitions) freeinterpretcontext(nc->definitions);
        EVAL_FREE(nc);
    }
    
    return NULL;
}

/* class_registerselector - Registers a selector for an intrinsic class.
 * Input:   (classintrinsic *) cls - The class to add the selector to. 
 *          (char *) label
 *          (int) hold
 *          (selectortype *) func
 * Output:
 * Returns:
 */
void class_registerselector(classintrinsic *cls, char *label, int hold, selectortype *func) {
    if ((!cls)||(!cls->selectors)) return;
    
    selector *old;
    selector *ns = EVAL_MALLOC(sizeof(selector));
    
    /*if (hold) {
        printf("Selector '%s' in class '%s' uses hold.\n", label, cls->name);
    }*/
    
    if (ns) {
        old=hashget(cls->selectors, label);
        if (old) {
#ifdef DEBUG
            printf("Warning: Selector '%s' in class '%s' is being redefined.\n", label, cls->name);
#endif
            if (old->exp) freeexpression(old->exp);
            EVAL_FREE(old); 
        }
        
        ns->hold=hold;
        ns->func=func;
        ns->type=CLASS_INTRINSIC;
        ns->exp=NULL;
        
        hashinsert(cls->selectors,label,ns);
    }
}

/* class_lookupselector - Looks up a selector.
 * Input:   (classgeneric *) cls - The class to search.
 *          (char *) label
 * Output:
 * Returns: (selector *) The selector, or NULL if not found.
 */
int class_lookupselector(classgeneric *cls, char *label, selector *sel) {

    if (label) for (classgeneric *c=cls; c!=NULL; c=c->parent) {
        if (c->type==CLASS_INTRINSIC) {
            selector *s=NULL;
            classintrinsic *ci=(classintrinsic *) c;
            s=hashget(ci->selectors,label);
            if (s) {
                if (sel) {
                    sel->func = s->func;
                    sel->exp=NULL;
                    sel->type=CLASS_INTRINSIC;
                    sel->hold=s->hold;
                }
                return TRUE;
            }
            
        } else if (c->type==CLASS_USER) {
            expression *exp;
            exp=findsymbol(((classuser *) c)->definitions, label);
            
            if ((exp)&&(exp->type==EXPRESSION_FUNCTION_DEFINITION)) {
                if (sel) {
                    sel->exp=exp;
                    sel->type=CLASS_USER;
                    sel->func=NULL;
                    sel->hold=FALSE;
                }
                return TRUE;
            }
        }
    }

    return FALSE;
}

/* class_free - Frees data associated with a class object.
 * Input:   (classgeneric *) cls
 * Output:
 * Returns:
 */
void class_free(classgeneric *cls) {
    if (!cls) return;
    
    switch (cls->type) {
        case CLASS_INTRINSIC:
            {
                classintrinsic *clsint=(classintrinsic *) cls;
                if (clsint->selectors) {
                    /* Free the selector definitions */
                    hashmap(clsint->selectors,class_hashfreeselectors,NULL);
                    hashfree(clsint->selectors);
                }
            }
            break;
        case CLASS_USER:
            {
                classuser *clsusr=(classuser *) cls;
                if (clsusr->definitions) {
                    freeinterpretcontext(clsusr->definitions);
                }
                
                if (clsusr->name) EVAL_FREE(clsusr->name);
            }
            break;
    }
    
    EVAL_FREE(cls);
}

void class_hashfreeselectors(char *name, void *sel, void *ref) {
    selector *s=(selector *) sel;
    
    if (s->exp) freeexpression(s->exp);
    EVAL_FREE(s);
}

void class_hashfree(char *name, void *cls, void *ref) {
    class_free((classgeneric *) cls);
}

void class_copydefinitions(char *name, void *element, void *obj) {
    objectuser *uobj = (objectuser *) obj;
    expression *el = (expression *) element;
    expression_name nameexp;
    
    /* Lightweight name */
    nameexp.type=EXPRESSION_NAME;
    nameexp.name=name;
    
    if (el->type!=EXPRESSION_FUNCTION_DEFINITION) {
        freeexpression(opassign(uobj->data, (expression *) &nameexp, el));
    }
}

/* class_instantiate - Creates an object from a class
 * Input:   (classgeneric *cls) - the class to instantiate
 * Output:
 * Returns: (object *) - a pointer to a newly created object.
 */
object *class_instantiate(classgeneric *cls) {
    if (!cls) return NULL;
    
    object *obj=EVAL_MALLOC(cls->objectsize);
    
    if (obj) {
        obj->clss=cls;
        obj->refcnt=1; /* If the new selector happens to return a reference to this object it will force
                          freeing. Setting refcnt to 1 prevents this and corrects the record later. 
                          Formally, of course, all new() selectors should always return NULL. */
        obj->listeners=NULL;
        obj->listeningto=NULL;
        
        if (cls->type==CLASS_USER) {
            objectuser *uobj = (objectuser *) obj;
            classuser *ucls = (classuser *) cls;
            
            /* Copy across data from class definition */
            uobj->data=newinterpretcontext(defaultinterpretcontext, ucls->definitionstablesize);
            hashmap(ucls->definitions->symbols,class_copydefinitions,uobj);
        }
        
        /* Call the new selector, bypassing the normal interfaces for performance reasons. */
        selector s;
        if (class_lookupselector(obj->clss, EVAL_NEWSELECTORLABEL, &s)) {
            if (obj->clss->type==CLASS_INTRINSIC) {
                // This is a *fast* but hacky way of calling a function, specifically used here because it's done so frequently
                s.func(obj,NULL,0,NULL);
            } else {
                expression *newret = class_callselector(NULL, obj, EVAL_NEWSELECTORLABEL, 0);
                if (newret) freeexpression(newret);
            }
        }
        
        obj->refcnt=0;
    }
    
    return obj;
}

/* Lightweight instantiation used for performance reasons in a few internal classes (e.g. multivector) */
object *class_lwinstantiate(classgeneric *cls) {
    if (!cls) return NULL;
    
    object *obj=EVAL_MALLOC(cls->objectsize);
    
    if (obj) {
        obj->clss=cls;
        obj->refcnt=1; /* If the new selector happens to return a reference to this object it will force
                        freeing. Setting refcnt to 1 prevents this and corrects the record later.
                        Formally, of course, all new() selectors should always return NULL. */
        obj->listeners=NULL;
        obj->listeningto=NULL;
        obj->refcnt=0;
    }
    
    return obj;
}

/* class_newi - Intrinsic function to instantiate a class
 * Input:   (interpretcontext *context) - current eval context
 *          (int) nargs                 - number of arguments passed
 *          (expression **) args        - arguments
 * Output:
 * Returns: (object *) - a pointer to a newly created object.
 */
expression *class_newi(interpretcontext *context, int nargs, expression **args) {
    classgeneric *clss;
    object *obj;
    expression_objectreference *ref=NULL;

    
    /* Validate arguments */
    if ((nargs!=1)||(!args[0])||(args[0]->type!=EXPRESSION_NAME)) return NULL;
    
    /* Find the class */
    clss=class_lookup(((expression_name *) args[0])->name);
    
    if (clss) {
        obj=class_instantiate(clss);
        if (obj) ref=class_newobjectreference(obj);
    } else {
        sprintf(error_buffer(), ERROR_CLASSNOTDEFINED_MSG,((expression_name *) args[0])->name);
        error_raise(context, ERROR_CLASSNOTDEFINED, error_buffer(), ERROR_FATAL);
    }
    
    return (expression *) ref;
}

/* class_objecti - Intrinsic function to create an object
 *                 Mostly intended for serialization purposes
 * Input:   (interpretcontext *context) - current eval context
 *          (int) nargs                 - number of arguments passed
 *          (expression **) args        - arguments
 * Output:
 * Returns: (object *) - a pointer to a newly created object.
 */
expression *class_objecti(interpretcontext *context, int nargs, expression **args) {
    if (nargs<1) return NULL;
    if (!eval_isname(args[0])) return NULL;
    
    object *obj=NULL;
    expression *ret=NULL;
    interpretcontext *newcontext=NULL;
    expression_objectreference *hash=NULL;

    newcontext=eval_options(context, 2, nargs, args);
    
    if (newcontext) {
        hash=class_hashtable_objectfromhashtable(newcontext->symbols);
        
        if (hash) {
            obj=class_instantiate(class_lookup(((expression_name *) args[0])->name));
        
            if (class_objectrespondstoselector(obj, EVAL_DESERIALIZE)) {
                class_callselector(context, obj, EVAL_DESERIALIZE, 1, EXPRESSION_OBJECT_REFERENCE, hash);
            }
            
            ret=(expression *) class_newobjectreference(obj);
        }
    }
    
    /* If we successively created a hashtable object, free it. Only free the context structure,
       as its contents are now part of the hashtable object. */
    if (hash) {
        freeexpression((expression *) hash);
        EVAL_FREE(newcontext);
    } else {
        /* Otherwise get rid of the context normally. */
        freeinterpretcontext(newcontext);
    }
            
    return ret;
}

/*
 * Object references
 */

/* class_newobjectreference - Creates a reference to an object
 * Input:   (object *obj) - the object to refer to
 * Output:
 * Returns: (expression_objectreference *) - a pointer to a newly created object reference.
 */
expression_objectreference *class_newobjectreference(object *obj) {
    if (!obj) return NULL;
    
    expression_objectreference *ref=EVAL_MALLOC(sizeof(expression_objectreference));
    
    if (ref) {
        ref->type=EXPRESSION_OBJECT_REFERENCE;
        ref->obj=obj;
        obj->refcnt++;
    }
        
    return ref;
}

void class_freeobject(object *obj) {
    /* Call the free selector, bypassing the normal interfaces. */
    selector s;
    if (class_lookupselector(obj->clss, EVAL_FREESELECTORLABEL, &s)) {
        if (obj->clss->type==CLASS_INTRINSIC) {
            // This is a *fast* but hacky way of calling a function, specifically used here because it's done so frequently
            s.func(obj,NULL,0,NULL);
        } else {
            freeexpression(class_callselector(NULL, obj, EVAL_FREESELECTORLABEL, 0));
        }
    }
    
    /* Free any attached listeners */
    class_destroylisteners(obj);
    
    /* Inform any objects we're listening to that we're vanishing, so don't call us... */
    if (obj->listeningto) {
        linkedlistentry *next=NULL;
        for (linkedlistentry *e = obj->listeningto->first; e!=NULL; e=next) {
            next=e->next;
            class_removelistenerforallselectors(e->data, obj);
        }
        /* And remove the listeningto list */
        linkedlist_free(obj->listeningto);
    }
    
    if (obj->clss->type==CLASS_USER) {
        freeinterpretcontext(((objectuser *) obj)->data);
    }
    
    /* Free the data itself */
    EVAL_FREE(obj);
}

/* class_freeobjectreference - Frees data attached to an object reference expression, checking if the object
 *                             is needed any longer. 
 * N.B- It is anticipated that this function will only be called by freeexpression().
 * Input:   (expression_objectreference *) ref - expression to be freed
 * Output:
 * Returns:
 */
void class_freeobjectreference(expression_objectreference *ref) {
    if (ref) {
        ref->obj->refcnt--;
        if (ref->obj->refcnt<1) class_freeobject(ref->obj);
    }
    /* Does not free ref as this is done inside expression_free */
}

/*
 * Retain and release manually increment and decrement the reference counting system for an object.
 * This should very rarely be needed--use with extreme caution only where objects are being referred to by things
 * outside a morpho kernel, e.g. a gui.
 */

void class_retain(object *obj) {
    if (obj) obj->refcnt++;
}

void class_release(object *obj) {
    if (obj) {
        obj->refcnt--;
        if (obj->refcnt<1) class_freeobject(obj);
    }
}


/* 
 *  Call selectors
 */

/* Check whether an object responds to a selector */
/*int class_objectrespondstoselector(object *obj, char *label) {
    if (!obj || !label) return FALSE;
    
    return class_lookupselector(obj->clss, label, NULL);
}*/

/* Perform a selector */
expression *class_performselector(interpretcontext *context, object *obj, expression_function *sfunc) {
    expression *ret=NULL;
    selector s;
    interpretcontext *ctxt=NULL;
    char *name=NULL;
    expression *nexp=NULL;
    int success=FALSE;

    if ((obj==NULL)||(sfunc==NULL)) return NULL;
    
    /* Resolve name */
    if (eval_isname(sfunc->name)) {
        name=((expression_name *) sfunc->name)->name;
    } else {
        nexp=interpretexpression(context, sfunc->name);
        if (eval_isname(nexp)) {
            name=((expression_name *) nexp)->name;
        }
    }
    
    success=class_lookupselector(obj->clss, name, &s);
    if (nexp) freeexpression(nexp);
    
    if (!name) {
        sprintf(error_buffer(),ERROR_CANTRESOLVEINDEXNAME_MSG);
        error_raise(context, ERROR_CANTRESOLVEINDEXNAME, error_buffer(), ERROR_FATAL);
    }
    
    if (success) {
        switch (s.type) {
            case CLASS_INTRINSIC:
                {
                    expression *args[sfunc->nargs+1];
                    
                    if (context) ctxt=newinterpretcontext(context, EVAL_LOCALCONTEXTTABLESIZE);
                    
                    if (s.hold) { /* Simply copy the arguments, if hold is true */
                        for (int i=0; i<sfunc->nargs; i++) args[i]=cloneexpression(sfunc->argslist[i]);
                    } else { /* Evaluate arguments otherwise */
                        /* !!TODO: Which context should function arguments be evaluated in? */
                        for (int i=0; i<sfunc->nargs; i++) args[i]=interpretexpression(context,sfunc->argslist[i]);
                    }
                    
                    ret=s.func(obj,ctxt,sfunc->nargs,args);
                    
                    /* Delete arguments */
                    for (int i=0; i<sfunc->nargs; i++) freeexpression(args[i]);
                }
                break;
            case CLASS_USER:
                if (s.exp->type==EXPRESSION_FUNCTION_DEFINITION) {
                    /* Patch in the object data as a parent to the function call context.
                       opassign specifically looks for a parent that has insideselector to make
                       assignment to object values inside selectors work properly. */
                    interpretcontext objctxt;
                    objctxt.parent=context;
                    objctxt.symbols=((objectuser *)obj)->data->symbols;
                    objctxt.insideselector=TRUE;
                    objctxt.errorenv=NULL;
                    objctxt.freerecords=NULL;
                    
                    ret=eval_calluserfunction(&objctxt,sfunc,(expression_functiondefinition *) s.exp);
                }
                break;
        }
    } else {
        sprintf(error_buffer(),ERROR_CLASSNOTRESPONDTOSEL_MSG, obj->clss->name, name);
        error_raise(context, ERROR_CLASSNOTRESPONDTOSEL, error_buffer(), ERROR_FATAL);
    }
    
    /* Inform any listeners */
    if (obj->listeners) {
        linkedlist *l=hashget(obj->listeners, name);
        /* If this is a selector targetted for listening, call all listeners */
        if (l) {
            expression_objectreference *this=class_newobjectreference(obj);
            
            for (linkedlistentry *e=l->first; e!=NULL; e=e->next) {
                class_listener *listen=(class_listener *) e->data;
                expression *lret=NULL;
                if (listen && ret) {
                    lret=class_callselector(context, listen->listener, listen->reportselector->name, 2, EXPRESSION_OBJECT_REFERENCE, this, ret->type, ret);
                } else {
                    lret=class_callselector(context, listen->listener, listen->reportselector->name, 1, EXPRESSION_OBJECT_REFERENCE, this);
                }
                
                if (lret) freeexpression(lret);
            }
            
            freeexpression((expression *) this);
        }
    }
    
    if (ctxt) freeinterpretcontext(ctxt);
        
    return ret;
}

/* Performs a selector on a class [An operator] */
expression *class_selectorop(interpretcontext *context, expression *left, expression *right) {
    if ((left==NULL)||(right==NULL)||(right->type!=EXPRESSION_FUNCTION)) return NULL;
    
    expression_objectreference *ref=NULL;
    expression_function *sfunc = (expression_function *) right;

    expression *ret=NULL;
    
    if (left->type==EXPRESSION_NAME) {
        ref=(expression_objectreference *) lookupsymbol(context, ((expression_name *)left)->name);
    } else {
        ref=(expression_objectreference *) interpretexpression(context, left);
    }
    
    if ((ref) && (ref->type==EXPRESSION_OBJECT_REFERENCE)) {
        /* Perform the selector */
        ret=class_performselector(context,ref->obj, sfunc);
                
        freeexpression((expression *) ref);
    } else {
        error_raise(context, CLASS_COULDNTRESOLVEOBJ, CLASS_COULDNTRESOLVEOBJ_MSG, ERROR_FATAL);
    }
    
    return ret;
}

/* call - Calls a user-defined selector on a specified object with literal arguments
 *        One of the public interfaces to the interpreter.
 * Input:   interpretcontext *context - current context
 *          object *obj             - The object to address
 *          (char *) name           - Name of the function
 *          int nargs               - Number of type/argument pairs.
 *          <variable number of arguments should then follow in pairs>
 *          Each pair should be:
 *              expressiontype type - Type of parameter
 *              void *value         - pointer to value
 * Output:
 * Returns: (expression *)          - the result of the interpretation, newly allocated.
 */
expression *class_callselector(interpretcontext *context, object *obj, char *name, unsigned int nargs, ...) {
    va_list a;
    expression *ret=NULL;
    expression *args[nargs];
    expressiontype type;
    expression_function func;
    int argsok=TRUE;
    unsigned int i;
    
    /* Add arguments to the local context */
    va_start(a, nargs);
    for (i=0; i<nargs && argsok; i++) {
        type = va_arg(a, expressiontype);
        switch (type) {
            case EXPRESSION_FLOAT:
                args[i]=newexpressionfloat(va_arg(a, double));
                break;
            case EXPRESSION_INTEGER:
                args[i]=newexpressioninteger(va_arg(a, int));
                break;
            case EXPRESSION_BOOL:
                args[i]=newexpressionbool(va_arg(a, int));
                break;
            case EXPRESSION_COMPLEX:
                args[i]=newexpressioncomplex(va_arg(a, complex double));
                break;
            case EXPRESSION_STRING:
                args[i]=newexpressionstring(va_arg(a, char*));
                break;
            case EXPRESSION_NAME:
                args[i]=newexpressionname(va_arg(a, char*));
                break;
            case EXPRESSION_OBJECT_REFERENCE:
            case EXPRESSION_LIST:
            case EXPRESSION_FUNCTION:
            case EXPRESSION_FUNCTION_DEFINITION:
            case EXPRESSION_INDEX:
                args[i]=cloneexpression(va_arg(a, expression*));
                break;
            default:
                args[i]=NULL;
                argsok=FALSE;
                break;
        }
    }
    va_end(a);
    
    /* Set up the function call */
    expression_name nm;
    nm.type=EXPRESSION_NAME;
    nm.name=name;
    func.type=EXPRESSION_FUNCTION;
    func.name=(expression *) &nm;
    func.nargs=nargs;
    func.argslist=args;
    
    /* Perform the selector */
    ret=class_performselector(context, obj, &func);
    
    /* Free the arguments */
    for (i=0;i<nargs;i++) freeexpression(args[i]);
    
    return ret;
}

/* class_callselectorwithargslist - Calls a user-defined selector on a specified object with a list of arguments
 *        One of the public interfaces to the interpreter.
 * Input:   interpretcontext *context - current context
 *          object *obj             - The object to address
 *          (char *) name           - Name of the function
 *          int nargs               - Number of type/argument pairs.
 *          expression **args       - a set of arguments.
 * Output:
 * Returns: (expression *)          - the result of the interpretation, newly allocated.
 */
expression *class_callselectorwithargslist(interpretcontext *context, object *obj, char *name, unsigned int nargs, expression **args) {
    expression_function func;
    
    /* Set up the function call */
    expression_name nm;
    nm.type=EXPRESSION_NAME;
    nm.name=name;
    func.type=EXPRESSION_FUNCTION;
    func.name=(expression *) &nm;
    func.nargs=nargs;
    func.argslist=args;
    
    /* Perform the selector */
    return class_performselector(context, obj, &func);
}

/*
 *
 */

int class_objectisofclass(expression *ref, char *name) {
    classgeneric *cls = class_lookup(name);
    int ret = FALSE;
    
    if (eval_isobjectref(ref)) {
        if (eval_objectfromref(ref)->clss == cls) ret=TRUE;
    }
    
    return ret;
}

/* Map function for class_options */
typedef struct {
    interpretcontext *context;
    object *obj;
} class_optionsmapdata;

void class_optionsmapfunction(char *name, expression *exp, class_optionsmapdata *data) {
    if (!((name)&&(exp)&&(data))) return;
    
    size_t len = strlen(name);
    char sel[len+4];
    
    strcpy(sel,"set");
    strcpy(sel+3,name);
    sel[len+3]='\0';
    
    if (class_objectrespondstoselector(data->obj, sel)) {
        freeexpression(class_callselectorwithargslist(data->context, data->obj, sel, 1, &exp));
    }
}

/* class_options - process option arguments for intrinsic classes
 *                 Similar to eval_options, except this version checks each option OPTION=XXXX to find
 *                 a selector called setOPTION, which is then called with argument XXXX.
 *
 * Input:   (object *) obj  - the object to set options for
 *          (interpretcontext *) context - the current context
 *          (int) start     - index of the first argument that is an option [counting from 1 as the 1st argument]
 *          (int) narg      - number of arguments
 *          (expression *) args[] - the arguments
 * Output:
 * Returns: (interpretcontext *) a context containing the arguments
 */
interpretcontext *class_options(object *obj, interpretcontext *context, int start, int narg, expression **args) {
    interpretcontext *ret=NULL;
    class_optionsmapdata data = {context, obj};
    
    if (start<=narg) {
        ret = newinterpretcontext(context, narg-start+1);
        if (ret) for(int i=start-1; i<narg; i++) freeexpression(interpretexpression(ret, args[i]));
    }
    
    if (ret && ret->symbols) hashmap(ret->symbols, (hashmapfunction *) class_optionsmapfunction , &data);
    
    return ret;
}

/*
 * Listeners
 */

/* New listener record, clones arguments so you can discard them. */
class_listener *class_newlistener(expression_objectreference *listener, expression_name *reportselector) {
    class_listener *l=NULL;
    
    if (eval_isobjectref(listener) && eval_isname(reportselector)) {
        l=EVAL_MALLOC(sizeof(class_listener));
        
        if (l) {
            l->listener=listener->obj;
            l->reportselector=(expression_name *) cloneexpression((expression *) reportselector);
        }
    }
 
    return l;
}

/* Free listner record */
void class_freelistener(class_listener *listener) {
    if (listener) {
        if (listener->reportselector) freeexpression((expression *) listener->reportselector);
    }
    EVAL_FREE(listener);
}

void class_freelistenermapfunction(char *name, void *data, void *ref) {
    linkedlist *l=(linkedlist *) data;
    object *this=ref;
    
    if (l) for (linkedlistentry *e=l->first; e!=NULL; e=e->next) {
        class_listener *l = e->data;
        
        /* Remove the object from listener's listeningto */
        if (l && l->listener && l->listener->listeningto) {
            linkedlist *r = l->listener->listeningto;
            
            if (linkedlist_position(r, this)) {
                linkedlist_removeentry(r, this);
            }
        }
        
        class_freelistener(l);
    }
}

/* Remove all listeners  */
void class_destroylisteners(object *obj) {
    if (obj->listeners) {
        hashmap(obj->listeners, class_freelistenermapfunction, obj);
        hashfree(obj->listeners);
        obj->listeners=NULL;
    }
}

/* Adds a listener to an object  */
int class_addlistener(object *obj, expression_name *selector, expression_objectreference *listener, expression_name *reportselector) {
    class_listener *l=NULL;
    linkedlist *llist=NULL;
    int success=FALSE;
    
    if (!obj->listeners) obj->listeners=hashcreate(1);
    
    if (obj->listeners) {
        l=class_newlistener(listener, reportselector);
        /* Are there any listeners already for this selector? */
        llist=hashget(obj->listeners, selector->name);
        
        /* If not, let's add a new list of them */
        if (!llist) {
            llist=linkedlist_new();
            if (llist) hashinsert(obj->listeners, selector->name, llist);
        }
        
        /* Does the listener have a listeningto list? */
        if (! listener->obj->listeningto) {
            listener->obj->listeningto = linkedlist_new();
        }
        
        /* Once we have both lists set up, add the listener record */
        if (llist && listener->obj->listeningto) {
            linkedlist_addentry(llist, l);
            
            /* And record a reference to object in the listeningto list for the listener  */
            if (!linkedlist_position(listener->obj->listeningto, obj)) linkedlist_addentry(listener->obj->listeningto, obj);
            success=TRUE;
        }
    }
    
    return success;
}

/* Removes a listener */
int class_removelistener(object *obj, expression_name *selector, expression_objectreference *listener) {
    int success=FALSE;
    linkedlist *llist=NULL;
    
    if ((selector) && (obj->listeners)) {
        llist=hashget(obj->listeners, selector->name);
        
        if (llist) {
            for (linkedlistentry *e=llist->first; e!=NULL; e=e->next) {
                class_listener *l = (class_listener *) e->data;
                if (l) {
                    if (listener->obj == l->listener) {
                        linkedlist_removeentry(llist, (void *) l);
                        class_freelistener(l);
                        success=TRUE;
                        break;
                    }
                }
            }
        }
    }
    
    return success;
}


void class_removelistenerforallselectorsmapfunction(char *name, linkedlist *llist, object *listener) {
    linkedlistentry *next=NULL;
    for (linkedlistentry *e = llist->first; e!=NULL; e=next) {
        next=e->next;
        class_listener *l = (class_listener *) e->data;
        if ((l)&&(listener == l->listener)) {
            linkedlist_removeentry(llist, (void *) l);
            class_freelistener(l);
        }
    }
}

/* Removes a listener from all selectors */
void class_removelistenerforallselectors(object *obj, object *listener) {
    
    if (obj->listeners) {
        hashmap(obj->listeners, (hashmapfunction *) class_removelistenerforallselectorsmapfunction, (void *) listener);
        
        /* Remove the object from the listeningto list of the listener. */
        if (listener->listeningto) {
            linkedlist_removeentry(listener->listeningto, obj);
        }
    }
}

/* 
 * Generic selectors
 */

/* Returns the class of the object */
expression *class_classi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    if ((obj)&&(obj->clss)) ret=newexpressionname(obj->clss->name);
    
    return ret;
}

/* Returns the superclass of the object */
expression *class_superi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    if ((obj)&&(obj->clss)&&(obj->clss->parent)) ret=newexpressionname(obj->clss->parent->name);
    
    return ret;
}

/* Tests whether an object responds to a particular selector */
expression *class_respondstoselectori(object *obj, interpretcontext *context, int nargs, expression **args) {
    int ret=FALSE;
    
    if ((nargs==1)&&(args[0]->type=EXPRESSION_NAME)) ret=class_objectrespondstoselector(obj, ((expression_name *)args[0])->name);
    
    return newexpressionbool(ret);
}

/* Deserializes an object from a given hashtable */
expression *class_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    objectuser *uobj=NULL;
    expression_list *objsymbols=NULL;
    hashtableobject *hash=NULL;
    expression *value=NULL;
    
    if ((obj)&&(obj->clss)&&(obj->clss->type==CLASS_USER)) {
        uobj=(objectuser *) obj;
        
        /* Check that the argument is an object */
        if ((nargs==1)&&(eval_isobjectref(args[0]))) {
            expression_objectreference *ref = (expression_objectreference *) args[0];
            
            /* and that it is a hashtable */
            if ((ref->obj)&&(ref->obj->clss==class_lookup(EVAL_HASHTABLE))) {
                hash = (hashtableobject *) ref->obj;
                
                /* Get an expression_list of all the symbols in the object's symbol table */
                objsymbols=(expression_list *) eval_symbols(uobj->data, 0, NULL);
                
                if (objsymbols) {
                    /* Loop over all symbols in the object's symbol table */
                    for (linkedlistentry *e=objsymbols->list->first; e!=NULL; e=e->next) {
                        expression_name *name = (expression_name *) e->data;
                        
                        /* If it's a name, look it up in the hashtable */
                        if (eval_isname(name)) {
                            value=hashget(hash->table, name->name);
                            
                            /* If found, assign the value from the hashtable to the object */
                            if (value) {
                                freeexpression(opassign(uobj->data, (expression *) name, value));
                            }
                        }
                    }
                    
                    freeexpression((expression *) objsymbols);
                }
                
            }
        }
    }
    
    return newexpressionnone();
}

/* A hashmap function to serialize the entries in a given hashtable */
void class_serializemapfunction(char *name, expression *exp, linkedlist *list) {
    expression *sname = newexpressionstring(name);
    expression *seq = newexpressionstring("=");
    expression *svalue = eval_tostring(defaultinterpretcontext, 1, &exp);
    expression *pieces[3] = {sname, seq, svalue};
    
    linkedlist_addentry(list, eval_stringjoin(defaultinterpretcontext, 3, pieces));
    
    freeexpression(sname);
    freeexpression(seq);
    freeexpression(svalue);
}

expression *class_serializefromhashtable(interpretcontext *context, hashtable *table, char *classname) {
    expression *ret=NULL;
    linkedlist *list = linkedlist_new();
    
    if ((list)&&(table)) {
        hashmap(table, (hashmapfunction *) class_serializemapfunction, list);
        
        expression *pieces[3+2*linkedlist_length(list)];
        unsigned int n=0;
        
        pieces[n]=newexpressionstring("object("); n++;
        pieces[n]=newexpressionstring(classname); n++;
        
        for (linkedlistentry *e=list->first; e!=NULL; e=e->next) {
            pieces[n]=newexpressionstring(", "); n++;
            pieces[n]=e->data; n++;
        }
        
        pieces[n]=newexpressionstring(")"); n++;
        
        ret=eval_stringjoin(context, n, pieces);
        for (unsigned int i=0; i<n; i++) freeexpression(pieces[i]);
        
        linkedlist_free(list);
    }
    
    return ret;
}

/* Generic serialization */
expression *class_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    objectuser *uobj;
    
    if ((obj)&&(obj->clss)) {
        switch (obj->clss->type) {
            case CLASS_INTRINSIC:
                error_raise(context, ERROR_CLASSCANTSERIALIZE, ERROR_CLASSCANTSERIALIZE_MSG, ERROR_FATAL);
                break;
            case CLASS_USER:
                uobj = (objectuser *) obj;
                ret=class_serializefromhashtable(context, uobj->data->symbols, uobj->clss->name);
                break;
        }
    }
    
    return ret;
}

/* Performs a selector */
expression *class_performselectori(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression_function sfunc;
    expression *sel;
    expression *ret=NULL;
    
    if (nargs>=1) {
        sel=interpretexpression(context, args[0]);
        
        if (eval_isname(sel)) {
            sfunc.type=EXPRESSION_FUNCTION;
            sfunc.name=sel;
            sfunc.argslist=&args[1];
            sfunc.nargs=nargs-1;
            
            ret=class_performselector(context, obj, &sfunc);
        }
        
        if (sel) freeexpression(sel);
    }
    
    return ret;
}

void class_selectorsi_user(char *name, void *el, void *ref) {
    linkedlist *list = (linkedlist *) ref;
    expression *exp = (expression *) el;
    
    if (exp->type==EXPRESSION_FUNCTION_DEFINITION) {
        linkedlist_addentry(list, newexpressionname(name));
    }
}

void class_selectorsi_intrinsic(char *name, void *el, void *ref) {
    linkedlist *list = (linkedlist *) ref;
    
    linkedlist_addentry(list, newexpressionname(name));
}

expression *class_selectorsi(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    expression *retsrt=NULL;
    interpretcontext *defs=NULL;
    hashtable *sels=NULL;
    
    if (!obj) return NULL;
    classgeneric *c=NULL;
    
    linkedlist *list = linkedlist_new();
    
    if (list) for (c=obj->clss; c!=NULL; c=c->parent) {
        switch (c->type) {
            case CLASS_INTRINSIC:
                sels=((classintrinsic *) c)->selectors;
                
                hashmap(sels, class_selectorsi_intrinsic, list);
                break;
                
            case CLASS_USER:
                defs=((classuser *) c)->definitions;
                
                hashmap(defs->symbols, class_selectorsi_user, list);
                break;
        }
    }
    
    ret=(expression *) newexpressionlist(list);
    
    if (ret) {
        retsrt=eval_sort(context, 1, &ret);
        freeexpression(ret);
    }
    
    linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
    linkedlist_free(list);
    
    return retsrt;
}

expression *class_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    
    if ((obj)&&(obj->clss)) switch (obj->clss->type) {
        case CLASS_INTRINSIC:
            sprintf(error_buffer(),ERROR_CLASSCANTCLONE_MSG);
            error_raise(context, ERROR_CLASSCANTCLONE, error_buffer(), ERROR_FATAL);
            break;
            
        case CLASS_USER:
            {
                objectuser *old = (objectuser *) obj;
                object *new=class_instantiate(obj->clss);
                
                /* Copy across data from class definition */
                hashmap(old->data->symbols,class_copydefinitions,new);
                
                ret=(expression *) class_newobjectreference(new);
            }
            break;
    }
    
    return ret;
}


expression *class_exporti(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *ret=NULL;
    char *type=NULL;
    
    switch (nargs) {
        case 1:
            {
                /* If one argument, extract the filetype from the name */
                expression_string *sexp = (expression_string *) args[0];
                if (sexp->type==EXPRESSION_STRING) {
                    for (type=sexp->value+strlen(sexp->value); type>=sexp->value; type--)
                        if ((*type)=='.') break;
                    if (*type=='.') type++; else type=NULL;
                }
            }
            break;
        case 2:
            {
                /* If two arguments, the second argument is the type */
                expression_name *nexp = (expression_name *) args[1];
                if (nexp->type==EXPRESSION_NAME) {
                    type=nexp->name;
                }
            }
            break;
    }
    
    if (type) {
        char sname[strlen(type)+strlen(EVAL_EXPORTLABEL)+1];
        sprintf(sname, "%s%s",EVAL_EXPORTLABEL,type);
        
        if (class_objectrespondstoselector(obj, sname)) {
            ret=class_callselectorwithargslist(context, obj, sname, 1, args);
        }
    }
    
    return ret;
}

expression *class_addlisteneri(object *obj, interpretcontext *context, int nargs, expression **args) {
    expression *target=NULL, *selector=NULL, *reportselector=NULL;
    unsigned int i;
    
    if (nargs!=3) {
        error_raise(context, CLASS_ADDLISTENERARGS, CLASS_ADDLISTENERARGS_MSG, ERROR_FATAL);
    }
    
    /* Identify arguments */
    for (i=0; i<nargs; i++) {
        if ((!target)&&(eval_isobjectref(args[i]))) target=args[i];
        if ((selector)&&(!reportselector)&&(eval_isname(args[i]))) reportselector=args[i];
        if ((!selector)&&(eval_isname(args[i]))) selector=args[i];
    }
    
    /* If obj actually responds to this selector... */
    if (class_objectrespondstoselector(obj, ((expression_name *) selector)->name)) {
        /* Then add the listener */
        class_addlistener(obj, (expression_name *) selector, (expression_objectreference *) target, (expression_name *) reportselector);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/*
 * Hashtable data structure
 */

#define HASHTABLE_INITIALSIZE 16

/* Wraps an internal hashtable structure in an object wrapper and returns a reference to it. */
expression_objectreference *class_hashtable_objectfromhashtable(hashtable *table) {
    hashtableobject *hobj=malloc(sizeof(hashtableobject));
    
    if (hobj) {
        hobj->clss=class_lookup(EVAL_HASHTABLE);
        hobj->refcnt=0;
        hobj->table=table;
        hobj->listeners=NULL;
        hobj->listeningto=NULL;
    }
    
    return class_newobjectreference((object *) hobj);
}

/* Initializes the hashtable */
expression *class_hashtable_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    
    hobj->table=hashcreate(HASHTABLE_INITIALSIZE);
    
    return NULL;
}

/* Frees the hashtable */
expression *class_hashtable_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    
    if (hobj->table) {
        hashmap(hobj->table, evalhashfreeexpression, NULL);
        hashfree(hobj->table);
    }
    
    return NULL;
}

/* Adds an entry */
expression *class_hashtable_addi(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    expression_string *key;
    
    if ((hobj)&&(hobj->table)&&(nargs==2)) {
        key = (expression_string *) args[0];
        if (eval_isstring(key)) {
            hashinsert(hobj->table, key->value, cloneexpression(args[1]));
        }
        
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Adds an entry using setindex */
expression *class_hashtable_setindexi(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    expression_string *key;
    
    if ((hobj)&&(hobj->table)&&(nargs==2)) {
        key = (expression_string *) args[1];
        if (eval_isstring(key)) {
            hashinsert(hobj->table, key->value, cloneexpression(args[0]));
        }
        
    }
    
    return (expression *) class_newobjectreference(obj);
}


/* Removes an entry */
expression *class_hashtable_removei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    expression_string *key;
    
    if ((hobj)&&(hobj->table)&&(nargs==1)) {
        key = (expression_string *) args[0];
        if (eval_isstring(key)) {
            hashremove(hobj->table, key->value);
        }
        
    }
    
    return (expression *) class_newobjectreference(obj);
}

/* Looks up an entry */
expression *class_hashtable_lookupi(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    expression_string *key=NULL;
    expression *value=NULL;
    
    if ((hobj)&&(hobj->table)&&(nargs==1)) {
        key = (expression_string *) args[0];
        if (eval_isstring(key)) {
            value=hashget(hobj->table, key->value);
            if (value) return cloneexpression(value);
            else return newexpressionnone(); 
        }
        
    }
    
    return NULL;
}

/* Serialize */
expression *class_hashtable_serializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj = (hashtableobject *) obj;
    
    return class_serializefromhashtable(context, hobj->table, hobj->clss->name);
}

/* Deserialize */
void class_hashtable_copydefinitions(char *name, void *element, void *obj) {
    hashtableobject *newobj=(hashtableobject *) obj;
    expression *data = (expression *) element;
    
    hashinsert(newobj->table, name, cloneexpression(data));
}

expression *class_hashtable_deserializei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *newobj=(hashtableobject *) obj;
    if ((nargs==1)&&(eval_isobjectref(args[0]))) {
        hashtableobject *hashobj = (hashtableobject *) ((expression_objectreference *) args[0])->obj;
        hashmap(hashobj->table, class_hashtable_copydefinitions, newobj);
    }
    
    return (expression *) class_newobjectreference(obj);
}

void class_hashtable_clonemapfunction(char *name, expression *exp, hashtable *new) {
    hashinsert(new, name, cloneexpression(exp));
}

expression *class_hashtable_clonei(object *obj, interpretcontext *context, int nargs, expression **args) {
    hashtableobject *hobj=(hashtableobject *) obj;
    hashtable *nh = hashcreate(hobj->table->size);
    expression *ret = NULL;
    
    if (nh) {
        hashmap(hobj->table, (hashmapfunction *) class_hashtable_clonemapfunction, nh);
        ret = (expression *) class_hashtable_objectfromhashtable(nh);
    }
    
    return ret;
}

/*
 * Intrinsic functions
 */

expression *class_classesi(interpretcontext *context, int nargs, expression **args) {
    linkedlist *list = linkedlist_new();
    expression *ret = NULL;
    expression *retsrt = NULL;
    
    if (list) {
        hashmap(classes, eval_name_map, list);
        
        ret=(expression *) newexpressionlist(list);
        linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
        linkedlist_free(list);
        
        if (ret) {
            retsrt=eval_sort(context, 1, &ret);
            freeexpression(ret);
        }

    }
    
    return retsrt;
}

/*
 *  Initialization/Finalization
 */

/* evalinitializeclasses - Initialize classes.
 * Input:
 * Output:
 * Returns:
 */
void evalinitializeclasses(void) {
    classintrinsic *cls;
    classes = hashcreate(EVAL_CLASSTABLESIZE);

    cls=class_classintrinsic(EVAL_OBJECT, NULL, sizeof(object), 10);
    class_registerselector(cls, EVAL_RESPONDSTOSELECTORLABEL, FALSE, class_respondstoselectori);
    class_registerselector(cls, EVAL_EXPORTLABEL, FALSE, class_exporti);
    class_registerselector(cls, EVAL_SUPER, FALSE, class_superi);
    class_registerselector(cls, EVAL_CLASS, FALSE, class_classi);
    class_registerselector(cls, EVAL_SELECTORS, FALSE, class_selectorsi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, class_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, class_deserializei);
    class_registerselector(cls, EVAL_PERFORMSELECTOR, TRUE, class_performselectori);
    class_registerselector(cls, EVAL_CLONE, FALSE, class_clonei);
    class_registerselector(cls, EVAL_ADDLISTENER, FALSE, class_addlisteneri);
    
    /* Define the selection operator */
    operator_infix(OPERATOR_SELECTOR, 100, NULL, (ledtype *) parseinfix, class_selectorop, NULL, OPERATOR_DOT_SELECTOR);
    
    /* Generic intrinsic constructor function for classes */
    intrinsic(EVAL_NEWSELECTORLABEL,TRUE,class_newi,NULL);
    
    /* Generic intrinsic constructor function for objects (used for serialization) */
    intrinsic(EVAL_OBJECTLABEL,TRUE,class_objecti,NULL);
    
    /* Hashtable class */
    cls=class_classintrinsic(EVAL_HASHTABLE, class_lookup(EVAL_OBJECT), sizeof(hashtableobject), 7);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, class_hashtable_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, class_hashtable_freei);
    class_registerselector(cls, EVAL_ADDSELECTORLABEL, FALSE, class_hashtable_addi);
    class_registerselector(cls, EVAL_REMOVESELECTORLABEL, FALSE, class_hashtable_removei);
    class_registerselector(cls, EVAL_LOOKUPSELECTORLABEL, FALSE, class_hashtable_lookupi);
    class_registerselector(cls, EVAL_SERIALIZE, FALSE, class_hashtable_serializei);
    class_registerselector(cls, EVAL_DESERIALIZE, FALSE, class_hashtable_deserializei);
    class_registerselector(cls, EVAL_CLONE, FALSE, class_hashtable_clonei);
    
    class_registerselector(cls, EVAL_SETINDEX, FALSE, class_hashtable_setindexi);
    class_registerselector(cls, EVAL_INDEX, FALSE, class_hashtable_lookupi);

    /* Intrinsic functions */
    intrinsic(EVAL_CLASSES,FALSE,class_classesi,NULL);
}

/* evalfinalizeclasses - Finalize classes
 * Input:
 * Output:
 * Returns:
 */
void evalfinalizeclasses(void) {
    if (classes) {
        hashmap(classes, &class_hashfree,NULL);
        hashfree(classes);
    }
}
