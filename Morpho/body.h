/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* 
 * Body.h
 *
 * Bodies are high-level objects that the user will typically interact with directly. They may contain
 * meshes, fields, energies, etc. Bodies provide management of these elements, and direct operations
 * that involve more than one of them (e.g. relax, etc.)
 *
 * For ways to construct various elementary bodies, see constructors.c
 */

#include "geometry.h"

#ifndef body_h
#define body_h

/* Body type */
typedef struct body_object_s {
    CLASS_GENERICOBJECTDATA
    
    linkedlist *manifolds;
    linkedlist *fields;
    linkedlist *functionalrefs;
} body_object;

typedef enum {
    GEOMETRY_ENERGY,
    GEOMETRY_CONSTRAINT,
    GEOMETRY_FIELDCONSTRAINT, 
    GEOMETRY_LOCALCONSTRAINT,
    GEOMETRY_LOCALFIELDCONSTRAINT,
    GEOMETRY_REGULARIZATION
} functionaltype;

typedef struct {
    functionaltype type; 
    expression_objectreference *ref;
    expression_objectreference *sel; 
} functionalref;

/* Body class label */
#define GEOMETRY_BODYCLASS "body"

/* Body selector labels */
#define GEOMETRY_RELAX_SELECTOR "relax"
#define GEOMETRY_RELAXFIELD_SELECTOR "relaxfield"
#define GEOMETRY_LINESEARCH_SELECTOR "linesearch"
#define GEOMETRY_LINESEARCHFIELD_SELECTOR "linesearchfield"
#define GEOMETRY_DRAW "draw"
#define GEOMETRY_REFINE_SELECTOR "refine"
#define GEOMETRY_VISUALIZEFORCES_SELECTOR "visualizeforces"
#define GEOMETRY_SELECT_SELECTOR "select"
#define GEOMETRY_SELECTALL_SELECTOR "selectall"
#define GEOMETRY_SELECTBOUNDARY_SELECTOR "selectboundary"
#define GEOMETRY_MANIFOLD_SELECTOR "manifold"

#define GEOMETRY_SHOW_FUNCTIONALS "showfunctionals"
#define GEOMETRY_REMOVEFUNCTIONAL_SELECTOR "removefunctional"
#define GEOMETRY_TOTALENERGY_SELECTOR "totalenergy"
#define GEOMETRY_TOTALFORCE_SELECTOR "totalforce"
#define GEOMETRY_TOTALGENERALIZEDFORCE_SELECTOR "totalgeneralizedforce"
#define GEOMETRY_EVALUATEFUNCTIONAL_SELECTOR "evaluatefunctional"
#define GEOMETRY_EVALUATEFORCE_SELECTOR "evaluateforce"

#define GEOMETRY_ADDENERGY_SELECTOR "addenergy"
#define GEOMETRY_ADDCONSTRAINT_SELECTOR "addconstraint"
#define GEOMETRY_ADDLOCALCONSTRAINT_SELECTOR "addlocalconstraint"
#define GEOMETRY_ADDFIELDCONSTRAINT_SELECTOR "addfieldconstraint"
#define GEOMETRY_ADDLOCALFIELDCONSTRAINT_SELECTOR "addlocalfieldconstraint"
#define GEOMETRY_ADDREGULARIZATION_SELECTOR "addregularization"

#define GEOMETRY_NEWFIELD_SELECTOR "newfield"
#define GEOMETRY_ADDFIELD_SELECTOR "addfield"
#define GEOMETRY_CHANGED_SELECTOR "changed"

/* Functional type labels */
#define GEOMETRY_ENERGY_LABEL "energy"
#define GEOMETRY_CONSTRAINT_LABEL "constraint"
#define GEOMETRY_FIELDCONSTRAINT_LABEL "fieldconstraint"
#define GEOMETRY_LOCALCONSTRAINT_LABEL "localconstraint"
#define GEOMETRY_LOCALFIELDCONSTRAINT_LABEL "localfieldconstraint"
#define GEOMETRY_REGULARIZATION_LABEL "regularization"

#define GEOMETRY_SCALE_OPTION "scale"
#define GEOMETRY_SCALELIMIT_OPTION "scalelimit"

/* Errors */
#define GEOMETRY_NEEDSOBJECT                           0x9900
#define GEOMETRY_NEEDSOBJECT_MSG                       "Selector requires an object as the argument."

#define GEOMETRY_LINESEARCHCONST                       0x9901
#define GEOMETRY_LINESEARCHCONST_MSG                   "The functional appears to be constant."

#define GEOMETRY_LINESEARCHEXCEED                      0x9902
#define GEOMETRY_LINESEARCHEXCEED_MSG                  "Linesearch exceeded limit."

#define GEOMETRY_LINESEARCHSMALL                       0x9903
#define GEOMETRY_LINESEARCHSMALL_MSG                   "Linesearch could not find a finite stepsize that decreased the functional."

#define GEOMETRY_REPROJECTMAX                          0x9904
#define GEOMETRY_REPROJECTMAX_MSG                      "Exceeded maximum number of iterations for constraint reprojection."

#define GEOMETRY_CONSTRAINTDIVERGING                   0x9905
#define GEOMETRY_GEOMETRY_CONSTRAINTDIVERGING_MSG      "Constraint reprojection appears to be diverging."

#define GEOMETRY_NOFIELD                               0x9906
#define GEOMETRY_NOFIELD_MSG                           "No field specified."


#define GEOMETRY_MAXREPROJECTIONITERATIONS             10

/*
 * Function prototypes
 */

struct manifold_object_s;

expression *body_new(void);
void body_addmanifold(body_object *obj, struct manifold_object_s *mobj);

/* Selectors */

void bodyinitialize(void);

void body_projectontoconstraints(body_object *obj, interpretcontext *context);

void body_relax(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale);
MFLD_DBL body_evaluateenergy(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel);
void body_linesearch(body_object *obj, interpretcontext *context, expression_objectreference *func, expression_objectreference *sel, MFLD_DBL scale, MFLD_DBL scale_limit);

#endif
