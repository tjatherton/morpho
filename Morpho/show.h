/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#ifndef show_h
#define show_h

#include <stdio.h>

#define SHOW_LABEL "show"

#define SHOW_DRAW "draw"

#define SHOW_CLOSESELECTOR "close"
#define SHOW_UPDATESELECTOR "update"
#define SHOW_VIEWDIRECTION "viewdirection"
#define SHOW_SETVIEWDIRECTION "setviewdirection"
#define SHOW_VIEWVERTICAL "viewvertical"
#define SHOW_SETVIEWVERTICAL "viewvertical"

#ifndef SHOW_FONT
#define SHOW_FONT "/System/Library/Fonts/Helvetica.ttc"
#endif

typedef struct {
    CLASS_GENERICOBJECTDATA
} show_object;

#ifdef USE_GLFW

void glfw_initialize(void);
void glfw_finalize(void);

void show_redraw3d(void *ref, int setcontext, float *matrix, float *project);
expression *glfw_show(interpretcontext *context, int nargs, expression **args);
#endif

void show_cli(char *fin, char *fout, int quiet);

void showinitialize(void);
void showfinalize(void);

#endif
