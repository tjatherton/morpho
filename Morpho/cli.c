/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/* The CLI */

#include "cli.h"

/*
 * Interactive help
 */

expression *help_helpi(interpretcontext *context, int nargs, expression **args) {
    
    
    return newexpressionnone();
}


/*
 * Command line interface
 */

char *cli_continuation(void *data) {
    cli_continuationinfo *info = (cli_continuationinfo *) data;
    char *next=NULL;
    char *sinput=NULL;
#ifdef USE_LINENOISE
    char *line=NULL;
#endif
    
    if (info) switch (info->mode) {
        case CLI_FROMLIST:
            if (info->next!=NULL) {
                next=info->next->data; /* The next command string */
                printf("~%s",next);
                /* Update the info block to point to the next command for further continuation. */
                info->next=info->next->next;
            }
            break;
        case CLI_FROMCOMMANDLINE:
            
#ifdef USE_LINENOISE
            line = linenoise("?");
            if (line[0] != '\0' && line[0] != '/') {
                linenoiseHistoryAdd(line); /* Add to the history. */
                sinput=EVAL_STRDUP(line);
                if (sinput) linkedlist_addentry(info->commands, sinput);
            }
            free(line);
            next=sinput;
#else
            printf("?");
            if (fgets(info->buffer, (int) info->bufferlength, stdin)) {
                /* Add the command to the linked list */
                sinput=EVAL_STRDUP(info->buffer);
                if (sinput) linkedlist_addentry(info->commands, sinput);
                
                next=info->buffer;
            }
#endif
            break;
    }
    return next;
}

#ifdef USE_LINENOISE
typedef struct {
    size_t nchar;
    const char *buf;
    linenoiseCompletions *lc;
} linennoisecompletionrecord;

void cli_name_map(char *name, void *el, void *ref) {
    linennoisecompletionrecord *rec = (linennoisecompletionrecord *) ref;
    if (!strncmp(rec->buf, name, rec->nchar)) {
        linenoiseAddCompletion(rec->lc, name);
    }
}

void cli_completion(const char *buf, linenoiseCompletions *lc) {
    linennoisecompletionrecord rec;
    rec.nchar=strlen(buf);
    rec.buf=buf;
    rec.lc=lc;
    
    hashmap(intrinsics, cli_name_map, &rec);
    hashmap(constants, cli_name_map, &rec);
    hashmap(classes, cli_name_map, &rec);
    hashmap(defaultinterpretcontext->symbols, cli_name_map, &rec);
}
#endif

/* Runs a script */
expression *cli_run(interpretcontext *context, int nargs, expression **args) {
    if ((nargs<1)&&(args[0]->type!=EXPRESSION_STRING)) return NULL;
// TODO: Implement quiet=true
//    interpretcontext *options = eval_options(context, 2, nargs, args);
    
    cli(((expression_string *)args[0])->value, NULL, FALSE);
    
    return newexpressionnone();
}

/* Initializes the cli */
void cliinitialize(void) {
    /* Registers an intrinsic function to run scripts */
    intrinsic("run",FALSE,cli_run,NULL);
    intrinsic("help",TRUE,help_helpi,NULL);
    
#ifdef USE_LINENOISE
    /* Linenoise completion callback, called whenever <tab> is pressed. */
    linenoiseSetCompletionCallback(cli_completion);
#endif
    
}

void clifinalize(void) {
    
}

/* cli - Runs the cli loop
 * Input:   (char *) fin - a file to use as input.
 *          (char *) fout - a file to use as output.
 * Output:
 * Returns:
 */
void cli(char *fin, char *fout, int quiet) {
    FILE *inputfile=NULL;
    cli_continuationinfo cinfo;
    char inputbuffer[INPUT_BUFFER_SIZE];
    expression *exp=NULL;
    expression *returnvalue=NULL;
    linkedlist *commands=linkedlist_new();
    linkedlistentry *currentcommand=NULL;
    int command_cli=FALSE;
#ifdef USE_LINENOISE
    char *line=NULL;
#endif
    
    if (!commands) return;
    
    if ((fin)&&(strcmp(fin, ""))) {
        inputfile=fopen(fin,"r");
        
        /* Load in the whole command file at once. */
        if (inputfile) {
            do {
                if (fgets(inputbuffer, INPUT_BUFFER_SIZE, inputfile)) {
                    char *s=EVAL_STRDUP(inputbuffer);
                    if(s) linkedlist_addentry(commands, s);
                }
            } while (!feof(inputfile));
            fclose(inputfile);
            
            currentcommand=commands->first;
            cinfo.mode=CLI_FROMLIST;
        } else {
            linkedlist_free(commands);
            printf("Could not open '%s'\n",fin);
            return;
        }
    } else {
        cinfo.mode=CLI_FROMCOMMANDLINE;
    }
    
    /* Make commands available to continuation function */
    cinfo.commands=commands;
    
    do {
        char *sinput=NULL;
        
        if (!inputfile) {
#ifdef USE_LINENOISE
            line = linenoise(">");
            if (line[0] != '\0' && line[0] != '/') {
                linenoiseHistoryAdd(line); /* Add to the history. */
                sinput=EVAL_STRDUP(line);
                if (sinput) linkedlist_addentry(commands, sinput);
            }
            free(line);
#else
            printf(">");
            if (fgets(inputbuffer, INPUT_BUFFER_SIZE, stdin)) {
                sinput=EVAL_STRDUP(inputbuffer);
                if (sinput) linkedlist_addentry(commands, sinput);
            } /* SHOULD RAISE AN ERROR HERE! */
            
#endif
            
        } else {
            if (currentcommand) sinput=(char *) currentcommand->data;
            if (!quiet) printf("~%s",sinput);
        }
        
        /* Allow the cli to process commands that it understands. x*/
        command_cli=FALSE;
        if (sinput) if (!strncmp(sinput,"quit",4)) break;
        if (sinput) if (!strncmp(sinput,"save",4)) {
            int k;
            for (k=5; (sinput[k]==' ')&&(sinput[k]!='\0'); k++);
            /* Filename needs to be zero-terminated */
            for (int l=k; (sinput[l]!='\0'); l++) if (sinput[l]=='\n') sinput[l]='\0';
            FILE *save=fopen(sinput+k,"w");
            
            if (save) {
                for (linkedlistentry *c=commands->first; c!=NULL; c=c->next) {
                    /* avoid saving save commands */
                    if (strncmp((char *) c->data, "save",4)) fprintf(save,"%s",(char *) c->data);
                }
                fclose(save);
                if (!quiet) printf("Saved '%s'\n",sinput+k);
            }
            command_cli=TRUE;
        }
        
        if (sinput&&!command_cli) {
            /* Prepare information for possible call to continuation function by the parser. */
            switch (cinfo.mode) {
                case CLI_FROMCOMMANDLINE:
                    cinfo.buffer=inputbuffer;
                    cinfo.bufferlength=INPUT_BUFFER_SIZE;
                    break;
                case CLI_FROMLIST:
                    if (currentcommand) cinfo.next=currentcommand->next;
                    break;
            }
            
            exp=parse(sinput,cli_continuation,&cinfo);
            if (exp!=NULL) {
                /*printf("[Parsed as] ");
                expressionprint(exp);
                printf("\n");*/
                
                returnvalue=interpret(exp);
                
                if (eval_isobjectref(returnvalue)) {
                    object *obj = ((expression_objectreference *) returnvalue)->obj;
                    
                    if (class_objectrespondstoselector(obj, EVAL_PRINTSELECTORLABEL)) {
                        freeexpression(class_callselector(defaultinterpretcontext, obj, EVAL_PRINTSELECTORLABEL, 0));
                    } else {
                        expressionprint(returnvalue);
                        printf("\n");
                    }
                    
                } else if (returnvalue) {
                    if (!quiet) expressionprint(returnvalue);
                    if (!quiet) printf("\n");
                }
                
                if (returnvalue!=NULL && returnvalue!=exp) freeexpression(returnvalue);
                if (exp!=NULL) freeexpression(exp);
            }
        }
        
        if ((sinput)&&(!strncmp(sinput,"restart",7))) {
            if (!inputfile) {
                linkedlist_map(commands, EVAL_FREE);
                linkedlist_free(commands);
                commands=linkedlist_new();
            }
        }
        
        if (inputfile) {
            /* Get the next command from the input file if we're using it. */
            currentcommand=cinfo.next;
            if (!currentcommand) break;
        }
    } while (1);
    
    /* Free the commands list */
    linkedlist_map(commands, EVAL_FREE);
    linkedlist_free(commands);
}
