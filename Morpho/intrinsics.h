/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

/*
 * intrinsics.h - Implements intrinsic functions.
 */

#ifndef Morpho_intrinsics_h
#define Morpho_intrinsics_h

#include <stdio.h>
#include <time.h>
#include "eval.h"
#include "mt19937ar.h"

#define ERROR_JOINLISTREQUIRED     0x1601
#define ERROR_JOINLISTREQUIRED_MSG "Join requires each argument to be a list"

/* The size of the global hashtable used to contain intrinsic functions. Increase if many more intrinsics are defined */
#define EVAL_INTRINSICTABLESIZE 32

#define HISTOGRAM_DEFAULT_NBINS 5

/* A type of function that implements an intrinsic function.
 * Such a function receives an interpreter context, a number of args and a list of expressions as args. It must return an expression.
 */
typedef expression* intrinsicfunctiontype (interpretcontext*,int,expression**);

/* Intrinsic function record
 */
typedef struct {
    int hold;
    intrinsicfunctiontype *func;
    diffexpfunctype *diff;
} intrinsicfunction;

/* Counters are lists of the form {u,0,1} that allow ranges etc. to be specified. These types help to
 * manipulate them.
 */
typedef enum { COUNTER_INT, COUNTER_FLOAT } countertype;

typedef struct {
    countertype type;
    expression_name *name;
    unsigned int nsteps;
} genericcountertype;

typedef struct {
    countertype type;
    expression_name *name;
    unsigned int nsteps;
    int value;
    int start;
    int end;
    int increment;
} intcountertype;

typedef struct {
    countertype type;
    expression_name *name;
    unsigned int nsteps;
    double value;
    double start;
    double end;
    double increment;
} floatcountertype;

extern hashtable *intrinsics;

expression *eval_sort(interpretcontext *context, int nargs, expression **args);

void intrinsic(char *label, int hold, intrinsicfunctiontype *func, diffexpfunctype *diff);

/* Functions that are useful for writing intrinsic functions and selectors */
expression *eval_tostring(interpretcontext *context, int nargs, expression **args);
expression *eval_stringjoin(interpretcontext *context, int nargs, expression **args);
expression *eval_stringsplit(interpretcontext *context, int nargs, expression **args); 
expression *eval_argstolist(int nargs, expression **args);
int eval_optionstart(interpretcontext *context, int narg, expression **args);
interpretcontext *eval_options(interpretcontext *context, int start, int narg, expression **args);
expression *eval_getelement(expression_list *list, unsigned int element);
expression *eval_setelement(expression_list *list, unsigned int element, expression *exp);

double eval_floatvalue(expression *exp);
int eval_integervalue(expression *exp);
int eval_boolvalue(expression *exp);
char *eval_stringvalue(expression *exp);
expression *eval_listfromfloatarray(float *flt, unsigned int nentries);
expression *eval_listfromunsignedintegerarray(unsigned int *data, unsigned int nentries);
expression *eval_listfromdoublearray(double *flt, unsigned int nentries);
expression *eval_listfromexpressionarray(expression **data, unsigned int nentries);

int eval_listtofloatarray(expression_list *list, float *flt, unsigned int maxentries);
int eval_listtodoublearray(expression_list *list, double *flt, unsigned int maxentries);
int eval_listtoexpressionarray(expression_list *list, expression **exp, unsigned int maxentries);

int eval_listtomatrixsize(expression_list *list, unsigned int *nrows, unsigned int *ncols);
int eval_listtodoublematrix(expression_list *list, double *flt, unsigned int nrows, unsigned int ncols, int colmajor);

genericcountertype *eval_listtocounter(interpretcontext *context, expression_list *lst, int forcefloat);
void eval_counterreset(genericcountertype *counter);
int eval_counteradvance(genericcountertype *counter);
void eval_counterstore(interpretcontext *context, genericcountertype *counter);

unsigned int eval_listlength(expression_list *list);

void symbolprint(char *name, expression *exp, void *arg);
expression *eval_symbols(interpretcontext *context, int nargs, expression **args);
void eval_symbolsinexpression(expression *exp, linkedlist *list);

void eval_name_map(char *name, void *el, void *ref);

int eval_histogram_nbins(interpretcontext *context, int nargs, expression **args, int *nbins, int *userbins);
void eval_histogram_bincounts(expression_list *data, expression_list *userbinlist, int userbins, int nbins, double *binbounds, unsigned int *bincounts);
expression *eval_histogram(interpretcontext *context, int nargs, expression **args);

void evalinitializeintrinsics(void);
void evalfinalizeintrinsics(void);

#endif
