/* *************************************
 * Morpho                              *
 * by the Tufts softmattertheory group *
 ************************************* */

#include "selection.h"

/* ----------------------------------------------------------------------------------------------------
 * Select part of a body
 * ----------------------------------------------------------------------------------------------------  */

/* Creates a new, empty selection */
expression *selection_new() {
    selection_object *obj=NULL;
    expression_objectreference *ref=NULL;
    
    obj=(selection_object *) class_instantiate(class_lookup(GEOMETRY_SELECTIONCLASS));
    
    if (obj) {
        ref=class_newobjectreference((object *) obj);
    }
    
    return (expression *) ref;
}

void selection_addmanifoldref(selection_object *sobj, selection_manifoldref *mref) {
    if (sobj) {
        if (!sobj->selection) sobj->selection=linkedlist_new();
        if (sobj->selection) linkedlist_addentry(sobj->selection, mref);
        
        /* Add a listener to the manifold so we hear about when it's been updated. */
        {
            expression_name *refine = (expression_name *) newexpressionname(MANIFOLD_REFINE);
            expression_objectreference *sref = class_newobjectreference((object *) sobj);
            
            class_addlistener((object *) mref->mref, refine, sref, refine);
            
            if (refine) freeexpression((expression *)refine);
            if (sref) freeexpression((expression *)sref);
        }
    }
}

/* Creates a new manifold reference */
selection_manifoldref *selection_newmanifoldref(manifold_object *manifold) {
    selection_manifoldref *ref=NULL;
    
    if (manifold) {
        unsigned int maxgrade = manifold->grade;
        
        ref=EVAL_MALLOC(sizeof(selection_manifoldref));
        
        if (ref) {
            ref->mref=manifold;
            ref->maxgrade=maxgrade;
            /* Reserve space for selection idtables */
            ref->selection=EVAL_MALLOC(sizeof(idtable *)*(maxgrade+1));
            
            if (ref->selection) for (unsigned int i=0; i<=maxgrade; i++) ref->selection[i]=NULL;
        }
    }
    
    return ref;
}

/* Process which grades to include in a selection from a list */
int selection_processgradeselection(interpretcontext *context, expression *grade, int *includegrade, unsigned int dim) {
    expression_list *grd = (expression_list *) grade;
    unsigned int i;
    
    if (grade) {
        /* If a grade specification, assume FALSE until otherwise  */
        for (i=0; i<=dim; i++) includegrade[i]=FALSE;
        
        if (eval_islist(grade)) {
            if (grd->list) for (linkedlistentry *e = grd->list->first; e!=NULL; e=e->next) {
                expression *exp = (expression *) e->data;
                
                if (eval_isinteger(exp)) {
                    i=eval_integervalue(exp);
                    if (i<=dim) includegrade[i]=TRUE;
                }
            } else return FALSE;
        } else if (eval_isinteger(grade)) {
            i=eval_integervalue(grade);
            if (i<=dim) includegrade[i]=TRUE;

        } else return FALSE;
    } else {
        /* Select everything if no grade specification is given */
        for (unsigned int i=0; i<=dim; i++) includegrade[i]=TRUE;
    }
    
    return TRUE;
}

typedef struct {
    idtable *prev;
    idtable *target;
    int includepartials;
} selection_addidtablefromprevgraderef;

void selection_addidtablefromprevgrade(uid id, void *content, void *ref) {
    selection_addidtablefromprevgraderef *r = (selection_addidtablefromprevgraderef *) ref;
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    int isin[2] = {FALSE, FALSE};
    
    /* Validate */
    if ((!r) || (!r->prev) || (!r->target)) return;
    
    /* Were the two lowering elements included in the previous idtable? */
    for (unsigned int i=0; i<2; i++) isin[i] = idtable_ispresent(r->prev, lower->el[i]);
    
    /* If either or both, depending on whether includepartials is selected, also select this element by including it into target. */
    if (r->includepartials) {
        if (isin[0] || isin[1]) idtable_insertwithid(r->target, id, NULL);
    } else {
        if (isin[0] && isin[1]) idtable_insertwithid(r->target, id, NULL);
    }
}

void selection_addcopyidtablemapfunction(uid id, void *content, void *ref) {
    idtable *target = (idtable *) ref;
    
    if (target) idtable_insertwithid(target, id, NULL);
}

void selection_addtoidtablemapfunction(uid id, void *content, void *ref) {
    idtable *target = (idtable *) ref;
    expression *exp = (expression *) content;
    
    if (target) {
        if (eval_isbool(exp) && eval_boolvalue(exp)) idtable_insertwithid(target, id, NULL);
    }
}


void selection_addselectionwithfieldmapfunction (uid id, void *content, void *ref) {
    idtable *table = (idtable *) ref;
    expression *exp = (expression *) content;
    
    if (eval_isbool(exp) && eval_boolvalue(exp)) {
        idtable_insertwithid(table, id, NULL);
    }
}

void selection_addselectionwithfield(interpretcontext *context, selection_manifoldref *msel, field_object *field) {
    /* Exit if there's no manifold ref or if it doesn't point to a manifold */
    if ((!msel)||(! msel->mref)) return;
    
    if (field->grade<=msel->maxgrade) {
        /* Check if we have a selection idtable to contain the new entries */
        if (!msel->selection[field->grade]) {
            unsigned int nentries = 1;
            if (field->entries) nentries = field->entries->nentries;
            msel->selection[field->grade] = idtable_create(nentries);
        }
        
        if (msel->selection[field->grade])  field_map(field, selection_addselectionwithfieldmapfunction, msel->selection[field->grade]);
    }
}

/* Adds a vertex selection to a manifold reference. Additional grades computed from the selection may also be included. */
void selection_addvertexselection(interpretcontext *context, selection_manifoldref *mref, expression *exp, expression *coords, expression *grade, int includepartials) {
    expression_objectreference *fref=NULL;
    field_object *fobj=NULL;
    selection_addidtablefromprevgraderef idpref;
    idpref.includepartials=includepartials;
    
    /* Exit if there's no manifold ref or if it doesn't point to a manifold */
    if ((!mref)||(! mref->mref)) return;
    
    unsigned int dim = mref->maxgrade;
    unsigned int maxgrade = mref->maxgrade;
    int includegrade[dim+1];
    idtable *vtable[dim+1];
    
    if (selection_processgradeselection(context, grade, includegrade, dim)) {
        for (unsigned int i=0; i<=dim; i++) vtable[i]=NULL;
        for (maxgrade=mref->maxgrade; maxgrade>0 && includegrade[maxgrade]==FALSE; maxgrade--);
    
        /* Create a field over the vertices to determine what should be included */
        fref = (expression_objectreference *) field_newwithexpression(mref->mref, context, exp, coords);
        if (fref) fobj = (field_object *) fref->obj;
        
        if ((fobj)&&(fobj->entries)) {
            
            /* Now build up from there */
            for (unsigned int i=0; i<=maxgrade; i++) {
                vtable[i]=idtable_create(fobj->entries->size);
                if (vtable[i]) {
                    if (i>0) {
                        idpref.prev=vtable[i-1];
                        idpref.target=vtable[i];
                        idtable_map(mref->mref->gradelower[i-1], selection_addidtablefromprevgrade, (void *) &idpref);
                    } else field_map(fobj, selection_addtoidtablemapfunction, vtable[i]);
                }
            }
            
            /* Copy these idtables into the selection */
            for (unsigned int i=0; i<=maxgrade; i++) {
                if (includegrade[i] && mref->selection) {
                    if (mref->selection[i]) {
                        /* If an idtable has already been created for this selection grade, copy it over */
                        idtable_map(mref->selection[i], selection_addcopyidtablemapfunction, (void *) vtable[i]);
                    } else {
                        mref->selection[i]=vtable[i]; vtable[i]=NULL; /* Just patch in the idtable and don't free it */
                    }
                }
            }
            
            /* Free the idtables */
            for (unsigned int i=0; i<=maxgrade; i++) if (vtable[i]) idtable_free(vtable[i]);
        }
    }
    
    if (fref) freeexpression((expression *) fref);
}

/* Add elements to a specified grade */
void selection_addelements(selection_manifoldref *mref, unsigned int grade, unsigned int nelements, uid *elements) {
    
    /* Exit if there's no manifold ref or if it doesn't point to a manifold */
    if ((!mref)||(! mref->mref)) return;
    
    if (grade>mref->maxgrade) return;
    
    /* If there's no idtable for this selection, create one */
    if (! mref->selection[grade]) {
        mref->selection[grade] = idtable_create(nelements);
    }
    
    /* Now add the selection */
    if (mref->selection[grade]) {
        for (unsigned int i=0; i<nelements; i++) idtable_insertwithid(mref->selection[grade], elements[i], NULL);
    }
}

int selection_isselected(selection_object *selection, manifold_object *mobj, unsigned int grade, uid id) {
    selection_manifoldref *mref=NULL;
    
    if ((selection)&&(selection->selection)) {
        for (linkedlistentry *e=selection->selection->first; e!=NULL; e=e->next) {
            mref = e->data;
            
            if ((mref)&&(grade<=mref->maxgrade)) {
                if (mref->selection[grade] && idtable_ispresent(mref->selection[grade], id)) return TRUE;
            }
        }
    }
    return FALSE;
}

idtable *selection_idtableforgrade(selection_object *selection, manifold_object *mobj, unsigned int grade) {
    selection_manifoldref *mref=NULL;
    idtable *ret=NULL;
    
    if ((selection)&&(selection->selection)) {
        for (linkedlistentry *e=selection->selection->first; e!=NULL; e=e->next) {
            mref = e->data;
            
            if ((mref)&&(grade<=mref->maxgrade)) ret=mref->selection[grade];
        }
    }
    return ret;
}

/* Frees a manifold ref */
void selection_freemanifoldref(selection_manifoldref *ref) {
    if (ref) for (unsigned int i=0; i<=ref->maxgrade; i++) {
        if (ref->selection[i]) {
            idtable_free(ref->selection[i]);
        }
    }
    EVAL_FREE(ref->selection);
    EVAL_FREE(ref);
}

/* Frees things attached to a selection object */
void selection_free(selection_object *selection) {
    if ((selection)&&(selection->selection)) {
        for (linkedlistentry *e=selection->selection->first; e!=NULL; e=e->next) {
            selection_manifoldref *mref=(selection_manifoldref *) e->data;
            if (mref) selection_freemanifoldref(mref);
        }
        linkedlist_free(selection->selection);
    }
}

/*
 * Rebuild a selection after refinement
 */

typedef struct {
    field_object *map;
    idtable *target;
    selection_manifoldref *mref;
    unsigned int grade;
    int select_interior;
} selection_refinemapfunctionref;

void selection_refinemapfunction (uid id, void *content, void *r) {
    selection_refinemapfunctionref *ref = (selection_refinemapfunctionref *) r;
    expression_list *list = (expression_list *) field_get(ref->map, id);
    
    if (eval_islist(list) && list->list) for (linkedlistentry *e = list->list->first; e!=NULL; e=e->next) {
        expression *entry = e->data;
        uid newid=(uid) eval_integervalue(entry);
        
        if (eval_isinteger(entry)) idtable_insertwithid(ref->target, newid, NULL);
        
        /* Also add interior objects to the selection using gradefind if requested */
        if (ref->select_interior) {
            uid ids[MANIFOLD_GRADEFIND_MAXENTRIES];
            unsigned int nelements;
            
            for (unsigned int j=0; j<ref->grade; j++) {
                /* Are we selecting elements of this grade in any case? */
                if (ref->mref->selection[j]) {
                    /* Find items */
                    if (manifold_gradefind(ref->mref->mref, ref->grade, newid, j, ids, &nelements, FALSE)) {
                        for (unsigned int k=0; k<nelements; k++) {
                            idtable_insertwithid(ref->mref->selection[j], ids[k], NULL);
                        }
                    }
                }
            }
        }
    }
}

void selection_refine(selection_object *selection, interpretcontext *context, manifold_object *mobj, expression_list *refine) {
    selection_refinemapfunctionref ref;
    
    if ((selection)&&(selection->selection)) {
        /* Loop over manifolds in selection */
        for (linkedlistentry *e=selection->selection->first; e!=NULL; e=e->next) {
            selection_manifoldref *mref=(selection_manifoldref *) e->data;
            ref.mref=mref;
            
            if (mref && mref->mref==mobj) {
                /* Keep track of whether to select interior points of each grade */
                int selinterior[mref->maxgrade+1];
                for (unsigned int i=0; i<=mref->maxgrade; i++) selinterior[i]=FALSE;
                    
                /* Loop over grades */
                for (unsigned int i=0; i<=mref->maxgrade; i++) {
                    /* If something of this grade is selected.. */
                    if (mref->selection[i]) {
                        /* Extract the field containing refinement information for this grade */
                        linkedlistentry *entry=linkedlist_element(refine->list, i);
                        expression_objectreference *fobj=(expression_objectreference *) entry->data;
                        if (eval_isobjectref(fobj)) ref.map = (field_object *) eval_objectfromref(fobj);
                        
                        /* Create a new idtable to hold the corrected entries */
                        ref.target=idtable_create(mref->selection[i]->nentries);
                        ref.grade=i;
                        ref.select_interior=selinterior[i];
                        
                        if (ref.target && ref.map) {
                            /* Call the refine map function */
                            idtable_map(mref->selection[i], selection_refinemapfunction, &ref);
                        
                            idtable_free(mref->selection[i]);
                            mref->selection[i]=ref.target;
                        }
                        
                        /* If something of a higher grade is also selected, incorporate interior elements as well later */
                        if (i<mref->maxgrade && mref->selection[i+1]) {
                            selinterior[i+1]=TRUE;
                        }
                    }
                }
            }
        }
    }
}

typedef struct {
    selection_object *srcobj;
    manifold_object *mobj;
    selection_manifoldref *src;
    selection_manifoldref *target;
    unsigned int grade;
    int *includegrades;
    unsigned int includelength;
    int includeattached;
} selection_changegrademapfunctionref;

void selection_changegrademapfunction (uid id, void *content, void *r) {
    selection_changegrademapfunctionref *ref = (selection_changegrademapfunctionref *) r;
    unsigned int dim = ref->src->mref->dimension;
    uid out[3*(dim+1)];
    unsigned int nel=0;
    
    for (unsigned int i=0; i<dim && i<ref->includelength; i++) {
        if (i<ref->grade && ref->includegrades[i]) {
            manifold_gradefind(ref->src->mref, ref->grade, id, i, out, &nel, FALSE);
            if (nel>0) selection_addelements(ref->target, i, nel, out);
        }
    }
}

void selection_changegradeupmapfunction (uid id, void *content, void *r) {
    selection_changegrademapfunctionref *ref = (selection_changegrademapfunctionref *) r;
    unsigned int dim = ref->src->mref->dimension; /* Dimension of the original manifold */
    uid out[3*(dim+1)]; /* Elements */
    unsigned int nel=0; /* Number obtained */
    
    /* Loop over grades below this one */
    for (unsigned int i=0; i<ref->grade; i++) {
        unsigned int sel=0;
        
        /* Find the elements that compose this one */
        manifold_gradefind(ref->src->mref, ref->grade, id, i, out, &nel, FALSE);
        
        /* Which are selected? */
        for (unsigned int j=0; j<nel; j++) {
            if (selection_isselected(ref->srcobj, ref->mobj, i, out[j])) sel++;
        }
        
        /* Are they all selected? */
        if (sel==nel || (ref->includeattached && sel>0)) {
            selection_addelements(ref->target, ref->grade, 1, &id);
        }
    }
}

/* Creates a new selection with elements of different grade by calling grade_lower */
expression *selection_changegrade(selection_object *sel, interpretcontext *context, int *includegrades, unsigned int includelength, int includeattached) {
    expression_objectreference *new = (expression_objectreference *) selection_new();
    selection_changegrademapfunctionref ref;
    unsigned int i=0;
    unsigned int sgrade=0; /* Highest grade that we have for this manifold */
    
    if (eval_isobjectref(new)) {
        selection_object *snew = (selection_object *) ((expression_objectreference *) new)->obj;
        
        /* Loop over manifold references in old selection */
        if (sel->selection) for (linkedlistentry *e=sel->selection->first; e!=NULL; e=e->next) {
            selection_manifoldref *mref = e->data;
            selection_manifoldref *newmref = selection_newmanifoldref(mref->mref); /* A new manifold reference */
            
            /* Fill out reference for call to map function */
            ref.src=mref;
            ref.target=newmref;
            ref.includegrades=includegrades;
            ref.includelength=includelength;
            ref.srcobj=sel;
            ref.mobj=mref->mref;
            ref.includeattached=includeattached;
            
            if (newmref) {
                selection_addmanifoldref(snew, newmref);
                
                /* What's the highest grade present in the original selection? */
                sgrade=0;
                for (i=0; i<=mref->maxgrade; i++) if (mref->selection[i]) sgrade=i;
                
                /* For elements of this grade or below, we project *down* and identify elements
                   that belong to the selected elements */
                for (i=0; i<=sgrade; i++) {
                    ref.grade=i;
                    if (mref->selection) idtable_map(mref->selection[i], selection_changegrademapfunction, &ref);
                }
                
                /* Now check if we were asked to provide higher grade elements */
                for (i=sgrade+1; i<includelength; i++) {
                    ref.grade=i;
                    if (includegrades[i] && i<mref->mref->grade) {
                        /* Map over the higher grade elements */
                        if (mref->mref->gradelower[i-1]) {
                            idtable_map(mref->mref->gradelower[i-1], selection_changegradeupmapfunction, &ref);
                        }
                    }
                }
            }
        }
    }
    
    return (expression *) new;
}

/* Constructor functions  */

expression *selection_selectwithfield(body_object *bobj, interpretcontext *context, field_object *field) {
    expression *select=NULL;
    selection_manifoldref *msel = NULL;
    
    select=selection_new();
    
    if (eval_isobjectref(select)) {
        selection_object *sobj = (selection_object *) ((expression_objectreference *) select)->obj;
        /* Loop over manifolds */
        if (sobj) if (bobj->manifolds) for (linkedlistentry *me = bobj->manifolds->first; me!=NULL; me=me->next) {
            expression_objectreference *mref = (expression_objectreference *) me->data;
            
            if (eval_objectrefeq(mref, field->manifold)) {
                msel = selection_newmanifoldref((manifold_object *) mref->obj);
                
                selection_addselectionwithfield(context, msel, field);
                
                selection_addmanifoldref(sobj, msel);
            }
        }
    }
    
    return select;
}

expression *selection_select(body_object *bobj, interpretcontext *context, expression *exp, interpretcontext *options) {
    expression *select=NULL;
    expression *coordinates=NULL;
    expression *e=NULL;
    selection_manifoldref *msel = NULL;
    int includepartials=FALSE;
    expression *grade=NULL;
    
    /* Validate input and options*/
    if (!options) return NULL;
    
    coordinates = lookupsymbol(options, GEOMETRY_SELECTION_COORDS);
    if (coordinates) {
        if (!eval_islist(coordinates)) error_raise(context, ERROR_SELECTIONCOORDS, ERROR_SELECTIONCOORDS_MSG, ERROR_FATAL);
    } else {
        error_raise(context, ERROR_COORDSREQD, ERROR_COORDSREQD_MSG, ERROR_FATAL);
    }
    
    grade = lookupsymbol(options, GEOMETRY_SELECTION_GRADE);
    
    e = lookupsymbol(options, GEOMETRY_SELECTION_INCLUDEPARTIALELEMENTS);
    if (eval_isbool(e)) includepartials = eval_boolvalue(e);
    if (e) freeexpression(e);
        
    select=selection_new();
    
    if (eval_isobjectref(select)) {
        selection_object *sobj = (selection_object *) ((expression_objectreference *) select)->obj;
        /* Loop over manifolds */
        if (sobj) if (bobj->manifolds) for (linkedlistentry *me = bobj->manifolds->first; me!=NULL; me=me->next) {
            expression_objectreference *mref = (expression_objectreference *) me->data;
            
            if (mref) {
                msel = selection_newmanifoldref((manifold_object *) mref->obj);
                
                selection_addvertexselection(context, msel, exp, coordinates, grade, includepartials);
                
                selection_addmanifoldref(sobj, msel);
            }
        }
    }

    if (coordinates) freeexpression(coordinates);
    if (grade) freeexpression(grade);
    
    return select;
}

expression *selection_selectwithidlist(manifold_object *mobj, unsigned int grade, unsigned int nels, uid *ids) {
    expression *select=NULL;
    selection_manifoldref *msel = NULL;
    
    select=selection_new(); 
    
    if (eval_isobjectref(select)) {
        selection_object *sobj = (selection_object *) ((expression_objectreference *) select)->obj;
        
        msel = selection_newmanifoldref(mobj);
        
        if (msel) {
            selection_addmanifoldref(sobj, msel);
            selection_addelements(msel, grade, nels, ids);
        }
    }
    
    return select;
}

typedef struct {
    manifold_object *mobj;
    selection_manifoldref *msel;
    unsigned int grade;
} selection_selectboundarymapfunctionref;

void selection_addboundaryfromprevgrade(uid id, void *content, void *r) {
    manifold_gradelowerentry *lower = (manifold_gradelowerentry *) content;
    idtable *target = (idtable *) r;
    
    idtable_insertwithid(target, lower->el[0], NULL);
    idtable_insertwithid(target, lower->el[1], NULL);
}

void selection_selectboundarymapfunction (uid id, void *content, void *ref) {
    selection_selectboundarymapfunctionref *r = (selection_selectboundarymapfunctionref *) ref;
    
    if (manifold_isboundary(r->mobj, r->grade, id)) {
        selection_addelements(r->msel, r->grade, 1, &id);
    }
}

expression *selection_selectboundary(manifold_object *mobj) {
    expression *select=NULL;
    selection_manifoldref *msel = NULL;
    selection_selectboundarymapfunctionref ref;
    
    ref.mobj=mobj;
    
    select=selection_new();
    
    if (eval_isobjectref(select) && mobj->grade>0) {
        selection_object *sobj = (selection_object *) ((expression_objectreference *) select)->obj;
        
        msel = selection_newmanifoldref(mobj);
        
        if (msel) {
            selection_addmanifoldref(sobj, msel);
            
            idtable *sweep=mobj->vertices;
            if (mobj->grade>1 && mobj->gradelower[mobj->grade-2]) sweep=mobj->gradelower[mobj->grade-2];
            
            if (sweep) {
                ref.grade=mobj->grade-1;
                ref.msel=msel;
                idtable_map(sweep, selection_selectboundarymapfunction, (void *) &ref);
                
                /* Now add in lower grades */
                for (unsigned int i=mobj->grade-1; i>0; i--) {
                    if (!msel->selection[i-1]) msel->selection[i-1]=idtable_create(msel->selection[i]->nentries);
                    if (msel->selection[i-1]) idtable_mapintersection(mobj->gradelower[i-1], msel->selection[i], selection_addboundaryfromprevgrade, msel->selection[i-1]);
                }
            }
        }
    }
    
    return select;
}

/* Select All */

typedef struct {
    selection_manifoldref *msel;
    unsigned int grade;
} selection_selectallmapfunctionref;

void selection_selectallmapfunction (uid id, void *content, void *ref) {
    selection_selectallmapfunctionref *r = (selection_selectallmapfunctionref *) ref;
    
    selection_addelements(r->msel, r->grade, 1, &id);
}

/* Selects the entire manifold. Can optionally select everything of a particular grade */
expression *selection_selectall(body_object *bobj, unsigned int ngrades, unsigned int *grades) {
    expression *select=NULL;
    
    select=selection_new();
    selection_manifoldref *msel = NULL;
    
    selection_selectallmapfunctionref ref;
    
    if (eval_isobjectref(select)) {
        selection_object *sobj = (selection_object *) ((expression_objectreference *) select)->obj;
        /* Loop over manifolds */
        if (sobj) if (bobj->manifolds) for (linkedlistentry *me = bobj->manifolds->first; me!=NULL; me=me->next) {
            expression_objectreference *mref = (expression_objectreference *) me->data;
            
            if (mref) {
                manifold_object *mobj = (manifold_object *) eval_objectfromref(mref);
                msel = selection_newmanifoldref(mobj);
                ref.msel=msel;
                
                if (ngrades>0 && grades) {
                    for (unsigned int i=0; i<ngrades; i++) {
                        ref.grade=grades[i];
                        if (ref.grade<mobj->grade) {
                            if (ref.grade>0) {
                                idtable_map(mobj->gradelower[ref.grade-1], &selection_selectallmapfunction, &ref);
                            } else {
                                idtable_map(mobj->vertices, &selection_selectallmapfunction, &ref);
                            }
                        }
                    }
                } else {
                    ref.grade=MANIFOLD_GRADE_POINT;
                    idtable_map(mobj->vertices, &selection_selectallmapfunction, &ref);
                    
                    for (unsigned int i=1; i<=mobj->grade; i++) {
                        ref.grade=i;
                        idtable_map(mobj->gradelower[ref.grade-1], &selection_selectallmapfunction, &ref);
                    }
                }
                
                selection_addmanifoldref(sobj, msel);
            }
        }
    }

    
    return select;
}

/*
 * Selectors
 */

void selection_idlistforgrademapfunction (uid id, void *content, void *ref) {
    linkedlist *list = (linkedlist *) ref;
    expression *new = (expression *) newexpressioninteger(id);
    if (new) linkedlist_addentry(list, new);
}

expression *selection_idlistforgradei(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    expression *ret=FALSE;
    expression_objectreference *mref=NULL;
    int grade=0;
    
    if (nargs==2) {
        if (eval_isobjectref(args[0])) mref = (expression_objectreference *) args[0];
        if (eval_isinteger(args[1])) grade = eval_integervalue(args[1]);
        
        if (mref) {
            idtable *table=selection_idtableforgrade(sobj, (manifold_object *) eval_objectfromref(mref), grade);

            linkedlist *list = linkedlist_new();
            if (list && table) {
                idtable_map(table, selection_idlistforgrademapfunction, list);
            }
            if (list) {
                ret=(expression *) newexpressionlist(list);
                linkedlist_map(list, (linkedlistmapfunction *) freeexpression);
                linkedlist_free(list);
            }
        }
    }
    
    return ret;
}

expression *selection_isselectedi(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    int ret=FALSE;
    expression_objectreference *mref=NULL;
    int grade=0;
    uid id=0;
    
    if (nargs==3) {
        if (eval_isobjectref(args[0])) mref = (expression_objectreference *) args[0];
        if (eval_isinteger(args[1])) grade = eval_integervalue(args[1]);
        if (eval_isinteger(args[2])) id = (uid) eval_integervalue(args[2]);
        
        if ((mref)&&(mref->obj)) ret = selection_isselected(sobj, (manifold_object *) mref->obj, grade, id);
    }
    
    return newexpressionbool(ret);
}

/* Map over a selection */

void selection_map(selection_object *sel, manifold_object *mobj, unsigned int grade, idtablemapfunction *func, void *ref) {
    if (sel && sel->selection) {
        for (linkedlistentry *e=sel->selection->first; e!=NULL; e=e->next) {
            selection_manifoldref *r = (selection_manifoldref *) e->data;
            if (r->mref==mobj && grade<r->maxgrade) {
                idtable_map(r->selection[grade], func, ref);
            }
        }
    }
}

/* Initializes the field */
expression *selection_newi(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;

    sobj->selection=NULL;
    
    return NULL;
}

/* Frees the field and attached expressions */
expression *selection_freei(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    
    selection_free(sobj);
    
    return NULL;
}

unsigned int selection_maxdimension(selection_object *sobj) {
    unsigned int maxdim=0;
    for (linkedlistentry *e=sobj->selection->first; e!=NULL; e=e->next) {
        selection_manifoldref *mref = e->data;
        if (mref->mref && mref->mref->dimension > maxdim) maxdim = mref->mref->dimension;
    }
    return maxdim;
}

/* Creates a new selection with elements of a different grade */
/* TODO: Should support the option includeattached */
expression *selection_changegradei(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    unsigned int maxdim = selection_maxdimension(sobj);
    expression *ret=NULL;
    
    int outgrade[maxdim+1];
    for (unsigned int i=0; i<maxdim+1; i++) outgrade[i]=FALSE;
    
    if (nargs==1) {
        if (eval_isinteger(args[0])) {
            if (eval_integervalue(args[0])<=maxdim) outgrade[eval_integervalue(args[0])]=TRUE;
        } else if (eval_islist(args[0])) {
            expression_list *lst = (expression_list *) args[0];
            
            if (lst->list) for (linkedlistentry *e = lst->list->first; e!=NULL; e=e->next) {
                expression *val = (expression *) e->data;
                if (eval_isinteger(val) && eval_integervalue(val)<=maxdim) outgrade[eval_integervalue(val)]=TRUE;
            }
        }
        
        ret=selection_changegrade(sobj, context, outgrade, maxdim, FALSE);
    }
    return ret;
}

/* Listener called when a manifold is refined */
expression *selection_refinei(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    
    if (nargs==2 && eval_isobjectref(args[0]) && eval_islist(args[1])) {
        selection_refine(sobj, context, (manifold_object *) eval_objectfromref(args[0]), (expression_list *) args[1]);
    }
    
    return (expression *) class_newobjectreference(obj);
}

/*void selection_printselectionmapfunction (uid id, void *content, void *ref) {
    printf("%i ", (int) id);
}*/

/* Prints the contents of the selection */
/*expression *selection_printselectioni(object *obj, interpretcontext *context, int nargs, expression **args) {
    selection_object *sobj = (selection_object *) obj;
    unsigned int i=1;
    
    if ((sobj)&&(sobj->selection)) {
        for (linkedlistentry *e = sobj->selection->first; e!=NULL; e=e->next) {
            selection_manifoldref *ref = (selection_manifoldref *) e->data;
            
            if (ref) {
                printf("Manifold %i.\n",i);
                for (unsigned int j=0; j<=ref->maxgrade; j++) {
                    if (ref->selection[j]) {
                        printf(" Grade %i.\n",j);
                        idtable_map(ref->selection[j], selection_printselectionmapfunction, NULL);
                        printf("\n");
                    }
                }
            }
            
            i++;
        }
    }
    
    return (expression *) class_newobjectreference(obj);
}*/


/* Initialization */

void selectioninitialize(void) {
    classintrinsic *cls;
    
    cls=class_classintrinsic(GEOMETRY_SELECTIONCLASS, class_lookup(EVAL_OBJECT), sizeof(selection_object), 6);
    class_registerselector(cls, SELECTION_ISSELECTED_SELECTOR, FALSE, selection_isselectedi);
    class_registerselector(cls, SELECTION_IDLISTFORGRADE_SELECTOR, FALSE, selection_idlistforgradei);
    class_registerselector(cls, SELECTION_CHANGEGRADE_SELECTOR, FALSE, selection_changegradei);
    //class_registerselector(cls, SELECTION_PRINTSELECTION_SELECTOR, FALSE, selection_printselectioni);
    class_registerselector(cls, EVAL_NEWSELECTORLABEL, FALSE, selection_newi);
    class_registerselector(cls, EVAL_FREESELECTORLABEL, FALSE, selection_freei);
    class_registerselector(cls, SELECTION_REFINE_SELECTOR, FALSE, selection_refinei);
}
